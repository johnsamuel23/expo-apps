import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, KeyboardAvoidingView } from 'react-native';
import Root from './src/prayer_capsule';
import { Font, AppLoading } from 'expo';
import AppLoadingScreen from './src/prayer_capsule/AppLoading';

export default class App extends Component {

  state = {
    isReady: false
  }

  async componentWillMount() {
    
    // Expo.Util.reload() // To reload app and fetch published updates.

    await Font.loadAsync({
        'lato-light': require('./assets/fonts/Lato-Light.ttf'),
        'lato-heavy': require('./assets/fonts/Lato-Heavy.ttf'),
        'lato-regular': require('./assets/fonts/Lato-Regular.ttf'),
        'BebasNeueBold': require('./assets/fonts/BebasNeueBold.ttf'),
    });

    this.setState({ isReady: true });
  }

  render() {
    if(!this.state.isReady) {
      return <AppLoadingScreen />
    }

    return (
      <KeyboardAvoidingView style={[styles.container]} behavior="padding">
        <View style={styles.container}>
          <Root />
        </View>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});
