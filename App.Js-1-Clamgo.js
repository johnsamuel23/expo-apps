import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, KeyboardAvoidingView } from 'react-native';
import Root from './src/clamgo';

export default class App extends Component {
  render() {
    return (
      <KeyboardAvoidingView style={[styles.container]} behavior="padding">
        <View style={styles.container}>
          <Root />
        </View>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});
