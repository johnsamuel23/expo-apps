import React, { Component } from 'react';
import { StyleSheet, View, WebView } from 'react-native';

export default class App extends Component {

  render() {

    let url = "http://www.gtkforum.com";

    return (
        <View style={styles.WebViewEmbed}>
            <WebView
                automaticallyAdjustContentInsets={false}
                source={{uri: url}}
                javaScriptEnabled={true}
                domStorageEnabled={true}
                decelerationRate="normal"
                startInLoadingState={true}
            />
        </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  WebViewEmbed: {
    backgroundColor: "#ccc",
    flex: 1
  }
});
