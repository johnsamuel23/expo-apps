import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, KeyboardAvoidingView } from 'react-native';
import Root from './src/navigator';
import { Font, AppLoading } from 'expo';
import AppLoadingScreen from './src/navigator/AppLoading';

export default class App extends Component {

  state = {
    isReady: false
  }

  async componentWillMount() {
    
    // Expo.Util.reload() // To reload app and fetch published updates.

    await Font.loadAsync({
        'merriweather-light': require('./assets/fonts/merriweather-light.ttf'),
        // 'merriweather-bold': require('./assets/fonts/merriweather-bold.ttf'),
        'raleway-medium': require('./assets/fonts/raleway-medium.ttf'),
        'raleway-regular': require('./assets/fonts/raleway-regular.ttf'),
        'raleway-semibold': require('./assets/fonts/raleway-semibold.ttf'),
        'raleway-light': require('./assets/fonts/raleway-light.ttf'),
        'segoe-ui': require('./assets/fonts/segoeui.ttf'),
    });

    this.setState({ isReady: true });
  }

  render() {
    if(!this.state.isReady) {
      return <AppLoadingScreen />
    }

    return (
      <KeyboardAvoidingView style={[styles.container]} behavior="padding">
        <View style={styles.container}>
          <Root />
        </View>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});
