import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, Dimensions } from 'react-native';
import SplashScreen from '../agrostore_learn/components/SplashScreen';
import WelcomeMenu from './WelcomeMenu';
import ChurchMap from './ChurchMap';
import Newsletter from './Newsletter';
import InternetRadio from './InternetRadio';
import Resources from './Resources';
import Prayer from './Prayer';
import Blog from './Blog';
import Donation from './Donation';
import Management from './Management';
import Shopping from './Shopping';
import Bible from './Bible';
import Ministry from './Ministry';
// import StartMenu from './StartMenu';
import StartMenu from './StartMenu2';
import Events from './Events';
import { theme } from './theme';


export default class App extends Component {
  state = {
    nextScreen: 'start_screen'
  }

  changeScreen(nextScreen) {
    this.setState({
      nextScreen: nextScreen
    })
  }

  changeHeaderPanColor(color) {
    this.setState({
      headerPanColor: color
    })
  }

  render() {
    
    let currentScreen = 'splash';
    switch(this.state.nextScreen) {
      case 'splash_screen':
        currentScreen = <SplashScreen store={this.state} 
                            changeScreen={this.changeScreen.bind(this)}
                            changeHeaderPanColor={this.changeHeaderPanColor.bind(this)}
                          />
        break;
      case 'welcome_screen':
        currentScreen = <WelcomeMenu store={this.state} 
                          changeScreen={this.changeScreen.bind(this)} />
        break;
      case 'map_screen':
        currentScreen = <ChurchMap store={this.state} 
                            changeScreen={this.changeScreen.bind(this)}
                            changeHeaderPanColor={this.changeHeaderPanColor.bind(this)}
                          />
        break;
      case 'newsletter_screen':
        currentScreen = <Newsletter store={this.state} 
                            changeScreen={this.changeScreen.bind(this)}
                            changeHeaderPanColor={this.changeHeaderPanColor.bind(this)}
                          />
        break;
      case 'radio_screen':
        currentScreen = <InternetRadio store={this.state} 
                            changeScreen={this.changeScreen.bind(this)}
                            changeHeaderPanColor={this.changeHeaderPanColor.bind(this)}
                          />
        break;
      case 'prayer_screen':
        currentScreen = <Prayer store={this.state} 
                            changeScreen={this.changeScreen.bind(this)}
                            changeHeaderPanColor={this.changeHeaderPanColor.bind(this)}
                          />
        break;
      case 'blog_screen':
        currentScreen = <Blog store={this.state} 
                            changeScreen={this.changeScreen.bind(this)}
                            changeHeaderPanColor={this.changeHeaderPanColor.bind(this)}
                          />
        break;
      case 'donation_screen':
        currentScreen = <Donation store={this.state} 
                            changeScreen={this.changeScreen.bind(this)}
                            changeHeaderPanColor={this.changeHeaderPanColor.bind(this)}
                          />
        break;
      case 'management_screen':
        currentScreen = <Management store={this.state} 
                            changeScreen={this.changeScreen.bind(this)}
                            changeHeaderPanColor={this.changeHeaderPanColor.bind(this)}
                          />
        break;
      case 'shopping_screen':
        currentScreen = <Shopping store={this.state} 
                            changeScreen={this.changeScreen.bind(this)}
                            changeHeaderPanColor={this.changeHeaderPanColor.bind(this)}
                          />
        break;
      case 'bible_screen':
        currentScreen = <Bible store={this.state} 
                            changeScreen={this.changeScreen.bind(this)}
                            changeHeaderPanColor={this.changeHeaderPanColor.bind(this)}
                          />
        break;
      case 'ministry_screen':
        currentScreen = <Ministry store={this.state} 
                            changeScreen={this.changeScreen.bind(this)}
                            changeHeaderPanColor={this.changeHeaderPanColor.bind(this)}
                          />
        break;
      case 'events_screen':
        currentScreen = <Events store={this.state} 
                            changeScreen={this.changeScreen.bind(this)}
                            changeHeaderPanColor={this.changeHeaderPanColor.bind(this)}
                          />
        break;
      case 'start_screen':
        currentScreen = <StartMenu store={this.state} 
                            changeScreen={this.changeScreen.bind(this)}
                            changeHeaderPanColor={this.changeHeaderPanColor.bind(this)}
                          />
        break;
      case 'resources_screen':
        currentScreen = <Resources store={this.state} 
                            changeScreen={this.changeScreen.bind(this)}
                            changeHeaderPanColor={this.changeHeaderPanColor.bind(this)}
                          />
        break;
      default: 
        currentScreen =  <WelcomeMenu store={this.state} 
                          changeScreen={this.changeScreen.bind(this)} />;
    }

    return (
      <View style={[styles.container]}>
        <View style={[styles.header, {backgroundColor: this.state.headerPanColor}]}>
        </View>
          <View style={styles.container}> 
              { currentScreen }
          </View>
      </View>
    );
  }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#040d46',
        width: theme.contentWidth
    },
    header: {
        backgroundColor: '#116cc5',
        minHeight: 25
    },
    headerText: {
        fontSize: 20
    }
});
