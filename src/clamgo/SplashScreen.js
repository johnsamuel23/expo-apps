import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, Dimensions } from 'react-native';
import LocalImage from './components/LocalImage';

export default class SplashScreen extends Component {
  render() {
    return (
      <View style={[styles.container]}>
        <LocalImage source={require('./img/splash_pic.png')} 
            originalWidth="1000"
            originalHeight="1000"
            guideWidth={Dimensions.get('window').width}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#2d3192',
  },
});
