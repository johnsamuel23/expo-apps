import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';

export default class SocialIconLink extends React.Component {

    render() {
        return (
            <TouchableOpacity style={[styles.iconOpaqueWrapper]} 
                onPress={() => this.props.openWebLink()}>
                <View style={[styles.iconWrapper, {backgroundColor: this.props.bgColor}]}>
                    { this.props.icon }
                </View>
            </TouchableOpacity>
        );
  }
}

const styles = StyleSheet.create({
    iconWrapper: {
        height: 50,
        width: 50,
        // padding: 10,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        borderRadius: 14,
    },
    iconOpaqueWrapper: {
        paddingLeft: 5,
        paddingRight: 5,
        paddingTop: 10,
        paddingBottom: 10,
    },
    iconText: {
       textAlign: 'center',
       color: '#485b92'
    },
});
