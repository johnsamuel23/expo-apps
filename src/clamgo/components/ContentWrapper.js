import React from 'react';
import { View, StyleSheet } from 'react-native';
import { theme } from '../theme';

export default ContentWrapper = ({
    children
}) => {
    return (
        <View style={styles.container}>
            { children}
        </View>
    )
} 

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: theme.contentWidth,
        marginTop: theme.contentMarginTop,
        marginLeft: 'auto',
        marginRight: 'auto',
        backgroundColor: 'white'
    }
});