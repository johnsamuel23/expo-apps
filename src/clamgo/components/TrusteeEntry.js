import React from 'react';
import { 
    StyleSheet, 
    Text, 
    View,
    TouchableOpacity,
} from 'react-native';
import LocalImage from '../components/LocalImage';
// import { theme } from ''

export default class Entry extends React.Component {

    render() {
        let { data } = this.props;
        let { image } = data;
        return (
            <View style={styles.entry}>
                <LocalImage source={image.source} 
                    originalWidth={image.originalWidth}
                    originalHeight={image.originalHeight}
                    guideWidth={image.guideWidth}
                />
                <View style={styles.entryInfo}>
                    <View><Text style={styles.entryName}>{ data.name} </Text></View>
                    <View><Text style={styles.entryDesignation}>({data.designation}) </Text></View>
                    <View><Text style={styles.entryPostInfo}>{ data.postInfo} </Text></View>
                </View>
            </View>
            
        );
    }
}

const styles = StyleSheet.create({
    entry: {
        padding: 20,
        paddingTop: 30,
        paddingBottom: 30,
        overflow: 'hidden',
        alignItems: 'center',
        borderBottomColor: '#ddd',
        borderBottomWidth: 1
    },
    entryName: {
        alignItems: 'center',
        textAlign: 'center',
        marginTop: 10,
        marginBottom: 5,
        fontSize: 16   
    },
    entryDesignation: {
        alignItems: 'center',
        textAlign: 'center',
        color: '#cf2820',
        marginBottom: 5,
        fontSize: 13
    },
    entryPostInfo: {
        alignItems: 'center',
        textAlign: 'center',
        color: 'rgb(152, 152, 152)',
        fontSize: 15
    }
});



