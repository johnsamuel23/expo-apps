import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, TouchableOpacity } from 'react-native';

export default class App extends Component {
  state = {
    items: [
      {
        name: 'vision',
        title: 'OUR VISION',
      },
      {
        name: 'identity',
        title: 'WHO WE ARE',
      },
      {
        name: 'mission',
        title: 'OUR MISSION',
      },
      {
        name: 'history',
        title: 'OUR HISTORY',
      }
    ],
    activeItem: 'vision'
  }

  changeItem(itemName) {
    this.setState({
      activeItem: itemName
    });

    this.props.switchTabItem(itemName);
  }

  render() {
    const menu = this.state.items.map((item, index) => {
      let activeStyles = {backgroundColor: 'red'};
      let activeTextStyles = {color: 'white'};

      if(item.name === this.state.activeItem) {
        activeStyles = {backgroundColor: 'blue'};         
        activeTextStyles = {color: '#f1f1f1'};         
      }

      return <TouchableOpacity key={item.name} style={[styles.NavTab, activeStyles]} 
                onPress={() => this.changeItem(item.name)}>
            <Text style={[styles.NavText]}>{ item.title }</Text>
        </TouchableOpacity>
    });

    return (
      <ScrollView contentContainerStyle={[styles.container]} horizontal={true} showsHorizontalScrollIndicator={false}>
          { menu }        
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#555',
    maxHeight: 50,
  },
  NavTab: {
    backgroundColor: 'red',
    padding: 15,
    justifyContent: 'center',
    alignItems: 'center'
  },
  NavText: {
    fontSize: 18
  }
});
