import React from 'react';
import { StyleSheet, View, Dimension, TouchableOpacity, TouchableHighlight } from 'react-native';
import LocalImage from './LocalImage';

export default class TouchImage extends React.Component {
  
  changeScreen() {
    if(this.props.changeScreen !== undefined) {
      this.props.changeScreen();
    }
  }

  render() {
    let moreStyles = {marginTop: 0, marginBottom: 0};
    if(this.props.gutter) {
      moreStyles.marginTop = 5;
      moreStyles.marginBottom = 5;
    }

    return (
      <View style={[styles.container, moreStyles, this.props.style]}>
        <TouchableHighlight 
          onPress={() => this.changeScreen()}
          underlayColor="red"
          >
            <View>
                <LocalImage
                    source={this.props.source}
                    originalWidth={this.props.originalWidth}
                    originalHeight={this.props.originalHeight}
                    guideWidth={this.props.guideWidth}
                />
            </View>
        </TouchableHighlight> 
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    // backgroundColor: '#1a1d6f',
    alignItems: 'center',
    overflow: 'hidden',
  },
});
