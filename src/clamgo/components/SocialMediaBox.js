import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, Linking } from 'react-native';
import TouchImage from '../components/TouchImage';
import BackgroundImage from '../components/BackgroundImage';
import SocialIconLink from '../components/SocialIconLink';

import {
    Entypo,
    FontAwesome,
    Ionicons,
    MaterialCommunityIcons,
    MaterialIcons
} from '@expo/vector-icons';

const FaceBookIcon = (<FontAwesome name="facebook" size={25} color='#fff' />)
const TwitterIcon = (<FontAwesome name="twitter" size={25} color='#fff' />)
const InstagramIcon = (<FontAwesome name="instagram" size={55} color='#fff' />)
const YoutubeIcon = (<Entypo name="youtube" size={25} color='#fff' />)

export default class SocialMediaBox extends Component {

  openSocialLink(platform) {
    let twitter_link = 'https://twitter.com/clam_church';
    let facebook_link = "http://www.facebook.com/clamchurch"; 
    let instagram_link = "https://www.instagram.com/clam_church/";
    let youtube_link = "http://www.youtube.com/clamgo";

    switch(platform) {
      case "facebook":
        Linking.openURL(facebook_link);
        break;
      case "twitter":
        Linking.openURL(twitter_link);
        break;
      case "instagram":
        Linking.openURL(instagram_link);
        break;
      case "youtube":
        Linking.openURL(youtube_link);
        break;
      default: 
        return false;
    }
    return false;
  }

  render() {

    let { bgGuideWidth } = this.props;
    let moreStyles = {marginTop: 0, marginBottom: 0};
    if(this.props.gutter) {
      moreStyles.marginTop = 5;
      moreStyles.marginBottom = 5;
      moreStyles.paddingLeft = this.props.gutterSize / 2;
      moreStyles.paddingRight = this.props.gutterSize / 2;
    }

    return (
      <View style={[styles.container, moreStyles]}>
          <BackgroundImage
              style={styles.bgImage}
              source={require('../img/welcomescreenSLIZE_16.jpg')}>

            <View style={styles.socialBox}>
              <View style={styles.socialBoxContent}>
                <SocialIconLink 
                  icon={FaceBookIcon}
                  openWebLink={() => this.openSocialLink('facebook')}
                  bgColor="#3b5999"
                />
                <SocialIconLink 
                  icon={TwitterIcon}
                  openWebLink={() => this.openSocialLink('twitter')}
                  bgColor="#00b0ed"            
                />
                <SocialIconLink 
                  icon={InstagramIcon}
                  openWebLink={() => this.openSocialLink('instagram')}
                  bgColor="#e95950"
                  noPadding={true}              
                />
                <SocialIconLink 
                  icon={YoutubeIcon}
                  openWebLink={() => this.openSocialLink('youtube')}
                  bgColor="#e6241b"
                />
              </View>
            </View>

          </BackgroundImage>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'transparent',
  },
  socialBox: {
    paddingLeft: 25,
    paddingRight: 25,
    paddingTop: 20,
    paddingBottom: 20
  },
  socialBoxContent: {
    flexDirection: 'row',
    justifyContent: 'space-around'
  }
});
