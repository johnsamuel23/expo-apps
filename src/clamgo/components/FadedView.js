import React, { Component } from 'react';
import { Animated, Text, View } from 'react-native';

class FadeInView extends Component {
  state = {
    fadeAnim: new Animated.Value(0),  // Initial value for opacity: 0
  }

  componentDidMount() {
    // Animated.timing(                  // Animate over time
    //   this.state.fadeAnim,            // The animated value to drive
    //   {
    //     toValue: 1,                   // Animate to opacity: 1 (opaque)
    //     duration: 300,              // Make it take a while
    //   }
    // ).start();                        // Starts the animation
  }

  render() {
    let { fadeAnim } = this.state;

    return (
      <Animated.View                 // Special animatable View
        // Bind opacity to animated value
        style={[this.props.style, {opacity: 1}]}
      >
        {this.props.children}
      </Animated.View>
    );
  }
}

export default FadeInView;