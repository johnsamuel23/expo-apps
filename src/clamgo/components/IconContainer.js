import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import {
    Entypo,
    FontAwesome,
    Ionicons,
    MaterialCommunityIcons,
    MaterialIcons
 } from '@expo/vector-icons';

export default class App extends React.Component {

  render() {
    return (
        <TouchableOpacity style={[styles.iconOpaqueWrapper]} onPress={() => this.props.changeScreen()}>
            <View style={[styles.iconWrapper, {backgroundColor: this.props.bgColor}]}>
                { this.props.icon }
            </View>
            <Text style={styles.iconText}>{this.props.text}</Text>
        </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
    iconWrapper: {
        // backgroundColor: 'red',
        height: 60,
        width: 60,
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        borderRadius: 10,
        marginBottom: 5
    },
    iconOpaqueWrapper: {
        paddingLeft: 5,
        paddingRight: 5,
        paddingTop: 10,
        paddingBottom: 10,
        marginBottom: 10
    },
    iconText: {
       textAlign: 'center',
       color: '#485b92'
    },
});
