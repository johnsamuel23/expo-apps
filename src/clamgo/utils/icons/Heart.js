import React, { Component } from 'react';
import { FontAwesome } from '@expo/vector-icons';
import { View } from 'react-native';

const Icon = (<FontAwesome name="heart" size={25} color="#555" />)

export default class Heart extends Component {
    render() {
        return (
            <View>
                { Icon }     
            </View>
        )
    }
}