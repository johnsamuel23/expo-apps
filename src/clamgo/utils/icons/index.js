import React from 'react';
import { FontAwesome, Entypo, MaterialCommunityIcons, Ionicons, MaterialIcons } from '@expo/vector-icons';

import PinIcon  from './Pin';
import ShareIcon  from './Share';
import HeartIcon  from './Heart';
import BackIcon  from './Back';
import MoreIcon  from './More';

export const Share = ShareIcon;
export const Pin = PinIcon;
export const More = MoreIcon;
export const Back = BackIcon;
export const Heart = HeartIcon;


