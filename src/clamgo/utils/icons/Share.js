import React, { Component } from 'react';
import { Ionicons } from '@expo/vector-icons';
import { View } from 'react-native';

const Icon = (<Ionicons name="ios-send" size={35} color="#555" />)

export default class Share extends Component {
    render() {
        return (
            <View>
                { Icon }     
            </View>
        )
    }
}