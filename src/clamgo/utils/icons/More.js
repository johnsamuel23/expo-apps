import React, { Component } from 'react';
import { MaterialIcons } from '@expo/vector-icons';
import { View } from 'react-native';

const Icon = (<MaterialIcons name="more-horiz" size={45} color="#555" />)

export default class More extends Component {
    render() {
        return (
            <View>
                { Icon }     
            </View>
        )
    }
}