import React, { Component } from 'react';
import { 
    StyleSheet, 
    Text, 
    View, 
    ScrollView, 
    TouchableOpacity,
    WebView
} from 'react-native';
import { Navigator } from 'react-native-deprecated-custom-components';
import { 
    Entypo
} from '@expo/vector-icons';
import CustomTabBar from './components/CustomTabBar';
import ScrollableTabView, { ScrollableTabBar } from 'react-native-scrollable-tab-view';
import Row from './layouts/Row';
import FadedView from './components/FadedView';
import AboutChurchVision from './AboutChurchVision';
import AboutChurchIdentity from './AboutChurchIdentity';
import AboutChurchMission from './AboutChurchMission';
import AboutChurchHistory from './AboutChurchHistory';

const Back = (<Entypo name="arrow-left" size={35} color='#fff' />);

export default class AboutChurch extends Component {
    state = {
        activeTabColor: "#111570"
    }

    componentDidMount() {
        this.props.changeHeaderPanColor('#757575');
    }

    TabScroll() {
        this.setState({
            activeTabColor: 'transparent'
        });

        console.log("Tab Scrolled");
    }
    
    changeScreen(nextScreen) {
        this.props.navigator.pop({name: 'about_screen'});
    }

    switchTabItem(nextScreen) {
        // this.navigator.pop(nextScreen);  
        this.navigator.push(nextScreen);   
    }
    render() {

        return (
            <FadedView style={styles.container}>
                <View style={styles.header}>
                    <Row style={{alignItems: 'center'}}>
                        <TouchableOpacity onPress={() => this.changeScreen('welcome_screen')} 
                            style={styles.BackButton}>
                            { Back }
                        </TouchableOpacity>
                        <Text style={styles.headerText}>About CLAM</Text>
                    </Row>
                </View>
                 <View style={styles.container}> 
                    <ScrollableTabView
                        style={{marginTop: 0}}
                        initialPage={0}
                        renderTabBar={() => <CustomTabBar 
                            activeBackgroundColor={this.state.activeTabColor} 
                            inactiveBackgroundColor="transparent"
                            backgroundColor="#eb1c22" 
                            activeTextColor="#fff" 
                            underlineStyle={{height: 49, zIndex: 0,}}
                            tabStyle={{zIndex: 1,}}
                            inactiveTextColor="#fff" />} 
                    >
                        <ScrollView tabLabel="OUR VISION" style={styles.tabView}>
                            <AboutChurchVision />
                        </ScrollView>
                        <ScrollView tabLabel="WHO WE ARE" style={styles.tabView}>
                            <AboutChurchIdentity />  
                        </ScrollView>
                        <ScrollView tabLabel="OUR MISSION" style={styles.tabView}>
                            <AboutChurchMission /> 
                        </ScrollView>
                        <ScrollView tabLabel="CLAM HISTORY" style={styles.tabView}>
                            <AboutChurchHistory /> 
                        </ScrollView> 
                    </ScrollableTabView>
                </View> 
            </FadedView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    header: {
        backgroundColor: '#02223b',
        minHeight: 50,
        justifyContent: 'center'
    },
    headerText: {
        fontSize: 15,
        color: 'white',
        justifyContent: 'center',
    },
    BackButton: {
        marginRight: 10,
        marginLeft: 10
    },
    WebViewEmbed: {
        backgroundColor: "gray",
        flex: 1
    },
    content: {
        // paddingLeft: 10,
        // paddingRight: 10,
        // paddingTop: 10,
        // paddingBottom: 10
    },
    section: {
        backgroundColor: '#f7f7f7'
    },
    sectionTitle: {
        backgroundColor: '#a9e0ec',
        borderBottomColor: '#bbb',
        borderBottomWidth: 1,
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 15,
        paddingRight: 15
    },
    sectionTitleText: {
        fontSize: 20
    },
    sectionContent: {
        backgroundColor: '#f7f7f7',
        paddingBottom: 20,
        paddingTop: 10,
        paddingLeft: 15,
        paddingRight: 15
    },
    sectionContentText: {
        fontSize: 15,
    },
    bulletPoint: {
        flexDirection: 'row',
        marginBottom: 15
    },
    bulletNumber: {
        fontWeight: 'bold',
        color: '#c22727',
        // marginRight: 20
    },
    bulletText: {
        paddingLeft: 10,
        fontSize: 15,
        textAlign: 'justify'
    },
    paragraph: {
        marginTop: 10,
        marginBottom: 10
    },
    paragraphText: {
        fontSize: 15
    },
    subHeadBox: {
        backgroundColor: '#c22727',
        padding: 10,
        marginBottom: 20,
        borderRadius: 5

    },
    subHeadBoxText: {
        color: 'white',
        fontSize: 20
    }
});
