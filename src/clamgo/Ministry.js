import React from 'react';
import { 
    StyleSheet, 
    Text, 
    View, 
    ScrollView, 
    TouchableOpacity,
    WebView
} from 'react-native';
import { 
    Entypo
} from '@expo/vector-icons';
import Row from './layouts/Row';
import FadedView from './components/FadedView';

const Back = (<Entypo name="arrow-left" size={35} color='#fff' />)

export default class Ministry extends React.Component {

    componentDidMount() {
        if(!this.props.noHeader === true) {
            this.props.changeHeaderPanColor('#757575');
        }
    }
    
    changeScreen(nextScreen) {
        this.props.navigator.pop({name: 'ministry_screen'});
    }

    renderHeader() {
        if(!this.props.noHeader === true) {
            return (
                <View style={styles.header}>
                    <Row style={{alignItems: 'center'}}>
                        <TouchableOpacity onPress={() => this.changeScreen('welcome_screen')} 
                            style={styles.BackButton}>
                            { Back }
                        </TouchableOpacity> 
                        <Text style={styles.headerText}>Ministry Products</Text>
                    </Row>
                </View>
            )
        }
    }

    render() {

        let webUrl = 'http://woleoladiyun.com/';
        let Header = this.renderHeader();

        if(!this.props.ministry === undefined) {
            if(this.props.ministry === 'wole') {
                webUrl = 'www.woleoladiyun.com';
            } else if (this.props.ministry === 'voice_of_joy') {
                webUrl = 'www.woleoladiyun.com/voice-of-joy';
            }
        }

        return (
            <View style={styles.container}> 
                <View>
                    {!this.props.noHeader === true && Header }
                </View>
                <View style={styles.WebViewEmbed}>
                    <WebView
                        automaticallyAdjustContentInsets={false}
                        source={{uri: webUrl}}
                        javaScriptEnabled={true}
                        domStorageEnabled={true}
                        decelerationRate="normal"
                        startInLoadingState={true}
                    />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    header: {
        backgroundColor: '#02223b',
        minHeight: 50,
        justifyContent: 'center'
    },
    headerText: {
        fontSize: 15,
        color: 'white',
        justifyContent: 'center',
    },
    BackButton: {
        marginRight: 10,
        marginLeft: 10
    },
    WebViewEmbed: {
        backgroundColor: "#ccc",
        flex: 1
    }
});
