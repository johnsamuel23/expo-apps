import React, { Component } from 'react';
import { 
    StyleSheet, 
    Text, 
    View, 
    ScrollView, 
    TouchableOpacity,
    WebView
} from 'react-native';
import { 
    Entypo
} from '@expo/vector-icons';
import Row from './layouts/Row';
import FadedView from './components/FadedView';
import HorizontalTab from './components/HorizontalTab';

const Back = (<Entypo name="arrow-left" size={35} color='#fff' />)

export default class AboutChurchIdentity extends Component {

    render() {
        return (
            <View style={styles.section}>
                {/* <View style={styles.sectionTitle}>
                    <Text style={styles.sectionTitleText}>Who We Are</Text> 
                </View> */}
                <View style={styles.sectionContent}>
                    <Text style={styles.sectionContentText}>We are an eternity-conscious people glorifying God with our lives for the great grace of our salvation in Christ, happy to be co-labourers with Him in winning souls for His eternal Kingdom and destroying the works of darkness in the name of Jesus</Text>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    header: {
        backgroundColor: '#02223b',
        minHeight: 50,
        justifyContent: 'center'
    },
    headerText: {
        fontSize: 15,
        color: 'white',
        justifyContent: 'center',
    },
    BackButton: {
        marginRight: 10,
        marginLeft: 10
    },
    WebViewEmbed: {
        backgroundColor: "gray",
        flex: 1
    },
    content: {
        // paddingLeft: 10,
        // paddingRight: 10,
        // paddingTop: 10,
        // paddingBottom: 10
    },
    section: {
        // backgroundColor: '#f7f7f7'
    },
    sectionTitle: {
        backgroundColor: '#a9e0ec',
        borderBottomColor: '#bbb',
        borderBottomWidth: 1,
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 15,
        paddingRight: 15
    },
    sectionTitleText: {
        fontSize: 20
    },
    sectionContent: {
        // backgroundColor: '#f7f7f7',
        paddingBottom: 25,
        paddingTop: 25,
        paddingLeft: 25,
        paddingRight: 25
    },
    sectionContentText: {
        fontSize: 20,
        lineHeight: 35
    },
    bulletPoint: {
        flexDirection: 'row',
        marginBottom: 15
    },
    bulletNumber: {
        fontWeight: 'bold',
        color: '#c22727',
        // marginRight: 20
    },
    bulletText: {
        paddingLeft: 10,
        fontSize: 15,
        textAlign: 'justify'
    },
    paragraph: {
        marginTop: 10,
        marginBottom: 10
    },
    paragraphText: {
        fontSize: 15
    },
    subHeadBox: {
        backgroundColor: '#c22727',
        padding: 10,
        marginBottom: 20,
        borderRadius: 5

    },
    subHeadBoxText: {
        color: 'white',
        fontSize: 20
    }
});
