import React, { Component } from 'react';
import { 
    StyleSheet, 
    Text, 
    View, 
    ScrollView, 
    TouchableOpacity,
    TouchableHighlight,
    Dimensions
} from 'react-native';
import LocalImage from './components/LocalImage';
import TouchImage from './components/TouchImage';
import SocialMedia from './components/SocialMediaBox';

export default class StartMenu extends Component {

    componentDidMount() {
        this.props.changeHeaderPanColor('#1a1d6f');
    }

    changeScreen(nextScreen) {
        this.props.changeScreen(nextScreen);
    }

    render() {
        
        let bgGuideWidth = Dimensions.get('window').width;

        return (
            <ScrollView contentContainerStyle={styles.container}>
                <LocalImage source={require('./img/welcomescreen_slize_0101.jpg')} 
                    originalWidth="1611"
                    originalHeight="524"
                    guideWidth={Dimensions.get('window').width}
                />
                <View style={{marginTop: 1, marginBottom: 1}}>
                    <TouchImage source={require('./img/welcomescreenSLIZE_03.jpg')} 
                        originalWidth="1611"
                        originalHeight="320"
                        guideWidth={bgGuideWidth}
                    />
                </View>
                <View>
                    <TouchImage source={require('./img/welcomescreenSLIZE_05.jpg')} 
                        originalWidth="1611"
                        originalHeight="478"
                        changeScreen={() => this.changeScreen('welcome_screen')}
                        guideWidth={bgGuideWidth}
                    />
                </View>
                <TouchImage source={require('./img/welcomescreenSLIZE_07.jpg')} 
                    originalWidth="1611"
                    originalHeight="424"
                    guideWidth={bgGuideWidth}
                    changeScreen={() => this.changeScreen('events_screen')}
                />
                <TouchImage source={require('./img/welcomescreenSLIZE_09.jpg')} 
                    originalWidth="1611"
                    originalHeight="425"
                    guideWidth={bgGuideWidth}
                    changeScreen={() => this.changeScreen('about_screen')}
                />
                <TouchImage source={require('./img/welcomescreenSLIZE_11.jpg')} 
                    originalWidth="1611"
                    originalHeight="557"
                    guideWidth={bgGuideWidth}
                    changeScreen={() => this.changeScreen('bible_screen')}
                />
                <TouchImage source={require('./img/welcomescreenSLIZE_12.jpg')} 
                    originalWidth="1611"
                    originalHeight="330"
                    guideWidth={bgGuideWidth}
                    changeScreen={() => this.changeScreen('prayer_screen')}
                />
                <TouchImage source={require('./img/welcomescreenSLIZE_14.jpg')} 
                    originalWidth="1611"
                    originalHeight="572"
                    guideWidth={bgGuideWidth}
                    changeScreen={() => this.changeScreen('shopping_screen')}
                />
                <SocialMedia 
                    bgGuideWidth={bgGuideWidth}
                    changeScreen={this.props.changeScreen} 
                /> 
                <TouchImage source={require('./img/welcomescreenSLIZE_17.jpg')} 
                    originalWidth="1611"
                    originalHeight="448"
                    guideWidth={bgGuideWidth}
                />
                <TouchImage source={require('./img/welcomescreenSLIZE_18.jpg')} 
                    originalWidth="1611"
                    originalHeight="461"
                    guideWidth={bgGuideWidth}
                    changeScreen={() => this.changeScreen('map_screen')}
                />
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
    }
});
