import React, { Component } from 'react';
import { 
    StyleSheet, 
    Text, 
    View, 
    ScrollView, 
    TouchableOpacity, 
    BackHandler 
} from 'react-native';
import {
    Entypo,
    FontAwesome,
    Ionicons,
    MaterialCommunityIcons,
    MaterialIcons
 } from '@expo/vector-icons';
import Row from './layouts/Row';
import Col from './layouts/Col';
import FadedView from './components/FadedView';
import IconContainer from './components/IconContainer';
import ContentWrapper from './components/ContentWrapper';
import { theme } from './theme';

const Back = (<Entypo name="arrow-left" size={35} color='#fff' />);
const GlobeIcon = (<Entypo name="globe" size={35} color='#fff' />);
const Direction = (<Entypo name="direction" size={35} color='#fff' />);
const MapMarker = (<FontAwesome name="map-marker" size={35} color='#fff' />);
const NavigateIcon = (<Entypo name="location" size={35} color='#fff' />);
const ShoppingBasket = (<FontAwesome name="shopping-basket" size={35} color='#fff' />);
const Donate = (<FontAwesome name="gratipay" size={35} color='#fff' />);
const BlogIcon = (<MaterialCommunityIcons name="blogger" size={35} color='#fff' />);
const PrayerIcon = (<MaterialCommunityIcons name="baby" size={35} color='#fff' />);
const BoardIcon = (<MaterialCommunityIcons name="developer-board" size={35} color='#fff' />);
const BibleIcon = (<MaterialCommunityIcons name="bible" size={35} color='#fff' />);
const RadioIcon = (<Entypo name="radio" size={35} color='#fff' />);
const libraryBooks = (<MaterialIcons name="library-books" size={35} color='#fff' />);
const NewsletterIcon = (<Entypo name="newsletter" size={35} color='#fff' />);
const galleryIcon = (<Entypo name="newsletter" size={35} color='#fff' />);
const TVIcon = (<FontAwesome name="tv" size={35} color='#fff' />);
    
export default class WelcomeMenu extends Component {

  changeScreen(screen) {
    this.props.navigator.push({name: screen});
  }

  render() {
    return (
        <ContentWrapper> 
            <ScrollView contentContainerStyle={styles.container} showsVerticalScrollIndicator={false}>
                <Row style={styles.row}>
                    <Col style={[styles.iconColumn]}>
                        <IconContainer
                            icon={Back} 
                            text="Back To Home" 
                            bgColor="#061898"
                            changeScreen={() => this.props.navigator.pop({name: 'start_screen'})}
                        />
                    </Col>
                    <Col style={[styles.iconColumn]}>
                        <IconContainer 
                            icon={PrayerIcon} 
                            text="Prayer Centers" 
                            bgColor="#e03136" 
                            changeScreen={() => this.changeScreen('branches_screen')}
                        />
                    </Col>
                    <Col style={[styles.iconColumn]}>
                        <IconContainer 
                            icon={libraryBooks} 
                            text="Prayer Request" 
                            bgColor="#c20000" 
                            bgColor2='#52bbfd' 
                            changeScreen={() => this.changeScreen('prayer_request_screen')}
                        />
                    </Col>
                </Row>
                <Row style={styles.row}>
                    <Col style={[styles.iconColumn]}>
                        <IconContainer 
                            icon={RadioIcon} 
                            text="CLAM Radio" 
                            bgColor="#d0b04d" 
                            changeScreen={() => this.changeScreen('radio_screen')}
                        />
                    </Col>
                    <Col style={[styles.iconColumn]}>
                        <IconContainer 
                            icon={TVIcon} 
                            text="CLAM TV" 
                            bgColor="#b3398e" 
                            bgColor2="#116cc5"
                            changeScreen={() => this.changeScreen('tv_screen')}
                        />
                    </Col>
                    <Col style={[styles.iconColumn]}>
                        <IconContainer 
                            icon={BlogIcon} 
                            text="CLAM Blog" 
                            bgColor="#7543c8" 
                            changeScreen={() => this.changeScreen('blog_screen')}
                        />
                    </Col>
                </Row>
                <Row style={styles.row}>
                    {/* <Col style={[styles.iconColumn]}>
                        <IconContainer 
                            icon={Donate} 
                            text="Give Online" 
                            bgColor="#116cc5" 
                            changeScreen={() => this.changeScreen('donation_screen')}
                        />
                    </Col> */}
                    <Col style={[styles.iconColumn]}>
                        <IconContainer 
                            icon={ BoardIcon } 
                            text="Our Trustees"
                            bgColor="#c756a4" 
                            changeScreen={() => this.changeScreen('trustees_screen')}
                        />
                    </Col>
                    <Col style={[styles.iconColumn]}>
                        <IconContainer 
                            icon={MapMarker} 
                            text="Find CLAM"
                            bgColor="#43c85d"
                            changeScreen={() => {this.props.selectLocation('clam_church'); this.changeScreen('map_screen')}}
                        />
                    </Col>
                    <Col style={[styles.iconColumn]}>
                        <IconContainer 
                            icon={ GlobeIcon } 
                            text="Ministry Products"
                            bgColor="#c73327" 
                            changeScreen={() => this.changeScreen('ministries_screen')}
                        />
                    </Col>
                </Row>
                <Row style={styles.row}>
                    {/* <Col style={[styles.iconColumn]}>
                        <IconContainer 
                            icon={ ShoppingBasket } 
                            text="CLAM Shop"
                            bgColor="#ff9800" 
                            changeScreen={() => this.changeScreen('shopping_screen')}
                        />
                    </Col> */}
                    <Col style={[styles.iconColumn]}>
                        <IconContainer
                            icon={PrayerIcon} 
                            text="Prayer Solutions" 
                            bgColor="#061898"
                            changeScreen={() => this.changeScreen('prayer_solution_screen')}
                        />
                    </Col>
                    <Col style={[styles.iconColumn]}>
                        <IconContainer 
                            icon={galleryIcon} 
                            text="Gallery" 
                            bgColor="#7543c8" 
                            changeScreen={() => this.changeScreen('gallery_screen')}
                        />
                    </Col>
                    <Col style={[styles.iconColumn]}>
                        <IconContainer 
                            icon={BibleIcon} 
                            text="Mobile Bible"
                            bgColor="#1d45b4" 
                            changeScreen={() => this.changeScreen('bible_screen')}
                        />
                    </Col>
                </Row>
                <Row style={styles.row}>
                    <Col style={[styles.iconColumn]}>
                        <IconContainer 
                            icon={libraryBooks} 
                            text="Sermons" 
                            bgColor="#c20000" 
                            changeScreen={() => this.changeScreen('sermon_screen')}
                        />
                    </Col>
                    <Col style={[styles.iconColumn]}>
                        {/* <IconContainer
                            icon={PrayerIcon} 
                            text="Prayer Solutions" 
                            bgColor="#061898"
                            changeScreen={() => this.changeScreen('prayer_solution_screen')}
                        /> */}
                    </Col>
                    <Col style={[styles.iconColumn]}>
                        {/* <IconContainer 
                            icon={galleryIcon} 
                            text="Gallery" 
                            bgColor="#7543c8" 
                            changeScreen={() => this.changeScreen('gallery_screen')}
                        /> */}
                    </Col>
                </Row>
            </ScrollView>
        </ContentWrapper>
    );
  }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#ffffff',
        paddingTop: 10,
        marginTop: theme.contentMarginTop
    },
    iconColumn: {
        flex: 1,
        alignItems: 'center',
    },
    iconWrapper: {
        height: 60,
        width: 60,
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        borderRadius: 10,
        marginBottom: 5
    },
    iconOpaqueWrapper: {
        paddingLeft: 5,
        paddingRight: 5,
        paddingTop: 10,
        paddingBottom: 10,
        marginBottom: 10
    },
    iconText: {
       textAlign: 'center',
       color: '#485b92'
    },
    row: {
        height: 120,
    }
});
