import React, { Component } from 'react';
import { 
    StyleSheet, 
    Text, 
    View, 
    ScrollView, 
    TouchableOpacity,
    WebView
} from 'react-native';
import { 
    Entypo
} from '@expo/vector-icons';
import Row from './layouts/Row';
import FadedView from './components/FadedView';
import HorizontalTab from './components/HorizontalTab';

const Back = (<Entypo name="arrow-left" size={35} color='#fff' />)

export default class AboutChurchHistory extends Component {

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.section}>
                    {/* <View style={styles.sectionTitle}>
                        <Text style={styles.sectionTitleText}>Our History </Text> 
                    </View> */}
                    <View style={styles.sectionContent}>
                        <View>
                            <View>
                                <Text style={styles.subHead}>IN THE BEGINNING</Text>
                            </View>
                            <View style={styles.paragraph}>
                                <View style={styles.lineBreak}>  
                                    <Text style={styles.paragraphText}>The day was Thursday, 2 April 1998. It was a solemn evening meeting in the Assembly Hall of Dansol Tutorial School, Wempco Road, Ogba, Lagos, with the kind permission of Mrs Adunola Akinyemiju, Director of the school. And it was the inaugural meeting of the Christ LivingSpring Apostolic Ministry (CLAM).</Text>
                                </View>
                                <View style={styles.lineBreak}>  
                                    <Text style={styles.paragraphText}>Mrs Bukola Oladiyun (the founder’s wife) led the intercessory prayers after which the set man himself, Pastor Wole Oladiyun, mounted the pulpit to proclaim the vision of the commission after reading from John 4:14.</Text>
                                </View>
                                <View style={styles.lineBreak}>  
                                    <Text style={styles.paragraphText}>Attendance that day comprised seven adults and three children, among whom were Pastor Kola Oluwarinde, Pastor and Pastor (Mrs) Davies and Evangelist J. B. Ayo-Rotimi. From that small beginning in 1998, today CLAM records a minimum of 6,000-strong roll-call of worshippers in its flagship services.</Text>
                                </View>
                            </View>
                            <View style={styles.paragraph}>
                                <View style={styles.subHeadBox}>
                                    <Text style={styles.subHeadBoxText}>
                                        God’s Mandate for CLAM & Pastor Wole Oladiyun  (Psalms 132:13-18) 
                                    </Text>
                                </View>
                                <View style={styles.bulletPoint}>
                                    <Text style={[styles.bulletNumber, {fontSize: 20, justifyContent: 'flex-start'}]}>•</Text> 
                                    <Text style={[styles.bulletText]}>For the Lord has chosen CLAM, He has desired it His habitation </Text>
                                </View>
                                <View style={styles.bulletPoint}>
                                    <Text style={[styles.bulletNumber, {fontSize: 20, justifyContent: 'flex-start'}]}>•</Text> 
                                    <Text style={[styles.bulletText]}>CLAM is His rest for ever; here will God dwell for He has desired it </Text>
                                </View>
                                <View style={styles.bulletPoint}>
                                    <Text style={[styles.bulletNumber, {fontSize: 20, justifyContent: 'flex-start'}]}>•</Text> 
                                    <Text style={[styles.bulletText]}>God will abundantly bless her provision. He will satisfy her congregation with bread </Text>
                                </View>
                                <View style={styles.bulletPoint}>
                                    <Text style={[styles.bulletNumber, {fontSize: 20, justifyContent: 'flex-start'}]}>•</Text> 
                                    <Text style={[styles.bulletText]}>God will clothe all CLAM Priests with salvation and her saints shall shout aloud for joy </Text>
                                </View>
                                <View style={styles.bulletPoint}>
                                    <Text style={[styles.bulletNumber, {fontSize: 20, justifyContent: 'flex-start'}]}>•</Text> 
                                    <Text style={[styles.bulletText]}>God will make the horn of Pastor Wole Oladiyun to bud for He had ordained a lamp for him as his anointed </Text>
                                </View>
                                <View style={styles.bulletPoint}>
                                    <Text style={[styles.bulletNumber, {fontSize: 20, justifyContent: 'flex-start'}]}>•</Text> 
                                    <Text style={[styles.bulletText]}>All our enemies will God clothe with shame but upon CLAM and the entire congregation shall God’s crown FLOURISH in Jesus name </Text>
                                </View>                                        
                            </View>
                        </View>
                    </View> 
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    header: {
        backgroundColor: '#02223b',
        minHeight: 50,
        justifyContent: 'center'
    },
    headerText: {
        fontSize: 15,
        color: 'white',
        justifyContent: 'center',
    },
    BackButton: {
        marginRight: 10,
        marginLeft: 10
    },
    WebViewEmbed: {
        backgroundColor: "gray",
        flex: 1
    },
    content: {
        // paddingLeft: 10,
        // paddingRight: 10,
        // paddingTop: 10,
        // paddingBottom: 10
    },
    section: {
        backgroundColor: '#f7f7f7'
    },
    sectionTitle: {
        backgroundColor: '#a9e0ec',
        borderBottomColor: '#bbb',
        borderBottomWidth: 1,
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 15,
        paddingRight: 15
    },
    sectionTitleText: {
        fontSize: 20
    },
    sectionContent: {
        backgroundColor: '#f7f7f7',
        paddingBottom: 20,
        paddingTop: 10,
        paddingLeft: 15,
        paddingRight: 15
    },
    sectionContentText: {
        fontSize: 25,
    },
    bulletPoint: {
        flexDirection: 'row',
        marginBottom: 15
    },
    bulletNumber: {
        fontWeight: 'bold',
        color: '#c22727',
        // marginRight: 20
    },
    bulletText: {
        paddingLeft: 10,
        fontSize: 17,
        textAlign: 'justify'
    },
    paragraph: {
        marginTop: 10,
        marginBottom: 10
    },
    paragraphText: {
        fontSize: 17,
        lineHeight: 26
    },
    subHeadBox: {
        backgroundColor: '#c22727',
        padding: 10,
        marginBottom: 20,
        borderRadius: 5
    },
    subHead: {
        fontSize: 22
    },
    lineBreak: {
        marginBottom: 20
    },
    subHeadBoxText: {
        color: 'white',
        fontSize: 20
    }
});
