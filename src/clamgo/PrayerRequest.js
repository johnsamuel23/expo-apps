import React, { Component } from 'react';
import { 
    StyleSheet, 
    Text, 
    View, 
    ScrollView, 
    TouchableOpacity,
    WebView,
    TextInput,
    KeyboardAvoidingView
} from 'react-native';
import { 
    Entypo
} from '@expo/vector-icons';
import Row from './layouts/Row';
import FadedView from './components/FadedView';
import MultilineTextInput from './components/MultilineTextInput';

const Back = (<Entypo name="arrow-left" size={35} color='#fff' />)

export default class PrayerRequest extends Component {

    componentDidMount() {
        this.props.changeHeaderPanColor('#757575');
    }
    
    changeScreen(nextScreen) {
        this.props.navigator.pop({name: 'prayer_request_screen'});
    }

    changeText(field, value) {
        this.setState({
            [field]: value
        });
    }

    state = {
        name: "",
        email: "",
        message: ""
    }

    makeActive(field) {
        this.setState({
            activeInput: field
        });
    } 

    removeActive() {
        this.setState({
            activeInput: 'none'
        })
    }

    render() {

        var DEFAULT_URL = 'http://woleoladiyun.com/prayer-request/';
        var nameActive = {};
        var emailActive = {};
        var messageActive = {};
        var borderColor = '#116cc5';

        if(this.state.activeInput === 'name') {
            nameActive = {borderColor};
        } else if(this.state.activeInput === 'email') {
            emailActive = {borderColor};
        } else if(this.state.activeInput === 'message') {
            messageActive = {borderColor};
        }

        return (
            <FadedView style={styles.container}>
                <View style={styles.header}>
                    <Row style={{alignItems: 'center'}}>
                        <TouchableOpacity onPress={() => this.changeScreen('welcome_screen')} 
                            style={styles.BackButton}>
                            { Back }
                        </TouchableOpacity> 
                        <Text style={styles.headerText}>Prayer Request</Text>
                    </Row>
                </View>
                <View style={styles.container}> 
                     <View style={styles.WebViewEmbed}>
                        <WebView
                            automaticallyAdjustContentInsets={false}
                            source={{uri: DEFAULT_URL}}
                            javaScriptEnabled={true}
                            domStorageEnabled={true}
                            decelerationRate="normal"
                            startInLoadingState={true}
                        />
                    </View>
                </View>
            </FadedView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    header: {
        backgroundColor: '#02223b',
        minHeight: 50,
        justifyContent: 'center'
    },
    headerText: {
        fontSize: 15,
        color: 'white',
        justifyContent: 'center',
    },
    BackButton: {
        marginRight: 10,
        marginLeft: 10
    },
    input: {
        backgroundColor: 'transparent',
        padding: 10,
        borderColor: '#ccc',
        borderWidth: 1,
        marginBottom: 10
    },
    formView: {
        padding: 20,
    },
    buttonContainer: {
        backgroundColor: '#116cc5',
        width: 150,
        padding: 10,
    },
    buttonText: {
        color: '#fff',
        textAlign: 'center'
    },
    WebViewEmbed: {
        backgroundColor: "#ccc",
        flex: 1
    }
});
