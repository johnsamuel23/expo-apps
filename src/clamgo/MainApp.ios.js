import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, BackHandler, BackAndroid } from 'react-native';
import { Navigator } from 'react-native-deprecated-custom-components';
import WelcomeMenu from './WelcomeMenu';
import ChurchMap from './ChurchMap';
import Newsletter from './Newsletter';
import InternetRadio from './InternetRadio';
import Resources from './Resources';
import AboutChurch from './AboutChurch';
import IntroVideo from './IntroVideo';
import PrayerCenter from './PrayerCenter';
import PrayerSolution from './PrayerSolution';
import PrayerRequest from './PrayerRequest';
import Sermon from './Sermon';
import Blog from './Blog';
// import Donation from './Donation';
// import Shopping from './Shopping';
import Trustees from './Trustees';
import Branches from './Branches';
import Bible from './Bible';
import Ministry from './Ministry';
import TV from './TV';
import Ministries from './Ministries';
import StartMenu from './StartMenu';
import Events from './Events';
import Gallery from './Gallery';
import { Constants } from 'expo';
import { theme } from './theme';
// import StartMenu from './StartMenu';
// import { StackNavigator } from 'react-navigation';

export default class MainApp extends Component {
  state = {
    nextScreen: 'start_screen'
  }
  
  constructor(props) {
    super(props);

    this.navigator = null;
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', () => {
      if (this.navigator && this.navigator.getCurrentRoutes().length > 1) {
        this.navigator.pop();
        // BackHandler.exitApp();
        return true;
      }
      return false;
    });
  }

  changeScreen(nextScreen) {
    // Navigator.push({name: nextScreen});
  }

  setCurrentScreen(screen) {
    this.setState({
      currentScreen: screen
    })
  }

  changeHeaderPanColor(color, screen=null) {
    // this.setState({
    //   headerPanColor: color
    // });
  }

  selectLocation(center) {
      this.setState({
          selectedCenter: center
      })
  }

  renderScene(route, navigator) {
    this.navigator = navigator;
    
    switch(route.name) {
        case 'gallery_screen':
            currentScreen = <Gallery store={this.state} 
                                navigator={navigator}
                                changeHeaderPanColor={this.changeHeaderPanColor.bind(this)}
                            />
            break;
        case 'sermon_screen':
            currentScreen = <Sermon store={this.state} 
                                navigator={navigator}
                                changeHeaderPanColor={this.changeHeaderPanColor.bind(this)}
                            />
            break;
        case 'prayer_solution_screen':
            currentScreen = <PrayerSolution store={this.state} 
                                navigator={navigator}
                                changeHeaderPanColor={this.changeHeaderPanColor.bind(this)}
                            />
            break;
        case 'prayer_center_screen':
            currentScreen = <PrayerCenter store={this.state} 
                                navigator={navigator}
                                changeHeaderPanColor={this.changeHeaderPanColor.bind(this)}
                            />
            break;
        case 'prayer_request_screen':
            currentScreen = <PrayerRequest store={this.state} 
                                navigator={navigator}
                                changeHeaderPanColor={this.changeHeaderPanColor.bind(this)}
                            />
            break;
        case 'intro_video_screen':
            currentScreen = <IntroVideo store={this.state} 
                                navigator={navigator}
                                changeHeaderPanColor={this.changeHeaderPanColor.bind(this)}
                            />
            break;
        case 'welcome_screen':
            currentScreen = <WelcomeMenu store={this.state} 
                                navigator={navigator} 
                                changeHeaderPanColor={this.changeHeaderPanColor.bind(this)}
                            />
            break;
        case 'about_screen':
            currentScreen = <AboutChurch store={this.state} 
                                navigator={navigator}
                                changeHeaderPanColor={this.changeHeaderPanColor.bind(this)} 
                            />
            break;
        case 'map_screen':
            currentScreen = <ChurchMap store={this.state} 
                                navigator={navigator}
                                changeHeaderPanColor={this.changeHeaderPanColor.bind(this)}    
                            />
            break;
        case 'tv_screen':
            currentScreen = <TV store={this.state} 
                                navigator={navigator}
                                changeHeaderPanColor={this.changeHeaderPanColor.bind(this)}    
                            />
            break;
        case 'newsletter_screen':
            currentScreen = <Newsletter store={this.state} 
                                navigator={navigator}
                                changeHeaderPanColor={this.changeHeaderPanColor.bind(this)}
                            />
            break;
        case 'radio_screen':
            currentScreen = <InternetRadio store={this.state} 
                                navigator={navigator}
                                changeHeaderPanColor={this.changeHeaderPanColor.bind(this)}
                            />
            break;
        case 'blog_screen':
            currentScreen = <Blog store={this.state} 
                                navigator={navigator}
                                changeHeaderPanColor={this.changeHeaderPanColor.bind(this)}
                            />
            break;
        case 'donation_screen':
            currentScreen = <Donation store={this.state} 
                                navigator={navigator}
                                changeHeaderPanColor={this.changeHeaderPanColor.bind(this)}
                            />
            break;
        case 'trustees_screen':
            currentScreen = <Trustees store={this.state} 
                                navigator={navigator}
                                changeHeaderPanColor={this.changeHeaderPanColor.bind(this)}
                            />
            break;
         case 'branches_screen':
            currentScreen = <Branches store={this.state} 
                                navigator={navigator}
                                selectLocation={this.selectLocation.bind(this)}
                                changeHeaderPanColor={this.changeHeaderPanColor.bind(this)}
                            />
            break;
        case 'shopping_screen':
            currentScreen = <Shopping store={this.state} 
                                navigator={navigator}
                                changeHeaderPanColor={this.changeHeaderPanColor.bind(this)}
                            />
            break;
        case 'bible_screen':
            currentScreen = <Bible store={this.state} 
                                navigator={navigator}
                                changeHeaderPanColor={this.changeHeaderPanColor.bind(this)}
                            />
            break;
        case 'ministry_screen':
            currentScreen = <Ministry store={this.state} 
                                navigator={navigator}
                                changeHeaderPanColor={this.changeHeaderPanColor.bind(this)}
                            />
            break;
        case 'ministries_screen':
            currentScreen = <Ministries store={this.state} 
                                navigator={navigator}
                                changeHeaderPanColor={this.changeHeaderPanColor.bind(this)}
                            />
            break;
        case 'events_screen':
            currentScreen = <Events store={this.state} 
                                navigator={navigator}
                                changeHeaderPanColor={this.changeHeaderPanColor.bind(this)}
                            />
            break;
        case 'start_screen':
            currentScreen = <StartMenu store={this.state} 
                                navigator={navigator}
                                changeHeaderPanColor={this.changeHeaderPanColor.bind(this)}
                            />
            break;
        case 'resources_screen':
            currentScreen = <Resources store={this.state} 
                                navigator={navigator}
                                changeHeaderPanColor={this.changeHeaderPanColor.bind(this)}
                            />
            break;
        default: 
            currentScreen =  <WelcomeMenu store={this.state} 
                            navigator={navigator} />;
    }
    return currentScreen;   
  }

  render() {
    let moreStyle = {};
    if(this.state.headerPanColor) {
        moreStyle.backgroundColor = this.state.headerPanColor;
    }
    
    return (
      <View style={[styles.container]}>
        <View style={[styles.header, moreStyle]}>
        </View>
        <View style={styles.container}> 
            <Navigator
                initialRoute={{name: this.state.nextScreen}}
                renderScene={this.renderScene.bind(this)}
                configureScene={(route, routeStack) => Navigator.SceneConfigs.FloatFromRight}
            />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    header: {
        backgroundColor: '#000',
        minHeight: 25
    },
    headerText: {
        fontSize: 20
    }
});
