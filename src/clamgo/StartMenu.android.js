import React, { Component } from 'react';
import { 
    StyleSheet, 
    Text, 
    View, 
    ScrollView, 
    TouchableOpacity,
    TouchableHighlight,
    Dimensions,
    BackHandler
} from 'react-native';
import LocalImage from './components/LocalImage';
import TouchImage from './components/TouchImage';
import SocialMedia from './components/SocialMediaBox';
import { theme } from './theme';
import ContentWrapper from './components/ContentWrapper';

export default class StartMenu2 extends Component {

    componentDidMount() {
        this.props.changeHeaderPanColor('#000');
    }

    changeScreen(nextScreen) {
        this.props.navigator.push({name: nextScreen});
    }

    state = {
        hasGutter: false
    }

    render() {

        let dynamicStyles = {};
        dynamicStyles.menuContainer = {};
        dynamicStyles.mast = {};
        dynamicStyles.container = {};
        dynamicStyles.videoBox = {};
        dynamicStyles.menuContainer.sidePadding = 0;
        dynamicStyles.menuContainer.marginBottom = 0;
        // dynamicStyles.videoBox.marginBottom = 5;
        dynamicStyles.videoBox.borderBottomWidth = 5;
        dynamicStyles.videoBox.borderBottomColor = "#fff";
        let gutter = 0;
        let imageGutter = 0;
        
        
        if(this.state.hasGutter) {
            dynamicStyles.menuContainer.sidePadding = 10;
            dynamicStyles.menuContainer.marginBottom = 10;

            gutter = dynamicStyles.menuContainer.sidePadding;
            imageGutter = gutter + 20;
            dynamicStyles.container.backgroundColor = '#000';
            dynamicStyles.mast.borderBottomColor = '#000';

            dynamicStyles.videoBox.marginTop = gutter - 3;
            dynamicStyles.videoBox.marginBottom = gutter - 3;
            // dynamicStyles.videoBox.borderBottomWidth = gutter - 3;
            // dynamicStyles.videoBox.borderBottomColor = "#fff";
        }

        let bgGuideWidth = theme.contentWidth - imageGutter;
        
        return (
            <ContentWrapper>
                <View style={[styles.container, dynamicStyles.container]}>
                    <View style={[styles.mast, dynamicStyles.mast]}>
                        <LocalImage source={require('./img/welcomescreen_slize_0101.jpg')} 
                            originalWidth="1611"
                            originalHeight="524"
                            guideWidth={bgGuideWidth}
                        />
                    </View>
                    <View style={[styles.menuContainer, 
                        {   paddingLeft: dynamicStyles.menuContainer.sidePadding, 
                            paddingRight: dynamicStyles.menuContainer.sidePadding,
                            marginBottom: dynamicStyles.menuContainer.marginBottom,
                        }]}>
                        <ScrollView showsVerticalScrollIndicator={false}>           
                            <View style={{marginTop: 0, marginBottom: 0}}>
                                <TouchImage source={require('./img/welcomescreenSLIZE_03.jpg')} 
                                    originalWidth="1611"
                                    originalHeight="320"
                                    changeScreen={() => this.changeScreen('intro_video_screen')}
                                    guideWidth={bgGuideWidth}
                                    gutter={this.state.hasGutter}
                                    style={dynamicStyles.videoBox}
                                />
                            </View>
                            <View>
                                <TouchImage source={require('./img/welcomescreenSLIZE_05.jpg')} 
                                    originalWidth="1611"
                                    originalHeight="478"
                                    changeScreen={() => this.changeScreen('welcome_screen')}
                                    guideWidth={bgGuideWidth}
                                    gutter={this.state.hasGutter}
                                />
                            </View>
                            <TouchImage source={require('./img/welcomescreenSLIZE_07.jpg')} 
                                originalWidth="1611"
                                originalHeight="424"
                                guideWidth={bgGuideWidth}
                                gutter={this.state.hasGutter}
                                changeScreen={() => this.changeScreen('events_screen')}
                            />
                            <TouchImage source={require('./img/welcomescreenSLIZE_09.jpg')} 
                                originalWidth="1611"
                                originalHeight="425"
                                guideWidth={bgGuideWidth}
                                gutter={this.state.hasGutter}
                                changeScreen={() => this.changeScreen('about_screen')}
                            />
                            <TouchImage source={require('./img/welcomescreenSLIZE_11.jpg')} 
                                originalWidth="1611"
                                originalHeight="557"
                                guideWidth={bgGuideWidth}
                                gutter={this.state.hasGutter}
                                changeScreen={() => this.changeScreen('bible_screen')}
                            />
                            <TouchImage source={require('./img/welcomescreenSLIZE_01_14_2.jpg')} 
                                originalWidth="1611"
                                originalHeight="517"
                                guideWidth={bgGuideWidth}
                                gutter={this.state.hasGutter}
                                changeScreen={() => this.changeScreen('shopping_screen')}
                            /> 
                            <SocialMedia navigator={this.props.navigator}
                                gutter={this.state.hasGutter}
                                gutterSize={gutter}
                            /> 
                            <TouchImage source={require('./img/welcomescreenSLIZE_18.jpg')} 
                                originalWidth="1611"
                                originalHeight="461"
                                guideWidth={bgGuideWidth}
                                gutter={this.state.hasGutter}
                                changeScreen={() => this.changeScreen('branches_screen')}
                            />
                        </ScrollView>
                    </View>
                </View>
            </ContentWrapper>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    mast: {
        borderBottomColor: '#fff',
        borderBottomWidth: 5
    },
    menuContainer: {
        flex: 1
    }
});
