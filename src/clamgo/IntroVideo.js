import React from 'react';
import { 
    StyleSheet, 
    Text, 
    View, 
    ScrollView, 
    TouchableOpacity,
    WebView
} from 'react-native';
import { 
    Entypo
} from '@expo/vector-icons';
import Row from './layouts/Row';
import FadedView from './components/FadedView';

const Back = (<Entypo name="arrow-left" size={35} color='#fff' />)

export default class IntroVideo extends React.Component {

    componentDidMount() {
        this.props.changeHeaderPanColor('#757575');
    }
    
    changeScreen(nextScreen) {
        this.props.navigator.pop({name: 'bible_screen'});
    }

    render() {

        var DEFAULT_URL = 'https://www.youtube.com/embed/zXge5iCy1uw?rel=0&autoplay=0&controls=0&modestbranding=0';

        return (
            <FadedView style={styles.container}>
                 <View style={styles.header}>
                    <Row style={{alignItems: 'center'}}>
                        <TouchableOpacity onPress={() => this.changeScreen('welcome_screen')} 
                            style={styles.BackButton}>
                            { Back }
                        </TouchableOpacity> 
                        <Text style={styles.headerText}>Back</Text>
                    </Row>
                </View> 
                <View style={styles.container}> 
                    <View style={styles.WebViewEmbed}>
                        <WebView
                            automaticallyAdjustContentInsets={false}
                            source={{uri: DEFAULT_URL}}
                            javaScriptEnabled={true}
                            domStorageEnabled={true}
                            decelerationRate="normal"
                            startInLoadingState={true}
                        />
                    </View>
                </View>
            </FadedView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#000',
    },
    header: {
        backgroundColor: '#000',
        minHeight: 50,
        justifyContent: 'center'
    },
    headerText: {
        fontSize: 15,
        color: 'white',
        justifyContent: 'center',
    },
    BackButton: {
        marginRight: 10,
        marginLeft: 10
    },
    WebViewEmbed: {
        backgroundColor: "#ccc",
        flex: 1
    }
});
