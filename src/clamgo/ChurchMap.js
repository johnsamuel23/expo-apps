import React from 'react';
import { 
    StyleSheet, 
    Text, 
    View, 
    ScrollView, 
    TouchableOpacity,
    WebView
} from 'react-native';
import { 
    Entypo
} from '@expo/vector-icons';
import Row from './layouts/Row';
import FadedView from './components/FadedView';

const Back = (<Entypo name="arrow-left" size={35} color='#fff' />)

export default class ChurchMap extends React.Component {

    componentDidMount() {
        this.props.changeHeaderPanColor('#757575');
    }
    
    changeScreen(nextScreen) {
        this.props.navigator.pop({name: 'map_screen'});
    }

    render() {

        var DEFAULT_URL = "http://maps.google.com/?q=christLivingSpringMinistry";
        let { selectedCenter } = this.props.store;
        if(selectedCenter !== undefined) {
            if(selectedCenter === 'lekki_map_screen') {
                DEFAULT_URL = 'https://www.google.co.in/maps/place/The+Eventistry,+Lekki/@6.450481,3.482336,16.5z/data=!4m12!1m6!3m5!1s0x103bf4382c34b91d:0x92d7d0c830a6c673!2sThe+Eventistry,+Lekki!8m2!3d6.4497119!4d3.4821899!3m4!1s0x103bf4382c34b91d:0x92d7d0c830a6c673!8m2!3d6.4497119!4d3.4821899';
            } else if(selectedCenter === 'abuja_map_screen') {
                DEFAULT_URL = 'https://www.google.com.ng/maps/place/Federal+Govt.+Boys+College/@9.0044288,7.4734998,20z/data=!4m13!1m7!3m6!1s0x104e0c8daca0fa93:0x9c71211285823c49!2sIbrahim+Waziri+Crescent+Apo+Abuja+Rd,+Gudu,+Abuja!3b1!8m2!3d9.0075133!4d7.4733895!3m4!1s0x0:0x66d7fbf1df7ad4eb!8m2!3d9.0043939!4d7.4736375?hl=en';
            } else {
                DEFAULT_URL = "http://maps.google.com/?q=christLivingSpringMinistry";
            }
        } 

        return (
            <FadedView style={styles.container}>
                <View style={styles.header}>
                    <Row style={{alignItems: 'center'}}>
                        <TouchableOpacity onPress={() => this.changeScreen('welcome_screen')} 
                            style={styles.BackButton}>
                            { Back }
                        </TouchableOpacity> 
                        <Text style={styles.headerText}>Get Church Direction</Text>
                    </Row>
                </View>
                <View style={styles.container}> 

                    <View style={styles.WebViewEmbed}>
                        <WebView
                            automaticallyAdjustContentInsets={false}
                            source={{uri: DEFAULT_URL}}
                            javaScriptEnabled={true}
                            domStorageEnabled={true}
                            decelerationRate="normal"
                            startInLoadingState={true}
                        />
                    </View>
                </View>
            </FadedView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    header: {
        backgroundColor: '#02223b',
        minHeight: 50,
        justifyContent: 'center'
    },
    headerText: {
        fontSize: 15,
        color: 'white',
        justifyContent: 'center',
    },
    BackButton: {
        marginRight: 10,
        marginLeft: 10
    },
    WebViewEmbed: {
        backgroundColor: "#ccc",
        flex: 1
    }
});
