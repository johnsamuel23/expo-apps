import React from 'react';
import { 
    StyleSheet, 
    Text, 
    View, 
    ScrollView, 
    TouchableOpacity,
    Dimensions
} from 'react-native';
import { 
    Entypo
} from '@expo/vector-icons';
import Row from './layouts/Row';
import FadedView from './components/FadedView';
import LocalImage from './components/LocalImage';
import { theme } from './theme';
import ContentWrapper from './components/ContentWrapper';

const Back = (<Entypo name="arrow-left" size={35} color='#fff' />)

export default class Branches extends React.Component {

    componentDidMount() {
        this.props.changeHeaderPanColor('#757575');
    }

    changeMapScreen(center) {
        this.props.selectLocation(center);
        this.props.navigator.push({name: 'map_screen'});
    }
    
    goBack(nextScreen) {
        this.props.navigator.pop({name: 'branches_screen'});
    }

    render() {

        var DEFAULT_URL = 'http://clamgo.org/our-trustees/';

        return (
            <FadedView style={styles.container}>
                <View style={styles.header}>
                    <Row style={{alignItems: 'center'}}>
                        <TouchableOpacity onPress={() => this.goBack('welcome_screen')} 
                            style={styles.BackButton}>
                            { Back }
                        </TouchableOpacity> 
                        <Text style={styles.headerText}>CLAM Branches</Text>
                    </Row>
                </View>
                <View style={styles.container}> 
                    <ContentWrapper>
                        <ScrollView>
                            <View style={styles.entry}>
                                <Text style={styles.entryHeadText}>CLAM LEKKI Prayer Center</Text>
                                <LocalImage source={require('./img/clam_lekki1.jpg')} 
                                    originalWidth="800"
                                    originalHeight="533"
                                    guideWidth={theme.contentWidth}
                                />
                                <View style={styles.centerInfo}>
                                    <View style={styles.infoBlock}>
                                        <Text style={styles.infoText}><Text style={styles.infoTextLabel}>Address:</Text> The Eventistry (After the Dome), Freedom Road (by the 3rd roundabout), Lekki, Lagos, Nigeria. </Text>
                                    </View>
                                    <View style={styles.infoBlock}>
                                        <Text style={styles.infoText}><Text style={styles.infoTextLabel}>Email:</Text> clamprayercentrelekki@clamgo.org </Text>
                                    </View>
                                    <View style={styles.infoBlock}>
                                        <Text style={styles.infoText}><Text style={styles.infoTextLabel}>Facebook:</Text> www.facebook.com/clamlekki</Text>
                                    </View>
                                    <View style={styles.infoBlock}>
                                        <Text style={styles.infoText}><Text style={styles.infoTextLabel}>Phone:</Text> +234-803-3487-830 </Text>
                                    </View>
                                    <TouchableOpacity 
                                        onPress={() => this.changeMapScreen('lekki_map_screen')}
                                    >
                                        <View style={styles.button}>
                                            <Text style={styles.buttonText}> Locate Lekki Prayer Center</Text>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <View style={styles.entry}>
                                <Text style={styles.entryHeadText}>CLAM ABUJA Prayer Center</Text>
                                <LocalImage source={require('./img/clam_abuja1.jpg')} 
                                    originalWidth="800"
                                    originalHeight="533"
                                    guideWidth={theme.contentWidth}
                                />
                                <View style={styles.centerInfo}>
                                    <View style={styles.infoBlock}>
                                        <Text style={styles.infoText}><Text style={styles.infoTextLabel}>Address:</Text> Plot 948, Wasiri Ibrahim Crescent, FEDERAL GOVERNMENT BOYS COLLEGE</Text>
                                    </View>
                                    <View style={styles.infoBlock}>
                                        <Text style={styles.infoText}><Text style={styles.infoTextLabel}>Email:</Text> abujaclamprayercentre@clamgo.org </Text>
                                    </View>
                                    <View style={styles.infoBlock}>
                                        <Text style={styles.infoText}><Text style={styles.infoTextLabel}>Facebook:</Text> www.facebook.com/CLAMABUJA</Text>
                                    </View>
                                    <View style={styles.infoBlock}>
                                        <Text style={styles.infoText}><Text style={styles.infoTextLabel}>Phone:</Text> +234-703-0114-91 </Text>
                                    </View>
                                    <TouchableOpacity 
                                        onPress={() => this.changeMapScreen('abuja_map_screen')}
                                    >
                                        <View style={styles.button}>
                                            <Text style={styles.buttonText}> Locate Abuja Prayer Center</Text>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </ScrollView>
                    </ContentWrapper>
                </View>
            </FadedView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // backgroundColor: '#fff',
    },
    header: {
        backgroundColor: '#02223b',
        minHeight: 50,
        justifyContent: 'center'
    },
    headerText: {
        fontSize: 15,
        color: 'white',
        justifyContent: 'center',
    },
    BackButton: {
        marginRight: 10,
        marginLeft: 10
    },
    WebViewEmbed: {
        backgroundColor: "#ccc",
        flex: 1
    },
    entry: {
        marginBottom: 10
    },
    entryHeadText: {
        fontSize: 20,
        padding: 20
    },
    centerInfo: {
        backgroundColor: '#f7f7f7',
        borderBottomWidth: 2,
        borderBottomColor: '#eee',
        padding: 20
    },
    infoBlock: {
        marginBottom: 5,
    },
    infoText: {
        fontSize: 15
    },
    infoTextLabel: {
        color: '#c22727'
    },
    button: {
        backgroundColor: '#c22727',
        padding: 10,
        borderRadius: 5,
        marginTop: 10
    },
    buttonText: {
        textAlign: 'center',
        fontSize: 20,
        color: 'white'
    },
});
