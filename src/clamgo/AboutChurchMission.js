import React, { Component } from 'react';
import { 
    StyleSheet, 
    Text, 
    View, 
    ScrollView, 
    TouchableOpacity,
    WebView
} from 'react-native';
import { 
    Entypo
} from '@expo/vector-icons';
import Row from './layouts/Row';
import FadedView from './components/FadedView';
import HorizontalTab from './components/HorizontalTab';

const Back = (<Entypo name="arrow-left" size={35} color='#fff' />)

export default class AboutChurchMission extends Component {

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.section}>
                    <View style={styles.sectionTitle}>
                        <Text style={styles.sectionTitleText}>Our Mission</Text> 
                    </View>
                    <View style={styles.sectionContent}>
                        <Text style={styles.sectionContentText}>Soul Winning through Crusades and Word-based Apostolic Prayers. 
                        </Text>
                    </View>
                </View>
                <View style={styles.section}>
                    <View style={styles.sectionTitle}>
                        <Text style={styles.sectionTitleText}>Our purpose in ministry, therefore, is as defined by the following objectives:</Text> 
                    </View>
                    <View style={styles.sectionContent}>
                        <View style={styles.bulletPoint}>
                            <Text style={styles.bulletNumber}>1.</Text>	
                            <Text style={styles.bulletText}>To propagate the Gospel of our Lord Jesus Christ across the globe </Text>
                        </View>
                        <View style={styles.bulletPoint}>
                            <Text style={styles.bulletNumber}>2.</Text>	
                            <Text style={styles.bulletText}>To promote the revival of apostolic signs, wonders and miracles as of old </Text>
                        </View>
                        <View style={styles.bulletPoint}>
                            <Text style={styles.bulletNumber}>3.</Text>	
                            <Text style={styles.bulletText}>To win a rich harvest of souls for Christ through aggressive crusades and other evangelistic outreaches </Text>
                        </View>
                        <View style={styles.bulletPoint}>
                            <Text style={styles.bulletNumber}>4.</Text>	
                            <Text style={styles.bulletText}>To train believers in the “acts of the apostles,” building and equipping them in the art of spiritual warfare </Text>
                        </View>
                        <View style={styles.bulletPoint}>
                            <Text style={styles.bulletNumber}>5.</Text>	
                            <Text style={styles.bulletText}>To catch the youth for Christ and teach them of Christ to be world changers </Text>
                        </View>
                        <View style={styles.bulletPoint}>
                            <Text style={styles.bulletNumber}>6.</Text>	
                            <Text style={styles.bulletText}>To give succour to the less-privileged, widows, orphans and the aged through the establishment of maternity homes, hospitals, hospices and care-giving facilities </Text>
                        </View>
                        <View style={styles.bulletPoint}>
                            <Text style={styles.bulletNumber}>7.</Text>	
                            <Text style={styles.bulletText}>To enforce the name of Jesus globally for land redemption. </Text>
                        </View>
                    </View>
                </View>
            </View>
            
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    header: {
        backgroundColor: '#02223b',
        minHeight: 50,
        justifyContent: 'center'
    },
    headerText: {
        fontSize: 15,
        color: 'white',
        justifyContent: 'center',
    },
    BackButton: {
        marginRight: 10,
        marginLeft: 10
    },
    WebViewEmbed: {
        backgroundColor: "gray",
        flex: 1
    },
    content: {
        // paddingLeft: 10,
        // paddingRight: 10,
        // paddingTop: 10,
        // paddingBottom: 10
    },
    section: {
        backgroundColor: '#f7f7f7'
    },
    sectionTitle: {
        // backgroundColor: '#a9e0ec',
        // borderBottomColor: '#bbb',
        // borderBottomWidth: 1,
        paddingTop: 10,
        // paddingBottom: 10,
        paddingLeft: 15,
        paddingRight: 15
    },
    sectionTitleText: {
        fontSize: 20,
        fontWeight: 'bold',
        color: '#02223b'
    },
    sectionContent: {
        backgroundColor: '#f7f7f7',
        paddingBottom: 20,
        paddingTop: 10,
        paddingLeft: 15,
        paddingRight: 15
    },
    sectionContentText: {
        fontSize: 20,
    },
    bulletPoint: {
        flexDirection: 'row',
        marginBottom: 15
    },
    bulletNumber: {
        fontWeight: 'bold',
        color: '#c22727',
        fontSize: 17
    },
    bulletText: {
        paddingLeft: 10,
        fontSize: 17,
        textAlign: 'justify'
    },
    paragraph: {
        marginTop: 10,
        marginBottom: 10
    },
    paragraphText: {
        fontSize: 17
    },
    subHeadBox: {
        backgroundColor: '#c22727',
        padding: 10,
        marginBottom: 20,
        borderRadius: 5

    },
    subHeadBoxText: {
        color: 'white',
        fontSize: 20
    }
});
