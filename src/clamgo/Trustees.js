import React from 'react';
import { 
    StyleSheet, 
    Text, 
    View, 
    ScrollView, 
    TouchableOpacity,
    WebView,
    Dimensions
} from 'react-native';
import { 
    Entypo
} from '@expo/vector-icons';
import Row from './layouts/Row';
import FadedView from './components/FadedView';
import LocalImage from './components/LocalImage';
import TrusteeEntry from './components/TrusteeEntry';
import { theme } from './theme';
import ContentWrapper from './components/ContentWrapper';

const Back = (<Entypo name="arrow-left" size={35} color='#fff' />)

const options = {
    imagePadding: 40
};

export default class Trustees extends React.Component {

    componentDidMount() {
        this.props.changeHeaderPanColor('#757575');
    }
    
    goBack(nextScreen) {
        this.props.navigator.pop({name: 'trustees_screen'});
    }

    state = {
        trustees: [
            {
                name: 'PASTOR WOLE OLADIYUN',
                designation: 'PRESIDENT & SENIOR PASTOR',
                postInfo: 'Pastor Wole Oladiyun, the Senior Pastor and set man of Christ Livingspring Apostolic Ministry',
                image: {
                    source: require('./img/management/Pastor-Wole.jpg'),
                    originalWidth: '211',
                    originalHeight: '169',
                    guideWidth: theme.contentWidth - options.imagePadding
                }
            },
            {
                name: 'PASTOR (MRS) BUKOLA OLADIYUN',
                designation: 'DIRECTOR FOR FAMILY LIFE',
                postInfo: 'She is the Visionary of Voice of Joy and Gladness Women International',
                image: {
                    source: require('./img/management/pastor-Bukola.jpg'),
                    originalWidth: '211',
                    originalHeight: '169',
                    guideWidth: theme.contentWidth - options.imagePadding
                }
            },
            {
                name: 'PASTOR AKIN DANIA',
                designation: 'MEMBER',
                postInfo: 'Pastor Akin Dania, a graduate of the University of Ibadan is an experienced Administrator and Human Resources Manager, with over 30years experience in both the Public and Private sector of the Nigerian economy.',
                image: {
                    source: require('./img/management/Pastor-Akin-Daina.png'),
                    originalWidth: '211',
                    originalHeight: '169',
                    guideWidth: theme.contentWidth - options.imagePadding
                }
            },
            {
                name: 'PASTOR VICTOR-UKO-ABASI',
                designation: 'MEMBER',
                postInfo: 'Pastor Victor Uko-Abasi is the Senior Pastor, Sure Foundation Apostolic Christian Ministry (SFAM), Ikot Ekpene.',
                image: {
                    source: require('./img/management/Pastor-Victor-Uko-Abasi.jpg'),
                    originalWidth: '211',
                    originalHeight: '169',
                    guideWidth: theme.contentWidth - options.imagePadding
                }
            },
            {
                name: 'LARA COKER',
                designation: 'CO-ORDINATOR, LEGAL SERVICES',
                postInfo: 'She is a Trustee of Christ LivingSpring Apostolic Ministry and she is also its Coordinator, Legal Services',
                image: {
                    source: require('./img/management/Lara-Coker.png'),
                    originalWidth: '211',
                    originalHeight: '169',
                    guideWidth: theme.contentWidth - options.imagePadding
                }
            },
            {
                name: 'EVANGELIST EMEKA UKAEGBU',
                designation: 'MEMBER',
                postInfo: 'He is a prolific teacher of the word. He has ministered in various churches within and outside Nigeria.',
                image: {
                    source: require('./img/management/evang_emeka.jpg'),
                    originalWidth: '211',
                    originalHeight: '211',
                    guideWidth: theme.contentWidth - options.imagePadding
                }
            }
        ]
    }

    render() {

        const Entries = this.state.trustees.map((item, index)=> {
            return <TrusteeEntry key={index} data={item} />
        });

        return (
            <FadedView style={styles.container}>
                    <View style={styles.header}>
                        <Row style={{alignItems: 'center'}}>
                            <TouchableOpacity onPress={() => this.goBack('welcome_screen')} 
                                style={styles.BackButton}>
                                { Back }
                            </TouchableOpacity> 
                            <Text style={styles.headerText}>Trustees</Text>
                        </Row>
                    </View>
                    <View style={[styles.container, {marginTop: theme.contentMarginTop}]}> 
                        <ContentWrapper>
                            <ScrollView>
                                <View style={styles.heading}>
                                    <Text style={styles.headingText}>Our Trustees</Text>
                                </View>
                                <View style={styles.content}>

                                    { Entries }

                                </View>
                            </ScrollView>
                        </ContentWrapper>
                    </View>
            </FadedView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    header: {
        backgroundColor: '#02223b',
        minHeight: 50,
        justifyContent: 'center'
    },
    headerText: {
        fontSize: 15,
        color: 'white',
        justifyContent: 'center',
    },
    BackButton: {
        marginRight: 10,
        marginLeft: 10
    },
    WebViewEmbed: {
        backgroundColor: "#ccc",
        flex: 1
    },
    heading: {
        padding: 20,
        borderBottomColor: '#ccc',
        borderBottomWidth: 1
    },
    headingText: {
        fontSize: 25
    },
    content: {
        marginBottom: 30
    }
});
