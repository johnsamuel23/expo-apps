import React from 'react';
import { StyleSheet, Text, View, ScrollView } from 'react-native';
import {
  Asset,
  AppLoading,
} from 'expo';
import App from './MainApp';
import SplashScreen from './SplashScreen';

export default class Root extends React.Component {
  state = {
    isReady: false,
  };

  componentWillMount() {
    this._cacheResourcesAsync();
  }

  async _cacheResourcesAsync() {
    // _this = this;
    // setTimeout(function(){
    //   _this.setState({isReady: true});
    // }, 5000);

    const images = [
      require('./img/welcomescreenSLIZE_01.jpg'),
      require('./img/welcomescreenSLIZE_03.jpg'),
      require('./img/welcomescreenSLIZE_05.jpg'),
      require('./img/welcomescreenSLIZE_07.jpg'),
      require('./img/welcomescreen_slize_0101.jpg'),      
      require('./img/welcomescreenSLIZE_09.jpg'),
      require('./img/welcomescreenSLIZE_11.jpg'),
      require('./img/welcomescreenSLIZE_12.jpg'),
      require('./img/welcomescreenSLIZE_01_14.jpg'),
      require('./img/welcomescreenSLIZE_18.jpg'),
    ];

    for (let image of images) {
      await Asset.fromModule(image).downloadAsync();
    }

    // this.setState({isReady: true});
  }

  render() {

    // if (!this.state.isReady) {
    //   return <SplashScreen />;
    // }

    return (
      <View style={[styles.container]}>
        <View style={styles.container}>
            <App />  
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
});
