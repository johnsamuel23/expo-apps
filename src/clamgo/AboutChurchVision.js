import React, { Component } from 'react';
import { 
    StyleSheet, 
    Text, 
    View, 
    ScrollView, 
    TouchableOpacity,
    WebView
} from 'react-native';
import { 
    Entypo
} from '@expo/vector-icons';
import Row from './layouts/Row';
import FadedView from './components/FadedView';
import HorizontalTab from './components/HorizontalTab';

const Back = (<Entypo name="arrow-left" size={35} color='#fff' />);

export default class AboutChurchVision extends Component {

    render() {
        return (
            <View style={styles.container}><View style={styles.sectionContent}><Text style={styles.sectionContentText}>Apostolic Revival for Global Soul Harvesting.</Text></View></View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        paddingTop: 20,
        justifyContent: 'center',
        alignItems: 'center'
    },
    sectionTitle: {
        backgroundColor: '#a9e0ec',
        borderBottomColor: '#bbb',
        borderBottomWidth: 1,
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 15,
        paddingRight: 15
    },
    sectionTitleText: {
        fontSize: 30
    },
    sectionContent: {
        // backgroundColor: '#f7f7f7',
        paddingBottom: 25,
        paddingTop: 25,
        paddingLeft: 25,
        paddingRight: 25,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    sectionContentText: {
        fontSize: 25,
        textAlign: 'center'
    },
});
