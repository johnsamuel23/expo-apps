import { Dimensions } from 'react-native';

let windowWidth = Dimensions.get('window').width;
let screenPadding = 10;
let contentWidth = windowWidth;
let readingContentMarginTop = 0;
let contentMarginTop = 0;

if(windowWidth > 600) {
    contentWidth = 500;
    readingContentMarginTop = 20;
} 

if(windowWidth > 800) {
    contentWidth = 700;
    contentMarginTop = 20;
}


export const theme = {
    contentWidth,
    contentMarginTop,
}