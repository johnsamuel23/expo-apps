import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, Image, TextInput, Dimensions, Platform, KeyboardAvoidingView, TouchableOpacity, Animated, Easing,} from 'react-native';
import ResponsiveImage from 'react-native-responsive-image';

const deviceWidth = Dimensions.get("window").width;

export default class CartProduct extends Component {

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.productContainer}>
                    <View style={styles.productImage}> 
                    <View style={styles.productImage}>
                        <ResponsiveImage
                            style={{
                                width: deviceWidth,
                                height: 100
                            }}
                            source={this.props.product_image}
                            initWidth={deviceWidth} 
                            initHeight={100} />
                        </View>
                    </View>

                    <View style={styles.productInfo}>
                        <View style={styles.productInfoLabel}>
                            <Text style={[styles.ProductInfoLabelText]}> Product: </Text>
                            <Text style={[styles.ProductInfoValueText]}> {this.props.product_name} </Text>
                        </View>
                        <View style={styles.productInfoLabel}>
                            <Text style={[styles.ProductInfoLabelText]}> Quantity: </Text>
                            <Text style={[styles.ProductInfoValueText]}> {this.props.quantity} </Text>
                        </View>
                        <View style={styles.productInfoLabel}>
                            <Text style={[styles.ProductInfoLabelText]}> Total (Naira): </Text>
                            <Text style={[styles.ProductInfoValueText]}> {this.props.total_cost} </Text>
                        </View>
                        
                    </View>
                </View>
            </View>

        );
    } 
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#347c3e',
        marginBottom: 1,
        marginTop: 10,
        ...Platform.select({
            ios: {
              shadowColor: 'rgba(0,0,0, .7)',
              shadowOffset: { height:0, width: 0 },
              shadowOpacity: 1,
              shadowRadius: 3,
            },
            android: {
              elevation: 3
            }
        }),
    },
    productInfo: {
        backgroundColor: 'ghostwhite',
        padding: 15
    },
    productInfoLabel: {
        flexDirection: 'row',
        paddingBottom: 10,

    },
    ProductInfoLabelText: {
        color: 'green',
        flex: 1
    },
    ProductInfoValueText: {
        flex: 1,
        color: '#333'
    },
     
});
  