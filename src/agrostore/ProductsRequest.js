import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, Image, TextInput, Dimensions, KeyboardAvoidingView, TouchableOpacity, Animated, Easing,} from 'react-native';
import LocalImage from './components/LocalImage';
import ProductRequest from './ProductRequest';


export default class ProductsRequest extends Component {

  componentWillMount() {
        this.state = {
            productsrequest: [ 
                {
                    id: 1,
                    product_name: '',
                    quantity_available: '' ,
                },
                {
                    id: 2,
                    product_name: '',
                    quantity_available: '' ,
                },
                {
                    id: 3,
                    product_name: '',
                    quantity_available: '' ,
                },
                {
                    id: 4,
                    product_name: '',
                    quantity_available: '' ,
                },  
            ]
        }
}    

 render() {

     const productsrequest = this.state.productsrequest.map((item, index) => {
            return <ProductRequest
                    key={item.id}
                    product_name={item.product_name}
                    quantity={item.quantity_available}
                    />
    });
      
        return (
            <ScrollView style={styles.container}>
                { productsrequest }     
            </ScrollView>
        );     
    }
}            

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#347c3e',
        padding: 15
    },
})
    
 