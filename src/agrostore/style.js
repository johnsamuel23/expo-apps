import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#16794a',
    // justifyContent: 'center',
    // alignItems: 'center'
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'center',
    flex: 2,
    paddingTop: 20,
    alignItems: 'center'
  },
  body: {
    flex: 4,
    paddingLeft: 30,
    paddingRight: 30,
    paddingBottom: 20,
    justifyContent: 'flex-end'
  },  
  HeaderText: {
    fontSize: 30,
    fontWeight: 'bold',
    color: '#fff'
  },
  SignupText: {
    fontSize: 20,
    color: '#fff',
    marginBottom: 20
  },
  input: {
    backgroundColor: 'white',
    padding: 10,
    marginBottom: 10,
    fontSize: 15,
    borderRadius: 3
  },
  formInputs: {
    justifyContent: 'space-around',
  },
  category: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20,
    marginBottom: 20,
  },
  selectCategoryText: {
    color: '#fff',
  },
  buttons: {
    flexDirection: 'row',
  },
  buttonContainer: {
    backgroundColor: '#7bc24b',
    padding: 10,
    borderRadius: 4,
  },
  buttonText: {
    color: '#555'
  },
  Left: {
    justifyContent: 'flex-start',
    flexDirection: 'row',
    flex: 1,
  },
  Right: {
    justifyContent: 'flex-end',
    flexDirection: 'row',
    flex: 1
  },
  splashBody: {
     flex: 1,
     justifyContent: 'center',
     alignItems: 'center'
  },
  SplashText: {
    fontSize: 40,
    color: 'rgba(255,255,255,0.9)'
  },
  SplashTextMotto: {
    fontSize: 15,
    color: '#7dc24a',
  },
  splashTextArea: {
    justifyContent: 'center',
  },
  SplashTextDiv: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  splashLogo: {
    paddingLeft: 30,
    marginBottom: 20,
  },
  SplashMottoDiv: {
    flexDirection: 'column',
    alignItems: 'center',
    paddingBottom: 10
  },
  LoveCaption: {
    color: 'yellow'
  }
});

export default styles;
