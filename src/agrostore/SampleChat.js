import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, Image, TextInput, TouchableOpacity, KeyboardAvoidingView, Dimensions } from 'react-native';
import LocalImages from './components/LocalImage';
import StatusBar from './components/StatusBar';
window.navigator.userAgent = 'react-native';
import io from 'socket.io-client/dist/socket.io';
import BgImage from './components/BackgroundImage';

export default class Chat extends Component {

  async componentDidMount() {
    let _this = this; 
    this.socket = io('http://192.168.32.1:3000', {jsonp: false});
    await this.socket.disconnect();
    this.socket.connect('http://192.168.32.1:3000');
    this.socket.on('disconnect', () => {
        _this.socket.connect('http://192.168.32.1:3000');
    });
  }

  render() {
    return (
        <View style={styles.container}>
          <View style={{zIndex: 0}}>
            <BgImage source={require('./img/formBack.jpg')}>
              <View style={{height: Dimensions.get('window').height}}>
              </View>  
            </BgImage>
          </View>
          <View style={styles.contentArea}>
            <StatusBar style={{zIndex: 1000}} backgroundColor='transparent' />
            <View style={styles.mainContent}>
              <View style={styles.logo}>
                <View></View>  
              </View>
              <View style={styles.formContent}>
                <View></View>                  
              </View>
            </View>
          </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#16794a',
    justifyContent: 'center',
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'center',
    flex: 2,
    alignItems: 'center'
  },
  contentArea: {
    backgroundColor: 'rgba(37,173,224,0.7)',
    position: 'absolute',
    zIndex: 10, 
    width: Dimensions.get('window').width, 
    height: Dimensions.get('window').height
  },
  mainContent: {
   
  },
  body: {
    flex: 4,
    paddingLeft: 30,
    paddingRight: 30,
    paddingBottom: 20,
    justifyContent: 'flex-end'
  }, 
  HeaderText: {
    fontSize: 30,
    fontWeight: 'bold',
    color: '#fff'
  },
  SignupText: {
    fontSize: 20,
    color: '#fff',
    marginBottom: 20
  },
  input: {
    backgroundColor: 'white',
    padding: 10,
    marginBottom: 10,
    fontSize: 15,
    borderRadius: 5
  },
  formInputs: {
    justifyContent: 'space-around',
  },
  category: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20,
    marginBottom: 20,
  },
  selectCategoryText: {
    color: '#fff',
  },
  buttons: {
    flexDirection: 'row',
  },
  buttonContainer: {
    backgroundColor: '#7bc24b',
    padding: 10,
    borderRadius: 4,
  },
  buttonText: {
    color: '#fff'
  },
  Left: {
    justifyContent: 'flex-start',
    flexDirection: 'row',
    flex: 1,
  },
  Right: {
    justifyContent: 'flex-end',
    flexDirection: 'row',
    flex: 1
  }

});
