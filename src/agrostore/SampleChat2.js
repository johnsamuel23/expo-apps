import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, Image, TextInput, TouchableOpacity, KeyboardAvoidingView } from 'react-native';
import LocalImages from './components/LocalImage';
import StatusBar from './components/StatusBar';
window.navigator.userAgent = 'react-native';
import io from 'socket.io-client/dist/socket.io';

export default class Chat extends Component {

  async componentDidMount() {
    let _this = this; 
    this.socket = io('http://192.168.32.1:3000', {jsonp: false});
    await this.socket.disconnect();
    this.socket.connect('http://192.168.32.1:3000');
    this.socket.on('disconnect', () => {
        _this.socket.connect('http://192.168.32.1:3000');
    });
  }

  render() {
    return (
      <View style={styles.container}>
        
        <StatusBar backgroundColor="#007542" />
        <View style={{flex: 1}}>
            <View style={styles.formInputs}>
                <TextInput 
                  style={styles.input} 
                  placeholder="Message" 
                  placeholderTextColor="rgba(0,0,0,0.7)"
                  returnKeyType="next"
                  autoCapitalise="none"
                  autoCorrect={false}
                  underlineColorAndroid="transparent" />
            </View>

            <View style={styles.buttons}>
                <View style={styles.Left}>
                    <TouchableOpacity style={[styles.buttonContainer]} onPress={() => this.sendMessage()}>
                    <Text style={styles.buttonText}> Send </Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#16794a',
    justifyContent: 'center',
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'center',
    flex: 2,
    alignItems: 'center'
  },
  body: {
    flex: 4,
    paddingLeft: 30,
    paddingRight: 30,
    paddingBottom: 20,
    justifyContent: 'flex-end'
  }, 
  HeaderText: {
    fontSize: 30,
    fontWeight: 'bold',
    color: '#fff'
  },
  SignupText: {
    fontSize: 20,
    color: '#fff',
    marginBottom: 20
  },
  input: {
    backgroundColor: 'white',
    padding: 10,
    marginBottom: 10,
    fontSize: 15,
    borderRadius: 5
  },
  formInputs: {
    justifyContent: 'space-around',
  },
  category: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20,
    marginBottom: 20,
  },
  selectCategoryText: {
    color: '#fff',
  },
  buttons: {
    flexDirection: 'row',
  },
  buttonContainer: {
    backgroundColor: '#7bc24b',
    padding: 10,
    borderRadius: 4,
  },
  buttonText: {
    color: '#fff'
  },
  Left: {
    justifyContent: 'flex-start',
    flexDirection: 'row',
    flex: 1,
  },
  Right: {
    justifyContent: 'flex-end',
    flexDirection: 'row',
    flex: 1
  }

});
