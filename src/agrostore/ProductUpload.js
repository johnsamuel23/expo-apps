import React, { Component } from 'react';
import { 
    StyleSheet, 
    Text, 
    View, 
    ScrollView, 
    TextInput, 
    KeyboardAvoidingView, 
    TouchableOpacity, 
    Dimensions
} from 'react-native';
import ImageUploader from './components/ImageUploader';
import LocalImage from './components/LocalImage';
import StatusBar from './components/StatusBar';
import ResponsiveImage from 'react-native-responsive-image';
import { ImagePicker } from 'expo';
import { uploadProduct, storageRoot } from './api';

export default class ProductUpload extends Component {

    startImagePick() {
        this.takeAndUploadPhotoAsync();
    }

    state = {
        uploading: false,
        interest_id: "0",
        product_name: "New Product",
        quantity_stocked: "20",
        quantity_sold: "0",
        price_per_unit: "1000",
        price_bulk_total: "15000",
        creator_user_id: "17",
        source_location: "0",
    }

    async takeAndUploadPhotoAsync() {
        // Display the camera to the user and wait for them to take a photo or to cancel
        // the action
        let result = await ImagePicker.launchImageLibraryAsync({
          allowsEditing: false,
        });
      
        if (result.cancelled) {
          return;
        }
      
        // ImagePicker saves the taken photo to disk and returns a local URI to it
        let localUri = result.uri;
        let filename = localUri.split('/').pop();
      
        // Infer the type of the image
        let match = /\.(\w+)$/.exec(filename);
        let type = match ? `image/${match[1]}` : `image`;

        console.log("data type:", type);
        console.log("localUri:", localUri);

        this.setState({
            image: localUri
        });

        let data = {
            interest_id: this.state.interest_id,
            name: this.state.product_name,
            quantity_stocked: this.state.quantity_stocked,
            quantity_sold: this.state.quantity_sold,
            price_per_unit: this.state.price_per_unit,
            price_bulk_total: this.state.price_bulk_total,
            creator_user_id: this.state.creator_user_id,
            source_location: this.state.source_location,
            image: {
                uri: localUri,
                name: filename, 
                type
            }
        }

        uploadProduct(data).then(response => {
            console.log("RESPONSE", response.data);
            // uploaded image link
            let imageSource = storageRoot + response.data.product_image_name;
            console.log("ImAGE Source", imageSource);
            this.setState({
                image: imageSource
            });

        }).catch(error => {
            console.log("ERROR", error.data);
        });

    }

    maybeRenderImage() {
        let { image } = this.state;
        if(!image) {
            return;
        }

        // console.log();
        console.log("ImAGE Source", image);

        return (
            <View style={styles.imagePlaceholder}>
                <ResponsiveImage
                  style={{
                      width: deviceWidth,
                      height: 200
                  }}
                  source={this.state.image}
                  initWidth={deviceWidth} 
                  initHeight={200} />
            </View>
        )
    }

    saveField(field, value) {
        this.setState({
            [field]: value
        });
    }
    
    render() {
        return(
             <View style={styles.container}>
                 <StatusBar backgroundColor="#007542"/>
                 <ScrollView>
                    <View style={styles.productContainer}>
                        <View style={styles.productInfo}>
                            
                            <View style={styles.proInfoLabel}>
                                <Text style={[styles.ProInfoLabelText]}> PRODUCT UPLOAD </Text>
                            </View> 

                            <View style={styles.productInfoLabel}>
                                <Text style={[styles.ProductInfoLabelText]}>Product Name: </Text>
                                <TextInput
                                    style={styles.input}
                                    value={this.state.product_name}
                                    onChangeText={(value) => this.saveField('product_name', value)}
                                    underlineColorAndroid="#e5eae9"        
                                />
                            </View>
                            <View style={styles.productInfoLabel}>
                                <Text style={[styles.ProductInfoLabelText]}>From: </Text>
                                <TextInput
                                    style={styles.input}
                                    value={this.state.source_location}
                                    onChangeText={(value) => this.saveField('source_location', value)}
                                    underlineColorAndroid="#e5eae9"        
                                />
                            </View>
                            <View style={styles.productInfoLabel}>
                                <Text style={[styles.ProductInfoLabelText]}>Price: </Text>
                                <TextInput
                                    style={styles.input}
                                    value={this.state.price_per_unit}
                                    onChangeText={(value) => this.saveField('price_per_unit', value)}
                                    underlineColorAndroid="#e5eae9"        
                                />
                            </View>
                            <View style={styles.productInfoLabel}>
                                <Text style={[styles.ProductInfoLabelText]}>Bulk Price: </Text>
                                <TextInput
                                    value={this.state.price_bulk_total}
                                    onChangeText={(value) => this.saveField('price_bulk_total', value)}
                                    style={styles.input}
                                    underlineColorAndroid="#e5eae9"        
                                />
                            </View>
                            <View style={styles.productInfoLabel}>
                                <Text style={[styles.ProductInfoLabelText]}>Shipping Location: </Text>
                                <TextInput
                                    value={this.state.source_location}
                                    onChangeText={(value) => this.saveField('source_location', value)}
                                    style={styles.input}
                                    underlineColorAndroid="#e5eae9"        
                                />
                            </View>
                            <TouchableOpacity onPress={() => this.startImagePick()} style={styles.uploadContainer}>
                                    <Text style={styles.uploadbuttonText}>Upload image </Text>
                            </TouchableOpacity>

                            { this.maybeRenderImage() }

                            <TouchableOpacity style={styles.buttonContainer}>
                                <Text style={styles.buttonText}>Save Product </Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                 </ScrollView>
             </View>
        );
    }
}

let deviceWidth = Dimensions.get('window').width;

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#347c3e',
        marginBottom: 50,
    },
    imagePlaceholder: {
        flex: 1,
        minHeight: 200
    },
    productInfo: {
        backgroundColor: 'ghostwhite',
        padding: 15,
        borderColor: '#050505'
    },
    productInfoLabel: {
        flexDirection: 'row',
        paddingBottom: 6,
        alignItems: 'center',
    },
    ProductInfoLabelText: {
        color: 'green',
        flex: 1,
        alignItems: 'center',
    },

    proInfoLabel: {
        flexDirection: 'row',
        paddingBottom: 10

    },
    ProInfoLabelText: {
        color: '#050505',
        flex: 1,
        fontWeight: 'bold',
        textAlign: 'center',
        marginTop: 10
    },
      uploadContainer: {
        backgroundColor: '#a4a5a4',
        padding: 5,
        width: 130,
        borderRadius: 3,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 10,
        marginBottom: 10,
    },
    uploadbuttonText: {
         color: 'ghostwhite',
         textAlign: 'center'
    },
    buttonContainer: {
        backgroundColor: 'red',
        padding: 8,
        borderRadius: 10,
        marginTop: 10,
    },
    buttonText: {
        textAlign: 'center',
        color: '#fff'
    },
    input: {
      flex: 4,  
      backgroundColor: '#e5eae9',
      flexDirection: 'row',
      borderRadius: 2,
      padding: 5
    
    },
});   