import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, TouchableOpacity, TouchableNativeFeedback} from 'react-native';
import { Constants } from 'expo'; 
export default class CartProduct extends Component {

    state = {
        screens: [
            {
                screenName: 'SplashScreen',
                route_name: 'splash_screen'
            },
            {
                screenName: 'Registration Screen',
                route_name: 'registration_screen'
            },
            // {
            //     screenName: 'Customer Registration Screen',
            //     route_name: 'registration_customer_screen'
            // },
            // {
            //     screenName: 'Merchant Registration Screen',
            //     route_name: 'registration_merchant_screen'
            // }, 
            {
                screenName: 'Products Screen',
                route_name: 'products_screen'
            },
            {
                screenName: 'Products Search Screen',
                route_name: 'product_search_screen'
            },
            {
                screenName: 'Product Upload Screen',
                route_name: 'product_upload_screen'
            },
            {
                screenName: 'Product Request Screen',
                route_name: 'product_request_screen'
            },
            {
                screenName: 'Product Payment Screen',
                route_name: 'product_payment_screen'
            }
        ]
    }

    switchScreen(route) {
        // const { closeDrawer } = this.props;
        // console.log("props 000000000000000-------", this.props);
        // if(closeDrawer) {
        //     closeDrawer();
        // }
        setTimeout(() => {
            this.props.navigator.push({name: route});
        }, 50);
    }

    renderMenu() {
        const menu = this.state.screens.map((item, index) => {
            return <TouchableOpacity
                onPress={() => this.switchScreen(item.route_name)}
                key={index}
            >
                <View style={styles.menuButton}>
                    <Text style={styles.itemText}>{item.screenName}</Text>
                </View>
            </TouchableOpacity>
        }); 
        
        return menu;
    }

    render() {
        const menu = this.renderMenu();
        return (
            <View style={styles.container}>
                <View style={styles.statusBar}></View>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <View style={[styles.menuButton, {backgroundColor: 'orange', height: 60}]}>
                        <Text style={{color: '#fff', textAlign: 'center', fontSize: 15, padding: 20}}>The User Interface Screens available on the App</Text>
                    </View>
                    { menu }
                </ScrollView>
            </View>  

        );
    } 
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20,
        backgroundColor: '#007542'
    },
    statusBar: {
        height: Constants.statusBarHeight,
        backgroundColor: 'transparent'
    },
    menuButton: {
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
        height: 45,
        marginBottom: 10
    },
    itemText: {
        color: '#555',
        textAlign: 'center'
    }, 
});
  