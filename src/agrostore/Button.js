import React, { Component } from 'react';
import { StyleSheet, Text, View  } from 'react-native';
 
export default class FlexLayouts extends Component {
    render() {
        let bgColor, weight, textWeight, textColor
        if(this.props.red){
            bgColor = "red"
            textWeight = 'normal'
        } else {
            bgColor = "#cecece"
            textWeight = 'bold'
        }
        return (
            <View style={[styles.PinButton, {backgroundColor: bgColor}]}>
                {this.props.icon && <Pin color="white" />}
                <Text style={[styles.PinButtonText, {fontWeight: textWeight}]}>{this.props.text}</Text>
            </View>
                       
        )
    }
}

const styles = StyleSheet.create({
    PinButton: {
        flexDirection: 'row',
        backgroundColor: 'red',
        borderRadius: 6,
        width: 60,
        padding: 5,
        justifyContent: 'center' 
    },
    
    PinButtonText: {
        color: 'white',
        justifyContent: 'center'
    },
  
}); 