import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, Image, TextInput, Dimensions,  TouchableOpacity, Animated, Easing,} from 'react-native';
import LocalImage from './components/LocalImage';
import Product from './Product';


export default class Products extends Component {

  componentWillMount() {
        this.state = {
        products: [ 
            {
                id: 1,
                product_name: 'Vegetables',
                stock_available: 1000,
                location: 'Random',
                uploaded: '3RD JULY, 2017',
                price_per_unit: 1000,
                bulk_price: 1000,
                shipping_location: "Owerri, Benue, Ogun",
                product_image: require('./img/tomatoes-5356_960_720.jpg'),
            },
            {
                id: 2,
                product_name: 'Tomatoes',
                stock_available: 9000,
                location: 'Random',
                uploaded: '4TH JULY, 2017',
                price_per_unit: 1000,
                bulk_price: 1000,
                shipping_location: "Akure, Ibadan, Jos",
                product_image: require('./img/tomatoes-5356_960_720.jpg')

            },
            {
                id: 3,
                product_name: 'Yam',
                stock_available: 6000,
                location: 'Random',
                uploaded: '5TH JULY, 2017',
                price_per_unit: 600,
                bulk_price: 5000,
                shipping_location: "Delta, Uyo, Katsina",
                product_image: require('./img/yam.jpg')
            },
            {
                id: 4,
                product_name: 'Onions',
                stock_available: 10000,
                location: 'Random',
                uploaded: '15TH JULY, 2017',
                price_per_unit: 50,
                bulk_price: 1000,
                shipping_location: "Onitcha, Ogun, Kaduna",
                product_image: require('./img/onions.jpg')
            },
            {
                id: 5,
                product_name: 'Cassava',
                stock_available: 4000,
                location: 'Random',
                uploaded: '20TH JULY, 2017',
                price_per_unit: 300,
                bulk_price: 3000,
                shipping_location: "Zamfara, Kogi, Niger",
                product_image: require('./img/inline-cassava.jpg')
            },
          
        ]
    }
  }

    render() {

        const products = this.state.products.map((item, index) => {
            return <Product
                    key={item.id}
                    product_name={item.product_name}
                    stock_available={item.stock_available}
                    location={item.location}
                    uploaded={item.uploaded}
                    price_per_unit={item.price_per_unit}
                    bulk_price={item.bulk_price}
                    shipping_location={item.shipping_location}
                    product_image={item.product_image}
                    />
        });  
           
        return (
            <ScrollView style={styles.container}>
                { products }
            </ScrollView>
        );
    }
}    

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#347c3e',
        padding: 15
    },
})

