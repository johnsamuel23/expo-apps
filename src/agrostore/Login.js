import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, Image, TextInput, TouchableOpacity, KeyboardAvoidingView, Dimensions, Settings } from 'react-native';
import LocalImages from './components/LocalImage';
import StatusBar from './components/StatusBar';
import { userLogin } from './api';
import ToastFeedback from './components/ToastFeedback';

export default class Registration extends Component {

  handleChange(value, field) {
    // console.log("input Values => ", value);
    let { saveUserData } = this.props;
    let userData = {
      [field]: value
    }

    saveUserData(userData);

    this.setState({
      [field]: value
    })
  }

  state = {
    messageBoxOpacity: 0,
  }

  userLogin() {
    let { email, password } = this.props.store.userInfo;

    let data = {
        email,
        password
    }

    userLogin(data).then(response => {
        console.log("response", response.data);
        let { data } = response;
        let message = ""; 
        let messageBoxOpacity = 0;

        if(data.message === "logged_in") {
            message = "Successfully Logged In";
            messageBoxOpacity = 1;
        } else { 
            // message = "Please provide correct Login details";
            // messageBoxOpacity = 1;
        }

        setTimeout(() => {
            this.setState({
                message: '',
                messageBoxOpacity: 0
            });
        }, 3000);

        this.props.navigator.push({name: 'products_screen'});
        this.setState({errorMessage: message, messageBoxOpacity});
    });
  }

  render() {
    let { email, password} = this.props.store.userInfo;
    let { navigator } = this.props;

    return (
      <View style={styles.container}>
        <StatusBar backgroundColor="#007542" />
        <View style={{flex: 1}}>
          <View style={styles.header}>
              <View>
                <LocalImages 
                  source={require('./img/logo.png')} 
                  originalWidth={512}
                  originalHeight={512}
                  guideWidth={80} />
              </View>
              <View>
                <Text style={styles.HeaderText}> AgroMass</Text>
              </View>
          </View>
          <View style={styles.body}>
            <View>
              <Text style={styles.SignupText}>Sign Up</Text>
            </View>
            <View style={styles.formArea}>
              <View style={styles.formInputs}>
                <TextInput 
                  value={email}
                  style={styles.input} 
                  placeholder="Email" 
                  placeholderTextColor="rgba(0,0,0,0.7)"
                  returnKeyType="next"
                  autoCapitalise="none"
                  autoCorrect={false}
                  onChangeText={(value) => this.handleChange(value, "email")}
                  underlineColorAndroid="transparent" />

                <TextInput 
                  style={styles.input} 
                  placeholder="Password" 
                  placeholderTextColor="rgba(0,0,0,0.7)"
                  returnKeyType="next"
                  secureTextEntry={true}
                  autoCapitalise="none"
                  autoCorrect={false}
                  value={password}
                  onChangeText={(value) => this.handleChange(value, "password")}
                  underlineColorAndroid="transparent" />
              </View>

              <View style={styles.buttons}>
                  <TouchableOpacity 
                    onPress={() => this.userLogin()} 
                    style={[styles.buttonContainer, {width: 200, marginLeft: 'auto', marginRight: 'auto'}]}>
                    <Text style={[styles.buttonText, {textAlign: 'center'}]}> Login </Text>
                  </TouchableOpacity>
              </View>

              <View style={styles.category}>
                <Text style={styles.selectCategoryText}>
                  Not Yet Registered?
                </Text>

                <View style={styles.buttons}>
                    <TouchableOpacity 
                        onPress={() => navigator.push({name: 'registration_screen'})} 
                        style={[styles.buttonContainer, {backgroundColor: '#e06411', width: 100, marginTop: 10, marginLeft: 'auto', marginRight: 'auto'}]}
                    >
                        <Text style={[styles.buttonText, {textAlign: 'center'}]}> Sign up </Text>
                    </TouchableOpacity>
                </View>
              </View>

              

            </View>
          </View>
        </View>
        <ToastFeedback />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#16794a',
    justifyContent: 'center',
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'center',
    flex: 2,
    alignItems: 'center'
  },
  body: {
    flex: 4,
    paddingLeft: 30,
    paddingRight: 30,
    paddingBottom: 50,
    justifyContent: 'flex-end'
  }, 
  HeaderText: {
    fontSize: 30,
    fontWeight: 'bold',
    color: '#fff'
  },
  SignupText: {
    fontSize: 20,
    color: '#fff',
    marginBottom: 20
  },
  input: {
    backgroundColor: 'white',
    padding: 10,
    marginBottom: 10,
    fontSize: 15,
    borderRadius: 5
  },
  formInputs: {
    justifyContent: 'space-around',
  },
  category: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20,
    marginBottom: 20,
  },
  selectCategoryText: {
    color: '#fff',
  },
  buttons: {
    // flexDirection: 'row',
  },
  buttonContainer: {
    backgroundColor: '#7bc24b',
    padding: 10,
    borderRadius: 4,
  },
  buttonText: {
    color: '#fff'
  },
  Left: {
    justifyContent: 'flex-start',
    flexDirection: 'row',
    flex: 1,
  },
  Right: {
    justifyContent: 'flex-end',
    flexDirection: 'row',
    flex: 1
  }

});
