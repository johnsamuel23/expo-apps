import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, Image, TextInput, TouchableOpacity, KeyboardAvoidingView } from 'react-native';
import LocalImages from './components/LocalImage';
import StatusBar from './components/StatusBar';
import { registerUser } from './api';

export default class Registration extends Component {

  constructor(props) {
    super(props);
    
  }

  handleUserSelect(userType) {
    let { store } = this.props;
    let { name, contact, email, address, username, primary_location, user_type, password, interest_ids } = store.userInfo;
    let userData = { name, contact, address, username, primary_location, user_type, password, interest_ids };
    let userInfo = store.userInfo;

    this.updateUserSelection(userData);

    let splitName = name.split(" ");
    let firstname = splitName[0];
    let lastname = splitName.length > 1 ? splitName[splitName.length - 1] : "";

    userInfo.firstname = firstname;
    userInfo.lastname = lastname;
    userInfo.user_type = userType;

    console.log("userInfo", userInfo);

    let isValid = false;

    if(name==="" || email==="" || contact==="" || password==="") {
      isValid = false;
    } else {
      isValid = true;
    }

    if(isValid) {
      registerUser(userInfo).then(response => {
        console.log("RESPONSE", response.data);
      });

      if(userType === "merchant") {
        this.props.navigator.push({name: 'registration_merchant_screen'});

      } else {
        this.props.navigator.push({name: 'registration_customer_screen'});      
      }
    }

  }

  updateUserSelection(userType) {
    this.props.updateUser(userType);
  }

  handleChange(value, field) {
    console.log("input Values => ", value);
    let { saveUserData } = this.props;
    let userData = {
      [field]: value
    }

    saveUserData(userData);

    this.setState({
      [field]: value
    })
  }

  render() {
    let { email, name, contact, password} = this.props.store.userInfo;
    return (
      <View style={styles.container}>
        <StatusBar backgroundColor="#007542" />
        <View style={{flex: 1}}>
          <View style={styles.header}>
              <View>
                <LocalImages 
                  source={require('./img/logo.png')} 
                  originalWidth={512}
                  originalHeight={512}
                  guideWidth={80} />
              </View>
              <View>
                <Text style={styles.HeaderText}> AgroMass</Text>
              </View>
          </View>
          <View style={styles.body}>
            <View>
              <Text style={styles.SignupText}>Sign Up</Text>
            </View>
            <View style={styles.formArea}>
              <View style={styles.formInputs}>
                <TextInput 
                  value={name}
                  style={styles.input} 
                  placeholder="Name" 
                  placeholderTextColor="rgba(0,0,0,0.7)"
                  returnKeyType="next"
                  autoCapitalise="none"
                  autoCorrect={false}
                  onChangeText={(value) => this.handleChange(value, "name")}
                  underlineColorAndroid="transparent" />

                <TextInput 
                  style={styles.input} 
                  placeholder="E-mail" 
                  placeholderTextColor="rgba(0,0,0,0.7)"
                  keyboardType="email-address"
                  autoCapitalise="none"
                  autoCorrect={false}
                  value={email}
                  onChangeText={(value) => this.handleChange(value, "email")}
                  underlineColorAndroid="transparent" />

                <TextInput 
                  style={styles.input} 
                  placeholder="Phone Number" 
                  placeholderTextColor="rgba(0,0,0,0.7)"
                  returnKeyType="next"
                  autoCapitalise="none"
                  autoCorrect={false}
                  value={contact}
                  onChangeText={(value) => this.handleChange(value, "contact")}
                  underlineColorAndroid="transparent" />

                <TextInput 
                  style={styles.input} 
                  placeholder="Password" 
                  placeholderTextColor="rgba(0,0,0,0.7)"
                  returnKeyType="next"
                  secureTextEntry={true}
                  autoCapitalise="none"
                  autoCorrect={false}
                  value={password}
                  onChangeText={(value) => this.handleChange(value, "password")}
                  underlineColorAndroid="transparent" />
              </View>

              <View style={styles.category}>
                <Text style={styles.selectCategoryText}>
                  Are you a Customer or a Merchant?
                </Text>
              </View>

              <View style={styles.buttons}>
                <View style={styles.Left}>
                  <TouchableOpacity 
                    style={[styles.buttonContainer]} 
                    onPress={() => this.handleUserSelect('customer')}>
                    <Text style={styles.buttonText}> Customer </Text>
                  </TouchableOpacity>
                </View>
                <View style={styles.Right}>
                  <TouchableOpacity 
                    style={[styles.buttonContainer]} 
                    onPress={() => this.handleUserSelect('merchant')}>
                    <Text style={styles.buttonText}> Merchant </Text>
                  </TouchableOpacity>
                </View>
              </View>

              <View>
                <Text style={[{textAlign: 'center', color: 'white'}]}> Already Registered? </Text>                  
                <TouchableOpacity 
                    onPress={() => this.props.navigator.push({name: 'login_screen'})} 
                    style={[styles.buttonContainer, {backgroundColor: '#e06411', width: '100%', marginTop: 10, marginLeft: 'auto', marginRight: 'auto'}]}
                >
                    <Text style={[styles.buttonText, {textAlign: 'center'}]}> Login </Text>
                </TouchableOpacity>
              </View>

            </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#16794a',
    justifyContent: 'center',
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'center',
    flex: 2,
    alignItems: 'center'
  },
  body: {
    flex: 4,
    paddingLeft: 30,
    paddingRight: 30,
    paddingBottom: 20,
    justifyContent: 'flex-end'
  }, 
  HeaderText: {
    fontSize: 30,
    fontWeight: 'bold',
    color: '#fff'
  },
  SignupText: {
    fontSize: 20,
    color: '#fff',
    marginBottom: 20
  },
  input: {
    backgroundColor: 'white',
    padding: 10,
    marginBottom: 10,
    fontSize: 15,
    borderRadius: 5
  },
  formInputs: {
    justifyContent: 'space-around',
  },
  category: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20,
    marginBottom: 20,
  },
  selectCategoryText: {
    color: '#fff',
  },
  buttons: {
    flexDirection: 'row',
  },
  buttonContainer: {
    backgroundColor: '#7bc24b',
    padding: 10,
    borderRadius: 4,
  },
  buttonText: {
    color: '#fff'
  },
  Left: {
    justifyContent: 'flex-start',
    flexDirection: 'row',
    flex: 1,
  },
  Right: {
    justifyContent: 'flex-end',
    flexDirection: 'row',
    flex: 1
  }

});
