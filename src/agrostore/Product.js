import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, Image, TextInput, Dimensions,  TouchableOpacity, Animated, Easing,} from 'react-native';
import LocalImage from './components/LocalImage';

export default class Product extends Component {

    render() {
        // console.log("image url", this.props.product_image);

        return (
            <View style={styles.container}>
              <View style={styles.productContainer}>
                    <View style={styles.productImage}> 
                        <LocalImage
                            source= {this.props.product_image}
                            originalWidth="1170"
                            originalHeight="700"
                            guideWidth={Dimensions.get('window').width - 30}
                        />
                    </View>
        
                    <View style={styles.productInfo}>
                        <View style={styles.productInfoLabel}>
                            <Text style={[styles.ProductInfoLabelText]}> Product: </Text>
                            <Text style={[styles.ProductInfoValueText]}> {this.props.product_name} </Text>
                        </View>
                         <View style={styles.productInfoLabel}>
                            <Text style={[styles.ProductInfoLabelText]}> Stock Available: </Text>
                            <Text style={[styles.ProductInfoValueText]}> {this.props.stock_available} </Text>
                        </View>
                         <View style={styles.productInfoLabel}>
                            <Text style={[styles.ProductInfoLabelText]}> From: </Text>
                            <Text style={[styles.ProductInfoValueText]}> {this.props.location} </Text>
                        </View>
                         <View style={styles.productInfoLabel}>
                            <Text style={[styles.ProductInfoLabelText]}> Uploaded: </Text>
                            <Text style={[styles.ProductInfoValueText]}> {this.props.uploaded} </Text>
                        </View>
                         <View style={styles.productInfoLabel}>
                            <Text style={[styles.ProductInfoLabelText]}> Price Per Unit (Naira): </Text>
                            <Text style={[styles.ProductInfoValueText]}> {this.props.price_per_unit} </Text>
                        </View>
                         <View style={styles.productInfoLabel}>
                            <Text style={[styles.ProductInfoLabelText]}> Bulk Price (Naira):</Text>
                            <Text style={[styles.ProductInfoValueText]}> {this.props.bulk_price}</Text>
                        </View>
                         <View style={styles.productInfoLabel}>
                            <Text style={[styles.ProductInfoLabelText]}> Shipping Location: </Text>
                            <Text style={[styles.ProductInfoValueText]}> {this.props.shipping_location} </Text>     
                        </View> 
                        <TouchableOpacity style={styles.buttonContainer}>
                                <Text style={styles.buttonText}> Add to Cart </Text>
                        </TouchableOpacity>
   
                    </View>
                    
              </View>
            </View>
        );
    }
}    

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#347c3e',
        marginBottom: 10
    },
    productImage: {
        alignSelf: 'center'
    },
    productInfo: {
        backgroundColor: 'white',
        padding: 15
    },
    productInfoLabel: {
        flexDirection: 'row',
        paddingBottom: 10
    },
    ProductInfoLabelText: {
        color: 'green',
        flex: 1
    },
    ProductInfoValueText: {
        flex: 1,
        color: '#333',
    },
     buttonContainer: {
        backgroundColor: 'red',
        padding: 8,
        borderRadius: 10
    },
    buttonText: {
        textAlign: 'center',
        color: '#fff'
    },
});

