import React, { Component } from 'react';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { View } from 'react-native';

export default SelectDrop = ({
    size,
    color
}) => {
    const Icon = (<MaterialCommunityIcons name="chevron-down" size={size} color={color} />)
    return (
        <View>
            { Icon }     
        </View>
    )

}