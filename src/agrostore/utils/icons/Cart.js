import React, { Component } from 'react';
import { FontAwesome } from '@expo/vector-icons';
import { View } from 'react-native';

export default Back = ({
    size,
    color
}) => {
    const Icon = (<FontAwesome name="shopping-cart" size={size} color={color} />)
    return (
        <View>
            { Icon }     
        </View>
    )

}