import React, { Component } from 'react';
import { FontAwesome } from '@expo/vector-icons';
import { View } from 'react-native';

export default WpForms = ({
    size,
    color
}) => {
    const Icon = (<FontAwesome name="wpforms" size={size} color={color} />)
    return (
        <View>
            { Icon }     
        </View>
    )

}