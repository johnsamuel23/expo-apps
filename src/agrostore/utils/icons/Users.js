import React, { Component } from 'react';
import { FontAwesome } from '@expo/vector-icons';
import { View } from 'react-native';

export default Users = ({
    size,
    color
}) => {
    const Icon = (<FontAwesome name="users" size={size} color={color} />)
    return (
        <View>
            { Icon }     
        </View>
    )

}