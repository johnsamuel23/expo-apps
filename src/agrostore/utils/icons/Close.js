import React, { Component } from 'react';
import { FontAwesome } from '@expo/vector-icons';
import { View } from 'react-native';

export default Close = ({
    size,
    color
}) => {
    const Icon = (<FontAwesome name="times" size={size} color={color} />)
    return (
        <View>
            { Icon }     
        </View>
    )

}