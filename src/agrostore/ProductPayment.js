import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, Image, TextInput, Dimensions, KeyboardAvoidingView, TextInputMask, TouchableOpacity} from 'react-native';
import StatusBar from './components/StatusBar';
import { checkoutCart } from './api';

export default class ProductPayment extends Component {

    componentWillMount() {
        let { store } = this.props;
        let { cartOrders } = store;
        let total_pay = "";
        for(let i=0; i < cartOrders.length; i++) {
            let order = cartOrders[i];
            total_pay = total_pay + order.cost_price;
        }

        this.setState({ total_pay: total_pay , paymentDone: false });
    }


    makePayment() {
        let { navigator, store, saveStateData } = this.props;
        let { userInfo, cartOrders, currentCartId } = store;
        let total_pay = this.state.total_pay;

        let data = {
            user_id: userInfo.user_id,
            cart_id: currentCartId,
            total_pay
        }

        console.log("DATA", data);
        
        checkoutCart(data).then(response => {
            console.log("CHECKOUT", response.data);
            // reset following store data
            // currentCartId
            // cartOrders
            saveStateData("currentCartId", 0);
            saveStateData("cartOrders", []);
            this.setState({
                paymentDone: true
            });
        }).catch(error => {
            console.log("error saving checkout", error);
        });
    }

    renderPaymentView() {
        let { navigator } = this.props;

        return (
            <View style={styles.productInfo}>
                <View style={styles.logoContainer}>
                    <Image style={styles.logo} source={require('./img/card.jpg')} />
                </View>
                <View style={[styles.proInfoLabel, {marginTop: 20, marginBottom: 10}]}>
                    <Text style={[styles.ProInfoLabelText]}> DEBIT CARD DETAILS </Text>
                </View>
                <TextInput
                    editable={false}
                    value={this.state.total_pay}
                    style={styles.input}
                    placeholder= 'Amount (Naira):'
                    placeholderTextColor= "#999999"
                    underlineColorAndroid="#e5eae9"        
                />
                <TextInput
                    style={styles.input}
                    placeholder= 'Card Number:'
                    keyboardType={"numeric"}
                    placeholderTextColor= "#999999"
                    underlineColorAndroid="#e5eae9"        
                />
                <TextInput
                    style={styles.input}
                    placeholder= 'MM/YY:'
                    placeholderTextColor= "#999999"
                    underlineColorAndroid="#e5eae9"
                    returnKeyType="go" 
                    ref={'myDateText'}   
                    format='MM-YYYY'  
                    />
                <TextInput
                    style={styles.input}
                    placeholder="CVV:"
                    secureTextEntry
                    placeholderTextColor= "#999999"
                    underlineColorAndroid="#e5eae9"
                    returnKeyType="go"        
                />
                <TextInput
                    style={styles.input}
                    placeholder="Security Code:"
                    secureTextEntry
                    placeholderTextColor= "#999999"
                    underlineColorAndroid="#e5eae9"
                    returnKeyType="go"        
                />
                
                <TouchableOpacity style={styles.buttonContainer} onPress={() => this.makePayment()}>
                    <Text style={styles.buttonText}> PAY </Text>
                </TouchableOpacity>

                <View style={[styles.proInfoLabel, {margin: 10}]}>
                    <Text style={[styles.ProInfoLabelText]}> OR </Text>
                </View>

                <TouchableOpacity style={styles.btContainer}>
                    <Text style={styles.buttonText}> LOGIN TO VERVE e-WALLET </Text>
                </TouchableOpacity>
            </View> 
        );
    }

    renderCongratsView() {
        return (
            <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                <Text style={{fontSize: 20, color: '#555', textAlign: 'center'}}>Thank you</Text>
                <Text style={{fontSize: 20, color: '#555', textAlign: 'center'}}>Your Order has been placed</Text>
                <TouchableOpacity style={[styles.buttonContainer, {backgroundColor: '#e43137', marginTop: 20}]} onPress={() => this.props.navigator.push({name: 'products_screen'})}>
                    <Text style={[styles.buttonText]}> Back to Products </Text>
                </TouchableOpacity>
            </View>
        )
    }

    render() {
        let renderedView = this.renderPaymentView();
        if(this.state.paymentDone) {
            renderedView = this.renderCongratsView();
        }

        return(
            <KeyboardAvoidingView style={styles.container} behavior="padding">
                <StatusBar backgroundColor="#007542" />
                <View style={{flex: 1}}>

                    { renderedView }    

                 </View>               
              </KeyboardAvoidingView>

         );
     }
    
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        flex: 1
    },
    productInfo: {
        backgroundColor: '#fff',
        padding: 15,
        borderColor: '#050505'
    },
    productInfoLabel: {
        flexDirection: 'row',
        paddingBottom: 10,
    },
    logoContainer: {
        flex: 1,
        paddingBottom: 15,
        paddingTop: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    buttonContainer: {
        backgroundColor: '#4885ed',
        padding: 10,
        borderRadius: 3,
        marginTop: 12
    },
    btContainer: {
        backgroundColor: 'navy',
        padding: 10,
        borderRadius: 3,
    },
    buttonText: {
      textAlign: 'center',
      color: '#fff'
    }, 
    input: {
      backgroundColor: '#e5eae9',
      flexDirection: 'row',
      marginBottom: 10,
      paddingVertical: 7,
      paddingHorizontal: 8,
      borderColor: '#ccc',
      borderWidth: 1,
      fontSize: 15
    },
    proInfoLabel: {
        flexDirection: 'row',
    },
    ProInfoLabelText: {
        color: '#050505',
        flex: 1,
        fontWeight: 'bold',
        textAlign: 'center'
    },

});