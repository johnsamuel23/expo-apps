import React, { Component } from 'react';
import { 
    StyleSheet, 
    Text, 
    View, 
    ScrollView, 
    Image, 
    TextInput, 
    Dimensions, 
    KeyboardAvoidingView, 
    TouchableOpacity, 
    Animated, 
    Easing, 
    Platform 
} from 'react-native';
import LocalImage from './components/LocalImage'
import StatusBar from './components/StatusBar';
import ScreenHeading from './components/ScreenHeading';
import Product from './components/Product';
import Touch from './components/common/Touch';
import ResponsiveImage from 'react-native-responsive-image';
import { createOrder as saveOrder} from './api';
import Toast from './components/common/Toast';

const deviceWidth = Dimensions.get('window').width;

export default class ProductRequest extends Component {

    addToCart(newOrder, product) {
        let { store, saveStateData, navigator } = this.props;
        let { cartOrders, userInfo, currentCartId, appActions } = store;
        let { bulk_price, description, product_name, price_per_unit, product_image, stock_available } = product;
        let cost_price = newOrder.bulk_purchase ? bulk_price : price_per_unit * newOrder.quantity_ordered;

        let data = {
            user_id: userInfo.user_id,
            product_id: newOrder.productId,
            quantity_purchased: newOrder.quantity_ordered,
            order_location_id: 0,
            cost_price,
            cart_id: currentCartId
        }

        newOrder.cost_price = cost_price;

        if(newOrder.quantity_ordered !== undefined && newOrder.quantity_ordered != "") {
            // let appActions = {...appActions};
            
            let { createOrder } = appActions;
            let statusMessage = "Adding Order";  
            
            // console.log("CREATE ORDER", appActions);

            if(createOrder) {
                createOrder.status = "pending";
                createOrder.statusMessage = statusMessage;
                appActions.createOrder = createOrder;                
            } else {
                let createOrder = {};
                createOrder.status = "pending";
                createOrder.statusMessage = statusMessage                    
                appActions.createOrder = createOrder;
            }
            // add a cue 

            // console.log("CREATE ORDER", createOrder);
            appActions.createOrder = createOrder;                
            saveStateData("appActions", appActions);

            saveOrder(data).then(response => {
                let statusMessage = "Order Added";                    

                if(createOrder) {
                    createOrder.status = "done";
                    createOrder.statusMessage = statusMessage;
                    appActions.createOrder = createOrder;                    
                } else {
                    let createOrder = {};
                    createOrder.status = "done";
                    createOrder.statusMessage = statusMessage                    
                    appActions.createOrder = createOrder;
                }

                setTimeout(()=> {
                    saveStateData("appActions", appActions);
                    navigator.pop({name: 'product_request_screen'});
                }, 1000);
                // console.log("RESPONSE", response.data);
                // console.log("RESPONSEE.............", appActions);
                let { data } = response;
                if(data && data != []) {
                    saveStateData("currentCartId", data.cart_id);
                    newOrder.product_id = newOrder.productId;
                    let newCartOrders = [...cartOrders, newOrder];
                    saveStateData("cartOrders", newCartOrders);
                }
            }).catch(error => {
                console.log("ERROR", error);
            });
        }
    }

    saveInput(value) {
        let { store, saveStateData } = this.props;
        let { newOrder, products } = store;
        let product = products.find(t => t.id == newOrder.productId);
        newOrder.bulk_purchase = false;
        newOrder.quantity_ordered = value;
        
        // console.log("OBSERVING product", product);
        // console.log("OBSERVING value", value);

        value = parseInt(value);
        let stock_available = parseInt(product.stock_available);

        // console.log("ASSERTION", value === stock_available);

        if(value) {
            newOrder.cost_price = value * product.price_per_unit;
        }

        if(value == stock_available) {
            newOrder.bulk_purchase = true;
            newOrder.cost_price = product.bulk_price;
        }
        
        saveStateData("newOrder", newOrder);
    }
    
    render() {
        let { store, saveStateData } = this.props;
        let { newOrder, products, cartOrder=[], appActions } = store;
        let product = products.find(t => t.id == newOrder.productId) || {};
        let { bulk_price, description, product_name, price_per_unit, product_image, stock_available } = product;
        let { createOrder } = appActions;


        let quantity_ordered = newOrder.quantity_ordered;
        if(newOrder.bulk_purchase === true) {
            quantity_ordered = quantity_ordered + " (bulk purchase)";
        }

        // console.log("APP STATUS", appActions);

        let statusMessage = "";
        let active = false;
        if(createOrder.status === 'pending') {
            active = true;
            statusMessage = createOrder.statusMessage;
            // console.log("PENDING STATUS", createOrder.status);
        }

        return (
            <View style={{flex: 1}}>
                <StatusBar backgroundColor="#007542"/>
                <ScreenHeading>
                    <Text style={styles.headingText}>Product Request</Text>
                </ScreenHeading>
                <View style={styles.container}>
                    <View style={styles.productContainer}>
                        <View style={styles.productImage}>
                            <ResponsiveImage
                                style={{
                                    width: deviceWidth,
                                    height: 200
                                }}
                                source={product_image}
                                initWidth={deviceWidth} 
                                initHeight={200} />
                        </View>
                        <View style={styles.productInfo}>
                            <View style={styles.productInfoLabel}>
                                <Text style={[styles.ProductInfoLabelText]}>Product: </Text>
                                <Text style={[styles.ProductInfoValueText]}>{ product_name }</Text>
                            </View>
                            <View style={styles.productInfoLabel}>
                                <Text style={[styles.ProductInfoLabelText]}>Quantity Available: </Text>
                                <Text style={[styles.ProductInfoValueText]}>  { stock_available} </Text>
                            </View>
                            <View style={styles.productInfoLabel}>
                                <Text style={[styles.ProductInfoLabelText]}>Quantity Demanded: </Text>
                                <Text style={[styles.ProductInfoValueText]}>  { quantity_ordered} </Text>
                            </View>
                            <View style={styles.productInfoLabel}>
                                <Text style={[styles.ProductInfoLabelText]}>Price Per Unit: </Text>
                                <Text style={[styles.ProductInfoValueText]}>  { price_per_unit} </Text>
                            </View>
                            <View style={styles.productInfoLabel}>
                                <Text style={[styles.ProductInfoLabelText]}>Order Cost: </Text>
                                <Text style={[styles.ProductInfoValueText]}>  { newOrder.cost_price} </Text>
                            </View>
                            
                            <TextInput
                                value={newOrder.quantity_ordered}
                                onChangeText={(value) => this.saveInput(value)}
                                style={styles.input}
                                keyboardType={"numeric"}
                                placeholder='Quantity Demanded'
                                placeholderTextColor="green"
                                underlineColorAndroid="green"        
                            />
                            
                            <Touch style={styles.buttonContainer} action={() => this.addToCart(newOrder, product)}>
                                <Text style={styles.buttonText}> Add to Cart </Text>
                            </Touch>


                        </View>
                    </View>
    
                {/* <Toast text={statusMessage} active={active} /> */}
                </View>
            </View>

        );
    } 
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#347c3e',
        margin: 20,
        ...Platform.select({
            ios: {
              shadowColor: 'rgba(0,0,0, .7)',
              shadowOffset: { height:0, width: 0 },
              shadowOpacity: 1,
              shadowRadius: 3,
            },
            android: {
              elevation: 3
            }
        }),
    },
    headingText: {
        fontSize: 15,
        color: '#555',
        textAlign: 'center'
    },
    productInfo: {
        backgroundColor: 'ghostwhite',
        padding: 15
    },
    productInfoLabel: {
        flexDirection: 'row',
        paddingBottom: 10,

    },
    ProductInfoLabelText: {
        color: 'green',
        flex: 1,
    },
    ProductInfoValueText: {
        flex: 1,
        color: '#333'
    },
    buttonContainer: {
        backgroundColor: 'red',
        padding: 10,
        borderRadius: 10
    },
    buttonText: {
      textAlign: 'center',
      color: '#fff'
    }, 
    input: {
      backgroundColor: 'ghostwhite',
      flexDirection: 'row',
      marginBottom: 3,
      paddingBottom: 5,
      borderRadius: 20,
      paddingLeft: 0
  },
});
  