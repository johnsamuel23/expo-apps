import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, Platform } from 'react-native';
import LocalImage from './components/LocalImage';
import CartProduct from './CartProduct';
import StatusBar from './components/StatusBar';
import Touch from './components/common/Touch';

export default class CartProducts extends Component {

    checkOut() {
        let { navigator } = this.props;
        setTimeout(() => {
            navigator.push({name: 'product_payment_screen'});
        }, 1000);
    }

    render() {
        let { store } = this.props;
        let { products, cartOrders } = store;
        // console.log("Cart products", cartOrders);
        let cartProducts = [];

        cartProducts = cartOrders.map((item) => {
            let product = products.find(t => t.id === item.product_id);
            product = {...product, ...item};
            return product;
        });

        console.log("Cart products", cartProducts);
        
        
        
        const cartproducts = cartProducts.map((item, index) => {
            return <CartProduct
                    key={index}
                    product_name={item.product_name}
                    quantity={item.quantity_ordered}
                    total_cost={item.cost_price} 
                    product_image={item.product_image}
                    />
        });  
           
        return (
            <View style={{flex: 1}}>
                <StatusBar backgroundColor="#007542"/>
                <ScrollView style={[styles.container, {padding: 15}]}>
                    { cartproducts }     
                </ScrollView>
                
                <View style={styles.actionButton}>
                    <Touch action={() => this.checkOut()}>
                        <View style={[styles.buttonContainer]}>
                            <Text style={[styles.buttonText]}> Continue </Text>
                        </View>
                    </Touch>
                </View>
            </View>
        );
        
    }
}            

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    buttonContainer: {
        backgroundColor: "#ff5722",
        height: 45,
        margin: 15,
        justifyContent: 'center',
        borderRadius: 3,
    },
    shadow: {
        ...Platform.select({
            ios: {
              shadowColor: 'rgba(0,0,0, .7)',
              shadowOffset: { height:0, width: 0 },
              shadowOpacity: 1,
              shadowRadius: 3,
            },
            android: {
              elevation: 2
            }
        }),
    },
    centralised: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    checkOutButton: {
        backgroundColor: "#ff5722",
        height: 50,
    },
    buttonText: {
        color: "white",
        fontSize: 15,
        fontWeight: 'bold',
        textAlign: "center"
    }
})
