import axios from 'axios';

export let siteUrl = process.env.NODE_ENV === 'production' ? 'http://umbrellatvproductions.com' : 'http://192.168.107.1:8080';

// demo online test
// siteUrl = 'http://umbrellatvproductions.com';

export const siteRoot = siteUrl + "/agrostore/app/controllers";

export const storageRoot = siteUrl + "/agrostore/app/Storage/uploads/";

export const getProducts = () => (
    axios.get(siteRoot + "/ProductController.php/?action=list&fields=products.id, products.name, product_interests.name as interest_name, description, quantity_stocked, quantity_sold, price_per_unit, price_bulk_total, source_location, product_image_name")
);

export const getProductInterests = () => (
    axios.get(siteRoot + "/ProductInterestController.php/?action=list")
);

export const uploadProduct = data => {
    let { 
        interest_id, 
        name, 
        quantity_stocked, 
        quantity_sold, 
        price_per_unit, 
        price_bulk_total, 
        creator_user_id, 
        source_location, 
        image, 
    } = data;

    let formData = new FormData();
    formData.append('image', { uri: image.uri, name: image.name, type: image.type });
    let end_point = siteRoot + "/ProductController.php/?action=create";

    const config = {
        headers: { 'content-type': 'multipart/form-data' }
    }

    let requestParameter = "";
    requestParameter = requestParameter + "&interest_id=" + interest_id;
    requestParameter = requestParameter + "&name=" + name;
    requestParameter = requestParameter + "&quantity_stocked=" + quantity_stocked;
    requestParameter = requestParameter + "&quantity_sold=" + quantity_sold;
    requestParameter = requestParameter + "&price_per_unit=" + price_per_unit;
    requestParameter = requestParameter + "&price_bulk_total=" + price_bulk_total;  
    requestParameter = requestParameter + "&creator_user_id=" + creator_user_id;  
    requestParameter = requestParameter + "&source_location=" + source_location;  

    end_point = end_point + requestParameter;
    console.log("END POINT", end_point);

    return axios.post(end_point, formData, config);
}

export const createOrder = (data) => {
    // data.user_id,
    // data.product_id
    // data.cart_id if set
    // order_location_id,
    // quantity_purchased
    // cost_price

    // console.log("DATA", data);

    let requestParameter = "";
    requestParameter = requestParameter + "&user_id=" + data.user_id;
    requestParameter = requestParameter + "&product_id=" + data.product_id;
    requestParameter = requestParameter + "&order_location_id=" + data.order_location_id;
    requestParameter = requestParameter + "&quantity_purchased=" + data.quantity_purchased;
    requestParameter = requestParameter + "&cost_price=" + data.cost_price;
    requestParameter = requestParameter + "&cart_id=" + data.cart_id;    

    let request = siteRoot + "/OrderController.php/?action=create" + requestParameter;
    console.log("REQUEST", request);
    return axios.get(request);
}

export const checkoutCart = (data) => {

    let requestParameter = "";
    requestParameter = requestParameter + "&user_id=" + data.user_id;
    requestParameter = requestParameter + "&cart_id=" + data.cart_id;
    requestParameter = requestParameter + "&total_pay=" + data.total_pay; 

    let request = siteRoot + "/CartController.php/?action=checkout" + requestParameter;
    console.log("REQUEST", request);
    return axios.get(request);
}

export const userLogin = (data) => {

    let requestParameter = "";
    requestParameter = requestParameter + "&email=" + data.email;
    requestParameter = requestParameter + "&password=" + data.password;

    let request = siteRoot + "/UserController.php/?action=login" + requestParameter;
    console.log("REQUEST", request);
    return axios.get(request);
}

export const createUser = (userData) => {
    return axios.get(siteRoot + "/UserController.php/?action=create", userData)
}

export const registerUser = (data) => {

    let requestParameter = "";
    requestParameter = requestParameter + "&lastname=" + data.lastname;
    requestParameter = requestParameter + "&firstname=" + data.firstname;
    requestParameter = requestParameter + "&contact=" + data.contact;
    requestParameter = requestParameter + "&address=" + data.address;
    requestParameter = requestParameter + "&password=" + data.password;  
    requestParameter = requestParameter + "&username=" + data.username;  
    requestParameter = requestParameter + "&primary_location=" + data.primary_location;  
    requestParameter = requestParameter + "&user_type=" + data.user_type;  
    requestParameter = requestParameter + "&email=" + data.email;  

    let request = siteRoot + "/UserController.php/?action=create" + requestParameter;
    console.log("REQUEST", request);

    const config = {
        responseType: 'json'
    }

    return axios.get(request);
}