import React, { Component } from 'react';
import { StyleSheet, Text, View, Dimensions } from 'react-native';
import * as Animatable from 'react-native-animatable';

export default class Toast extends Component {

    state = {
        animation: "fadeInUp"
    }

    componentDidMount() {
        this.setState({
            animation: this.props.active ? "fadeInUp" : "fadeOutDown"
        });

        // _this = this;
        // setTimeout(() => {
        //     _this.setState({
        //         animation: "fadeOutDown"
        //     });
        // }, 5000);
    }

    componentWillReceiveProps() {
        this.setState({
            animation: this.props.active === true ? "fadeInUp" : "fadeOutDown"
        });
    }

    render() {
        return (
            <Animatable.View 
              animation={this.state.animation}
              duration={500}
              style={styles.container}>
                <View style={styles.container}>
                    <Text style={styles.text}>{this.props.text}</Text>
                </View>
            </Animatable.View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        height: 50,
        position: 'absolute',
        bottom: 0,
        width: Dimensions.get('window').width,
        backgroundColor: '#555',
        justifyContent: 'center',
        alignItems: 'center'
    },
    text: {
        color: "#ffffff",
        fontSize: 15,
        textAlign: 'center'
    }
});