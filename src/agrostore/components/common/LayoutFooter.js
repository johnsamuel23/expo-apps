import React from 'react';
import { View, StyleSheet } from 'react-native';

export default Component = ({
    children,
    style
}) => {
    return (
        <View style={[styles.container, style]}>
            { children }
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        minHeight: 50,
        borderTopColor: '#f1f1f1',
        borderTopWidth: 1,
        backgroundColor: 'white',
    }
});