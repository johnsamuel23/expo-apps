import React from 'react';
import { View, StyleSheet } from 'react-native';

export default Component = ({
    size
}) => {
    let { container} = extraStyles;

    size = size ? size : 20;
    extraStyles.container.width = size;

    return (
        <View style={[styles.container, extraStyles.container]}>
        </View>
    );
}

let extraStyles = {
    container: {},
};

const styles = StyleSheet.create({
    container: {
        width: 20
    },
});