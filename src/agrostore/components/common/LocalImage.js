import React, { Component } from 'react';
import {
    Image,
    Dimensions
} from 'react-native';

const LocalImage = ({
    source,
    originalWidth,
    originalHeight,
    guideWidth,
}) => {
    let observedWidth = Dimensions.get('window').width;

    if(guideWidth !== undefined) {
        observedWidth = guideWidth;
    } 
    
    let widthChange = observedWidth / originalWidth;
    let newWidth = originalWidth * widthChange;
    let newHeight = originalHeight * widthChange;
    return (
        <Image 
            source={source} 
            style={{
                width: newWidth,
                height: newHeight
            }}
        />
    )
}

export default LocalImage;