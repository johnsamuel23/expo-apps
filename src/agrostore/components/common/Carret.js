import React from 'react';
import { StyleSheet, View } from 'react-native';
import LocalImage from '../common/LocalImage';
import Menu from '../../utils/icons/Menu';

export default Component = () => {
    return (
        <View style={styles.icon}>
            <Menu color="#444" size={30} />
        </View>
    )
}

const styles = StyleSheet.create({
    icon: {
        height: 45,
        width: 45,
        justifyContent: 'center',
    }
});