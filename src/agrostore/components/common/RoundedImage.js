import React from 'react';
import { StyleSheet, View } from 'react-native';
import LocalImage from '../common/LocalImage';

export default Component = ({source, imageSize, height, style}) => {
    imageSize = imageSize ? imageSize : 30;
    height = height ? height : 45;
    let width = height;
    let borderRadius = width / 2;

    return (
        <View style={[styles.container, {height, width, borderRadius}, style]}>
            <LocalImage
                originalHeight={500}
                originalWidth={500}
                guideWidth={imageSize}
                source={source}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        height: 45,
        width: 45,
        justifyContent: 'center',
        alignItems: 'center',
    }
});