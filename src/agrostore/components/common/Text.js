import React from 'react';
import { Text, StyleSheet } from 'react-native';

export default Component = ({
    children,
    style
}) => {
    return (
        <Text style={[styles.text, style]}>
            { children }
        </Text>
    );
}

const styles = StyleSheet.create({
    text: {
        // fontFamily: 'lato-regular',
        fontSize: 15,
        color: '#1f1f1f'
    }
});