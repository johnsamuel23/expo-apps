import React, { Component } from 'react';
import { 
  StyleSheet, 
  Text, 
  View, 
  ScrollView, 
  Image , 
  TextInput,
  TouchableOpacity,
  Picker
} from 'react-native';
// import DropDown, {
//   Select,
//   Option,
//   OptionList,
// } from 'react-native-selectme';
// import SelectMultiple from 'react-native-select-multiple';
import LocalImage from '../components/LocalImage';
import styles from '../style';

const fruits = ['Apples', 'Oranges', 'Pears']
// --- OR ---
// const fruits = [
//   { label: 'Apples', value: 'appls' },
//   { label: 'Oranges', value: 'orngs' },
//   { label: 'Pears', value: 'pears' }
// ]



export default class RegistrationCustomerScreen extends Component {
  componentWillMount(){
    this.state = {
        language: 'java'
    }
  }

  state = { selectedFruits: [] }

  onSelectionsChange = (selectedFruits) => {
    // selectedFruits is array of { label, value }
    this.setState({ selectedFruits })
  }

  render() {

    return (
      <View>
        <Text>Registration Screen Next</Text>
        <Text>{ this.props.userInfo.userType}</Text>
      </View>
    );
  }
}

