import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  KeyboardAvoidingView,
  Image,
  TextInput,
  TouchableOpacity,
  findNodeHandle,
  KeyboardAwareScrollView,
  Animated,
  Keyboard
} from 'react-native';
import LocalImage from '../components/LocalImage';
// import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import styles from '../style';

const IMAGE_HEIGHT = 150;
const IMAGE_HEIGHT_SMALL = 100;

export default class Registration extends Component {

  constructor(props) {
    super(props);

    this.keyboardHeight = new Animated.Value(0);
    this.imageHeight = new Animated.Value(IMAGE_HEIGHT);
  }

  componentWillMount() {
    this.keyboardWillShowSub = Keyboard.addListener('keyboardDidShow', this.keyboardDidShow);
    this.keyboardWillHideSub = Keyboard.addListener('keyboardDidHide', this.keyboardDidHide);
  }

  componentWillUnmount() {
    this.keyboardWillShowSub.remove();
    this.keyboardWillHideSub.remove();
  }

  keyboardDidShow = (event) => {
    Animated.parallel([
      Animated.timing(this.keyboardHeight, {
        duration: event.duration,
        toValue: event.endCoordinates.height,
      }),
      Animated.timing(this.imageHeight, {
        duration: event.duration,
        toValue: IMAGE_HEIGHT_SMALL,
      }),
    ]).start();
  };

  keyboardDidHide = (event) => {
    Animated.parallel([
      Animated.timing(this.keyboardHeight, {
        duration: 500,
        toValue: 0,
      }),
      Animated.timing(this.imageHeight, {
        duration: 500,
        toValue: IMAGE_HEIGHT,
      }),
    ]).start();
  };

  handleUserSelect(userType) {
    // userType can be 'customer' or 'merchant'
    this.updateUserSelection(userType);
    this.props.navigator.push({name: 'registration_customer_screen__'});
  }

  updateUserSelection(userType) {
    this.props.updateUser(userType);
  }

  render() {
    return (
      <Animated.View style={[styles.container, { paddingBottom: this.keyboardHeight }]}>
        <View style={styles.header}>
          <View>
            <LocalImage
              source={require('../img/logo.png')}
              originalWidth={500}
              originalHeight={578}
              guideWidth={80}
            />
          </View>
          <View>
            <Text style={styles.HeaderText}> AgroMass</Text>
          </View>
        </View>
        <View style={styles.body}>
          <View>
            <Text style={styles.SignupText}>Sign Up</Text>
          </View>
          <View style={styles.formArea}>
            <View style={styles.formInputs}>
              <TextInput
                style={styles.input}
                placeholder="Name"
                placeholderTextColor="rgba(0,0,0,0.5)"
                returnKeyType="next"
                autoCapitalise="none"
                autoCorrect={false}
                underlineColorAndroid="transparent"
              />

              <TextInput
                style={styles.input}
                placeholder="email"
                placeholderTextColor="rgba(0,0,0,0.5)"
                keyboardType="email-address"
                autoCapitalise="none"
                autoCorrect={false}
                underlineColorAndroid="transparent"
              />

              <TextInput
                style={styles.input}
                placeholder="Phone Number"
                placeholderTextColor="rgba(0,0,0,0.5)"
                returnKeyType="next"
                autoCapitalise="none"
                autoCorrect={false}
                underlineColorAndroid="transparent"
              />
              
            {/*
              <TextInput
                style={styles.input}
                placeholder="Name"
                placeholderTextColor="rgba(0,0,0,0.5)"
                returnKeyType="next"
                autoCapitalise="none"
                autoCorrect={false}
                underlineColorAndroid="transparent"
              />

              <TextInput
                style={styles.input}
                placeholder="email"
                placeholderTextColor="rgba(0,0,0,0.5)"
                keyboardType="email-address"
                autoCapitalise="none"
                autoCorrect={false}
                underlineColorAndroid="transparent"
              />

              <TextInput
                style={styles.input}
                placeholder="Phone Number"
                placeholderTextColor="rgba(0,0,0,0.5)"
                returnKeyType="next"
                autoCapitalise="none"
                autoCorrect={false}
                underlineColorAndroid="transparent"
              />

              <TextInput
                style={styles.input}
                placeholder="Name"
                placeholderTextColor="rgba(0,0,0,0.5)"
                returnKeyType="next"
                autoCapitalise="none"
                autoCorrect={false}
                underlineColorAndroid="transparent"
              />

              <TextInput
                style={styles.input}
                placeholder="email"
                placeholderTextColor="rgba(0,0,0,0.5)"
                keyboardType="email-address"
                autoCapitalise="none"
                autoCorrect={false}
                underlineColorAndroid="transparent"
              />

              <TextInput
                style={styles.input}
                placeholder="Phone Number"
                placeholderTextColor="rgba(0,0,0,0.5)"
                returnKeyType="next"
                autoCapitalise="none"
                autoCorrect={false}
                underlineColorAndroid="transparent"
              />

              <TextInput
                style={styles.input}
                placeholder="Name"
                placeholderTextColor="rgba(0,0,0,0.5)"
                returnKeyType="next"
                autoCapitalise="none"
                autoCorrect={false}
                underlineColorAndroid="transparent"
              />

              <TextInput
                style={styles.input}
                placeholder="email"
                placeholderTextColor="rgba(0,0,0,0.5)"
                keyboardType="email-address"
                autoCapitalise="none"
                autoCorrect={false}
                underlineColorAndroid="transparent"
              />

              <TextInput
                style={styles.input}
                placeholder="Phone Number"
                placeholderTextColor="rgba(0,0,0,0.5)"
                returnKeyType="next"
                autoCapitalise="none"
                autoCorrect={false}
                underlineColorAndroid="transparent"
              /> 

              */}

            </View>

            <View style={styles.category}>
              <Text style={styles.selectCategoryText}>
                Are you a Customer or a Merchant?
              </Text>
            </View>

            <View style={styles.buttons}>
              <View style={styles.Left}>
                <TouchableOpacity style={[styles.buttonContainer]} onPress={() => this.handleUserSelect('Customer')}>
                  <Text style={styles.buttonText}> Customer </Text>
                </TouchableOpacity>
              </View>
              <View style={styles.Right}>
                <TouchableOpacity style={[styles.buttonContainer]} onPress={() => this.handleUserSelect('Merchant')}>
                  <Text style={styles.buttonText}> Merchant </Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
      </Animated.View>
    );
  }
}

