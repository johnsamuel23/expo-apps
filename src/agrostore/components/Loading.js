import React from 'react';
import {
    View,
    Text,
    ActivityIndicator,
    StyleSheet
} from 'react-native';

const Loading = ({
    text,
}) => {
    
    return (
        <View style={styles.waitingContainer}>
            <ActivityIndicator />
            <Text style={styles.waitingText}>{text}</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    waitingContainer: {
        flexDirection: 'row',
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 20
    },
    waitingText: {
        fontSize: 15,
        color: '#555',
        marginLeft: 5
    }
});

export default Loading;