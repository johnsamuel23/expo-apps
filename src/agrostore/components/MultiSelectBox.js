import React, { Component } from 'react';
import { StyleSheet, View, TouchableOpacity, ScrollView } from 'react-native';
import Text from './common/Text';
import SelectDrop from '../utils/icons/SelectDrop';
import Modal from 'react-native-modal';
import Touch from './common/Touch';

export default class MultiSelectBox extends Component {
    
    state = {
        isModalVisible: false
    }
    
    _showModal = () => this.setState({ isModalVisible: true })

    _hideModal = () => this.setState({ isModalVisible: false })

    getSelectionStatus(selectedOptions, optionId) {
        
        let selectedOption = selectedOptions.find(opt => opt === optionId) || 0;

        console.log("selectedOptions", selectedOptions);

        if(selectedOption === optionId) {
            return true;
        }

        return false;
    }

    render() {
        let { style, selectedOptionIds, handleSelect, options, text, selectedItemStyle, unselectedItemStyle} = this.props;
        
        console.log("SelectedOption == Ids", selectedOptionIds);

        let optionList = options.map((option, index) => {
            let id = option.id;
            let boxStyle = {};
            let isSelected = this.getSelectionStatus(selectedOptionIds, id);
            boxStyle = isSelected ? selectedItemStyle : unselectedItemStyle;
            return (
                <Touch key={index} action={() => {handleSelect(id)}}>
                    <View style={[styles.optionContainer, {backgroundColor: boxStyle.backgroundColor}]}>
                        <Text style={[styles.optionText, {color: boxStyle.textColor}]}> {option.value} </Text>
                    </View>
                </Touch>
            )
        });
        
        return (
            <View>
                <TouchableOpacity
                    activeOpacity={0.6}
                    style={[styles.container, style.boxStyle]}
                    onPress={() => this._showModal()}
                >
                    <View style={styles.label}>
                        <Text style={styles.labelText}>{text}</Text>
                    </View>
                    <View style={styles.dropIcon}>
                        <SelectDrop color="#777" size={20} />
                    </View>
                </TouchableOpacity>
                <Modal 
                    style={{margin: 20, borderRadius: 10}}
                    isVisible={this.state.isModalVisible}>
                    <View style={{ flex: 1 }}>
                        <TouchableOpacity
                            style={{backgroundColor: '#f2f2f2', padding: 10, marginBottom: 10, }}
                            activeOpacity={1}
                            onPress={() => this._hideModal()}>
                            <Text style={{textAlign: 'center'}}>Done</Text>
                        </TouchableOpacity>
                        <ScrollView>

                            { optionList }

                        </ScrollView>
                    </View>
                </Modal>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        height: 45,
        justifyContent: 'center',
        borderRadius: 3,
        borderWidth: 1,
        borderColor: '#777',
        paddingLeft: 10,
        paddingRight: 10
    },
    label: {
        flex: 1,
        justifyContent: 'center',
    },
    labelText: {
        // fontFamily: 'lato-regular'
    },
    dropIcon: {
        alignItems: 'flex-end',
        justifyContent: 'center',        
        width: 50
    },
    optionContainer: {
        padding: 10, 
        borderTopWidth: 1, 
        borderTopWidth: 1, 
        borderTopColor: '#f1f1f1',
        backgroundColor: '#fff'
    },
    optionText: {
        // fontFamily: 'lato-regular'
    }
});