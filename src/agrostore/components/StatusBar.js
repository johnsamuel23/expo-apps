import React from 'react';
import { View, StyleSheet } from 'react-native';
import { Constants } from 'expo';

export default StatusBar = ({
    backgroundColor
}) => {
    return (
        <View style={[styles.statusBar, {backgroundColor}]}></View>
    )
}

const styles = StyleSheet.create({
    statusBar: {
        height: Constants.statusBarHeight
    }
});