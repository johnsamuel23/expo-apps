import React, { Component } from 'react';
import { StyleSheet, Text, View  } from 'react-native';
import Back from '../utils/icons/Back';
import Heart from '../utils/icons/Heart';
import More from '../utils/icons/More';
import Share from '../utils/icons/Share';

export default class UtilityNavButton extends Component {
    render() {
        switch (this.props.icon) {
            case "Back":
                return <Back />
            case "Heart":
                return <Heart />
            case "Share":
                return <Share/>
            case "More":
                return <More/>
            default:
                return "No Icon"
        }
    }
}