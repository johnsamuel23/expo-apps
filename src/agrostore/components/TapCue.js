import React, { Component } from 'react';
import { 
  StyleSheet, 
  Text, 
  View, 
  Animated,
  Easing,
  UIManager
} from 'react-native';
import LocalImage from '../components/LocalImage';

export default class TapCue extends Component {

    componentWillMount() {
        this.opacityValue = new Animated.Value(0);
    }

    componentDidMount() {
        this.opacityAnim();
    }

    opacityAnim() {
        this.opacityValue.setValue(0);
        Animated.timing(
            this.opacityValue,
            {
                toValue: 1,
                duration: 2000,
                easing: Easing.easeOutBack
            }
        ).start(() => {
            this.opacityAnim();
        });
    }

    render() {

        const opacityValue = this.opacityValue.interpolate({
            inputRange: [0, 0.4, 0.7, 1],
            outputRange: [0, 1, 1, 0]
        }); 

        return (
            <Animated.View style={[styles.tapCue, {opacity: opacityValue}]}>
                <Text style={styles.tapCueText}>Tap screen to continue</Text>
            </Animated.View>
        )
    }
}

const styles = StyleSheet.create({
    tapCue: {
        backgroundColor: 'rgba(0,0,0,0.5)',
        padding: 10,
        borderRadius: 20,
        alignSelf: 'flex-end',
        marginBottom: 150
    },
    tapCueText: {
        textAlign: 'center',
        fontSize: 12,
        color: '#fff'
    },
});
