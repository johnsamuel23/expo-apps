import React from 'react';
import { StyleSheet, Text, View, Image, Dimensions } from 'react-native';
import LocalImage from './LocalImage';
import ResponsiveImage from 'react-native-responsive-image';

export default class Product extends React.Component {
  render() {
    let deviceWidth = Dimensions.get('window').width;
    return (
        <View style={styles.productInfo}>
            <View style={styles.productInfoLabel}>
                <Text style={[styles.productInfoLabelText]}>Product Name: </Text> 
                <Text style={[styles.productInfoValueText]}> { this.props.product_name } </Text>
            </View>
            <View style={styles.productInfoLabel}>
                <Text style={[styles.productInfoLabelText]}>Stock Available: </Text> 
                <Text style={[styles.productInfoValueText]}> { this.props.stock_available }</Text>
            </View>
            {<View style={styles.productInfoLabel}>
                <Text style={[styles.productInfoLabelText]}>From: </Text>
                <Text style={[styles.productInfoValueText]}> { this.props.source_location }</Text>
            </View>}
            {/* <View style={styles.productInfoLabel}>
                <Text style={[styles.productInfoLabelText]}>Uploaded On: </Text>
                <Text style={[styles.productInfoValueText]}> { this.props.date_created } </Text>
            </View> */}
            <View style={styles.productInfoLabel}>
                <Text style={[styles.productInfoLabelText]}>Price Per Unit (Naira): </Text>
                <Text style={[styles.productInfoValueText]}> { this.props.price_per_unit } </Text>
            </View>
            <View style={styles.productInfoLabel}>
                <Text style={[styles.productInfoLabelText]}>Bulk Price (Naira): </Text>
                <Text style={[styles.productInfoValueText]}> { this.props.bulk_price }</Text>
            </View>
            {/* <View style={styles.productInfoLabel}>
                <Text style={[styles.productInfoLabelText]}>Shipping Location: </Text>
                <Text style={[styles.productInfoValueText]}> { this.props.shipping_location } </Text>
            </View> */}
        </View>  
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#16794a',
    // marginBottom: 20,
    borderBottomWidth: 1,
    borderBottomColor: 'rgb(100,100,100)'
  },
  productImage: {
    //   alignSelf: 'center'
  },
  productInfo: {
      backgroundColor: 'white',
      padding: 10
  },
  productInfoLabel: {
      flexDirection: 'row',
      paddingBottom: 5
  },
  productInfoLabelText: {
    color: '#555',
    flex: 1
  },
  productInfoValueText: {
    flex: 1,
    color: '#333',
  }
  
 
});
