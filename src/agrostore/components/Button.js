import React from 'react';
import { View, StyleSheet } from 'react-native';
import Text from './common/Text';

export default Button = ({
    text,
    thick,
}) => {
    let { buttonStyles, textStyles } = extraStyles;

    if(thick) {
    
        textStyles = { 
            color: 'white',
        }

        return (
            <View>
                <View style={[styles.buttonContainer]}>
                    <Text style={[styles.text, textStyles]}> {text} </Text>
                </View>
                <View style={[styles.dropShadow]}>
                </View>
            </View>
        );

    }

    return (
        <View style={styles.container}>
            <View style={[styles.buttonContainer, buttonStyles]}>
                <Text style={[styles.text, textStyles]}> {text} </Text>
            </View>
        </View>
    );
}

let extraStyles = {
    buttonContainer: {},
    textStyle: {}
};

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    buttonContainer: {
        backgroundColor: 'white',
        borderWidth: 1,
        borderColor: theme.brandColor,
        justifyContent: 'center',
        alignItems: 'center',
        height: theme.buttonHeight,
        borderRadius: 3,
        zIndex: 2
    },
    text: {
        color: theme.brandColor
    },
    dropShadow: {
        backgroundColor: theme.darkButtonShadow,
        height: theme.buttonHeight + 3,
        borderRadius: 3,        
        zIndex: 1,
        marginTop: -45,
    }
});