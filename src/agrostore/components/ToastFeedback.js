import React, { Component } from 'react';
import {
    Image,
    Dimensions
} from 'react-native';
import * as Animatable from 'react-native-animatable';

const ToastFeedback = ({
   opacity,
   message
}) => {
    
    return (
        <Animatable.View 
        transition={['opacity']} 
        style={{justifyContent: 'center', 
        alignItems: 'center', 
        position: 'absolute',
        backgroundColor: '#222222', 
        justifyContent: 'center', 
        height: 60,
        overflow: 'hidden',
        bottom: 0,
        left: 0,
        right: 0,
        opacity: opacity || 0,
        width: Dimensions.get('window').width,
    }}>
        <Animatable.Text style={{textAlign: 'center', padding: 10,
                color: 'white', fontFamily: 'lato-regular'}}>
            {message}
        </Animatable.Text>
    </Animatable.View>
    )
}

export default ToastFeedback;