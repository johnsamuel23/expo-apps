import React from 'react';
import { StyleSheet, Text, View, Image, Dimensions } from 'react-native';
import LocalImage from './LocalImage';
import ResponsiveImage from 'react-native-responsive-image';
import ProductInfo from './ProductInfo';
import Touch from './common/Touch';

export default class Product extends React.Component {
  
  render() {
    let deviceWidth = Dimensions.get('window').width;
    return (
      <View style={styles.container}>
        <View style={styles.productContainer}>
            <View style={styles.productImage}>
              <ResponsiveImage
                  style={{
                      width: deviceWidth,
                      height: 200
                  }}
                  source={this.props.product_image}
                  initWidth={deviceWidth} 
                  initHeight={200} />
            </View>
            <ProductInfo {...this.props} />
            <Touch action={() => this.props.addToCart(this.props.product_id)}>
              <View style={styles.button}>
                <Text style={styles.buttonText}>Add to Cart</Text>
              </View>
            </Touch>
        </View>
      </View>  
    );
  }
}

const styles = StyleSheet.create({
  container: {
    // backgroundColor: '#16794a',
    backgroundColor: '#fff',
    // marginBottom: 20,
    borderBottomWidth: 1,
    borderBottomColor: 'rgb(100,100,100)'
  },
  button: {
    backgroundColor: "#e43137",
    padding: 10,
    margin: 10,
    width: 150,
    borderRadius: 3
  },
  buttonText: {
    color: "white",
    fontSize: 15,
    textAlign: "center"
  },
  productImage: {
    //   alignSelf: 'center'
  },
  productInfo: {
      backgroundColor: 'white',
      padding: 10
  },
  productInfoLabel: {
      flexDirection: 'row',
      paddingBottom: 5
  },
  productInfoLabelText: {
    color: '#555',
    flex: 1
  },
  productInfoValueText: {
    flex: 1,
    color: '#333',
  }
  
 
});
