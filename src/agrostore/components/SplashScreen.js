import React, { Component } from 'react';
import { 
  StyleSheet, 
  Text, 
  View, 
  ScrollView, 
  Image , 
  TextInput,
  TouchableOpacity,
  Animated,
  Easing,
  LayoutAnimation,
  UIManager
} from 'react-native';
import LocalImage from '../components/LocalImage';
import styles from '../style';

export default class SplashScreen extends Component {

  componentWillMount() { 
    this.spinValue = new Animated.Value(0);
    this.scaleValue = new Animated.Value(0);
    this.heightValue = new Animated.Value(0);
  }

  spin() {
    this.spinValue.setValue(0);
    Animated.timing(
        this.spinValue,
        {
            toValue: 1,
            duration: 1500,
            easing: Easing.linear
        }
    ).start(() => {
        this.spin();    
    });
  }

  scale() {
    this.scaleValue.setValue(0);
    Animated.timing(
        this.scaleValue,
        {
            toValue: 1,
            duration: 1800,
            easing: Easing.easeOutBack
        }
    ).start(() => {
        this.scale();
    });
  }

  height() {
    this.heightValue.setValue(0);
    Animated.timing(
        this.heightValue,
        {
            toValue: 1,
            duration: 1000,
            easing: Easing.easeInOut
        }
    ).start(() => {
        this.height();
    });
  }

  render() {
    const spin = this.spinValue.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '360deg']
    });

    const nearFar = this.scaleValue.interpolate({
      inputRange: [0, 0.5, 1],
      outputRange: [0.6, 0.7, 0.6]
    });

    const highLow = this.heightValue.interpolate({
      inputRange: [0, 0.5, 1],
      outputRange: [0, 20, 0]
    });

    this.scale();
    // this.spin();
    this.height();

    return (
      <View style={styles.container}>
        <View style={styles.splashBody}>
            <View>
                <View style={styles.splashLogo}>
                    {/* <LocalImage 
                        source={require('../img/logo.png')} 
                        originalWidth={500}
                        originalHeight={578}
                        guideWidth={150}
                    /> */}

                    <Animated.Image 
                        style={[
                            {
                                width: 150,
                                height: 170,
                                marginBottom: highLow,
                            }, 
                            {
                                transform: [
                                    {scale: nearFar},
                                    {rotate: spin}
                                ]
                            }
                        ]}
                        source={require('../img/logo.png')}
                    />

                </View>
            </View>
            <View style={styles.splashTextArea}>
                <View style={styles.SplashTextDiv}>
                    <Text style={styles.SplashText}> AgroMass</Text>
                </View>
                <View style={[styles.SplashTextDiv, styles.SplashMottoDiv]}>
                    <Text style={styles.SplashTextMotto}> Agricultural Products for all</Text>
                </View>
            </View>
        </View>
        <View style={[styles.SplashTextDiv, styles.SplashMottoDiv]}>
            <Text style={[styles.SplashTextMotto, {fontSize: 12}]}> Made with <Text style={styles.LoveCaption}>Love</Text> in Nigeria</Text>
        </View>
      </View>
    );
  }
}

