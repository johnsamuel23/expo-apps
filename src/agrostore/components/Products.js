import React from 'react';
import { StyleSheet, Text, View, Image, ScrollView, Dimensions, ActivityIndicator, Platform } from 'react-native';
import Product from './Product';
import StatusBar from './StatusBar';
import { theme } from '../theme';
import Loading from './Loading';
import Cart from '../utils/icons/Cart';
import Touch from '../components/common/Touch';

export default class Products extends React.Component {

  componentWillMount() {
    this.state = {
    }
  }

  addToCart(productId) {
    // console.log("product_id", productId);
    let { store, navigator, saveStateData } = this.props;
    let newOrder = {
      productId,
    }
    saveStateData("newOrder", newOrder);
    // console.log("newOrder", newOrder);
    navigator.push({name: "product_request_screen"});
  }
  
  render() {
    const { store, navigator } = this.props;
    let { products } = store;

    const productList = products.map((item, index) => {
      return <Product 
                addToCart={(productId) => this.addToCart(productId)}
                saveStateData={this.props.saveStateData}
                key={item.id} 
                product_id={item.id}
                product_name={item.product_name}
                stock_available={item.stock_available}
                source_location={item.source_location}
                date_created={item.date_created}
                price_per_unit={item.price_per_unit}
                bulk_price={item.bulk_price}
                shipping_location={item.shipping_location}
                product_image={item.product_image}
              />
    });

    const waitingContainer = (
      <Loading text="fetching products" />
    );

    const CartIcon = <Cart size={35} color="green" />
    const { cartOrders } = store;
    let cartItemsNumber = cartOrders.length;

    return (
      <View style={{flex: 1}}>
        <StatusBar backgroundColor="#007542"/>
        <View style={styles.titleContainer}>
          <View style={styles.cart}></View>
          <View style={styles.headCenter}>
            <Text style={styles.titleText}>Available Products</Text>
          </View>
          <View style={styles.cart}> 
            <Touch action={() => navigator.push({name: 'cart_products_screen'})}>
              <View style={{flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
                <View style={styles.badge}><Text style={styles.badgeText}>{cartItemsNumber}</Text></View>
                { CartIcon }
              </View>
            </Touch>
          </View>
        </View>
        <ScrollView contentContainerStyle={styles.container}>
            {products.length < 1 && waitingContainer || productList }    
        </ScrollView>
      </View>  
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
  },
  badge: {
    backgroundColor: 'red',
    // padding: 2,
    height: 20,
    width: 20,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    paddingBottom: 3
  },
  badgeText: {
    textAlign: 'center',
    color: "white"
  },
  titleContainer: {
    // padding: 20,
    backgroundColor: "#fff",
    ...Platform.select({
      android: {
        elevation: 5
      }
    }),
    flexDirection: 'row',
    // justifyContent: 'center',
    height: 50
  },
  headCenter: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1
  },
  cart: {
    flexDirection: 'row',
    width: 70,
    height: 50,
    // backgroundColor: 'red',
    justifyContent: 'center',
    alignItems: 'center',
  },
  titleText: {
    color: theme.backgroundColor,
    textAlign: 'center',
    fontSize: 17,
    fontWeight: '800'
  },
  waitingContainer: {
    flexDirection: 'row',
    height: 100,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 20
  },
  waitingText: {
    fontSize: 15,
    color: '#555',
    marginLeft: 5
  }
});
