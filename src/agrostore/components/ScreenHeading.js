import React from 'react';
import { StyleSheet, View, Text } from 'react-native';

export default Header = ({
    children,
    backgroundColor
}) => {
    if(backgroundColor === undefined) {
        backgroundColor = '#fff'
    }
    
    return (
        <View style={[styles.container, {backgroundColor}]}>
            { children }
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        height: 50,
        borderBottomWidth: 1,
        borderBottomColor: '#eee',
        justifyContent: 'center'
    }
});