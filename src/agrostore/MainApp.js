import React, { Component } from 'react';
import { 
  StyleSheet, 
  Text, 
  View, 
  ScrollView, 
  Image , 
  TextInput,
  TouchableOpacity,
  BackHandler
} from 'react-native';
import RegistrationScreen from './components/Registration';
import RegistrationCustomerScreen from './components/RegistrationCustomerScreen';
import SplashScreen from './components/SplashScreen';
import SplashScreen_1 from './components/SplashScreen_1';
import { Navigator } from 'react-native-deprecated-custom-components';

export default class App extends Component {

  constructor() {
    super();
    this.state = {
      currentScreen: 'RegistrationScreen',
      userInfo: {}
    }
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', () => {
      if (this.navigator && this.navigator.getCurrentRoutes().length > 1) {
        this.navigator.pop();
        // BackHandler.exitApp();
        return true;
      }
      return false;
    });
  }

  renderScene(route, navigator) {
    this.navigator = navigator;

    switch(route.name) {
      case "splash_screen":
        return <SplashScreen_1 navigator={navigator} />;
      case "registration_screen":
        return <RegistrationScreen 
                navigator={navigator}
                updateUser={this.updateUser.bind(this)} />;
      case "registration_customer_screen":
        return <RegistrationCustomerScreen 
                navigator={navigator} 
                userInfo={this.state.userInfo} />;
      default:
        return <SplashScreen_1 navigator={navigator} />;
    }
  }

  updateUser(userType) {
    let userInfo = {...this.state.userInfo, userType: userType};

    this.setState({
       userInfo: userInfo
    })
  }

  render() {
    return (
      <View style={styles.container}> 
        <Navigator
            initialRoute={{name: "splash_screen"}}
            renderScene={this.renderScene.bind(this)}
            configureScene={(route, routeStack) => Navigator.SceneConfigs.FloatFromRight}
        />
      </View>
    );
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});
