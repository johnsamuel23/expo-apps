import React, { Component } from 'react';
import { 
  StyleSheet, 
  Text, 
  View, 
  ScrollView, 
  Image, 
  TextInput,
  TouchableOpacity,
  BackHandler,
  DrawerLayoutAndroid,
} from 'react-native';
import { Navigator } from 'react-native-deprecated-custom-components';
import Chat from './SampleChat';
import CartProducts from './CartProducts';
import ProductPayment from './ProductPayment';
import ProductRequest from './ProductRequest';
import Products from './components/Products';
import ProductSearch from './ProductSearch';
import ProductUpload from './ProductUpload';
import RegistrationScreen from './Registration';
import RegistrationCustomerScreen from './RegistrationCustomerScreen';
import RegistrationMerchantScreen from './RegistrationMerchantScreen';
import Registration from './Registration';
import Login from './Login';
import SplashScreen from './SplashScreen';
import ScreensMenu from './ScreensMenu';
import { AppLoading, Asset, Font } from 'expo';
import { getProductInterests, getProducts, siteUrl } from './api';

export default class App extends Component {

  constructor() {
    super();
    this.state = {
      currentScreen: 'splash_screen',
      userInfo: {
        name: "Jennifer Onyenwe",
        contact: "08012345678",
        address: "",
        username: "",
        primary_location: "",
        user_type: "",
        password: "",
        email: "onyenwejennifer@gmail.com",
        interest_ids: []
      },
      // userInfo: {
      //   user_id: 18,
      //   name: "",
      //   contact: "",
      //   address: "",
      //   username: "",
      //   primary_location: "",
      //   user_type: "",
      //   password: "",
      //   email: "",
      //   interest_ids: []
      // },
      productInterests: [],
      products: [],
      cartOrders: [
        // {product_id :123, quantity_ordered: 2, cost_price: 500}, 
        // {product_id :124, quantity_ordered: 2, cost_price: 500}, 
        // {product_id :126, quantity_ordered: 2, cost_price: 500}, 
        // {product_id :127, quantity_ordered: 2, cost_price: 500},
      ],
      selectedProductId: 123,
      selectedProductId: 0,
      newOrder: {
        productId: 127, quantity_ordered: "2"
      },
      currentCartId: 0,
      appActions: {
        createOrder: {
          status: "pending",
          statusMessage: "Creating Product Order"
        }
      }
    }
  }

  cacheImages = (images) => {
    return images.map(image => {
        if (typeof image === 'string') {
            return Image.prefetch(image);
        } else {
            return Asset.fromModule(image).downloadAsync();
        }
    });
  }

  getProductInterests() {
    getProductInterests().then(response => {
        console.log("FETCHED PRODUCTS");
        this.setState({
            productInterests: response.data
        })
    }).catch(error => {
        console.log("Error fetching");
    });
  }

  getProducts() {
    getProducts().then(response => {
        this.fetchedImages = [];
        // console.log("Got Data", response);
        let products = response.data.map((p) => {
          let product_image = siteUrl + "/agrostore/app/Storage/uploads/" + p.product_image_name;
          // console.log("PRODUCT IMAGE --------", p.id);

          this.fetchedImages.push(product_image);

          return {
            id: parseInt(p.id),
            product_name: p.name,
            interest_name: p.interest_name,
            description: p.description,
            stock_available: p.quantity_stocked - p.quantity_sold,
            price_per_unit: p.price_per_unit,
            bulk_price: p.price_bulk_total,
            source_location: p.source_location,
            product_image
          }
        });

        this.cacheImages(this.fetchedImages);

        this.setState({
            products
        })
    }).catch(error => {
        console.log("Error fetching");
    });

    // getProducts().then((response) => {
    //   console.log(response.data);
    // });
  }

  componentWillMount() {
    // get product Interests
    setTimeout(() => {
    }, 2000); // simulate some network delay

    this.getProductInterests();
    this.getProducts();
  } 

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', () => {
      if (this.navigator && this.navigator.getCurrentRoutes().length > 1) {
        this.navigator.pop();
        // BackHandler.exitApp();
        return true;
      }
      return false;
    });
  }

  saveUserData(data) {
    let userInfo = {...this.state.userInfo, ...data};
    this.setState({userInfo});
    // save to the back
  }

  saveStateData(key, value) {
    this.setState({
      [key]: value
    });
  }

  renderScene(route, navigator) {
    this.navigator = navigator;
    const store = this.state;

    switch(route.name) {
      case "chat":
        return <Chat navigator={navigator} />;
      case "splash_screen":
        return <SplashScreen navigator={navigator} />;
      case "registration_screen":
        return <RegistrationScreen 
                navigator={navigator}
                store={this.state}
                saveUserData={this.saveUserData.bind(this)}
                saveStateData={this.saveStateData.bind(this)}
                updateUser={this.updateUser.bind(this)} />;
      case "cart_products_screen":
        return <CartProducts 
                navigator={navigator}
                store={this.state}
                saveUserData={this.saveUserData.bind(this)}
                saveStateData={this.saveStateData.bind(this)}
                updateUser={this.updateUser.bind(this)} />;
      case "registration_customer_screen":
        return <RegistrationCustomerScreen 
                navigator={navigator} 
                saveUserData={this.saveUserData.bind(this)}
                saveStateData={this.saveStateData.bind(this)}
                store={store} />;
      case "login_screen":
        return <Login 
                navigator={navigator} 
                saveUserData={this.saveUserData.bind(this)}
                saveStateData={this.saveStateData.bind(this)}
                store={store} />;
      case "registration_merchant_screen":
        return <RegistrationMerchantScreen 
                navigator={navigator} 
                store={store} />;
      case "products_screen":
        return <Products 
                saveStateData={this.saveStateData.bind(this)}
                navigator={navigator} 
                store={store} />;
      case "product_search_screen":
        return <ProductSearch 
                saveStateData={this.saveStateData.bind(this)}
                navigator={navigator} 
                store={store} />;
      case "product_upload_screen":
        return <ProductUpload
                saveStateData={this.saveStateData.bind(this)}
                navigator={navigator} 
                store={store} />;
      case "product_request_screen":
        return <ProductRequest 
                saveStateData={this.saveStateData.bind(this)}
                navigator={navigator} 
                store={store} />;
      case "product_payment_screen":
        return <ProductPayment 
                saveStateData={this.saveStateData.bind(this)}
                navigator={navigator} 
                store={store} />;
      case "screens_menu":
        return <ScreensMenu 
                saveStateData={this.saveStateData.bind(this)}
                navigator={navigator} 
                store={store} />;
      default:
        return <ScreensMenu navigator={navigator} />;
    }
  }

  updateUser(userData) {
    let userInfo = {...this.state.userInfo, ...userData};

    this.setState({
       userInfo: userInfo
    })
  }

  closeDrawer() {
    this.refs['DRAWER'].closeDrawer();
  }

  render() {
    var navigationView = () => {
      // console.log("closeDrawer ------", closeDrawer);
      // console.log("this refs", this.drawer);

      return (
        <View style={{flex: 1, backgroundColor: '#fff'}}>
          <ScreensMenu navigator={this.navigator}
                  
                  store={this.state} />
        </View>
      );
    }

    // let closeDrawer = this.closeDrawer;
    // onDrawerSlide={(e) => {
    //   console.log("onDrawerSlider");
    //   // console.log(e);
    // }}
    
    return (
      <View style={styles.container}>
          <DrawerLayoutAndroid
            drawerWidth={280}
            drawerPosition={DrawerLayoutAndroid.positions.Left}
            renderNavigationView={() => navigationView()}>
            <Navigator
                initialRoute={{name: this.state.currentScreen}}
                renderScene={this.renderScene.bind(this)}
                configureScene={(route, routeStack) => Navigator.SceneConfigs.FloatFromBottom}
            />
          </DrawerLayoutAndroid>
     
      </View>
    );
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});
