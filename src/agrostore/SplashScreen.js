import React, { Component } from 'react';
import { 
  StyleSheet, 
  Text, 
  View, 
  Image , 
  TextInput,
  Dimensions,
  TouchableNativeFeedback,
  Animated,
  Easing,
  UIManager,
  TouchableOpacity
} from 'react-native';
import LocalImage from './components/LocalImage';
import TapCue from './components/TapCue';

export default class SplashScreen extends Component {

    state = {
        tapCueActive: false
    }

    componentWillMount() {
        this.scaleValue = new Animated.Value(0);
        this.spaceValue = new Animated.Value(0);
    }

    componentDidMount() {
        this.space();
        setTimeout(function(){
            this.props.navigator.push({name: "registration_screen"});            
        }.bind(this), 3000);

        setTimeout(function(){
            this.setState({
                tapCueActive: true
            });
        }.bind(this), 4000);
    }

    switchScreen() {
        if(this.state.tapCueActive) {
            let _this = this;
            setTimeout(() => {
                _this.props.navigator.push({name: "registration_screen"});
            });
        }
    }

    scale() {
        this.scaleValue.setValue(0);
        Animated.timing(
            this.scaleValue,
            {
                toValue: 1,
                duration: 1800,
                easing: Easing.easeOutBack
            }
        ).start(() => {
            this.scale();
        });
    }

    space() {
        this.spaceValue.setValue(0);
        Animated.timing(
            this.spaceValue,
            {
                toValue: 1,
                duration: 1800,
                easing: Easing.easeOutBack
            }
        ).start(() => {
            this.space();
        });
    }

    render() {

        const nearFar = this.scaleValue.interpolate({
            inputRange: [0, 0.5, 1],
            outputRange: [0.5, 0.7, 0.5]
        });

        const spaceDown = this.spaceValue.interpolate({
            inputRange: [0, 0.5, 1],
            outputRange: [0, 20, 0]
        });

        return (
            <View style={styles.container}>
                <View style={{flex: 1, zIndex: 1, justifyContent: 'center', alignItems: 'center'}}>
                    <View style={styles.main}>
                        <View style={styles.logoContainer}>
                            {/* <LocalImage 
                                source={require('../img/logo.png')}
                                originalWidth={500}
                                originalHeight={578}
                                guideWidth={150}
                            /> */}

                            <Animated.Image style={[
                                {
                                    width: 200,
                                    height: 223,
                                    marginBottom: spaceDown,
                                },
                                {
                                    transform: [
                                        {scale: nearFar},
                                    ]
                                }
                            ]}
                                source={require('./img/logo.png')}
                            />
                        </View>
                        <View style={styles.TextContainer}>
                            <View style={styles.brandName}>
                                <Text style={styles.brandNameText}>AgroMass</Text>
                            </View>
                            <View style={styles.brandMotto}>
                                <Text style={styles.brandMottoText}>Agricultural Product For All</Text>
                            </View>
                        </View>
                    </View>
                    <View style={styles.footer}>
                        <Text style={styles.footerText}>made with <Text style={styles.loveText}>Love</Text> in Nigeria</Text>
                    </View>
                </View>
                <TouchableOpacity 
                    onPress={() => this.switchScreen('registration_screen')}
                    style={[styles.overlay]}
                >
                    <View style={[styles.overlay, {zIndex: 10, opacity: 1, backgroundColor: 'transparent'}]}>
                        {this.state.tapCueActive && <TapCue />}
                    </View>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#16794a',
        justifyContent: 'center',
        alignItems: 'center'
    },
    logoContainer: {
        marginLeft: 30,
        justifyContent: 'flex-end',
        flex: 3,
        // backgroundColor: 'red'
    },  
    TextContainer: {
        flex: 2,
        justifyContent: 'flex-start',
        alignItems: 'center',
        // backgroundColor: 'green',
    },
    brandNameText: {
        color: 'white',
        fontSize: 30
    },
    brandMottoText: {
        color: '#7bc24b'
    },
    main: {
        flex: 1,
    },
    footer: {
        height: 20,
        justifyContent: 'flex-end',
        marginBottom: 10
    },
    footerText: {
        fontSize: 13,
        color: '#7bc24b'
    },
    loveText: {
        color: 'yellow'
    },
    overlay: {
        position: 'absolute',
        // backgroundColor: 'red',
        zIndex: 10,
        height: Dimensions.get('window').height,
        width: Dimensions.get('window').width,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row'
    }
});
