import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, Image, TextInput, Dimensions, KeyboardAvoidingView, TouchableOpacity} from 'react-native';
// import UtilityNavButton from './components/UtilityNavButton';
import Button from './Button';
import { Constants } from 'expo';

export default class ProductSearch extends Component {
    render() {
        return (
            <View style={styles.PinContainer}>
                 <View style={styles.statusBar}></View> 
                {/* <View style={styles.PinHeader}>
                     <TouchableOpacity style={styles.UtilityNav}>
                        <UtilityNavButton icon="Back"/>
                     </TouchableOpacity>
                     <View style={styles.OrdersButtonContainer}>
                         <TouchableOpacity style={styles.OrdersButton}>
                            <Text style={styles.OrdersButtonText}>Orders</Text>
                         </TouchableOpacity>
                    </View>
                </View> */}
                <ScrollView>
                    <View style={styles.productInfo}>
                        <TextInput
                            style={styles.input}
                            placeholder= 'Search by Product'
                            placeholderTextColor= "black"
                            underlineColorAndroid="#e5eae9"        
                            />
                        <View style={styles.proInfoLabel}>
                            <Text style={[styles.ProInfoLabelText]}> LIST OF ORDERS </Text>
                        </View>
                        <View style={styles.orderEntry}>
                            <View style={styles.productInfoLabel}>
                                <Text style={[styles.ProductInfoLabelText]}> Product: Yam Tubers</Text>
                            </View>
                            <View style={styles.productInfoLabel}>
                                <Text style={[styles.ProductInfoLabelText]}> From: Asaba</Text>
                            </View>
                            <View style={styles.productInfoLabel}>
                                <Text style={[styles.ProductInfoLabelText]}> Quantity: 1000</Text>
                            </View>
                            <View style={styles.productInfoLabel}>
                                <Text style={[styles.ProductInfoLabelText]}> Cost Price (Naira): 30000</Text>
                            </View>
                            <View style={styles.productInfoLabel}>
                                <Text style={[styles.ProductInfoLabelText]}> Delivered: Not Yet</Text>
                            </View>
                            <View style={styles.productInfoLabel}>
                                <Text style={[styles.ProductInfoLabelText]}> Shippment Location: Lagos</Text>
                            </View>
                        </View>
                        <View style={styles.orderEntry}>
                            <View style={styles.productInfoLabel}>
                                <Text style={[styles.ProductInfoLabelText]}> Product: Yam Tubers</Text>
                            </View>
                            <View style={styles.productInfoLabel}>
                                <Text style={[styles.ProductInfoLabelText]}> From: Asaba</Text>
                            </View>
                            <View style={styles.productInfoLabel}>
                                <Text style={[styles.ProductInfoLabelText]}> Quantity: 1000</Text>
                            </View>
                            <View style={styles.productInfoLabel}>
                                <Text style={[styles.ProductInfoLabelText]}> Cost Price (Naira): 30000</Text>
                            </View>
                            <View style={styles.productInfoLabel}>
                                <Text style={[styles.ProductInfoLabelText]}> Delivered: Not Yet</Text>
                            </View>
                            <View style={styles.productInfoLabel}>
                                <Text style={[styles.ProductInfoLabelText]}> Shippment Location: Lagos</Text>
                            </View>
                        </View>
                        <View style={styles.orderEntry}>
                            <View style={styles.productInfoLabel}>
                                <Text style={[styles.ProductInfoLabelText]}> Product: Yam Tubers</Text>
                            </View>
                            <View style={styles.productInfoLabel}>
                                <Text style={[styles.ProductInfoLabelText]}> From: Asaba</Text>
                            </View>
                            <View style={styles.productInfoLabel}>
                                <Text style={[styles.ProductInfoLabelText]}> Quantity: 1000</Text>
                            </View>
                            <View style={styles.productInfoLabel}>
                                <Text style={[styles.ProductInfoLabelText]}> Cost Price (Naira): 30000</Text>
                            </View>
                            <View style={styles.productInfoLabel}>
                                <Text style={[styles.ProductInfoLabelText]}> Delivered: Not Yet</Text>
                            </View>
                            <View style={styles.productInfoLabel}>
                                <Text style={[styles.ProductInfoLabelText]}> Shippment Location: Lagos</Text>
                            </View>
                        </View>
                        <View style={styles.orderEntry}>
                            <View style={styles.productInfoLabel}>
                                <Text style={[styles.ProductInfoLabelText]}> Product: Yam Tubers</Text>
                            </View>
                            <View style={styles.productInfoLabel}>
                                <Text style={[styles.ProductInfoLabelText]}> From: Asaba</Text>
                            </View>
                            <View style={styles.productInfoLabel}>
                                <Text style={[styles.ProductInfoLabelText]}> Quantity: 1000</Text>
                            </View>
                            <View style={styles.productInfoLabel}>
                                <Text style={[styles.ProductInfoLabelText]}> Cost Price (Naira): 30000</Text>
                            </View>
                            <View style={styles.productInfoLabel}>
                                <Text style={[styles.ProductInfoLabelText]}> Delivered: Not Yet</Text>
                            </View>
                            <View style={styles.productInfoLabel}>
                                <Text style={[styles.ProductInfoLabelText]}> Shippment Location: Lagos</Text>
                            </View>
                        </View>
                        
                    </View>    
                </ScrollView>
            </View>    
        );
    }
}

const styles = StyleSheet.create({
    PinHeader: {
        flexDirection: 'row',
        alignItems: 'flex-end',
        backgroundColor: 'ghostwhite',
        padding: 3
    },
    statusBar: {
        height: Constants.statusBarHeight,
        backgroundColor: '#007542'
    },
    orderEntry: {
        // backgroundColor: '#c1c1c1',
        padding: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#c7c7c7'
    },
    UtilityNav: {
        flex: 2,
        flexDirection: 'row',
    },
    OrdersButtonContainer: {
        flex: 3,
        alignItems: 'flex-end'

    },
       PinContainer: {
         flex: 1,
    },  

    UtilityButton: {
        flexDirection: 'row',
        justifyContent: 'center'
    },
    UtilityButtonText: {
        color: 'black',
        fontWeight: 'bold'
    },
    OrdersButton: {
        backgroundColor: 'red',
        borderRadius: 5,
        padding: 6,
    },
    OrdersButtonText: {
        color: 'white'
    },
     input: {
      backgroundColor: '#e5eae9',
      flexDirection: 'row',
      marginBottom: 8,
      paddingVertical: 2,
      paddingHorizontal: 2,
      textAlign: 'center',
      borderRadius: 15,
    //   marginTop: 8,         
  },
     productInfo: {
        backgroundColor: 'ghostwhite',
        padding: 15,
        borderColor: '#050505'
    },
       ProInfoLabelText: {
        color: '#050505',
        flex: 1,
        fontWeight: 'bold',
        textAlign: 'center'
    },
       proInfoLabel: {
        flexDirection: 'row',
    },
    productInfoLabel: {
        flexDirection: 'row',
        paddingBottom: 10
    },
    ProductInfoLabelText: {
        color: 'green',
        flex: 1
    },
});        