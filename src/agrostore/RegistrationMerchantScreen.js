import React, { Component } from 'react';
import { StyleSheet, Text, View, Dimensions, TouchableOpacity, ListView, Platform, ScrollView } from 'react-native';
import LocalImage from './components/LocalImage';
import StatusBar from './components/StatusBar';
import { theme } from './theme';
import MultiSelectBox from './components/MultiSelectBox';
import { prepareOptions } from './helpers';
import List from './components/List';
import Touch from './components/common/Touch';
import { getProductInterests } from './api';
 
export default class RegistrationCustomerScreen extends Component {

    constructor() {
        super();
        this.state = {
            ds: new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2}),
            selectedInterestIds: []
        }
    }

    getSelectionStatus(selectedOptions, optionId) {
        let selectedOption = selectedOptions.find(opt => opt === optionId) || 0;

        if(selectedOption === optionId) {
            return true;
        }

        return false;
    }

    removeSelectedOption(selectedOptions, optionId) {
        
        newOptions = selectedOptions.filter((item, index) => item !== optionId);

        return newOptions;
    }

    handleSelectProduct(id) {
        let { selectedInterestIds=[] } = this.state;
        let isSelected = this.getSelectionStatus(selectedInterestIds, id);
        console.log("selectionStatus", isSelected);
        let newselectedInterestIds = [];
        if(isSelected) {
            newselectedInterestIds = this.removeSelectedOption(selectedInterestIds, id)
        } else {
            newselectedInterestIds = [...selectedInterestIds, id];
        }

        this.setState({
            selectedInterestIds: newselectedInterestIds
        });

        console.log("selectedInterestIds", selectedInterestIds);
    }

    getselectedInterests(selectedOptionIds, productInterests) {
        let data = [];
        let foundItem = {};
        for(let i=0; i < selectedOptionIds.length; i++) {
            foundItem = productInterests.find((item) => item.id == selectedOptionIds[i]);
            data.push(foundItem);
        }

        console.log("selected products", data);
        return data;
    }

    renderProductSelectionRow(rowData) {
        return (
            <View style={styles.productItem}>
                <Text style={styles.itemName}>{rowData.name}</Text>
            </View>
        )
    }

    render() {
        const { productInterests } = this.props.store;
        const options = prepareOptions(productInterests, "name");
        let selectedOptionIds = this.state.selectedInterestIds || [];
        let listInitialData = {};
        let selectedInterests = this.getselectedInterests(selectedOptionIds, productInterests);
        let dataSource =  this.state.ds.cloneWithRows(selectedInterests);
        let selectionContent = <View />;

        if(selectedInterests.length < 1) {
            selectionContent = (
                <View style={styles.productItem}>
                    <Text style={styles.itemName}>Add Interests</Text>
                </View>
            );
        } else {
            selectionContent = (
                <ListView
                    dataSource={dataSource}
                    enableEmptySections={true}
                    renderRow={(rowData) => this.renderProductSelectionRow(rowData)}
                />
            );
        }

        return (
            <View style={styles.container}>
                <StatusBar backgroundColor={theme.backgroundColor} />
                <ScrollView style={styles.contentArea}>

                    <Text style={[styles.screenTitle, {fontWeight: '700', fontSize: 25}]}> Welcome</Text>
                    {/* <Text style={[styles.screenTitle, {marginTop: 20, paddingLeft: 30, paddingRight: 30}]}> Screen under construction </Text> */}
                    <Text style={[styles.screenTitle, {marginTop: 20, paddingLeft: 30, paddingRight: 30}]}> 
                    
                        Hello, Merchant. You can now start uploading your products. 
                    
                    </Text>

                </ScrollView>
                <View style={styles.actionButton}>
                    <Touch action={() => this.props.navigator.push({name: 'product_upload_screen'})}>
                        <View style={[styles.buttonContainer]}>
                            <Text style={[styles.buttonText]}> Start Uploading </Text>
                        </View>
                    </Touch>
                </View>
            </View>   
         );
     }
}


const styles = StyleSheet.create({
    container: {
        backgroundColor: "white",
        flex: 1
    },
    contentArea: {
        padding: 15,
        flex: 1,
        marginBottom: 15,
        paddingBottom: 30
    },
    screenTitle: {
        color: '#555555',
        fontSize: 20,
        textAlign: 'center'
    },
    productItem: {
        marginLeft: 15,
        marginRight: 15,
        marginBottom: 15,
        borderRadius: 3,
        backgroundColor: '#347c3e9a'
    },
    selectedProducts: {
        backgroundColor: '#f5f5f5',
        // minHeight: 200,
        paddingBottom: 20,
        marginBottom: 30,
        // paddingTop: 20,
        borderColor: '#347c3e9a',
        borderRadius: 4,
        borderWidth: 1,
        overflow: 'hidden',
        flex: 1
    },
    selectionHeader: {
        backgroundColor: '#347c3e9a',
        marginBottom: 15,
        borderTopLeftRadius: 3,
        borderTopRightRadius: 3,
        padding: 10,
        // borderBottomWidth: 1,
        // borderBottomColor: "#347c3e9a",
    },
    selectionHeaderText: {
        fontSize: 15,
        color: 'white',
        fontWeight: '700'
    },
    itemName: {
        fontSize: 15,
        padding: 10,
        color: 'white'
    },
    actionButton: {
        height: 70,
        justifyContent: 'center',
        borderTopWidth: 1,
        borderTopColor: '#ccc',
    },
    buttonContainer: {
        backgroundColor: theme.backgroundColor,
        height: 45,
        margin: 15,
        justifyContent: 'center',
        borderRadius: 3,
    },
    buttonText: {
        fontSize: 15,
        textAlign: 'center',
        color: 'white',
        fontWeight: '700'
    }

});