import React, { Component } from 'react';
import { StyleSheet, Text, View, Dimensions, TouchableOpacity, ListView, Platform, ScrollView } from 'react-native';
import LocalImage from './components/LocalImage';
import StatusBar from './components/StatusBar';
import { theme } from './theme';
import MultiSelectBox from './components/MultiSelectBox';
import { prepareOptions } from './helpers';
import List from './components/List';
import Touch from './components/common/Touch';
import { getProductInterests, createUser } from './api';
import Loading from './components/Loading';
 
export default class RegistrationCustomerScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            ds: new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2}),
            selectedInterestIds: this.props.store.userInfo.interest_ids
        }
    }

    getSelectionStatus(selectedOptions, optionId) {
        if (selectedOptions === undefined) {
            selectedOptions = [];
            let selectedOption = selectedOptions.find(opt => opt === optionId) || 0;
    
            if(selectedOption === optionId) {
                return true;
            }
        }

        return false;
    }

    removeSelectedOption(selectedOptions, optionId) {
        
        newOptions = selectedOptions.filter((item, index) => item !== optionId);

        return newOptions;
    }

    handleSelectInterest(id) {
        let { store, saveUserData } = this.props;
        let selectedInterestIds = store.userInfo.interest_ids;
        let isSelected = this.getSelectionStatus(selectedInterestIds, id);
        console.log("selectionStatus", isSelected);
        let newselectedInterestIds = [];
        if(isSelected) {
            newselectedInterestIds = this.removeSelectedOption(selectedInterestIds, id)
        } else {
            newselectedInterestIds = [...selectedInterestIds, id];
        }

        saveUserData({interest_ids: newselectedInterestIds});
        // this.setState({
        //     selectedInterestIds: newselectedInterestIds
        // });

        // console.log("selectedInterestIds", selectedInterestIds);
    }

    getselectedInterests(selectedOptionIds, productInterests) {
        let data = [];
        let foundItem = {};
        for(let i=0; i < selectedOptionIds.length; i++) {
            foundItem = productInterests.find((item) => item.id == selectedOptionIds[i]);
            data.push(foundItem);
        }

        console.log("selected products", data);
        return data;
    }

    renderProductSelectionRow(rowData) {
        let name = rowData != undefined ? rowData.name : "";
        return (
            <View style={styles.productItem}>
                <Text style={styles.itemName}>{ name }</Text>
            </View>
        )
    }

    registerUser() {
        let prefix = "";
        // let { selectedInterestIds } = this.props.store.userInfo.interest_ids;
        let { saveUserData, navigator, store } = this.props;
        let { name, contact, address, username, primary_location, user_type, password, interest_ids } = store.userInfo;

        console.log("Interest Ids", interest_ids);

        let interest_ids_string = "";
        for(let i=0; i < interest_ids.length; i++) {
            prefix = i === 0 ? "" : ",";
            interest_ids_string = interest_ids_string + prefix + interest_ids[i];
        }
        let firstname = "";
        let lastname = "";
        let userData = {interest_ids_string};

        let name_string = name.split(" ");
        firstname = name_string[0];
        lastname = name_string[1];

        userData = { lastname, firstname, contact, address, username, primary_location, user_type, password, interest_ids_string };

        saveUserData(userData);
        navigator.push({name: 'products_screen'});
    }

    componentWillReceiveProps(nextProps) {
        let { store } = nextProps;

        console.log("Next Props => ", store.userInfo);
    }

    render() {
        const { productInterests=[], userInfo } = this.props.store;
        const options = prepareOptions(productInterests, "name");
        let selectedOptionIds = userInfo.interest_ids || [];
        let listInitialData = {};
        let selectedInterests = this.getselectedInterests(selectedOptionIds, productInterests);
        let dataSource =  this.state.ds.cloneWithRows(selectedInterests);
        let selectionContent = <View />;

        if(selectedInterests.length < 1) {
            selectionContent = (
                <View style={styles.productItem}>
                    <Text style={styles.itemName}>Add Interests</Text>
                </View>
            );
        } else {
            selectionContent = (
                <ListView
                    dataSource={dataSource}
                    enableEmptySections={true}
                    renderRow={(rowData) => this.renderProductSelectionRow(rowData)}
                />
            );
        }

        return (
            <View style={styles.container}>
                <StatusBar backgroundColor={theme.backgroundColor} />
                <ScrollView style={styles.contentArea}>

                    <Text style={[styles.screenTitle, {fontWeight: '700', fontSize: 25}]}> Welcome</Text>
                    <Text style={[styles.screenTitle, {marginTop: 20, paddingLeft: 30, paddingRight: 30}]}>Which Type of products will you be more interested in? </Text>

                    <View style={{marginTop: 20, marginBottom: 20}}>
                        {productInterests < 1 && <Loading text="Loading Product Categories" /> ||

                            <MultiSelectBox
                                style={{boxStyle: {borderColor: theme.lightGreenBackground}}}
                                selectedOptionIds={selectedOptionIds}
                                handleSelect={(id) => this.handleSelectInterest(id)}
                                text="Select Product Interests"
                                selectedItemStyle={{backgroundColor: theme.backgroundColor, textColor: "white"}}
                                unselectedItemStyle={{backgroundColor: "white"}}
                                options={options}
                            />
                        }
                    </View>

                    <View style={styles.selectedProducts}>
                        <View style={styles.selectionHeader}>
                            <Text style={styles.selectionHeaderText}>Selected Interests</Text>
                        </View>

                        { selectionContent }
                    
                    </View>
                    
                </ScrollView>
                
                <View style={styles.actionButton}>
                    <Touch action={() => this.registerUser()}>
                        <View style={[styles.buttonContainer]}>
                            <Text style={[styles.buttonText]}> Continue </Text>
                        </View>
                    </Touch>
                </View>
            </View>   
         );
     }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    contentArea: {
        padding: 15,
        flex: 1,
        marginBottom: 15,
        paddingBottom: 30
    },
    screenTitle: {
        color: '#555555',
        fontSize: 20,
        textAlign: 'center'
    },
    productItem: {
        marginLeft: 15,
        marginRight: 15,
        marginBottom: 15,
        borderRadius: 3,
        backgroundColor: '#347c3e9a'
    },
    selectedProducts: {
        backgroundColor: '#f5f5f5',
        // minHeight: 200,
        paddingBottom: 20,
        marginBottom: 30,
        // paddingTop: 20,
        borderColor: '#347c3e9a',
        borderRadius: 4,
        borderWidth: 1,
        overflow: 'hidden',
        flex: 1
    },
    selectionHeader: {
        backgroundColor: '#347c3e9a',
        marginBottom: 15,
        borderTopLeftRadius: 3,
        borderTopRightRadius: 3,
        padding: 10,
        // borderBottomWidth: 1,
        // borderBottomColor: "#347c3e9a",
    },
    selectionHeaderText: {
        fontSize: 15,
        color: 'white',
        fontWeight: '700'
    },
    itemName: {
        fontSize: 15,
        padding: 10,
        color: 'white'
    },
    actionButton: {
        height: 70,
        justifyContent: 'center',
        borderTopWidth: 1,
        borderTopColor: '#ccc',
    },
    buttonContainer: {
        backgroundColor: theme.backgroundColor,
        height: 45,
        margin: 15,
        justifyContent: 'center',
        borderRadius: 3,
    },
    buttonText: {
        fontSize: 15,
        textAlign: 'center',
        color: 'white',
        fontWeight: '700'
    }

});