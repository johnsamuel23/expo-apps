import React from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
import LocalImage from './LocalImage';

export default class App extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.body}>
            <View style={styles.logoContainer}>
                <LocalImage 
                    source={require('../img/logo.png')}
                    originalWidth="500"
                    originalHeight="578"
                    guideWidth="150"
                     />
            </View>
            <View style={styles.payoffContainer}>
                <View style={styles.brandname}>
                    <Text style={styles.brandnameText}> AgroMass</Text>
                </View>
                <View style={styles.payoffContainer}>
                    <Text style={styles.payoffText}> Agricultural Products For all</Text>
                </View>
            </View>
        </View>

        <View style={styles.footer}>
            <Text style={styles.footerText}> Made with <Text style={styles.yellow}>Love</Text> in Nigeria </Text>
        </View>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#16794a'
  },
  body: {
      flex: 1
  },
  logoContainer: {
    flex:  3,
    justifyContent: 'flex-end',
    alignItems: 'center',
    paddingLeft: 30
  },
  payoffContainer: {
    flex: 2,
    alignItems: 'center'
  },
  brandnameText: {
    fontSize: 35,
    color: 'white'
  },
  payoffText: {
    color: '#7bc24b',
    fontSize: 12
  },
  footerText: {
    fontSize: 12,
    color: '#7bc24b',
  },
  yellow: {
    color: 'yellow'
  },
  footer: {
    height: 50,
    alignItems: 'flex-end',
    justifyContent: 'center',
    flexDirection: 'row',
    paddingBottom: 15
  }
});
