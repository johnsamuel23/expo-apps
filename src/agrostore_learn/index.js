import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import SplashScreen from './components/SplashScreen';
import Products from './components/Products';

export default class App extends React.Component {
  render() {
    return (
      <View style={styles.container}>
          <SplashScreen />  
          {/* <Products /> */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});
