import React, { Component } from 'react';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { View } from 'react-native';


export default class Pin extends Component {
    render() {
        const Icon = (<MaterialCommunityIcons name="pin" size={20} color={this.props.color} />)
        return (
            <View>
                { Icon }     
            </View>
        )
    }
}