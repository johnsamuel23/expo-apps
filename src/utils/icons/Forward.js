import React, { Component } from 'react';
import { Ionicons } from '@expo/vector-icons';
import { View } from 'react-native';

const Icon = (<Ionicons name="ios-arrow-forward" size={35} color="#555" />)

export default class Forward extends Component {
    render() {
        return (
            <View>
                { Icon }     
            </View>
        )
    }
}