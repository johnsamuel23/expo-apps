// import { createStore } from 'redux';
import { createLogger } from 'redux-logger';
import { applyMiddleware, createStore, compose } from "redux";
import reducers from "../reducers";
import thunk from 'redux-thunk';

// const store = createStore(reducers);

const middlewares = [thunk];
if(process.env.NODE_ENV !== 'production') {
    // middlewares.push(createLogger());
}

const store =  createStore(
    reducers, 
    {},
    compose(
        applyMiddleware(...middlewares),
        // autoRehydrate({log: true}) // for development only
    )
);

export default store;

// import {persistStore, autoRehydrate} from 'redux-persist';
// import { asyncSessionStorage } from 'redux-persist/storages';
// import createActionBuffer from 'redux-action-buffer';
// import {REHYDRATE} from 'redux-persist/constants';

// const configureStore = () => {
// 	const middlewares = [thunk];
// 	middlewares.push(createActionBuffer('REHYDRATION_COMPLETE'));
// 	if(process.env.NODE_ENV !== 'production') {
// 	  middlewares.push(createLogger());
// 	}

// 	const store =  createStore(
// 		reducers, 
// 		{},
// 		compose(
// 			applyMiddleware(...middlewares),
// 			autoRehydrate({log: true}) // for development only
// 		)
// 	);
	
// 	// persistStore(store, {whitelist: ['users', 'fixtures'], storage: asyncSessionStorage}, function() {
// 	// 	store.dispatch({
// 	// 		type: 'REHYDRATION_COMPLETE'
// 	// 	})
// 	// });

// 	// persistStore(store).purge();

// 	return store;
// }

// export default configureStore;