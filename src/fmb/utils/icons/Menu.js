import React, { Component } from 'react';
import { MaterialIcons } from '@expo/vector-icons';
import { View } from 'react-native';

export default Menu = ({
    size,
    color
}) => {
    const Icon = (<MaterialIcons name="menu" size={size} color={color} />)
    return (
        <View>
            { Icon }     
        </View>
    )

}