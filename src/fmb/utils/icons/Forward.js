import React, { Component } from 'react';
import { Ionicons } from '@expo/vector-icons';
import { View } from 'react-native';

export default Forward = ({
    size,
    color
}) => {
    const Icon = (<Ionicons name="ios-arrow-forward" size={size} color={color} />)
    return (
        <View>
            { Icon }     
        </View>
    )

}