import React, { Component } from 'react';
import { Ionicons } from '@expo/vector-icons';
import { View } from 'react-native';

export default Back = ({
    size,
    color
}) => {
    const Icon = (<Ionicons name="ios-arrow-back" size={size} color={color} />)
    return (
        <View>
            { Icon }     
        </View>
    )

}