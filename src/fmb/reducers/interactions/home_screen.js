import * as actions from '../../actions/types';

const initialState = {
    activeTab: 'Home' // Home | Conversations | Profile
}

const home_screen = (state=initialState, action) => {
    switch(action.type) {
        case actions.CHANGE_ACTIVE_HOMESCREEN_TAB:
            return {...state, activeTab: action.payload};
        default:
            return state;
    }
}

export default home_screen;