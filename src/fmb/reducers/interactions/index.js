import { combineReducers } from 'redux';
import fixturesActions from './fixtures';
import homeScreenActions from './home_screen';

export default interaction = combineReducers({
    fixturesActions,
    homeScreenActions
});