import { combineReducers } from 'redux';
import fixtures from './fixtures';
import users from './users';
import interactions from './interactions';

export default combineReducers({
    fixtures,
    users,
    interactions
});