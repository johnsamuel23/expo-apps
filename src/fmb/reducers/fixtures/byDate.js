import * as actions from '../../actions/types';

const byDate = (fixtures=[], action) => {
    switch(action.type) {
        case actions.FIXTURES_SUCCESS:
            let byDateState = {dates: []};
            byDateState = fixtures.reduce((byDateState, fixture) => {
                byDateState[fixture.fixture_date] = byDateState[fixture.fixture_date] || [];

                if(! isInArray(fixture.fixture_date, byDateState.dates)) {
                    byDateState.dates.push(fixture.fixture_date);
                }

                byDateState[fixture.fixture_date].push(fixture);

                return byDateState;
            }, byDateState);

            // console.log("last values of bydate state", byDateState.dates.length);
            return byDateState;

        default:
            return {};
    }
}

const isInArray = (item, array) => {
    let ascertion = false;

    array.forEach(element => {
        if(element === item) {
            ascertion = true;
        }
    });

    return ascertion;
}

export default byDate;