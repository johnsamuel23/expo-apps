import * as actions from '../../actions/types';

const all = (state=[], action) => {
    switch(action.type) {
        case actions.FETCH_FIXTURES:
            return state;
        case actions.FIXTURES_FAILURE:
            return state;
        case actions.FIXTURES_SUCCESS:
            return [...state, ...action.payload];
        default:
            return state;
    }
}

export default all;