import { combineReducers } from 'redux';
import * as actions from '../../actions/types';
import all from './all';
import byDate from './byDate';

const initialState = {
    all: [],
    byDate: [] 
}

const fixtures = (state=initialState, action) => {

    switch(action.type) {
        case actions.FETCH_FIXTURES:
            return state;
        case actions.FIXTURES_FAILURE:
            return state;
        case actions.FIXTURES_SUCCESS:
            let newState = {};

            newState.all = [...state.all, ...action.payload];

            newState = {
                all: all(state.all, action),
                byDate: byDate(newState.all, action)
            }

            return newState;
        default:
            return state;
    }
}

export default fixtures;