import React, { Component } from 'react';
import { View, StyleSheet, ScrollView } from 'react-native';
import { headerStyle, headerTitleStyle } from '../styles';
import LayoutHeader from '../components/common/LayoutHeader';
import LayoutBody from '../components/common/LayoutBody';
import Left from '../components/common/Left';
import Center from '../components/common/Center';
import Row from '../components/common/Row';
import Right from '../components/common/Right';
import Text from '../components/common/Text';
import BackgroundWrapper from '../components/common/BackgroundWrapper';
import ClubLogo from '../components/common/ClubLogo';
import LocalImage from '../components/common/LocalImage';
import ActionButton from '../components/Button';
import HSpace from '../components/common/HSpace';
import { theme } from '../styles';

export default class EmailSubscribe extends Component {

    static navigationOptions = () => ({
        headerTitle: 'Club Selection',
        headerStyle,
        headerTitleStyle,
        // headerLeft: <View />
    });

    componentWillMount() {

        // load demo props
        this.setState({
            selectedClub: {
                clubName: 'AC Milan',
                clubLogo: require('../assets/img/ac_milan.png')
            }
        });
    }

    render() {
        const { navigation } = this.props;
        const { selectedClub={} } = this.state;

        return (
            <ScrollView style={styles.container}>
                <LayoutHeader style={styles.header}>
                    <Text style={[styles.headerText, {color: theme.brandColor}]}>Let's Begin</Text>
                    <Text style={[styles.headerText, {color: 'black', fontSize: 20}]}>Choose your bae club</Text>
                </LayoutHeader>
                <LayoutBody style={styles.body}>
                    <View style={styles.badgeContainer}>
                        <Row>
                            <Left style={{minWidth: 50}}>
                                <ClubLogo size={35} source={selectedClub.clubLogo} />
                            </Left>    
                            <Center style={styles.badgeClubName}>
                                <Text style={styles.clubName}>{selectedClub.clubName}</Text>
                            </Center>
                            <Right style={styles.right}>
                                <View style={styles.markBadge}>
                                    <LocalImage 
                                        source={require('../assets/img/mark_badge.png')} 
                                        originalHeight={112}
                                        originalWidth={112}
                                        guideWidth={30}
                                        />
                                </View>
                            </Right>
                        </Row>
                    </View>
                    <View>
                        <Row>
                            <Text style={styles.questionText}>
                                Would you like to receive email updates and information from your bae club?
                            </Text>
                        </Row>
                        <Row>
                            <View style={styles.columnDouble}>
                                <ActionButton text="Yes"/>
                            </View>
                            <HSpace size={25} />
                            <View style={styles.columnDouble}>
                                <ActionButton thick text="No"/>
                            </View>
                        </Row>
                        <View style={{marginTop: 25}}>
                            <ActionButton thick text="Oya Let's go"/>
                        </View>
                    </View>
                </LayoutBody>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    header: {
        backgroundColor: 'white',
        paddingBottom: 15,
        paddingTop: 30,
        justifyContent: 'center',
        borderBottomColor: '#f1f1f1',
        borderBottomWidth: 1
    },
    questionText: {
        padding: 20,
        paddingLeft: 50,
        paddingRight: 50,
        textAlign: 'center',
        fontSize: 15,
        color: '#555'
    },
    columnDouble: {
        flex: 1
    },
    headerText: {
        textAlign: 'center'
    },
    markBadge: {
        justifyContent: 'center'        
    },
    badgeClubName: {
        justifyContent: 'center'
    },
    right: {
        justifyContent: 'center',
    },
    badgeContainer: {
        borderColor: '#888888',
        borderWidth: 1,
        borderRadius: 3,
        paddingLeft: 15,
        paddingRight: 15,
        paddingTop: 10,
        paddingBottom: 10,
    },
    body: {
        padding: 20
    }
});