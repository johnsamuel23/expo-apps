import React, { Component } from 'react';
import { View, StyleSheet, ScrollView, Platform } from 'react-native';
import { headerStyle, headerTitleStyle, theme } from '../styles';
import { connect } from 'react-redux';
import Col from '../components/common/Col';
import ClubLogo from '../components/common/ClubLogo';
import BackgroundWrapper from '../components/common/BackgroundWrapper';
import Footer from '../components/Footer';
import Header from '../components/Header';
import Icon from '../components/Icon';
import LayoutHeader from '../components/common/LayoutHeader';
import LayoutBody from '../components/common/LayoutBody';
import Left from '../components/common/Left';
import Right from '../components/common/Right';
import MenuIcon from '../components/MenuIcon';
import Row from '../components/common/Row';
import SelectBox from '../components/SelectBox';
import Text from '../components/common/Text';
import TopSection from '../components/ScreenTopSection';
import { prepareOptions } from '../helpers';
import FixtureResultCard from '../components/FixtureResultCard';
import HSpace from '../components/common/HSpace';
import PostImage from '../components/PostImage';


class newsHomeScreen extends Component {
    
    state = {
        news: [
            {
                id: 1,
                category: 'Italy football',
                title: 'Totti thanks fans for support in new life',
                date: 'Wed, 27 Sep 2017',
                time: '12:30',
                postImage: require('../assets/img/totti.jpg')
            },
            {
                id: 2,
                category: 'Premier League',
                title: 'Ramsey - What Lacazette does well',
                date: 'Wed, 27 Sep 2017',
                time: '11:00',
                postImage: require('../assets/img/ramsey.jpg')
            },
            {
                id: 3,
                category: 'Arsenal FC',
                title: 'Why Wenger picks Iwobi in big matches',
                date: 'Wed, 27 Sep 2017',
                time: '09:00',
                postImage: require('../assets/img/wenger.jpg')
            },
            {
                id: 4,
                category: 'Champions League',
                title: 'Zidane hails Bale and Ronaldo',
                date: 'Tues, 26 Sep 2017',
                time: '17:00',
                postImage: require('../assets/img/zidane.jpg')
            },
        ]
    }

    static navigationOptions = ({ navigation }) => { 
        let header = <Header titleStyle={styles.titleStyle} titleText="latest news" noShadow={true} leftButtonAction={navigation.goBack} />;

        return {
            header
        }
    };
    
    render() {

        let newsList = this.state.news.map((item, index) => {
            return (
                <View
                    style={styles.newsCard} 
                    key={index}>
                    <View style={styles.newsHeader}>
                        <Text style={styles.headerText}>{item.category}</Text>
                    </View>
                    <View style={styles.newsBody}>
                        <View style={styles.postImage}>
                            <PostImage source={item.postImage}/>
                        </View>
                        <View style={styles.textArea}>
                            <Text style={styles.newsTitleText}>{item.title}</Text>
                            <Text style={styles.newsDateText}>{item.date} | {item.time}</Text>                            
                        </View>
                    </View>
                </View>
            );
        });
        
        return (
            <View style={{flex: 1, backgroundColor: '#fff'}}>
                <ScrollView style={{flex: 1}} contentContainerStyle={styles.container}>
                    <BackgroundWrapper style={{flex: 1, padding: 10}}>
                        
                        { newsList }

                    </BackgroundWrapper>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        // paddingTop: 20,
        // paddingBottom: 20,
    },
    topSection: {
        padding: 20,
        paddingBottom: 25,
    },
    title: {
        fontFamily: 'saira-regular',
        fontSize: 13,
        marginBottom: 5
    },
    titleStyle: {
        fontFamily: 'saira-semibold'
    },
    newsCard: {
        backgroundColor: 'white',
        marginBottom: 10,
        borderRadius: 3,
        overflow: 'hidden'
    },
    newsHeader: {
        paddingVertical: 5,
        paddingHorizontal: 10,
        borderBottomWidth: 1,
        borderBottomColor: theme.lightBackgroundColor
    },
    headerText: {
        color: theme.darkGrayColor,
        fontFamily: 'saira-semibold'
    },
    newsBody: {
        flexDirection: 'row',
    },
    postImage: {
    },
    textArea: {
        padding: 15,
        flex: 1,
        position: 'relative'
    },
    newsTitleText: {
        fontSize: 15,
        fontFamily: 'saira-semibold',
        lineHeight: 18,
        color: '#555'
    },
    newsDateText: {
        position: 'absolute',
        marginLeft: 15,
        bottom: 10,
        color: theme.grayTextColor
    }
});

const mapStateToProps = (state) => {
    
    let fixtures = state.fixtures;

    return {
        fixtures 
    }
}

export default connect(mapStateToProps)(newsHomeScreen);