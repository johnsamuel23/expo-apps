import React, { Component } from 'react';
import { View, StyleSheet, ScrollView } from 'react-native';
import { headerStyle, headerTitleStyle } from '../styles';
import LayoutHeader from '../components/common/LayoutHeader';
import StatusBar from '../components/common/StatusBar';
import LayoutBody from '../components/common/LayoutBody';
import Text from '../components/common/Text';
import BackgroundWrapper from '../components/common/BackgroundWrapper';
import ClubListItem from '../components/ClubListItem';

import { theme } from '../styles';

export default class ClubSelect extends Component {

    state = {
        clubs: [
            {
                clubName: 'AC Milan',
                clubLogo: require('../assets/img/ac_milan.png'),
            },
            {
                clubName: 'Arsenal',
                clubLogo: require('../assets/img/arsenal.png'),
            }, 
            {
                clubName: 'Bayern Munich',
                clubLogo: require('../assets/img/bayern_munich.png'),
            },
            {
                clubName: 'AC Milan',
                clubLogo: require('../assets/img/ac_milan.png'),
            },
            {
                clubName: 'Arsenal',
                clubLogo: require('../assets/img/arsenal.png'),
            }, 
            {
                clubName: 'Bayern Munich',
                clubLogo: require('../assets/img/bayern_munich.png'),
            },
            {
                clubName: 'Barcelona',
                clubLogo: require('../assets/img/barcelona.png'),
            },
            {
                clubName: 'Chelsea',
                clubLogo: require('../assets/img/chelsea.png'),
            }, 
            {
                clubName: 'Crystal Palace',
                clubLogo: require('../assets/img/crystal_palace.png'),
            },
            {
                clubName: 'Juventus',
                clubLogo: require('../assets/img/juventus.png'),
            },
            {
                clubName: 'Liverpool',
                clubLogo: require('../assets/img/liverpool.png'),
            }, 
            {
                clubName: 'Manchester United',
                clubLogo: require('../assets/img/manchester_united.png'),
            }, 
            {
                clubName: 'Manchester City',
                clubLogo: require('../assets/img/manchester_city.png'),
            },
        ]
    }

    render() {
        const { navigation } = this.props;
        const { clubs } = this.state;  
        const clubsList = clubs.map((club, index) => {
            let isLast = index === clubs.length - 1;
            return (
                <ClubListItem club={club} key={index} isLast={isLast} />
            )
        });

        return (
            <View style={{flex: 1}}>
                <StatusBar />
                <ScrollView style={styles.container}>
                    <LayoutHeader style={styles.header}>
                        <Text style={[styles.headerText, {color: theme.brandColor}]}>Let's Begin</Text>
                        <Text style={[styles.headerText, {color: 'black', fontSize: 20}]}>Choose your bae club</Text>
                    </LayoutHeader>
                    <LayoutBody>
                        <BackgroundWrapper style={styles.wrapper}>
                            <View style={styles.clubList}>

                                { clubsList }

                            </View>
                        </BackgroundWrapper>                  
                    </LayoutBody>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    header: {
        backgroundColor: 'white',
        paddingBottom: 15,
        paddingTop: 30,
        justifyContent: 'center',
        borderBottomColor: '#f1f1f1',
        borderBottomWidth: 1
    },
    headerText: {
        textAlign: 'center',
        fontFamily: 'saira-semibold'
    },
    wrapper: {
        padding: 15,
    },
    clubList: {
        backgroundColor: '#fff',
        paddingTop: 10,
        paddingBottom: 10,
        borderRadius: 3,
    }
});