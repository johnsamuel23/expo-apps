import React, { Component } from 'react';
import { View, StyleSheet, Text, Platform, TouchableOpacity } from 'react-native';
import { headerStyle, headerTitleStyle } from '../styles';
import ActionButton from '../components/Button';
import LogoTextGroup from '../components/LogoTextGroup';
import StatusBar from '../components/common/StatusBar';

export default class StartScreen extends Component {

    static navigationOptions = () => ({
        header: <StatusBar />
    });

    render() {
        const { navigation } = this.props;
        return (
            <View style={styles.container}>
                <View style={styles.logoTextGroup}>
                    <LogoTextGroup />
                </View>    
                <View style={styles.actionButtons}>
                    <View style={{marginTop: 10, marginBottom: 10}}>
                        <ActionButton 
                            action={() => console.log("get started clicked")}
                            thick={true} text="Get Started" />
                    </View>
                    <View style={{marginTop: 10, marginBottom: 10}}>
                        <ActionButton 
                            action={() => console.log("Login clicked")}
                            text="Login" />
                    </View>
                </View>    
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        flex: 1,
        padding: 20
    },
    logoTextGroup: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    actionButtons: {
        flex: 1,
        justifyContent: 'center'
    },
});