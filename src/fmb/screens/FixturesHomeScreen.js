import React, { Component } from 'react';
import { View, StyleSheet, ScrollView, Platform } from 'react-native';
import { headerStyle, headerTitleStyle, theme } from '../styles';
import { connect } from 'react-redux';
import Col from '../components/common/Col';
import ClubLogo from '../components/common/ClubLogo';
import BackgroundWrapper from '../components/common/BackgroundWrapper';
import Footer from '../components/Footer';
import Header from '../components/Header';
import Icon from '../components/Icon';
import LayoutHeader from '../components/common/LayoutHeader';
import LayoutBody from '../components/common/LayoutBody';
import MenuIcon from '../components/MenuIcon';
import Row from '../components/common/Row';
import SelectBox from '../components/SelectBox';
import Text from '../components/common/Text';
import TopSection from '../components/ScreenTopSection';
import { prepareOptions } from '../helpers';
import FixturesList from '../components/FixturesList';


class FixturesHomeScreen extends Component {
    
    state = {
        competitions: [
            {
                id: 1,
                name: "UEFA Champions League",
                abbreviation: ''
            },
            {
                id: 2,
                name: "La Liga",
                abbreviation: ''
            },
            {
                id: 3,
                name: "UEFA Champions League",
                abbreviation: ''
            },
            {
                id: 4,
                name: "La Liga",
                abbreviation: ''
            },
            {
                id: 5,
                name: "UEFA Champions League",
                abbreviation: ''
            },
            {
                id: 6,
                name: "La Liga",
                abbreviation: ''
            },
            {
                id: 7,
                name: "UEFA Champions League",
                abbreviation: ''
            },
            {
                id: 8,
                name: "La Liga",
                abbreviation: ''
            },
            {
                id: 9,
                name: "UEFA Champions League",
                abbreviation: ''
            },
            {
                id: 10,
                name: "La Liga",
                abbreviation: ''
            },
            {
                id: 11,
                name: "UEFA Champions League",
                abbreviation: ''
            },
            {
                id: 12,
                name: "La Liga",
                abbreviation: ''
            },
        ],
        selectedCompetitionId: 1,
        fixtures: [],
    }

    static navigationOptions = ({ navigation }) => { 
        let header = <Header titleStyle={styles.titleStyle} titleText="fixtures" noShadow={true} leftButtonAction={navigation.goBack} />;

        return {
            header
        }
    };
    
    selectCompetition(id) {
        this.setState({
            selectedCompetitionId: id
        });
    }

    render() {
        const { navigation, fixtures } = this.props;
        console.log("RENDER fixtures", fixtures.all.length);
        
        let competitions = this.state.competitions;
        let options = prepareOptions(competitions, "name");
        
        let selectedOptionId = this.state.selectedCompetitionId;

        return (
            <View style={{flex: 1, backgroundColor: '#fff'}}>
                <ScrollView style={{flex: 1}} contentContainerStyle={styles.container}>
                    <TopSection style={styles.topSection}>
                        <Text style={styles.title}>COMPETITION</Text>
                        <SelectBox
                            selectedOptionId={selectedOptionId}
                            handleSelect={(id) => this.selectCompetition(id)}
                            options={options} />
                    </TopSection>
                    <BackgroundWrapper style={{flex: 1, padding: 10}}>
                        
                        <FixturesList />

                    </BackgroundWrapper>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        // paddingTop: 20,
        // paddingBottom: 20,
    },
    topSection: {
        padding: 20,
        paddingBottom: 25,
    },
    title: {
        fontFamily: 'saira-regular',
        fontSize: 13,
        marginBottom: 5
    },
    titleStyle: {
        fontFamily: 'saira-semibold'
    },    
});

const mapStateToProps = (state) => {
    
    let fixtures = state.fixtures;

    return {
        fixtures 
    }
}

export default connect(mapStateToProps)(FixturesHomeScreen);