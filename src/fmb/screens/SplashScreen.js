import React, { Component } from 'react';
import { View, StyleSheet, Text, Platform, TouchableOpacity, ScrollView } from 'react-native';
import { headerStyle, headerTitleStyle, headerLeftStyle } from '../styles';
import HeaderLeft from '../components/HeaderLeft';

export default class SplashScreen extends Component {

    static navigationOptions = () => ({
        headerTitle: 'Splash Screen',
        headerStyle,
        headerTitleStyle,
        headerLeftStyle,
        header: <View />
        // headerLeft: <HeaderLeft />
    });

    render() {
        const { navigation } = this.props;
        return (
            <ScrollView style={{margin: 20}}>
                <Text style={{marginTop: 20}}> Expo Screen </Text>

                <TouchableOpacity 
                    activeOpacity={0.5}
                    onPress={() => navigation.navigate('ResultsHome')}
                    style={{backgroundColor: '#445599', padding: 20, borderRadius: 3, marginTop: 30}}>
                    <Text style={{fontSize: 15, color: '#fff'}}>ResultsHome</Text>
                </TouchableOpacity> 

                <TouchableOpacity 
                    activeOpacity={0.5}
                    onPress={() => navigation.navigate('TablesHome')}
                    style={{backgroundColor: '#445599', padding: 20, borderRadius: 3, marginTop: 30}}>
                    <Text style={{fontSize: 15, color: '#fff'}}>TablesHome</Text>
                </TouchableOpacity> 

                <TouchableOpacity 
                    activeOpacity={0.5}
                    onPress={() => navigation.navigate('newsHome')}
                    style={{backgroundColor: '#445599', padding: 20, borderRadius: 3, marginTop: 30}}>
                    <Text style={{fontSize: 15, color: '#fff'}}>newsHome</Text>
                </TouchableOpacity> 

                <TouchableOpacity 
                    activeOpacity={0.5}
                    onPress={() => navigation.navigate('newsRead')}
                    style={{backgroundColor: '#445599', padding: 20, borderRadius: 3, marginTop: 30}}>
                    <Text style={{fontSize: 15, color: '#fff'}}>newsRead</Text>
                </TouchableOpacity>

                <TouchableOpacity 
                    activeOpacity={0.5}
                    onPress={() => navigation.navigate('FixturesHome')}
                    style={{backgroundColor: '#445599', padding: 20, borderRadius: 3, marginTop: 30}}>
                    <Text style={{fontSize: 15, color: '#fff'}}>FixturesHome</Text>
                </TouchableOpacity>

                <TouchableOpacity 
                    activeOpacity={0.5}
                    onPress={() => navigation.navigate('ClubSelect')}
                    style={{backgroundColor: '#445599', padding: 20, borderRadius: 3, marginTop: 30}}>
                    <Text style={{fontSize: 15, color: '#fff'}}>Select Club</Text>
                </TouchableOpacity>

                <TouchableOpacity 
                    activeOpacity={0.5}
                    onPress={() => navigation.navigate('EmailSubscribe')}
                    style={{backgroundColor: '#445599', padding: 20, borderRadius: 3, marginTop: 30}}>
                    <Text style={{fontSize: 15, color: '#fff'}}>Email Subscribe</Text>
                </TouchableOpacity>

                <TouchableOpacity 
                    activeOpacity={0.5}
                    onPress={() => navigation.navigate('AppHome')}
                    style={{backgroundColor: '#445599', padding: 20, borderRadius: 3, marginTop: 30}}>
                    <Text style={{fontSize: 15, color: '#fff'}}>Go to App Home Screen</Text>
                </TouchableOpacity>

            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        flex: 1,
        padding: 20
    }
});