import React, { Component } from 'react';
import { View, StyleSheet, ScrollView, Platform } from 'react-native';
import { headerStyle, headerTitleStyle, theme } from '../styles';
import { connect } from 'react-redux';
import Col from '../components/common/Col';
import Carret from '../components/common/Carret';
import Footer from '../components/Footer';
import Icon from '../components/Icon';
import Header from '../components/Header';
import MenuIcon from '../components/MenuIcon';
import Row from '../components/common/Row';
import StatusBar from '../components/common/StatusBar';
import Text from '../components/common/Text';
import * as types from '../actions/types';

class AppHomeScreen extends Component {
    
    static navigationOptions = ({ navigation }) => { 
        let header = <Header 
                        titleText="football my bae" 
                        Left={<Carret />}
                        leftStyle={{paddingLeft: 14, width: 60}}
                        leftButtonAction={() => console.log("toggle drawer navigation")} />
        return {
            header
        }
    };

    render() {
        const { navigation, homeScreenState, setActiveTab } = this.props;
        let activeTab = homeScreenState.activeTab;
        const { navigate } = navigation;

        return (
            <View style={{flex: 1}}>
                <ScrollView style={{flex: 1}} contentContainerStyle={styles.container}>
                    <Row style={styles.title}>
                        <Text style={styles.titleText}>Hey football lover!</Text>
                    </Row>
                    <Row style={styles.menuIconRow}>
                        <MenuIcon onPress={() => navigate('FixturesHome') } iconSource={require('../assets/img/fixture_icon.png')} text="check fixtures" />
                        <MenuIcon iconSource={require('../assets/img/result_icon.png')} text="check results" />
                    </Row>
                    <Row style={styles.menuIconRow}>
                        <MenuIcon iconSource={require('../assets/img/table_icon.png')} text="check tables" />
                        <MenuIcon iconSource={require('../assets/img/news_icon.png')} text="read news" />
                    </Row>
                    <Row style={[styles.menuIconRow]}>
                        <MenuIcon iconSource={require('../assets/img/bae_club_icon.png')} text="bae club" />
                        <MenuIcon style={{opacity: 0}} iconSource={require('../assets/img/bae_club_icon.png')} text="bae club" />
                    </Row>
                </ScrollView>
                <Footer activeTab={activeTab} setActiveTab={setActiveTab} />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        paddingTop: 20,
        paddingBottom: 20,
    },
    menuIconRow: {
        justifyContent: 'space-around',
        marginBottom: 20
    },
    titleText: {
        fontSize: 25,
        fontFamily: 'saira-bold',
    },
    title: {
        marginTop: 0,
        marginBottom: 20,
        paddingLeft: 15
    },
});

const mapStateToProps = (state) => {
    return {
        homeScreenState: state.interactions.homeScreenActions
    }
}

const mapDispatchToProps = (dispatch, thisProps) => {
    let setActiveTab = (name) => {
        dispatch({type: types.CHANGE_ACTIVE_HOMESCREEN_TAB, payload: name});
    }
    return { setActiveTab }
}

export default connect(mapStateToProps, mapDispatchToProps)(AppHomeScreen);
