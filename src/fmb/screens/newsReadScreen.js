import React, { Component } from 'react';
import { View, StyleSheet, ScrollView, Platform } from 'react-native';
import { headerStyle, headerTitleStyle, theme } from '../styles';
import { connect } from 'react-redux';
import Col from '../components/common/Col';
import ClubLogo from '../components/common/ClubLogo';
import BackgroundWrapper from '../components/common/BackgroundWrapper';
import Footer from '../components/Footer';
import Header from '../components/Header';
import Icon from '../components/Icon';
import LayoutHeader from '../components/common/LayoutHeader';
import LayoutBody from '../components/common/LayoutBody';
import Left from '../components/common/Left';
import Right from '../components/common/Right';
import MenuIcon from '../components/MenuIcon';
import Row from '../components/common/Row';
import SelectBox from '../components/SelectBox';
import Text from '../components/common/Text';
import TopSection from '../components/ScreenTopSection';
import { prepareOptions } from '../helpers';
import FixtureResultCard from '../components/FixtureResultCard';
import HSpace from '../components/common/HSpace';
import BannerImage from '../components/BannerImage';


class newsReadScreen extends Component {
    
    state = {
        post: {
            id: 4,
            category: 'Champions League',
            title: 'Zidane hails Bale and Ronaldo',
            date: 'Tues, 26 Sep 2017',
            time: '17:00',
            postImage: require('../assets/img/zidane.jpg'),
            postBannerImage: {
               source: require('../assets/img/zidane_banner1.jpg'),
               width: 374,
               height: 164,
            },
            fullText: '',
        },
    }

    static navigationOptions = ({ navigation }) => { 

        return {
            header: <View />
        }
    };
    

    render() {
        
        let { navigation } = this.props;
        let { post } = this.state;

        return (
            <View style={{flex: 1, backgroundColor: '#fff'}}>
                <Header titleStyle={styles.titleStyle} titleText="Zidance hails Bale" noShadow={true} leftButtonAction={navigation.goBack} />
                <ScrollView style={{flex: 1}} contentContainerStyle={styles.container}>
                    <TopSection style={styles.topSection}>
                        <View style={styles.container}>
                            <BannerImage source={post.postBannerImage.source} width={post.postBannerImage.width} height={post.postBannerImage.height} />
                            <View style={styles.titleStrip}>
                                <Text style={styles.postTitle}>Zidance Hails Bale and Ronaldo</Text>
                            </View>
                            <View style={styles.postDate}>
                                <Text style={styles.postDateText}>{post.date}  |  {post.time}</Text>
                            </View>
                        </View>                    
                    </TopSection>
                    <BackgroundWrapper style={{flex: 1, padding: 10, minHeight: 1000}}>

                        <View style={styles.postContentArea}>
                            <Text style={styles.postContent}>
                                Lorem ipsum, dolor sit amet consectetur adipisicing elit. Totam placeat tempore animi. Iusto eum nobis voluptas ipsam commodi, molestiae tenetur quis, ex molestias ullam labore natus, expedita ipsa ut consequuntur?
                                Quis natus vero corporis earum non nam quam, ipsa excepturi! Praesentium odio doloribus aliquam cumque beatae vel! Labore eius, sit nemo sed maxime placeat fuga. Aut ab maxime odio cupiditate.
                                Quod quam tempore a autem quia reprehenderit, natus eos velit corrupti unde quos temporibus obcaecati. Quis ullam, quo dolorem, natus a eveniet id hic debitis tempora soluta, maiores nisi dolorum.
                                
                                r sit amet consectetur adipisicing elit. Totam placeat tempore animi. Iusto eum nobis voluptas ipsam commodi, molestiae tenetur quis, ex molestias ullam labore natus, expedita ipsa ut consequuntur?
                                Quis natus vero corporis earum non nam quam, ipsa excepturi! Praesentium odio doloribus aliquam cumque beatae vel! Labore eius, sit nemo sed maxime placeat fuga. Aut ab maxime odio cupiditate.
                                Quod quam tempore a autem quia reprehenderit, natus eos velit corrupti unde quos temporibus obcaecati. Quis ullam, quo dolorem, natus a eveniet id hic debitis tempora soluta, maiores nisi dolorum.
                                

                                r sit amet consectetur adipisicing elit. Totam placeat tempore animi. Iusto eum nobis voluptas ipsam commodi, molestiae tenetur quis, ex molestias ullam labore natus, expedita ipsa ut consequuntur?
                                Quis natus vero corporis earum non nam quam, ipsa excepturi! Praesentium odio doloribus aliquam cumque beatae vel! Labore eius, sit nemo sed maxime placeat fuga. Aut ab maxime odio cupiditate.
                                Quod quam tempore a autem quia reprehenderit, natus eos velit corrupti unde quos temporibus obcaecati. Quis ullam, quo dolorem, natus a eveniet id hic debitis tempora soluta, maiores nisi dolorum.

                                r sit amet consectetur adipisicing elit. Totam placeat tempore animi. Iusto eum nobis voluptas ipsam commodi, molestiae tenetur quis, ex molestias ullam labore natus, expedita ipsa ut consequuntur?
                                Quis natus vero corporis earum non nam quam, ipsa excepturi! Praesentium odio doloribus aliquam cumque beatae vel! Labore eius, sit nemo sed maxime placeat fuga. Aut ab maxime odio cupiditate.
                                Quod quam tempore a autem quia reprehenderit, natus eos velit corrupti unde quos temporibus obcaecati. Quis ullam, quo dolorem, natus a eveniet id hic debitis tempora soluta, maiores nisi dolorum.
                            </Text>
                        </View>

                    </BackgroundWrapper>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        // paddingTop: 20,
        // paddingBottom: 20,
    },
    topSection: {
        padding: 0,
        paddingLeft: 0,
        paddingRight: 0,
        paddingBottom: 0,
    },
    titleStrip: {
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(0,0,0,0.5)',
        marginTop: -50,
    },
    postTitle: {
        fontFamily: 'saira-bold',
        color: 'white',
        fontSize: 14
    },
    postDate: {
        height: 30,
        justifyContent: 'center',
        alignItems: 'center'
    },
    postDateText: {
        fontSize: 12,
        color: '#777777'
    },
    postContentArea: {
        backgroundColor: 'white',
        padding: 20,
    },
    postContent: {
        fontSize: 14
    }
    
});

const mapStateToProps = (state) => {
    
    let fixtures = state.fixtures;

    return {
        fixtures 
    }
}

export default connect(mapStateToProps)(newsReadScreen);