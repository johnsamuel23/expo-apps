import React, { Component } from 'react';
import { View, StyleSheet, ScrollView, Platform } from 'react-native';
import { headerStyle, headerTitleStyle, theme } from '../styles';
import { connect } from 'react-redux';
import Col from '../components/common/Col';
import ClubLogo from '../components/common/ClubLogo';
import BackgroundWrapper from '../components/common/BackgroundWrapper';
import Footer from '../components/Footer';
import Header from '../components/Header';
import Icon from '../components/Icon';
import LayoutHeader from '../components/common/LayoutHeader';
import LayoutBody from '../components/common/LayoutBody';
import Left from '../components/common/Left';
import Right from '../components/common/Right';
import MenuIcon from '../components/MenuIcon';
import Row from '../components/common/Row';
import SelectBox from '../components/SelectBox';
import Text from '../components/common/Text';
import TopSection from '../components/ScreenTopSection';
import { prepareOptions } from '../helpers';
import FixtureResultCard from '../components/FixtureResultCard';
import HSpace from '../components/common/HSpace';


class ResultsHomeScreen extends Component {
    
    state = {
        competitions: [
            {
                id: 1,
                name: "UEFA Champions League",
                abbreviation: ''
            },
            {
                id: 2,
                name: "La Liga",
                abbreviation: ''
            },
            {
                id: 3,
                name: "UEFA Champions League",
                abbreviation: ''
            },
            {
                id: 4,
                name: "La Liga",
                abbreviation: ''
            },
            {
                id: 5,
                name: "UEFA Champions League",
                abbreviation: ''
            },
            {
                id: 6,
                name: "La Liga",
                abbreviation: ''
            },
            {
                id: 7,
                name: "UEFA Champions League",
                abbreviation: ''
            },
            {
                id: 8,
                name: "La Liga",
                abbreviation: ''
            },
            {
                id: 9,
                name: "UEFA Champions League",
                abbreviation: ''
            },
            {
                id: 10,
                name: "La Liga",
                abbreviation: ''
            },
            {
                id: 11,
                name: "UEFA Champions League",
                abbreviation: ''
            },
            {
                id: 12,
                name: "La Liga",
                abbreviation: ''
            },
        ],
        seasons: [
            {
                id: 1,
                name: "2017/2018",
                abbreviation: ''
            },
            {
                id: 2,
                name: "2015/2016",
                abbreviation: ''
            },
        ],
        filters: [
            {
                id: 1,
                name: "All clubs",
            },
            {
                id: 2,
                name: "Home",
            },
            {
                id: 2,
                name: "Away",
            },
        ],
        selectedCompetitionId: 1,
        fixtures: [],
        results: [
            {
                id: 1,
                date: 'Sat Sept 30, 2017',
                homeTeam: {
                    score: 2,
                    abbrevName: 'AC MILAN',
                    clubLogo: require('../assets/img/ac_milan.png'),
                },
                awayTeam: {
                    score: 0,
                    abbrevName: 'Arsenal',
                    clubLogo: require('../assets/img/arsenal.png'),
                },
            },
            {
                id: 2,
                date: 'Sat Sept 30, 2017',
                homeTeam: {
                    score: 3,
                    abbrevName: 'Man U',
                    clubLogo: require('../assets/img/manchester_united.png'),
                },
                awayTeam: {
                    score: 1,
                    abbrevName: 'Liverpool',
                    clubLogo: require('../assets/img/liverpool.png'),
                },
            },
            {
                id: 3,
                date: 'Wed Sept 28, 2017',
                homeTeam: {
                    score: 2,
                    abbrevName: 'Barca',
                    clubLogo: require('../assets/img/barcelona.png'),
                },
                awayTeam: {
                    score: 0,
                    abbrevName: 'Chelsea',
                    clubLogo: require('../assets/img/chelsea.png'),
                },
            },
        ]
    }

    static navigationOptions = ({ navigation }) => { 
        let header = <Header titleStyle={styles.titleStyle} titleText="results" noShadow={true} leftButtonAction={navigation.goBack} />;

        return {
            header
        }
    };
    
    selectCompetition(id) {
        this.setState({
            selectedCompetitionId: id
        });
    }

    render() {
        const { navigation, fixtures } = this.props;
        console.log("RENDER fixtures", fixtures.all.length);
        
        let competitions = this.state.competitions;
        let seasons = this.state.seasons;
        let filters = this.state.filters;

        let seasonOptions = prepareOptions(seasons, 'name');
        let options = prepareOptions(competitions, "name");
        let filterOptions = prepareOptions(filters, "name");
        
        let selectedOptionId = this.state.selectedCompetitionId;

        let fixtureResults = this.state.results.map((result, index) => {
            return <FixtureResultCard key={index} 
                    homeTeam={result.homeTeam}
                    awayTeam={result.awayTeam}
                    date={result.date}
                    />
        });

        return (
            <View style={{flex: 1, backgroundColor: '#fff'}}>
                <ScrollView style={{flex: 1}} contentContainerStyle={styles.container}>
                    <TopSection style={styles.topSection}>
                        <Text style={styles.title}>COMPETITION</Text>
                        <SelectBox
                            selectedOptionId={selectedOptionId}
                            handleSelect={(id) => this.selectCompetition(id)}
                            options={options} />

                        <View style={{flexDirection: 'row', marginTop: 20}}>
                            <View style={{flex: 1}}>
                                <Text style={styles.title}>SEASON</Text>
                                <SelectBox
                                    selectedOptionId={selectedOptionId}
                                    handleSelect={(id) => this.selectCompetition(id)}
                                    options={seasonOptions} />
                            </View>
                            <HSpace size={20} />
                            <View style={{flex: 1}}>
                                <Text style={styles.title}>CLUB</Text>
                                <SelectBox
                                    selectedOptionId={selectedOptionId}
                                    handleSelect={(id) => this.selectCompetition(id)}
                                    options={filterOptions} />
                            </View>
                        </View>
                    </TopSection>
                    <BackgroundWrapper style={{flex: 1, padding: 10}}>
                        
                        { fixtureResults }
                        { fixtureResults }

                    </BackgroundWrapper>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        // paddingTop: 20,
        // paddingBottom: 20,
    },
    topSection: {
        padding: 20,
        paddingBottom: 25,
    },
    title: {
        fontFamily: 'saira-regular',
        fontSize: 13,
        marginBottom: 5
    },
    titleStyle: {
        fontFamily: 'saira-semibold'
    },    
});

const mapStateToProps = (state) => {
    
    let fixtures = state.fixtures;

    return {
        fixtures 
    }
}

export default connect(mapStateToProps)(ResultsHomeScreen);