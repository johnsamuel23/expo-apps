import React from 'react';
import { View, Text } from 'react-native';
import SplashScreen from '../screens/SplashScreen';
import StartScreen from '../screens/StartScreen';
import { TabNavigator, StackNavigator } from 'react-navigation';

const RouteConfigs = {
    Splash: { screen: SplashScreen },
    Start: { screen: StartScreen }, 
}

const StackNavigatorConfig = {
    initialRouteName: 'Start',
    mode: 'modal'
}
const RootNavigator = StackNavigator(RouteConfigs, StackNavigatorConfig);

export default RootNavigator;              