export const prepareOptions = (data, valueField) => {
    let options = [];
    for(let i=0; i < data.length; i++) {
        let value = {
            id: data[i]["id"],
            value: data[i][valueField],
        }

        options.push(value);
    }

    return options;
}