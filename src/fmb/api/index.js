import axios from 'axios';
export let apiHost = process.env.NODE_ENV === 'production' ? 'http://footballmybae.com' : 'http://192.168.107.1:8080/fmb/public';
export let apiRoot = apiHost;

export const getFixtures = () => {
    return axios.get(apiRoot + "/fixtures");
}
