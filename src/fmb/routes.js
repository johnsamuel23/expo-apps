import SplashScreen from './screens/SplashScreen';
import StartScreen from './screens/StartScreen';
import TabScreen from './screens/TabScreen';
import ClubSelectionScreen from './screens/ClubSelectionScreen';
import EmailSubscribeScreen from './screens/EmailSubscribeScreen';
import AppHomeScreen from './screens/AppHomeScreen';
import FixturesHomeScreen from './screens/FixturesHomeScreen';
import TablesHomeScreen from './screens/TablesHomeScreen';
import ResultsHomeScreen from './screens/ResultsHomeScreen';
import newsHomeScreen from './screens/newsHomeScreen';
import newsReadScreen from './screens/newsReadScreen';

export default routes = {
    Splash: { screen: SplashScreen },
    newsHome: { screen: newsHomeScreen },
    newsRead: { screen: newsReadScreen },
    FixturesHome: { screen: FixturesHomeScreen },
    TablesHome: { screen: TablesHomeScreen },
    ResultsHome: { screen: ResultsHomeScreen },
    EmailSubscribe: { screen: EmailSubscribeScreen, navigationOptions: {header: false} },
    AppHome: { screen: AppHomeScreen },
    ClubSelect: { screen: ClubSelectionScreen, navigationOptions: {header: false} },
    Start: { screen: StartScreen },
    // TabScreen: { screen: TabScreen }, 
}