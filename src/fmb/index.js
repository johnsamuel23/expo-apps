import React, { Component } from 'react';
import Routes from './routes';
import { getFixtures } from './actions';
import { View, Text, Image, StyleSheet } from 'react-native';
import { AppLoading, Asset, Font } from 'expo';
import { StackNavigator } from 'react-navigation';
import { Provider } from 'react-redux';
import store from './store';

const RootNavigator = StackNavigator(Routes);

export default class App extends Component {

    cacheImages(images) {
        return images.map(image => {
            if (typeof image === 'string') {
                return Image.prefetch(image);
            } else if (typeof image != null) {
                // return Asset.fromModule(image).downloadAsync();
            }
        });
    }

    cacheLocalImages(images) {
        return images.map(image => {
            if (typeof image === 'string') {
                return Image.prefetch(image);
            } else {
                return Asset.fromModule(image).downloadAsync();
            }
        });
    }

    state = {
        localImages: [
            require("./assets/img/back_arrow.png"),
            require("./assets/img/AppLogo.png"),
            require("./assets/img/ac_milan.png"),
            require("./assets/img/arsenal.png"),
            require("./assets/img/bayern_munich.png"),
            require("./assets/img/barcelona.png"),
            require("./assets/img/chelsea.png"),
            require("./assets/img/crystal_palace.png"),
            require("./assets/img/juventus.png"),
            require("./assets/img/liverpool.png"),
            require("./assets/img/manchester_city.png"),
            require("./assets/img/mark_badge.png"),
            require("./assets/img/fixture_icon.png"),
            require("./assets/img/result_icon.png"),
            require("./assets/img/table_icon.png"),
            require("./assets/img/news_icon.png"),
            require("./assets/img/bae_club_icon.png"),
            require("./assets/img/home_icon.png"),
            require("./assets/img/home_icon_inactive.png"),
            require("./assets/img/conversation_icon.png"),
            require("./assets/img/conversation_icon_inactive.png"),
            require("./assets/img/profile_icon.png"),
            require("./assets/img/profile_icon_inactive.png"),
            require("./assets/img/manchester_united.png"),
            require("./assets/img/splash_pic.png"),
        ]
    }

    componentWillMount() {

        const { dispatch, getState } = store;

        dispatch(getFixtures());

        // console.log("Redux Store::=>", getState());

        this.cacheLocalImages(this.state.localImages);
    }

    render() {
        return (
            <Provider store={store}>
                <View style={styles.container}>
                    <RootNavigator />
                </View>
            </Provider>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    }
});

// export default RootNavigator;