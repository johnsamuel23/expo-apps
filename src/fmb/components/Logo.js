import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import LocalImage from './common/LocalImage';

export default Logo = ({
    size
}) => {
  
    // if(!size) {
    //     size = 50;
    // }

    size = size === undefined ? 50 : size;

    return (
        <View>
            <LocalImage 
                originalHeight={512}
                originalWidth={512}
                guideWidth={size}
                source={require('../assets/img/AppLogo.png')}
            />
        </View>
    );
}


let styles = StyleSheet.create({
    
})