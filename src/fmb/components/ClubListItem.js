import React from 'react';
import { StyleSheet, View } from 'react-native';
import Text from './common/Text';
import ClubLogo from './common/ClubLogo';
import Touch from './common/Touch';

export default Component = ({
    club,
    isLast
}) => {
    let { clubLogo, clubName } = club;
    let borderBottomWidth = isLast ? 0 : 1;

    return (
        <Touch action={() => console.log("list item clicked")}>
            <View style={[styles.container, {borderBottomWidth}]}>
                <ClubLogo source={clubLogo} />
                <View style={styles.clubNameContainer}>
                    <Text style={styles.clubNameText}>{clubName}</Text>
                </View>
            </View>
        </Touch>
    )
};

const styles = StyleSheet.create({
    container: {
        height: 70,
        // borderBottomWidth: 1,
        borderBottomColor: "#ccc",
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: 'white',
        paddingLeft: 15,
        paddingLeft: 15,
    },
    clubNameContainer: {
        marginLeft: 10,
    }
});