import React from 'react';
import { Text, StyleSheet } from 'react-native';

export default Component = ({
    children,
    style
}) => {
    return (
        <Text style={[styles.text, style]}>
            { children }
        </Text>
    );
}

const styles = StyleSheet.create({
    text: {
        fontFamily: 'saira-regular',
        fontSize: 13,
        color: '#1f1f1f'
    }
});