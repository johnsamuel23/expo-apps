import React from 'react';
import { View, StyleSheet } from 'react-native';

export default Component = ({
    children,
    style
}) => {
    return (
        <View style={[styles.container, style]}>
            { children }
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        maxWidth: 100,
        justifyContent: 'center'
    }
});