import React from 'react';
import { View, StyleSheet } from 'react-native';
import { theme } from '../../styles';

export default Component = ({
    height
}) => {
    let { container} = extraStyles;

    height = height ? height : 25;
    // extraStyles.container.width = size;

    return (
        <View style={[styles.container, {height: height}]}>
        </View>
    );
}

let extraStyles = {
    container: {},
};

const styles = StyleSheet.create({
    container: {
        width: 1,
        height: 25,
        backgroundColor: theme.backgroundColor,
        alignSelf: 'center'
    },
});