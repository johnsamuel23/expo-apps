import React from 'react';
import { View, StyleSheet, TouchableNativeFeedback, TouchableOpacity, Platform } from 'react-native';

export default Component = ({
    children,
    style,
    action,
    opaque
}) => {

    action = action ? action : () => {};
    
    if(Platform.OS === "android" && !opaque) {
        return (
            <View style={[styles.container, style]}>
                <TouchableNativeFeedback 
                    onPress={() => action()}
                    background={TouchableNativeFeedback.Ripple('rgba(0,0,0,0.2)')}>
                        { children }
                </TouchableNativeFeedback>
            </View>
        );
    }
    
    return (
        <View style={[styles.container, style]}>
            <TouchableOpacity
                activeOpacity={0.6}
                onPress={() => action()}>
                    { children }
            </TouchableOpacity>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
    }
});