import React from 'react';
import { StyleSheet, View } from 'react-native';
import LocalImage from '../common/LocalImage';
import Menu from '../../utils/icons/Menu';
import { theme } from '../../styles';
import { STATUS_BAR_HEIGHT } from '../../constants';


export default Component = ({color}) => {
    let backgroundColor = color ? color : theme.brandColor;
    return (
        <View style={[styles.container, {backgroundColor}]}>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        height: STATUS_BAR_HEIGHT
    }
});