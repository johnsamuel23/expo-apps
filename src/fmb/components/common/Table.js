import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';

export default Table = ({
    children,
    style
}) => {
    return (
        <View style={[styles.container, style]}>
            { children }
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
    }
});