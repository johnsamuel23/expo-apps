import React from 'react';
import { StyleSheet, View } from 'react-native';
import LocalImage from '../common/LocalImage';
import SVGImage from 'react-native-svg-image';

export default Component = ({source, size, remote}) => {
    size = size ? size : 30;
    if(remote) {
        return ( 
            <SVGImage
                style={{ width: size, height: size }}
                source={source}
            />
        );
    }

    return (
        <View style={[styles.clubLogo, {width: size, height: size}]}>
            <LocalImage
                originalHeight={500}
                originalWidth={500}
                guideWidth={size}
                source={source}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    clubLogo: {
        height: 45,
        width: 45,
        justifyContent: 'center',
        alignItems: 'center',
    }
});