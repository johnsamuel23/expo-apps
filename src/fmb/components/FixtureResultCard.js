import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import Table from './common/Table';
import TableRow from './common/TableRow';
import TableCol from './common/TableCol';
import Left from './common/Left';
import Right from './common/Right';
import Center from './common/Center';
import Logo from './common/Logo';
import Text from './common/Text';
import Touch from './common/Touch';
import VLine from './common/VLine';
import { theme } from '../styles';

export default FixtureResultCard = ({
    homeTeam, awayTeam, date
}) => {

    let footballIcon = <Logo source={require('../assets/img/football_icon.png')} 
                            size={20} 
                        />

    return (
        <View style={[styles.container]}>
            <Table>
                <TableRow>
                    <Left style={[styles.alignLeft, styles.headerLeft]}>
                    </Left>
                    <Center style={styles.headerCenter}>
                        <Text style={[styles.bold, styles.date]}>{date}</Text>
                    </Center>
                    <Right style={[styles.alignRight, styles.headerRight]}>
                        { footballIcon } 
                    </Right>
                </TableRow>
                <TableRow>
                    <TableCol style={[styles.alignRight, styles.logo, {marginRight: 10}]}> 
                        <Logo source={homeTeam.clubLogo} 
                            size={22} 
                        />
                    </TableCol>
                    <TableCol style={styles.club1}> 
                        <Text style={[styles.bold, styles.clubName]}>{homeTeam.abbrevName}</Text>
                    </TableCol>
                    <TableCol> 
                        <Text style={[styles.time, styles.bold]}>{homeTeam.score}-{awayTeam.score}</Text>
                    </TableCol>
                    <TableCol style={styles.club2}> 
                        <Text style={[styles.bold, styles.clubName]}>{awayTeam.abbrevName}</Text>
                    </TableCol>
                    <TableCol style={[styles.alignLeft, styles.logo, {marginLeft: 10}]}> 
                        <Logo source={awayTeam.clubLogo} 
                            size={22} 
                        />
                    </TableCol>
                </TableRow>
                <TableRow style={styles.actionsRow}>
                    <TableCol> 
                        <Touch style={styles.actionTouch}>
                            <View style={styles.actionTouchView}>
                                <Text style={[styles.actionText]}>View match details</Text>                            
                            </View>
                        </Touch> 
                    </TableCol>
                    <VLine height={20} />
                    <TableCol>
                        <Touch style={styles.actionTouch}>
                            <View style={styles.actionTouchView}>
                                <Text style={[styles.actionText, styles.bold]}>Join conversation</Text>
                            </View>
                        </Touch> 
                    </TableCol>
                </TableRow>
            </Table>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        borderRadius: 4,
        backgroundColor: 'white',
        marginBottom: 10
    },
    headerCenter: {
        flex: 1, 
        justifyContent: 'center', 
        alignItems: 'center'
    },
    headerLeft: {
        maxWidth: 50
    },
    headerRight: {
        maxWidth: 50
    },
    actionText: {
        color: theme.brandColor
    },
    actionTouch: {
        flex: 1, 
        marginTop: -5, 
        alignSelf: 'stretch',
    },
    actionTouchView: {
        flex: 1, 
        alignItems: 'center', 
        justifyContent: 'center'
    },
    actionsRow: {
        paddingHorizontal: 0, 
        borderTopWidth: 1,
        borderTopColor: theme.lightBackgroundColor,
        height: 40,
    },
    lastItem: {
        borderBottomLeftRadius: 4,
        borderBottomRightRadius: 4,
    },
    boldTest: {
        color: theme.almostDarkColor,
        fontFamily: 'saira-semibold'
    },
    even: {
        backgroundColor: 'transparent'
    },
    dataRow: {
        backgroundColor: theme.grayBackgroundColor,
        borderTopWidth: 1,
        borderTopColor: theme.grayBackgroundBorderColor,
    },
    alignRight: {
        alignItems: 'flex-end',
    },
    alignLeft: {
        alignItems: 'flex-start',
    },
    bold: {
        fontFamily: 'saira-semibold'
    },
    clubName: {
        color: '#555'
    },
    date: {
        color: '#555'
    },
    logo: {
        maxWidth: 40,
    },
    club1: {
        alignItems: 'flex-start'   
    },
    club2: {
        alignItems: 'flex-end'
    }
});