import React from 'react';
import { StyleSheet, Text, View , Dimensions} from 'react-native';
import LocalImage from './common/LocalImage';

export default BannerImage = ({
    size,
    source,
    height,
    width
}) => {
    let screenWidth = Dimensions.get('window').width;
    size = size === undefined ? screenWidth : size;
    height = height === undefined ? 164 : height;
    width = width === undefined ? 556 : width;

    return (
        <View>
            <LocalImage 
                originalHeight={height}
                originalWidth={width}
                guideWidth={size}
                source={source}
            />
        </View>
    );
}


let styles = StyleSheet.create({
    
})