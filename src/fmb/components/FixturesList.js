import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';
import FixtureGroup from './FixtureGroup';

export default class FixturesList extends Component {
    render() {
        return (
            <View style={styles.container}>

                <FixtureGroup />
                <FixtureGroup />
                <FixtureGroup />
                <FixtureGroup />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});