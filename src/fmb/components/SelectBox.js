import React, { Component } from 'react';
import { StyleSheet, View, TouchableOpacity, ScrollView } from 'react-native';
import Text from './common/Text';
import SelectDrop from '../utils/icons/SelectDrop';
import Modal from 'react-native-modal';
import Touch from './common/Touch';

export default class SelectBox extends Component {
    
    state = {
        isModalVisible: false
    }
    
    _showModal = () => this.setState({ isModalVisible: true })

    _hideModal = () => this.setState({ isModalVisible: false })

    render() {
        let { style, selectedOptionId, handleSelect, options} = this.props;
        let selectedOption = options.find(opt => opt.id === selectedOptionId) || {};
        let optionList = options.map((option, index) => {
            let id = option.id;
            return (
                <Touch key={index} action={() => {handleSelect(id); this._hideModal()}}>
                    <View style={[styles.optionContainer]}>
                        <Text style={styles.optionText}> {option.value} </Text>
                    </View>
                </Touch>
            )
        });
        
        return (
            <View>
                <TouchableOpacity
                    activeOpacity={0.6}
                    style={styles.container}
                    onPress={() => this._showModal()}
                >
                    <View style={styles.label}>
                        <Text style={styles.labelText}>{selectedOption.value}</Text>
                    </View>
                    <View style={styles.dropIcon}>
                        <SelectDrop color="#777" size={20} />
                    </View>
                </TouchableOpacity>
                <Modal 
                    style={{margin: 20, borderRadius: 10}}
                    isVisible={this.state.isModalVisible}>
                    <View style={{ flex: 1 }}>
                        <TouchableOpacity
                            style={{backgroundColor: '#999999', padding: 10}}
                            activeOpacity={0.9}
                            onPress={() => this._hideModal()}>
                            <Text>Cancel</Text>
                        </TouchableOpacity>
                        <ScrollView>

                            { optionList }

                        </ScrollView>
                    </View>
                </Modal>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        height: 45,
        justifyContent: 'center',
        borderRadius: 3,
        borderWidth: 1,
        borderColor: '#777',
        paddingLeft: 10,
        paddingRight: 10
    },
    label: {
        flex: 1,
        justifyContent: 'center',
    },
    labelText: {
        fontFamily: 'saira-regular'
    },
    dropIcon: {
        alignItems: 'flex-end',
        justifyContent: 'center',        
        width: 50
    },
    optionContainer: {
        padding: 10, 
        borderTopWidth: 1, 
        borderTopWidth: 1, 
        borderTopColor: '#f1f1f1',
        backgroundColor: '#fff'
    },
    optionText: {
        fontFamily: 'saira-regular'
    }
});