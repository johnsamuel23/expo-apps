import React from 'react';
import { View, StyleSheet } from 'react-native';
import Text from './common/Text';
import RoundedImage from './common/RoundedImage';
import Touch from './common/Touch';

export default MenuIcon = ({
    text,
    iconSource,
    style,
    onPress
}) => {
    let height = 100;
    let borderRadius = height / 2;
    return (
        <View style={[styles.container, style]}>
            <Touch opaque style={{borderRadius, overflow: 'hidden'}}
                action={onPress}>
                <View style={[styles.container, {borderRadius}]}>
                    <RoundedImage 
                        imageSize={70}
                        source={iconSource}
                        height={height}
                        style={{
                            backgroundColor: '#e8e8e8'
                        }}
                    />
                </View>
                <Text style={styles.text}>{text}</Text>
            </Touch>
        </View>
    );
}


const styles = StyleSheet.create({
    container: {
        // flex: 1
    },
    text: {
        color: '#1d1d1d',
        marginTop: 5,
        textAlign: 'center'
    }

});