import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import LocalImage from './common/LocalImage';

export default PostImage = ({
    size,
    source
}) => {

    size = size === undefined ? 100 : size;

    return (
        <View>
            <LocalImage 
                originalHeight={512}
                originalWidth={512}
                guideWidth={size}
                source={source}
                style={{
                    overflow: 'hidden'
                }}
            />
        </View>
    );
}


let styles = StyleSheet.create({
    
})