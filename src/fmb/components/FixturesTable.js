import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import Table from './common/Table';
import TableRow from './common/TableRow';
import TableCol from './common/TableCol';
import Left from './common/Left';
import Right from './common/Right';
import Logo from './common/Logo';
import Text from './common/Text';
import { theme } from '../styles';

let HeaderText = ({children, style}) => {
    return (
        <Text style={{fontFamily: 'saira-semibold', color: '#555'}}> { children} </Text>
    )
}

export default class FixturesTable extends Component {

    render() {

        let footballIcon = <Logo source={require('../assets/img/football_icon.png')} 
                                size={25} 
                            />
        let clubIcon1 = <Logo source={require('../assets/img/arsenal.png')} 
                                size={25} 
                            />
        let clubIcon2 = <Logo source={require('../assets/img/ac_milan.png')} 
                                size={25} 
                            />

        return (
            <View style={[styles.container]}>
                <Table>
                    <TableRow>
                        <TableCol>
                            <HeaderText>Pos</HeaderText>
                        </TableCol>
                        <TableCol style={[styles.clubCol]}>
                            <HeaderText>Club</HeaderText>
                        </TableCol>
                        <TableCol>
                            <HeaderText>P</HeaderText>
                        </TableCol>
                        <TableCol>
                            <HeaderText>W</HeaderText>
                        </TableCol>
                        <TableCol>
                            <HeaderText>D</HeaderText>
                        </TableCol>
                        <TableCol>
                            <HeaderText>L</HeaderText>
                        </TableCol>
                        <TableCol>
                            <HeaderText>GD</HeaderText>
                        </TableCol>
                        <TableCol>
                            <HeaderText>Pts</HeaderText>
                        </TableCol>
                    </TableRow>
                    <TableRow style={styles.dataRow}>
                        <TableCol>
                            <HeaderText>1</HeaderText>
                        </TableCol>
                        <TableCol style={styles.clubCol}>
                            <TableRow>
                                <TableCol>
                                    <View style={[styles.circle]}></View>
                                </TableCol>
                                <TableCol style={[styles.alignRight, styles.logo, {marginRight: 1}]}> 
                                    <Logo source={require('../assets/img/ac_milan.png')} 
                                        size={25} 
                                    />
                                </TableCol>
                                <TableCol style={styles.club1}> 
                                    <Text style={[styles.bold, styles.clubName]}>ACM</Text>
                                </TableCol>
                            </TableRow>
                        
                        </TableCol>
                        <TableCol style={styles.figures}>
                            <Text style={styles.values}>6</Text>
                        </TableCol>
                        <TableCol style={styles.figures}>
                            <Text style={styles.values}>5</Text>
                        </TableCol>
                        <TableCol style={styles.figures}>
                            <Text style={styles.values}>1</Text>
                        </TableCol>
                        <TableCol style={styles.figures}>
                            <Text style={styles.values}>0</Text>
                        </TableCol>
                        <TableCol style={styles.figures}>
                            <Text style={styles.values}>19</Text>
                        </TableCol>
                        <TableCol style={styles.figures}>
                            <Text style={styles.values}>16</Text>
                        </TableCol>
                    </TableRow>
                    <TableRow style={[styles.dataRow, styles.even]}>
                        <TableCol>
                            <HeaderText>2</HeaderText>
                        </TableCol>
                        <TableCol style={styles.clubCol}>
                            <TableRow>
                                <TableCol>
                                    <View style={[styles.circle]}></View>
                                </TableCol>
                                <TableCol style={[styles.alignRight, styles.logo, {marginRight: 1}]}> 
                                    <Logo source={require('../assets/img/arsenal.png')} 
                                        size={25} 
                                    />
                                </TableCol>
                                <TableCol style={styles.club1}> 
                                    <Text style={[styles.bold, styles.clubName]}>ARS</Text>
                                </TableCol>
                            </TableRow>
                        
                        </TableCol>
                        <TableCol style={styles.figures}>
                            <Text style={styles.values}>6</Text>
                        </TableCol>
                        <TableCol style={styles.figures}>
                            <Text style={styles.values}>5</Text>
                        </TableCol>
                        <TableCol style={styles.figures}>
                            <Text style={styles.values}>1</Text>
                        </TableCol>
                        <TableCol style={styles.figures}>
                            <Text style={styles.values}>0</Text>
                        </TableCol>
                        <TableCol style={styles.figures}>
                            <Text style={styles.values}>15</Text>
                        </TableCol>
                        <TableCol style={styles.figures}>
                            <Text style={styles.values}>16</Text>
                        </TableCol>
                    </TableRow>
                    <TableRow style={styles.dataRow}>
                        <TableCol>
                            <HeaderText>3</HeaderText>
                        </TableCol>
                        <TableCol style={styles.clubCol}>
                            <TableRow>
                                <TableCol>
                                    <View style={[styles.circle]}></View>
                                </TableCol>
                                <TableCol style={[styles.alignRight, styles.logo, {marginRight: 1}]}> 
                                    <Logo source={require('../assets/img/bayern_munich.png')} 
                                        size={25} 
                                    />
                                </TableCol>
                                <TableCol style={styles.club1}> 
                                    <Text style={[styles.bold, styles.clubName]}>BYM</Text>
                                </TableCol>
                            </TableRow>
                        </TableCol>
                        <TableCol style={styles.figures}>
                            <Text style={styles.values}>6</Text>
                        </TableCol>
                        <TableCol style={styles.figures}>
                            <Text style={styles.values}>4</Text>
                        </TableCol>
                        <TableCol style={styles.figures}>
                            <Text style={styles.values}>1</Text>
                        </TableCol>
                        <TableCol style={styles.figures}>
                            <Text style={styles.values}>0</Text>
                        </TableCol>
                        <TableCol style={styles.figures}>
                            <Text style={styles.values}>7</Text>
                        </TableCol>
                        <TableCol style={styles.figures}>
                            <Text style={styles.values}>13</Text>
                        </TableCol>
                    </TableRow>
                    <TableRow style={[styles.dataRow, styles.even]}>
                        <TableCol>
                            <HeaderText>4</HeaderText>
                        </TableCol>
                        <TableCol style={styles.clubCol}>
                            <TableRow>
                                <TableCol>
                                    <View style={[styles.circle, styles.active]}></View>
                                </TableCol>
                                <TableCol style={[styles.alignRight, styles.logo, {marginRight: 1}]}> 
                                    <Logo source={require('../assets/img/barcelona.png')} 
                                        size={25} 
                                    />
                                </TableCol>
                                <TableCol style={styles.club1}> 
                                    <Text style={[styles.bold, styles.clubName]}>BFC</Text>
                                </TableCol>
                            </TableRow>
                        </TableCol>
                        <TableCol style={styles.figures}>
                            <Text style={styles.values}>6</Text>
                        </TableCol>
                        <TableCol style={styles.figures}>
                            <Text style={styles.values}>3</Text>
                        </TableCol>
                        <TableCol style={styles.figures}>
                            <Text style={styles.values}>2</Text>
                        </TableCol>
                        <TableCol style={styles.figures}>
                            <Text style={styles.values}>1</Text>
                        </TableCol>
                        <TableCol style={styles.figures}>
                            <Text style={styles.values}>5</Text>
                        </TableCol>
                        <TableCol style={styles.figures}>
                            <Text style={styles.values}>11</Text>
                        </TableCol>
                    </TableRow>
                    <TableRow style={styles.dataRow}>
                        <TableCol>
                            <HeaderText>5</HeaderText>
                        </TableCol>
                        <TableCol style={styles.clubCol}>
                            <TableRow>
                                <TableCol>
                                    <View style={[styles.circle, styles.active]}></View>
                                </TableCol>
                                <TableCol style={[styles.alignRight, styles.logo, {marginRight: 1}]}> 
                                    <Logo source={require('../assets/img/chelsea.png')} 
                                        size={25} 
                                    />
                                </TableCol>
                                <TableCol style={styles.club1}> 
                                    <Text style={[styles.bold, styles.clubName]}>CFC</Text>
                                </TableCol>
                            </TableRow>
                        </TableCol>
                        <TableCol style={styles.figures}>
                            <Text style={styles.values}>6</Text>
                        </TableCol>
                        <TableCol style={styles.figures}>
                            <Text style={styles.values}>3</Text>
                        </TableCol>
                        <TableCol style={styles.figures}>
                            <Text style={styles.values}>2</Text>
                        </TableCol>
                        <TableCol style={styles.figures}>
                            <Text style={styles.values}>1</Text>
                        </TableCol>
                        <TableCol style={styles.figures}>
                            <Text style={styles.values}>3</Text>
                        </TableCol>
                        <TableCol style={styles.figures}>
                            <Text style={styles.values}>9</Text>
                        </TableCol>
                    </TableRow>
                    <TableRow style={[styles.dataRow, styles.even]}>
                        <TableCol>
                            <HeaderText>6</HeaderText>
                        </TableCol>
                        <TableCol style={styles.clubCol}>
                            <TableRow>
                                <TableCol>
                                    <View style={[styles.circle, styles.active]}></View>
                                </TableCol>
                                <TableCol style={[styles.alignRight, styles.logo, {marginRight: 1}]}> 
                                    <Logo source={require('../assets/img/crystal_palace.png')} 
                                        size={25} 
                                    />
                                </TableCol>
                                <TableCol style={styles.club1}> 
                                    <Text style={[styles.bold, styles.clubName]}>CPC</Text>
                                </TableCol>
                            </TableRow>
                        </TableCol>
                        <TableCol style={styles.figures}>
                            <Text style={styles.values}>6</Text>
                        </TableCol>
                        <TableCol style={styles.figures}>
                            <Text style={styles.values}>3</Text>
                        </TableCol>
                        <TableCol style={styles.figures}>
                            <Text style={styles.values}>2</Text>
                        </TableCol>
                        <TableCol style={styles.figures}>
                            <Text style={styles.values}>1</Text>
                        </TableCol>
                        <TableCol style={styles.figures}>
                            <Text style={styles.values}>5</Text>
                        </TableCol>
                        <TableCol style={styles.figures}>
                            <Text style={styles.values}>7</Text>
                        </TableCol>
                    </TableRow>
                    <TableRow style={styles.dataRow}>
                        <TableCol>
                            <HeaderText>7</HeaderText>
                        </TableCol>
                        <TableCol style={styles.clubCol}>
                            <TableRow>
                                <TableCol>
                                    <View style={[styles.circle]}></View>
                                </TableCol>
                                <TableCol style={[styles.alignRight, styles.logo, {marginRight: 1}]}> 
                                    <Logo source={require('../assets/img/ac_milan.png')} 
                                        size={25} 
                                    />
                                </TableCol>
                                <TableCol style={styles.club1}> 
                                    <Text style={[styles.bold, styles.clubName]}>ACM</Text>
                                </TableCol>
                            </TableRow>
                        
                        </TableCol>
                        <TableCol style={styles.figures}>
                            <Text style={styles.values}>6</Text>
                        </TableCol>
                        <TableCol style={styles.figures}>
                            <Text style={styles.values}>5</Text>
                        </TableCol>
                        <TableCol style={styles.figures}>
                            <Text style={styles.values}>1</Text>
                        </TableCol>
                        <TableCol style={styles.figures}>
                            <Text style={styles.values}>0</Text>
                        </TableCol>
                        <TableCol style={styles.figures}>
                            <Text style={styles.values}>19</Text>
                        </TableCol>
                        <TableCol style={styles.figures}>
                            <Text style={styles.values}>16</Text>
                        </TableCol>
                    </TableRow>
                    <TableRow style={[styles.dataRow, styles.even]}>
                        <TableCol>
                            <HeaderText>8</HeaderText>
                        </TableCol>
                        <TableCol style={styles.clubCol}>
                            <TableRow>
                                <TableCol>
                                    <View style={[styles.circle]}></View>
                                </TableCol>
                                <TableCol style={[styles.alignRight, styles.logo, {marginRight: 1}]}> 
                                    <Logo source={require('../assets/img/arsenal.png')} 
                                        size={25} 
                                    />
                                </TableCol>
                                <TableCol style={styles.club1}> 
                                    <Text style={[styles.bold, styles.clubName]}>ARS</Text>
                                </TableCol>
                            </TableRow>
                        
                        </TableCol>
                        <TableCol style={styles.figures}>
                            <Text style={styles.values}>6</Text>
                        </TableCol>
                        <TableCol style={styles.figures}>
                            <Text style={styles.values}>5</Text>
                        </TableCol>
                        <TableCol style={styles.figures}>
                            <Text style={styles.values}>1</Text>
                        </TableCol>
                        <TableCol style={styles.figures}>
                            <Text style={styles.values}>0</Text>
                        </TableCol>
                        <TableCol style={styles.figures}>
                            <Text style={styles.values}>15</Text>
                        </TableCol>
                        <TableCol style={styles.figures}>
                            <Text style={styles.values}>16</Text>
                        </TableCol>
                    </TableRow>
                    <TableRow style={styles.dataRow}>
                        <TableCol>
                            <HeaderText>9</HeaderText>
                        </TableCol>
                        <TableCol style={styles.clubCol}>
                            <TableRow>
                                <TableCol>
                                    <View style={[styles.circle]}></View>
                                </TableCol>
                                <TableCol style={[styles.alignRight, styles.logo, {marginRight: 1}]}> 
                                    <Logo source={require('../assets/img/bayern_munich.png')} 
                                        size={25} 
                                    />
                                </TableCol>
                                <TableCol style={styles.club1}> 
                                    <Text style={[styles.bold, styles.clubName]}>BYM</Text>
                                </TableCol>
                            </TableRow>
                        </TableCol>
                        <TableCol style={styles.figures}>
                            <Text style={styles.values}>6</Text>
                        </TableCol>
                        <TableCol style={styles.figures}>
                            <Text style={styles.values}>4</Text>
                        </TableCol>
                        <TableCol style={styles.figures}>
                            <Text style={styles.values}>1</Text>
                        </TableCol>
                        <TableCol style={styles.figures}>
                            <Text style={styles.values}>0</Text>
                        </TableCol>
                        <TableCol style={styles.figures}>
                            <Text style={styles.values}>7</Text>
                        </TableCol>
                        <TableCol style={styles.figures}>
                            <Text style={styles.values}>13</Text>
                        </TableCol>
                    </TableRow>
                    <TableRow style={[styles.dataRow, styles.even]}>
                        <TableCol>
                            <HeaderText>10</HeaderText>
                        </TableCol>
                        <TableCol style={styles.clubCol}>
                            <TableRow>
                                <TableCol>
                                    <View style={[styles.circle, styles.active]}></View>
                                </TableCol>
                                <TableCol style={[styles.alignRight, styles.logo, {marginRight: 1}]}> 
                                    <Logo source={require('../assets/img/barcelona.png')} 
                                        size={25} 
                                    />
                                </TableCol>
                                <TableCol style={styles.club1}> 
                                    <Text style={[styles.bold, styles.clubName]}>BFC</Text>
                                </TableCol>
                            </TableRow>
                        </TableCol>
                        <TableCol style={styles.figures}>
                            <Text style={styles.values}>6</Text>
                        </TableCol>
                        <TableCol style={styles.figures}>
                            <Text style={styles.values}>3</Text>
                        </TableCol>
                        <TableCol style={styles.figures}>
                            <Text style={styles.values}>2</Text>
                        </TableCol>
                        <TableCol style={styles.figures}>
                            <Text style={styles.values}>1</Text>
                        </TableCol>
                        <TableCol style={styles.figures}>
                            <Text style={styles.values}>5</Text>
                        </TableCol>
                        <TableCol style={styles.figures}>
                            <Text style={styles.values}>11</Text>
                        </TableCol>
                    </TableRow>
                    <TableRow style={styles.dataRow}>
                        <TableCol>
                            <HeaderText>11</HeaderText>
                        </TableCol>
                        <TableCol style={styles.clubCol}>
                            <TableRow>
                                <TableCol>
                                    <View style={[styles.circle, styles.active]}></View>
                                </TableCol>
                                <TableCol style={[styles.alignRight, styles.logo, {marginRight: 1}]}> 
                                    <Logo source={require('../assets/img/chelsea.png')} 
                                        size={25} 
                                    />
                                </TableCol>
                                <TableCol style={styles.club1}> 
                                    <Text style={[styles.bold, styles.clubName]}>CFC</Text>
                                </TableCol>
                            </TableRow>
                        </TableCol>
                        <TableCol style={styles.figures}>
                            <Text style={styles.values}>6</Text>
                        </TableCol>
                        <TableCol style={styles.figures}>
                            <Text style={styles.values}>3</Text>
                        </TableCol>
                        <TableCol style={styles.figures}>
                            <Text style={styles.values}>2</Text>
                        </TableCol>
                        <TableCol style={styles.figures}>
                            <Text style={styles.values}>1</Text>
                        </TableCol>
                        <TableCol style={styles.figures}>
                            <Text style={styles.values}>3</Text>
                        </TableCol>
                        <TableCol style={styles.figures}>
                            <Text style={styles.values}>9</Text>
                        </TableCol>
                    </TableRow>
                    <TableRow style={[styles.dataRow, styles.even]}>
                        <TableCol>
                            <HeaderText>12</HeaderText>
                        </TableCol>
                        <TableCol style={styles.clubCol}>
                            <TableRow>
                                <TableCol>
                                    <View style={[styles.circle, styles.active]}></View>
                                </TableCol>
                                <TableCol style={[styles.alignRight, styles.logo, {marginRight: 1}]}> 
                                    <Logo source={require('../assets/img/crystal_palace.png')} 
                                        size={25} 
                                    />
                                </TableCol>
                                <TableCol style={styles.club1}> 
                                    <Text style={[styles.bold, styles.clubName]}>CPC</Text>
                                </TableCol>
                            </TableRow>
                        </TableCol>
                        <TableCol style={styles.figures}>
                            <Text style={styles.values}>6</Text>
                        </TableCol>
                        <TableCol style={styles.figures}>
                            <Text style={styles.values}>3</Text>
                        </TableCol>
                        <TableCol style={styles.figures}>
                            <Text style={styles.values}>2</Text>
                        </TableCol>
                        <TableCol style={styles.figures}>
                            <Text style={styles.values}>1</Text>
                        </TableCol>
                        <TableCol style={styles.figures}>
                            <Text style={styles.values}>5</Text>
                        </TableCol>
                        <TableCol style={styles.figures}>
                            <Text style={styles.values}>7</Text>
                        </TableCol>
                    </TableRow>
                </Table>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        borderRadius: 4,
        backgroundColor: 'white',
        marginBottom: 10
    },
    lastItem: {
        borderBottomLeftRadius: 4,
        borderBottomRightRadius: 4,
    },
    even: {
        backgroundColor: 'transparent'
    },
    values: {
        color: '#666666'
    },
    dataRow: {
        backgroundColor: theme.grayBackgroundColor,
        borderTopWidth: 1,
        borderTopColor: theme.grayBackgroundBorderColor,
    },
    figures: {
        // backgroundColor: 'red',
        // marginRight: 1,
    },
    clubCol: {
        minWidth: 70
    },
    logo: {
        maxWidth: 20,
    },
    club1: {
        alignItems: 'flex-start'   
    },
    alignRight: {
        alignItems: 'flex-end',
    },
    bold: {
        fontFamily: 'saira-semibold'
    },
    clubName: {
        color: '#555'
    },
    active: {
        backgroundColor: theme.greenCircleBackgroundColor
    },
    circle: {
        height: 6,
        width: 6,
        borderRadius: 5,
        backgroundColor: '#888888'
    }


    
});