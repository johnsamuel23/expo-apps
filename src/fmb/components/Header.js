import React from 'react';
import { StyleSheet, View, Dimensions, Platform } from 'react-native';
import LocalImage from './common/LocalImage';
import Row from '../components/common/Row';
import Col from '../components/common/Col';
import Text from '../components/common/Text';
import Touch from '../components/common/Touch';
import Back from '../utils/icons/Back';
import StatusBar from '../components/common/StatusBar';
import { headerStyle, headerTitleStyle, theme } from '../styles';
import { STATUS_BAR_HEIGHT } from '../constants';

export default Header = ({
    Left,
    Right,
    leftStyle,
    titleText,
    titleStyle,
    noShadow,
    leftButtonAction
}) => {
    Left = Left ? Left : <Back size={20} color="#999" />;
    Right = Right ? Right : <View />;
    titleText = titleText ? titleText : "Screen Title";
    let elevation = noShadow ? 0 : 3;
    let borderBottomColor = noShadow ? "#d9d9d9" : "transparent";
    let borderBottomWidth = noShadow ? 1 : 0;

    return (
        <View style={styles.header}>
            <StatusBar />
            <Row style={[styles.headerMain, {
                    borderBottomColor, 
                    borderBottomWidth,
                 ...Platform.select({
                    android: {
                        elevation
                    }
                })
            }]}>
                <View>
                    <Touch action={() => leftButtonAction()}>
                        <View style={[{marginLeft: -15, paddingLeft: 20, width: 50, height: 50, justifyContent: 'center'}, leftStyle]}>
                            { Left }
                        </View>
                    </Touch>
                </View>
                <View style={styles.headerTitle}><Text style={[styles.headerTitleText, titleStyle]}>{titleText}</Text></View>
                <View style={{width: 50}}>
                    { Right }
                </View>
            </Row>
        </View>
    )
}

const styles = StyleSheet.create({
    headerMain: {
        height: 50,
        paddingLeft: 15, 
        paddingRight: 15,
        backgroundColor: 'white',
        justifyContent: 'center',
        ...Platform.select({
            ios: {
                borderBottomWidth: 1,
                borderBottomColor: '#d9d9d9'
            },
            android: {
                elevation: 5
            }
        })
    },
    headerTitle: {
        justifyContent: 'center',
        flex: 1, 
        alignSelf: 'stretch'
    },
    headerTitleText: {
        color: theme.brandColor,
        fontSize: 16,
        textAlign: 'center'
    },
    header: {
        height: 50 + STATUS_BAR_HEIGHT, 
    },
});