import React from 'react';
import { StyleSheet, View } from 'react-native';
import LocalImage from './common/LocalImage';

export default HeaderLeft = () => {
    return (
        <View>
            <LocalImage
                originalHeight={500}
                originalWidth={500}
                guideWidth={20}
                source={require('../assets/img/back_arrow.png')}
            />
        </View>
    )
}