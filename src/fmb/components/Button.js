import React from 'react';
import { View, StyleSheet } from 'react-native';
import Text from './common/Text';
import { theme } from '../styles';
import Touch from './common/Touch';

export default Button = ({
    text,
    thick,
    action
}) => {
    let { buttonStyles, textStyles } = extraStyles;

    if(thick) {
        buttonStyles = { 
            backgroundColor: theme.brandColor,
            height: theme.buttonHeight - 3
        }
        textStyles = { 
            color: 'white',
        }

        return (
            <View>
                <Touch action={action}>
                    <View style={[styles.buttonContainer, buttonStyles]}>
                        <Text style={[styles.text, textStyles]}> {text} </Text>
                    </View>
                </Touch>
                <View style={[styles.dropShadow]}></View>
            </View>
        );

    }

    return (
        <Touch action={action}>
            <View style={[styles.buttonContainer, buttonStyles]}>
                <Text style={[styles.text, textStyles]}> {text} </Text>
            </View>
        </Touch>
    );
}

let extraStyles = {
    buttonContainer: {},
    textStyle: {}
};

const styles = StyleSheet.create({
    container: {
        // flex: 1
    },
    buttonContainer: {
        backgroundColor: 'white',
        borderWidth: 1,
        borderColor: theme.brandColor,
        justifyContent: 'center',
        alignItems: 'center',
        height: theme.buttonHeight,
        borderRadius: 3,
        zIndex: 2
    },
    text: {
        color: theme.brandColor
    },
    dropShadow: {
        backgroundColor: theme.darkButtonShadow,
        height: theme.buttonHeight,
        borderRadius: 3,        
        zIndex: 1,
        marginTop: -theme.buttonHeight + 3,
    }
});