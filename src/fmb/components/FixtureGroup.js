import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import Table from './common/Table';
import TableRow from './common/TableRow';
import TableCol from './common/TableCol';
import Left from './common/Left';
import Right from './common/Right';
import Logo from './common/Logo';
import Text from './common/Text';
import { theme } from '../styles';

export default class FixtureGroup extends Component {

    render() {

        let footballIcon = <Logo source={require('../assets/img/football_icon.png')} 
                                size={25} 
                            />
        let clubIcon1 = <Logo source={require('../assets/img/arsenal.png')} 
                                size={25} 
                            />
        let clubIcon2 = <Logo source={require('../assets/img/ac_milan.png')} 
                                size={25} 
                            />

        return (
            <View style={[styles.container]}>
                <Table>
                    <TableRow>
                        <Left>
                            <Text>Sat Sept 30, 2017</Text>
                        </Left>
                        <Right style={[styles.alignRight, {maxWidth: 50}]}>
                            { footballIcon } 
                        </Right>
                    </TableRow>
                    <TableRow style={styles.dataRow}>
                        <TableCol style={[styles.alignRight, styles.logo, {marginRight: 10}]}> 
                            <Logo source={require('../assets/img/ac_milan.png')} 
                                size={25} 
                            />
                        </TableCol>
                        <TableCol style={styles.club1}> 
                            <Text style={[styles.bold, styles.clubName]}>AC Milan</Text>
                        </TableCol>
                        <TableCol> 
                            <Text style={[styles.time, styles.bold]}>12: 30</Text>
                        </TableCol>
                        <TableCol style={styles.club2}> 
                            <Text style={[styles.bold, styles.clubName]}>Arsenal</Text>
                        </TableCol>
                        <TableCol style={[styles.alignLeft, styles.logo, {marginLeft: 10}]}> 
                            <Logo source={require('../assets/img/arsenal.png')} 
                                size={25} 
                            />
                        </TableCol>
                    </TableRow>
                    <TableRow style={[styles.dataRow, styles.even]}>
                        <TableCol style={[styles.alignRight, styles.logo, {marginRight: 10}]}> 
                            <Logo source={require('../assets/img/bayern_munich.png')} 
                                size={25} 
                            />
                        </TableCol>
                        <TableCol style={styles.club1}> 
                            <Text style={[styles.bold, styles.clubName]}>Bayern M</Text>
                        </TableCol>
                        <TableCol> 
                            <Text style={[styles.time, styles.bold]}>15: 00</Text>
                        </TableCol>
                        <TableCol style={styles.club2}> 
                            <Text style={[styles.bold, styles.clubName]}>Barca</Text>
                        </TableCol>
                        <TableCol style={[styles.alignLeft, styles.logo, {marginLeft: 10}]}> 
                            <Logo source={require('../assets/img/barcelona.png')} 
                                size={25} 
                            />
                        </TableCol>
                    </TableRow>
                    <TableRow style={styles.dataRow}>
                        <TableCol style={[styles.alignRight, styles.logo, {marginRight: 10}]}> 
                            <Logo source={require('../assets/img/ac_milan.png')} 
                                size={25} 
                            />
                        </TableCol>
                        <TableCol style={styles.club1}> 
                            <Text style={[styles.bold, styles.clubName]}>AC Milan</Text>
                        </TableCol>
                        <TableCol> 
                            <Text style={[styles.time, styles.bold]}>12: 30</Text>
                        </TableCol>
                        <TableCol style={styles.club2}> 
                            <Text style={[styles.bold, styles.clubName]}>Arsenal</Text>
                        </TableCol>
                        <TableCol style={[styles.alignLeft, styles.logo, {marginLeft: 10}]}> 
                            <Logo source={require('../assets/img/arsenal.png')} 
                                size={25} 
                            />
                        </TableCol>
                    </TableRow>
                    <TableRow style={[styles.dataRow, styles.even, styles.lastItem]}>
                        <TableCol style={[styles.alignRight, styles.logo, {marginRight: 10}]}> 
                            <Logo source={require('../assets/img/bayern_munich.png')} 
                                size={25} 
                            />
                        </TableCol>
                        <TableCol style={styles.club1}> 
                            <Text style={[styles.bold, styles.clubName]}>Bayern M</Text>
                        </TableCol>
                        <TableCol> 
                            <Text style={[styles.time, styles.bold]}>15: 00</Text>
                        </TableCol>
                        <TableCol style={styles.club2}> 
                            <Text style={[styles.bold, styles.clubName]}>Barca</Text>
                        </TableCol>
                        <TableCol style={[styles.alignLeft, styles.logo, {marginLeft: 10}]}> 
                            <Logo source={require('../assets/img/barcelona.png')} 
                                size={25} 
                            />
                        </TableCol>
                    </TableRow>
                </Table>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        borderRadius: 4,
        backgroundColor: 'white',
        marginBottom: 10
    },
    lastItem: {
        borderBottomLeftRadius: 4,
        borderBottomRightRadius: 4,
    },
    even: {
        backgroundColor: 'transparent'
    },
    dataRow: {
        backgroundColor: theme.grayBackgroundColor,
        borderTopWidth: 1,
        borderTopColor: theme.grayBackgroundBorderColor,
    },
    alignRight: {
        alignItems: 'flex-end',
    },
    alignLeft: {
        alignItems: 'flex-start',
    },
    bold: {
        fontFamily: 'saira-semibold'
    },
    clubName: {
        color: '#555'
    },
    time: {
        color: theme.brandColor
    },
    logo: {
        maxWidth: 40,
    },
    club1: {
        alignItems: 'flex-start'   
    },
    club2: {
        alignItems: 'flex-end'
    }
});