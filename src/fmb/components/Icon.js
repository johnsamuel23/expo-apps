import React from 'react';
import { View, StyleSheet } from 'react-native';
import Text from './common/Text';
import LocalImage from './common/LocalImage';

export default Icon = ({
    iconSource,
    size,
    style
}) => {
    return (
        <View style={[styles.container, style]}>
           <LocalImage 
                originalHeight={500}
                originalWidth={500}
                source={iconSource}
                guideWidth={size}
            />
        </View>
    );
}


const styles = StyleSheet.create({
    container: {
        // flex: 1
    },
    text: {
        color: '#1d1d1d',
        marginTop: 5,
        textAlign: 'center'
    }

});