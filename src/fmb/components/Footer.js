import React from 'react';
import { StyleSheet, View, Dimensions } from 'react-native';
import LocalImage from './common/LocalImage';
import Row from '../components/common/Row';
import Col from '../components/common/Col';
import Text from '../components/common/Text';
import Touch from '../components/common/Touch';

export default Footer = ({
    activeTab,
    setActiveTab
}) => {
    
    let icons = [
        {
            name: "Home",
            activeIconSource: require('../assets/img/home_icon.png'),
            inactiveIconSource: require('../assets/img/home_icon_inactive.png'),
        },
        {
            name: "Conversations",
            activeIconSource: require('../assets/img/conversation_icon.png'),
            inactiveIconSource: require('../assets/img/conversation_icon_inactive.png'),
        },
        {
            name: "Profile",
            activeIconSource: require('../assets/img/profile_icon.png'),
            inactiveIconSource: require('../assets/img/profile_icon_inactive.png'),
        },
    ];

    let IconList = icons.map((icon, index) => {

        let backgroundColor = activeTab === icon.name ? "#e6e6e6" : "#fff";
        let borderTopColor = activeTab === icon.name ? "#d9d9d9" : "#dddddd";
        let color = activeTab === icon.name ? "#000000" : "#c1c1c1";
        let iconSource = activeTab === icon.name ? icon.activeIconSource : icon.inactiveIconSource;

        return (
            <Col key={index} style={[styles.column, {backgroundColor, borderTopColor}]}>
                <Touch action={() => setActiveTab(icon.name)} key={index} style={{flex: 1}}>
                    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                        <Icon iconSource={iconSource} size={25} />
                        <Text style={[styles.text, {color}]}>{icon.name}</Text>
                    </View>
                </Touch>
            </Col>
        );
    });

    return (
        <View style={styles.container}>
            <Row style={[styles.footer]}>

                { IconList }

            </Row>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        height: 60,
        backgroundColor: 'red',
    },
    column: {
        flex: 1,
        // justifyContent: 'center',
        // alignItems: 'center',
        borderTopColor: '#e5e5e5',
        borderTopWidth: 1,
    },
    text: {
        fontSize: 12,
        fontFamily: 'saira-regular'
    },
    footer: {
        flex: 1
    }
});