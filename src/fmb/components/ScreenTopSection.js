import React from 'react';
import { StyleSheet, View, Dimensions, Platform } from 'react-native';
import LocalImage from './common/LocalImage';
import Row from '../components/common/Row';
import Col from '../components/common/Col';
import Text from '../components/common/Text';
import Touch from '../components/common/Touch';
import Carret from '../components/common/Carret';
import StatusBar from '../components/common/StatusBar';
import { headerStyle, headerTitleStyle, theme } from '../styles';
import { STATUS_BAR_HEIGHT } from '../constants';

export default ScreenTopSection = ({
    children,
    style
}) => {

    return (
        <View style={[styles.container, style]}>
            { children }
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        paddingLeft: 20,
        paddingRight: 20,
        // paddingTop: 15,
        paddingBottom: 15,
        ...Platform.select({
            android: {
                elevation: 5
            },
            ios: {
                borderBottomColor: '#d7d7d7',
                borderBottomWidth: 1
            }
        })
    }
});