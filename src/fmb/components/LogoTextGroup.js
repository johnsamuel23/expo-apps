import React from 'react';
import { StyleSheet, View } from 'react-native';
import Text from './common/Text';
import Logo from './Logo';

export default LogoTextGroup = () => {
  return (
      <View style={styles.container}>
        <View style={styles.logo}>
          <Logo size={80} />
        </View>
        <View style={[styles.text, {marginTop: 0}]}>
          <Text style={styles.appName}>football</Text>
          <Text style={[styles.appName, {marginTop: -5}]}>my bae</Text>
        </View>
      </View>
  );
}


let styles = StyleSheet.create({
  appName: {
    fontSize: 25,
    fontFamily: 'saira-semibold',
  },
  container: {
    margin: 20,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  logo: {
    marginRight: 10
  }

});