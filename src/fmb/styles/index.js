import { Platform } from 'react-native';
import { STATUS_BAR_HEIGHT } from '../constants';

export const headerStyle = {
    height: Platform.OS === "android" ? 54 + STATUS_BAR_HEIGHT : 54,
    backgroundColor: "#c12026",
    paddingTop: Platform.OS === "android" ? STATUS_BAR_HEIGHT : 0,
}

export const headerTitleStyle = {
    color: 'white',
}

export const headerLeftStyle = {
    marginTop: Platform.OS === "android" ? STATUS_BAR_HEIGHT : 0,
}

export const theme = {
    brandColor: '#c12026',
    darkButtonShadow: '#a3161e',
    buttonHeight: 45,
    backgroundColor: '#ffbdcf',
    lightBackgroundColor: '#fcccd9',
    grayBackgroundColor: '#f4f4f4',
    grayBackgroundBorderColor: '#dfdfdf',
    greenCircleBackgroundColor: '#3ab369',
    almostDarkColor: '#555',
    grayTextColor: '#aaa',
    darkGrayColor: '#888888'
}