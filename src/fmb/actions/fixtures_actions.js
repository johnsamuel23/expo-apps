import * as types from './types';
import * as api from '../api';

export const getFixtures = () => (dispatch) => { 
    dispatch({ 
        type: types.FETCH_FIXTURES 
    });
    
    api.getFixtures().then(response => {
        
        dispatch({
            type: types.FIXTURES_SUCCESS,
            payload: response.data
        });

    });
    
}


