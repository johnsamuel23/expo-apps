import React, { Component } from 'react';
import Routes from './routes';
import { getFixtures } from './api';
import { View, Text, Image, StyleSheet } from 'react-native';
import { AppLoading, Asset, Font } from 'expo';
import { StackNavigator } from 'react-navigation';

export default RootNavigator = StackNavigator(Routes);

// export default class App extends Component {

//     cacheImages = (images) => {
//         return images.map(image => {
//             if (typeof image === 'string') {
//                 return Image.prefetch(image);
//             } else if (typeof image != null) {
//                 // return Asset.fromModule(image).downloadAsync();
//             }
//         });
//     }

//     componentWillMount() {
//         getFixtures().then(response => {
//             this.fetchedImages = [];
//             // console.log(response.data);

//             let data = response.data;

//             let fixtures = data.map(f => {
//                 this.fetchedImages.push(f.localteam_logo_path);
//                 this.fetchedImages.push(f.visitorteam_logo_path);

//                 return {...f};
//             });

//             console.log(this.fetchedImages);
//             this.cacheImages(this.fetchedImages);
//         }).catch(error => {

//             console.log(error);
//         });
//     }

//     render() {
//         return (
//             <View style={styles.container}>
//                 <RootNavigator />
//             </View>
//         )
//     }
// }

const styles = StyleSheet.create({
    container: {
        flex: 1
    }
});

// export default RootNavigator;