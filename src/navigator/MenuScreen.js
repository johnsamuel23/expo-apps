import React, { Component } from 'react';
import { 
    View, 
    Text, 
    StyleSheet, 
    TouchableOpacity, 
    Dimensions,
    Animated,
    Easing,
    LayoutAnimation,
    UIManager } from 'react-native';
import { Font, Notifications, Constants } from 'expo';
import BackgroundImage from './components/BackgroundImage';
import LocalImage from './components/LocalImage';
import Button from './components/Button';
import StatusBar from './components/StatusBar';
import { theme } from './data/theme';
import MenuBox from './components/MenuBox';
import BackIcon from './utils/icons/Back';
import PencilIcon from './utils/icons/Pencil';
import EnvelopeIcon from './utils/icons/Envelope';
import UsersIcon from './utils/icons/Users';
import WpFormIcon from './utils/icons/WpForm';


export default class MenuScreen extends Component {

    componentDidMount() {
        const { navigator } = this.props;
    }

    changeScreen(screen) {
        setTimeout(() => {
            this.props.navigator.push({name: screen});
        }, 50);
    }

    render() {

        return (
            <View style={styles.container}>
                <StatusBar backgroundColor={theme.headerBackgroundColor} />
                <View style={styles.content}>
                    <View style={styles.heading}>
                        <View style={[styles.headBox, {backgroundColor: theme.headerBackgroundColor}]}>
                            <View style={styles.headerLeft}>
                                <TouchableOpacity style={styles.backIcon} 
                                    onPress={() => this.props.navigator.pop({name: 'main_menu_screen'})}>
                                    <BackIcon color={theme.headerTextColor} size={35} />
                                </TouchableOpacity>
                            </View>
                            <View style={styles.headerCenter}>
                                <Text style={[styles.headText, {color: theme.headerTextColor}]}> Main Menu</Text>
                            </View>
                            <View style={styles.headerRight}></View>
                        </View>
                    </View>
                    <View style={styles.menuBoxes}>
                        <View style={styles.contentRow}>
                            <MenuBox style={styles.menuBox} changeScreen={() => this.changeScreen('test_introduction_screen')}>
                                <View style={styles.itemIcon}>
                                    <PencilIcon color='#464646' size={65} />
                                </View>
                                <Text style={styles.itemText}>Take Test</Text>   
                            </MenuBox>
                            <MenuBox style={styles.menuBox} changeScreen={() => this.changeScreen('personalities_menu_screen')}>
                                <View style={styles.itemIcon}>
                                    <UsersIcon color='#464646' size={57} />
                                </View>
                                <Text style={styles.itemText}>Personality Profiles</Text>   
                            </MenuBox>
                        </View>
                        <View style={styles.contentRow}>
                            <MenuBox style={styles.menuBox} changeScreen={() => this.changeScreen('results_screen')}>
                                <View style={styles.itemIcon}>
                                    <WpFormIcon color='#464646' size={65} />
                                </View>
                                <Text style={styles.itemText}>View Results</Text>   
                            </MenuBox>
                            <MenuBox style={styles.menuBox} changeScreen={() => this.changeScreen('coach_message_screen')}>
                                <View style={styles.itemIcon}>
                                    <EnvelopeIcon color='#464646' size={65} />
                                </View>
                                <Text style={styles.itemText}>Talk to the Life Coach</Text>   
                            </MenuBox>
                        </View>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fafff1',
        flex: 1
    },
    content: {
        flex: 1
    },
    contentRow: {
        flexDirection: 'row',
        paddingLeft: 10,
        paddingRight: 10,
    },
    menuBoxes: {
        justifyContent: 'center',
        flex: 1
    },
    menuBox: {
        height: 200,
    },
    itemIcon: {
        flex: 1,
        justifyContent: 'flex-end',
    },
    itemText: {
        flex: 1,
        color: '#6e6e6e',
        fontFamily: 'raleway-medium',
        fontSize: 20,
        textAlign: 'center',
        marginTop: 15,
        lineHeight: 28
    },
    heading: {
        paddingLeft: 0,
        paddingRight: 0,
        paddingTop: 0,
        paddingBottom: 0,
        // backgroundColor: '#000',
    }, 
    headBox: {
        // borderRadius: 25,
        height: 50,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    headText: {
        textAlign: 'center',
        fontSize: 20,
        fontFamily: 'raleway-semibold'
    },
    headerLeft: {
        width: 50,
        paddingLeft: 20
    },
    headerCenter: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    }, 
    headerRight: {
        width: 50
    },

});