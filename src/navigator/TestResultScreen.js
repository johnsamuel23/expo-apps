import React, { Component } from 'react';
import { 
    View, 
    Text, 
    StyleSheet, 
    TouchableNativeFeedback,
    TouchableOpacity, 
    Dimensions,
    ScrollView,
    AsyncStorage
} from 'react-native';
import { Font, Notifications, Constants } from 'expo';
import BackgroundImage from './components/BackgroundImage';
import LocalImage from './components/LocalImage';
import StatusBar from './components/StatusBar';
import { theme } from './data/theme';
import { temperaments, getScores, getTopAnswers, getTemperamentBlend, getTestResults} from './data/temperaments';
import Button from './components/Button';
import BlendText from './components/BlendText';
import ForwardIcon from './utils/icons/Forward';
import BackIcon from './utils/icons/Back';
import Br from './components/Br';
import Modal from 'react-native-modal';
import { blendedArticles } from './data/articles';


export default class TestResultScreen extends Component {

    state = {
        primaryTemperament: "Choleric",
        secondaryTemperament: "Sanguine",
        temperamentBlend: "ChlorSan",
    }

    userExists(users, userData) {
        for(let i=0; i < users.length; i++) {
            if(userData.email === users[i].email) {
                return true;
            }
        }
    }
    
    componentWillMount() {
        let { store, saveStateData } = this.props;

        // console.log("store-----", store.returningResult);
        let primaryTemperament = "";
        let secondaryTemperament = "";
        let temperamentBlend = "";
        let primaryTemperamentRatio = "";
        let secondaryTemperamentRatio = "";
        
        if(store.returningView) {
            primaryTemperament = store.returningResult.primaryTemperament;
            secondaryTemperament = store.returningResult.secondaryTemperament;
            temperamentBlend = store.returningResult.temperamentBlend;
            primaryTemperamentRatio = store.returningResult.primaryTemperamentRatio;
            secondaryTemperamentRatio = store.returningResult.secondaryTemperamentRatio;
        } else {
            this.answers = store.answers;
            let topAnswers = getTopAnswers(this.answers);
            let scores = getScores(topAnswers);
            let testResults = getTestResults(scores);
            primaryTemperament = testResults.primaryTemperament;
            secondaryTemperament = testResults.secondaryTemperament;
            temperamentBlend = getTemperamentBlend(primaryTemperament, secondaryTemperament);
            primaryTemperamentRatio = testResults.flexRatio.primary;
            secondaryTemperamentRatio = testResults.flexRatio.secondary;
        }

        let stateData = {
            primaryTemperament,
            secondaryTemperament,
            temperamentBlend,
            primaryTemperamentRatio,
            secondaryTemperamentRatio
        }

        this.setState(stateData);

        let newUserData = {...store.userData, ...stateData};
    }    

    changeScreen(screen) {
        setTimeout(() => {
            this.props.navigator.push({name: screen});
        }, 50);
    }

    render() {

        let forwardIcon = (<ForwardIcon color={theme.buttonBorderColor} size={30} />);
        let primaryTemperament = this.state.primaryTemperament;
        let secondaryTemperament = this.state.secondaryTemperament;
        let temperamentBlend = this.state.temperamentBlend;
        let primaryTemperamentRatio = this.state.primaryTemperamentRatio;
        let secondaryTemperamentRatio = this.state.secondaryTemperamentRatio;
        let userData = this.props.store.userData;

        return (
            <View style={styles.container}>
                <StatusBar backgroundColor={theme.headerBackgroundColor} />
                <View style={styles.content}>
                    <View style={styles.heading}>
                        <View style={[styles.headBox, {backgroundColor: theme.headerBackgroundColor}]}>
                            <View style={styles.headerLeft}>
                                <TouchableOpacity style={styles.backIcon} 
                                    onPress={() => this.props.navigator.pop({name: 'test_result_screen'})}>
                                    <BackIcon color={theme.headerTextColor} size={35} />
                                </TouchableOpacity>
                            </View>
                            <View style={styles.headerCenter}>
                                <Text style={[styles.headText, {color: theme.headerTextColor}]}>Test Result</Text>
                            </View>
                            <View style={styles.headerRight}></View>
                        </View>
                    </View>
                    <View style={styles.introTextArea}>
                        <ScrollView style={[{flex: 1}, styles.introTextContaniner]} contentContainerStyle={styles.introTextContaniner}>
                            {!this.props.store.returningView && <View style={[styles.section, {backgroundColor: theme.softGreenBackground, borderBottomWidth: 1, borderBottomColor: '#eaeaea'}]}>
                                <View style={[styles.textContainer]}>
                                    <Text style={styles.topSectionText}>Thank You <Text style={styles.strong}>{userData.fullName}</Text> for taking the time to get to know yourself better.
                                        <Br />
                                        Here is the result of your test
                                    </Text>
                                </View>
                            </View>
                            }
                            <View style={[styles.section, {backgroundColor: 'white'}]}>
                                <View style={[styles.textContainer]}>
                                    <Text style={styles.topSectionText}>Your Test reads a blend of <Text style={styles.strong}>{temperamentBlend}</Text>
                                    </Text>
                                    <View style={styles.blendVisual}>
                                        <View style={[styles.primaryTemp, {backgroundColor: theme.temperaments[primaryTemperament].bgColor, flex: primaryTemperamentRatio}]}>
                                            <BlendText 
                                                style={[styles.tempText, {color: theme.temperaments[primaryTemperament].textColor}]} 
                                                text={primaryTemperament}
                                                />
                                        </View>
                                        <View style={[styles.secondaryTemp, {backgroundColor: theme.temperaments[secondaryTemperament].bgColor, flex: secondaryTemperamentRatio}]}>
                                            <BlendText 
                                                style={[styles.tempText, {color: theme.temperaments[secondaryTemperament].textColor}]} 
                                                text={secondaryTemperament}
                                                />
                                        </View>
                                    </View>
                                    <Text style={[styles.topSectionText, {textAlign: 'left', lineHeight: 25}]}>
                                        <Text style={styles.strong}>Primary:</Text> {primaryTemperament}
                                        <Br single/>
                                        <Text style={styles.strong}>Secondary:</Text> {secondaryTemperament}
                                    </Text>
                                    <View style={styles.buttonLeft}> 
                                        <Button text='More on Personality Profiles' 
                                            textStyles={{fontSize: 13}}
                                            buttonStyles={{height: 40, width: 250, marginTop: 20, marginBottom: 20}}
                                            switchScreen={() => this.changeScreen('personalities_menu_screen')}/>
                                    </View>
                                </View>
                            </View>
                            <View style={[styles.section, {backgroundColor: theme.softGreenBackground, borderTopWidth: 1, borderTopColor: '#eaeaea'}]}>
                                <Text style={styles.introText}>
                                    
                                    { blendedArticles[temperamentBlend] }
                                    
                                </Text>
                            </View>
                        </ScrollView>
                        <View style={styles.actionBtn}>
                            <View style={styles.buttonLeft}> 
                                <Button text='Career Suggestions' 
                                    textStyles={{fontSize: 13}}
                                    switchScreen={() => this.changeScreen('career_suggestion_screen')}/>
                            </View>
                            <View style={styles.buttonRight}> 
                                <Button text='Main Menu' 
                                    textStyles={{fontSize: 13}}
                                    switchScreen={() => this.changeScreen('main_menu_screen')}/>
                            </View>
                        </View>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        flex: 1
    },
    section: {
        padding: 20
    },
    textContainer: {
        // marginBottom: 10
    },
    topSectionText: {
        fontFamily: 'merriweather-light',
        textAlign: 'center',
        fontSize: 17,
        color: '#626262'
    },
    strong: {
        fontWeight: "500"
    },
    content: {
        flex: 1
    },
    contentRow: {
        flexDirection: 'row',
        paddingLeft: 10,
        paddingRight: 10,
    },
    menuBoxes: {
        justifyContent: 'center',
        flex: 1
    },
    menuBox: {
        height: 200,
    },
    itemIcon: {
        flex: 1,
        justifyContent: 'flex-end',
    },
    itemText: {
        flex: 1,
        color: '#6e6e6e',
        fontFamily: 'raleway-medium',
        fontSize: 20,
        textAlign: 'center',
        marginTop: 15,
        lineHeight: 28
    },
    heading: {
        paddingLeft: 0,
        paddingRight: 0,
        paddingTop: 0,
        paddingBottom: 0,
        // backgroundColor: '#000',
    }, 
    headBox: {
        // borderRadius: 25,
        height: 50,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    headText: {
        textAlign: 'center',
        fontSize: 20,
        fontFamily: 'raleway-semibold'
    },
    headerLeft: {
        width: 50,
        paddingLeft: 20
    },
    headerCenter: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    }, 
    headerRight: {
        width: 50
    },
    introTextContaniner: {
        // marginBottom: 20
    },
    introTextArea: {
        flex: 1
    },
    buttonLeft: {
        flex: 1,
        marginRight: 15
    },
    buttonRight: {
        flex: 1,
        marginLeft: 15,
    },
    actionBtn: {
        backgroundColor: 'white',
        height: 100,
        justifyContent: 'center',
        // alignItems: 'center',
        padding: 20,
        borderTopColor: '#f1f1f1',
        borderTopWidth: 1,
        flexDirection: 'row'
    },
    introText: {
        fontFamily: 'merriweather-light',
        fontSize: 17,
        lineHeight: 23,
        color: '#525252',
        textAlign: 'justify'
    },
    blendVisual: {
        height: 40,
        flexDirection: 'row',
        marginBottom: 20,
        marginTop: 20
    },
    primaryTemp: {
        flex: 3,
        // height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        borderTopLeftRadius: 3,
        borderBottomLeftRadius: 3,
    },
    secondaryTemp: {
        flex: 2,
        // height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        borderTopRightRadius: 3,
        borderBottomRightRadius: 3,
    },
    tempText: {
        fontFamily: 'raleway-semibold',
        fontSize: 15,
        // fontFamily: 'merriweather-light',
        // fontWeight: "700",
    },

});