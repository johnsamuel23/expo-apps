import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, BackHandler, AsyncStorage } from 'react-native';
import { Navigator } from 'react-native-deprecated-custom-components';
import AppLoadingScreen from './AppLoading';
import AppIntroductionScreen from './AppIntroductionScreen';
import CareerSuggestionScreen from './CareerSuggestionScreen';
import CoachMessageScreen from './CoachMessageScreen';
import DataCollectionScreen from './DataCollectionScreen';
import MenuScreen from './MenuScreen';
import PersonalityMenuScreen from './PersonalityMenuScreen';
import PersonalityProfileScreen from './PersonalityProfileScreen';
import ResultsScreen from './ResultsScreen';
import TestIntroScreen from './TestIntroScreen';
import TestGuideScreen from './TestGuideScreen';
import TestMainScreen from './TestMainScreen';
import TestResultScreen from './TestResultScreen';
import UserSelectMenu from './UserSelectScreen';
import { Notifications, Permissions, Constants, Audio } from 'expo';
import moment from 'moment';
import { getAnswers } from './data/demoAnswers';


// data_collection_screen
// results_screen
// personality_profile_screen
// personalities_menu_screen
// coach_message_screen

export default class MainApp extends Component {
  state = {
    initialRoute: 'loading_screen',
    // answers: getAnswers('melPhleg'), // apply a demo test,
    answers: [],
    selectedTemperament: 'Sanguine',
    userData: {
        fullName: "",
        academicLevel: "",
        email: "",
        age: "",
        country: "",
        subscriptionChecked: false,
    },
    // userData: {
    //     fullName: "John Samuel",
    //     academicLevel: "First Degree",
    //     email: "jsamchineme@gmail.com",
    //     age: "26",
    //     country: "Nigeria",
    //     subscriptionChecked: true,
    // },
    articleSelectedTemperament: "Choleric",
    messageData: {},
    users: [],
    results: [],
    justSaved: false,
    noOfQuestions: 40
  }

  async componentWillMount() {
    let resultsFound = false;
    let savedTestResults = "";

    try {
        // get the results saved 
        savedTestResults = await AsyncStorage.getItem('savedTestResults');
        if (savedTestResults !== null){
            resultsFound = true;
            console.log("RETURNING RESULT", savedTestResults);
        } 
    } catch (error) {
        // resultsFound = false;
    }

    // console.log("RESULTS FOUND", resultsFound);
    if(resultsFound) {
        // console.log("appUsers:", appUserValue);
        // appUserValue will be in string 
        // convert it to an array                 
        let fetchedResults = JSON.parse(savedTestResults);
        this.saveStateData("results", fetchedResults);
        let users = this.getUsersFromResult(fetchedResults);
        // console.log("Fetched Users", users);
        // console.log("Fetched Results", fetchedResults);
        this.saveUsers(users);
    }
  }

  getUsersFromResult(results) {
    let users = [];

    console.log("RESULTS", results);

    for(let i=0; i < results.length; i++) {
        let result = results[i];
        let newUser = {
            firstName: result.userData.firstName,
            lastName: result.userData.lastName,
            email: result.userData.email
        };

        if(users.length == 0) {
            users.push(newUser);
            // continue;
        } 

        // for(let j=0; j < users.length; j++) {
        //     let user = users[j];
        //     // console.log("RESULT CONSIDERED==============", result, user);
        //     if(user.email !== result.userData.email) {
        //         console.log("INSTANCE FOUND", result);
        //         users.push(newUser);
        //         // break;
        //     }
        // }
    }

    // console.log("USERS FOR RETURN", users);
    return users;
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', () => {
        if (this.navigator && this.navigator.getCurrentRoutes().length > 1) {
          this.navigator.pop();
          // BackHandler.exitApp();
          return true;
        }
        return false;
    });

    /* -- Available Tests
        sanChlor
        melPhleg
        phlegChlor
    */
  }

  saveUserData(data) {
    this.setState({
        userData: data
    });
  }

  saveContactMessageData(data) {
    this.setState({
        messageData: data
    });
  }

  removePreviousAnswer(data) {
    let previousAnswers = [...this.state.answers];
    let newAnswers = [];
    for(var i=0; i < previousAnswers.length; i++) {
        if(previousAnswers[i].questionSerial === data.questionSerial) {
            // skip this
        } else {
            newAnswers.push(previousAnswers[i]);
        }
    }

    // here are the filtered answers now
    return newAnswers;
  }

  saveOption(data) {
      // remove answer for this particular question if there is any and 
      // add this answer for this particular question 
      let allAnswers = this.removePreviousAnswer(data);
      let newAnswers = [...allAnswers, data];

      this.setState({
        answers: newAnswers
      })
  }

  setReturningResult(data) {
      let userResult = {
        primaryTemperament: data.primaryTemperament,
        secondaryTemperament: data.secondaryTemperament,
        temperamentBlend: data.temperamentBlend,
        primaryTemperamentRatio: data.primaryTemperamentRatio,
        secondaryTemperamentRatio: data.secondaryTemperamentRatio,
      };

      this.setState({
        returningResult: data,
        returningView: true,
        userData: data.userData,
        userResult
      })
  }

  setReturningUser(data) {
    this.setState({
        userData: data
    });
  }

  setArticleTemperament(temperament) {
    this.setState({
        articleSelectedTemperament: temperament     
    });

    // console.log("Selected Temperament", temperament);
  }

  saveResult(data) {
    this.setState({
        userResult: data
    });
  }

  saveUsers(users) {
    this.setState({users});
  }

  saveStateData(key, data) {
      this.setState({
          [key]: data
      });
  }

  renderScene(route, navigator) {
    this.navigator = navigator;
    switch(route.name) {
        case 'test_result_screen':
            currentScreen = <TestResultScreen store={this.state}
                                saveStateData={this.saveStateData.bind(this)}
                                saveResult={this.saveResult.bind(this)}
                                navigator={navigator}
                            />
            break;
        case 'user_select_screen':
            currentScreen = <UserSelectMenu store={this.state} 
                                setReturningUser={this.setReturningUser.bind(this)}
                                saveUsers={this.saveUsers.bind(this)}
                                navigator={navigator}
                            />
            break;
        case 'data_collection_screen':
            currentScreen = <DataCollectionScreen store={this.state}
                                saveStateData={this.saveStateData.bind(this)}
                                saveUserData={this.saveUserData.bind(this)}
                                saveResult={this.saveResult.bind(this)}                                
                                setReturningUser={this.setReturningUser.bind(this)} 
                                navigator={navigator}
                            />
            break;
        case 'results_screen':
            currentScreen = <ResultsScreen store={this.state}
                                setReturningResult={this.setReturningResult.bind(this)}
                                navigator={navigator}
                            />
            break;
        case 'career_suggestion_screen':
            currentScreen = <CareerSuggestionScreen store={this.state} 
                                navigator={navigator}
                            />
            break;
        case 'personality_profile_screen':
            currentScreen = <PersonalityProfileScreen store={this.state} 
                                navigator={navigator}
                            />
            break;
        case 'personalities_menu_screen':
            currentScreen = <PersonalityMenuScreen store={this.state}
                                setArticleTemperament={this.setArticleTemperament.bind(this)}
                                navigator={navigator}
                            />
            break;
        case 'coach_message_screen':
            currentScreen = <CoachMessageScreen store={this.state} 
                                saveContactMessageData={this.saveContactMessageData.bind(this)}
                                navigator={navigator}
                            />
            break;
        case 'app_introduction_screen':
            currentScreen = <AppIntroductionScreen store={this.state} 
                                navigator={navigator}
                            />
            break;
        case 'test_main_screen':
            currentScreen = <TestMainScreen store={this.state} 
                                navigator={navigator}
                                saveOption={this.saveOption.bind(this)}
                            />
            break;
        case 'test_introduction_screen':
            currentScreen = <TestIntroScreen store={this.state} 
                                saveStateData={this.saveStateData.bind(this)}
                                navigator={navigator}
                            />
            break;
        case 'test_guide_screen':
            currentScreen = <TestGuideScreen store={this.state} 
                                navigator={navigator}
                            />
            break;
        case 'loading_screen':
            currentScreen = <AppLoadingScreen store={this.state} 
                                navigator={navigator}
                            />
            break;
        case 'main_menu_screen':
            currentScreen = <MenuScreen store={this.state} 
                                navigator={navigator}
                            />
            break;
        default: 
            currentScreen = <MenuScreen store={this.state} 
                                navigattor={navigator}
                            />;
    }
    return currentScreen;   
  }

  render() {
    return (
      <View style={[styles.container]}>
        <View style={styles.container}> 
            <Navigator
                initialRoute={{name: this.state.initialRoute}}
                renderScene={this.renderScene.bind(this)}
                configureScene={(route, routeStack) => Navigator.SceneConfigs.FloatFromRight}
            />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    header: {
        backgroundColor: '#000',
        height: Constants.statusBarHeight,
    },
    headerText: {
        fontSize: 20
    }
});

