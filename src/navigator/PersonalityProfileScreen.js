import React, { Component } from 'react';
import { 
    View, 
    Text, 
    StyleSheet, 
    TouchableNativeFeedback,
    TouchableOpacity, 
    Dimensions,
    ScrollView
} from 'react-native';
import { Font, Notifications, Constants } from 'expo';
import BackgroundImage from './components/BackgroundImage';
import LocalImage from './components/LocalImage';
import StatusBar from './components/StatusBar';
import { theme } from './data/theme';
import Button from './components/Button';
import BackIcon from './utils/icons/Back';
import SendIcon from './utils/icons/Send';
import Br from './components/Br';
import TextInput from './components/TextInput';
import CheckBox from './components/CheckBox';
import Popup from 'react-native-popup';
import { articles } from './data/articles';

export default class PersonalityProfileScreen extends Component {

    componentWillMount() {

    }

    changeScreen(screen) {
        setTimeout(() => {
            this.props.navigator.push({name: screen});
        }, 50);
    }

    render() {

        let sendIcon = (<SendIcon color={theme.buttonBorderColor} size={20} />);
        let selectedTemperament = this.props.store.articleSelectedTemperament || "Melancholic";

        return (
            <View style={styles.container}>
                <StatusBar backgroundColor={theme.headerBackgroundColor} />
                <View style={styles.content}>
                    <View style={styles.heading}>
                        <View style={[styles.headBox, {backgroundColor: theme.headerBackgroundColor}]}>
                            <View style={styles.headerLeft}>
                                <TouchableOpacity style={styles.backIcon} 
                                    onPress={() => this.props.navigator.pop({name: 'data_collection_screen'})}>
                                    <BackIcon color={theme.headerTextColor} size={35} />
                                </TouchableOpacity>
                            </View>
                            <View style={styles.headerCenter}>
                                <Text style={[styles.headText, {color: theme.headerTextColor}]}>Personality Profile</Text>
                            </View>
                            <View style={styles.headerRight}></View>
                        </View>
                    </View>
                    <View style={styles.introTextArea}>
                        <ScrollView style={[{flex: 1}, styles.introTextContaniner]} contentContainerStyle={styles.introTextContaniner}>
                            <View style={[styles.temperamentTitle, {backgroundColor: theme.temperaments[selectedTemperament].bgColor}]}>
                                <Text style={[styles.temperamentTitleText, {color: theme.temperaments[selectedTemperament].textColor}]}>{selectedTemperament}</Text>
                            </View>
                            <Text style={styles.introText}>{articles[selectedTemperament]}</Text>
                        </ScrollView>
                        <View style={styles.actionBtn}>
                            <Button text='Main Menu' icon switchScreen={() => this.changeScreen('main_menu_screen')}/>
                        </View>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        flex: 1
    },
    section: {
        padding: 20
    },
    temperamentTitle: {
        height: 45,
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 20,
        borderRadius: 3
    },
    temperamentTitleText: {
        fontSize: 20,
        fontFamily: 'raleway-semibold'
    },
    textContainer: {
        // marginBottom: 10
    },
    topSectionText: {
        fontFamily: 'merriweather-light',
        textAlign: 'center',
        fontSize: 17,
        color: '#626262'
    },
    strong: {
        fontWeight: "500"
    },
    content: {
        flex: 1
    },
    contentRow: {
        flexDirection: 'row',
        paddingLeft: 10,
        paddingRight: 10,
    },
    menuBoxes: {
        justifyContent: 'center',
        flex: 1
    },
    menuBox: {
        height: 200,
    },
    itemIcon: {
        flex: 1,
        justifyContent: 'flex-end',
    },
    itemText: {
        flex: 1,
        color: '#6e6e6e',
        fontFamily: 'raleway-medium',
        fontSize: 20,
        textAlign: 'center',
        marginTop: 15,
        lineHeight: 28
    },
    heading: {
        paddingLeft: 0,
        paddingRight: 0,
        paddingTop: 0,
        paddingBottom: 0,
        // backgroundColor: '#000',
    }, 
    headBox: {
        // borderRadius: 25,
        height: 50,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    headText: {
        textAlign: 'center',
        fontSize: 20,
        fontFamily: 'raleway-semibold'
    },
    headerLeft: {
        width: 50,
        paddingLeft: 20
    },
    headerCenter: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    }, 
    headerRight: {
        width: 50
    },
    introTextContaniner: {
        // marginBottom: 20,
        backgroundColor: theme.softGreenBackground,
        padding: 10
    },
    introTextArea: {
        flex: 1
    },
    buttonLeft: {
        flex: 1,
        marginRight: 15
    },
    buttonRight: {
        flex: 1,
        marginLeft: 15,
    },
    actionBtn: {
        backgroundColor: 'white',
        height: 100,
        justifyContent: 'center',
        padding: 20,
        borderTopColor: '#f1f1f1',
        borderTopWidth: 1,
    },
    introText: {
        fontFamily: 'merriweather-light',
        fontSize: 17,
        lineHeight: 23,
        color: '#525252',
    },
    blendVisual: {
        height: 40,
        flexDirection: 'row',
        marginBottom: 20,
        marginTop: 20
    },
    primaryTemp: {
        flex: 3,
        // height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        borderTopLeftRadius: 3,
        borderBottomLeftRadius: 3,
    },
    secondaryTemp: {
        flex: 2,
        // height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        borderTopRightRadius: 3,
        borderBottomRightRadius: 3,
    },
    tempText: {
        fontFamily: 'raleway-semibold',
        fontSize: 15,
        // fontFamily: 'merriweather-light',
        // fontWeight: "700",
    },
    lastInputRow: {
        flexDirection: 'row',
    },
    subscriptionRow: {
        flexDirection: 'row'
    },
    subscribeText: {
        color: '#777777',
        fontSize: 15,
        fontFamily: 'merriweather-light'
    }

});