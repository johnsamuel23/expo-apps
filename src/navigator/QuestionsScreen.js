import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView } from 'react-native';
import { Constants} from 'expo';
import { getSectionQuestions } from './data/questions';
import ForwardIcon from './utils/icons/Forward';
import QuestionEntry from './components/QuestionEntry';
import { theme } from './data/theme';

export default class QuestionsScreen extends Component {

  render() {
    let section = this.props.section;
    let forwardIcon = (<ForwardIcon color={theme.buttonBorderColor} size={30} />);

    let sectionQuestions = getSectionQuestions(section);
    let allQuestions = sectionQuestions.map((item, index) => {
        let answers = this.props.store.answers;        
        let selectedAnswer = answers.find(answer => answer.questionSerial === item.serial_number) || {};
        return <QuestionEntry key={item.serial_number}
                    sectionNumber={item.section}
                    questionSerial={item.serial_number} 
                    questionText={item.text}
                    selectedValue={selectedAnswer.optionNumber}
                    selectOption={(SN, QSN, OPTN) => this.props.selectOption(SN, QSN, OPTN)} />
    });

    // console.log("THE QUESTION", sectionQuestions);
    let nextSection = section + 1;
    let buttonText = section === 4 ? "Finish" : "Next";
    return (
        <ScrollView style={[{flex: 1}, styles.introTextContaniner]} contentContainerStyle={styles.introTextContaniner}>
            <View style={[styles.container]}>
                
                { allQuestions }

                <Button text={buttonText} icon={forwardIcon} switchScreen={() => this.props.changeSection(nextSection)}/>

            </View>
        </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    introTextContaniner: {
        padding: 10,
        marginBottom: 20
    }
});

