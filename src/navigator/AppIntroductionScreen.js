import React, { Component } from 'react';
import { 
    View, 
    Text, 
    StyleSheet, 
    Dimensions, 
    Animated,
    Easing,
    LayoutAnimation,
    UIManager } from 'react-native';
import { Font, Notifications, Constants } from 'expo';
import BackgroundImage from './components/BackgroundImage';
import LocalImage from './components/LocalImage';
import Button from './components/Button';
import StatusBar from './components/StatusBar';

export default class AppIntroduction extends Component {

    componentDidMount() {
        const { navigator } = this.props;
    }

    switchScreen(screen) {
        setTimeout(() => {
            this.props.navigator.push({name: screen});
        }, 50);
    }

    render() {

        return (
            <View style={styles.container}>
                <StatusBar backgroundColor='#555' />
                <View style={styles.content}>
                    <View style={styles.top_section}>
                        <BackgroundImage style={{flex: 1}} source={require('./img/top_banner.png')}>
                            <View style={styles.top_logo_section}>
                                <LocalImage
                                    source={require('./img/AppLogo.png')}
                                    originalWidth={512}
                                    originalHeight={512}
                                    guideWidth={70}
                                />
                                <Text style={styles.top_logo_text}>Life Coach</Text>
                            </View>
                        </BackgroundImage>
                    </View>    
                    <View style={styles.lower_section}>
                        <View style={styles.pay_off_container}>
                            <View style={styles.pay_off_item}>
                                <View style={styles.bullet}>
                                    <LocalImage
                                        source={require('./img/bullet.png')}
                                        originalWidth={59}
                                        originalHeight={116}
                                        guideWidth={20}
                                    />
                                </View>
                                <View style={styles.pay_off_text_container}>
                                    <Text style={styles.pay_off_text}>Determine Your</Text>
                                    <Text style={styles.pay_off_text}>Personality</Text>
                                </View>
                            </View>
                            <View style={styles.pay_off_item}>
                                <View style={styles.bullet}>
                                    <LocalImage
                                        source={require('./img/bullet.png')}
                                        originalWidth={59}
                                        originalHeight={116}
                                        guideWidth={20}
                                    />
                                </View>
                                <View style={styles.pay_off_text_container}>
                                    <Text style={styles.pay_off_text}>Know Your</Text>
                                    <Text style={styles.pay_off_text}>Best Career</Text>
                                </View>
                            </View>
                        </View>
                        <Button text='Get Started' switchScreen={() => this.switchScreen('main_menu_screen')}/>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fafff1',
        flex: 1
    },
    content: {
        flex: 1
    },
    top_section: {
        flex: 4
    },
    lower_section: {
        flex: 5,
        paddingLeft: 30,
        paddingRight: 30,
        paddingTop: 40,        
        paddingBottom: 40,
        backgroundColor: '#fafff1',
        // backgroundColor: '#ffffff',
    },
    top_logo_section: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    top_logo_text: {
        fontFamily: 'raleway-light',
        fontSize: 20,
        marginTop: 10
    },
    pay_off_container: {
        flex: 1
    },
    pay_off_item: {
        flexDirection: 'row',
        height: 100
    },
    bullet: {
        // backgroundColor: 'red',
        width: 50,
        alignItems: 'flex-end',
    },
    pay_off_text_container: {
        flex: 1,
        // backgroundColor: 'gray',
        paddingLeft: 20
    },
    pay_off_text: {
        fontFamily: 'raleway-regular',
        fontSize: 20,
        color: '#525252'
    }

});