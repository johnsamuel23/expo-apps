import React, { Component } from 'react';
import { 
    View, 
    Text, 
    StyleSheet, 
    TouchableNativeFeedback,
    TouchableOpacity, 
    Dimensions,
    ScrollView
} from 'react-native';
import { Font, Notifications, Constants } from 'expo';
import BackgroundImage from './components/BackgroundImage';
import LocalImage from './components/LocalImage';
import StatusBar from './components/StatusBar';
import { theme } from './data/theme';
import Button from './components/Button';
import ForwardIcon from './utils/icons/Forward';
import BackIcon from './utils/icons/Back';
import Br from './components/Br';
import Bullet from './components/Bullet';
import { suggestedCareers } from './data/careers';

export default class CareerSuggestionScreen extends Component {

    constructor(props) {
        super(props);
        let { store } = this.props;
        /**
         * userResult Properties
            primaryTemperament,
            secondaryTemperament,
            temperamentBlend,
            primaryTemperamentRatio,
            secondaryTemperamentRatio
        */

        let { primaryTemperament, secondaryTemperament, temperamentBlend } = store.userResult;
        let primaryTempCareers = suggestedCareers[primaryTemperament];
        let secondaryTempCareers = suggestedCareers[secondaryTemperament];

        let careers = [...primaryTempCareers, ...secondaryTempCareers];

        this.state = {
            primaryTemperament,
            secondaryTemperament,
            temperamentBlend,
            careers
        }

    }

    componentDidMount() {
        const { navigator } = this.props;
    }

    changeScreen(screen) {
        setTimeout(() => {
            this.props.navigator.push({name: screen});
        }, 50);
    }

    render() {

        let forwardIcon = (<ForwardIcon color={theme.buttonBorderColor} size={30} />);
        let careerList = this.state.careers.map((item, index) => {
            return (
                <View style={styles.careerEntry} key={index}>
                    <Bullet style={{marginRight: 10}} /> 
                    <Text style={styles.careerText}>{item}</Text>
                </View>
            );
        });

        return (
            <View style={styles.container}>
                <StatusBar backgroundColor={theme.headerBackgroundColor} />
                <View style={styles.content}>
                    <View style={styles.heading}>
                        <View style={[styles.headBox, {backgroundColor: theme.headerBackgroundColor}]}>
                            <View style={styles.headerLeft}>
                                <TouchableOpacity style={styles.backIcon} 
                                    onPress={() => this.props.navigator.pop({name: 'career_suggestion_screen'})}>
                                    <BackIcon color={theme.headerTextColor} size={35} />
                                </TouchableOpacity>
                            </View>
                            <View style={styles.headerCenter}>
                                <Text style={[styles.headText, {color: theme.headerTextColor}]}>Career Suggestions</Text>
                            </View>
                            <View style={styles.headerRight}></View>
                        </View>
                    </View>
                    <View style={styles.introTextArea}>
                        <ScrollView style={[{flex: 1}, styles.introTextContaniner]} contentContainerStyle={styles.introTextContaniner}>
                            <View style={[styles.section, {backgroundColor: 'white'}]}>
                                <Text style={styles.introText}> 
                                    Based on your personality temperament blend, which came out to be <Text style={styles.strong}>{this.state.temperamentBlend}</Text>, there are certain fields of discipline we believe can you be very productive and fulfilled in. Below is a list of them.
                                    <Br />
                                </Text>
                            </View>
                            <View style={[styles.section, {backgroundColor: theme.softGreenBackground, borderTopWidth: 1, borderTopColor: '#eaeaea'}]}>
                                
                                { careerList }

                            </View>
                        </ScrollView>
                        <View style={styles.actionBtn}>
                            <Button text='Main Menu' icon={forwardIcon} switchScreen={() => this.changeScreen('main_menu_screen')}/>
                        </View>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fafff1',
        flex: 1
    },
    content: {
        flex: 1
    },
    contentRow: {
        flexDirection: 'row',
        paddingLeft: 10,
        paddingRight: 10,
    },
    menuBoxes: {
        justifyContent: 'center',
        flex: 1
    },
    menuBox: {
        height: 200,
    },
    itemIcon: {
        flex: 1,
        justifyContent: 'flex-end',
    },
    itemText: {
        flex: 1,
        color: '#6e6e6e',
        fontFamily: 'raleway-medium',
        fontSize: 20,
        textAlign: 'center',
        marginTop: 15,
        lineHeight: 28
    },
    heading: {
        paddingLeft: 0,
        paddingRight: 0,
        paddingTop: 0,
        paddingBottom: 0,
        // backgroundColor: '#000',
    }, 
    headBox: {
        // borderRadius: 25,
        height: 50,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    headText: {
        textAlign: 'center',
        fontSize: 20,
        fontFamily: 'raleway-semibold'
    },
    headerLeft: {
        width: 50,
        paddingLeft: 20
    },
    headerCenter: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    }, 
    headerRight: {
        width: 50
    },
    introTextContaniner: {
        // backgroundColor: 'red',
        // padding: 10,
        // marginBottom: 20
    },
    section: {
        padding: 20
    },
    introTextArea: {
        flex: 1
    },
    actionBtn: {
        backgroundColor: 'white',
        height: 100,
        justifyContent: 'center',
        // alignItems: 'center',
        padding: 20,
        borderTopColor: '#f1f1f1',
        borderTopWidth: 1
    },
    introText: {
        fontFamily: 'merriweather-light',
        fontSize: 17,
        lineHeight: 23,
        color: '#525252',
        textAlign: 'justify'
    },
    strong: {
        fontWeight: "500"
    },
    careerEntry: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 10
    },
    careerText: {
        fontFamily: 'merriweather-light',
        fontSize: 15,
    }

});