import React, { Component } from 'react';
import { 
    View, 
    Text, 
    StyleSheet, 
    TouchableNativeFeedback,
    TouchableOpacity, 
    Dimensions,
    ScrollView
} from 'react-native';
import { Font, Notifications, Constants } from 'expo';
import BackgroundImage from './components/BackgroundImage';
import LocalImage from './components/LocalImage';
import StatusBar from './components/StatusBar';
import { theme } from './data/theme';
import Button from './components/Button';
import ForwardIcon from './utils/icons/Forward';
import BackIcon from './utils/icons/Back';
import Br from './components/Br';


export default class TestIntroScreen extends Component {

    componentDidMount() {
        const { navigator } = this.props;
    }

    changeScreen(screen) {
        this.props.saveStateData("justSaved", false);
        this.props.saveStateData("answers", []);
        setTimeout(() => {
            this.props.navigator.push({name: screen});
        }, 50);
    }

    render() {

        let forwardIcon = (<ForwardIcon color={theme.buttonBorderColor} size={30} />);

        return (
            <View style={styles.container}>
                <StatusBar backgroundColor={theme.headerBackgroundColor} />
                <View style={styles.content}>
                    <View style={styles.heading}>
                        <View style={[styles.headBox, {backgroundColor: theme.headerBackgroundColor}]}>
                            <View style={styles.headerLeft}>
                                <TouchableOpacity style={styles.backIcon} 
                                    onPress={() => this.props.navigator.pop({name: 'test_introduction_screen'})}>
                                    <BackIcon color={theme.headerTextColor} size={35} />
                                </TouchableOpacity>
                            </View>
                            <View style={styles.headerCenter}>
                                <Text style={[styles.headText, {color: theme.headerTextColor}]}> Test Introduction</Text>
                            </View>
                            <View style={styles.headerRight}></View>
                        </View>
                    </View>
                    <View style={styles.introTextArea}>
                        <ScrollView style={[{flex: 1}, styles.introTextContaniner]} contentContainerStyle={styles.introTextContaniner}>
                            <Text style={styles.introText}>
                                Everyone has a specific personality style that allows them to perform well at certain things and not as well at others. That style tends to influence their likes and dislikes, interests and pursuits. One of the most important aspects of the career planning process is to make sure that the career path a person chooses is something that suits the individual’s personality style. Failure to do so can lead to a lifetime of displeasure in the wrong career.
                                <Br />
                                A good number of people are trapped in a job that if given the opportunity they would try to avoid. This is a sad reality that is happening because of inability to make the right choices early on. But it is never too late to look for another career especially if you are still in the productive stage of your life. What is important now is to look for a job that fits your personality as a person. This way, you get to enjoy what you are doing and earn a living at the same time. 
                                <Br />It is easy to grow professionally in the profession that you love so it is necessary to make sure that the job fits your personality. 
                                <Br />Remember, your age 
                                <Br single />(1-25) is PREPARATORY stage, 
                                <Br single />(26-50) is PRODUCTIVE stage, 
                                <Br single />(51-75) is CONSOLIDATION and SUCCESSION stage, and 
                                <Br single />(76-end) is DEPARTURE LOUNGE.
                                <Br />Discover the importance of matching your career to your personality and see how it can lead you to a wonderful life.
                            </Text>
                        </ScrollView>
                        <View style={styles.actionBtn}>
                            <Button text='Continue' icon={forwardIcon} switchScreen={() => this.changeScreen('test_guide_screen')}/>
                        </View>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fafff1',
        flex: 1
    },
    content: {
        flex: 1
    },
    contentRow: {
        flexDirection: 'row',
        paddingLeft: 10,
        paddingRight: 10,
    },
    menuBoxes: {
        justifyContent: 'center',
        flex: 1
    },
    menuBox: {
        height: 200,
    },
    itemIcon: {
        flex: 1,
        justifyContent: 'flex-end',
    },
    itemText: {
        flex: 1,
        color: '#6e6e6e',
        fontFamily: 'raleway-medium',
        fontSize: 20,
        textAlign: 'center',
        marginTop: 15,
        lineHeight: 28
    },
    heading: {
        paddingLeft: 0,
        paddingRight: 0,
        paddingTop: 0,
        paddingBottom: 0,
        // backgroundColor: '#000',
    }, 
    headBox: {
        // borderRadius: 25,
        height: 50,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    headText: {
        textAlign: 'center',
        fontSize: 20,
        fontFamily: 'raleway-semibold'
    },
    headerLeft: {
        width: 50,
        paddingLeft: 20
    },
    headerCenter: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    }, 
    headerRight: {
        width: 50
    },
    introTextContaniner: {
        // backgroundColor: 'red',
        padding: 10,
        marginBottom: 20
    },
    introTextArea: {
        flex: 1
    },
    actionBtn: {
        backgroundColor: 'white',
        height: 100,
        justifyContent: 'center',
        // alignItems: 'center',
        padding: 20,
        borderTopColor: '#f1f1f1',
        borderTopWidth: 1
    },
    introText: {
        fontFamily: 'merriweather-light',
        fontSize: 17,
        lineHeight: 23,
        color: '#525252',
        textAlign: 'justify'
    }

});