import { questions } from './questions';

// Section 1: Sanguine
// Section 2: Choleric
// Section 3: Melancholic
// Section 4: Phlegmatic

export const demoAnswers = {
    sanChlorSection1: [4, 3, 5, 5, 3, 4, 3, 2, 5, 5],
    sanChlorSection2: [3, 5, 3, 4, 2, 2, 3, 4, 3, 4],
    sanChlorSection3: [1, 4, 2, 2, 2, 4, 1, 2, 2, 4],
    sanChlorSection4: [1, 2, 3, 3, 1, 2, 3, 2, 2, 1],
    melPhlegSection1: [1, 2, 3, 3, 1, 2, 3, 2, 2, 1],
    melPhlegSection2: [1, 4, 2, 2, 2, 4, 1, 2, 2, 4],
    melPhlegSection3: [4, 3, 5, 5, 3, 4, 3, 2, 5, 5],
    melPhlegSection4: [3, 5, 3, 4, 2, 2, 3, 4, 3, 4],
    phlegChlorSection1: [1, 4, 2, 2, 2, 4, 1, 2, 2, 4],
    phlegChlorSection2: [3, 5, 3, 4, 2, 2, 3, 4, 3, 4],
    phlegChlorSection3: [1, 2, 3, 3, 1, 2, 3, 2, 2, 1],
    phlegChlorSection4: [4, 5, 5, 5, 3, 4, 3, 2, 5, 5],
}


// Available Tests
/* 
  sanChlor
  melPhleg
  phlegChlor
*/

export const getAnswers = (temperament) => {
    let answers = []; 

    // generate demo answers for the four sections
    let questionsNumber = 0;
    for(var i=1; i <= 4; i++) { 
        // i refers to the section number
        let sectionAnswers = temperament + "Section" + i; // Section[1|2|3|4] 
        for(var j=0; j < demoAnswers[sectionAnswers].length; j++) {
            questionsNumber++;
            let answerObject = {};
            answerObject.optionNumber = demoAnswers[sectionAnswers][j];
            answerObject.questionSerial = questionsNumber;
            answerObject.sectionNumber = i;
            
            if(answers.length < 39) {
            }
            answers.push(answerObject);
        }
    }
    
    // console.log("answers", answers);
    return answers;
}


/* Algorithm to determine the temperament Blend
    ONCE YOU HAVE COMPLETED ALL OF THE ABOVE SECTIONS READ HERE:
    After you have finished all of the above 4 Sections go back 
    and cancel out each description that you scored either a 1 or 2 by 
    drawing a line through that number. Since that score is so low it 
    doesn't really apply to your overall scoring in each Section. 
    Now add up all of the 3's, 4's, & 5's in each Section and write your 
    total somewhere within each appropriate Section. The Section with the
    highest score is your Primary Temperament and the Section with the 
    second highest score is your Secondary Temperament. No one is one pure temperament,
    but instead we are a blend of all the temperaments.
*/




