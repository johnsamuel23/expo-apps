export const questions = [
    {
        section: 1,
        serial_number: 1,
        text: 'Emotional',
    },
    {
        section: 1,
        serial_number: 2,
        text: 'Interrupts others',
    },
    {
        section: 1,
        serial_number: 3,
        text: 'Compassionate',
    },
    {
        section: 1,
        serial_number: 4,
        text: 'Funny',
    },
    {
        section: 1,
        serial_number: 5,
        text: 'Easily angered',
    },
    {
        section: 1,
        serial_number: 6,
        text: 'Extrovert',
    },
    {
        section: 1,
        serial_number: 7,
        text: 'Talkative',
    },
    {
        section: 1,
        serial_number: 8,
        text: 'Popular',
    },
    {
        section: 1,
        serial_number: 9,
        text: 'Delightful / Cheerful',
    },
    {
        section: 1,
        serial_number: 10,
        text: 'Difficulty concentrating',
    },
    {
        section: 2,
        serial_number: 11,
        text: 'Determined',
    },
    {
        section: 2,
        serial_number: 12,
        text: 'Bossy',
    },
    {
        section: 2,
        serial_number: 13,
        text: 'Goal-oriented',
    },
    {
        section: 2,
        serial_number: 14,
        text: 'Self-confident',
    },
    {
        section: 2,
        serial_number: 15,
        text: 'Practical',
    },
    {
        section: 2,
        serial_number: 16,
        text: 'Outgoing',
    },
    {
        section: 2,
        serial_number: 17,
        text: 'Adventurous',
    },
    {
        section: 2,
        serial_number: 18,
        text: 'Outspoken',
    },
    {
        section: 2,
        serial_number: 19,
        text: 'Hot-tempered',
    },
    {
        section: 2,
        serial_number: 20,
        text: 'Leadership ability',
    },
    {
        section: 3,
        serial_number: 21,
        text: 'Critical',
    },
    {
        section: 3,
        serial_number: 22,
        text: 'Indecisive',
    },
    {
        section: 3,
        serial_number: 23,
        text: 'Self-centered',
    },
    {
        section: 3,
        serial_number: 24,
        text: 'Pessimistic',
    },
    {
        section: 3,
        serial_number: 25,
        text: 'Introvert',
    },
    {
        section: 3,
        serial_number: 26,
        text: 'Faithful Friend',
    },
    {
        section: 3,
        serial_number: 27,
        text: 'Analytical',
    },
    {
        section: 3,
        serial_number: 28,
        text: 'Respectful',
    },
    {
        section: 3,
        serial_number: 29,
        text: 'Creative',
    },
    {
        section: 3,
        serial_number: 30,
        text: 'Perfectionist',
    },
    {
        section: 4,
        serial_number: 31,
        text: 'Very quiet',
    },
    {
        section: 4,
        serial_number: 32,
        text: 'Selfish',
    },
    {
        section: 4,
        serial_number: 33,
        text: 'Easy-going',
    },
    {
        section: 4,
        serial_number: 34,
        text: 'Indecisive',
    },
    {
        section: 4,
        serial_number: 35,
        text: 'Submissive to others',
    },
    {
        section: 4,
        serial_number: 36,
        text: 'Reserved',
    },
    {
        section: 4,
        serial_number: 37,
        text: 'Patient',
    },
    {
        section: 4,
        serial_number: 38,
        text: 'Consistent',
    },
    {
        section: 4,
        serial_number: 39,
        text: 'Dependable',
    },
    {
        section: 4,
        serial_number: 40,
        text: 'Listener',
    },
]

export const getSectionQuestions = (section) => {
    let selectedQuestions = [];

    for(var i=0; i < questions.length; i++) {
        if(questions[i].section === section) {
            selectedQuestions.push(questions[i]);
        }
    }

    return selectedQuestions;
}
