import React from 'react';
import { Text, StyleSheet, View } from 'react-native';
import Br from '../components/Br';


const melancholicArticle = () => {
    return (
        <Text>
            Most of the world’s great composers, artists, musicians, inventors, philosophers, theologians and dedicated educators have been predominantly Melancholics. Think about Rembrandt, Van Gogh, Wagner and many others.
            <Br />
            Almost every true musician has some Melancholy temperament, whether he is a composer, performing artist or a soloist. When you listen to country-western, you can hear the wail of the Melancholic.
            <Br />
            One vocation that seems to attract the Melancholy is acting to the surprise of many, because we tend to identify this profession with extroverts. On stage they can adopt another personality, no matter how much extroversion it requires, but as soon as the play is over, he reverts back to his own introvert personality.
            <Br />
            Many melancholics work in the arts, many become craftsmen of high quality – finish carpenters, bricklayers, plumbers, plasters, scientists, horticulturalists, mechanics, engineers, and other professions that provide a meaningful service to humanity.
            <Br />
            Almost any humanitarian vocation will attract melancholics to its staff. It almost requires a Melancholic mind to get through the rigors of medical school, for a doctor has to be a perfectionist, an analytical specialist, and a humanitarian propelled by a heart that yearns to help other people.
            <Br />
            Any vocation that requires perfection, self-sacrifice and creativity is open to melancholics.
            <Br />

        </Text>
    );
}

const sanguineArticle = () => {
    return (
        <Text>
            Sanguines enrich the world with their natural charisma. They usually make excellent salespeople. If you ever want to see Sanguine in action, just visit your local used-car dealer. About two-thirds of the sales reps seem to be Sanguines.
            <Br />
            In addition to being good at sales, Sanguines make good actors as well, entertainers, and preachers (particularly evangelists). They are outstanding masters of ceremonies, auctioneers and sometimes leaders (if properly blended with another temperament). Because of our mass media today, they are increasingly in demand within the political arena, where natural charisma has proved to be a great advantage.
            <Br />
            In the area of helping people, Sanguines excel as hospital workers. Most sick people respond to a Sanguine nurse question “How are you today?” by saying, “Fine,” while nurse Melancholy can ask the same question and would probably receive a pity answer. Sanguines are never moderate about anything.
            <Br />
        </Text>
    );
}

const cholericArticle = () => {
    return (
        <Text>
            Cholerics might consider careers that require leadership, motivation and productivity. They do not require too much attention to details and analytical planning. Committee meetings and long-range planning bore the Choleric – a doer. Rarely will you find a predominant Choleric as a surgeon, dentist, philosopher, inventor or watchmaker. They often supervise craftsmen. They usually enjoy construction work, because it is so productive, and will frequently end up being foremen or project supervisors.
            <Br />
            A Choleric is a developer by nature. When they drive through the countryside, he envisions road graders carving out streets and builders construction homes, schools and shopping centers.
            <Br />
            They do well by hiring a melancholic as the architect with the analytical and creative ability to draw the plans for what he wants to build. The choleric person thinks it is enough to draw a few lines on the back of end of an envelope to gain the approval of the city planning department.So, it is a wise thing to do for the Choleric to hire a melancholic assistant or to go into a business partnership with a Melancholic. Together they make an unbeatable team.
            <Br />
            Most entrepreneurs are Cholerics. They are not by nature good delegators of responsibility (although with proper training they can learn). They tend to evaluate negatively the efforts others and end up trying to do everything themselves.
            <Br />
            The Choleric is a natural motivator of other people. He oozes self-confidence and is extremely goal-conscious. Associates may find themselves even more productive by following the leadership of a choleric.
            <Br />
        </Text>
    );
}

const phlegmaticArticle = () => {
    return (
        <Text>
            Phlegmatics seems drawn to the field of education. Most elementary school teachers are phlegmatic. Only they can have the patience to teach a group of first graders to read. A sanguine would spend the entire class period telling stories to the children. A melancholy would so criticize them that they would be afraid to read aloud. Can you imagine a choleric as a first grade teacher; the students might want to leap out of the windows!
            <Br />  
            The gentle nature the phlegmatic assures the ideal atmosphere for such learning. This is not only true on the elementary level but in both high school and college, particularly in math, physics, grammar, literature and languages.
            <Br />  
            It is not uncommon to find phlegmatics as school administrators, librarians, counsellors and college department heads.
            <Br />  
            Phlegmatics also like engineering. They are attracted to planning and calculation, they make good structural engineers, chemical engineers, mechanical and civil engineers and statisticians.
            <Br />  
            When given positions of leadership, they seem to bring order out of chaos and produce a working harmony conducive to increased productivity. They are well organized, never come to a meeting unprepared or late, tend to work well under pressure and are extremely dependable.
            <Br />  
            Phlegmatics are definitely not risk takers. They often stay with one company. Because they struggle with the problem of personal insecurity, they may take a job with retirement of security benefits in mind.
            <Br />  
            Therefore, civil service, the military, local government, or some other “good security risk” will attract them. Rarely will they launch out on a business venture of their own, although they are eminently qualified to do so. Instead they usually enhance the earning power of someone else and are quite content with a simple lifestyle.
            <Br />  
        </Text>
    );
}

// ------------------- there Articles on Temperament blends -------------- //

const MelPhlegBlendArticle = () => {
    return (
        <Text>
            Some of the greatest scholars the world has ever known have been MelPhlegs. These gifted introverts combine the analytical perfectionism of the melancholy with the organized efficiency of the phlegmatic. They are usually good-natured humanitarians who prefer a quiet, solitary environment for study and research to the endless rounds of activities sought by the more extroverted temperaments. You are usually excellent spellers and good mathematicians. These gifted people like you have greatly benefited humanity. Most of the world's significant inventions and medical discoveries have been made by MelPhlegs.
            <Br />
            Despite your abilities, the MelPhleg, like the rest of us, you have your own potential weaknesses. Unless controlled by God, you easily become discouraged and develops a very negative thinking pattern. Ordinarily you are a quiet person, who is capable of inner angers and hostility caused by your tendency to be vengeful.
            <Br />
            MelPhlegs are unusually vulnerable to fear, anxiety, and a negative self-image. It has always amazed us that the people with the greatest talents and capabilities are often victimized by genuine feelings of poor self-worth.             
            <Br />
        </Text>
    )
}

const MelChlorBlendArticle = () => {
    return (
        <Text>
            The mood swings of the melancholy are usually stabilized by the MelChlor's self-will and determination. There is almost nothing vocationally which you cannot do and do well. You are both a perfectionist and a driver. You possess strong leadership capabilities. Almost any craft, construction, or educational level is open to you. 
            <Br />
            The natural weaknesses of MelChlors reveal themselves in the mind, emotions, and mouth. You are an extremely difficult person to please, rarely satisfying even yourself. Once you start thinking negatively about something or someone (including yourself), you can be intolerable to live with. Your mood follows your thought process. Although you do not retain a depressed mood as long as the other blends of the melancholy, you can lapse into it more quickly. The two basic temperaments haunted by self-persecution, hostility, and criticism are the melancholy and the choleric. In extreme cases, you can become sadistic. When confronted with his vile thinking pattern and angry, bitter spirit, you can be expected to explode.            
            <Br />
        </Text>
    )
}

const MelSanBlendArticle = () => {
    return (
        <Text>
            MelSan is usually a very gifted person, fully capable of being a musician who can steal the heart of an audience. As an artist, you will not only draw or paint beautifully but can sell your own work - if you’re in the right mood. It is not uncommon to encounter MelSans in the field of education, for they make good scholars and probably the best of all classroom teachers, particularly on the high school and college level. The melancholy in you will ferret out little-known facts and be exacting in the use of events and detail, while the sanguine will enable you to communicate well with students.
            <Br />
            MelSan shows an interesting combination of mood swings. Be sure of this: you’re an emotional individual! When circumstances are pleasing, you can reflect a fantastically happy mood. But if things work out badly or you are rejected, insulted, or injured, you drop into such a mood that your lesser sanguine nature drowns in the resultant sea of self-pity. You are easily moved to tears, feels everything deeply, but can be unreasonably critical and hard on others. You tend to be rigid and usually will not cooperate unless things go your way, which is often idealistic and impractical. You often feel fearful and insecure, with a poor self-image which limits you unnecessarily.
            <Br />
        </Text>
    )
}

const ChlorMelBlendArticle = () => {
    return (
        <Text>
            The choleric/melancholy is an extremely industrious and capable person. The optimism and practicality of the choleric overcome the tendency toward moodiness of the melancholy, making the ChlorMel both goal-oriented and detailed. Such a person usually does well in school, possesses a quick, analytical mind, and yet is decisive. You develop into a thorough leader, the kind whom one can always count on to do an extraordinary job. People deer not take you on in a debate unless they are assured of their facts, for you will make mincemeat of them, combining verbal aggressiveness and attention to detail. You are extremely competitive and forceful in all that you do. You are a dogged researcher and usually successful, no matter what kind of business you pursue. This temperament probably makes the best natural leader. 
            <Br />
            Equally as great as your strengths, so also are your weaknesses. You are apt to be autocratic, a dictator type who inspires admiration and hate simultaneously. You are usually a quick-witted talker whose sarcasm can devastate others. You are a natural-born crusader whose work habits are irregular and long. A ChlorMel harbors considerable hostility and resentment, and unless you enjoy a good love relationship with your parents, you will find interpersonal relationships difficult, particularly with your family. No man is more apt to be an overly strict disciplinarian than the ChlorMel father. You combine the hard-to-please tendency of the choleric and the perfectionism of the melancholy.
            <Br />
        </Text>
    )
}

const ChlorPhlegBlendArticle = () => {
    return (
        <Text>
            The most subdued of all the extrovert temperaments is the ChlorPhleg, a happy blend of the quick, active, and hot with the calm, cool, and unexcited. You are not as apt to rush into things as quickly as the other extroverts because you are more deliberate and subdued. You are extremely capable in the long run, although you do not particularly impress people that way at first. You are very organized person who combines planning and hard work. People will usually enjoy working with and for you because youknows where you are going and has charted your course, yet is not unduly severe with people. You have the ability to help others make the best use of theirs skills and rarely offends people or makes them feel used. The ChlorPhleg's slogan on organization states: "Anything that needs to be done can be done better if it's organized." 
            <Br />
            In spite of your obvious capabilities, the ChlorPhleg is not without a notable set of weaknesses. Although not as addicted to the quick anger of some temperaments, you are known to harbor resentment and bitterness. Some of the cutting edge of choleric's sarcasm is here offset by the gracious spirit of the phlegmatic; so instead of uttering cutting and cruel remarks, your barbs are more apt to emerge as cleverly disguised humor. One is never quite sure whether you are kidding or ridiculing, depending on your mood. No one can be more bull headedly stubborn that a ChlorPhleg, and it will be very difficult for you to change your mind once it is committed. Repentance or the acknowledgment of a mistake is not at all easy for you. Consequently, you will be more apt to make it up to those you are wronged without really facing your mistake.             
            <Br />
        </Text>
    )
}

const ChlorSanBlendArticle = () => {
    return (
        <Text>
            The second-strongest extrovert among the blends of temperament will be the reverse of the first - the ChlorSan. Your life is given over completely to activity. Most of your efforts are productive and purposeful, but watch your recreation - it is so activity-prone that it borders being violent. Your are naturally a promoter and salesman, with enough charisma to get along well with others. Certainly the best motivator of people and one who thrives on a challenge, you are almost fearless and exhibit boundless energy. A convincing debater, what you lack in facts or arguments you makes up in bluff or bravado. As a teacher, you’re an excellent communicator, particularly in the social sciences; rarely you are drawn to math, science, or the abstract. Whatever your professional occupation, your brain is always in motion.
            <Br />
            Your weaknesses as ChlorSan, the chief of which is hostility, are as broad as your talents. You combine the quick, explosive anger of the sanguine (without the forgiveness) and the long-burning resentment of the choleric. You are one personality type who not only gets ulcers yourself, but gives them to others. Impatient with those who do not share your motivation and energy, you pride yourself on being brutally frank (some call it sarcastically frank). It is difficult for you to concentrate on one thing very long, which is why you often enlist others to finish what you have started. You are opinionated, prejudiced, impetuous, and inclined doggedly to finish a project you probably should not have started in the first place. If not controlled by God, you are apt to justify anything you do - and rarely hesitate to manipulate or walk over other people to accomplish your ends. Most ChlorSans get so engrossed in their work that they neglect wife and family, even lashing out at them if they complain. Once you comprehend the importance of giving love and approval to your family, however, you can transform your entire household.
            <Br />
        </Text>
    )
}

const SanMelBlendArticle = () => {
    return (
        <Text>
            SanMels are highly emotional people who fluctuate drastically. You can laugh hysterically one minute and burst into tears the next. It is almost impossible for you to hear a sad tale, observe a tragic plight of another person, or listen to melancholic music without weeping profusely. You genuinely feel the grief's of others. Almost any field is open to you, especially public speaking, acting, music, and the fine arts. However, SanMels reflect an uninhibited perfectionism that often alienates them from others because they verbalize their criticisms. They are usually a people-oriented individuals who have sufficient substance to make a contribution to others’ lives - if your ego and arrogance don't make you so obnoxious that others become hostile to you.
            <Br />
            One of the crucial weaknesses of this temperament blend prevails in SanMel's thought-life. Both sanguines and melancholies are dreamers, and thus if the melancholy part of his nature suggests a negative train of thought, it can nullify a SanMel's potential. It is easy for you to get down on yourself. In addition, more than most others, will have both an anger problem and a tendency toward fear. Both temperaments in your makeup suffer with an insecurity problem; not uncommonly, you are fearful to utilize your potential. Being admired by others is so important to you that it will drive you to a consistent level of performance. You have a great ability to commune with God, and if you walk in the Spirit you will make an effective servant of God.
            <Br />
        </Text>
    )
}

const SanPhlegBlendArticle = () => {
    return (
        <Text>
            The easiest person to like is a SanPhleg. The overpowering and obnoxious tendencies of a sanguine are offset by the gracious, easygoing phlegmatic. SanPhlegs are extremely happy people who carefree spirit and good humor make you lighthearted entertainers sought after by others. Helping people is your regular business, along with sales of various kinds. You are the least extroverted of any of the sanguines and often regulated by your environment and circumstances rather than being self motivated. SanPhlegs are naturally pro-family and preserve the love of their children - and everyone else for that matter. You would not purposefully hurt anyone.
            <Br />
            The SanPhleg's greatest weaknesses are lack of motivation and discipline. You would rather socialize than work, and you tend to take life too casually. You rarely get upset over anything and tend to find the bright side of everything. You usually have an endless repertoire of jokes and delights in making others laugh, often when the occasion calls for seriousness.
            <Br />
        </Text>
    )
}

const SanChlorBlendArticle = () => {
    return (
        <Text>
            The strongest extrovert of all the blends of temperaments is SanChlor, The happy charisma of the sanguine makes you an oriented, enthusiastic, salesman type; while the choleric side of your nature will provide you with the necessary resolution and character traits that will fashion a somewhat more organized and productive individual. 
            <Br />
            The potential weaknesses of a SanChlor are usually apparent to everyone because you are such an external person. You customarily talk too much, thus exposing yourself and your weaknesses for all to see. You are highly opinionated. Consequently, you express yourself loudly even before you knows all the facts. To be honest, no one has more mouth trouble! If you are the life of the party, you are lovable; but if you feel threatened or insecure, you can become obnoxious. Your leading emotional problem will be anger, which can catapult you into action at the slightest provocation. Since you combine the easy forgetfulness of the sanguine and the stubborn casuistry of the choleric, you may not have a very active conscience. Consequently, you tend to justify your actions
            <Br />
        </Text>
    )
}

const PhlegMelBlendArticle = () => {
    return (
        <Text>
            Of all the temperament blends, the PhlegMel is the most gracious, gentle, and quiet. They are rarely angry or hostile and almost never say anything for which they must apologize (mainly because they rarely say much). They never embarrass themselves or others, always do the proper thing, dress simply, dependable and exact. They tend to have the spiritual gifts of mercy and help, and are neat and organized in their working habits. Like any phlegmatic, they are handy around the house and as energy permits will keep the home in good repair.
            <Br />
            As a PhlegMel the other weaknesses revolve around fear, selfishness, negativism, criticism, and lack of self-image. Once a PhlegMel realizes that only his/her fears and negative feelings about himself/herself keep him/her from succeeding, they are able to come out of your shell and become very effective. Most PhlegMels are so afraid of over-extending themselves or getting over involved that they automatically refuse almost any kind of affiliation.            
            <Br />
        </Text>
    )
}

const PhlegChlorBlendArticle = () => {
    return (
        <Text>
            The most active of all phlegmatics is the PhlegChlor. But it must be remembered that since you are predominantly a phlegmatic, you will never be a ball of fire. Like phlegmatics, you are easy to get along with and may become an excellent group leader. The phlegmatic has the potential to become a good counselor, for they are excellent listeners, does not interrupt the client with stories about their self, and genuinely interested in other people. Although as a PhlegChlor you rarely offer your services to others, when they come to your organized office where you exercise control, you are first-rate professional. Your advice will be practical and helpful. Your gentle spirit never makes people feel threatened. You always do the right thing, rarely goes beyond the norm. 
            <Br />
            The weaknesses of the PhlegChlor are not readily apparent but gradually come to the surface, especially in the home. In addition to the lack of motivation and the fear problems of the other phlegmatics, you can be determinedly stubborn and unyielding. You don't blow up at others, but simply refuses to give in or cooperate. You are not a fighter by nature, but often letsyour inner anger and stubbornness reflect itself in silence. As a PhlegChlor you often retreat to your workshop alone or nightly immerse your mind in TV. The older you get, the more you selfishly indulge your sedentary tendency and become increasingly passive. Although you will probably live a long and peaceful life, if you indulge these passive feelings it is a boring life - not only for you, but the people around you.             
            <Br />
        </Text>
    )
}

const PhlegSanBlendArticle = () => {
    return (
        <Text>
            You are congenial, happy, cooperative, thoughtful, people-oriented, diplomatic, dependable, fun-loving, and humorous. A favorite with children and adults, you never display an abrasive personality. Ordinarily you attend a church/mosque where the pastor is a good motivator; there you probably take an active role.
            <Br />
            The weaknesses of the PhlegSan are as gentle as your personality - unless one has to live with you all the time. Since you inherited the lack of discipline of a sanguine, it is not uncommon for the PhlegSan to fall short of your true capabilities. You often pass up good opportunities, and avoid anything that involves "too much effort." Fear is another problem that accentuates your unrealistic feelings of insecurity. With more faith, you could grow beyond your timidity and self-defeating anxieties. However, you prefer to build a self-protective shell around yourself and selfishly avoids the kind of involvement or commitment to activity that you need and that would be a rich blessing to your partner. 
            <Br />
        </Text>
    )
}


export const articles = {
    "Melancholic": melancholicArticle(),
    "Sanguine": sanguineArticle(),
    "Phlegmatic": phlegmaticArticle(),
    "Choleric": cholericArticle()
}

export const blendedArticles = {
    "MelPhleg": MelPhlegBlendArticle(),
    "MelChlor": MelChlorBlendArticle(),
    "MelSan": MelSanBlendArticle(),
    "ChlorMel": ChlorMelBlendArticle(),
    "ChlorPhleg": ChlorPhlegBlendArticle(),
    "ChlorSan": ChlorSanBlendArticle(),
    "SanMel": SanMelBlendArticle(),
    "SanPhleg": SanPhlegBlendArticle(),
    "SanChlor": SanChlorBlendArticle(),
    "PhlegMel": PhlegMelBlendArticle(),
    "PhlegChlor": PhlegChlorBlendArticle(),
    "PhlegSan": PhlegSanBlendArticle(),
}