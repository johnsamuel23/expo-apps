export const theme =  {
    headerTextColor: '#fff',
    headerBackgroundColor: '#424242',
    buttonBorderColor: '#00a651',
    softGreenBackground: '#fafff1',
    temperaments: {
        "Melancholic": {
            bgColor: '#454544',
            textColor: '#ffffff'
        },
        "Choleric": {
            bgColor: '#ebc42b',
            textColor: '#555555'
        },
        "Phlegmatic": {
            bgColor: '#b2d1af',
            textColor: '#666565'
        },
        "Sanguine": {
            bgColor: '#e51c14',
            textColor: '#ffffff'
        },

    }
}