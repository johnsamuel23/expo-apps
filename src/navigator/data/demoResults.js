export const demoResults = [
    {
        userData: {
            firstName: "Blessing"
        },
        date: "29th Oct 2017",
        time: "10:15am",
        primaryTemperament: "Melancholic",
        secondaryTemperament: "Phlegmatic",
        temperamentBlend: "MelPhleg",
        primaryTemperamentRatio: 7,
        secondaryTemperamentRatio: 3,
    },
    {
        userData: {
            firstName: "Blessing"
        },
        date: "29th Oct 2017",
        time: "11:10am",
        primaryTemperament: "Melancholic",
        secondaryTemperament: "Phlegmatic",
        temperamentBlend: "MelPhleg",
        primaryTemperamentRatio: 6,
        secondaryTemperamentRatio: 4,
    },
    {
        userData: {
            firstName: "Blessing"
        },
        date: "30th Nov 2017",
        time: "10:15am",
        primaryTemperament: "Melancholic",
        secondaryTemperament: "Phlegmatic",
        temperamentBlend: "MelPhleg",
        primaryTemperamentRatio: 7,
        secondaryTemperamentRatio: 3,
    },
    {
        userData: {
            firstName: "John"
        },
        date: "1st June 2017",
        time: "1:15pm",
        primaryTemperament: "Choleric",
        secondaryTemperament: "Melancholic",
        temperamentBlend: "ChlorMel",
        primaryTemperamentRatio: 7,
        secondaryTemperamentRatio: 3,
    },
    {
        userData: {
            firstName: "John"
        },
        date: "1st June 2017",
        time: "1:15pm",
        primaryTemperament: "Choleric",
        secondaryTemperament: "Melancholic",
        temperamentBlend: "ChlorMel",
        primaryTemperamentRatio: 6,
        secondaryTemperamentRatio: 4,
    },
    {
        userData: {
            firstName: "Jeneth"
        },
        date: "1st June 2017",
        time: "2:15pm",
        primaryTemperament: "Choleric",
        secondaryTemperament: "Sanguine",
        temperamentBlend: "ChlorSan",
        primaryTemperamentRatio: 6,
        secondaryTemperamentRatio: 4,
    }
]