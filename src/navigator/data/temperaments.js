export const temperaments = {
    "Choleric": {
        abbreviation: "Chlor"
    },
    "Melancholic": {
        abbreviation: "Mel"
    },
    "Sanguine": {
        abbreviation: "San"
    },
    "Phlegmatic": {
        abbreviation: "Phleg"
    }
}

export const getTestResults = (scores) => {
    let result = {};
    let primaryTemperament = "";
    let primaryTemperamentScore = 0;
    let secondaryTemperament = "";
    let secondaryTemperamentScore = 0;
    let scorePoints = [];

    /*  Tests */
    // scores[2].sectionScores = 39;
    // scores[1].sectionScores = 40;
    /* End of Test */

    for(let i=0; i < scores.length; i++) {
        scorePoints.push(scores[i].sectionScores);
    }

    // arrange the scorePoints in descending order
    scorePoints.sort(function(a, b){return b-a});

    let primaryTemperamentIndex = 0; // the temperament at the first position in the arrangement
    let secondaryTemperamentIndex = 1;
    // console.log(scorePoints);
    let foundIndex = 0;
    for(let i=0; i < scores.length; i++) {
        if(scorePoints[primaryTemperamentIndex] === scores[i].sectionScores) {
            primaryTemperament = scores[i].sectionTemperament;
            primaryTemperamentScore = scores[i].sectionScores;
            foundIndex = i;
            break;
        }
    }

    // remove the first item in the scores array that has the top score
    // scores = popFirstHighest(scores, foundIndex);
    scores = scores.filter((item, index) => index !== foundIndex);
    // console.log("filtered scores", scores);

    for(let i=0; i < scores.length; i++) {
        if(scorePoints[secondaryTemperamentIndex] === scores[i].sectionScores) {
            secondaryTemperament = scores[i].sectionTemperament;
            secondaryTemperamentScore = scores[i].sectionScores;
        }
    }

    // console.log("ScorePoints", scorePoints);
    let flexRatio = getFlexRatios(primaryTemperamentScore, secondaryTemperamentScore);
    
    result = {
        primaryTemperament,
        primaryTemperamentScore,
        secondaryTemperament,
        secondaryTemperamentScore,
        flexRatio
    }

    // console.log("Scores ", scores);
    // console.log("result ", result);
    return result;
}

const getFlexRatios = (primaryTemperamentScore, secondaryTemperamentScore) => {
    let totatScore = primaryTemperamentScore + secondaryTemperamentScore;
    let primaryTemperamentValue = Math.round(primaryTemperamentScore / totatScore * 10);
    let secondaryTemperamentValue = 10 - primaryTemperamentValue;
    let result = {
        primary: primaryTemperamentValue,
        secondary: secondaryTemperamentValue
    }

    return result;
}

const popFirstHighest = (scores, scoreIndex) => {
    for(var i=0; i < scores.length; i++) {
        if(scoreIndex === i) {
            return scores = scores.filter((item, index) => index !== foundIndex);
        }
    }
} 

export const getTemperamentBlend = (primaryTemperament, secondaryTemperament) => {
    return temperaments[primaryTemperament].abbreviation + temperaments[secondaryTemperament].abbreviation;
}

export const getTopAnswers = (answers) => {
    let result = [];
    // Top answers are the one marked between 3 and 5 
    for(var i=0; i < answers.length; i++) {
        if(answers[i].optionNumber >= 3 ) {
            result.push(answers[i]);
        }
    }
    return result;
}

export const getScores = (answers) => {
    let scores = {};
    for(let i=0; i < answers.length; i++) {
        let sectionKey = "section_" + answers[i].sectionNumber;
        let answerNumber = answers[i].optionNumber;
        if(! scores.hasOwnProperty(sectionKey)) {
            scores[sectionKey] = [];
            scores[sectionKey].push(answerNumber);
        } else {
            scores[sectionKey].push(answerNumber);
        }
    }

    // console.log("Scores++++++", scores);
    return getSummedScores(scores);
}

export const getSectionTemperament = (section) => {
    if(section === 1) {
        return "Sanguine";
    } else if (section === 2) {
        return "Choleric";
    } else if (section === 3) {
        return "Melancholic";
    } else if (section === 4) {
        return "Phlegmatic";
    }
}    

export const getSummedScores = (scores) => {
    let summedScores = [];

    for(var i=1; i <= 4; i++) { // for the four sections
        let sectionNumber = i;
        let sectionKey = "section_" + sectionNumber;
        let sectionTemperament = getSectionTemperament(sectionNumber);
        let sectionScores = sumUp(scores[sectionKey]);
        let scoreData = {
            sectionTemperament,
            sectionNumber,
            sectionScores 
        };

        summedScores.push(scoreData);
    }

    // console.log("Summed Scores +++++ ", summedScores);
    return summedScores;
}

export const sumUp = (scores) => {
    let sum = 0;
    for(let i=0; i < scores.length; i++) {
        sum += scores[i];
    }

    return sum;
}