import React from 'react';
import {TextInput, View, StyleSheet} from 'react-native';
// import { theme } from '../data/theme';

export default TextInputBox = ({
    placeholder,
    style,
    value,
    changeInput,
    multiline,
    numberOfLines
}) => {

    return (
        <View style={[styles.container, style]}>
            <TextInput 
                  style={styles.input} 
                  placeholder={placeholder} 
                  placeholderTextColor="rgba(0,0,0,0.4)"
                  returnKeyType="next"
                  autoCapitalise="none"
                  autoCorrect={false}
                  value={value}
                  onChangeText={(value) => changeInput(value)}
                  multiline={multiline}
                  numberOfLines={numberOfLines}
                  underlineColorAndroid="transparent" />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    input: {
        backgroundColor: 'white',
        padding: 10,
        marginBottom: 13,
        fontSize: 15,
        fontFamily: 'raleway-semibold',
        borderRadius: 3,
        borderColor: '#a7a7a7',
        borderWidth: 1
    } 
});