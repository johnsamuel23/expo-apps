import React, { Component } from 'react';
import { StyleSheet, View, TouchableNativeFeedback, Text} from 'react-native';

export default Button = ({
    onPress,
    month,
    theme
}) => {
    return (
        <View style={styles.container}>
            <TouchableNativeFeedback 
                background={TouchableNativeFeedback.Ripple('rgba(0,0,0,0.2)')} 
                onPress={onPress}
            >
                <View style={[styles.button, {backgroundColor: theme.selectorBackgroundColor}]}>
                    <Text style={[styles.text, {color: theme.selectorTextColor}]}>{month}</Text>
                </View>
            </TouchableNativeFeedback>
        </View>
    )
}

const styles = StyleSheet.create({
    text: {
        color: '#555',
        fontSize: 15,
        textAlign: 'center'
    },
    button: {
        backgroundColor: '#fff',
        borderColor: '#eee',
        marginBottom: 15,
        padding: 15,
        borderRadius: 30
    }
});