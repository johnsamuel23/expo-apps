import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet
} from 'react-native';

export default class SelectedBox extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.dot}></View>
            </View>
        )
    }    
}

const styles = StyleSheet.create({
    container: {
        height: 18,
        width: 18,
        borderRadius: 9,
        borderColor: '#626262',
        borderWidth: 2,
        padding: 2
    },
    dot: {
        flex: 1,
        backgroundColor: 'red',
        backgroundColor: '#5d9c31',
        borderRadius: 9,        
    }
});