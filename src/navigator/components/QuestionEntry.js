import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet
} from 'react-native';
import OptionEntry from './OptionEntry';

export default QuestionEntry = ({
    selectOption,
    sectionNumber,
    questionSerial,
    questionText,
    selectedValue
}) => {

    let answer = 0;
    return (
        <View style={styles.questionEntry}>
            <View style={styles.questionTextContainer}>
                <Text style={styles.questionText}>{questionText}</Text> 
            </View>
            <View style={styles.optionsContainer}>
                <OptionEntry 
                    text="1" 
                    value={1} 
                    selectedValue={selectedValue} 
                    selectOption={() => selectOption(sectionNumber, questionSerial, answer=1)} />
                <OptionEntry 
                    text="2" 
                    value={2} 
                    selectedValue={selectedValue} 
                    selectOption={() => selectOption(sectionNumber, questionSerial, answer=2)} />
                <OptionEntry 
                    text="3" 
                    value={3} 
                    selectedValue={selectedValue} 
                    selectOption={() => selectOption(sectionNumber, questionSerial, answer=3)} />
                <OptionEntry 
                    text="4" 
                    value={4} 
                    selectedValue={selectedValue} 
                    selectOption={() => selectOption(sectionNumber, questionSerial, answer=4)} />
                <OptionEntry 
                    text="5" 
                    value={5} 
                    selectedValue={selectedValue} 
                    selectOption={() => selectOption(sectionNumber, questionSerial, answer=5)} />
            </View>
        </View>
    )
}


const styles = StyleSheet.create({
    questionEntry: {
        flex: 1,
        // backgroundColor: 'white',
        height: 70,
        marginBottom: 20
    },
    optionsContainer: {
        flexDirection: 'row',
        marginTop: 5
    },
    questionText: {
        fontFamily: 'merriweather-light',
        fontSize: 18,
        color: '#525252'
    }
});