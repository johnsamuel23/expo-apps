import React, { Component } from 'react';
import { 
    View, Text, StyleSheet, TouchableNativeFeedback
} from 'react-native';

export default Button = ({
    text,
    backgroundColor,
    bordercolor,
    switchScreen,
    icon,
    textStyles,
    buttonStyles,
    inline,
    children
}) => {
    let iconSet = false;
    let iconBtn = (<View></View>);
    let extraStyles = {};
    if(icon !== undefined) {
        iconSet = true;
        iconBtn = (<View style={styles.icon}>{icon}</View>);
        extraStyles.buttonText = {marginRight: -60}
    } 

    if(inline !== undefined) {
        extraStyles.buttonText = {marginRight: 0, textAlign: 'left', padding: 0, alignSelf: 'flex-start'};
        extraStyles.buttonStyles = {justifyContent: 'flex-start', alignItems: 'center', padding: 0};
        extraStyles.iconContainer = {marginRight: 0};
    } 

    if(!children) {
        children = (
            <View style={[styles.buttonContainer, buttonStyles, extraStyles.buttonStyles]}> 
                <View style={styles.buttonTextContainer}><Text style={[styles.buttonText, extraStyles.buttonText, textStyles]}>{text}</Text></View>
                {iconSet && <View style={[styles.iconContainer, extraStyles.iconContainer]}>{iconBtn}</View> }
            </View>
        );
    } else {
        children = (
            <View style={[styles.buttonContainer, buttonStyles, extraStyles.buttonStyles]}> 
                {children}
            </View>
        );
    }

    return (
        <View style={styles.container}>
            <TouchableNativeFeedback 
                onPress={() => switchScreen()}
                background={TouchableNativeFeedback.Ripple('rgba(0,0,0,0.2)')}>
                    { children }
            </TouchableNativeFeedback>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
    },
    icon: {
        // marginLeft: 10,
    },
    buttonTextContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    iconContainer: {
        // backgroundColor: 'red',
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        width: 40,
        marginRight: 20
    },
    buttonText: {
        fontFamily: 'raleway-semibold',
        fontSize: 20,
        color: '#5d9c31',
        color: '#00a651',
        textAlign: 'center'
    },
    buttonContainer: {
        backgroundColor: '#e6f9c9',
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
        borderColor: '#00a651',
        borderRadius: 3,
        flexDirection: 'row',
        paddingLeft: 20,
        paddingRight: 20
    }
});