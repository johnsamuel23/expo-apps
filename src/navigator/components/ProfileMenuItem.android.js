import React from 'react';
import { View, StyleSheet, Platform, Button, Text, TouchableNativeFeedback } from 'react-native';
import LocalImage from './LocalImage';

export default MenuItem = ({
  changeScreen,
  title,
  backgroundColor,
  textColor,
  iconImage,
}) => {
  return (
    <TouchableNativeFeedback 
        onPress={() => changeScreen()}
        background={TouchableNativeFeedback.Ripple('rgba(0,0,0,0.2)')} 
        style={[styles.overlay]}
    >
      <View style={[styles.container, {backgroundColor}]}>
        <View style={styles.content}>
          <View style={styles.iconImage}>
            <LocalImage 
              source={iconImage}
              originalHeight={300}
              originalWidth={300}
              guideWidth={50}
              />
          </View>
          <View style={styles.textContainer}>
            <Text style={[styles.text, {color: textColor}]}>{title}</Text>
          </View>
        </View>
      </View>
        
    </TouchableNativeFeedback>
  );
}

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    height: 80,
    margin: 15,
    marginTop: 10,
    marginBottom: 10,
    padding: 10,
    borderRadius: 3,
    elevation: 3,
    justifyContent: 'center'
  },
  content: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  textContainer: {
    justifyContent: 'center',
  },
  iconImage: {
    marginRight: 20,
    marginLeft: 5
  },
  text: {
    fontSize: 22,
    fontFamily: 'raleway-semibold'
  },
  boxSelected: {
    width:100,
    height:100,
    backgroundColor:'blue',
    borderRadius:10,
    ...Platform.select({
      ios: {
        shadowColor: 'rgba(0,0,0, .7)',
        shadowOffset: { height:0, width: 0 },
        shadowOpacity: 1,
        shadowRadius: 5,
      },
      android: {
        elevation: 5
      },
    }),
  },
  boxUnselected: {
    width:100,
    height:100,
    backgroundColor:'blue',
    borderRadius:10,
  }
});
