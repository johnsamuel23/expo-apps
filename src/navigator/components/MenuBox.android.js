import React from 'react';
import { View, StyleSheet, Platform, Button, TouchableNativeFeedback } from 'react-native';

export default MenuBox = ({
  children,
  changeScreen,
  style
}) => {
  return (
    <TouchableNativeFeedback 
        onPress={() => changeScreen()}
        style={[styles.overlay]}
    >
      <View style={[styles.container, style]}>
        {children}
      </View>
        
    </TouchableNativeFeedback>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
    height: 100,
    margin: 15,
    padding: 10,
    borderRadius: 3,
    elevation: 3
  },
  boxSelected: {
    width:100,
    height:100,
    backgroundColor:'blue',
    borderRadius:10,
    ...Platform.select({
      ios: {
        shadowColor: 'rgba(0,0,0, .7)',
        shadowOffset: { height:0, width: 0 },
        shadowOpacity: 1,
        shadowRadius: 5,
      },
      android: {
        elevation: 5
      },
    }),
  },
  boxUnselected: {
    width:100,
    height:100,
    backgroundColor:'blue',
    borderRadius:10,
  }
});
