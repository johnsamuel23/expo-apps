import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet
} from 'react-native';

export default class NonSelectedBox extends Component {
    render() {
        return (
            <View style={styles.container}>
            </View>
        )
    }    
}

const styles = StyleSheet.create({
    container: {
        height: 18,
        width: 18,
        borderRadius: 9,
        borderColor: '#626262',
        borderWidth: 2
    }
});