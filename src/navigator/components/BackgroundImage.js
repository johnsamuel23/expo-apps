import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, Image, Dimensions } from 'react-native';
import LocalImage from '../components/LocalImage';

export default class BackgroundImage extends Component {

    render() {
        return (
            <Image 
                style={[styles.backgroundImage, this.props.style]}
                source={this.props.source} 
                guideWidth={Dimensions.get('window').width}>

                        {this.props.children}

            </Image>
        )
    }
}

const styles = StyleSheet.create({
    backgroundImage: {
        width: null,
        height: null,
        resizeMode: 'cover',
    }
});