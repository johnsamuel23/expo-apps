import React, { Component } from 'react';
import { Text, StyleSheet, View } from 'react-native';
import LocalImage from './LocalImage';

export default Bullet = ({text, style}) => {
    return (
        <View style={[styles.container, style]}>
            <LocalImage
                source={require('../img/bullet_small.png')}
                originalWidth={50}
                originalHeight={72}
                guideWidth={20} 
            />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
    }
});
