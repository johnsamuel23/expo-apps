import React, { Component } from 'react';
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    TouchableHighlight,
} from 'react-native';
import NonSelectedBox from './NonSelectedBox';
import SelectedBox from './SelectedBox';

export default OptionEntry = ({
    text,
    selectOption,
    selectedValue,
    value
}) => {
    let box = (<NonSelectedBox />);
    if(selectedValue === value) { 
        box = (<SelectedBox />);
    }

    return (
        <TouchableOpacity onPress={() => selectOption()}>
            <View style={styles.container}>
                <View style={styles.box}>
                    { box }
                </View>
                <View style={styles.textContainer}><Text style={styles.text}>{text}</Text></View>
            </View>
        </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    container: {
        // backgroundColor: '#ccc',
        height: 30,
        width: 35,
        // marginBottom: 15,
        marginRight: 20,
        flexDirection: 'row',
        // justifyContent: 'center',
        alignItems: 'center',
    },
    box: {
        marginRight: 5,
        flex: 1
    },
    text: {
        fontSize: 18,
        // fontFamily: 'merriweather-light',
        fontFamily: 'segoe-ui',        
        alignSelf: 'center',
        textAlignVertical: 'center',
        color: '#5d9c31'
    },
    textContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1
    }
});