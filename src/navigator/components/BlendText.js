import React, { Component } from 'react';
import { Text, StyleSheet } from 'react-native';

export default BlendText = ({text, style}) => {
    return (
        <Text style={[style, styles.tempText]}>{text}</Text>
    )
}

const styles = StyleSheet.create({
    tempText: {
        fontSize: 15,
        // fontFamily: 'merriweather-light',
        fontFamily: 'raleway-semibold',
        // fontWeight: "700",
    },
});
