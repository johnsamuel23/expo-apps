import React, { Component } from 'react';
import {
    View,
    StyleSheet
} from 'react-native';

export default CheckBox = ({
    checked
}) => {
    return (
        <View style={[styles.container]}>
            {checked && <View style={styles.dot}></View>}
        </View>
    )    
}

const styles = StyleSheet.create({
    container: {
        height: 18,
        width: 18,
        borderRadius: 0,
        borderColor: '#a7a7a7',
        borderWidth: 2,
        padding: 2
    },
    dot: {
        flex: 1,
        backgroundColor: 'red',
        backgroundColor: '#5d9c31',
        borderRadius: 0,        
    }
});