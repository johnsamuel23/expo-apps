import React from 'react';
import { View, StyleSheet, Platform, TouchableOpacity } from 'react-native';

export default MenuBox = ({
  children,
  changeScreen,
  style
}) => {
  return (
    <TouchableOpacity
        activeOpacity={0.4}
        onPress={() => changeScreen()}
        style={[styles.overlay]}
    >
      <View style={[styles.container, style]}>
        {children}
      </View>
        
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
    height: 100,
    margin: 15,
    padding: 10,
    borderRadius: 3,
    shadowColor: 'rgba(0,0,0,0.4)',
    shadowOffset: { height:0, width: 0 },
    shadowOpacity: 1,
    shadowRadius: 5,
  },
});
