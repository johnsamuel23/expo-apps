import React, { Component } from 'react';
import { StyleSheet, View, TouchableOpacity, Text} from 'react-native';

export default Button = ({
    theme,
    day,
    onPress
}) => {
    let extraStyles = {};
    // extraStyles = {width: 60, height: 30};

    return (
        <View style={styles.container}>
            <TouchableOpacity style={[styles.button, {backgroundColor: theme.selectorBackgroundColor }, extraStyles]} 
                onPress={onPress}>
                <Text style={[styles.text, {color: theme.selectorTextColor}]}>{day}</Text>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    text: {
        fontSize: 15,
        textAlign: 'center'
    },
    button: {
        // backgroundColor: '#6ac3f7',
        // backgroundColor: '#c0e6fc',
        borderColor: '#eee',
        marginBottom: 10,
        width: 50,
        height: 50,
        margin: 5,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 25
    }
});