import React, { Component } from 'react';
import { 
    View, 
    Text, 
    StyleSheet, 
    TouchableNativeFeedback,
    TouchableOpacity, 
    Dimensions,
    ScrollView
} from 'react-native';
import { Font, Notifications, Constants } from 'expo';
import BackgroundImage from './components/BackgroundImage';
import LocalImage from './components/LocalImage';
import StatusBar from './components/StatusBar';
import { theme } from './data/theme';
import Button from './components/Button';
import ForwardIcon from './utils/icons/Forward';
import BackIcon from './utils/icons/Back';
import Br from './components/Br';
import ProfileMenuItem from './components/ProfileMenuItem';


export default class PersonalityMenuScreen extends Component {

    componentWillMount() {
    }

    changeScreen(temperament) {
        this.props.setArticleTemperament(temperament);
        setTimeout(() => {
            this.props.navigator.push({name: 'personality_profile_screen'});
        }, 50);
    }

    render() {

        return (
            <View style={styles.container}>
                <StatusBar backgroundColor={theme.headerBackgroundColor} />
                <View style={styles.content}>
                    <View style={styles.heading}>
                        <View style={[styles.headBox, {backgroundColor: theme.headerBackgroundColor}]}>
                            <View style={styles.headerLeft}>
                                <TouchableOpacity style={styles.backIcon} 
                                    onPress={() => this.props.navigator.pop({name: 'personalities_menu_screen'})}>
                                    <BackIcon color={theme.headerTextColor} size={35} />
                                </TouchableOpacity>
                            </View>
                            <View style={styles.headerCenter}>
                                <Text style={[styles.headText, {color: theme.headerTextColor}]}>Select Temperament</Text>
                            </View>
                            <View style={styles.headerRight}></View>
                        </View>
                    </View>
                    <View style={styles.introTextArea}>
                        <ScrollView style={[{flex: 1}, styles.introTextContaniner]} contentContainerStyle={styles.introTextContaniner}>
                            <ProfileMenuItem 
                                title="Sanguine" 
                                changeScreen={() => this.changeScreen('Sanguine')}
                                backgroundColor={theme.temperaments['Sanguine'].bgColor}
                                textColor={theme.temperaments['Sanguine'].textColor}
                                iconImage={require('./img/sanguineIcon.png')} />
                            <ProfileMenuItem 
                                title="Choleric" 
                                backgroundColor={theme.temperaments['Choleric'].bgColor}
                                textColor={theme.temperaments['Choleric'].textColor}
                                changeScreen={() => this.changeScreen('Choleric')}
                                iconImage={require('./img/cholericIcon.png')} />
                            <ProfileMenuItem 
                                title="Melancholic" 
                                backgroundColor={theme.temperaments['Melancholic'].bgColor}
                                textColor={theme.temperaments['Melancholic'].textColor}
                                changeScreen={() => this.changeScreen('Melancholic')}
                                iconImage={require('./img/melancholicIcon.png')} />
                            <ProfileMenuItem 
                                title="Phlegmatic" 
                                backgroundColor={theme.temperaments['Phlegmatic'].bgColor}
                                textColor={theme.temperaments['Phlegmatic'].textColor}
                                changeScreen={() => this.changeScreen('Phlegmatic')}
                                iconImage={require('./img/phlegmaticIcon.png')} />
                        </ScrollView>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        flex: 1
    },
    section: {
        padding: 20
    },
    textContainer: {
        // marginBottom: 10
    },
    topSectionText: {
        fontFamily: 'merriweather-light',
        textAlign: 'center',
        fontSize: 17,
        color: '#626262'
    },
    strong: {
        fontWeight: "500"
    },
    content: {
        flex: 1
    },
    contentRow: {
        flexDirection: 'row',
        paddingLeft: 10,
        paddingRight: 10,
    },
    menuBoxes: {
        justifyContent: 'center',
        flex: 1
    },
    menuBox: {
        height: 200,
    },
    itemIcon: {
        flex: 1,
        justifyContent: 'flex-end',
    },
    itemText: {
        flex: 1,
        color: '#6e6e6e',
        fontFamily: 'raleway-medium',
        fontSize: 20,
        textAlign: 'center',
        marginTop: 15,
        lineHeight: 28
    },
    heading: {
        paddingLeft: 0,
        paddingRight: 0,
        paddingTop: 0,
        paddingBottom: 0,
        // backgroundColor: '#000',
    }, 
    headBox: {
        // borderRadius: 25,
        height: 50,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    headText: {
        textAlign: 'center',
        fontSize: 20,
        fontFamily: 'raleway-semibold'
    },
    headerLeft: {
        width: 50,
        paddingLeft: 20
    },
    headerCenter: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    }, 
    headerRight: {
        width: 50
    },
    introTextContaniner: {
        paddingTop: 10,
        paddingBottom: 20,
        paddingLeft: 2,
        paddingRight: 2,
    },
    introTextArea: {
        flex: 1
    },
    buttonLeft: {
        flex: 1,
        marginRight: 15
    },
    buttonRight: {
        flex: 1,
        marginLeft: 15,
    },
    actionBtn: {
        backgroundColor: 'white',
        height: 100,
        justifyContent: 'center',
        // alignItems: 'center',
        padding: 20,
        borderTopColor: '#f1f1f1',
        borderTopWidth: 1,
        flexDirection: 'row'
    },
    introText: {
        fontFamily: 'merriweather-light',
        fontSize: 17,
        lineHeight: 23,
        color: '#525252',
        textAlign: 'justify'
    },
    blendVisual: {
        height: 40,
        flexDirection: 'row',
        marginBottom: 20,
        marginTop: 20
    },
    primaryTemp: {
        flex: 3,
        // height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        borderTopLeftRadius: 3,
        borderBottomLeftRadius: 3,
    },
    secondaryTemp: {
        flex: 2,
        // height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        borderTopRightRadius: 3,
        borderBottomRightRadius: 3,
    },
    tempText: {
        fontFamily: 'raleway-semibold',
        fontSize: 15,
        // fontFamily: 'merriweather-light',
        // fontWeight: "700",
    },

});