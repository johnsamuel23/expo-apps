import React, { Component } from 'react';
import { 
    View, 
    Text, 
    StyleSheet, 
    TouchableNativeFeedback,
    TouchableOpacity, 
    Dimensions,
    ScrollView
} from 'react-native';
import { Font, Notifications, Constants } from 'expo';
import BackgroundImage from './components/BackgroundImage';
import LocalImage from './components/LocalImage';
import StatusBar from './components/StatusBar';
import { theme } from './data/theme';
import Button from './components/Button';
import ForwardIcon from './utils/icons/Forward';
import BackIcon from './utils/icons/Back';
import Br from './components/Br';


export default class TestGuideScreen extends Component {

    componentDidMount() {
        const { navigator } = this.props;
    }

    changeScreen(screen) {
        setTimeout(() => {
            this.props.navigator.push({name: screen});
        }, 50);
    }

    render() {

        let forwardIcon = (<ForwardIcon color={theme.buttonBorderColor} size={30} />);

        return (
            <View style={styles.container}>
                <StatusBar backgroundColor={theme.headerBackgroundColor} />
                <View style={styles.content}>
                    <View style={styles.heading}>
                        <View style={[styles.headBox, {backgroundColor: theme.headerBackgroundColor}]}>
                            <View style={styles.headerLeft}>
                                <TouchableOpacity style={styles.backIcon} 
                                    onPress={() => this.props.navigator.pop({name: 'test_introduction_screen'})}>
                                    <BackIcon color={theme.headerTextColor} size={35} />
                                </TouchableOpacity>
                            </View>
                            <View style={styles.headerCenter}>
                                <Text style={[styles.headText, {color: theme.headerTextColor}]}> Test Guide</Text>
                            </View>
                            <View style={styles.headerRight}></View>
                        </View>
                    </View>
                    <View style={styles.introTextArea}>
                        <ScrollView style={[{flex: 1}, styles.introTextContaniner]} contentContainerStyle={styles.introTextContaniner}>
                            <Text style={styles.introText}> 
                                It's important that you be honest and objective. Don't mark a box according to how you want to be seen, rather mark it according to how you really are.
                                <Br />
                                There are four (4) sections in this test. Don’t worry. They have only 10 questions each.
                                <Br />
                                Indicate the extent to which each character applies to you.
                                <Br />
                                This can be:
                                <Br single={true} />
                                1. Inaccurate
                                <Br single={true} />
                                2. Moderately inaccurate 
                                <Br single={true} />
                                3. Neither inaccurate nor accurate
                                <Br single={true} />
                                4. Moderately accurate
                                <Br single={true} />
                                5. Very accurate
                                <Br />

                            </Text>
                        </ScrollView>
                        <View style={styles.actionBtn}>
                            <Button text='Start Test' icon={forwardIcon} switchScreen={() => this.changeScreen('test_main_screen')}/>
                        </View>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fafff1',
        flex: 1
    },
    content: {
        flex: 1
    },
    contentRow: {
        flexDirection: 'row',
        paddingLeft: 10,
        paddingRight: 10,
    },
    menuBoxes: {
        justifyContent: 'center',
        flex: 1
    },
    menuBox: {
        height: 200,
    },
    itemIcon: {
        flex: 1,
        justifyContent: 'flex-end',
    },
    itemText: {
        flex: 1,
        color: '#6e6e6e',
        fontFamily: 'raleway-medium',
        fontSize: 20,
        textAlign: 'center',
        marginTop: 15,
        lineHeight: 28
    },
    heading: {
        paddingLeft: 0,
        paddingRight: 0,
        paddingTop: 0,
        paddingBottom: 0,
        // backgroundColor: '#000',
    }, 
    headBox: {
        // borderRadius: 25,
        height: 50,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    headText: {
        textAlign: 'center',
        fontSize: 20,
        fontFamily: 'raleway-semibold'
    },
    headerLeft: {
        width: 50,
        paddingLeft: 20
    },
    headerCenter: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    }, 
    headerRight: {
        width: 50
    },
    introTextContaniner: {
        // backgroundColor: 'red',
        padding: 10,
        marginBottom: 20
    },
    introTextArea: {
        flex: 1
    },
    actionBtn: {
        backgroundColor: 'white',
        height: 100,
        justifyContent: 'center',
        // alignItems: 'center',
        padding: 20,
        borderTopColor: '#f1f1f1',
        borderTopWidth: 1
    },
    introText: {
        fontFamily: 'merriweather-light',
        fontSize: 17,
        lineHeight: 23,
        color: '#525252',
        textAlign: 'justify'
    }

});