import React, { Component } from 'react';
import { 
    View, 
    Text, 
    StyleSheet, 
    TouchableNativeFeedback, 
    Dimensions, 
    Animated,
    Easing,
    LayoutAnimation,
    UIManager } from 'react-native';
import { Font, Notifications, Constants } from 'expo';
import BackgroundImage from './components/BackgroundImage';

export default class AppLoading extends Component {

    state = {
        tapCueActive: false
    }

    componentWillMount() {
        this.opacityValue = new Animated.Value(0);
    }

    componentDidMount() {
        const { navigator } = this.props;
        this.opacityAnim();

        if(navigator) {
            setTimeout(function(){
                navigator.push({name: 'app_introduction_screen'});
            }, 2000);
            setTimeout(() => {
                this.setState({
                    tapCueActive: true
                })
            }, 3000);
        }
    }

    opacityAnim() {
        this.opacityValue.setValue(0);
        Animated.timing(
            this.opacityValue,
            {
                toValue: 1,
                duration: 2000,
                easing: Easing.easeOutBack
            }
        ).start(() => {
            this.opacityAnim();
        });
    }

    switchScreen(screen) {
        if(this.state.tapCueActive) {
            setTimeout(() => {
                this.props.navigator.push({name: screen});
            }, 50);
        }
    }

    render() {

        const opacityValue = this.opacityValue.interpolate({
            inputRange: [0, 0.4, 0.7, 1],
            outputRange: [0, 1, 1, 0]
        }); 

        return (
            <View style={styles.container}>
                <View style={{flex: 1}}>
                    <BackgroundImage style={{zIndex: 0}} source={require('./img/splash_pic.png')}>
                        <View style={styles.backgroundView}>
                            {this.state.tapCueActive &&
                                <Animated.View style={[styles.tapCue, {opacity: opacityValue}]}>
                                    <Text style={styles.tapCueText}>Tap screen to continue</Text>
                                </Animated.View>
                            }
                        </View>   
                    </BackgroundImage>
                </View>
                <TouchableNativeFeedback 
                    onPress={() => this.switchScreen('app_introduction_screen')}
                    background={TouchableNativeFeedback.Ripple('rgba(0,0,0,0.2)')} 
                    style={[styles.overlay]}
                >
                    <View style={[styles.overlay, {opacity: 1, backgroundColor: 'transparent'}]}></View>
                </TouchableNativeFeedback>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        position: 'absolute',
        height: Dimensions.get('window').height,
        width: Dimensions.get('window').width,
        top: 1,
        bottom: 1
    },
    tapCue: {
        backgroundColor: 'rgba(0,0,0,0.5)',
        padding: 10,
        borderRadius: 20,
        alignSelf: 'flex-end',
        marginBottom: 150
    },
    tapCueText: {
        textAlign: 'center',
        fontSize: 12,
        color: '#fff'
    },
    backgroundView: {
        height: Dimensions.get('window').height,
        width: Dimensions.get('window').width,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
    overlay: {
        position: 'absolute',
        backgroundColor: 'red',
        zIndex: 10,
        height: Dimensions.get('window').height,
        width: Dimensions.get('window').width,
        justifyContent: 'center',
        alignItems: 'center'
    }


});