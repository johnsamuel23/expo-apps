import React, { Component } from 'react';
import { 
    View, 
    Text, 
    StyleSheet, 
    TouchableNativeFeedback,
    TouchableOpacity, 
    Dimensions,
    ScrollView
} from 'react-native';
import { Font, Notifications, Constants } from 'expo';
import BackgroundImage from './components/BackgroundImage';
import LocalImage from './components/LocalImage';
import StatusBar from './components/StatusBar';
import { theme } from './data/theme';
import Button from './components/Button';
import BackIcon from './utils/icons/Back';
import SendIcon from './utils/icons/Send';
import Br from './components/Br';
import TextInput from './components/TextInput';
import CheckBox from './components/CheckBox';
import Popup from 'react-native-popup';
import { saveCoachMessage } from './utils/api';

export default class CoachMessageScreen extends Component {

    componentWillMount() {
        let { store } = this.props;
        // console.log("This Store", store);
        
        this.state = {
            email: store.userData.email || "",
            fullName: store.userData.fullName || "",
            messageTitle: "",
            messageContent: "",
            popUpActive: true
        }
    }

    changeInput(field, value) {
        this.setState({
            [field]: value
        });
    }

    validateInputs() {
        let allFilled = false;
        let inputs = ["messageTitle", "messageContent", "email", "fullName"];
        for(let i=0; i < inputs.length; i++) {
            if(this.state[inputs[i]] === "") {
                return false;
            }
        }

        return true;
    }

    clearInput() {
        this.setState({
            messageTitle: "",
            messageContent: "",
        });
    }

    saveData() {
        let data = {
            email: this.state.email,
            fullName: this.state.fullName,
            messageTitle: this.state.messageTitle,
            messageContent: this.state.messageContent,
        };

        if(this.validateInputs() === true) {
            // this.changeScreen('test_result_screen');
            // send the message to the coach and wait for further user action
            this.clearInput();
            saveCoachMessage(data).then(response => {
                console.log("Message Sent To Coach", response.data);
            }).catch(error => {
                // console.log("Error Received", error);
            });
            this.popup.alert("Thank you. Your message has been sent. The Coach will reach you soon."); // demo             
        } else {
            this.popup.alert("Please fill all the fields. Thank You");
        }

        this.props.saveContactMessageData(data);
    }

    changeScreen(screen) {
        setTimeout(() => {
            this.props.navigator.push({name: screen});
        }, 50);
    }

    render() {

        let sendIcon = (<SendIcon color={theme.buttonBorderColor} size={20} />);

        return (
            <View style={styles.container}>
                <StatusBar backgroundColor={theme.headerBackgroundColor} />
                <View style={styles.content}>
                    <View style={styles.heading}>
                        <View style={[styles.headBox, {backgroundColor: theme.headerBackgroundColor}]}>
                            <View style={styles.headerLeft}>
                                <TouchableOpacity style={styles.backIcon} 
                                    onPress={() => this.props.navigator.pop({name: 'data_collection_screen'})}>
                                    <BackIcon color={theme.headerTextColor} size={35} />
                                </TouchableOpacity>
                            </View>
                            <View style={styles.headerCenter}>
                                <Text style={[styles.headText, {color: theme.headerTextColor}]}>Personal Information</Text>
                            </View>
                            <View style={styles.headerRight}></View>
                        </View>
                    </View>
                    <View style={styles.introTextArea}>
                        <ScrollView style={[{flex: 1}, styles.introTextContaniner]} contentContainerStyle={styles.introTextContaniner}>
                            <Text style={styles.introText}> 
                                Send a message to the Life Coach
                                <Br />
                            </Text>
                            <View style={{flex: 1}}>
                                <TextInput value={this.state.fullName} placeholder="Your Name" changeInput={(value) => this.changeInput('fullName', value)} />
                                <TextInput value={this.state.email} placeholder="Your Email" changeInput={(value) => this.changeInput('email', value)} />
                                <TextInput value={this.state.messageTitle} placeholder="Message Title" changeInput={(value) => this.changeInput('messageTitle', value)} />
                                <TextInput 
                                    multiline={true}
                                    numberOfLines={4}
                                    value={this.state.messageContent}
                                    placeholder="Compose Message" changeInput={(value) => this.changeInput('messageContent', value)} />
                                <View style={{marginTop: 0, marginBottom: 20, width: 250 }}>
                                    <Button text='Send Message' icon={sendIcon} inline={true} switchScreen={() => this.saveData()}/>
                                </View>
                            </View>
                        </ScrollView>
                        <View style={styles.actionBtn}>
                            <Button text='Main Menu' icon switchScreen={() => this.changeScreen('main_menu_screen')}/>
                        </View>
                    </View>
                </View>

                {<Popup ref={popup => this.popup = popup } />}

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        flex: 1
    },
    section: {
        padding: 20
    },
    textContainer: {
        // marginBottom: 10
    },
    topSectionText: {
        fontFamily: 'merriweather-light',
        textAlign: 'center',
        fontSize: 17,
        color: '#626262'
    },
    strong: {
        fontWeight: "500"
    },
    content: {
        flex: 1
    },
    contentRow: {
        flexDirection: 'row',
        paddingLeft: 10,
        paddingRight: 10,
    },
    menuBoxes: {
        justifyContent: 'center',
        flex: 1
    },
    menuBox: {
        height: 200,
    },
    itemIcon: {
        flex: 1,
        justifyContent: 'flex-end',
    },
    itemText: {
        flex: 1,
        color: '#6e6e6e',
        fontFamily: 'raleway-medium',
        fontSize: 20,
        textAlign: 'center',
        marginTop: 15,
        lineHeight: 28
    },
    heading: {
        paddingLeft: 0,
        paddingRight: 0,
        paddingTop: 0,
        paddingBottom: 0,
        // backgroundColor: '#000',
    }, 
    headBox: {
        // borderRadius: 25,
        height: 50,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    headText: {
        textAlign: 'center',
        fontSize: 20,
        fontFamily: 'raleway-semibold'
    },
    headerLeft: {
        width: 50,
        paddingLeft: 20
    },
    headerCenter: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    }, 
    headerRight: {
        width: 50
    },
    introTextContaniner: {
        // marginBottom: 20,
        backgroundColor: theme.softGreenBackground,
        padding: 10
    },
    introTextArea: {
        flex: 1
    },
    buttonLeft: {
        flex: 1,
        marginRight: 15
    },
    buttonRight: {
        flex: 1,
        marginLeft: 15,
    },
    actionBtn: {
        backgroundColor: 'white',
        height: 100,
        justifyContent: 'center',
        // alignItems: 'center',
        padding: 20,
        borderTopColor: '#f1f1f1',
        borderTopWidth: 1,
        // flexDirection: 'row'
    },
    introText: {
        fontFamily: 'merriweather-light',
        fontSize: 17,
        lineHeight: 23,
        color: '#525252',
        textAlign: 'center'
    },
    blendVisual: {
        height: 40,
        flexDirection: 'row',
        marginBottom: 20,
        marginTop: 20
    },
    primaryTemp: {
        flex: 3,
        // height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        borderTopLeftRadius: 3,
        borderBottomLeftRadius: 3,
    },
    secondaryTemp: {
        flex: 2,
        // height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        borderTopRightRadius: 3,
        borderBottomRightRadius: 3,
    },
    tempText: {
        fontFamily: 'raleway-semibold',
        fontSize: 15,
        // fontFamily: 'merriweather-light',
        // fontWeight: "700",
    },
    lastInputRow: {
        flexDirection: 'row',
    },
    subscriptionRow: {
        flexDirection: 'row'
    },
    subscribeText: {
        color: '#777777',
        fontSize: 15,
        fontFamily: 'merriweather-light'
    }

});