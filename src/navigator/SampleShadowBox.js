import React, { Component } from 'react';
import { View, StyleSheet,Platform,Button } from 'react-native';

export default class App extends Component {
  state = {
    isSelected: false
  }
  render() {
    return (
      <View style={styles.container}>
        <View style={this.state.isSelected ? styles.boxSelected : styles.boxUnselected} ></View>
        <Button title='toggle select' onPress={() => this.setState({isSelected: !this.state.isSelected}) } />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#ecf0f1',
  },
  boxSelected: {
    width:100,
    height:100,
    backgroundColor:'blue',
    borderRadius:10,
    ...Platform.select({
      ios: {
        shadowColor: 'rgba(0,0,0, .7)',
        shadowOffset: { height:0, width: 0 },
        shadowOpacity: 1,
        shadowRadius: 5,
      },
      android: {
        elevation: 5
      },
    }),
  },
  boxUnselected: {
    width:100,
    height:100,
    backgroundColor:'blue',
    borderRadius:10,
  }
});
