import React, { Component } from 'react';
import { FontAwesome } from '@expo/vector-icons';
import { View } from 'react-native';

export default Send = ({
    size,
    color
}) => {
    const Icon = (<FontAwesome name="send" size={size} color={color} />)
    return (
        <View>
            { Icon }
        </View>
    )

}