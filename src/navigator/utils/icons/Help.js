import React, { Component } from 'react';
import { FontAwesome } from '@expo/vector-icons';
import { View } from 'react-native';

export default Help = ({
    size,
    color
}) => {
    const Icon = (<FontAwesome name="question-circle-o" size={size} color={color} />)
    return (
        <View>
            { Icon }     
        </View>
    )

}