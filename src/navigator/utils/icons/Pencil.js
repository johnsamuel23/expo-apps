import React, { Component } from 'react';
import { FontAwesome } from '@expo/vector-icons';
import { View } from 'react-native';

export default Pencil = ({
    size,
    color
}) => {
    const Icon = (<FontAwesome name="pencil-square" size={size} color={color} />)
    return (
        <View>
            { Icon }     
        </View>
    )

}