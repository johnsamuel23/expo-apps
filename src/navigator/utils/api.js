import axios from 'axios';

export const saveTestResult = (data) => {
    let requestParameters = "email=" + data.email;
    requestParameters = requestParameters + "&personality_blend=" + data.personalityBlend;
    requestParameters = requestParameters + "&name=" + data.fullName;
    requestParameters = requestParameters + "&academic_level=" + data.academicLevel;
    requestParameters = requestParameters + "&age=" + data.age;
    requestParameters = requestParameters + "&subscribed=" + data.subscriptionChecked;
    requestParameters = requestParameters + "&country=" + data.country;
    requestParameters = requestParameters + "&blend_info=" + data.blendInfo;
    console.log(requestParameters);
    return axios.get("http://basiclifecoach.com/apps/scripts/sendTestResult.php?" + requestParameters);
}

export const saveCoachMessage = (data) => {
    let requestParameters = "email=" + data.email;
    requestParameters = requestParameters + "&name=" + data.fullName;
    requestParameters = requestParameters + "&subject=" + data.messageTitle;
    requestParameters = requestParameters + "&message=" + data.messageContent;
    console.log(requestParameters);
    return axios.get("http://basiclifecoach.com/apps/scripts/sendCoachMessage.php?" + requestParameters);
}