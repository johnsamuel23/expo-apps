import React, { Component } from 'react';
import { 
    View, 
    Text, 
    StyleSheet, 
    TouchableNativeFeedback,
    TouchableOpacity, 
    Dimensions,
    ScrollView,
    AsyncStorage
} from 'react-native';
import { Font, Notifications, Constants } from 'expo';
import BackgroundImage from './components/BackgroundImage';
import LocalImage from './components/LocalImage';
import StatusBar from './components/StatusBar';
import { theme } from './data/theme';
import Button from './components/Button';
import BackIcon from './utils/icons/Back';
import Br from './components/Br';
import TextInput from './components/TextInput';
import CheckBox from './components/CheckBox';
import Popup from 'react-native-popup';
import UserSelectMenu from './UserSelectScreen';
import { saveTestResult as saveData } from './utils/api';
import { temperaments, getScores, getTopAnswers, getTemperamentBlend, getTestResults} from './data/temperaments';

export default class DataCollectionScreen extends Component {

    constructor(props) {
        super(props);
        let { store } = this.props;
        let { userData } = store;
        this.state = {
            subscriptionChecked: userData.subscriptionChecked,
            fullName: userData.fullName,
            academicLevel: userData.academicLevel,
            email: userData.email,
            age: userData.age,
            country: userData.country,
            popUpActive: true,
            userSelectActive: false
        }
    }

    changeInput(field, value) {
        this.setState({
            [field]: value
        });
    }

    sendResult(data) {
        saveData(data).then(response => {
            console.log(response.data);
        });
    }

    formatAMPM(date) {
        let hours = date.getHours();
        let minutes = date.getMinutes();
        let ampm = hours >= 12 ? 'pm' : 'am';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 ? '0'+minutes : minutes;
        let strTime = hours + ':' + minutes + ' ' + ampm;
        return strTime;
    }

    async storeUserResult(userData) {
        // Fetching data:
        let resultsFound = false;
        let savedTestResults = "";
        
        try {
            // get the results saved 
            savedTestResults = await AsyncStorage.getItem('savedTestResults');
            if (savedTestResults !== null){
                resultsFound = true;
            } 
        } catch (error) {
            // resultsFound = false;
        }

        // console.log("RESULTS_FOUND", resultsFound);

        let dateObj = new Date();
        let month = dateObj.getUTCMonth() + 1; // usually 0 - 11. So +1 is need to correspond with human reading
        let day = dateObj.getUTCDate();
        let year = dateObj.getUTCFullYear();
        let newDate = day + "/" + month + "/" + year;

        let date = new Date();
        let displayTime = this.formatAMPM(date);
        
        let splitName = userData.fullName.split(" ");
        let firstName = splitName[0];
        let lastName = splitName.length > 1 ? splitName[splitName.length - 1] : "";

        let userResult = {
            userData: {
                firstName: firstName,
                fullName: userData.fullName,
                lastName: lastName,
                email: userData.email,
            },
            date: newDate,
            time: displayTime,
            primaryTemperament: userData.primaryTemperament,
            secondaryTemperament: userData.secondaryTemperament,
            temperamentBlend: userData.temperamentBlend,
            primaryTemperamentRatio: userData.primaryTemperamentRatio,
            secondaryTemperamentRatio: userData.secondaryTemperamentRatio,
        };
        
        let results = [];
        let { store } = this.props;
        let { saveStateData } = this.props;
        let { justSaved } = store;

        // console.log("STORE", store);

        if(resultsFound) {
            // console.log("appUsers:", appUserValue);
            // appUserValue will be in string 
            // convert it to an array                 
            let fetchedResults = JSON.parse(savedTestResults);
            results = [...fetchedResults];
            if(! justSaved) {     
                results.push(userResult);
                saveStateData("justSaved", true);
            }
            // results = [];
            AsyncStorage.removeItem('savedTestResults');
            AsyncStorage.setItem('savedTestResults', JSON.stringify(results));
        } else {
            // appUserValue will be in string 
            // convert it to an array
            if(! justSaved) {                  
                results.push(userResult);
                saveStateData("justSaved", true);
            } 
            await AsyncStorage.setItem('savedTestResults', JSON.stringify(results));
        }
        
        saveStateData("results", results);
        
        // console.log("RESULTS--->", results);
        // console.log("USERDATA", userData);
    }

    validateInputs() {
        let allFilled = false;
        let inputs = ["fullName", "academicLevel", "email", "age", "country"];
        for(let i=0; i < inputs.length; i++) {
            if(this.state[inputs[i]] === "") {
                return false;
            }
        }

        return true;
    }

    processData() {
        let { store, saveStateData } = this.props;

        // console.log("store-----", store.returningResult);
        let primaryTemperament = "";
        let secondaryTemperament = "";
        let temperamentBlend = "";
        let primaryTemperamentRatio = "";
        let secondaryTemperamentRatio = "";
        
        this.answers = store.answers;
        let topAnswers = getTopAnswers(this.answers);
        let scores = getScores(topAnswers);
        let testResults = getTestResults(scores);
        primaryTemperament = testResults.primaryTemperament;
        secondaryTemperament = testResults.secondaryTemperament;
        temperamentBlend = getTemperamentBlend(primaryTemperament, secondaryTemperament);
        primaryTemperamentRatio = testResults.flexRatio.primary;
        secondaryTemperamentRatio = testResults.flexRatio.secondary;
    

        let stateData = {
            primaryTemperament,
            secondaryTemperament,
            temperamentBlend,
            primaryTemperamentRatio,
            secondaryTemperamentRatio
        }

        this.props.saveResult(stateData);

        // console.log("stateData -- ", stateData);
        // console.log("testResults -- ", testResults);
        // console.log("scores -- ", scores);
        let newUserData = {...store.userData, ...stateData};
        
        this.storeUserResult(newUserData);
    }

    saveData() {
        let { store } = this.props;
        this.answers = store.answers;
        let topAnswers = getTopAnswers(this.answers);
        let scores = getScores(topAnswers);
        let testResults = getTestResults(scores);
        primaryTemperament = testResults.primaryTemperament;
        secondaryTemperament = testResults.secondaryTemperament;
        temperamentBlend = getTemperamentBlend(primaryTemperament, secondaryTemperament);
        primaryTemperamentRatio = testResults.flexRatio.primary;
        secondaryTemperamentRatio = testResults.flexRatio.secondary;

        let data = {
            fullName: this.state.fullName,
            academicLevel: this.state.academicLevel,
            email: this.state.email,
            age: this.state.age,
            country: this.state.country,
            subscriptionChecked: this.state.subscriptionChecked,
            blendInfo: "",
            personalityBlend: temperamentBlend
        };

        if(this.validateInputs() === true) {
            saveData(data);
            this.processData();
            this.changeScreen('test_result_screen');
        } else {
            this.popup.alert("Please fill all the fields. Thank You");
        }

        this.props.saveUserData(data);
    }

    changeScreen(screen) {
        setTimeout(() => {
            this.props.navigator.push({name: screen});
        }, 50);
    }

    toggleUserMenu() {
        this.setState({
            userSelectActive: ! this.state.userSelectActive
        });
    }

    render() {

        return (
            <View style={styles.container}>
                <StatusBar backgroundColor={theme.headerBackgroundColor} />
                <View style={styles.content}>
                    <View style={styles.heading}>
                        <View style={[styles.headBox, {backgroundColor: theme.headerBackgroundColor}]}>
                            <View style={styles.headerLeft}>
                                <TouchableOpacity style={styles.backIcon} 
                                    onPress={() => this.props.navigator.pop({name: 'data_collection_screen'})}>
                                    <BackIcon color={theme.headerTextColor} size={35} />
                                </TouchableOpacity>
                            </View>
                            <View style={styles.headerCenter}>
                                <Text style={[styles.headText, {color: theme.headerTextColor}]}>Personal Information</Text>
                            </View>
                            <View style={styles.headerRight}></View>
                        </View>
                    </View>
                    <View style={styles.introTextArea}>
                        <ScrollView style={[{flex: 1}, styles.introTextContaniner]} contentContainerStyle={styles.introTextContaniner}>
                            <Text style={styles.introText}> 
                                Please provide the details required here. We will send your test results to your email.
                                <Br />
                            </Text>
                            <View style={{flex: 1}}>
                                <TextInput 
                                    value={this.state.fullName} 
                                    placeholder="Full Name" changeInput={(value) => this.changeInput('fullName', value)} />
                                <TextInput 
                                    value={this.state.academicLevel} 
                                    placeholder="Academic Level" changeInput={(value) => this.changeInput('academicLevel', value)} />
                                <TextInput 
                                    value={this.state.email} 
                                    placeholder="Email" changeInput={(value) => this.changeInput('email', value)} />
                                <View style={styles.lastInputRow}>
                                    <TextInput value={this.state.age} placeholder="Age" style={{flex: 1, marginRight: 10}} changeInput={(value) => this.changeInput('age', value)}/>
                                    <TextInput value={this.state.country} placeholder="Country" style={{flex: 4, marginLeft: 10}} changeInput={(value) => this.changeInput('country', value)}/>
                                </View>
                                <View style={styles.subscriptionRow}>
                                    <TouchableOpacity onPress={() => this.changeInput('subscriptionChecked', !this.state.subscriptionChecked)}
                                        style={{marginRight: 8}}>
                                        <CheckBox checked={this.state.subscriptionChecked} />
                                    </TouchableOpacity>
                                    <Text style={styles.subscribeText}>Subscribe to our email list</Text>
                                </View>
                                <View style={{marginTop: 20, marginBottom: 20}}>
                                    <Button
                                        buttonStyles={{backgroundColor: 'white'}}
                                        switchScreen={() => this.toggleUserMenu()} children={
                                        <Text style={[{color: '#555', fontSize: 14, fontFamily: 'segoe-ui'}]}>
                                            Already registered? 
                                            <Text style={{fontWeight: '700'}}> Select Your Name</Text>
                                        </Text>
                                    } />
                                </View>
                                <View style={{marginTop: 20, marginBottom: 20}}>
                                    <Button text='View Test Result' switchScreen={() => this.saveData()}/>
                                </View>
                            </View>
                        </ScrollView>
                        <View style={styles.actionBtn}>
                            <Button text='Main Menu' icon switchScreen={() => this.changeScreen('main_menu_screen')}/>
                        </View>
                    </View>
                </View>
                {this.state.userSelectActive && 
                    <UserSelectMenu
                        saveData={this.saveData.bind(this)}
                        users={this.props.store.users}
                        switchScreen={() => this.changeScreen('test_result_screen')}
                        setReturningUser={(data) => this.props.setReturningUser(data)}
                        toggleUserMenu={() => this.toggleUserMenu()}
                      />
                }

                {<Popup ref={popup => this.popup = popup } />}

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        flex: 1
    },
    section: {
        padding: 20
    },
    textContainer: {
        // marginBottom: 10
    },
    topSectionText: {
        fontFamily: 'merriweather-light',
        textAlign: 'center',
        fontSize: 17,
        color: '#626262'
    },
    strong: {
        fontWeight: "500"
    },
    content: {
        flex: 1
    },
    contentRow: {
        flexDirection: 'row',
        paddingLeft: 10,
        paddingRight: 10,
    },
    menuBoxes: {
        justifyContent: 'center',
        flex: 1
    },
    menuBox: {
        height: 200,
    },
    itemIcon: {
        flex: 1,
        justifyContent: 'flex-end',
    },
    itemText: {
        flex: 1,
        color: '#6e6e6e',
        fontFamily: 'raleway-medium',
        fontSize: 20,
        textAlign: 'center',
        marginTop: 15,
        lineHeight: 28
    },
    heading: {
        paddingLeft: 0,
        paddingRight: 0,
        paddingTop: 0,
        paddingBottom: 0,
        // backgroundColor: '#000',
    }, 
    headBox: {
        // borderRadius: 25,
        height: 50,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    headText: {
        textAlign: 'center',
        fontSize: 20,
        fontFamily: 'raleway-semibold'
    },
    headerLeft: {
        width: 50,
        paddingLeft: 20
    },
    headerCenter: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    }, 
    headerRight: {
        width: 50
    },
    introTextContaniner: {
        // marginBottom: 20,
        backgroundColor: theme.softGreenBackground,
        padding: 10
    },
    introTextArea: {
        flex: 1
    },
    buttonLeft: {
        flex: 1,
        marginRight: 15
    },
    buttonRight: {
        flex: 1,
        marginLeft: 15,
    },
    actionBtn: {
        backgroundColor: 'white',
        height: 100,
        justifyContent: 'center',
        // alignItems: 'center',
        padding: 20,
        borderTopColor: '#f1f1f1',
        borderTopWidth: 1,
        // flexDirection: 'row'
    },
    introText: {
        fontFamily: 'merriweather-light',
        fontSize: 17,
        lineHeight: 23,
        color: '#525252',
        textAlign: 'center'
    },
    blendVisual: {
        height: 40,
        flexDirection: 'row',
        marginBottom: 20,
        marginTop: 20
    },
    primaryTemp: {
        flex: 3,
        // height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        borderTopLeftRadius: 3,
        borderBottomLeftRadius: 3,
    },
    secondaryTemp: {
        flex: 2,
        // height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        borderTopRightRadius: 3,
        borderBottomRightRadius: 3,
    },
    tempText: {
        fontFamily: 'raleway-semibold',
        fontSize: 15,
        // fontFamily: 'merriweather-light',
        // fontWeight: "700",
    },
    lastInputRow: {
        flexDirection: 'row',
    },
    subscriptionRow: {
        flexDirection: 'row'
    },
    subscribeText: {
        color: '#777777',
        fontSize: 15,
        fontFamily: 'merriweather-light'
    }

});