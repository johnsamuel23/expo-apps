import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, Platform, TouchableNativeFeedback, Dimensions, TouchableOpacity } from 'react-native';
import { Constants} from 'expo';
import { theme } from './data/theme';
import CloseIcon from './utils/icons/Close';
import StatusBar from './components/StatusBar';

export default class UserSelectScreen extends Component {

  switchScreen(data) {
    setTimeout(() => {
        this.props.toggleUserMenu();
    }, 50);
    this.props.setReturningUser(data);
    this.props.saveData();
    this.props.switchScreen();
  }

  render() {
    let section = this.props.section;
    let closeIcon = (<CloseIcon size={20} color="red" />);
    let users = this.props.users;

    console.log("Select Users:::", users);
    let extraStyles = {};

    let usersList = users.map((item, index) => {
        if(index === 0 || index % 2 === 0) { // event number
            extraStyles.buttonStyles = {
                backgroundColor: "#f4ffe2"
            };
        } else {
            extraStyles.buttonStyles = {
                backgroundColor: "#ffffff"
            };
        }

        if(index < users.length - 1) {
            extraStyles.buttonStyles.borderBottomWidth = 1;
            extraStyles.buttonStyles.borderBottomColor = "#eeeeee";
        }

        if(Platform.OS === "android")
            return (
                <TouchableNativeFeedback 
                    onPress={() => this.switchScreen({
                        firstName: item.firstName,
                        lastName: item.lastName,
                        fullName: item.firstName + " " + item.lastName
                    })}
                    key={index}
                    background={TouchableNativeFeedback.Ripple('rgba(0,0,0,0.2)')}>
                    <View style={[styles.buttonContainer, extraStyles.buttonStyles]}> 
                        <Text style={[styles.buttonText]}>{item.firstName} {item.lastName}</Text>
                    </View>
                </TouchableNativeFeedback>
            )
        else {
            return (
                <TouchableOpacity
                    activeOpacity={0.5}
                    key={index}
                    onPress={() => this.switchScreen({
                        firstName: item.firstName,
                        lastName: item.lastName,
                        fullName: item.firstName + " " + item.lastName
                    })}
                    >
                    <View style={[styles.buttonContainer, extraStyles.buttonStyles]}> 
                        <Text style={[styles.buttonText]}>{item.firstName} {item.lastName}</Text>
                    </View>
                </TouchableOpacity>
            );
        }

    });

    let title = (users.length < 1) ? "There are no users already registered on this device" : "Please Select Your Name";

    return (
        <View style={styles.container}>
            <StatusBar backgroundColor={theme.headerBackgroundColor} />
            <ScrollView>
                <View>
                    <View style={styles.heading}>
                        <View style={styles.headLeft}> 
                            <Text style={styles.headingText}>{title}</Text>
                        </View>
                        <View style={styles.headRight}>
                            <TouchableOpacity onPress={() => this.props.toggleUserMenu()}>
                                <View style={styles.cancelBtn}>
                                    { closeIcon }
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={styles.usersList}> 
                        { usersList }
                    </View>
                </View>
            </ScrollView>
        </View>
    );
  }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        position: 'absolute',
        height: Dimensions.get('window').height,
        width: Dimensions.get('window').width,
        zIndex: 1000
    },
    introTextContaniner: {
        padding: 10,
        marginBottom: 20
    },
    buttonContainer: {
        padding: 20,
        justifyContent: 'center'
    },
    buttonText: {
        fontSize: 22,
        fontFamily: 'segoe-ui',
        color: '#777777'
    },
    heading: {
        flexDirection: 'row',
        alignItems: 'center',
        height: 70,
        paddingLeft: 20,
        // paddingRight: 20
    },
    headLeft: {
        flex: 1
    },
    headingText: {
        fontSize: 20,
        fontFamily: 'segoe-ui'
    },
    headRight: {
        alignItems: 'flex-end',
        width: 40
    },
    cancelBtn: {
        width: 40,
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#f4ffe2',
        borderWidth: 1,
        borderColor: '#d7d7d7',
        borderRightWidth: 0,
        // ...Platform.select({
        //     ios: {
        //       shadowColor: 'rgba(0,0,0, .7)',
        //       shadowOffset: { height:0, width: 0 },
        //       shadowOpacity: 1,
        //       shadowRadius: 3,
        //     },
        //     android: {
        //       elevation: 3
        //     },
        // }),
    },
});

