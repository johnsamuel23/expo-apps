import React, { Component } from 'react';
import { 
    View, 
    Text, 
    StyleSheet, 
    TouchableNativeFeedback,
    TouchableOpacity, 
    Dimensions,
    ScrollView
} from 'react-native';
import { Font, Notifications, Constants } from 'expo';
import BackgroundImage from './components/BackgroundImage';
import LocalImage from './components/LocalImage';
import StatusBar from './components/StatusBar';
import { theme } from './data/theme';
import Button from './components/Button';
import ForwardIcon from './utils/icons/Forward';
import BackIcon from './utils/icons/Back';
import Br from './components/Br';
import { demoResults } from './data/demoResults';


export default class ResultsScreen extends Component {

    componentWillMount() {
    }

    changeScreen(screen) {
        setTimeout(() => {
            this.props.navigator.push({name: screen});
        }, 50);
    }

    getResultScreen() {
        let { results } = this.props.store;
        let resultList = results.map((item, index) => {
            let primaryTemperament = item.primaryTemperament;
            let secondaryTemperament = item.secondaryTemperament;
            let temperamentBlend = item.temperamentBlend;
            let primaryTemperamentRatio = item.primaryTemperamentRatio;
            let secondaryTemperamentRatio = item.secondaryTemperamentRatio;
            let firstName = item.userData.firstName;
            item.userData.fullName = item.userData.firstName + "";
            let time = item.time;
            let date = item.date;

            let returningResult = {
                primaryTemperament: item.primaryTemperament,
                secondaryTemperament: item.secondaryTemperament,
                temperamentBlend: item.temperamentBlend,
                primaryTemperamentRatio: item.primaryTemperamentRatio,
                secondaryTemperamentRatio: item.secondaryTemperamentRatio,
                userData: item.userData
            }

            return (
                <TouchableOpacity onPress={() => {this.props.setReturningResult(returningResult); this.changeScreen('test_result_screen') }} style={styles.itemEntry} key={index}>
                    <View style={styles.userData}>
                        <View style={styles.userDataLeft}>
                            <Text style={styles.usernameText}>{firstName}</Text>
                        </View>
                        <View style={styles.userDataRight}>
                            <Text style={styles.dateText}>{date} <Text style={styles.timeText}>{time}</Text></Text>
                        </View>
                    </View>
                    <View style={styles.blendVisual}>
                        <View style={[styles.primaryTemp, {backgroundColor: theme.temperaments[primaryTemperament].bgColor, flex: primaryTemperamentRatio}]}>
                            <BlendText 
                                style={[styles.tempText, {color: theme.temperaments[primaryTemperament].textColor}]} 
                                text={primaryTemperament}
                                />
                        </View>
                        <View style={[styles.secondaryTemp, {backgroundColor: theme.temperaments[secondaryTemperament].bgColor, flex: secondaryTemperamentRatio}]}>
                            <BlendText 
                                style={[styles.tempText, {color: theme.temperaments[secondaryTemperament].textColor}]} 
                                text={secondaryTemperament}
                                />
                        </View>
                    </View>
                </TouchableOpacity>
            )
        });

        return resultList;

    }

    render() {

        let resultList = this.getResultScreen();

        return (
            <View style={styles.container}>
                <StatusBar backgroundColor={theme.headerBackgroundColor} />
                <View style={styles.content}>
                    <View style={styles.heading}>
                        <View style={[styles.headBox, {backgroundColor: theme.headerBackgroundColor}]}>
                            <View style={styles.headerLeft}>
                                <TouchableOpacity style={styles.backIcon} 
                                    onPress={() => this.props.navigator.pop({name: 'results_screen'})}>
                                    <BackIcon color={theme.headerTextColor} size={35} />
                                </TouchableOpacity>
                            </View>
                            <View style={styles.headerCenter}>
                                <Text style={[styles.headText, {color: theme.headerTextColor}]}>All Results</Text>
                            </View>
                            <View style={styles.headerRight}></View>
                        </View>
                    </View>
                    <View style={styles.introTextArea}>
                        <ScrollView style={[{flex: 1}, styles.introTextContaniner]} contentContainerStyle={styles.introTextContaniner}>
                            { resultList }
                        </ScrollView>
                        <View style={styles.actionBtn}>
                            <Text style={styles.helpText}>Tap result for more details</Text>
                        </View>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        flex: 1
    },
    userData: {
        flexDirection: 'row',
        flex: 1,
        marginBottom: 2
    },
    usernameText: {
        textAlign: 'left',
        color: '#555',
        fontFamily: 'segoe-ui',
        fontSize: 13
    },
    userDataLeft: {
        flex: 3,
    },
    userDataRight: {
        flex: 6,
        alignItems: 'flex-end'
    },
    dateText: {
        textAlign: 'right',
        color: '#555',
        fontFamily: 'segoe-ui',
        fontSize: 13,
        alignSelf: 'flex-end'        
    },
    timeText: {
        marginLeft: 10
    },
    section: {
        padding: 20
    },
    textContainer: {
        // marginBottom: 10
    },
    topSectionText: {
        fontFamily: 'merriweather-light',
        textAlign: 'center',
        fontSize: 17,
        color: '#626262'
    },
    strong: {
        fontWeight: "500"
    },
    content: {
        flex: 1
    },
    contentRow: {
        flexDirection: 'row',
        paddingLeft: 10,
        paddingRight: 10,
    },
    menuBoxes: {
        justifyContent: 'center',
        flex: 1
    },
    menuBox: {
        height: 200,
    },
    itemIcon: {
        flex: 1,
        justifyContent: 'flex-end',
    },
    itemText: {
        flex: 1,
        color: '#6e6e6e',
        fontFamily: 'raleway-medium',
        fontSize: 20,
        textAlign: 'center',
        marginTop: 15,
        lineHeight: 28
    },
    heading: {
        paddingLeft: 0,
        paddingRight: 0,
        paddingTop: 0,
        paddingBottom: 0,
        // backgroundColor: '#000',
    }, 
    headBox: {
        // borderRadius: 25,
        height: 50,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    headText: {
        textAlign: 'center',
        fontSize: 20,
        fontFamily: 'raleway-semibold'
    },
    headerLeft: {
        width: 50,
        paddingLeft: 20
    },
    headerCenter: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    }, 
    headerRight: {
        width: 50
    },
    introTextContaniner: {
        // marginBottom: 20
        padding: 10
    },
    introTextArea: {
        flex: 1
    },
    buttonLeft: {
        flex: 1,
        marginRight: 15
    },
    buttonRight: {
        flex: 1,
        marginLeft: 15,
    },
    actionBtn: {
        backgroundColor: 'white',
        height: 30,
        justifyContent: 'center',
        // alignItems: 'center',
        padding: 20,
        borderTopColor: '#f1f1f1',
        borderTopWidth: 1,
        // flexDirection: 'row'
    },
    helpText: {
        color: '#888',
        fontFamily: 'segoe-ui',
        textAlign: 'center'
    },
    introText: {
        fontFamily: 'merriweather-light',
        fontSize: 17,
        lineHeight: 23,
        color: '#525252',
        textAlign: 'justify'
    },
    blendVisual: {
        height: 40,
        flexDirection: 'row',
        marginBottom: 20,
    },
    primaryTemp: {
        flex: 3,
        // height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        borderTopLeftRadius: 3,
        borderBottomLeftRadius: 3,
    },
    secondaryTemp: {
        flex: 2,
        // height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        borderTopRightRadius: 3,
        borderBottomRightRadius: 3,
    },
    tempText: {
        fontFamily: 'raleway-semibold',
        fontSize: 15,
        // fontFamily: 'merriweather-light',
        // fontWeight: "700",
    },

});