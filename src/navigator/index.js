import React from 'react';
import { StyleSheet, Text, View, ScrollView } from 'react-native';
import {
  Asset,
  AppLoading,
} from 'expo';
import App from './App';

export default class Root extends React.Component {
  state = {
    isReady: false,
  };

  render() {

    return (
      <View style={[styles.container]}>
        <App />  
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#555',
  },
});
