import React, { Component } from 'react';
import { 
    View, 
    Text, 
    StyleSheet, 
    TouchableOpacity, 
    Dimensions,
    ScrollView
} from 'react-native';
import { Font, Notifications, Constants } from 'expo';
import BackgroundImage from './components/BackgroundImage';
import LocalImage from './components/LocalImage';
import StatusBar from './components/StatusBar';
import { theme } from './data/theme';
import Button from './components/Button';
import ForwardIcon from './utils/icons/Forward';
import BackIcon from './utils/icons/Back';
import CloseIcon from './utils/icons/Close';
import HelpIcon from './utils/icons/Help';
import Br from './components/Br';
import { Navigator } from 'react-native-deprecated-custom-components';
import QuestionsScreen from './QuestionsScreen';
import Popup from 'react-native-popup';

export default class TestMainScreen extends Component {

    componentDidMount() {
        const { navigator } = this.props;
    }

    state = {
        section: 1,
        initialSection: 'section_1',
        guideActive: false,
        guardActive: false,
    }

    changeScreen(screen) {
        setTimeout(() => {
            this.props.navigator.push({name: screen});
        }, 50);
    }

    selectOption(sectionNumber, questionSerial, optionNumber) {
        let data = {sectionNumber, questionSerial, optionNumber};
        this.props.saveOption(data);
    }

    getSectionTitle() {
        let sectionTitle = "";
        switch(this.state.section) {
            case 1:
                sectionTitle = "One";
                break;
            case 2:
                sectionTitle = "Two";
                break;
            case 3:
                sectionTitle = "Three";
                break;
            case 4:
                sectionTitle = "Four";
                break;
        }
        return sectionTitle;
    }

    changeSection(section) {
        let section_name = "section_" + section;
        let { answers } = this.props.store;

        
        if(section > 4) {
            if(answers.length === this.props.store.noOfQuestions) {
                this.changeScreen('data_collection_screen');
            } else {
                this.toggleGuard();
            }
            // console.log("No of Answers", answers.length);
        } else {
            this.setState({
                section
            });
        } 
    }

    renderScene(route, navigator) {
        this.navigator = navigator;
        switch(route.name) {
            case 'section_1':
                currentScreen = <QuestionsScreen store={this.props.store}
                                    section={this.state.section} 
                                    changeSection={this.changeSection.bind(this)}
                                    changeScreen={this.changeScreen.bind(this)}
                                />
                break;
            case 'section_2':
                currentScreen = <QuestionsScreen store={this.props.store}
                                    section={this.state.section} 
                                    changeSection={this.changeSection.bind(this)}
                                    changeScreen={this.changeScreen.bind(this)}
                                />
                break;
            case 'section_3':
                currentScreen = <QuestionsScreen store={this.props.store} 
                                    section={this.state.section} 
                                    changeSection={this.changeSection.bind(this)}
                                    changeScreen={this.changeScreen.bind(this)}
                                />
                break;
            case 'section_4':
                currentScreen = <QuestionsScreen store={this.state}
                                    section={this.state.section} 
                                    changeSection={this.changeSection.bind(this)}
                                    changeScreen={this.changeScreen.bind(this)}
                                />
                break;
            default: 
                currentScreen = <QuestionsScreen store={this.state}
                                    section={this.state.section} 
                                    changeSection={this.changeSection.bind(this)}
                                    changeScreen={this.changeScreen.bind(this)}
                                />
        }
        return currentScreen;   
    }

    toggleGuide() {
        this.setState({
            guideActive: ! this.state.guideActive
        })
    }

    toggleGuard() {
        this.setState({
            guardActive: ! this.state.guardActive
        })
    }

    goBack() {
        let { section } = this.state;
        console.log("SECTION", section);
        let nextSection = section - 1;
        if(section > 1) { 
            this.setState({
                section: nextSection
            });
        } else {
            this.props.navigator.pop({name: 'test_main_screen'});
        }
    }
    
    render() {
        let sectionTitle = this.getSectionTitle();

        return (
            <View style={styles.container}>
                <StatusBar backgroundColor={theme.headerBackgroundColor} />
                <View style={styles.content}>
                    <View style={styles.heading}>
                        <View style={[styles.headBox, {backgroundColor: theme.headerBackgroundColor}]}>
                            <View style={styles.headerLeft}>
                                <TouchableOpacity style={styles.backIcon} 
                                    onPress={() => this.goBack()}>
                                    <BackIcon color={theme.headerTextColor} size={35} />
                                </TouchableOpacity>
                            </View>
                            <View style={styles.headerCenter}>
                                <Text style={[styles.headText, {color: theme.headerTextColor}]}>Section {sectionTitle}</Text>
                            </View>
                            <View style={styles.headerRight}></View>
                        </View>
                    </View>
                    <View style={styles.introTextArea}>
                        {/* <Navigator
                            initialRoute={{name: this.state.initialSection}}
                            renderScene={this.renderScene.bind(this)}
                            configureScene={(route, routeStack) => Navigator.SceneConfigs.FloatFromLeft}
                        /> */}
                        <QuestionsScreen store={this.props.store}
                                    key={this.state.section}
                                    section={this.state.section} 
                                    changeSection={this.changeSection.bind(this)}
                                    selectOption={(SN, QSN, OPTN) => this.selectOption(SN, QSN, OPTN)} 
                                    changeScreen={this.changeScreen.bind(this)}
                                />
                                
                        <TouchableOpacity onPress={() => this.toggleGuide()}>
                            <View style={styles.actionBtn}>
                                <Text style={styles.helpText}> Quick Guide </Text><HelpIcon color="#5d9c31" size={25} />
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
                {this.state.guideActive &&  
                <View style={styles.modal}>
                    <View style={styles.modalContent}>
                        <View style={styles.modalAction}>
                            <TouchableOpacity onPress={() => this.toggleGuide()}
                                style={styles.modalActionButton}>
                                <Text style={styles.closeText}>close</Text>
                                <CloseIcon size={20} color="red" />
                            </TouchableOpacity>
                        </View>
                        <Text style={styles.modalContentText}>
                            Indicate the extent to which each character applies to you.
                            <Br />
                            This can be:
                            <Br single={true} />
                            1. Inaccurate
                            <Br single={true} />
                            2. Moderately inaccurate 
                            <Br single={true} />
                            3. Neither inaccurate nor accurate
                            <Br single={true} />
                            4. Moderately accurate
                            <Br single={true} />
                            5. Very accurate
                            <Br />
                        </Text>
                    </View>
                </View>
                }

                {this.state.guardActive &&  
                <View style={styles.modal}>
                    <View style={styles.modalContent}>
                        <View style={styles.modalAction}>
                            <TouchableOpacity onPress={() => this.toggleGuard()}
                                style={styles.modalActionButton}>
                                <Text style={styles.closeText}>close</Text>
                                <CloseIcon size={20} color="red" />
                            </TouchableOpacity>
                        </View>
                        <Text style={[styles.modalContentText, {textAlign: 'center'}]}>
                            Please select an option for all questions
                            <Br />
                            Check all sections
                        </Text>
                    </View>
                </View>
                }

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fafff1',
        // backgroundColor: '#fff',
        flex: 1,
    },
    modalAction: {
        marginBottom: 20,
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    modalActionButton: {
        flexDirection: 'row',
        height: 20,
        justifyContent: 'center',
        alignItems: 'center'
    },
    closeText: {
        marginRight: 3,
        fontSize: 17,
        color: '#777',
        fontFamily: 'segoe-ui'
    },
    modal: {
        position: 'absolute',
        height: Dimensions.get('window').height,
        width: Dimensions.get('window').width,
        backgroundColor: 'rgba(179,209,164, 0.7)',
        zIndex: 2,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 20,
        bottom: 45
    },
    modalContent: {
        backgroundColor: '#fff',
        padding: 20,
        // maxHeight: 300
    },
    modalContentText: {
        fontFamily: 'segoe-ui',
        fontSize: 18,
        color: '#333'
    },
    content: {
        flex: 1,
        zIndex: 1
    },
    contentRow: {
        flexDirection: 'row',
        paddingLeft: 10,
        paddingRight: 10,
    },
    menuBoxes: {
        justifyContent: 'center',
        flex: 1
    },
    menuBox: {
        height: 200,
    },
    itemIcon: {
        flex: 1,
        justifyContent: 'flex-end',
    },
    itemText: {
        flex: 1,
        color: '#6e6e6e',
        fontFamily: 'raleway-medium',
        fontSize: 20,
        textAlign: 'center',
        marginTop: 15,
        lineHeight: 28
    },
    heading: {
        paddingLeft: 0,
        paddingRight: 0,
        paddingTop: 0,
        paddingBottom: 0,
        // backgroundColor: '#000',
    }, 
    headBox: {
        // borderRadius: 25,
        height: 50,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    headText: {
        textAlign: 'center',
        fontSize: 20,
        fontFamily: 'raleway-semibold'
    },
    headerLeft: {
        width: 50,
        paddingLeft: 20
    },
    headerCenter: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    }, 
    headerRight: {
        width: 50
    },
    introTextContaniner: {
        // backgroundColor: 'red',
        padding: 10,
        marginBottom: 20
    },
    introTextArea: {
        flex: 1
    },
    actionBtn: {
        backgroundColor: 'white',
        height: 45,
        justifyContent: 'center',
        alignItems: 'center',
        // padding: 20,
        borderTopColor: '#f1f1f1',
        borderTopWidth: 1,
        flexDirection: 'row'
    },
    introText: {
        fontFamily: 'merriweather-light',
        fontSize: 17,
        lineHeight: 23,
        color: '#525252',
        textAlign: 'justify'
    },
    helpText: {
        fontSize: 18,
        fontFamily: 'raleway-semibold',
        color: '#525252'
    }

});