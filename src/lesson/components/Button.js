import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import styles from './styles';
import UtilityNavButton from './UtilityNavButton';

export default class Button extends Component {
    
    render() {
        let buttonStyles = {};
        let textStyles = {};
        this.props.red ? buttonStyles.backgroundColor = "red" : null;
        this.props.red ? textStyles.color = this.props.color = "#fff" : textStyles.color = '#555';;
        this.props.bold ? textStyles.fontWeight = "bold" : null;

        return (
            <View style={[styles.PinButton, buttonStyles]}>
                {this.props.icon && <UtilityNavButton icon="Pin" color='white' /> }
                <Text style={[styles.PinButtonText, textStyles]}> {this.props.text} </Text>
            </View>
        )
    }
}
