import React, { Component } from 'react';
import { 
    StyleSheet, 
    Text, 
    View, 
    Dimensions,
    Image
} from 'react-native';
import UtilityNavButton from './UtilityNavButton';
import styles from './styles';
import Button from './Button';
import LocalImage from './LocalImage';

let windowWidth = Dimensions.get('window').width;

export default class PracticalFlexLayout extends Component {
    render() {

        const { user } = this.props;

        return (
            <View style={[styles.PinContainer, {width: windowWidth/this.props.columns}]}>
                <View style={styles.RestContent}>
                    <View style={styles.PinHeader}>
                        <View style={styles.UtilityNav}>
                            <View style={styles.UtilityNavContent}>
                                <UtilityNavButton icon='Back' />
                                <UtilityNavButton icon='Heart' />
                                <UtilityNavButton icon='Share' />
                                <UtilityNavButton icon='More' />
                            </View>
                        </View>
                        <View style={[styles.PinButtonContainer, styles.HeaderRight]}>
                            <View style={styles.PinButtonContent}>
                                <Button red icon text="Save" />
                            </View>
                        </View>
                    </View>

                    <View style={styles.PinContent}>
                        <LocalImage 
                            source={user.imagesource}
                            originalWidth={user.originalWidth}
                            originalHeight={user.originalHeight}
                            columns={this.props.columns}
                        />
                        
                    </View>

                    <View style={styles.PinMeta}>
                        <View style={styles.PinMetaTextContainer}>
                            <Text style={styles.PinMetaText}>Saved from</Text>
                            <Text style={[styles.PinMetaText, styles.TextBold]}>website.com</Text>
                        </View>
                        <View style={[styles.PinButtonContainer, styles.MetaRight]}>
                            <Button bold text="Visit" />
                        </View>
                    </View>

                    <View style={styles.PinUser}>
                        <View style={styles.PinUserAvatar}>
                            <Image 
                                source={user.imagesource}
                                style={{
                                    width: 50,
                                    height: 50,
                                    borderRadius: 25
                                }}
                            />
                        </View>
                        <View style={styles.PinUserContainer}>
                            <Text style={styles.PinUserText}> 
                                <Text style={[styles.TextBold]}>User Name</Text> 
                                <Text> saved to</Text> 
                                <Text style={styles.TextBold}> Space </Text>
                            </Text>
                            <Text style={styles.PinUserText}>
                                Description Lorem ipsum dolor sit amet, 
                            </Text>
                        </View>
                    </View>
                </View>


            </View>
        )
    }
}
