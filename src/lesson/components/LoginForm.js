import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, TextInput, TouchableOpacity, StatusBar } from 'react-native';

export default class LoginForm extends Component {
  render() {
    return (
        <View style={styles.container}>
            <StatusBar barStyle="light-content" />
            <TextInput 
              style={styles.input} 
              placeholder="username or email" 
              placeholderTextColor="rgba(255,255,255,0.7)"
              returnKeyType="next"
              keyboardType="email-address"
              autoCapitalise="none"
              autoCorrect={false}
              underlineColorAndroid="transparent"
              />

            <TextInput 
              style={styles.input} 
              placeholder="password" 
              secureTextEntry
              placeholderTextColor="rgba(255,255,255,0.7)"
              returnKeyType="go"
              underlineColorAndroid="transparent"
              />

            <TouchableOpacity style={styles.buttonContainer}>
              <Text style={styles.buttonText}> LOGIN </Text>
            </TouchableOpacity>
        </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10
  },
  input: {
      paddingVertical: 10,
      paddingHorizontal: 10,
      backgroundColor: '#9ccff1',
      flexDirection: 'row',
      marginBottom: 10,
      color: "#fff",
      fontSize: 16
  },
  buttonContainer: {
    paddingVertical: 10,
    backgroundColor: '#2980b9',
  },
  buttonText: {
    textAlign: 'center',
    color: '#fff'
  }

});