import React, { Component } from 'react';
import { 
    StyleSheet, 
    Text, 
    View 
} from 'react-native';
import Pin from './PracticalFlexLayout';

export default class App extends Component {
    componentWillMount() {
        this.state = {
            columns: 2,
            pins: [
                {
                    imagesource: require('../../images/handsome-young-college-boy-holding-books-on-campus.jpg'),
                    originalWidth: 640,
                    originalHeight: 428
                },
                {
                    imagesource: require('../../images/fine_undergrad.jpg'),
                    originalWidth: 448,
                    originalHeight: 534
                }, 
                {
                    imagesource: require('../../images/AAANBlack-College-Students-2014BW2.jpg'),
                    originalWidth: 1500,
                    originalHeight: 1000
                },
                {
                    imagesource: require('../../images/fine_undergrad.jpg'),
                    originalWidth: 448,
                    originalHeight: 534
                }, 
                {
                    imagesource: require('../../images/college-1024x688.jpg'),
                    originalWidth: 1024,
                    originalHeight: 638
                },
                {
                    imagesource: require('../../images/AAANBlack-College-Students-2014BW2.jpg'),
                    originalWidth: 1500,
                    originalHeight: 1000
                }   
            ]
        }
    }

    UseMasonry(dynamicStyles) {
        let leftColumn = [];
        let rightColumn = [];

        for(var i=0; i < this.state.pins.length; i++) {
            if(i % 2 === 0) {
                leftColumn.push(this.state.pins[i]);
            } else {
                rightColumn.push(this.state.pins[i]);
            }
        }

        let leftColumnView = leftColumn.map((pin, index) => {
            return <Pin key={index} user={pin} columns={this.state.columns}/>
        });

        let rightColumnView = rightColumn.map((pin, index) => {
            return <Pin key={index} user={pin} columns={this.state.columns}/>
        });

        return (
            <View style={[styles.container, dynamicStyles.container]}>
                <View style={styles.PinContainer}>
                    {leftColumnView}                                    
                </View>
                <View style={styles.PinContainer}>
                    {rightColumnView}
                </View>
            </View>
        )
    }
    
    render() {
        let dynamicStyles = {
            container: {},
            PinContainer: {}
        };

        this.state.columns === 1 ? dynamicStyles.container.flexDirection = 'column' : 'row'; 
        
        switch(this.state.columns) {
            case this.state.column > 1:
                return this.UseMasonry(dynamicStyles);
            default: 
                return this.UseMasonry(dynamicStyles); // USE NON-MASONRY LAYOUT

        }
    }
}

let styles = StyleSheet.create({
    container: {
        flexDirection: 'row' // active for masonry
    },
    PinContainer: {
        // flex: 1,
        // flexDirection: 'column',
        alignSelf: 'flex-start',
        alignItems: 'flex-start'
    }
});