import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, TextInput, TouchableOpacity, KeyboardAvoidingView } from 'react-native';
import LoginForm from './LoginForm';
import { Pin, Back, Heart, More, Share } from '../../utils/icons';

class Login2 extends Component {

    render() {
        return (
            <KeyboardAvoidingView behavior='padding' style={styles.container}>
                <TouchableOpacity style={styles.logoContainer}>
                     { Pin }
                    { Back }
                    { Heart }
                    { More }
                    { Share }
                    <Text style={styles.title}>Login | Enjoy</Text>
                </TouchableOpacity>
                <View style={styles.formContainer}>
                    <View style={styles.row}>
                        <LoginForm />
                    </View>
                    <View style={styles.row}>
                        <LoginForm />
                    </View>
                    <View style={styles.row}>
                        <LoginForm />
                    </View> 

                    {/*<View style={styles.formSubContainer}>
                        <TextInput 
                            style={styles.input} 
                            placeholderTextColor="rgba(255,255,255, 1)" 
                            underlineColorAndroid="transparent" 
                            placeholder="email" 
                            />
                        <TextInput 
                            style={styles.input} 
                            placeholderTextColor="rgba(255,255,255, 1)" 
                            underlineColorAndroid="transparent" 
                            placeholder="password" 
                            />
                        <TouchableOpacity style={styles.buttonContainer}>
                            <Text style={styles.buttonText}> LOGIN </Text>
                        </TouchableOpacity>
                    </View>*/}
                </View>
            </KeyboardAvoidingView>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#00BCD4'
    },
    logo: {
        width: 100,
        height: 100,
    },
    title: {
        alignItems: 'center',
        color: 'white',
        fontSize: 20
    },
    row: {
        flexDirection: 'row',
        flexGrow: 1
    },
    logoContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    buttonContainer: {
        backgroundColor: '#2980b9',
        padding: 10
    },
    buttonText: {
        textAlign: 'center',
        color: '#fff'
    },
    formSubContainer: {
        padding: 10,
        flex: 1
    },    
    formContainer: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'flex-end',
    },
    input: {
        backgroundColor: '#9ccff1',
        color: 'white',
        fontSize: 15,
        marginBottom: 10,
        padding: 10,
        flexDirection: 'row'
    }

});

export default Login2;