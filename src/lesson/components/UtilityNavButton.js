import React, { Component } from 'react';
import Pin from '../../utils/icons/Pin';
import Heart from '../../utils/icons/Heart';
import More from '../../utils/icons/More';
import Back from '../../utils/icons/Back';
import Share from '../../utils/icons/Share';

export default class UtilityNavButton extends Component {
    render() {
        switch(this.props.icon) {
            case "Back":
                return <Back />
            case "Pin":
                return <Pin color={this.props.color} />
            case "Heart":
                return <Heart />
            case "More":
                return <More />
            case "Share":
                return <Share />
            default: 
                return <Share />;

        }
    }
}