import { StyleSheet } from 'react-native';


const styles = StyleSheet.create({
    PinContainer: {
        // flex: 1,
        backgroundColor: '#fff',
        // alignSelf: 'stretch',
        paddingTop: 25,
    },
    RestContent: {
        // flex: 1,
        // backgroundColor: 'white'
    }, 
    PinHeader: {
        flexDirection: 'row',
        backgroundColor: '#fff',
        minHeight: 50,
        alignSelf: 'stretch',
        justifyContent: 'space-between'
    },
    UtilityNav: {
        flex: 3,
        // backgroundColor: 'red'
    },
    UtilityNavContent: {
        flexDirection: 'row',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-around'
    },
    HeaderRight: {
        flex: 2,
        flexDirection: 'column',
        // backgroundColor: '#555',
        alignItems: 'flex-end',
        paddingRight: 10,
        justifyContent: 'center',
    },
    PinButtonContent: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around'
    },
    PinButton: {
        backgroundColor: '#cecece',
        flexDirection: 'row',
        padding: 5,
        borderRadius: 4
    },
    PinButtonText: {
        color: '#fff'
    },

    PinContent: {
        // flex: 5,
        alignItems: 'center',
        justifyContent: 'center',
        paddingLeft: 8,
        paddingRight: 8
    },
    ImagePlaceholder: {
        backgroundColor: '#1e1e1e',
        // flex: 1,
        alignSelf: 'stretch'
    },  

    PinMeta: {
        // flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingRight: 8,
        paddingLeft: 8,
        paddingTop: 8,
        paddingBottom: 8,
        minHeight: 60,
        // backgroundColor: 'red'
    },
    UtilityButton: {
        backgroundColor: '#cecece',
    },
    UtilityButtonText: {
        color: '#000',
        fontWeight: 'bold'
    },
    PinUser: {
        // flex: 7,
        flexDirection: 'row',
        paddingLeft: 8,
        paddingRight: 8
    },
    PinUserAvatar: {
        width: 50,
        height: 50,
        backgroundColor: 'black',
        borderRadius: 25,
        marginRight: 8
    }, 

    TextBold: {
        fontWeight: 'bold'
    }
});

export default styles;