import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, KeyboardAvoidingView } from 'react-native';
import LoginForm from './LoginForm';

export default class Login extends Component {
  render() {
    return (
        <KeyboardAvoidingView behavior="padding" style={styles.container}>
            <View style={styles.logoContainer}>
                <Image 
                    style={styles.logo} 
                    source={require('../../images/unnamed.png')}
                    />

                <Text style={styles.text}>My first Demo App</Text>
            </View>
            <View style={styles.formContainer}>
                <LoginForm />
            </View>
        </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#3498db',
  },
  logoContainer: {
      alignItems: 'center',
      flexGrow: 1,
      justifyContent: 'center'
  },
  formContainer: {
      alignItems: 'flex-end',
      flex: 1,
      flexDirection: 'row',
  },
  logo: {
      width: 100,
      height: 100
  },
  text: {
      color: '#fff',
      opacity: 0.9
  }
});