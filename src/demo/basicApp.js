/*
  This is where I pull in my required packages
*/
import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import newStyles from './styles';
import Form from './Form';

export default class App extends React.Component {

    state = {
        formEnabled: true
    }

    toggleForm() {
        this.setState({
            formEnabled: !this.state.formEnabled
        })
    }

    render() {
        return (
            <View style={[styles.container, {backgroundColor: newStyles.backgroundColor}]}>
                <Text style={styles.text}>Welcome to </Text> 
                <Text style={styles.text}>Grandrey Training Center</Text>

                <TouchableOpacity onPress={() => this.toggleForm()}>
                    <View style={styles.button}>
                        <Text style={styles.buttonText}>Sign in</Text>
                    </View>
                </TouchableOpacity>

                {this.state.formEnabled && <Form />}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#b52a2a', // rgb
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    text: {
        fontSize: 25,
        color: 'white',
        fontWeight: 'bold',
        textAlign: 'center'
    },
    button: {
        backgroundColor: '#b52a2a',
        padding: 20,
        margin: 20,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 30,
    },
    buttonText: {
        color: 'white',
        fontSize: 20,
        textAlign: 'center'
    }
});