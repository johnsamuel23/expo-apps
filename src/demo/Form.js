import React from  'react';
import { TextInput, View, StyleSheet, Dimensions } from 'react-native';

export default class Form extends React.Component {
    state = {
        text: ""
    }

    changeInput(input) {
        // console.log("input", input);
        this.setState({
            text: input
        })
    }

    render() {
        return (
            <View style={styles.container}>
                <TextInput 
                    value={this.state.text} 
                    style={styles.input} 
                    placeholder= 'Amount (Naira):'
                    placeholderTextColor= "black"
                    underlineColorAndroid={"transparent"} 
                    onChangeText={(input) => this.changeInput(input)} 
                    />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        // backgroundColor: '#cccccc',
        padding: 20,
        margin: 20,
        alignItems: 'center',
        width: Dimensions.get('window').width
    },
    input: {
        width: Dimensions.get('window').width,
        height: 50,
        paddingLeft: 10,
        backgroundColor: "#fff",
        borderBottomColor: "#fff",
        borderBottomWidth: 5
    }
});