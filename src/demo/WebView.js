import React from 'react';
import { StyleSheet, Text, View, Image, ScrollView, Dimensions, WebView } from 'react-native';

export default class WebViewApp extends React.Component {
  
  state = {
    scalesPageToFit: true
  }
  render() {
    var DEFAULT_URL = 'http://clamgo.org/about-us';

    return (
      <ScrollView contentContainerStyle={styles.container}>
        {/* <View style={styles.header}>
          <Text> Bible </Text>
        </View> */}
        <View style={styles.WebViewEmbed}>
            <WebView
              automaticallyAdjustContentInsets={false}
              source={{uri: DEFAULT_URL}}
              javaScriptEnabled={true}
              domStorageEnabled={true}
              decelerationRate="normal"
              startInLoadingState={true}
              scalesPageToFit={this.state.scalesPageToFit}
            />
        </View>

      </ScrollView>  
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#16794a',
  },
  header: {
    backgroundColor: 'black',
    height: 70
  },
  WebViewEmbed: {
    backgroundColor: "gray",
    flex: 1
  }
});
