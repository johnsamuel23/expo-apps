import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Dimensions } from 'react-native';
// import WebBrowser from './WebBrowser';
import { Font, Notifications, Constants } from 'expo';
import LocalImage from './components/LocalImage';
import Touch from './components/Touch';

export default class SplashScreen extends Component {

    componentDidMount() {
        const { navigator } = this.props;

        setTimeout(function(){
            //navigator.push({name: 'months_menu'});
        }, 3000);
    }

    switchScreen(screen) {
        setTimeout(() => {
            this.props.navigator.push({name: screen});
        }, 50);
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={{flex: 1, zIndex: 1}}>
                    <View style={styles.statusBar}></View>
                    <View style={styles.message}>
                        <View style={styles.messageText}>
                            <Text style={[styles.number, styles.closeBlocks]}>365</Text><Text style={[styles.dayText, styles.closeBlocks, {paddingBottom: 10}]}>-Day</Text>
                        </View>
                        <View style={{marginTop: -10}}>
                            <Text style={styles.appName}>Prayer Capsules</Text>
                        </View>
                    </View>
                    <View style={styles.actionView}>
                        <Touch 
                            action={() => this.switchScreen('months_menu')}>
                            <View style={styles.continueBtn}>
                                <Text style={styles.actionText}> Explore </Text>
                            </View>
                        </Touch>
                    </View>
                </View>
                <View style={styles.waterMark}>
                    <LocalImage 
                        source={require('./img/clamLogo_512x512.png')}
                        originalWidth={512}
                        originalHeight={512}
                        guideWidth={400}
                    />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff'
    },
    waterMark: {
        position: 'absolute',
        opacity: 0.1,
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        justifyContent: 'center',
        alignItems: 'center',
        zIndex: 0
    },
    statusBar: {
        backgroundColor: '#4090ab',
        height: Constants.statusBarHeight,
    },
    text: {
        fontSize: 30,
        color: '#555',
        textAlign: 'center',
        fontFamily: 'lato-heavy'
    },
    message: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    continueBtn: {
        height: 70,        
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#4090ab',
    },
    closeBlocks: {
        height: 100,
        textAlignVertical: 'bottom'
    },
    actionText: {
        fontSize: 20,
        color: '#fff'
    },
    number: {
        fontSize: 100,
        color: 'red',
        fontFamily: 'BebasNeueBold',
        // marginRight: 10
    },
    dayText: {
        fontSize: 42,
        color: 'black',
        fontFamily: 'BebasNeueBold'
    },
    messageText: {
        flexDirection: 'row'
    },
    appName: {
        fontSize: 34,
        textAlign: 'center',
        fontFamily: 'BebasNeueBold',
        color: '#555'
    }

});