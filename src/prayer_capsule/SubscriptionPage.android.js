import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, WebView, TouchableOpacity, Platform, Dimensions } from 'react-native';
import BackIcon from './utils/icons/Back';
import SearchIcon from './utils/icons/Menu';
import { Constants } from 'expo';
import theme, { capsuleTheme } from './data/theme';
import TextInput from './components/TextInput';
import Button from './components/Button';
import SendIcon from './utils/icons/Send';
import PayIcon from './utils/icons/Pay';
import SubscribeIcon from './utils/icons/Subscribe';
import { activateSubscription } from './utils/api';
import * as Animatable from 'react-native-animatable';

export default class SubscriptionPage extends Component {

  async componentWillMount() {
    // let { isConnected } = this.props.store;

    this.setState({
      subscribed: false,
      subscriptionCode: '',
      email: ''
    });
  }

  async openWebPay() {
    this.setState({
      payOnlineActive: true, 
      payBoxHeight: Dimensions.get('window').height
    });
  }

  closeWebPay() {
    this.setState({
      payOnlineActive: false, 
      payBoxHeight: 0,
    });
  }

  openRegisterView() {
    this.setState({
      registerViewActive: true
    });

    this.refs.registerForm.transitionTo({height: 100});
  }

  closeRegisterView() {
    this.setState({
      registerViewActive: true
    });

    this.refs.registerForm.transitionTo({height: 0});
  }

  componentWillUnmount() {    
  }

  navigationChange(navigationState) {
    console.log("Navigation State Change ", navigationState);
    let { navigator, saveStateData } = this.props;
    let str = navigationState.url;
    let pos = str.indexOf('http://clamgo.org');
    
    if ( pos !== -1 ) {
        console.log('Found at location ' + pos);
        // This means the success redirect url was found.
        // So the user has successfully made their payment.
        // navigate them to a congratulations on successful subscription page
        saveStateData("subscriptionActive", true);
        this.setState({
          subscribed: true
        });
        
        this.props.activateApp("somedata"); // can pass the transaction Id for instance
        navigator.pop({name: 'subscription_screen'});
        navigator.push({name: 'subscription_success'});
    }
  }

  changeInput(field, value) {
    this.setState({
      [field]: value
    });
  }

  async activateApp() {
    let data = {
      email: this.state.email,
      subscriptionCode: this.state.subscriptionCode
    }
    
    activateSubscription(data).then(response => {
      console.log(response.data);
      if(response.data.message === 'code_used') {
        let message = "The Subscription Code you provided has been activated by another user. Please register or contact support";
        this.setState({messageBoxMarginBottom: 20, errorMessage: message, errorMessageHeight: 70, errorMessageTextSize: 15, messageBackgroundColor: 'rgb(184, 36, 36)'});
      } else if (response.data.message === 'not_found') {
        let message = "The Subscription Code you provided is invalid. Try again or contact support for help";
        this.setState({messageBoxMarginBottom: 20, errorMessage: message, errorMessageHeight: 70, errorMessageTextSize: 15, messageBackgroundColor: 'rgb(184, 36, 36)'});
      } else if (response.data.message === 'empty') {
        let message = "Please provide EMAIL and SUBSCRIPTION CODE";
        this.setState({messageBoxMarginBottom: 20, errorMessage: message, errorMessageHeight: 70, errorMessageTextSize: 15, messageBackgroundColor: 'rgb(184, 36, 36)'});
      } else if (response.data.message === 'validated') {
        let message = "You have successfully completed your app subscription";
        this.setState({messageBoxMarginBottom: 20, errorMessage: message, errorMessageHeight: 70, errorMessageTextSize: 15, messageBackgroundColor: 'green'});
      }

      setTimeout(() => {
        this.setState({messageBoxMarginBottom: 0, errorMessageHeight: 0, errorMessageTextSize: 0,});

        if(response.data.message === 'validated') {
          setTimeout(() => {
            let { navigator, saveStateData } = this.props;          
            saveStateData("subscriptionActive", true);
            this.setState({
              subscribed: true
            });
            
            this.props.activateApp("somedata"); // can pass the transaction Id for instance
            navigator.pop({name: 'subscription_screen'});
            navigator.push({name: 'subscription_success'});
          }, 1000);
        }
      }, 5000);
    }).catch(error => {
        let message = "Please check your internet";
        this.setState({messageBoxMarginBottom: 20, errorMessage: message, errorMessageHeight: 70, errorMessageTextSize: 15, messageBackgroundColor: 'rgb(184, 36, 36)'});
        setTimeout(() => {
          this.setState({messageBoxMarginBottom: 0, errorMessageHeight: 0, errorMessageTextSize: 0,});
        }, 5000);
    });
  }

  render() {
    let DEFAULT_URL = 'https://paystack.com/pay/dailyprayercapsules';
    
    let { navigator } = this.props;
    let sendIcon = (<SendIcon color={"white"} size={20} />);
    let payIcon = (<PayIcon color={"white"} size={20} />);
    let subscribeIcon = (<SubscribeIcon color={"white"} size={20} />);
    let windowHeight = Dimensions.get('window').height;
    let windowWidth = Dimensions.get('window').width;

    return (
      <View style={[styles.container]}>
        <View style={[styles.statusBar, {backgroundColor: '#000'}]}></View>
        <View style={[styles.monthHead, {backgroundColor: capsuleTheme.May.headerBackgroundColor}]}>
          <View style={styles.headerLeft}>
            <TouchableOpacity style={{width: 50}} onPress={() => navigator.pop({name: 'subscription_screen'})}>
              <View />
            </TouchableOpacity>
          </View>
          <View style={styles.headerCenter}>
            <Text style={[styles.headText, {color: capsuleTheme.May.headerTextColor}]}> App Subscription </Text>
          </View>
          <View style={styles.headerRight}></View>
        </View>
        <View style={styles.WebViewEmbed}>
          <View style={styles.form}>
            <View style={styles.formTop}>
              <Animatable.View style={styles.subscribeForm} ref="subscribeForm">
                <TextInput value={this.state.subscriptionCode} placeholder="Enter Subscription Code" changeInput={(value) => this.changeInput('subscriptionCode', value)} />
                <TextInput value={this.state.email} placeholder="Your Email" changeInput={(value) => this.changeInput('email', value)} />
              </Animatable.View>
              {/* <Animatable.View style={[styles.registerForm, {height: 0, overflow: 'hidden'}]} ref="registerForm">
                <TextInput value={this.state.firstname} placeholder="Firstname" changeInput={(value) => this.changeInput('firstname', value)} />
                <TextInput value={this.state.lastname} placeholder="Lastname" changeInput={(value) => this.changeInput('lastname', value)} />
                <TextInput value={this.state.phone_number} placeholder="Phone Number" changeInput={(value) => this.changeInput('phone_number', value)} />
                <TextInput value={this.state.email} placeholder="Your Email" changeInput={(value) => this.changeInput('email', value)} />
              </Animatable.View> */}
              <View style={{marginTop: 0, marginBottom: 20}}>
                  <Button 
                    buttonStyles={{backgroundColor: theme.menuBackgroundColor, borderColor: '#555', width: '100%'}}
                    textStyles={{color: 'white', fontSize: 15, marginLeft: -5}}
                    text='Activate Subscription' icon={subscribeIcon} inline={true} switchScreen={() => this.activateApp()} />

                  <Animatable.View 
                    transition={['height']} 
                    style={{justifyContent: 'center', 
                      alignItems: 'center', 
                      backgroundColor: this.state.messageBackgroundColor || 'rgb(184, 36, 36)', 
                      marginTop: 20, 
                      marginBottom: this.state.messageBoxMarginBottom || 0, 
                      borderRadius: 3, 
                      justifyContent: 'center', 
                      alignItems: 'center', 
                      height: this.state.errorMessageHeight || 0,
                      overflow: 'hidden'
                    }}>
                      <Animatable.Text style={{textAlign: 'center', padding: 10,
                        fontSize: this.state.errorMessageTextSize || 0, color: 'white', fontFamily: 'lato-regular'}}>
                          {this.state.errorMessage}
                      </Animatable.Text>
                  </Animatable.View>

                  <Button 
                    buttonStyles={{marginBottom: 20, backgroundColor: theme.greenBackgroundColor, borderColor: '#555', marginTop: 0, width: '100%'}}
                    textStyles={{color: 'white', fontSize: 15, marginLeft: -5}}
                    text='Register for Subscription Code' icon={payIcon} inline={true} switchScreen={() => this.props.navigator.push({name: 'registration_screen'})} />

                  <Button 
                    buttonStyles={{backgroundColor: 'rgb(184, 36, 36)', borderColor: '#555', marginTop: 0, width: '100%'}}
                    textStyles={{color: 'white', fontSize: 15, marginLeft: -5}}
                    text='Pay online and Activate' icon={payIcon} inline={true} switchScreen={() => this.openWebPay()} />
              </View>
            </View>
            <View style={styles.formBellow}>
              <View style={styles.contactSection}>
                  <Text style={styles.contactText}>Support Contact: <Text style={styles.number}>0803 830 1804</Text></Text>
              </View>
            </View>
          </View>
          
          <Animatable.View 
              transition={['height']} 
              duration={1300}
              easing="ease-in-out-back"
              ref="payView"
              style={{
                height: this.state.payBoxHeight || 0 ,
                width: windowWidth,
                position: 'absolute',
                // top: 0,
                // bottom: 0,
                zIndex: 10
              }}>
              <View style={{backgroundColor: 'white', height: windowHeight - 70 - 100}}>
                {this.state.payOnlineActive &&
                <WebView
                  onNavigationStateChange={(navigationState) => this.navigationChange(navigationState)}
                  automaticallyAdjustContentInsets={false}
                  source={{uri: DEFAULT_URL}}
                  javaScriptEnabled={true}
                  domStorageEnabled={true}
                  decelerationRate="normal"
                  startInLoadingState={true}
                />
                }
              </View>
              <View style={{flex: 1, backgroundColor: '#555', padding: 20}}>
                <Button 
                  buttonStyles={{backgroundColor: 'rgb(184, 36, 36)', borderColor: '#555', elevation: 2, marginTop: 0, width: '100%'}}
                  textStyles={{color: 'white', fontSize: 15, marginLeft: -5}}
                  text='Back to Code Subscription' icon={payIcon} inline={true} switchScreen={() => this.closeWebPay()} />
              </View>
            </Animatable.View>
        </View>
      </View>
    );
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  form: {
    height: 300,
    flex: 1,
  },
  formTop: {
    flex: 1,
    padding: 20,
  },
  formBellow: {
    height: 40
  },
  contactSection: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: theme.menuBackgroundColor,
  },
  contactText: {
    fontFamily: 'lato-regular',
    color: 'white'
  },
  number: {
    fontFamily: 'lato-heavy'
  },
  header: {
      backgroundColor: '#02223b',
      minHeight: 50,
      justifyContent: 'center'
  },
  headText: {
    fontSize: 17,
    textAlign: 'center'
  },
  statusBar: {
    backgroundColor: '#fbf7d2',
    height: Constants.statusBarHeight,
  },
  monthHead: {
    backgroundColor: '#555',
    paddingLeft: 20,
    paddingRight: 20,
    height: 50,
    alignItems: 'center',
    flexDirection: 'row'
    // marginTop: 10
  },
  headerText: {
      fontSize: 15,
      color: 'white',
      justifyContent: 'center',
  },
  BackButton: {
      marginRight: 10,
      marginLeft: 10
  },
  headerLeft: {
    width: 70,
    justifyContent: 'center',
  },
  headerCenter: {
    justifyContent: 'center',
    alignSelf: 'center',
    flex: 1
  },
  headerRight: {
    width: 70
  },
  WebViewEmbed: {
      backgroundColor: "#eee",
      flex: 1
  }
});


