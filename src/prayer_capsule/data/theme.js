export const capsuleTheme =  {
    January: {
        selectorTextColor: '#ec3237',
        selectorBackgroundColor: '#fbf7d2',
        headerBackgroundColor: '#fbf7d2',
        headerTextColor: "#ec3237",
    },
    February: {
        selectorTextColor: '#fff',
        selectorBackgroundColor: '#ee963c',
        headerBackgroundColor: '#ee963c',
        headerTextColor: '#fff'
    },
    March: {
        selectorTextColor: '#555',
        selectorBackgroundColor: '#9fd4f4',
        headerBackgroundColor: '#9fd4f4',
        headerTextColor: '#555'
    },
    April: {
        selectorTextColor: '#555',
        selectorBackgroundColor: '#bcc971',
        headerBackgroundColor: '#bcc971',
        headerTextColor: '#555'
    },
    May: {
        selectorTextColor: '#fff',
        selectorBackgroundColor: '#4090ab',
        headerBackgroundColor: '#4090ab',
        headerTextColor: '#fff'
    },
    June: {
        selectorTextColor: '#3e4095',
        selectorBackgroundColor: '#fbdd41',
        headerBackgroundColor: '#fbdd41',
        headerTextColor: '#3e4095'
    },
    July: {
        selectorTextColor: '#363435',
        selectorBackgroundColor: '#dadad0',
        headerBackgroundColor: '#dadad0',
        headerTextColor: '#363435'
    },
    August: {
        selectorTextColor: '#fff9af',
        selectorBackgroundColor: '#df7e9f',
        headerBackgroundColor: '#df7e9f',
        headerTextColor: '#fff9af'
    },
    September: {
        selectorTextColor: '#fff',
        selectorBackgroundColor: '#cb0c13',
        headerBackgroundColor: '#cb0c13',
        headerTextColor: '#fff'
    },
    October: {
        selectorTextColor: '#353334',
        selectorBackgroundColor: '#dbe9ab',
        headerBackgroundColor: '#dbe9ab',
        headerTextColor: '#353334'
    },
    November: {
        selectorTextColor: '#363435',
        selectorBackgroundColor: '#ece9f2',
        headerBackgroundColor: '#ece9f2',
        headerTextColor: '#363435'
    },
    December: {
        selectorTextColor: '#fff',
        selectorBackgroundColor: '#d86890',
        headerBackgroundColor: '#d86890',
        headerTextColor: '#fff'
    }
}

import { Dimensions } from 'react-native';

let windowWidth = Dimensions.get('window').width;
let screenPadding = 10;
let contentWidth = '100%';
let readingContentMarginTop = 0;
if(windowWidth > 600) {
    contentWidth = 500;
    readingContentMarginTop = 20;
}

export default theme = {
    headerColor: '#154fbd',
    statusBarColor: '#154fbd',
    topSectionBackgroundColor: '#e5edf9',
    menuBackgroundColor: '#1d2067',
    menuBackgroundColor2: '#3d2067',
    daySelectorBackgroundColor: 'white',
    greenBackgroundColor: '#077a41',
    contentAreaWidth: contentWidth,
    contentOffsetBackgroundColor: '#cccccc',
    readingContentMarginTop
}