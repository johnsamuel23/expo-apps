export const months = [
    {
        "name": "January", 
        "slug": "jan",
        "days": [
            {day: 1},
            {day: 2},
            {day: 3},
            {day: 4},
            {day: 5},
            {day: 6},
            {day: 7},
            {day: 8},
            {day: 9},
            {day: 10},
            {day: 11},
            {day: 12},
            {day: 13},
            {day: 14},
            {day: 15},
            {day: 16},
            {day: 17},
            {day: 18},
            {day: 19},
            {day: 20},
            {day: 21},
            {day: 22},
            {day: 23},
            {day: 24},
            {day: 25},
            {day: 26},
            {day: 27},
            {day: 28},
            {day: 29},
            {day: 30},
            {day: 31}
        ]  
    },
    {
        "name": "February", 
        "slug": "feb",
        "days": [
            {day: 1},
            {day: 2},
            {day: 3},
            {day: 4},
            {day: 5},
            {day: 6},
            {day: 7},
            {day: 8},
            {day: 9},
            {day: 10},
            {day: 11},
            {day: 12},
            {day: 13},
            {day: 14},
            {day: 15},
            {day: 16},
            {day: 17},
            {day: 18},
            {day: 19},
            {day: 20},
            {day: 21},
            {day: 22},
            {day: 23},
            {day: 24},
            {day: 25},
            {day: 26},
            {day: 27},
            {day: 28}
        ]  
    },
    {
        "name": "March", 
        "slug": "mar",
        "days": [
            {day: 1},
            {day: 2},
            {day: 3},
            {day: 4},
            {day: 5},
            {day: 6},
            {day: 7},
            {day: 8},
            {day: 9},
            {day: 10},
            {day: 11},
            {day: 12},
            {day: 13},
            {day: 14},
            {day: 15},
            {day: 16},
            {day: 17},
            {day: 18},
            {day: 19},
            {day: 20},
            {day: 21},
            {day: 22},
            {day: 23},
            {day: 24},
            {day: 25},
            {day: 26},
            {day: 27},
            {day: 28},
            {day: 29},
            {day: 30},
            {day: 31}
        ]  
    },
    {
        "name": "April", 
        "slug": "April",
        "days": [
            {day: 1},
            {day: 2},
            {day: 3},
            {day: 4},
            {day: 5},
            {day: 6},
            {day: 7},
            {day: 8},
            {day: 9},
            {day: 10},
            {day: 11},
            {day: 12},
            {day: 13},
            {day: 14},
            {day: 15},
            {day: 16},
            {day: 17},
            {day: 18},
            {day: 19},
            {day: 20},
            {day: 21},
            {day: 22},
            {day: 23},
            {day: 24},
            {day: 25},
            {day: 26},
            {day: 27},
            {day: 28},
            {day: 29},
            {day: 30}
        ]  
    },
    {
        "name": "May", 
        "slug": "may",
        "days": [
            {day: 1},
            {day: 2},
            {day: 3},
            {day: 4},
            {day: 5},
            {day: 6},
            {day: 7},
            {day: 8},
            {day: 9},
            {day: 10},
            {day: 11},
            {day: 12},
            {day: 13},
            {day: 14},
            {day: 15},
            {day: 16},
            {day: 17},
            {day: 18},
            {day: 19},
            {day: 20},
            {day: 21},
            {day: 22},
            {day: 23},
            {day: 24},
            {day: 25},
            {day: 26},
            {day: 27},
            {day: 28},
            {day: 29},
            {day: 30},
            {day: 31}
        ]  
    },
    {
        "name": "June", 
        "slug": "june",
        "days": [
            {day: 1},
            {day: 2},
            {day: 3},
            {day: 4},
            {day: 5},
            {day: 6},
            {day: 7},
            {day: 8},
            {day: 9},
            {day: 10},
            {day: 11},
            {day: 12},
            {day: 13},
            {day: 14},
            {day: 15},
            {day: 16},
            {day: 17},
            {day: 18},
            {day: 19},
            {day: 20},
            {day: 21},
            {day: 22},
            {day: 23},
            {day: 24},
            {day: 25},
            {day: 26},
            {day: 27},
            {day: 28},
            {day: 29},
            {day: 30},
        ]  
    },
    {
        "name": "July", 
        "slug": "july",
        "days": [
            {day: 1},
            {day: 2},
            {day: 3},
            {day: 4},
            {day: 5},
            {day: 6},
            {day: 7},
            {day: 8},
            {day: 9},
            {day: 10},
            {day: 11},
            {day: 12},
            {day: 13},
            {day: 14},
            {day: 15},
            {day: 16},
            {day: 17},
            {day: 18},
            {day: 19},
            {day: 20},
            {day: 21},
            {day: 22},
            {day: 23},
            {day: 24},
            {day: 25},
            {day: 26},
            {day: 27},
            {day: 28},
            {day: 29},
            {day: 30},
            {day: 31}
        ]  
    },
    {
        "name": "August", 
        "slug": "Aug",
        "days": [
            {day: 1},
            {day: 2},
            {day: 3},
            {day: 4},
            {day: 5},
            {day: 6},
            {day: 7},
            {day: 8},
            {day: 9},
            {day: 10},
            {day: 11},
            {day: 12},
            {day: 13},
            {day: 14},
            {day: 15},
            {day: 16},
            {day: 17},
            {day: 18},
            {day: 19},
            {day: 20},
            {day: 21},
            {day: 22},
            {day: 23},
            {day: 24},
            {day: 25},
            {day: 26},
            {day: 27},
            {day: 28},
            {day: 29},
            {day: 30},
            {day: 31}
        ]  
    },
    {
        "name": "September", 
        "slug": "sep",
        "days": [
            {day: 1},
            {day: 2},
            {day: 3},
            {day: 4},
            {day: 5},
            {day: 6},
            {day: 7},
            {day: 8},
            {day: 9},
            {day: 10},
            {day: 11},
            {day: 12},
            {day: 13},
            {day: 14},
            {day: 15},
            {day: 16},
            {day: 17},
            {day: 18},
            {day: 19},
            {day: 20},
            {day: 21},
            {day: 22},
            {day: 23},
            {day: 24},
            {day: 25},
            {day: 26},
            {day: 27},
            {day: 28},
            {day: 29},
            {day: 30}
        ]  
    },
    {
        "name": "October", 
        "slug": "oct",
        "days": [
            {day: 1},
            {day: 2},
            {day: 3},
            {day: 4},
            {day: 5},
            {day: 6},
            {day: 7},
            {day: 8},
            {day: 9},
            {day: 10},
            {day: 11},
            {day: 12},
            {day: 13},
            {day: 14},
            {day: 15},
            {day: 16},
            {day: 17},
            {day: 18},
            {day: 19},
            {day: 20},
            {day: 21},
            {day: 22},
            {day: 23},
            {day: 24},
            {day: 25},
            {day: 26},
            {day: 27},
            {day: 28},
            {day: 29},
            {day: 30},
            {day: 31}
        ]  
    },
    {
        "name": "November", 
        "slug": "Nov",
        "days": [
            {day: 1},
            {day: 2},
            {day: 3},
            {day: 4},
            {day: 5},
            {day: 6},
            {day: 7},
            {day: 8},
            {day: 9},
            {day: 10},
            {day: 11},
            {day: 12},
            {day: 13},
            {day: 14},
            {day: 15},
            {day: 16},
            {day: 17},
            {day: 18},
            {day: 19},
            {day: 20},
            {day: 21},
            {day: 22},
            {day: 23},
            {day: 24},
            {day: 25},
            {day: 26},
            {day: 27},
            {day: 28},
            {day: 29},
            {day: 30},
        ]  
    },
    {
        "name": "December", 
        "slug": "Dec",
        "days": [
            {day: 1},
            {day: 2},
            {day: 3},
            {day: 4},
            {day: 5},
            {day: 6},
            {day: 7},
            {day: 8},
            {day: 9},
            {day: 10},
            {day: 11},
            {day: 12},
            {day: 13},
            {day: 14},
            {day: 15},
            {day: 16},
            {day: 17},
            {day: 18},
            {day: 19},
            {day: 20},
            {day: 21},
            {day: 22},
            {day: 23},
            {day: 24},
            {day: 25},
            {day: 26},
            {day: 27},
            {day: 28},
            {day: 29},
            {day: 30},
            {day: 31}
        ]  
    },
];

export const capsuleData = {
    January: [
        {   
            scriptureText: 'Deuteronomy 23:14 - “For the Lord your God walks in the midst of your camp, to deliver you and give your enemies over to you; therefore your camp shall be Holy, that He may see no unclean things among you, and turn away from you',
            prayerText: "The only barrier between the believer's security and God is sin. O Lord, I shall not touch anything unholy to scare your presence away from my life, in the name of Jesus." 
        },
        {
            scriptureText: 'Joshua 24:17 - “For the Lord our God is He who brought us and our fathers up out of the land of Egypt from the house of bondage, who did those great signs in our sight and preserved us in all the way that we want among all the people through who we passed.',
            prayerText: 'Teach my heart to follow your path, O Lord!, in the name of Jesus'
        },
        {
            scriptureText: "Ezekiah 9:9 - For we were slaves, Yet our God did not forsake us in our bondage; but He extended mercy to us in the sight of the kings of Persia to revive us, to repair the house of our God, to rebuild its ruins, and to give us a wall in Judah and Jerusalem.",
            prayerText: "Thank you dear Lord for while I was yet a slave to sin you did not forsake me. O Lord extend your mercy through your protective angels to revive me, rebuild every broken down walls of my life, in the name of Jesus.",
        },
        {
            scriptureText: "Psalm 5:11 - But let all those that put their trust in you; let them ever shout for joy because you defend them; Let those also who love your name be joyful in you.",
            prayerText: "Because I put my trust in you Lord, I shall be joyful, I shall rejoice and I will continually shout for joy because you are my defence, in the name of Jesus.",
        },
        {
            scriptureText: "Psalm 32:7 - You are my hiding place; you shall preserve me from trouble; you shall surround me with songs of deliverance",
            prayerText: "O Lord, you shall continually hide me from troubles, you will preserve me and surround me with testimonies and songs of victory through your mighty deliverance, in the name of Jesus.",
        },
        {
            scriptureText: "Psalm 40:11 - Do not withhold your tender mercies from me, O Lord, let your loving kindness and your truth continually preserve me.",
            prayerText: "O Lord my God let your divine truth, loving kindness and tender mercies preserve my life, husband, wife, children and work  in the name of Jesus.",
        },
        {
            scriptureText: "Psalm 40:2 - He also brought me up out of a horrible pit, out of the miry clay, and set my feet upon a rock, And established my steps.",
            prayerText: "This day;, in the name of Jesus, I am jumping out of every pit of delay and thou O Lord shall set my feet upon the Rock of Ages who is Jesus my Lord.",
        },
        {
            scriptureText: "Psalm 91:4 - He shall cover you with His feathers, And under His wings you shall take refuge; His truth shall be your shield and buckler.",
            prayerText: "The Lord shall cover my Head, the Head of my Husband, wife, children and work, in the name of Jesus. I confess today that our refuge is in Him - He remains our Truth and buckler, in the name of Jesus.",
        },
        {
            scriptureText: "Psalm 116:6 - The Lord preserves the simple; I was brought low, and He saved me.",
            prayerText: "O Lord my Father; make my heart simple enough to enjoy your safety throughout my life time, in the name of Jesus.",
        },
        {
            scriptureText: "Psalm 140:1 - Deliver me, O Lord from evil men; preserve me from violent men.",
            prayerText: "Oh Lord my God, deliver my life, Household, work/staff and children from evil men/women. Deliver our children from kidnappers and preserve us and our Nation from every form of violence, in the name of Jesus.",
        },
        {
            scriptureText: "Proverbs 2:11 - Discretion will preserve you; understanding will keep you.",
            prayerText: "O Lord, baptize me with fresh wisdom for effectiveness, in the name of Jesus.",
        },
        {
            scriptureText: "Proverbs 4:6 - Do not forsake her, and she will preserve you; love her and she will keep you.",
            prayerText: "Through your wisdom O Lord, I shall be preserved, in the name of Jesus.",
        },
        {
            scriptureText: "John 17:11 - “Now I am no longer in the world, but these are in the world and I come to you. Holy Father keep through your name those whom you have given me, that they may be one as we are.",
            prayerText: "O Lord, give me grace to abide in You to maintain security through the name of Jesus Christ my Lord.",
        },
        {
            scriptureText: "2 Thessalonians 3:3 - That no one should be shaken by these afflictions; for you yourselves know that we are appointed to this.",
            prayerText: "O Lord, help me to remain steadfast in you, in the name of Jesus.",
        },
        {
            scriptureText: "1 Chronicles 16:8 - Oh, give thanks to the Lord; call upon this name; make known His deeds among the peoples!",
            prayerText: "My father and my Lord, today; I declare a voice of thanksgiving that will bring a come and see testimony and miracle in my life, in the name of Jesus.",
        },
        {
            scriptureText: "Nehemiah 12:31 - So I brought the leaders of Judah up on the wall, and appointed two large thanksgiving choirs. One went to the right hand on the wall towards the refuse Gate.",
            prayerText: "O Lord, help me to maintain a grateful heart throughout my lifetime, in the name of Jesus.",
        },
        {
            scriptureText: "Psalm 75:1 - We give thanks to you, O God we give thanks; for your wondrous works declare that your name is near. ",
            prayerText: "Eternal Father teach me to acknowledge your presence at every declaration of thanksgiving in my life, work and family, in the name of Jesus.",
        },
        {
            scriptureText: "Psalm 35:18 - I will give thanks in the great assembly, I will praise you amongst many people.",
            prayerText: "Daddy Lord, in the name of Jesus confront me today with testimonies and miracles that great men and women in noble callings shall celebrate with me, in the name of Jesus.",
        },
        {
            scriptureText: "Psalm 95:2 - Let us come before His presence with thanksgiving; Let us shout joyfully to Him with Psalm.",
            prayerText: "Thank you Lord for the adoption through the blood of Jesus where by I can join to the saints in your presence to render thanksgiving and to shout joyfully to your name.",
        },
        {
            scriptureText: "Psalm 100:4 - Enter unto His gates with thanksgiving; And into His courts with praise. Be thankful to Him, and bless His name.",
            prayerText: "Keep me O God at thy gates with thanksgiving and in throne courts with praises, in the name of Jesus.",
        },
        {
            scriptureText: "Psalm 118:28 - You are my God, I will praise you; you are my God I will exalt you.",
            prayerText: "Today the assurance of your Lordship over my life is double; I will praise you and my soul will exalt you over my life, work and Christian journey, in the name of Jesus.",
        },
        {
            scriptureText: "Psalm 136:1 - Oh, give thanks to the Lord for He is good. For His mercy endureth for ever.",
            prayerText: "In thanksgiving, I confess your goodness over my life today. Your mercy over my family members will endure till the close of age, in the name of Jesus.",
        },
        {
            scriptureText: "Romans 1:21 - Because although they knew God, they did not glorify Him as God, nor were thankful but became futile in their thoughts and their foolish hearts were darkened.",
            prayerText: "Holy Spirit, baptize us with the right perception of your glory so that we may acknowledge you all the days of our lives and in all that we do, in the name of Jesus.",
        },
        {
            scriptureText: "1 Corinthians 11:24 - And when He had given thanks He broke it and said “Take, eat; this is my body which is broken for you. Do this in remembrance of me”",
            prayerText: "At every opportunity I have, Daddy Lord, help me to say “Thank you” over everything that keeps my life going, in the name of Jesus.",
        },
        {
            scriptureText: "1 Corinthians 15:57 - But thanks be to God who gives us the victory through our Lord Jesus Christ.",
            prayerText: "Thank you Lord, for giving me victory in life and my work, in the name of Jesus.",
        },
        {
            scriptureText: "2 Corinthians 9:15 - Thanks be to God for His indescribable gift!",
            prayerText: "Thank you Lord for giving us Jesus who has given us all things through His death and resurrection, in the name of Jesus.",
        },
        {
            scriptureText: "Philippians 1:3 - I thank my God upon every remembrance of you.",
            prayerText: "Lord Jesus grant me the grace to remember and pray for my brethren in faith, in the name of Jesus.",
        },
        {
            scriptureText: "Psalm 119:11 - Your word have I hid in my heart, That I might not sin against you.",
            prayerText: "Father, in the name of Jesus, grant me an insight into the bible, to study, meditate and memorise your Word; help me to keep your truth in my heart so that I will not ere nor sin against you in all my endeavours, in the name of Jesus.",
        },
        {
            scriptureText: "Psalm 119:105 - Your word is a lamp to my feet, and a light unto my path.",
            prayerText: "I confess and declare in the mighty name of Jesus that the Word of God will continue to guide my path unto peace, success, righteousness, in the name of Jesus. The light through your Word shall continually guide my path unto the perfect day, in the name of Jesus.",
        },
        {
            scriptureText: "Isaiah 55:11 - So shall my word be that goes forth from my mouth; It shall not return to me void, But it shall accomplish what I please. And it shall prosper in the thing for which I sent.",
            prayerText: "Every spoken word of God concerning my life, family, work and business, shall prosper, in the name of Jesus.",
        },
        {
            scriptureText: "Matthew 12:36 - “But I say unto you that for every idle word men may speak, they will give account of it in the day of judgment.",
            prayerText: "O Lord, through your power and your fire; Deliver my tongue from every idle word, in the name of Jesus.",
        },    
    ],
    February: [
        {
            scriptureText: "John 1:1 - In the beginning was the Word and the Word was with God, and the Word was God.  ",
            prayerText: "Prayer: O Lord, by the  power of the Spirit help  me to see Jesus in  everything I do, in the name of Jesus.",
        },
        {
            scriptureText: "John 1:14 - And the word became flesh and dwelt among us, and we beheld His glory, the glory as of the only begotten of the father, full of grace and truth.",
            prayerText: "I receive your word today as flesh and I shall behold your glory through your grace and truth by your Holy Spirit, in the name of Jesus.",
        },
        {
            scriptureText: "Hebrew 4:12 - For the word of God is living and sharper than any two edged sword, piercing even to the dividing of soul and spirit and of joints and marrow and is a discerner of the thoughts and intents of the heart. ",
            prayerText: "These ministries of the Word shall be revealed as my life, work and family encounter God afresh through His word, in the name of Jesus",
        },
        {
            scriptureText: "Revelation 19:13 - He was clothed with a robe dipped in blood, and His name is called the Word of God. ",
            prayerText: "I receive Grace to live in the Word of God, in the name of Jesus",
        },
        {
            scriptureText: "Exodus 14:14: - The Lord shall fight for you, and ye shall hold your peace. ",
            prayerText: "Dear Lord, fight my battles for me and let your peace settle in my heart, in the name of Jesus.",
        },
        {
            scriptureText: "Exodus 23:20 - Behold, I send an Angel before thee, to keep thee in the way. ",
            prayerText: "Lord, go before me today and make my way prosperous, in the name of Jesus.",
        },
        {
            scriptureText: "Numbers 32:23 - But if you will not do so, behold, ye have sinned against the Lord: and be sure your sin will find you out.",
            prayerText: "Unrighteousness that has been hindering my blessings, Lord reveal it to me, in the name of Jesus.",
        },
        {
            scriptureText: "Deuteronomy 29:29 - The secret things belong unto the LORD, our God: but those things which are revealed belong unto us and to our children for ever...",
            prayerText: "Holy Spirit reveal secrets that will enhance my Christian living unto me, in the name of Jesus.",
        },
        {
            scriptureText: "Deuteronomy 31:6 - Be strong and of a good courage, fear not, nor be afraid of them: for the Lord thy God, he it is that doth go with thee; he will not fail thee, nor forsake thee.",
            prayerText: "I receive courage to go through my ordained activities for today, in the name of Jesus.",
        },
        {
            scriptureText: "Judges 21:25 - In those days there was no king in Israel: every man did that which was right in his own eyes.",
            prayerText: "Father Lord, give me Grace to do things that are pleasing in your sight, in the name of Jesus.",
        },
        {
            scriptureText: "1 Samuel 15:22 - And Samuel said, Hath the Lord as great delight in burnt offerings and sacrifices, as in obeying the voice of the Lord? Behold, to obey is better than sacrifice, and to hearken than the fat of rams.",
            prayerText: "Lord, help me to obey your words, and not to trust in my sacrifices unto you, in the name of Jesus.",
        },
        {
            scriptureText: "1 Samuel 22:31 - As for God, his way is perfect; the word of the Lord, is tried: he is a buckler to all them that trust in him.",
            prayerText: "Today, the word of God shall be my defence, and my trust, in the name of Jesus.",
        },
        {
            scriptureText: "1 Kings 2:3 - And keep the charge of the Lord thy God, to walk in his ways, to keep his statues, ....that thou mayest prosper in all that thou doest, and whithersoever thou turnest thyself.",
            prayerText: "By the Grace of God I shall walk in the Word that will prosper me, in the name of Jesus.",
        },
        {
            scriptureText: "2 Kings 17:39 - But the Lord your God ye shall fear; and he shall deliver you out of the hand of all your enemies.",
            prayerText: "Lord, let this prophetic truth, be real and come to pass in my life today, in the name of Jesus.",
        },
        {
            scriptureText: "Job 13:26 - For thou writest bitter things against me, and makest me to possess the iniquities of my youth.",
            prayerText: "Every bitter handwriting against my life, be erased by the blood of Jesus, in the name of Jesus.",
        },
        {
            scriptureText: "Job 10:12 - Thou hast granted me life and favour, and thy visitation hath preserved my spirit.",
            prayerText: "I desire your favour and visitation Oh Lord, visit me in every department of my life, in the name of Jesus.",
        },
        {
            scriptureText: "Job 12:13 - With him is wisdom and strength, he hath counsel and understanding.",
            prayerText: "I receive divine wisdom and strength to be effective today in every thing I shall do, in the name of Jesus.",
        },
        {
            scriptureText: "Job 23:10 - But he knoweth the way that I take: when he hath tried me, I shall come forth as gold.",
            prayerText: "in the name of Jesus, my season and time of celebration shall surely come, in the name of Jesus.",
        },
        {
            scriptureText: "Job 28:9 - He pulleth forth his hand upon the rock; he overturneth the mountains by the roots.",
            prayerText: "Every mountain of hindrance on my way, by the mighty hands of Jehovah, be overturn, in the name of Jesus.",
        },
        {
            scriptureText: "Job 28:3 - He setteth an end to darkness and searcheth out all perfection: the stones of darkness, and the shadow of death.",
            prayerText: "By the shield of God, I reject stones of darkness in every department of my life, in the name of Jesus.",
        },
        {
            scriptureText: "Isaiah 6:8 - Also I heard the voice of the Lord, saying, whom shall I send, and who will go for us? Then said I, Here am I? Send me.",
            prayerText: "Holy Spirit, Illuminate my spirit so that I can continually hear your voice for direction concerning my life, in the name of Jesus.",
        },
        {
            scriptureText: "Proverbs 27:1 - Boast not thyself of tomorrow; for thou knowest not what a day may bring forth.",
            prayerText: "Father Lord, I commit my life and future into your hands, lead me in the way I should go,, in the name of Jesus.",
        },
        {
            scriptureText: "Isaiah 30:8 - Now go, write it before them in a table and note it in a book, that it may be for the time to come forever and ever.",
            prayerText: "By the help of the Holy Spirit, the Word of God will not depart from my heart and mouth, in the name of Jesus.",
        },
        {
            scriptureText: "Isaiah 40:28-29 – Hast thou not known? Hast thou not heard, that the everlasting God, the Lord, the creator of the ends of the Earth, fainteth not, neither is weary? There is no searching of his understanding. He giveth power to the faint; and to them that have no might he increaseth strength.",
            prayerText: "Lord grant unto me a higher dimension of understanding concerning the affairs of life,, in the name of Jesus.",
        },
        {
            scriptureText: "Isaiah 41:10 - Behold, the Lord God will come with strong hand, and his arm shall rule for him; behold his reward is with him, and his work before him.",
            prayerText: "Oh Lord, let your stronghold be upon me,, in the name of Jesus.",
        },
        {
            scriptureText: "Genesis 35:22 - Then Jacob said unto his household, and to all that were with him, put away the strange gods that are among you, and be clean, and change your garment.",
            prayerText: "The filthy garment on my Spiritual body, I set you ablaze by fire,, in the name of Jesus.",
        },
        {
            scriptureText: "Ecclesiastes 3:1 - To every thing there is a season, and a time to every purpose under heaven.",
            prayerText: "The time and season of my lifting will not pass me by,, in the name of Jesus.",
        },
        {
            scriptureText: "Psalm 46:10 - Be still and know that I am God: I will be exalted among the heathen, I will be exalted in the earth.",
            prayerText: "Let my head Oh Lord, be lifted above the unbelievers around me,, in the name of Jesus.",
        }
    ],
    March: [
        {
            scriptureText: "Psalm 118:24 - This is the day which the Lord hath made; we will rejoice and be glad in it.",
            prayerText: "I command joy and gladness to saturate my life today,, in the name of Jesus.",
        },
        {
            scriptureText: "Proverbs 16:18 - Pride goeth before destruction, and a haughty spirit before a fall.",
            prayerText: "I brake myself loose from the bondage of pride, in the name of Jesus.",
        },
        {
            scriptureText: "Proverbs 22:6 - Train up a child in the way he should go; and when he is old, he will not depart from it.",
            prayerText: "Destructive habits that have become parts of my life, I break loose from your Dominion, in the name of Jesus.",
        },
        {
            scriptureText: "Psalm 119:117 - Hold thou me up, and I shall be safe...",
            prayerText: "Prayer: Oh Lord, Hold me and keep me safe, in the name of Jesus.",
        },
        {
            scriptureText: "2 Timothy 1:7 - For God has not given us the spirit of fear...",
            prayerText: "Today, I renounce and reject fear and I declare that I am full of power, Love and sound mind, in the name of Jesus.",
        },
        {
            scriptureText: "2 Thessalonians 1:6 - Seeing it is a righteous thing with God to recompense tribulation to them that trouble you.",
            prayerText: "Oh God, let everyone troubling me, my household, my work and ministry be recompensed with a full dose of tribulation, in the name of Jesus.",
        },
        {
            scriptureText: "Psalm 17:8 - Keep me as the Apple of the eye, hide me under the shadow of thy wings.",
            prayerText: "Prayer: Oh Lord, keep me as the apple of your eyes and hide me under the shadow of thy wings, in the name of Jesus.",
        },
        {
            scriptureText: "Romans 8:37 - Nay, in all these things we are more than conqueror",
            prayerText: "I am more than a conqueror and I will go forth today conquering, in the name of Jesus.",
        },
        {
            scriptureText: "Isaiah 54:15 - Behold they shall surely gather together; but not by me: whosoever shall gather together against thee shall fall for they sake.",
            prayerText: "Oh God, let all that are gathered together against me fall to uselessness, in the name of Jesus.",
        },
        {
            scriptureText: "Psalm 46:9 - He maketh wars to cease unto the end of the earth; He breaketh the bow, and cutteth the spear in sunder",
            prayerText: "Oh God, by your power, break the bows of my enemies. Cut their spear in sunder, burn all their weapons and cause every war raging against me to cease, in the name of Jesus.",
        },
        {
            scriptureText: "Psalm 46:1 - God is our refuge and strength, a very present help in trouble.",
            prayerText: "Oh God, be my refuge and strength as I go about fulfilling my prophetic destiny, in the name of Jesus.",
        },
        {
            scriptureText: "Psalm 18:48 - He delivereth me from my enemies:",
            prayerText: "Oh God, by your mercy, deliver me from my enemies, in the name of Jesus.",
        },
        {
            scriptureText: "Psalm 3:3 - But thou O Lord, art a shield for me:",
            prayerText: "Everywhere that my head is bowed down, Oh Lord by your mercy, lift me up, in the name of Jesus.",
        },
        {
            scriptureText: "Numbers 23:8 - How shall I curse whom God has not cursed?",
            prayerText: "Just as the attempt to curse Israel by Balak was foiled, O Lord! let every attempt by the enemies to devise evil against me fail, in the name of Jesus.",
        },
        {
            scriptureText: "Numbers 23:23 - Surely, there is no enchantment against Jacob. ",
            prayerText: "Oh God, let every evil enchantments and divinations against me be turned to blessings, in the name of Jesus.",
        },
        {
            scriptureText: "Psalm 70:2 - Let them be ashamed and confounded that seek after my soul:",
            prayerText: "Oh God, let them that desire my soul and seek after my heart be confounded and put to shame, in the name of Jesus.",
        },
        {
            scriptureText: "Psalm 34:19 - Many are the afflictions of the righteous but the Lord delivereth him out of them all.",
            prayerText: "Prayer: From all my afflictions O Lord, let your hands be stretched forth for my deliverance, in the name of Jesus.",
        },
        {
            scriptureText: "Galatians 5:10b - But he that troubleth you shall bear his judgment, whosoever he be.",
            prayerText: "Let all the brewers of trouble against me receive the judgment of fire, in the name of Jesus.",
        },
        {
            scriptureText: "Psalm 9:16 - The Lord is known by the judgment which he executeth...",
            prayerText: "Oh Lord, let them that are devising wickedness against me, be taken by the snares of their evil machinations, in the name of Jesus.",
        },
        {
            scriptureText: "Psalm 25:2 - Oh my God, I trust in thee let me not be ashamed, let not mine enemies triumph over me.",
            prayerText: "Oh God, let me not be ashamed and let not my enemies triumph over me in all my godly endeavours, in the name of Jesus.",
        },
        {
            scriptureText: "Psalm 25:15 - Mine eyes are ever toward the Lord; for he shall pluck my feet out of the net.",
            prayerText: "Oh Lord, keep me from the snares of the enemies, in the name of Jesus.",
        },
        {
            scriptureText: "Deuteronomy 20:4 - For the Lord your God is He that goeth with you.",
            prayerText: "Oh God, by your mercy, arise, fight my battles and save me, in the name of Jesus.",
        },
        {
            scriptureText: "1 Samuel 2:1b - My mouth is enlarged over mine enemies.",
            prayerText: "Oh God, by your word, enlarge my mouth over my enemies, in the name of Jesus.",
        },
        {
            scriptureText: "Psalm 31:13 - For I have heard the slander of many fear was on every side: while they took counsel together against me...",
            prayerText: "Oh God, let every evil counsel of the enemies against my life be turned to naught, in the name of Jesus.",
        },
        {
            scriptureText: "Exodus 15:9 - The enemy said, I will pursue, I will overtake...",
            prayerText: "Oh God, let your judgment fall upon all my stubborn pursuers, in the name of Jesus.",
        },
        {
            scriptureText: "Isaiah 59:19b - When the enemy shall come in life a flood...",
            prayerText: "Oh God, by your Spirit let a standard be raised against every flood of my enemy, in the name of Jesus.",
        },
        {
            scriptureText: "Psalm 143:3 - For the enemy hath persecuted my soul; he hath smithen my life down to the ground...",
            prayerText: "Oh God, arise and defend my soul from the evil persecutors, in the name of Jesus.",
        },
        {
            scriptureText: "Micah 5:9 - Thine hand shall be lifted up upon thine adversaries, and all thine enemies shall be cut off.",
            prayerText: "Oh God, empower my hand above my enemies, in the name of Jesus.",
        },
        {
            scriptureText: "Micah 7:6b - A man's enemies are the men of his own house...",
            prayerText: "By your mercy O God, deliver me from every household enemy, in the name of Jesus.",
        },
        {
            scriptureText: "Nahum 1:7 - The Lord is good, a stronghold in the day of trouble; and he knoweth them that trust in him.",
            prayerText: "I put my trust in You, Lord, be my stronghold against life troubles, in the name of Jesus.",
        },
        {
            scriptureText: "Nahum 1:13 - For now will I break his yoke from off thee",
            prayerText: "Oh Lord, by your power, let every yoke of the enemy on my life, home, job, ministry be broken and his bonds burst in sunder, in the name of Jesus.",
        },
    ],
    April: [
        {
            scriptureText: "Psalm 27:12 - Deliver me not over unto the will of my enemies:",
            prayerText: "O God, deliver me from falling into the will of my enemies, in the name of Jesus.",
        },
        {
            scriptureText: "Psalm 18:17 - He delivered me from my strong enemy; and from them which hated me: for they were too strong for me.",
            prayerText: "Oh God, by your power, deliver me from the enemies of my soul that are too strong for me, in the name of Jesus.",
        },
        {
            scriptureText: "Revelation 12:11a - And they overcame him by the blood of the lamb, and by the Word of their testimony;",
            prayerText: "I plead the blood of Jesus upon my life and household, in the name of Jesus.",
        },
        {
            scriptureText: "Galatians 6:17 - From henceforth, let no man trouble me.",
            prayerText: "I receive immunity by the marks of Christ in my body and against attacks of the enemies, in the name of Jesus.",
        },
        {
            scriptureText: "Colossians 2:14 - Blotting out the handwriting of ordinances that was against us, which was contrary to us, and took it out of the way, nailing it to his cross.",
            prayerText: "Every evil handwritings against me, my family, work, business and ministry be canceled, in the name of Jesus.",
        },
        {
            scriptureText: "Isaiah 54:17 - No weapon that is formed against thee shall prosper; and every tongue",
            prayerText: "No weapon of death, sickness, poverty and distraction in anyway prosper, in the name of Jesus. against me and my family shall, in the name of Jesus",
        },
        {
            scriptureText: "Psalm 107:29 - He maketh the storm a calm, so that the waves thereof are still.",
            prayerText: "By your power O God, let every raging storm against my life receive calmness, in the name of Jesus.",
        },
        {
            scriptureText: "Jeremiah 32:17 - Ah Lord God, thou hast made the heaven and the earth by thy great power and stretched out arm, and there is nothing too hard for you.",
            prayerText: "O God, let your mighty hand with which you created the heaven and earth be stretched out to fight my battles, in the name of Jesus.",
        },
        {
            scriptureText: "Isaiah 41:13 - For I the Lord will hold thy right hand, saying unto thee, fear not; I will help thee.",
            prayerText: "Oh God my helper, send me help, in the name of Jesus.",
        },
        {
            scriptureText: "2 Kings 6:12B - But Elisha the prophet that is in Israel.",
            prayerText: "O Lord, grant me by your grace to know and understand the strategies of the enemies against my life, family and endeavours,, in the name of Jesus.",
        },
        {
            scriptureText: "Hosea 14:9 - Who is wise, and he shall understand there things? Prudent, and he shall know them? For the ways of the Lord are right, and the just shall walk in them; but the",
            prayerText: "I receive grace to walk with God in purity and righteousness today, in the name of Jesus.",
        },
        {
            scriptureText: "Mathew 5:16 - Let your light so shine before men, that they may see your good works and glorify your father which is in heaven.",
            prayerText: "Let the light of God in my life shine and be captivating to men and women, in the name of Jesus.",
        },
        {
            scriptureText: "Mathew 5:28 - But I say unto you. That whosoever looketh on a woman to lust after her hath committed adultery with her already in his heart.",
            prayerText: "Lust and carnality in my body I command you to die, in the name of Jesus.",
        },
        {
            scriptureText: "Isaiah 41:10 - Fear thou not, for I am with thee; be not dismayed; for I am thy God; I will strengthen thee; Yea, I will help thee; Yea, I will uphold thee with the right hand of my",
            prayerText: "Lord strengthen me in the inner-man by your Spirit,, in the name of Jesus.",
        },
        {
            scriptureText: "Isaiah 43:25 - I, even I, am he that blotteth out thy transgressions for mine own sake, and will not remember thine iniquities.",
            prayerText: "Lord, thank you for salvation through the shed blood of Jesus Christ.",
        },
        {
            scriptureText: "Isaiah 43:2 - When thou passest through the waters, I will be with thee; and through the rivers they shall not overflow thee; when thou walkest through the fire, thou shall not be burned; neither shall the flame kindle upon thee.",
            prayerText: "The Evil rivers of life will not subdue or swallow me,, in the name of Jesus.",
        },
        {
            scriptureText: "Isaiah 59:2 - But your iniquities have separate between you and your God, and your sins have hid his face from you, that he will not hear.",
            prayerText: "Every SIN that is making God to hid his face from me, I wipe you off by the blood of Jesus.",
        },
        {
            scriptureText: "Ezekiel 34:16 - Moreover the word of the Lord came unto me saying...",
            prayerText: "Holy Spirit, give me direction for my life today, in the name of Jesus.",
        },
        {
            scriptureText: "Mark 8:34 - And when he had called the people unto him with his disciples also, he said unto them, whosoever will come after me let him deny himself, and take up his cross, and follow me.",
            prayerText: "Grace to follow the Lord wholeheartedly possess my life,, in the name of Jesus.",
        },
        {
            scriptureText: "Mark 13:37 - And what I say unto you I say unto all, watch.",
            prayerText: "My spiritual eyes be opened, in the name of Jesus.",
        },
        {
            scriptureText: "Genesis 39:21 - But the Lord was with Joseph, and showed him mercy, and gave him favour in the sight of the keeper of the prison.",
            prayerText: "Lord, let your presence abide continually with me, and show me favour before all men,, in the name of Jesus.",
        },
        {
            scriptureText: "Luke 17:32 - Remember Lot's wife.",
            prayerText: "Spirit of backsliding, masquerading around me, I bury you forever,, in the name of Jesus.",
        },
        {
            scriptureText: "Genesis 6:9 - These are the generations of Noah: Noah was a just man and perfect in his generations, and Noah walk with God.",
            prayerText: "The grace to walk with God and to be blameless before him fall on me and my, in the name of Jesus.",
        },
        {
            scriptureText: "Genesis 25:31 - And Jacob said, sell me this day thy birthright.",
            prayerText: "My birthright will not be stolen from me, in the name of Jesus!",
        },
        {
            scriptureText: "Genesis 30:22 - And God Remember Rachel, and God hearkened to her and opened her womb.",
            prayerText: "Prayer: Oh Lord arise, and open the book of remembrance concerning me,, in the name of Jesus.",
        },
        {
            scriptureText: "Psalm 27:1 - The Lord is my light and my salvation; whom shall I fear, the Lord is the strength of my life, of whom shall I be afraid.",
            prayerText: "Prayer: Oh Lord, let your light drive away fear from me,, in the name of Jesus.",
        },
        {
            scriptureText: "Psalm 34:10 - The Young lions do lack and suffer hunger; but they that seek the Lord shall not want any good thing.",
            prayerText: "My father, let the good things of life pursue and overtake me, in the name of Jesus.",
        },
        {
            scriptureText: "Psalm 34:19 - Many are the afflictions of the righteous; but the Lord delivereth him out of them all.",
            prayerText: "Power of God, bury all my afflictions, in the name of Jesus.",
        },
        {
            scriptureText: "Psalm 84:11 - For the Lord God is a sun and shield: the Lord will give grace and glory; no good thing will he withhold from them that walk uprightly.",
            prayerText: "By the glory and the grace of God, I will not lack any good thing, in the name of Jesus.",
        },
        {
            scriptureText: "Mark 6:46 - And when he had sent them away, he departed into a mountain to pray.",
            prayerText: "I receive fresh Grace to pray more than before, in the name of Jesus.",
        },
    ],
    May: [
        {
            scriptureText: "Philippians 4:6 - Be anxious for nothing, but in everything by prayer and supplication, with thanks giving, let your requests be made known unto God.",
            prayerText: "I shall manifest through thanksgiving and all my request shall be granted, in the name of Jesus.",
        },
        {
            scriptureText: "Colossians 2:7 - Rooted and build up in this and established in the faith, as you have been taught, abounding in it with thanksgiving.",
            prayerText: "Today, through the attitude of gratitude, I shall be rooted and be built up in faith, in the name of Jesus.",
        },
        {
            scriptureText: "1 Thessalonians 5:18 - In everything give thanks; for this is the will of God in Christ Jesus for you.",
            prayerText: "Through thanksgiving Lord Jesus teach me your will, in the name of Jesus.",
        },
        {
            scriptureText: "Leviticus 19:8 - You shall not take vengeance, nor bear any grudge against the children of your people but you shall love your neighbour as yourself: I am the Lord.",
            prayerText: "O Lord Almighty bestow upon me today the grace to over come vengeance, grudge and the ability to love others as myself, in the name of Jesus.",
        },
        {
            scriptureText: "Deuteronomy 6:5 - You shall love the Lord your God with all your heart, with all your soul, and with all your strength.",
            prayerText: "Daddy, Lord, today, I yield my members my heart, soul and strength to co-operate with this commandment of loving you till ETERNITY, in the name of Jesus.",
        },
        {
            scriptureText: "Proverbs 10:12 - Hatred stirs up strife, But love covereth all sins.",
            prayerText: "Father, in the name of Jesus I bind the spirit of Hatred and strife in my life and I take over through grace of Jesus to love everyone, in the name of Jesus.",
        },
        {
            scriptureText: "Ecclesiastes 3:8 - A time to love, and a time to hate. A time of war, and a time of Peace.",
            prayerText: "O Lord, cause your love to silent hatred and war in my life, family, church and let your peace rule our Nation, in the name of Jesus.",
        },
        {
            scriptureText: "Songs of Solomon 2:4 - He brought me to the banquet house, And his banner over me was Love.",
            prayerText: "I join in the anthem of the saints and I confess your banner over my life, family and my work today and forever, in the name of Jesus.",
        },
        {
            scriptureText: "Matthew 5:44 - “But I say to you, love your enemies, bless those who curse you, do good to those who hate you, and pray for those who spitefully use you and persecute you.",
            prayerText: "Grant me the grace to love and to bless those who have wronged me especially in my work and in my Christian journey, in the name of Jesus.",
        },
        {
            scriptureText: "John 14:15 If you love me, keep my commandments.",
            prayerText: "Because of the covenant of sonship I have with the Lord, I receive the enablement by the Holy Spirit to keep God's commandment in my life, family and work, in the name of Jesus.",
        },
        {
            scriptureText: "John 15:12 - This is my commandment, that you love one another as I have loved you.",
            prayerText: "Lord Jesus, baptize me, my family members with the kind of love that took you to the cross for my sake. Help me to love others, in the name of Jesus.",
        },
        {
            scriptureText: "Romans 12:9 - Let love be without hypocrisy, Abhor what is evil, cling to what is good.",
            prayerText: "Let the blood of Jesus destroy every activities hypocrisy in my lives, in the name of Jesus",
        },
        {
            scriptureText: "1 Corinthians 8:1b - Knowledge puffs up, but love edifies.",
            prayerText: "Lord, cause your love to edify me in every area of my life (spiritual, physical, emotional, financial) through the blood of Jesus.",
        },
        {
            scriptureText: "1 Corinthians 13:8 - Love never fails. But whether there are prophecies, they will fail whether there are tongues, they will cease. Whether there is knowledge. It will vanish away.",
            prayerText: "My father, my greatest desire is your unconditional love, the love that surpasses sings/wonders, prophecies, tongues and knowledge that will vanish at the end of the age. Help me to make Heaven through your love, in the name of Jesus.",
        },
        {
            scriptureText: "2 Corinthians 5:14 - For the love of Christ compels us, because we judge thus, that if one died for all, then all died.",
            prayerText: "I prophesy concerning my life, work, family and Nation today that the love of Christ shall decorate us, in the name of Jesus.",
        },
        {
            scriptureText: "Galatians 5:22a - But the fruit of the spirit is Love.",
            prayerText: "Love is a vital component of the fruit of the spirit; Holy Spirit, I desire greatly your love, that I may be able to exhibit the fullness of the fruit of your spirit, in the name of Jesus.",
        },
        {
            scriptureText: "Ephesians 5:25 - Husbands love your wives, just as Christ also loved the Church and gave Himself for her.",
            prayerText: "Lord Jesus, Help me to love my spouse as you love the Church, in the name of Jesus.",
        },
        {
            scriptureText: "Titus 2:14 - Who gave Himself for us that He might redeem us from every lawless deed, and purify for Himself His own special people, zealous for good works.",
            prayerText: "O Lord redeem my life, work, family, ministry from every mistake. Purify us to every good work, in the name of Jesus.",
        },
        {
            scriptureText: "1 Peter 4:8 - And above all things have fervent love for one another for love will cover a multitude of sins.",
            prayerText: "By your spirit O Lord, dominate my life with your love. Cleanse every sin in my life, children, work, Church and Nation in Jesus precious name.",
        },
        {
            scriptureText: "1 John 3:14 - We know that we have passed from death to life, because we love the brethren. He who does not love his brother abides in death.",
            prayerText: "O Lord my God, by the blood of Jesus, through the fire of the Holy Spirit, release my life from the bondage of untimely death. I recieve grace to love my brethren, in the name of Jesus.",
        },
        {
            scriptureText: "1 John 4:8 - He who does not love does not know God, for God is love.",
            prayerText: "O Lord, my God, you are love personified, I desire a resemblance with You, I confess today that I love You that I may know You more.",
        },
        {
            scriptureText: "Revelation 2:4 - Nevertheless, I have this against you, that you have left your first love.",
            prayerText: "I frustrate every evil ambition and wicked spirit that may want to take me away from your love and commandments, in the name of Jesus.",
        },
        {
            scriptureText: "Psalm 63:3 - Because your loving kindness is better than life, my lips shall praise you.",
            prayerText: "Your love and kindness in my life shall not be in vain, in the name of Jesus. There shall be a voice of thanksgiving, my lips continually will praise to you O Lord.",
        },
        {
            scriptureText: "Exodus 9:16 - But indeed for this purpose I have raised you up, that I may show my power in you, and that my name may be declared in all the earth.",
            prayerText: "I confess that God raised me up to show in me His power, that His name may be declared throughout all the earth, in the name of Jesus.",
        },
        {
            scriptureText: "Deuteronomy 8:18 - And you shall remember the Lord your God. For it is He who gives you power to get wealth, that He may establish His covenant which He swore to your fathers, as it is this day.",
            prayerText: "I remember the Lord my God, for it is He that giveth me power to get wealth this day, in the name of Jesus.",
        },
        {
            scriptureText: "Psalm 62: 11 - God has spoken once, twice I have heard this, that power belongs to God.",
            prayerText: "O power of God re-mould my life, in the name of Jesus.",
        },
        {
            scriptureText: "Psalm 63:2 - So I have looked for you in the sanctuary, to see your power and your glory.",
            prayerText: "Lord, reveal your glory unto me, in the name of Jesus.",
        },
        {
            scriptureText: "Psalm 68:35b - The God of Israel is He who gives strength and power to His people",
            prayerText: "I receive strength and power to do that which the Lord has commanded me today, in the name of Jesus.",
        },
        {
            scriptureText: "Psalm 110:3 - Your people shall be volunteers in the day of your power; in the beauties of holiness, from the womb of the",
            prayerText: "Compelling power of God to do His bidding fall upon me, in the name of Jesus.",
        },
        {
            scriptureText: "2 Corinthians 9:15 - Thanks be to God for His indescribable gift!",
            prayerText: "Compelling power of God to always do His bidding fall upon me, in the name of Jesus. Jeremiah 10:12a - He has made the earth by His power, He has established the world by His wisdom.",
        },
        {
            scriptureText: "Ezekiel 19:9 - And they put him in ward in chains, and brought him to the king of Babylon: they brought him into holds, that his voice should no more be heard upon the mountains of Israel.",
            prayerText: "Challenges of life that want to postpone my manifestation; I reject you, in the name of Jesus.",
        },

    ],
    June: [
        {
            scriptureText: "Jeremiah 10:12a - He has made the earth by His power, He has established the world by His wisdom.",
            prayerText: "Dear lord, renew my life with your power, in the name of Jesus."
        },
        {
            scriptureText: "Daniel 8:24a - His power shall be mighty, but not by his own power...",
            prayerText: "My God your power shall be mighty in every area of my life, in the name of Jesus."
        },
        {
            scriptureText: "Mathew 6:13b - And do not lead us into temptation, but deliver us from evil",
            prayerText: "Lord, hinder me from falling into temptation that will chase your glory away, in the name of Jesus."
        },
        {
            scriptureText: "1 Corinthians 1:19 - For it is written, I will destroy the wisdom of the wise, and will bring to nothing the understanding of the prudent.",
            prayerText: "Let the wisdom of the world be destroyed in my life and let the wisdom of God come alive in me, in the name of Jesus."
        },
        {
            scriptureText: "1 Corinthians 1:30 - But of him are ye in Christ Jesus, who of God is made unto us wisdom, and righteousness and sanctification, and redemption:",
            prayerText: "I receive the wisdom of God to excel today, in the name of Jesus."
        },
        {
            scriptureText: "Ephesians 1:17 - That the God of our Lord Jesus Christ, the father of glory, may give unto you the spirit of wisdom and revelation in the knowledge of him.",
            prayerText: "I receive the spirit of wisdom and revelation in the knowledge of God, in the name of Jesus"
        },
        {
            scriptureText: "Colossians 1:9 - For this cause we also, since the day we heard it, do not cease to pray for you, and to desire that ye might be filled with the knowledge of his will in all spiritual understanding.",
            prayerText: "That I might be filled with the knowledge of his will in all wisdom and spiritual understanding, in the name of Jesus. Today, I shall be guided by the wisdom and spiritual understanding of God, in the name of Jesus."
        },
        {
            scriptureText: "Colossians 3:16 - Let the word of Christ dwell in you richly in all wisdom, teaching and admonishing one another in Psalms and hymns and spiritual songs, singing with grace in your hearts to the Lord.",
            prayerText: "Lord, help me to meditate and retain your Word in my heart, in the name of Jesus."
        },
        {
            scriptureText: "Proverbs 2:21 - For the upright shall dwell in the land, and the perfect shall remain in it.",
            prayerText: "I receive the grace to live upright in God, in the name of Jesus."
        },
        {
            scriptureText: "Proverbs 7:25 - Let not thine heart decline to her ways, go no astray in her paths.",
            prayerText: "I apply my heart to know, to search and to seek for God’s wisdom, in the name of Jesus."
        },
        {
            scriptureText: "Isaiah 33:6 - And wisdom and knowledge shall be the stability of thy times, and strength of salvation: the fear of the Lord is his treasure.",
            prayerText: "Wisdom and the knowledge of God shall be the strength as I set out today, in the name of Jesus."
        },
        {
            scriptureText: "Daniel 2:20 - Daniel answered and said; Blessed be the name of God for ever and ever: for wisdom and might are his:",
            prayerText: "Lord, bless me today with the wisdom to unlock the benefits of today, in the name of Jesus."
        },
        {
            scriptureText: "Acts 7:10 - And delivered him out of his afflictions, and gave him favour and wisdom in the sight of Pharaoh king of Egypt; and he made him governor over Egypt and all his house.",
            prayerText: "Deliver me from afflictions and give me favour and wisdom before men, in the name of Jesus."
        },
        {
            scriptureText: "Romans 11:33 - O the depth of the riches both of the wisdom and knowledge of God! How unsearchable are his judgments, and his ways past finding out!",
            prayerText: "Wisdom and knowledge of God be revealed in my life and family, in the name of Jesus."
        },
        {
            scriptureText: "Job 39:17 - Because God hath deprived her wisdom neither hath he imparted to her understanding.",
            prayerText: "I would not be deprived of God's wisdom for a fruitful life, in the name of Jesus."
        },
        {
            scriptureText: "Psalm 49:3 - My mouth shall speak of wisdom; and the meditation of my heart shall be of understanding.",
            prayerText: "I reject foolishness in all my ways, in the name of Jesus."
        },
        {
            scriptureText: "Psalm 90:12 - So teach us to number our days, that we may apply our hearts unto wisdom.",
            prayerText: "I receive divine wisdom to conduct my life affairs today, in the name of Jesus."
        },
        {
            scriptureText: "Proverbs 2:2 - So that thou incline thine ear unto wisdom, and apply thine heart to understanding.",
            prayerText: "I would incline my ear unto wisdom and apply my heart to understanding, in the name of Jesus."
        },
        {
            scriptureText: "Proverbs 4:7 - Wisdom is the principal thing; therefore get wisdom: and with all thy getting get understanding.",
            prayerText: "Oh wisdom of God saturate my decision today, in the name of Jesus."
        },
        {
            scriptureText: "Proverbs 8:11 - For wisdom is better than rubies; and all the things that may be desired are not to be compared to it.",
            prayerText: "Above earthly things Oh Lord, I desire your wisdom, in the name of Jesus."
        },
        {
            scriptureText: "1 Kings 4:34 - And there came of all people to hear the wisdom of Solomon, from all kings of the earth, which had heard of his wisdom.Psalm 118:28 - You are my God, I will praise you; you",
            prayerText: "You my destiny manifest the wisdom of Heaven, in the name of Jesus."
        },
        {
            scriptureText: "1 Kings 10:8 - Happy are thy men; Happy are these thy servants, which stand continually before thee, and that hear thy wisdom.",
            prayerText: "Lord may I hear Your wisdom that I may be happy, in the name of Jesus."
        },
        {
            scriptureText: "1 Kings 10:24 - And all the earth sought Solomon to hear his wisdom, which God has put in his heart.",
            prayerText: "May I be sought after to hear God's wisdom through me, in the name of Jesus."
        },
        {
            scriptureText: "1 Kings 10:23 - So king Solomon exceeded all things of the earth for riches and for wisdom.",
            prayerText: "Spirit of excellence, saturate my life, in the name of Jesus."
        },
        {
            scriptureText: "Job 11:6 - And that he would shew thee the secrets of wisdom, that they are double to that which is? Know therefore that God exacteth of thee less than thine iniquity deserveth.",
            prayerText: "Lord show me the secrets of wisdom, in the name of Jesus."
        },
        {
            scriptureText: "Exodus 28:3 - So you shall speak to all who are gifted artisans, whom I have filled with the spirit of wisdom. That they may make Aaron's garments to consecrate him, that he may minister to me as priest.",
            prayerText: "Oh Lord fill me with the Spirit of wisdom that I may be wise hearted and be effective in my God given assignment, in the name of Jesus."
        },
        {
            scriptureText: "Deuteronomy 34:9 - Now Joshua the son of Nun was full of the Spirit of wisdom for Moses had laid his hands on him; so the children of Israel heeded him and did as the Lord had commanded Moses.",
            prayerText: "May I Oh Lord be full with the Spirit of wisdom, in the name of Jesus. Lord Jesus, lay your hands of wisdom upon me and let your wisdom flow through me today."
        },
        {
            scriptureText: "2 Samuel 14:20 - To bring out about this change of affairs your servant Joab has done this thing; but my Lord is wise according",
            prayerText: "Dear lord, grant me the wisdom to know all the things I need to know, in the name of Jesus."
        },
        {
            scriptureText: "1 Kings 3:28 - And all Isreal heard of the judgement which the king had rendered and they feared the king, for they said that wisdom of God was in him to administer justice.",
            prayerText: "Grant me your wisdom Oh God to do right judgment, in the name of Jesus."
        },
        {
            scriptureText: "1 Kings 4:29 - And God gave Solomon wisdom and exceedingly great understanding, and largeness of heart like the sand on the seashore.",
            prayerText: "God as you gave Solomon wisdom and understanding exceedingly much and largeness of heart, even as the sand that is on the seashore, give unto me, in the name of Jesus."
        },

    ],
    July: [
        {
            scriptureText: "1 Kings 4:30 - Thus Solomon's wisdom excelled the wisdom of all the men of the East and all the wisdom of Egypt.",
            prayerText: "I receive wisdom to excel in all my endeavors, in the name of Jesus."
        },
        {
            scriptureText: "Acts 9:31 - Then the churches throughout all Judea, Galilee and Samaria had peace and were edified. And walking in the fear of the Lord and in the comfort of the Holy Spirit, they were multiplied.",
            prayerText: "Oh Lord because I walk in the fear of the Lord and the comfort of the Holy Spirit the Lord would multiply me in every area of my life, in the name of Jesus."
        },
        {
            scriptureText: "Acts 13:2 - As they ministered to the Lord and fasted, the Holy Spirit said, now separate to me Barnabas and Saul for the work to which I have called them.",
            prayerText: "Holy Spirit I am separated unto you for the work were unto you have called me, in the name of Jesus."
        },
        {
            scriptureText: "Acts 15:8 - So God who knows the heart acknowledge them by given them the Holy Spirit, just as He did to us.",
            prayerText: "And God, which knows the hearts, have them witness giving them the Holy Spirit even unto me, in the name of Jesus."
        },
        {
            scriptureText: "Romans 5:5 - Now hope does not disappoint because the love of God has been poured out in our hearts by the Holy Spirit who was given to us.",
            prayerText: "The Love of God is shed abroad in my hearts by the Holy Spirit which is given unto me, in the name of Jesus."
        },
        {
            scriptureText: "Romans 15:29 - But I know that when I come to you, I shall come in the fullness of the blessing of the gospel of Christ.",
            prayerText: "God of hope fill me with all Joy and Peace that I may abound in hope, through the power of the Holy Spirit, in the name of Jesus."
        },
        {
            scriptureText: "Isaiah 63:11b - With the shepherd of his flock? Where is he who put his Holy Spirit within them.",
            prayerText: "The Lord God is He that put his Holy Spirit within me to show forth his glory, in the name of Jesus."
        },
        {
            scriptureText: "John 1:33 - I did not know him, but he who sent me to baptize with water said to me, upon whom you see the spirit descending, and remaining on him, this is He who baptizes with the Holy Spirit. And I have seen and testified that this is the son of God.",
            prayerText: "Holy Spirit descend and remain upon me and dwell within me, in the name of Jesus."
        },
        {
            scriptureText: "John 14:26 - But the Helper, the Holy Spirit, whom the father will send in my name. He will teach you all things and bring to your remembrance all things that I said to you.",
            prayerText: "Holy Spirit be my comforter, my teacher and bring all things to my remembrance, in the name of Jesus."
        },
        {
            scriptureText: "John 20:22 - And when He had said this, he breathed on them and said to them, receive the Holy Spirit.",
            prayerText: "My Lord and God breath upon me that I may receive the Holy Spirit, in the name of Jesus."
        },
        {
            scriptureText: "Acts 2:4 - And they were all filled with the Holy Spirit and began to speak with other tongues as the Spirit gave them utterance.",
            prayerText: "Holy Spirit grant me utterance as I begin to speak and pray in other tongues, in the name of Jesus."
        },
        {
            scriptureText: "Act 2:33 - Therefore being exalted to the right hand of God, and having received from the father the promise of Holy Spirit, He poured out this which you now see and hear.",
            prayerText: "Father I receive your promise of the Holy Spirit to direct and lead me today, in the name of Jesus."
        },
        {
            scriptureText: "Acts 2:38 - Then Peter said to them, Repent and let everyone of you be baptized, in the name of Jesus Christ for remission of sins; and you shall receive the gift of the Holy Spirit",
            prayerText: "Anything hindering the fullness of God's spirit in my life, I drop you, in the name of Jesus."
        },
        {
            scriptureText: "Luke 4:1 - Then Jesus been filled with the Holy Spirit, returned form the Jordan and was led by the Spirit into the wilderness.",
            prayerText: "In every wilderness, Lord by your wisdom lead me out, in the name of Jesus."
        },
        {
            scriptureText: "Luke 10:21 - In that hour Jesus rejoiced in the Spirit and said, 'I thank you father Lord of heaven and earth, that you have hidden these things from the wise and prudent and revealed them to babes. Even so, father, for so it seemed good in your sight.",
            prayerText: "Oh Lord my God reveal things that would change my life for good unto me, in the name of Jesus."
        },
        {
            scriptureText: "Luke 12:12 - For the Holy Spirit will teach you in that very hour what you ought to say?",
            prayerText: "Holy Spirit teach me everyday, every hour, every minute, every and every second of my life what to say and to do, in the name of Jesus."
        },
        {
            scriptureText: "Psalm 51:11- Do not cast me away from your presence, And do not take your Holy Spirit from me.",
            prayerText: "Cast me not away from thy presence, Oh Lord and take not thy Holy Spirit from me, in the name of Jesus."
        },
        {
            scriptureText: "Mathew 3:11b - I indeed baptized you with water unto repentance, but He who is coming after me is mightier than I, He will baptize you with the Holy Spirit and fire.",
            prayerText: "Lord baptize me with Holy Spirit and fire, in the name of Jesus."
        },
        {
            scriptureText: "Mark 13:36 - Lest coming suddenly, he find you sleeping...",
            prayerText: "Father, may the Lord not find me sleeping when He comes, in the name of Jesus."
        },
        {
            scriptureText: "Luke 1:15b - He will also be filled with the Holy Spirit, even from his mother's womb.",
            prayerText: "Oh Lord fill me up with your Holy Spirit, in the name of Jesus."
        },
        {
            scriptureText: "Luke 1:35 - And the angel answered and said to her, the Holy Spirit will come upon you, and the power of the Highest will overshadow you; therefore, also that Holy one who is to be born will be called the son of God.",
            prayerText: "Power of the most High, overshadow my life, in the name of Jesus."
        },
        {
            scriptureText: "Luke 2:25 - And behold, there was a man in Jerusalem whose name was Simeon, and this man was just and devout waiting for the consolation of Israel, and the Holy Spirit was upon him.",
            prayerText: "Today, I shall enjoy the comfort of the Holy Spirit, in the name of Jesus."
        },
        {
            scriptureText: "Acts 5:3 - But Peter said, Ananias why has Satan filled your heart to lie to the Holy Spirit and keep back part of the price of the land for yourself?",
            prayerText: "Oh Lord I would never lie to the Holy Spirit, in the name of Jesus."
        },
        {
            scriptureText: "Acts 5:32 - And we are his witnesses to these things and so also is the Holy Spirit whom God has given to those who obey Him”",
            prayerText: "Lord let me be a witness for you as the Holy Spirit is, in the name of Jesus."
        },
        {
            scriptureText: "Acts 7:51 - You stiff-necked and uncircumcised in heart and ears! You always resist the Holy Spirit; as your fathers did, so do you.",
            prayerText: "Holy Spirit let me not resist you in my life, in the name of Jesus."
        },
        {
            scriptureText: "Acts 8:15 - Who when they had come down, prayed for them that they might receive the Holy Spirit.",
            prayerText: "I pray Oh Lord that every member of my family would receive the Holy Spirit, in the name of Jesus."
        },
        {
            scriptureText: "Mark 14:64 - Ye have heard the blasphemy: what think ye? And they all condemned him to be guilty of death.",
            prayerText: "Let the right hand of the power of God be revealed in my family against the wrath of the adversaries, in the name of Jesus."
        },
        {
            scriptureText: "Romans 16:25 - Now to him that is of power to establish you according to my gospel, and the preaching of Jesus Christ, according to the revelation of the mystery/which was kept secret since the world began.",
            prayerText: "Lord let your power establish me and my household according to your gospel, in the name of Jesus."
        },
        {
            scriptureText: "Ephesians 1:19 - And what is the exceeding greatness of his power to us-ward who believe, according to the working of his mighty power.",
            prayerText: "My God let the exceeding greatness of your mighty working power deliver me from any yoke of bondage, in the name of Jesus."
        },
        {
            scriptureText: "Romans 1:8 - First, I thank my God through Jesus Christ for you all, that your faith is spoken of throughout the whole world.",
            prayerText: "My Lord and God, empower me to be a witness for you, in the name of Jesus."
        },
        {
            scriptureText: "Mathew 22:29 - Jesus answered and said to them, “You are mistaken, not knowing the scriptures nor the power of God.",
            prayerText: "Lord I want to know your power in my life, in the name of Jesus."
        }
    ],
    August: [
        {
            scriptureText: "1 Corinthians 1:18 - For the message of the cross is foolishness to those who are perishing, but to us who are being saved it is the power of God.",
            prayerText: "The power of God shall be made manifest in my life, in the name of Jesus."
        },
        {
            scriptureText: "1 Corinthians 1:24 - But to those who are called, both Jews and Greeks, Christ the power of God and the wisdom of God.",
            prayerText: "Christ the power of God appear in every areas of my life, in the name of Jesus."
        },
        {
            scriptureText: "Mathew 9:6 - But that ye may know that the son of man hath power on earth to forgive sins (then saith he to the sick of palscy arise, take up they bed, and go unto thine house.",
            prayerText: "The son of man has power on earth to forgive sin and my sins are forgiven praise God, in the name of Jesus."
        },
        {
            scriptureText: "Luke 5:17 - And it came to pass on a certain day, as he was teaching, that there were pharises and doctors of the law sitting by, which were come of every town of Galilee, and Judea, and Jerusalem; and the power of the Lord was present to heal them.",
            prayerText: "The healing power of God, fall upon every area of my life, in the name of Jesus."
        },
        {
            scriptureText: "Romans 1:16 - For I am not ashamed of the Gospel of Christ for it is the power of God unto salvation to every one that believeth; to the Jew first; and also to the Greek.",
            prayerText: "I receive Grace to proclaim the gospel of Christ and I shall not be ashamed, in the name of Jesus."
        },
        {
            scriptureText: "Romans 9:22 - What if God, willing to shew his wrath, and to make his power known, endured with much long suffering the vessels of wrath filled to destruction.",
            prayerText: "Lord make your power known in every situation am facing right now, in the name of Jesus."
        },
        {
            scriptureText: "Philippians 3:10 - That I may know him, and the power of resurrection, and the fellowship of his sufferings being made conformable unto his death.",
            prayerText: "By the power of resurrection, every good part of my life that is dead come alive, in the name of Jesus."
        },
        {
            scriptureText: "2 Peter 1:3 - According as his divine power hath given unto us all things that pertain unto life and godliness, through the knowledge of him that hath called us to glory and virtue.",
            prayerText: "Lord, take me deeper in knowing you that I may enjoy all settled calvary benefits, in the name of Jesus."
        },
        {
            scriptureText: "Colossians 1:11- Strengthened with all might, according to his glorious power, unto all patience and longsuffering with joyfulness.",
            prayerText: "I refused to give up so that my joy shall be full, in the name of Jesus."
        },
        {
            scriptureText: "2 Corinthians 4:7 - But we have this treasure in earthen vessels, that the excellency of the power may be of God, and not of us.",
            prayerText: "Lord, thank you for the Heavenly treasure deposited in me, in the name of Jesus."
        },
        {
            scriptureText: "Genesis 1:2 - And the Earth was without form and void; and darkness was upon the face of the deep. And the Spirit of God moved upon the face of the waters.",
            prayerText: "I dispel and scatter every form of darkness embedded in my foundation by wind of fire, in the name of Jesus."
        },
        {
            scriptureText: "Genesis 1:31a - And God saw everything that he had made, and, behold it was very good...",
            prayerText: "Every power that is working against my efficiency in Life, receive the judgment of God, in the name of Jesus."
        },
        {
            scriptureText: "Psalm 1:1- Blessed is the man that walketh not in the counsel of the ungodly, nor standeth in the way of sinners, nor sitteth in the seat of the scornful.",
            prayerText: "In the name of Jesus, I will not walk in the counsel of the ungodly men nor stand in the way of sinners."
        },
        {
            scriptureText: "Psalm 1:2 - But his delight is in the Law of the Lord, and in his law doth he meditate day and night.",
            prayerText: "I anchor my spirit man to the law of God, in the name of Jesus."
        },
        {
            scriptureText: "Psalm 2:1 - Why do the heathen rage and the people imagine a vain thing?",
            prayerText: "The rage and the imagination of evil spirits against my life, fall to the ground and die, in the name of Jesus."
        },
        {
            scriptureText: "Psalm 2:5 - Then shall he speak unto them in his wrath, and vex them in his sure displeasure.",
            prayerText: "Every power working against my closeness to my God, Oh Lord arise and rebuke them in your sure displeasure,, in the name of Jesus."
        },
        {
            scriptureText: "Psalm 4:2 - O ye sons of men, how long will ye turn my glory into shame? How long will ye love vanity, and seek after leasing?",
            prayerText: "Hiding place of vanity in my heart, be desolate, in the name of Jesus."
        },
        {
            scriptureText: "Psalm 5:12 - For thou, Lord wilt bless the righteous; with favour wilt thou compass him as with a shield.",
            prayerText: "My father compass me about with your favour, in the name of Jesus."
        },
        {
            scriptureText: "Psalm 8:1 - O Lord our Lord, how excellent is thy name in all the Earth! Who hast set thy glory above the heavens.",
            prayerText: "Oh Lord, let your excellence be evident in every area of my life,, in the name of Jesus."
        },
        {
            scriptureText: "Psalm 18:44 - As soon as they hear of me, they shall obey me; the strangers shall submit themselves unto me.",
            prayerText: "Every strangers living inside my body, receive the arrows of God, in the name of Jesus."
        },
        {
            scriptureText: "Psalm 18:45 - The strangers shall fade away, and be afraid and come of their close places.",
            prayerText: "I command every stranger in my life to fade away in name Jesus."
        },
        {
            scriptureText: "Psalm 19:1 - The heavens declare the glory of God; and the firmament shewth his handywork.",
            prayerText: "Oh heavens speak out the will and purpose of God concerning my life,, in the name of Jesus."
        },
        {
            scriptureText: "Psalm 19:14 - Let the words of my mouth, and the meditation of my heart be acceptable in thy sight, O Lord my strength and my redeemer.",
            prayerText: "My heart think acceptable thoughts, my mouth speak acceptable words, in the name of Jesus."
        },
        {
            scriptureText: "Psalm 21:3 - For thou preventest him with the blessings of goodness; thou bettest a crown of pure gold on his head.",
            prayerText: "Blessings of God pursue me and overtake me, in the name of Jesus."
        },
        {
            scriptureText: "Psalm 21:3 - For thou preventest him with the blessings of goodness; thou settest a crown of pure gold on his head.",
            prayerText: "My father, decorate my head with the crown of pure gold, in the name of Jesus."
        },
        {
            scriptureText: "Psalm 23:2 - He maketh me to lie down in green pastures; he leadeth me beside still waters.",
            prayerText: "The green pastures allocated to my destiny, Lord let me begin to enjoy them, in the name of Jesus."
        },
        {
            scriptureText: "Psalm 23:5 - Thou preparest a table before me in the presence of mine enemies; thou annointest my head with oil, my cup runneth over.",
            prayerText: "As from this day, I begin to eat from the table prepared for me by the Lord, in the name of Jesus."
        },
        {
            scriptureText: "Psalm 23:6 - Surely goodness and mercy shall follow me all the days of my life; and I will dwell in the house of the Lord forever.",
            prayerText: "Goodness and mercy, pursue me and overtake me, in the name of Jesus."
        },
        {
            scriptureText: "Psalm 24:7 - Lift up your heads, O ye gates and be ye lift up, ye everlasting doors; and the king of glory shall come in.",
            prayerText: "Every evil gates and doors that are working against my glory, I pull you down, in the name of Jesus."
        },
        {
            scriptureText: "Psalm 25:14 - The secret of the Lord is with them that fear him, and he will shew them is covenant.",
            prayerText: "Oh Lord engrave your fear into my spirit man and cause me to know secret things concerning your works, in the name of Jesus."
        },
        {
            scriptureText: "Genesis 8:1 - And God remembered Noah, and every living thing, and all the cattle that was with him in the ark.",
            prayerText: "Lord! Remember me for good this month and forever, in the name of Jesus."
        },
    ],
    September: [
        {
            scriptureText: "Genesis 8:15 - And God spake unto Noah, saying Go forth of the ark.",
            prayerText: "Oh Lord, speak your word to direct me, in the name of Jesus."
        },
        {
            scriptureText: "Genesis 9:7 - And you be ye fruitful and multiply; bring forth abundantly in the earth and multiply there in.",
            prayerText: "Multiply me oh Lord and cause me to be fruitful round about, in the name of Jesus."
        },
        {
            scriptureText: "Genesis 11:6 - Go to, let us go down, and there confound their language, that may not understand one another speech.",
            prayerText: "Every evil unity against my life, be scattered unto desolation, in the name of Jesus."
        },
        {
            scriptureText: "Genesis 12:1 - Now the Lord said unto Abram, get thee out of thy country, and from thy kindred, and from they father's house, unto a land that I will shew thee:",
            prayerText: "Lord by your power, take me from where I am to where you want me to be, in the name of Jesus."
        },
        {
            scriptureText: "Psalm 91:1 - He that dwelleth in the secret place of the most high shall abide under the shadow of the Almighty.",
            prayerText: "Hide me Oh Lord and keep me in your secret places, in the name of Jesus."
        },
        {
            scriptureText: "Psalm 119:18 - Open thou mine eyes, that I may behold wondrous things out of your land.",
            prayerText: "Open my eyes to the life of your words, in the name of Jesus."
        },
        {
            scriptureText: "Psalm 125:3a - For the rod of the wicked shall not rest upon the lot of the righteous.",
            prayerText: "The rod of the wicked will not fall upon my life, home, finance, children, in the name of Jesus."
        },
        {
            scriptureText: "Exodus 23:20 - Behold I send an angel before thee, to keep thee in the way, and to bring thee into the place which I have prepared for thee.",
            prayerText: "Keep me in your way and take me to the place you have prepared for me, in the name of Jesus Christ our Lord and Saviour."
        },
        {
            scriptureText: "Job 5:26 - Thou shalt come to thy grave in a full age, like a shock of corn cometh in his season.",
            prayerText: "I shall not die before my time, in the name of Jesus."
        },
        {
            scriptureText: "Job 28:3a - He setteth an end to darkness, and searcheth out all perfection.",
            prayerText: "Put an end O Lord to everything that translate to darkness in my life, in the name of Jesus."
        },
        {
            scriptureText: "Genesis 37:18 - Now they saw him afar off, even before he came near them, they conspired against him to kill him.",
            prayerText: "Every conspiracy to terminate my life be scattered, in the name of Jesus."
        },
        {
            scriptureText: "Genesis 37:20 - “Come therefore, let us now kill him and cast him into some pit; and we shall say, 'some wild beast has devoured him. We shall see what will become of his dreams.",
            prayerText: "Every calculated attempt to kill my dream/vision, be aborted, in the name of Jesus."
        },
        {
            scriptureText: "Genesis 37:21 - But Reuben heard it and he delivered him out of their hands, and said, “Let us not kill him.”",
            prayerText: "Deliver me Oh Lord from the hands of the wicked, in the name of Jesus."
        },
        {
            scriptureText: "Genesis 37:27 - “Come Let us sell him to the Ishmaelites, and Let not our hand be upon him, for he is our brother and our flesh.” And his brothers instead.",
            prayerText: "Let my enemies make mistakes that will move me forward, in the name of Jesus."
        },
        {
            scriptureText: "Genesis 37:36 - Now the Midianites had sold him in Egypt to Portiphar, an officer of Pharaoh and captain of the Guard.",
            prayerText: "By your arrangement Oh Lord, take me to where you want me to be, in the name of Jesus."
        },
        {
            scriptureText: "Job 32:8 - But there is a spirit in man and the inspiration of the Almighty giveth him understanding.",
            prayerText: "By your spirit Oh Lord, open me up to understanding, in the name of Jesus."
        },
        {
            scriptureText: "Job 34:21 - For his eyes are upon the ways of man, and he seeth all his goings.",
            prayerText: "Keep my goings before you and keep my ways, in the name of Jesus."
        },
        {
            scriptureText: "Psalm 7:9 - Oh let the wickedness of the wicked come to an end",
            prayerText: "Every wickedness in and around my life come to an end, in the name of Jesus."
        },
        {
            scriptureText: "Psalm 17:5 - Hold up my goings in thy path that my footsteps slip not. ",
            prayerText: "Uphold me Oh Lord that I might not fail you, in the name of Jesus."
        },
        {
            scriptureText: "Psalm 18:32 - It is God that giveth me with strength and maketh my way perfect.",
            prayerText: "Strengthen me Oh God by your power and perfect my ways, in the name of Jesus."
        },
        {
            scriptureText: "Genesis 1:2 - And the earth was without form and void; and darkness was upon the face of the deep.",
            prayerText: "Every void in my life, fill it Oh Lord by your power, in the name of Jesus."
        },
        {
            scriptureText: "Genesis 1:3 - And God said, Let there be light; and there was light.",
            prayerText: "Let your light shine unto all the dark areas of my life, in the name of Jesus."
        },
        {
            scriptureText: "Ezekiah 37:5 - Thus said the Lord God unto these bones; Behold, I will cause breath to enter into you, and ye shall live.",
            prayerText: "Every dryness in my life, speak life into it Oh Lord, in the name of Jesus."
        },
        {
            scriptureText: "Psalm 16:1 - Preserve me O God: for in thee do I put my trust.",
            prayerText: "Keep me O Lord, for I trust in you; let me not be put to shame, in the name of Jesus."
        },
        {
            scriptureText: "Psalm 18:16 - He sent me from above, he took me, he drew me out of many waters.",
            prayerText: "Deliver me Oh Lord from my adversaries, in the name of Jesus."
        },
        {
            scriptureText: "Psalm 18:28 - For thou wilt light my candle: the Lord my God will enlighten my darkness.",
            prayerText: "Everything that represents darkness in my life, be lightened by the light of God, in the name of Jesus."
        },
        {
            scriptureText: "Genesis 12:2 - And I will make of thee a great Nation; and I will bless thee; and make thy name great; and thou shalt be a blessing.",
            prayerText: "That which you want to do with my life do it oh Lord; in your great mercy, in the name of Jesus."
        },
        {
            scriptureText: "Genesis 13:9a - Is not the whole land before thee? Separate thyself, I pray thee, from me:",
            prayerText: "Every evil attachment journeying with me, be separated by the finger of God, in the name of Jesus."
        },
        {
            scriptureText: "Genesis 16:2 - And Sarai said unto Abram, behold now, the Lord hath restrained me from bearing I pray thee, go in unto my maid it may be that I may obtain children by her. And Abram harkened to the voice of Sarai.",
            prayerText: "The mistakes that will cause me sorrow, Lord help me not to make them, in the name of Jesus."
        },
        {
            scriptureText: "Psalm 1:1 - Blessed is the man that walketh not in the counsel of the ungodly, nor standeth in the way of the sinners nor sitteth in the seat of the scornful.",
            prayerText: "Lord help me to continually keep your statutes and walk in your way, in the name of Jesus."
        },
    ],
    October: [
        {
            scriptureText: "Psalm 122:6 - Pray for the peace of Jerusalem: they shall prosper that love thee.",
            prayerText: "O, Lord! bless Nigeria. Let not the will of the evil ones prevail over her, in the name of Jesus."
        },
        {
            scriptureText: "Proverbs 20:12 - The hearing ear, the seeing eye, the Lord hath made even both of them.",
            prayerText: "Open my ears that I may hear you, open my eyes that I may see your ways, in the name of Jesus."
        },
        {
            scriptureText: "Isaiah 40:4 - Every valley shall be exalted and every mountain and hill shall be made low; and the crooked shall be made straight, and the rough placed plain.",
            prayerText: "Every low places in my life be lifted, all the crooked places be smoothe, in the name of Jesus."
        },
        {
            scriptureText: "Joel 2:29 - And also upon the servants and upon the handmaids in those days will I pour out my spirit.",
            prayerText: "Pour your spirit upon me afresh Oh Lord, in the name of Jesus."
        },
        {
            scriptureText: "Malachi 3:3 - And he shall sit as a refiner and purifier of silver and he shall purify the sons of Levi, and purge them as gold and silver, that they may offer unto the Lord an offering in righteousness.",
            prayerText: "Purge me Oh Lord of every impurity and refine me for your service, in the name of Jesus."
        },
        {
            scriptureText: "Matthew 10:22 - And ye shall be hated of all men for my names sake: but he that endureth to the end shall be saved.",
            prayerText: "Uphold me to the very end even in face of adversity, in the name of Jesus."
        },
        {
            scriptureText: "Psalm 33:10 - The Lord bringeth the counsel of the heathen to nought: he maketh the devices of the people of none effect.",
            prayerText: "Every counsel of the wicked concerning my life be brought to nought, in the name of Jesus."
        },
        {
            scriptureText: "Psalm 33:11 - The counsel of the Lord standeth for ever, the thoughts of his heart to all generations.",
            prayerText: "Let only your counsel for my life stand, in the name of Jesus."
        },
        {
            scriptureText: "Psalm 33:22 - Let they mercy, O Lord, be upon us, according as we hope in thee.",
            prayerText: "Let your mercy be upon us continually O Lord, in the name of Jesus."
        },
        {
            scriptureText: "Matthew 19:26 - But Jesus beheld them, and said unto them, with me this is impossible but with God all things are possible.",
            prayerText: "Make the impossible possible in my Life Oh Lord, in the name of Jesus."
        },
        {
            scriptureText: "Luke 12:2 - For there is nothing covered that shall not be revealed; neither hid, that shall not be known.",
            prayerText: "By the power of revelation, everything hidden in my life be uncovered, in the name of Jesus."
        },
        {
            scriptureText: "Psalm 112:3 - Wealth and riches will be in his house, and his righteousness endures forever.",
            prayerText: "Oh Lord, may the wealth and riches of heaven never depart from my family because your righteousness endures forever, in the name of Jesus."
        },
        {
            scriptureText: "Psalm 119:40 - Behold I long for your precepts; revive me in your righteousness.",
            prayerText: "Lord, increase my appetite for your precepts (command) and revive me, in the name of Jesus."
        },
        {
            scriptureText: "Psalm 119:11 - Your word I have hidden in my heart, that I might not sin against you.",
            prayerText: "Oh Lord give me the desire to hide your word in my heart that I might not sin against you, in the name of Jesus."
        },
        {
            scriptureText: "Psalm 1:2 - But his delight is in the Law of the Lord, and in this law he meditate day and night.",
            prayerText: "Because I delight in your law Oh Lord my God give me the appetite to meditate upon your law day and night, in the name of Jesus."
        },
        {
            scriptureText: "Psalm 2:1 - Why do the nations rage. And the people plot a vain thing?",
            prayerText: "My Lord this our nation shall not rage and every evil vain plot against our nation be cancelled, in the name of Jesus."
        },
        {
            scriptureText: "Psalm 7:9 - Oh Let the wickedness of the wicked come to an end. But establish the just; for the righteous God tests the hearts and minds.",
            prayerText: "Let every wickedness of the wicked over my family, and nation come to an end, in the name of Jesus."
        },
        {
            scriptureText: "Psalm 17:5 - Uphold my steps in your paths, that my footsteps may not slip.",
            prayerText: "Lord uphold my marital, financial, emotional, family, children steps, in the name of Jesus."
        },
        {
            scriptureText: "Proverbs 30:5 - Every word of God is pure, He is a shield to those who put their trust in Him.",
            prayerText: "Father Lord, let your pure word shield my family and the works of my hands, in the name of Jesus."
        },
        {
            scriptureText: "Isaiah 1:18 - If you are willing and obedient, you shall eat the good of the land.",
            prayerText: "Lord am willing, let my appetite to obey your word in me increase so that I may eat the good of the land, in the name of Jesus."
        },
        {
            scriptureText: "Luke 11:3 - Give us day by day our daily bread",
            prayerText: "Let not your daily provisions cease over me and my family, in the name of Jesus."
        },
        {
            scriptureText: "Psalm 127:3 - Behold children are heritage from the Lord, the fruit of the womb is a reward.",
            prayerText: "Thank you Lord for gifting me with prophetic children, in the name of Jesus."
        },
        {
            scriptureText: "1 Corinthians 13:12 - And now abide faith, hope, love, these three; but the greatest of these is love.",
            prayerText: "Help me Lord to walk in pure love everyday of my life, in the name of Jesus."
        },
        {
            scriptureText: "Genesis 24:1 - Now Abraham was old well advanced; and the Lord had blessed Abraham in all things.",
            prayerText: "O Lord your blessings shall continually be the heritage for me and my household, in the name of Jesus."
        },
        {
            scriptureText: "Deuteronomy 7:14 - You shall be blessed above all peoples: there shall not be a male or female barren among you or among your livestock.",
            prayerText: "Thank you Lord for your blessings upon my family, in the name of Jesus."
        },
        {
            scriptureText: "Deuteronomy 12:7 - And there you shall eat before the Lord your God and you shall rejoice in all to which you have put your hands, you and your households, in which the Lord your God has blessed you.",
            prayerText: "Oh Lord may your blessings upon the works of my hands last forever, in the name of Jesus."
        },
        {
            scriptureText: "Deuteronomy 28:2 - And all these blessings shall come upon you and overtake you, because you obey the voice of the Lord your God.",
            prayerText: "O Lord by the power of your Spirit help me and my household to obey your voice at all times, so that your blessings would remain permanent in our family, in the name of Jesus."
        },
        {
            scriptureText: "Genesis 1:28 - Then God blessed them, and God said to them, be fruitful and multiply: fill the earth and subdue it, have dominion over the fish of the sea, over the birds of the air, and over every living thing that moves on the earth.",
            prayerText: "My God I thank you for you Lord have blessed my family, I pray that every member of mine family will be fruitful, multiply, subdue and have dominion in every area of our lives, in the name of Jesus."
        },
        {
            scriptureText: "Genesis 1:22 - And God blessed them, saying, be fruitful and multiply, and fill the waters in the seas and let birds multiply on the earth.",
            prayerText: "Lord let every unfruitfulness and stagnation vanish from my household, in the name of Jesus."
        },
        {
            scriptureText: "Hebrew 13:4 - Marriage is honourable among all, and the bed undefiled, but fornicators and adulterers God will judge.",
            prayerText: "My father may I not defile the bed of my marriage but instead honour and nuture it, and help me Oh Lord to departing from fornication and adultery, in the name of Jesus."
        },
        {
            scriptureText: "Psalm 110:1 - The Lord said to my lord, sit at my right hand, till I make your enemies your footstool.",
            prayerText: "Lord I depend on you to make every enemy of my household our footstool, in the name of Jesus."
        },
    ],
    November: [
        {
            scriptureText: "Galatians 6:17 - From henceforth let no man trouble me for I bear on my body the mark of our Lord Jesus Christ.",
            prayerText: "Oh Lord I decree that your mark will for ever distinguish me amongst my enemies, in the name of Jesus."
        },
        {
            scriptureText: "Isaiah 50:7-9 - They all shall wax old as garment; the moth shall eat them u",
            prayerText: "In the name of Jesus let all my enemies wax old as garment and be eaten up by moth in the mighty name of Jesus."
        },
        {
            scriptureText: "Psalm 118:17 - I shall not die, but live to declare the glory of the Lord",
            prayerText: "I will not die before my Glory appears, in the name of Jesus."
        },
        {
            scriptureText: "Proverbs 3:24 - When thou liest down, thou shall not be afraid; yea, thou shall lie down and thy sleep shall be sweet.",
            prayerText: "As I lay down to sleep keep me Oh Lord, in the name of Jesus."
        },
        {
            scriptureText: "Psalm 65:4 - Blessed is the man whom thou choosest, and causeth to approach unto thee, that he may dwell in thy courts: we shall be satisfied with the goodness of thy house, even of they Holy temple.",
            prayerText: "Satisfy me Oh Lord with the goodness of thy house, in the name of Jesus."
        },
        {
            scriptureText: "Philippians 4:19 - But my God shall abundantly supply all your need according to his riches in Glory by Christ Jesus.",
            prayerText: "I shall not lack any good thing, in the name of Jesus."
        },
        {
            scriptureText: "Psalm 40:2 - He also brought me up out of a horrible pit, out of the miry clay, and set my feet upon a rock, And established my steps.",
            prayerText: "By your manifold wisdom, fill my life with riches roundabout, in the name of Jesus."
        },
        {
            scriptureText: "Psalm 81:10b - Open thy mouth wide, and I will fill it.",
            prayerText: "Let me not be short of words in and out of seasons, in the name of Jesus."
        },
        {
            scriptureText: "Romans 15:13 - Now the God of hope fill you with all joy and peace in believing, that ye may abound in hope, through the power of the Holy Ghost.",
            prayerText: "By the power of your Spirit O God fill my life with abundant joy, hope and peace, in the name of Jesus."
        },
        {
            scriptureText: "Isaiah 41:10 - Fear thou not; for I am with thee: be not dismayed; for I am thy God: I will strengthen thee; yea, I will help thee; yea, I will uphold thee with the right hand of my righteousness.",
            prayerText: "I will not be afraid for God is with me, I will not be dismayed for He is my strength, He will also help me and establish me, in the name of Jesus.",
        },
        {
            scriptureText: "Ephesians 4:30 - And grieve not the Holy Spirit of God whereby ye are sealed unto the day of redemption.",
            prayerText: "Empower me Oh Lord not to grieve you so that my life can be preserved, in the name of Jesus.",
        },
        {
            scriptureText: "Psalm 50:15 - And call upon me in the day of trouble: I will deliver thee and thou shall glorify me.",
            prayerText: "Deliver me Oh Lord in the day of trouble to the glory of your name, Christ Jesus.",
        },
        {
            scriptureText: "Deut. 8:7 - For the Lord thy God bringeth thee unto a good land, a land of brooks of water, of fountains and depths that spring out of valleys and hills.",
            prayerText: "Take me Oh Lord unto my rest, to refresh my soul, in the name of Jesus.",
        },
        {
            scriptureText: "Isaiah 42:1 - Behold my servant, whom I uphold; mine elect, in whom my soul delighteth; I have put my Spirit upon him: he shall bring forth judgment to the Gentiles.",
            prayerText: "Uphold me Oh Lord - Put your Spirit upon me afresh to execute judgment, in the name of Jesus.",
        },
        {
            scriptureText: "Psalm 18:37 - I have pursued my enemies, and overtaken them: neither did I turn again till they were consumed.",
            prayerText: "Empower me to pursue, overtake and consume all my enemies, in the name of Jesus.",
        },
        {
            scriptureText: "Jeremiah 32:27 - Behold, I am the Lord, the God of all flesh: is there anything too hard for me?",
            prayerText: "Oh Lord, I acknowledge you as my Lord and saviour and I know that my case is not hard for you to do, in the name of Jesus.",
        },
        {
            scriptureText: "Psalm 37:23 - The steps of a good man are ordered by the Lord: and he delipteth in his way.",
            prayerText: "Today Oh Lord as I go out and always, order my steps aright, in the name of Jesus.",
        },
        {
            scriptureText: "Isaiah 50:7 - For the Lord God will help me; therefore shall I not be confounded: therefore have I set my face like a flint, and I know that I shall not be ashamed.",
            prayerText: "Lord help me that I may not be put to shame, in the name of Jesus.",
        },
        {
            scriptureText: "Psalm 61:4 - I will abide in thy tabernacle for ever: I will trust in the cover of thy wings.",
            prayerText: "Preserve me Oh Lord for my trust is in you, in the name of Jesus.",
        },
        {
            scriptureText: "Psalm 96:6 - Honour and majesty are before him: strength and beauty are in his sanctuary.",
            prayerText: "Clothe me with beauty and honour by the power of your majesty, in the name of Jesus.",
        },
        {
            scriptureText: "Acts 18:10 - For I am with thee, and no man shall set on thee to hurt thee: for I have much people in this city.",
            prayerText: "Encampeth around my life Oh Lord that no man might hurt me, in the name of Jesus.",
        },
        {
            scriptureText: "Psalm 51:12 - Restore unto me the Joy of thy salvation and uphold me with thy free Spirit.",
            prayerText: "Revive my Spirit and let the Joy of your salvation be restored unto me,, in the name of Jesus.",
        },
        {
            scriptureText: "Psalm 24:7 - Lift up your heads, O ye gates and be lifted up, ye everlasting door; and the king of Glory shall come in.",
            prayerText: "Every ancient door of poverty, lack, delay, stagnancy in my life and around me be lifted up, in the name of Jesus.",
        },
        {
            scriptureText: "Psalm 22:5 - They cried unto thee, and were delivered: they trusted in thee and were not confounded.",
            prayerText: "As I lift up my voice to call upon you Oh Lord, deliver me and let me not be put to shame, in the name of Jesus.",
        },
        {
            scriptureText: "Psalm 21:2 - Thou hast given him his hearts' desire and hast not withholden the request of his lips.",
            prayerText: "Grant my desires Oh Lord and let not my request be withholding, in the name of Jesus.",
        },
        {
            scriptureText: "Psalm 22:26 - The meet shall eat and be satisfied: they shall praise the Lord that seek him: your heart shall live forever.",
            prayerText: "Satisfy me Oh Lord that your praise may continually be in my mouth.",
        },
        {
            scriptureText: "Psalm 29:11 - The Lord will give strength unto his people; the Lord will bless his people with Peace.",
            prayerText: "Strengthen me Oh Lord by your power. Bless me with your peace, in the name of Jesus.",
        },
        {
            scriptureText: "Psalm 30:10 - Hear, O Lord, and have mercy upon me: Lord, be thou my helper.",
            prayerText: "Have mercy upon me and help me out of every difficult situations, in the name of Jesus.",
        },
        {
            scriptureText: "Psalm 43:3 - O send out they light and thy truth: let them lead me; let them bring me unto thy Holy hill, and to thy tabernacle.",
            prayerText: "Let the light of your word be my guide, lead me into righteousness, in the name of Jesus.",
        },
        {
            scriptureText: "Psalm 49:15 - But God will redeem my soul from the Power of the Grave: for he shall receive me.",
            prayerText: "Redeem my soul, marriage, health, children from the Power of the Grace, in the name of Jesus.",
        },
    ],
    December: [
        {
            scriptureText: "Isaiah 22:25 - In that day, saith the Lord of hosts, shall the nail that is fastened in the place be removed, and be cut down and fall; and the burden that was upon it shall be cut off: for the Lord hath spoken it.",
            prayerText: "Every unpleasant situation, oppressing my life, cutting short my testimonies be cut down by the authority of heaven, in the name of Jesus."
        },
        {
            scriptureText: "Isaiah 27:6 - He shall cause them that come of Jacob to take root: Israel shall blossom and bud, and fill the face of the world with fruit.",
            prayerText: "Oh Lord, I depend on you, root me down and make me blossom, in the name of Jesus."
        },
        {
            scriptureText: "Isaiah 28:18 - And your covenant with death shall be disannulled, and your agreement with hell shall not stand; when the overflowing scourge shall pass through the ye shall be trodden down by it.",
            prayerText: "Every covenant of death operating in my life, family, finance be broken, in the name of Jesus."
        },
        {
            scriptureText: "Isaiah 34:16 - Seek ye out of the book of the Lord, and read: no one of these shall fall, none shall want her mate, for my mouth it hath commanded and his spirit it hath gathered them.",
            prayerText: "Every of your word spoken concerning my destiny be quickened by your spirit, in the name of Jesus."
        },
        {
            scriptureText: "Isaiah 54:3 - For thou shall break forth on the right hand and on the left, and thy seed shall inherit the Gentiles, and make the desolate cities to be inhabited.",
            prayerText: "Enlarge my life; release the power of advancement upon me, in the name of Jesus."
        },
        {
            scriptureText: "Jeremiah 10:23 - O Lord, I know that the way of man is not in himself: it is not in man that walketh to direct his steps.",
            prayerText: "Direct me O God in the way that I should go, in the name of Jesus."
        },
        {
            scriptureText: "Daniel 2:22 - He revealeth the deep and secret things: he knoweth what is in the darkness, and the light dwelleth with him.",
            prayerText: "Let your light shine through every darkened area of my life and bring every hidden thing out, in the name of Jesus."
        },
        {
            scriptureText: "Daniel 2:21 - And he changeth the times and the seasons; he removeth kings, and setteth up kings; he giveth wisdom unto the wise, and knowledge to them that know understanding.",
            prayerText: "Change my time Oh Lord, establish me in your wisdom and let your wisdom teach me understanding, in the name of Jesus."
        },
        {
            scriptureText: "Micah 6:8 - He hath shewed thee o man, what is good; and what doth the Lord require of thee, but to do justly, and to love mercy, and to walk humbly with thy God.",
            prayerText: "Let your spirit of humility envelope me that I might be empowered to walk in your precepts, in the name of Jesus."
        },
        {
            scriptureText: "Proverbs 1:10 - My son, if sinners entice thee, consent thou not.",
            prayerText: "Every sin that is quietly dragging me away from God, I break loose from your power, in the name of Jesus."
        },
        {
            scriptureText: "2 Timothy 2:21 - If a man therefore purge himself from these, he shall be a vessel unto honour, sanctified, and meet for the master's use, and prepared unto every good work.",
            prayerText: "By the Blood of Jesus, I purge myself of every uncleanliness residing inside my body, in the name of Jesus."
        },
        {
            scriptureText: "Psalm 143:8 - Cause me to hear thy lovingkindness in the morning; for in thee do I trust; cause me to know the way wherein I should walk; for I lift up my soul unto thee.",
            prayerText: "Holy Spirit of the Lord, give me instruction that will make me to be outstanding in my career, in the name of Jesus."
        },
        {
            scriptureText: "Jeremiah 32:19 - Great in counsel, and might in work; for thine eyes are open all the ways of the sons of men: to give everyone according to his ways, and according to the fruit of his doings.",
            prayerText: "Father Lord, let the counsel and dimension of your Spirit be operational in my life, in the name of Jesus."
        },
        {
            scriptureText: "Mathew 11:29 - Take my Yoke upon you, and learn of me; For I am meek and lowly in heart; and ye shall find rest unto your souls.",
            prayerText: "Every plantation of pride and arrogancy, hiding within my heart be destroyed, in the name of Jesus."
        },
        {
            scriptureText: "Mathew 11:30 - For my Yoke is easy, and my burden is light.",
            prayerText: "Every difficult yoke and heavy burden given to me by the Enemy, I set you on fire, in the name of Jesus."
        },
        {
            scriptureText: "Psalm 119:105 - Thy word is a lamp unto my feet, and a light unto my path.",
            prayerText: "Father Lord, engrave your word on my paths that I may walk with you in truth and Holiness, in the name of Jesus."
        },
        {
            scriptureText: "Obadiah 17: - But upon mount Zion shall be deliverance and there shall be holiness; and the house of Jacob shall possess their possessions.",
            prayerText: "Father Lord, let your deliverance power take root in every department of my life, in the name of Jesus."
        },
        {
            scriptureText: "Philippians 4:13 - I can do all things through Christ which strengtheneth me.",
            prayerText: "Holy Spirit, enable me to maximize my potential, in the name of Jesus."
        },
        {
            scriptureText: "Philippians 4:13 - I can do all things through Christ which strengtheneth me.",
            prayerText: "Anointing of the Holy Spirit, strengthen me in my inner-man, in the name of Jesus."
        },
        {
            scriptureText: "2 Corinthians 4:4 - In whom the god of this world hath blinded the minds of them which believe not, lest the light of the glorious gospel of Christ who is the image of God, should shine unto them.",
            prayerText: "I speak against ignorance that is hiding in my spiritual life, by the blood of Jesus, in the name of Jesus."
        },
        {
            scriptureText: "Isaiah 44:3 - For I will pour water upon him that is thirsty, and floods upon the dry ground; I will pour my spirit upon thy seed, and my blessing upon thine offspring.",
            prayerText: "Oh Lord, water me with me with your spirit, in the name of Jesus."
        },
        {
            scriptureText: "Colossians 4:6 - Let your speech be always with grace, seasoned with salt, that ye may know how ye ought to answer every man.",
            prayerText: "My father, let your grace rest upon my tongue, that I may know how to answer every man, in the name of Jesus."
        },
        {
            scriptureText: "Acts 1:20 - For it is written in the book of Psalms, let his habitation be desolate, and let no man dwell therein; and his Bishoprick let another take.",
            prayerText: "Oh Lord, let the habitation of poverty and lack in my life become desolate, in the name of Jesus."
        },
        {
            scriptureText: "Isaiah 43:2 - When thou passest through the waters, I will be with thee; and through the rivers, they shall not overflow thee; when thou walkest through the fire, thou shall not be burdened, neither shall the flame kindle upon thee.",
            prayerText: "Every evil river flowing from my foundation, I command you to dry up, in the name of Jesus."
        },
        {
            scriptureText: "Colossians 1:13 - Who hath delivered us from the power of darkness and hath translated us into the kingdom of his dear son.",
            prayerText: "I receive my deliverance from the Dominion of darkness, in the name of Jesus."
        },
        {
            scriptureText: "Philippians 4:4 - Rejoice in the Lord always: and again I say, Rejoice.",
            prayerText: "Every dream attacks, that is working against my joy, I destroy your power, in the name of Jesus."
        },
        {
            scriptureText: "Psalm 91:4 - He shall cover thee with his feathers, and under his wings shalt thou trust; his truth shall be thy shield and buckler.",
            prayerText: "I take covering under the feather of the Lord, I take protection under his wings, in the name of Jesus."
        },
        {
            scriptureText: "Psalm 91:10 There shall not evil befall thee, neither shall any plague come nigh thy dwelling.",
            prayerText: "Environmental plague around my dwelling, I command you to scatter by the blood of Jesus, in the name of Jesus."
        },
        {
            scriptureText: "Psalm 5:11 - But let all those that put their trust in you; let them ever shout for joy because you defend them; Let those also who love your name be joyful in you",
            prayerText: "Oh Lord my God, I put my trust in you, I shall be joyful always, in the name of Jesus."
        },
        {
            scriptureText: "Psalm 75:1 - We give thanks to you, O God we give thanks; for your wondrous works declare that your name is near.",
            prayerText: "Oh Lord, I give you thanks for all that you have for me throughout the year, in the name of Jesus."
        },
        {
            scriptureText: "1 Corinthians 15:57 - But thanks be to God who gives us the victory through our Lord Jesus Christ.",
            prayerText: "Lord, I thank you, for giving me permanent victory all through the year, in the name of Jesus."
        },
    ]
}
