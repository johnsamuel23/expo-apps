export const getAudio = () => {
    let randomIndex = selectRandomIndex(16); // there are 17 songs, so I am selecting between 0 - 2

    switch(randomIndex) {
        case 0:
            return require('../sounds/olowogbogboro.mp3');
            break;
        case 1:
            return require('../sounds/casting_crowns.mp3');
            break;
        case 2: 
            return require('../sounds/onishe_iyanu.mp3'); 
            break;
        case 3: 
            return require('../sounds/drawMeCloseToYou.mp3'); 
            break;
        case 4: 
            return require('../sounds/elohim.mp3'); 
            break;
        case 5: 
            return require('../sounds/InChristAlone.mp3'); 
            break;
        case 6: 
            return require('../sounds/ISeeHimStanding.mp3'); 
            break;
        case 7: 
            return require('../sounds/MercySaidNo.mp3'); 
            break;
        case 8: 
            return require('../sounds/mmamma.mp3'); 
            break;
        case 9: 
            return require('../sounds/okaka.mp3'); 
            break;
        case 10: 
            return require('../sounds/TisSoSweet2TrustInYou.mp3'); 
            break;
        case 11: 
            return require('../sounds/WagingWar.mp3'); 
            break;
        case 12: 
            return require('../sounds/you_are_good.mp3'); 
            break;
        case 13: 
            return require('../sounds/YouAreAlpha.mp3'); 
            break;
        case 14: 
            return require('../sounds/WeWaitOnYou.mp3'); 
            break;
        case 15: 
            return require('../sounds/YouAreGreat_Steve_Crown.mp3'); 
            break;
        case 16: 
            return require('../sounds/AmazingGrace.mp3'); 
            break;
        default:
            return require('../sounds/onishe_iyanu.mp3');
    }
}

const selectRandomIndex = (max) => {
    return Math.floor(Math.random() * max);
}