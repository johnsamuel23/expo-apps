import React, { Component } from 'react';
import { StyleSheet, View, TouchableOpacity, Text} from 'react-native';

export default class Button extends Component {
    render() {
        return (
            <View style={styles.container}>
                <TouchableOpacity style={styles.button} onPress={this.props.onPress}>
                    <Text style={this.styles.text}>{this.props.text}</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    text: {
        color: '#555',
        fontSize: 15
    },
    button: {
        backgroundColor: '#fff',
        borderColor: '#eee',
    }
});