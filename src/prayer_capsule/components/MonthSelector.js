import React, { Component } from 'react';
import { StyleSheet, View, Text} from 'react-native';
import Touch from './Touch';

export default Button = ({
    onPress,
    month,
    theme
}) => {
    return (
        <View style={styles.container}>
            <Touch action={onPress}>
                <View style={[styles.button, {backgroundColor: theme.selectorBackgroundColor}]}>
                    <Text style={[styles.text, {color: theme.selectorTextColor}]}>{month}</Text>
                </View>
            </Touch>
        </View>
    )
}

const styles = StyleSheet.create({
    text: {
        color: '#555',
        fontSize: 15,
        textAlign: 'center'
    },
    button: {
        backgroundColor: '#fff',
        borderColor: '#eee',
        marginBottom: 15,
        padding: 15,
        borderRadius: 30
    }
});