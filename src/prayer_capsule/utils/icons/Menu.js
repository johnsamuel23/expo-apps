import React, { Component } from 'react';
import { Ionicons } from '@expo/vector-icons';
import { View } from 'react-native';

export default Menu = ({
    size,
    color
}) => {
    const Icon = (<Ionicons name="ios-search" size={size} color={color} />)
    return (
        <View>
            { Icon }     
        </View>
    )

}