import React, { Component } from 'react';
import { MaterialIcons } from '@expo/vector-icons';
import { View } from 'react-native';

export default Send = ({
    size,
    color
}) => {
    const Icon = (<MaterialIcons name="payment" size={size} color={color} />)
    return (
        <View>
            { Icon }
        </View>
    )

}