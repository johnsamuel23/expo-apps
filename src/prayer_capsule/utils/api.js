import axios from 'axios';

export const apiServer = process.env.NODE_ENV === 'production' ? "http://woleoladiyun.com/" : "http://192.168.107.1:8080/";

export const apiRoot = apiServer + "clam_apps/capsule/scripts/";

export const activateSubscription = (data) => {
    let requestParameters = "email=" + data.email;
    requestParameters = requestParameters + "&subscription_code=" + data.subscriptionCode;
    let request = apiRoot + "activate_app.php?" + requestParameters;
    console.log(request);
    return axios.get(request);
}

export const userRegistration = (data) => {
    let requestParameters = "email=" + data.email;
    requestParameters = requestParameters + "&firstname=" + data.firstname;
    requestParameters = requestParameters + "&lastname=" + data.lastname;
    requestParameters = requestParameters + "&phone_number=" + data.phone_number;
    let request = apiRoot + "user_registration.php?" + requestParameters;
    console.log(request);
    return axios.get(request);
}