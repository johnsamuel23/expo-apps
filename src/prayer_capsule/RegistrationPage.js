import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, WebView, TouchableOpacity, Platform, Dimensions, KeyboardAvoidingView } from 'react-native';
import BackIcon from './utils/icons/Back';
import SearchIcon from './utils/icons/Menu';
import { Constants } from 'expo';
import theme, { capsuleTheme } from './data/theme';
import TextInput from './components/TextInput';
import Button from './components/Button';
import SendIcon from './utils/icons/Send';
import PayIcon from './utils/icons/Pay';
import SubscribeIcon from './utils/icons/Subscribe';
import { userRegistration } from './utils/api';
import * as Animatable from 'react-native-animatable';

export default class RegistrationPage extends Component {

  componentWillMount() {
    this.setState({
      subscribed: false,
      email: '',
      firstname: '',
      lastname: '',
      phone_number: '',
      justRegistered: false
    });
  }

  changeInput(field, value) {
    this.setState({
      [field]: value
    });
  }

  register() {
    let data = {
      email: this.state.email,
      firstname: this.state.firstname,
      lastname: this.state.lastname,
      phone_number: this.state.phone_number
    }
    
    if(!this.state.justRegistered) {
        userRegistration(data).then(response => {
          console.log(response.data);
          if(response.data.message === 'empty_fields') {
            let error_fields = response.data.error_fields;
            let fields = '';
            for(let i=0; i < error_fields.length; i++) {
                fields += "- " + error_fields[i] + "\n";
            }
    
            let message = "Please fill the required fields \n " + fields;
            this.setState({messageBoxMarginBottom: 20, errorMessage: message, errorMessageHeight: 70, errorMessageTextSize: 15, messageBackgroundColor: 'rgb(184, 36, 36)'});
          } else if (response.data.message === 'success') {
            let message = "We have recieved your registration details and will send your subscription code soon";
            this.setState({messageBoxMarginBottom: 20, errorMessage: message, errorMessageHeight: 70, errorMessageTextSize: 15, messageBackgroundColor: theme.greenBackgroundColor});
            this.setState({justRegistered: true, firstname: '', lastname: '', email: '', phone_number: ''});
        } else { 
            let message = "Sorry. There is some error on our side. Please Check your ENTRIES and try again or contact support";
            this.setState({messageBoxMarginBottom: 20, errorMessage: message, errorMessageHeight: 70, errorMessageTextSize: 15, messageBackgroundColor: 'rgb(184, 36, 36)'});
        }
        
        setTimeout(() => {
            this.setState({messageBoxMarginBottom: 0, errorMessageHeight: 0, errorMessageTextSize: 0,});
                setTimeout(() => {
                    this.props.navigator.pop({name: 'registration_screen'});
                }, 1000);
          }, 5000);
        });
    }
  }

  render() {
    let { navigator } = this.props;
    let sendIcon = (<SendIcon color={"white"} size={20} />);
    let payIcon = (<PayIcon color={"white"} size={20} />);
    let subscribeIcon = (<SubscribeIcon color={"white"} size={20} />);
    let windowHeight = Dimensions.get('window').height;
    let windowWidth = Dimensions.get('window').width;

    return (
      <View style={[styles.container]}>
        <View style={[styles.statusBar, {backgroundColor: '#000'}]}></View>
        <View style={[styles.monthHead, {backgroundColor: capsuleTheme.May.headerBackgroundColor}]}>
          <View style={styles.headerLeft}>
              <TouchableOpacity style={styles.backIcon} onPress={() => this.props.navigator.pop({name: 'registration_screen'})}>
                <BackIcon color={"white"} size={35} />
              </TouchableOpacity>
          </View>
          <View style={styles.headerCenter}>
            <Text style={[styles.headText, {color: capsuleTheme.May.headerTextColor}]}> App Subscription </Text>
          </View>
          <View style={styles.headerRight}></View>
        </View>
        <View style={styles.WebViewEmbed}>
          <View style={styles.form}>
            <View style={styles.formTop}>
              <View>
                <TextInput value={this.state.firstname} placeholder="Firstname" changeInput={(value) => this.changeInput('firstname', value)} />
                <TextInput value={this.state.lastname} placeholder="Lastname" changeInput={(value) => this.changeInput('lastname', value)} />
                <TextInput value={this.state.phone_number} placeholder="Phone Number" changeInput={(value) => this.changeInput('phone_number', value)} />
                <TextInput value={this.state.email} placeholder="Your Email" changeInput={(value) => this.changeInput('email', value)} />
              </View>
              <View style={{marginTop: 0, marginBottom: 20}}>
                <Button 
                    buttonStyles={{backgroundColor: theme.menuBackgroundColor, borderColor: '#555', width: '100%'}}
                    textStyles={{color: 'white', fontSize: 15, marginLeft: -5}}
                    text='Register For Code' icon={subscribeIcon} inline={true} switchScreen={() => this.register()} />

                <Animatable.View 
                    transition={['height']} 
                    style={{justifyContent: 'center', 
                      alignItems: 'center', 
                      backgroundColor: this.state.messageBackgroundColor || 'rgb(184, 36, 36)', 
                      marginTop: 20, 
                      marginBottom: this.state.messageBoxMarginBottom || 0, 
                      borderRadius: 3, 
                      justifyContent: 'center', 
                      alignItems: 'center', 
                      height: this.state.errorMessageHeight || 0,
                      overflow: 'hidden'
                    }}>
                      <Animatable.Text style={{textAlign: 'center', padding: 10,
                        fontSize: this.state.errorMessageTextSize || 0, color: 'white', fontFamily: 'lato-regular'}}>
                          {this.state.errorMessage}
                      </Animatable.Text>
                  </Animatable.View>
              </View>
            </View>
            <View style={styles.formBellow}>
              <View style={styles.contactSection}>
                  <Text style={styles.contactText}>Support Contact: <Text style={styles.number}>0803 830 1804</Text></Text>
              </View>
            </View>
          </View>
        </View>
      </View>
    );
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  form: {
    height: 300,
    flex: 1,
  },
  formTop: {
    flex: 1,
    padding: 20,
  },
  formBellow: {
    height: 40
  },
  contactSection: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: theme.menuBackgroundColor,
  },
  contactText: {
    fontFamily: 'lato-regular',
    color: 'white'
  },
  number: {
    fontFamily: 'lato-heavy'
  },
  header: {
      backgroundColor: '#02223b',
      minHeight: 50,
      justifyContent: 'center'
  },
  headText: {
    fontSize: 17,
    textAlign: 'center'
  },
  statusBar: {
    backgroundColor: '#fbf7d2',
    height: Constants.statusBarHeight,
  },
  monthHead: {
    backgroundColor: '#555',
    paddingLeft: 20,
    paddingRight: 20,
    height: 50,
    alignItems: 'center',
    flexDirection: 'row'
    // marginTop: 10
  },
  headerText: {
      fontSize: 15,
      color: 'white',
      justifyContent: 'center',
  },
  BackButton: {
      marginRight: 10,
      marginLeft: 10
  },
  headerLeft: {
    width: 70,
    justifyContent: 'center',
  },
  headerCenter: {
    justifyContent: 'center',
    alignSelf: 'center',
    flex: 1
  },
  headerRight: {
    width: 70
  },
  WebViewEmbed: {
      backgroundColor: "#eee",
      flex: 1
  }
});


