import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, TouchableOpacity } from 'react-native';
import DaySelector from './components/DaySelector';
import { capsuleTheme } from './data/theme';
import BackIcon from './utils/icons/Back';
import { Constants } from 'expo';

export default DaysMenu = ({
  store,
  navigator,
  selectDay
}) => {

    let { months, selectedMonth } = store;

    let selectedMonthObject = months.find(month => month.name === selectedMonth) || {};

    let theme = capsuleTheme[selectedMonth];

    let daySelectOptions = selectedMonthObject.days.map((item, index) => {
        return <DaySelector 
                  key={index} 
                  day={item.day} 
                  theme={theme}
                  onPress={() => {
                      navigator.push({name: 'capsule_screen'}); 
                      selectDay(item.day) 
                    }}
                  />
    });
    
    return (
      <View style={[styles.container]}>
        <View style={[styles.statusBar, {backgroundColor: '#000'}]}></View>
        <View style={[styles.monthHead, {backgroundColor: theme.headerBackgroundColor}]}>
          <View style={styles.headerLeft}>
            <TouchableOpacity style={{width: 50}} onPress={() => navigator.pop({name: 'days_menu'})}>
              <BackIcon size={35} color={theme.headerTextColor} />
            </TouchableOpacity>
          </View>
          <View style={styles.headerCenter}>
            <Text style={[styles.headText, {color: theme.headerTextColor}]}>{ selectedMonth }</Text>
          </View>
          <View style={styles.headerRight}></View>
        </View>
        <ScrollView>
          <View style={styles.daysList}>
            <Text style={styles.cue}>Select a Day</Text> 
            <ScrollView style={styles.daysBox} contentContainerStyle={[styles.daysBoxWrapper]} >

                { daySelectOptions }

            </ScrollView>
          </View>
        </ScrollView>
      </View>
    );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    flex: 1
  },
  statusBar: {
    backgroundColor: '#fbf7d2',
    height: Constants.statusBarHeight,
  },
  headerLeft: {
    width: 70,
    justifyContent: 'center',
  },
  headerCenter: {
    justifyContent: 'center',
    alignSelf: 'center',
    flex: 1
  },
  headerRight: {
    width: 70
  },
  monthHead: {
    backgroundColor: '#555',
    paddingLeft: 20,
    paddingRight: 20,
    height: 50,
    alignItems: 'center',
    flexDirection: 'row'
    // marginTop: 10
  },
  headText: {
    fontSize: 20,
    textAlign: 'center'
  },
  daysList: {
    justifyContent: 'center',
    alignItems: 'center',
    padding: 20,
  },
  cue: {
    fontSize: 15,
    marginBottom: 20,
    color: '#555'
    // alignSelf: 'flex-start'
  },
  daysBox: {
    flexDirection: 'column',
  },
  daysBoxWrapper: {
    flexWrap: 'wrap',
    alignItems: 'flex-start',
    flexDirection: 'row',
    justifyContent: 'space-between'
  }
});
