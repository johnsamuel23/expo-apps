import React, { Component } from 'react';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { View } from 'react-native';

export default Subscribe = ({
    size,
    color
}) => {
    const Icon = (<MaterialCommunityIcons name="open-in-app" size={size} color={color} />)
    return (
        <View>
            { Icon }
        </View>
    )

}