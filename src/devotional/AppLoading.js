import React, { Component } from 'react';
import { 
    View, 
    Text, 
    StyleSheet, 
    Dimensions, 
    Animated,
    Easing,
    LayoutAnimation,
    UIManager } from 'react-native';
import { Font, Notifications, Constants } from 'expo';
import BackgroundImage from './components/BackgroundImage';
import Touch from './components/Touch';
import LocalImage from './components/LocalImage';

export default class AppLoading extends Component {

    state = {
        tapCueActive: false
    }

    componentWillMount() {
        this.opacityValue = new Animated.Value(0);
    }

    componentDidMount() {
        const { navigator } = this.props;
        this.opacityAnim();

        if(navigator) {
            setTimeout(function(){
                navigator.push({name: 'reading_screen'});
            }, 2000);

            let _this = this;
            setTimeout(() => {
                _this.setState({
                    tapCueActive: true
                })
            }, 3000);
        }
    }

    opacityAnim() {
        this.opacityValue.setValue(0);
        Animated.timing(
            this.opacityValue,
            {
                toValue: 1,
                duration: 2000,
                easing: Easing.easeOutBack
            }
        ).start(() => {
            this.opacityAnim();
        });
    }

    switchScreen(screen) {
        console.log("state++++", this.state);

        if(this.state.tapCueActive) {
            setTimeout(() => {
                this.props.navigator.push({name: screen});
            }, 50);
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <BackgroundImage style={{zIndex: 0}} source={require('./img/splash_square.png')}>
                    <View style={styles.backgroundView}>
                        <View style={styles.actionView}>
                            <View style={{flexDirection: 'row', marginBottom: 0, height: 70, justifyContent: 'center', alignItems: 'center'}}>
                                <LocalImage
                                    originalHeight="110"
                                    originalWidth="73"
                                    source={require('./img/gr5_logo.png')}
                                    guideWidth={20}
                                />
                                <Text style={{marginLeft: 10, color: 'white', fontSize: 12}}>Powered by GR5</Text>
                            </View>
                            <Touch 
                                action={() => this.switchScreen('reading_screen')}>
                                <View style={[styles.continueBtn, {backgroundColor: '#0a802d'}]}>
                                    <Text style={styles.actionText}> Continue </Text>
                                </View>
                            </Touch>
                        </View>
                    </View>   
                </BackgroundImage>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#040d46',
        position: 'absolute',
        height: Dimensions.get('window').height,
        width: Dimensions.get('window').width,
        top: 1,
        bottom: 1
    },
    actionView: {
        zIndex: 20,
        alignSelf: 'stretch',
        justifyContent: 'flex-end',
        width: '100%'
    },
    continueBtn: {
        height: 70,        
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#4090ab',
        width: '100%'
    },
    actionText: {
        fontSize: 20,
        color: '#fff'
    },
    tapCue: {
        backgroundColor: 'rgba(0,0,0,0.5)',
        padding: 10,
        borderRadius: 20,
        alignSelf: 'flex-end',
        marginBottom: 150
    },
    tapCueText: {
        textAlign: 'center',
        fontSize: 12,
        color: '#fff'
    },
    backgroundView: {
        height: Dimensions.get('window').height,
        width: Dimensions.get('window').width,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
    overlay: {
        position: 'absolute',
        backgroundColor: 'transparent',
        zIndex: 10,
        height: Dimensions.get('window').height,
        width: Dimensions.get('window').width,
        justifyContent: 'center',
        alignItems: 'center'
    }


});