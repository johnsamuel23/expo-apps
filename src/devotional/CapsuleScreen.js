import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, Dimensions, Image, TouchableOpacity } from 'react-native';
import MonthSelector from './components/MonthSelector';
import LocalImage from './components/LocalImage';
import { capsuleData } from './data/MonthsCapsules';
import { capsuleTheme  } from './data/theme';
import BackgroundImage from './components/BackgroundImage';
import { getAudio } from './data/capsuleData';
import { Audio, Constants, KeepAwake  } from 'expo';
import BackIcon from './utils/icons/Back';
import SearchIcon from './utils/icons/Menu';

export default class CapsuleScreen extends Component {

  componentDidMount() {
    this.props.activateCapsule();
    this.props.stopSoundTrack();    
    this.props.playSoundTrack();
    KeepAwake.activate();
  }

  componentWillUnmount() {
    this.props.stopSoundTrack();
    this.props.deactivateCapsule();
    KeepAwake.deactivate();
  }

  render() {

    let { selectedMonth, selectedDay } = this.props.store;
    let dayIndex = selectedDay - 1;
    let capsule = capsuleData[selectedMonth][dayIndex];
    let theme = {...capsuleTheme[selectedMonth]};
    if(selectedMonth === "December" || selectedMonth === "August") {
      theme.headerTextColor = "#333";
    }

    return (
      <View style={[styles.container]}>
        <View style={styles.statusBar}></View>
        <View style={{flex: 1, zIndex: 3}}>
          <View style={styles.heading}>
            <View style={[styles.headBox, {backgroundColor: theme.selectorBackgroundColor}]}>
              <View style={styles.headerLeft}>
                <TouchableOpacity style={styles.backIcon} onPress={() => this.props.navigator.pop({name: 'capsule_screen'})}>
                  <BackIcon color={theme.headerTextColor} size={35} />
                </TouchableOpacity>
              </View>
              <View style={styles.headerCenter}>
                <Text style={[styles.headText, {color: theme.headerTextColor}]}> Prayer Capsule: </Text>
                <Text style={[styles.headText, {color: theme.headerTextColor, fontFamily: 'lato-regular'}]}>{selectedMonth} { selectedDay }</Text>
              </View>
              <View style={styles.headerRight}>
                <TouchableOpacity activeOpacity={0.5} style={styles.backIcon} onPress={() => this.props.navigator.push({name: 'months_menu'})}>
                  <SearchIcon color={theme.headerTextColor} size={35} />
                </TouchableOpacity>
              </View>
            </View>
          </View>
          <View style={[styles.capsuleContainer, {backgroundColor: 'transparent'}]}>
            <View style={styles.capsuleHeader}>
              <LocalImage source={require('./img/Prayer_Capsules_Flower_header.png')} 
                    originalWidth="274"
                    originalHeight="107"
                    guideWidth={100}
                />   
            </View>
            <ScrollView style={{zIndex: 10}} contentContainerStyle={styles.contentText} showsVerticalScrollIndicator={false}>
              <View style={styles.scriptureBox}>
                <Text style={[styles.scriptureText, {color: theme.headerTextColor}]}>
                  { capsule.scriptureText }
                </Text>
              </View>
              <View style={styles.scriptureBox}>
                <Text style={[styles.prayerText, {color: theme.headerTextColor}]}>
                  Prayer: { capsule.prayerText }
                </Text>
              </View>
            </ScrollView>
            <View style={styles.absoluteBanner}>
                <Image 
                  source={require('./img/Prayer_Capsules_bread.png')} 
                  style={{
                      width: 120,
                      height: 120
                  }}
              />
            </View>
          </View>
        </View>
        <View style={[styles.backgroundView, {zIndex: 2, backgroundColor: theme.selectorBackgroundColor}]}>
          <View style={styles.backgroundImage}>
            <LocalImage
                originalHeight={457}
                originalWidth={467}
                source={require('./img/pastor_and_mummy_circle.png')}
                guideWidth={90}
            />
          </View>
        </View>
      </View>
    );
  }

}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#000',
    // padding: 20,
    // justifyContent: 'center',
    // alignItems: 'center',
    flex: 1
  },
  menuIcon: {
    position: 'absolute',
    left: 25,
    top: 15,
    width: 50,
    justifyContent: 'center'
  },
  backgroundView: {
    position: 'absolute',
    backgroundColor: 'transparent',
    height: Dimensions.get('window').height,
    width: Dimensions.get('window').width,
    justifyContent: 'flex-start',
    alignItems: 'flex-end',
    zIndex: 0,
  }, 
  backgroundImage: {
    opacity: 0.7,
    marginRight: 10,
    marginTop: 110,
  }, 
  statusBar: {
    backgroundColor: '#000',
    height: Constants.statusBarHeight,
    zIndex: 100
  },
  BackIcon: {
    width: 50, 
  },
  heading: {
    paddingLeft: 15,
    paddingRight: 15,
    paddingTop: 15,
    paddingBottom: 15,
    backgroundColor: '#000',
    // borderTopLeftRadius: 30,
    // borderTopRightRadius: 30,
    // marginTop: 10
  }, 
  headBox: {
    borderRadius: 25,
    height: 45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  headText: {
    textAlign: 'center',
    fontSize: 15,
    fontFamily: 'lato-heavy'
  },
  headerLeft: {
    width: 50,
    paddingLeft: 20
  },
  headerCenter: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  }, 
  headerRight: {
    width: 50,
    justifyContent: 'flex-end',
    marginRight: -5
  },
  scriptureText: {
    fontSize: 17,
    textAlign: 'center',
    paddingTop: 20,
    paddingLeft: 10,
    paddingRight: 10,
    fontFamily: 'lato-regular'
  },
  prayerText: {
    textAlign: 'center',
    fontSize: 20,
    fontFamily: 'lato-heavy',
    paddingTop: 20,
    paddingLeft: 10,
    paddingRight: 10,
  },
  scriptureBox: {
    justifyContent: 'center',
    paddingLeft: 10,
    paddingRight: 10,
    alignItems: 'center',
    zIndex: 10,
    marginBottom: 10
  },
  contentText: {
    zIndex: 10,
    paddingTop: 30,
    marginBottom: 10,
    paddingBottom: 10
  },
  capsuleContainer: {
    flex: 1,
    backgroundColor: 'red',
  },
  capsuleHeader: {
    paddingTop: 20,
    // paddingBottom: 20,
    justifyContent: 'center',
    alignItems: 'center'
  },
  absoluteBanner: {
    position: 'absolute',
    bottom: 0,
    zIndex: 0,
  }
});
