import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, BackHandler, AsyncStorage } from 'react-native';
import { Navigator } from 'react-native-deprecated-custom-components';
import { months } from './data/MonthsCapsules';
import { getAudio } from './data/capsuleData';
import { Notifications, Permissions, Constants, Audio, Asset, AppLoading } from 'expo';
import ReadingScreen from './ReadingScreen';
import DaysMenu from './DaysMenu';
import MonthsMenu from './MonthsMenu';
import SubscriptionPage from './SubscriptionPage';
import RegistrationPage from './RegistrationPage';
import SplashScreen from './SplashScreen';
import moment from 'moment';
import AppLoadingScreen from './AppLoading';

export default class MainApp extends Component {

  state = {
    initialRoute: 'loading_screen',
    months: months,
    selectedMonth: this.getCalender().month,
    selectedDay: this.getCalender().day,
    isLoading: true,
    capsuleActive: true,
    audioLoaded: false,
    subscriptionActive: true,
    rehydrating: false,
    localImages: [
      require('./img/splash_square.png'),
      require('./img/gr5_logo.png'),
    ]
  }


  cacheLocalImages(images) {
    return images.map(image => {
      if (typeof image === 'string') {
          return Image.prefetch(image);
      } else {
          return Asset.fromModule(image).downloadAsync();
      }
    });
  }

  async componentWillMount() {
    this.cacheLocalImages(this.state.localImages);
    // let subscriptionState = await this.checkSubscriptionStatus();
    // console.log("Component will mount Subscription State", subscriptionState);
    // temporarily unset subscription
    // AsyncStorage.removeItem('SubscriptionActivated');
  }

  saveStateData(key, value) {
    this.setState({
      [key]: value
    });
  }

  async checkSubscriptionStatus() {
    let isSubscribed = false;
    try {
      // AsyncStorage.removeItem('dailySchedulesDone'); // remove an item // remove the old key
      const value = await AsyncStorage.getItem('SubscriptionActivated');
      if (value !== null) {
        // We have data!!
        console.log("subscriptionActivated", value); // The value must be 'yes'
        isSubscribed = true;
      } else {
        console.log("------ NOT ACTIVATED ------", value);        
      }
    } catch (error) {
      // Error retrieving data
      console.log("not activated", value);
      isSubscribed = false;
    }

    console.log("SUBSCRIBED", isSubscribed);

    // setting subscription state
    this.setState({
      subscriptionActive: isSubscribed,
    });

    if(!isSubscribed) {
      this.setState({
        initialRoute: 'subscription_screen'
      });
    }

    let _this = this;
    // delay web page loading
    // setTimeout(() => {
    // }, 7000);



    _this.setState({
      rehydrating: false
    });

    return isSubscribed;
  }

  async activateSubscription(data) {
    console.log("Activating the Application", data);
        
    try {
      await AsyncStorage.setItem('SubscriptionActivated', 'yes');
      console.log("SubscriptionActivated key Persisted");
      this.setState({
        subscriptionActive: true,
        justSubscribed: true
      });
    } catch (error) {
      // Error saving data
      console.log("could not persist SubscriptionActivated");
    }
  }

  deactivateCapsule() {
    this.setState({
      capsuleActive: false
    });
  }

  activateCapsule() {
    this.setState({
      capsuleActive: true
    });
    console.log("capsule active");
  }

  async playSoundTrack() {
    this.soundTrack = new Audio.Sound();
    console.log("sound play initialised");
    try {
      await this.soundTrack.loadAsync(getAudio());
      if(this.state.capsuleActive) {
        await this.soundTrack.playAsync();
        this.setState({
          audioLoaded: true
        });
      }
    } catch (error) {
      // An error occurred!
    }

  }

  stopSoundTrack() {
    if(this.state.audioLoaded) {
      this.soundTrack.stopAsync(); 
    }
  }

  getCalender() {
    t = new Date();     
    let day = parseInt(t.getDate());
    let month = t.getMonth(); // months range from 0 - 11 hence the requirement of adding 1
    switch(month) {
      case 0: month = 'January';
        break;
      case 1: month = 'February';
        break;
      case 2: month = 'March';
        break;
      case 3: month = 'April';
        break;
      case 4: month = 'May';
        break;
      case 5: month = 'June';
        break;
      case 6: month = 'July';
        break;
      case 7: month = 'August';
        break;
      case 8: month = 'September';
        break;
      case 9: month = 'October';
        break;
      case 10: month = 'November';
        break;
      case 11: month = 'December';
    }

    return { day, month };
  }
  
  constructor(props) {
    super(props);
    this.navigator = null;
  }

  handleNotifications(notificationObject) {
    const { data } = notificationObject;
 
    let calender = this.getCalender();

    if(notificationObject.origin === 'selected' && notificationObject.remote === false) {
      if(!this.state.subscriptionActive) {
        this.navigator.push({name: 'subscription_screen'});
      } else {
        this.selectDay(calender.day);
        this.selectMonth(calender.month);
        this.navigator.push({name: data.screen});
      }
    }
  }

  async RegisterLocalNotifications() {

    let localNotification = {
      title: 'Restoration Devotional',
      body: 'Your study for the day',
      data: {
        screen: 'reading_screen',
        month: "September",
        day: 1
      },
      ios: {
        sound: true
      },
      android: {
        sound: true,
        vibrate: true,
        color: '#cb0c13',
        priority: 'high'
      }
    }    

    // let target_date = new Date();
    // let current_date = new Date();
    // target_date.setDate(current_date.getDate()+1);
    // target_date.setHours(8,0,0,0); // 8am tomorrow
    // let tomorrow = target_date.getTime();

    // Action Plan
    // Schedule Notifications for every days in the month

    // NOTE:::: Cancel All Scheduled Notifications 
    // Notifications.cancelAllScheduledNotificationsAsync(); 

    //--------- Beginning of Manual Scheduling 
      // let year = new Date().getFullYear();
      // for(var monthIndex=0; monthIndex < months.length; monthIndex++) {
      //   let month = months[monthIndex];
      //   let days = month.days;
      //   for(var dayIndex=1; dayIndex <= days; dayIndex++) {
      //     let new_time = new Date();
      //     new_time.setFullYear(year, monthIndex, dayIndex);
      //     new_time.setHours(8,0,0,0);
      //     let schedule_date = new_time.getTime();

      //     const schedulingOptions = {
      //       time: launch_time,
      //       repeat: 'day'
      //     }

      //     let notificationId = Notifications.scheduleLocalNotificationAsync(localNotification, schedulingOptions);
      //   }
      // }
    //--------------- End of Manual Schedulling for ----------------

    // Show a notification once app is opened
    let target_time = new Date();
    let current_time = new Date();
    target_time.setSeconds(current_time.getSeconds()+5); // 5 seconds from now
    // target_date.setHours(8,0,0,0);
    let launch_time = target_time.getTime();
    let onOpenSchedulingOptions = {
      time: launch_time,
    }

    // let notificationId = Notifications.scheduleLocalNotificationAsync(localNotification, onOpenSchedulingOptions);
    // End of once-off on app open notification

    /* 
     * We need to have a way to know if the notifications have been scheduled already and avoid rescheduling
     * when app is refreshed or re-opened.
     * To that effect, upon receiving the notification Id, we will persist it. and check that the app does not have 
     * any persisted value before determining whether to run this schedule.
     */
    
    let target_date = new Date();
    let current_date = new Date();
    target_date.setDate(current_date.getDate()+1); // tomorrow 
    target_date.setHours(8,0,0,0); // 8am tomorrow
    // target_date.setDate(current_date.getDate()); // today -- just testing
    // target_date.setHours(11,12,0,0); // 11am today -- just testing
    let scheduledTime = target_date.getTime();

    const dailySchedulingOptions = {
      time: scheduledTime,
      repeat: 'day'
    }

    // Every time app opens, We cancel previous notifications and reschedule
    // The reason is that after the first schedule, once the device is turned off, the notifications no longer work
    // So, once the app is opened again by the user, the notification should be scheduled again.
    // --- Another approach can be to save the notification id upon schedule
    // and on app open, check if the notification with that id is still active,
    // if not, reschedule it, other wise, do not schedule any

    Notifications.cancelAllScheduledNotificationsAsync(); 
    this.persistNotification(localNotification, dailySchedulingOptions);    

    try {
      // AsyncStorage.removeItem('dailySchedulesDone'); // remove an item // remove the old key
      const value = await AsyncStorage.getItem('dailySchedulesDone_T2');
      if (value !== null) {
        // We have data!!
        console.log(value); // The value must be 'yes'
      } else {
        // Apply the Schedule
        Notifications.cancelAllScheduledNotificationsAsync(); 
        this.persistNotification(localNotification, dailySchedulingOptions);    
      }
    } catch (error) {
      // Error retrieving data
      // console.log('dailySchedulesDone_T2 not found');
    }
  }

  async persistNotification(localNotification, dailySchedulingOptions) {
      let notificationId = Notifications.scheduleLocalNotificationAsync(localNotification, dailySchedulingOptions);
      try {
        await AsyncStorage.setItem('dailySchedulesDone_T2', 'yes');
        console.log("dailySchedulesDone_T2 persisted");
      } catch (error) {
        // Error saving data
        console.log("could not persist dailySchedulesDone_T2");
      }
  }

  async componentDidMount() {
    Notifications.addListener((data) => this.handleNotifications(data)); // Listen for Notifications
    this.RegisterLocalNotifications(); 

    let result = await Permissions.askAsync(Permissions.NOTIFICATIONS);
    if (Constants.lisDevice && resut.status === 'granted') {
      console.log('Notification permissions granted.')
    }

    BackHandler.addEventListener('hardwareBackPress', () => {
      if (this.navigator && this.navigator.getCurrentRoutes().length > 1) {
        this.navigator.pop();
        // BackHandler.exitApp();
        return true;
      }
      return false;
    });

  }

  selectMonth(value) {
    this.setState({
      selectedMonth: value
    });
  }

  selectDay(value) {
    this.setState({
      selectedDay: value
    });
  }

  renderScene(route, navigator) {
    this.navigator = navigator;

    switch(route.name) {
      case 'months_menu':
          currentScreen = <MonthsMenu store={this.state} 
                              navigator={navigator}
                              selectDay={this.selectDay.bind(this)}
                              selectMonth={this.selectMonth.bind(this)}
                          />
          break;
      case 'subscription_screen':
          currentScreen = <SubscriptionPage store={this.state} 
                          activateApp={this.activateSubscription.bind(this)}
                          saveStateData={this.saveStateData.bind(this)}
                          navigator={navigator}
                      />
          break;
      case 'registration_screen':
          currentScreen = <RegistrationPage store={this.state} 
                          activateApp={this.activateSubscription.bind(this)}
                          saveStateData={this.saveStateData.bind(this)}
                          navigator={navigator}
                      />
          break;
      case 'subscription_success':
          currentScreen = <SplashScreen store={this.state} 
                            navigator={navigator}
                            selectMonth={this.selectMonth.bind(this)}
                            selectDay={this.selectDay.bind(this)}
                          />
          break;
      case 'splash_screen':
          currentScreen = <SplashScreen store={this.state} 
                            navigator={navigator}
                            selectMonth={this.selectMonth.bind(this)}
                            selectDay={this.selectDay.bind(this)}
                          />
          break;
      case 'loading_screen':
          currentScreen = <AppLoadingScreen store={this.state} 
                              navigator={navigator}
                          />
          break;
      case 'days_menu':
          currentScreen = <DaysMenu store={this.state} 
                              navigator={navigator}
                              selectDay={this.selectDay.bind(this)}
                              selectMonth={this.selectMonth.bind(this)}
                          />
          break;
      case 'reading_screen':
          currentScreen = <ReadingScreen store={this.state} 
                              navigator={navigator}
                              selectDay={this.selectDay.bind(this)}
                              selectMonth={this.selectMonth.bind(this)}
                              activateCapsule={this.activateCapsule.bind(this)}
                              playSoundTrack={this.playSoundTrack.bind(this)}
                              stopSoundTrack={this.stopSoundTrack.bind(this)}
                              deactivateCapsule={this.deactivateCapsule.bind(this)}
                          />
          break;
      default: 
          currentScreen =  <MonthsMenu store={this.state} 
                              navigator={navigator}
                              selectDay={this.selectDay.bind(this)}
                              selectMonth={this.selectMonth.bind(this)} />;
    }

    return currentScreen;   
  }

  render() {
    if(this.state.rehydrating) {
      return (<AppLoading />);
    }

    return (
      <View style={[styles.container]}>
        <View style={styles.container}> 
            <Navigator
                initialRoute={{name: this.state.initialRoute}}
                renderScene={this.renderScene.bind(this)}
                configureScene={(route, routeStack) => Navigator.SceneConfigs.FloatFromBottom}
            />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    header: {
        backgroundColor: '#000',
        height: Constants.statusBarHeight,
    },
    headerText: {
        fontSize: 20
    }
});

