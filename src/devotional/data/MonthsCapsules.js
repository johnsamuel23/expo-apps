export const months = [
    {
        "name": "January", 
        "slug": "jan",
        "days": [
            {day: 22},
            {day: 23},
            {day: 24},
            {day: 25},
            {day: 26},
            {day: 27},
            {day: 28},
            {day: 29},
            {day: 30},
            {day: 31}
        ]  
    },
    {
        "name": "February", 
        "slug": "feb",
        "days": [
            {day: 1},
            {day: 2},
            {day: 3},
            {day: 4},
            {day: 5},
            {day: 6},
            {day: 7},
            {day: 8},
            {day: 9},
            {day: 10},
            {day: 11},
            {day: 12},
            {day: 13},
            {day: 14},
            {day: 15},
            {day: 16},
            {day: 17},
            {day: 18},
            {day: 19},
            {day: 20},
            {day: 21},
            {day: 22},
            {day: 23},
            {day: 24},
            {day: 25},
            {day: 26},
            {day: 27},
            {day: 28}
        ]  
    },
    {
        "name": "March", 
        "slug": "mar",
        "days": [
            {day: 1},
            {day: 2},
            {day: 3},
            {day: 4},
            {day: 5},
            {day: 6},
            {day: 7},
            {day: 8},
            {day: 9},
            {day: 10},
            {day: 11},
            {day: 12},
            {day: 13},
            {day: 14},
            {day: 15},
            {day: 16},
            {day: 17},
            {day: 18},
            {day: 19},
            {day: 20},
            {day: 21},
            {day: 22},
            {day: 23},
            {day: 24},
            {day: 25},
            {day: 26},
            {day: 27},
            {day: 28},
            {day: 29},
            {day: 30},
            {day: 31}
        ]  
    },
    {
        "name": "April", 
        "slug": "April",
        "days": [
            {day: 1},
            {day: 2},
            {day: 3},
            {day: 4},
            {day: 5},
            {day: 6},
            {day: 7},
            {day: 8},
            {day: 9},
            {day: 10},
            {day: 11},
            {day: 12},
            {day: 13},
            {day: 14},
            {day: 15},
            {day: 16},
            {day: 17},
            {day: 18},
            {day: 19},
            {day: 20},
            {day: 21},
            {day: 22},
            {day: 23},
            {day: 24},
            {day: 25},
            {day: 26},
            {day: 27},
            {day: 28},
            {day: 29},
            {day: 30}
        ]  
    },
    {
        "name": "May", 
        "slug": "may",
        "days": [
            {day: 1},
            {day: 2},
            {day: 3},
            {day: 4},
            {day: 5},
            {day: 6},
            {day: 7},
            {day: 8},
            {day: 9},
            {day: 10},
            {day: 11},
            {day: 12},
            {day: 13},
            {day: 14},
            {day: 15},
            {day: 16},
            {day: 17},
            {day: 18},
            {day: 19},
            {day: 20},
            {day: 21},
            {day: 22},
            {day: 23},
            {day: 24},
            {day: 25},
            {day: 26},
            {day: 27},
            {day: 28},
            {day: 29},
            {day: 30},
            {day: 31}
        ]  
    },
    {
        "name": "June", 
        "slug": "june",
        "days": [
            {day: 1},
            {day: 2},
            {day: 3},
            {day: 4},
            {day: 5},
            {day: 6},
            {day: 7},
            {day: 8},
            {day: 9},
            {day: 10},
            {day: 11},
            {day: 12},
            {day: 13},
            {day: 14},
            {day: 15},
            {day: 16},
            {day: 17},
            {day: 18},
            {day: 19},
            {day: 20},
            {day: 21},
            {day: 22},
            {day: 23},
            {day: 24},
            {day: 25},
            {day: 26},
            {day: 27},
            {day: 28},
            {day: 29},
            {day: 30},
        ]  
    },
    {
        "name": "July", 
        "slug": "july",
        "days": [
            {day: 1},
            {day: 2},
            {day: 3},
            {day: 4},
            {day: 5},
            {day: 6},
            {day: 7},
            {day: 8},
            {day: 9},
            {day: 10},
            {day: 11},
            {day: 12},
            {day: 13},
            {day: 14},
            {day: 15},
            {day: 16},
            {day: 17},
            {day: 18},
            {day: 19},
            {day: 20},
            {day: 21},
            {day: 22},
            {day: 23},
            {day: 24},
            {day: 25},
            {day: 26},
            {day: 27},
            {day: 28},
            {day: 29},
            {day: 30},
            {day: 31}
        ]  
    },
    {
        "name": "August", 
        "slug": "Aug",
        "days": [
            {day: 1},
            {day: 2},
            {day: 3},
            {day: 4},
            {day: 5},
            {day: 6},
            {day: 7},
            {day: 8},
            {day: 9},
            {day: 10},
            {day: 11},
            {day: 12},
            {day: 13},
            {day: 14},
            {day: 15},
            {day: 16},
            {day: 17},
            {day: 18},
            {day: 19},
            {day: 20},
            {day: 21},
            {day: 22},
            {day: 23},
            {day: 24},
            {day: 25},
            {day: 26},
            {day: 27},
            {day: 28},
            {day: 29},
            {day: 30},
            {day: 31}
        ]  
    },
    {
        "name": "September", 
        "slug": "sep",
        "days": [
            {day: 1},
            {day: 2},
            {day: 3},
            {day: 4},
            {day: 5},
            {day: 6},
            {day: 7},
            {day: 8},
            {day: 9},
            {day: 10},
            {day: 11},
            {day: 12},
            {day: 13},
            {day: 14},
            {day: 15},
            {day: 16},
            {day: 17},
            {day: 18},
            {day: 19},
            {day: 20},
            {day: 21},
            {day: 22},
            {day: 23},
            {day: 24},
            {day: 25},
            {day: 26},
            {day: 27},
            {day: 28},
            {day: 29},
            {day: 30}
        ]  
    },
    {
        "name": "October", 
        "slug": "oct",
        "days": [
            {day: 1},
            {day: 2},
            {day: 3},
            {day: 4},
            {day: 5},
            {day: 6},
            {day: 7},
            {day: 8},
            {day: 9},
            {day: 10},
            {day: 11},
            {day: 12},
            {day: 13},
            {day: 14},
            {day: 15},
            {day: 16},
            {day: 17},
            {day: 18},
            {day: 19},
            {day: 20},
            {day: 21},
            {day: 22},
            {day: 23},
            {day: 24},
            {day: 25},
            {day: 26},
            {day: 27},
            {day: 28},
            {day: 29},
            {day: 30},
            {day: 31}
        ]  
    },
    {
        "name": "November", 
        "slug": "Nov",
        "days": [
            {day: 1},
            {day: 2},
            {day: 3},
            {day: 4},
            {day: 5},
            {day: 6},
            {day: 7},
            {day: 8},
            {day: 9},
            {day: 10},
            {day: 11},
            {day: 12},
            {day: 13},
            {day: 14},
            {day: 15},
            {day: 16},
            {day: 17},
            {day: 18},
            {day: 19},
            {day: 20},
            {day: 21},
            {day: 22},
            {day: 23},
            {day: 24},
            {day: 25},
            {day: 26},
            {day: 27},
            {day: 28},
            {day: 29},
            {day: 30},
        ]  
    },
    {
        "name": "December", 
        "slug": "Dec",
        "days": [
            {day: 1},
            {day: 2},
            {day: 3},
            {day: 4},
            {day: 5},
            {day: 6},
            {day: 7},
            {day: 8},
            {day: 9},
            {day: 10},
            {day: 11},
            {day: 12},
            {day: 13},
            {day: 14},
            {day: 15},
            {day: 16},
            {day: 17},
            {day: 18},
            {day: 19},
            {day: 20},
            {day: 21},
            {day: 22},
            {day: 23},
            {day: 24},
            {day: 25},
            {day: 26},
            {day: 27},
            {day: 28},
            {day: 29},
            {day: 30},
            {day: 31}
        ]  
    },
];

import React from 'react';
import { Text, StyleSheet, View, Dimensions } from 'react-native';
import Br from '../components/Br';
import Bold from '../components/Bold';
import IndentList from '../components/IndentList';
import Italic from '../components/Italic';

const styles = StyleSheet.create({
    bold: {
        fontFamily: 'lato-heavy'
    },
    text: {
        fontFamily: 'lato-regular',
        fontSize: 16,
        paddingHorizontal: 10
    },
    textInner: {
        paddingHorizontal: 10,
    },
    bodyText: {
        color: 'black',
        fontFamily: 'lato-regular',
        fontSize: 16
    },
});

export const devotionContent = {
    January: [
        {
            day: 22,
            topic: "A New Beginning",
            wordStudy: "Jeremiah 9: 23-24",
            confession: <Text style={styles.text}>The Lord will increase me greatly this year, in the name of Jesus. Amen.</Text>,
            fullText: <Text style={styles.text}>
                God says: <Text style={styles.bold}>"Let not the wise man boast of his wisdom or the strong man
                boast of his strength or the rich man boast of his riches, but let him who
                boasts boast about this: that he understands and knows Me" (Jeremiah
                9:23-24 NIV).</Text> The truth is, <Br /> if you don't know God, it doesn't matter how much
                money you have in your bank account, or what diplomas hang on your wall, or
                what position you hold in a company. Until you have a relationship with God,
                you haven't really begun to live! And part of getting to know Him is learning
                the truth about yourself.
                <Br />
                After witnessing the miracle-working power of Christ, Peter
                acknowledged, <Bold>"I am a sinful man!" (Luke 5:8 NIV) </Bold> When the prophet
                Isaiah saw the Lord sitting upon His throne, he cried, <Bold>"Woe is me!" (Isaiah
                6:5 NKJV)</Bold> But God doesn't tell you the truth about yourself and then leave
                you that way. No, like a good doctor, He tells you you're sick so that you can
                get the proper treatment. And the proper treatment for sin is salvation through
                the blood of Jesus. You will never know God until you are related to Him
                through Jesus Christ. So if you've never accepted Him as your Saviour, start
                this New Year by praying:
                <Br />
                Lord, I repent and turn from my sin. I place my life in Your hands, trusting
                You as my Lord and Saviour. By faith I receive the gift of eternal life. Starting
                today, I ask You to lead and guide me and fulfil Your will through me. In Jesus'
                name I pray. Amen. Happy New Year!
            </Text>,
            prayerPoints: [
                "Father, let me experience Your goodness and mercy this year, in the name Jesus.",
                "I shall not suffer loss in any form this year, in the name of Jesus.",
                "Father, grant me the grace to walk in the ways of the Lord this year, in the name of Jesus.",
            ]
        },
        {
            day: 23,
            topic: "The Roller Coaster Ride of Life (1)",
            wordStudy: "1 John 5:1-5",
            confession: "By faith, my walls of Jericho shall fall, in the name of Jesus (Hebrew 11:30).",
            fullText: <Text style={styles.text}>
                C.S. Lewis, author of The Chronicles of Narnia, also wrote a book imagining
                what it's like to be a devil who wants to stop a Christian loving God. In The
                Screwtape Letters, he writes this: "Do not be deceived, Wormwood. Our
                cause is never in greater danger than when a human, no longer desiring, but
                still intending to do our Enemy's will, looks around upon a universe from
                which every trace of Him seems to have vanished, asks why he has been
                forsaken, and still obeys!" The point C.S. Lewis is making is that the devil
                loses it when we choose to follow God no matter what. When nothing seems
                to make sense, when it feels like God hasn't said 'Hello' in a while, when
                everything else screams at us to throw off this whole silly God-nonsense, but
                still we choose to go on trusting…that's the kind of faith, the devil can't chip
                away. That's the kind of faith-proven gold in the eyes of God.
                <Br />
                So, you mean, when it feels like everything has gone dark, I'm actually
                proving my faith is gold? EXACTLY. Textbook example: Jesus. Where does
                Jesus' ministry begin? In front of a huge cheering crowd? Far from it: It
                begins in the dark, dry place of the desert. Do you feel a bit stuck out in the
                desert? Perhaps, that's where you're supposed to be.
            </Text>,
            prayerPoints: [
                "I reject faith-crushing problems, in the name of Jesus.",
                "Let the satanic activities over my life be paralysed, in the name of Jesus.",
                "The Lord shall continue to uphold me in all my ways, in the name of Jesus.",
            ]
        },
        {
            day: 24,
            topic: "The Roller Coaster Ride of Life (2)",
            wordStudy: "Daniel 3:23-30",
            confession: "Confession: The Lord shall deliver me form unreasonable and wicked men, in the name of Jesus (2 Thessalonica 3:2). Amen.",
            fullText: <Text style={styles.text}>
                Shadrach, Meshach, and Abednego. It sounds like a pretty expensive law firm
                at the top of a shiny office building, doesn't it? Well, these three weren't
                lawyers, but they were firm on God's law. You see, a foreign king had ideas
                above his station; he wanted people to bow down to him as if he was some sort
                of a living God. This was clearly against God's law. So when our Jewish trio
                refused to bend a knee in the king's direction, they had a problem. A problem
                punishable by death, and not just any death. Death in an oversize pizza oven!
                <Br />
                So, what should our guys do:
                <Br />
                <Bold>a.</Bold> Apologize quick-smart and get on with the business of grovelling? 
                <Br sinlge/>
                <Bold>b.</Bold> Make a run for it; go find a place to live where the ruler hasn't got such an inflated ego?
                <Br single />
                <Bold>c.</Bold> Or stick to their principles and burn for the sake of it?
                <Br />
                Our heroes chose the third option. Oh! Come on, what good are principles
                when your body's about to go up in James. So what was going through their
                minds? Well... in Daniel 3:17 NKJ the guys say, <Bold>"Our God... is able to
                deliver us."</Bold> They never doubted God's ability. <Bold>"And He will deliver us."</Bold>
                They never doubted God's intention. <Bold>"But if not... we will not... worship the
                golden image"</Bold> (Daniel 3:18 NKJ). They never considered an alternative to
                God. They had to go through to get through. And so should you.
            </Text>,
            prayerPoints: [
                "Lord, deliver me from the hand of the wicked people, in the name of Jesus.",
                "Every weapon of warfare fashioned against me shall not prosper, in the name of Jesus.",
                "Let the name of the Lord be glorified in my life, in the name of Jesus.",
            ]
        },
        {
            day: 25,
            topic: "The Roller Coaster Ride of Life (3)",
            wordStudy: "Deuteronomy 31:7 & 8",
            confession: "All things shall work for my good in all areas this year, in the name of Jesus (Romans 8:28).",
            fullText: <Text style={styles.text}> You would be a bit "wisdom-challenged" to unstrap yourself from the safety
                rails of a roller coaster whilst it was still 50 metres in the air. Sure, your
                digestive system may be telling you to get off the ride, but you can't bail out
                mid-ride, can you? Well, ditto for your faith life. Just because you're feeling a
                bit sick to the stomach of Church, or other Christians make you want to throw
                up, you'd be a little bit dumb to bail out on God now. You might want to give
                up God, but God won't give up on you. He promises, He will <Bold>"...never leave
                you nor forsake you [so] ...do not be discouraged" (Deuteronomy 31:8
                NIV).</Bold>
                <Br />
                You might not have faith in God, but God is still faithful to you. His Word
                says, "If we are faithless, He will remain faithful, for He cannot disown
                Himself" <Bold>(2 Timothy 2:13 NIV).</Bold> God will get you through it and out of it. He
                promises us an exit. <Bold>"...When you are tempted, He will also provide a way
                out so that you can stand up under it" (1 Corinthians 10:13 NIV).</Bold> You see
                bad; God makes good. He's producing good in all your circumstances. <Bold>"And
                we know that all things work together for good to them ...who are the
                called..." (Romans 8:28 KJV).</Bold> God puts pits and peaks together, and out
                comes your good.
                So stay strapped in for the ride
            </Text>,
            prayerPoints: [
                "Lord, make a way for me, in the name Jesus.",
                "I shall testify, in the name of Jesus.",
                "Grant me the grace to overcome my challenges, in the name of Jesus.",
            ]
        },
        {
            day: 26,
            topic: "Don't Keep Your Love in a Box",
            wordStudy: "Mark 14:3-9",
            confession: "I will offer to thee the sacrifice of thanksgiving, and will call upon the name of the LORD (Psalm 116:17).",
            fullText: <Text style={styles.text}>A woman came with an alabaster jar of very expensive perfume... She broke
                the jar and poured the perfume on His head. Some of those present were saying
                indignantly to one another, <Bold>"Why this waste of perfume? It could have been
                sold... and the money given to the poor."</Bold> And they rebuked her harshly.
                <Bold>"Leave her alone,"</Bold> said Jesus, <Bold>"she has done a beautiful thing... The poor
                you will always have with you... But you will not always have Me. She did
                what she could. She poured perfume on My body beforehand to prepare
                for My burial. I tell you... wherever the gospel is preached throughout the
                world, what she has done will also be told, in memory of her" (Mark 14:3-
                9 NIV).</Bold>
                <Br />
                She could've given the money to the poor. It would've been a 'Christian'
                thing to do, wouldn't it? I mean, a jar of perfume 'wasted' on a man. What did
                Jesus want to smell like an aromatherapy shop for, anyway? The partypoopers
                so miss the point in this. The woman gave the best she had to offer for
                Jesus. This is real sacrificial worship: giving your best to Jesus. What about
                you? What's your best? What can you sacrificially give to worship Jesus? The
                woman gave what she could, when she could. What love have you got to pour
                out of your jar today?
            </Text>,
            prayerPoints: [
                "Father, grant me the grace to love you to the end, in the name of Jesus.",
                "Lord, make me a blessing to my generation, in the name of Jesus.",
                "My sacrificial giving shall be acceptable to the Lord, in the name of Jesus.",
            ]
        },
        {
            day: 27,
            topic: "Always Take the High Road",
            wordStudy: "Luke 6:28-31",
            confession: "The Lord shall deliver me from my enemies who are stronger than me, in the name of Jesus (Psalm 18:17). Amen.",
            fullText: <Text style={styles.text}>It's easy to get too familiar with Jesus' words and therefore lose the sheer
                impact of what He's saying. Take this one for starters: <Bold>"Pray for those who
                spitefully use you. To him who strikes you on the one cheek, offer the
                other also. And from him who takes away your cloak, do not withhold
                your tunic either... just as you want men to do to you, you also do to them
                likewise" (Luke 6:28-31 NKJ).</Bold> Whoaa! Let's backtrack a minute. What
                kind of planet is Jesus living on when He says this? Offer your other cheek to
                be punched! Yeah and why not draw a target on your pants so they won't miss
                when they kick you there? Giving away your cloak: What? Are we supposed
                to mug ourselves as well? What is Jesus going on about?
                It's not about mugging yourself or allowing yourself to be treated like
                dirt. Jesus is talking about setting yourself a standard here. And, oh boy, is it a
                high standard! If someone strikes you, you want to hurt them back, but the
                new standard won't allow it. In fact, the new standard isn't even saying you
                should walk away and forget it. We're now required to pray good things and
                give good things to our enemy. How radical is that!
            </Text>,
            prayerPoints: [
                "Father, grant me a new heart for the work of the Kingdom, in the name of Jesus.",
                "I receive grace to forgive my enemies, in the name of Jesus.",
                "My enemies shall not prevail over me, in the name of Jesus.",
            ]
        },
        {
            day: 28,
            topic: "The Call to Love (1)",
            wordStudy: "Ephesians 6:1-4",
            confession: "The love of God for my family shall be planted in my heart, in the name of Jesus.",
            fullText: <Text style={styles.text}>Hey, here's an idea... Why not take your television out for a romantic walk
                down to the park? Yeah, then you could stroll down to the mall, just you and
                your 102cm widescreen walking hand in hand. You could treat her to some
                new batteries for her remote. Take her HD tuner out for something nice to eat.
                Cuddle up in the back row of the flicks, just you and your high definition,
                surround sound technological beauty. No, seriously, why not? Don't you have
                to make out time for the people you love? Er... one problem, a television isn't
                a person! So why do you spend four hours a day staring lovingly into its eyes,
                and on the other hand, spend just fifteen minutes with your Mum? Why do
                you say you value your mates but then place more value on your iPod? Why do
                you say you love Jesus but never find time to talk to Him because you're too
                busy spending time, money and energy on other things that will pass away
                with this world?
                <Br />
                This might seem harsh but maybe this is "wake-up" time for you. Are you
                wasting too much time on gadgets and gizmos at the expense of people?
                Here's an idea: Unplug the Xbox, sign off Facebook, put down the ear phones
                and open your mouth. Go and see your Grandma and have a good chat with
                her. Treat your mate to something unexpected. Go for a walk with a neighbour.
                Stop lazing around and start loving.
            </Text>,
            prayerPoints: [
                "Father, help me to take care of my family, in the name of Jesus.",
                "I shall not be distracted from my family members, in the name of Jesus.",
                "My family shall be great, in the name of Jesus.",
            ]
        },
        {
            day: 29,
            topic: "The Call to Love (2)",
            wordStudy: "Romans 15:2",
            confession: "I shall not be a reproach to my neighbour, in the name of Jesus. Amen.",
            fullText: <Text style={styles.text}>Right, so this is a bit of a morbid thought, but imagine Martians from the
                Inter-planetary Highways Agency land on earth to announce our lovely
                planet is to be destroyed to make room for a new inter-galactic motorway.
                You've got one month to live before life as we know it ends. What do you do
                with your remaining 30 days here on this little spherical world? Do you put in
                a few extra hours in on cracking level 17 of Grand Theft Auto 12? Do you put
                in the extra shifts at work so you'll finally be able to afford those to-die-for
                boots or the deposit on the car of your dreams? Hopefully none of these
                things would be high on your to-do list in the face of immediate disaster.
                <Br />
                So, here's the scary news. That day will one day arrive. OK, it won't
                involve Martians with clipboards and hard hats, but one day your time on this
                little ball of land and sea will be up. Will you have spent your time wisely?
                Will you have made a difference to the world? Will people miss you? Will a
                part of this universe be a little less lovely because you are gone? Will people
                notice or will it be business as usual? You only get one life. Live it. Live a life
                of love.
            </Text>,
            prayerPoints: [
                "Father, grant me the grace to use my time wisely, in the name of Jesus.",
                "Grant me the grace to be a vessel unto honour for Your glory, in the name of Jesus.",
                "I shall not live a wasted life; my life shall be to the glory and praise of my Maker, in the name of Jesus.",
            ]
        },
        {
            day: 30,
            topic: "God-goggles",
            wordStudy: "Numbers 13:26-33",
            confession: "I wil wait on the LORD and be of good courage, and He shall strengthen my heart; I will wait, I say, on the LORD, in the name of Jesus! (Psalm 27:14)",
            fullText: <Text style={styles.text}>"Two men looked through prison bars; one saw mud, the other stars." So
                which are you? A star-gazer or a mud-minder? Do you see life as a glass half
                empty or half full? Well, whatever you see, make sure you've got your Godgoggles
                on. No, you won't find them at Specsavers; God-goggles are given
                through prayer-life, reading the Word and being in partnership with God's
                people. God-goggles give you a vision for your life, through them you begin
                to see things as God sees them. Moses had a pair when he "saw" the Promised
                Land while trudging through the desert. The Israelites had "me-goggles" on
                and so only saw a hot, deserted hell. Moses saw that every journey begins with
                a single step, and that every other step is a significant step to the Promised
                Land. The Israelites saw an impossible journey, a task too big for their tired
                and blistered feet. Moses and the Israelites were on the same journey; the
                difference was perspective; the difference was vision. One saw stars, the
                others mud.
                <Br />
                What about you? Have you been given a promise by God but you can't
                work out what to do with it? Does the task seem too big for you? What a great
                opportunity to put on your God-goggles. Take your eyes up from the floor and
                set them to the stars. Remember every journey begins with that first step.
            </Text>,
            prayerPoints: [
                "I bind every spirit of fear in my life and ministry, in the name Jesus.",
                "I shall fulfill the promise and purpose of God for my life and ministry, in the name Jesus.",
                "Holy Spirit, open my spiritual eyes and ears, in the name Jesus.",
            ]
        },
        {
            day: 31,
            topic: "When You Feel Anxious (1)",
            wordStudy: "Matthew 6:25-34",
            confession: "The Lord shall grant me the grace to overcome worry and anxiety, in the name of Jesus (Philippians 4:6). Amen.",
            fullText: <Text style={styles.text}>Are you obsessed with trying to figure out things that should be left in God's
                hands? When you've been working through some questions but have come to
                the point of deciding what you are going to do, stop fretting and just trust
                God. And don't get stressed again a week later because you don't like the way
                He's handling it! Are you expecting God to just wave a magic wand over your
                life so that everything suddenly becomes all neat and tidy? Maybe as He
                takes you through the process of sorting out the mess you'll discover valuable
                things hidden and forgotten.
                <Br />
                Paul says, <Bold>"Do not be anxious about anything..." (Philippians 4:6
                NIV).</Bold> That's hard to swallow if you're a bit insecure or you like to organise
                things; to find a place to put them so they make sense and feel "taken care of."
                But how do you file unanswered questions? They annoy you like a crackly
                earpiece in your headphones or a scratch on your favourite CD! But when
                you have a crackle or a scratch in your faith, you can't just go down the shops
                and get a replacement. Instead you have to talk to God about it and ask Him
                what He's going to do about it. You might find out that the things you think are
                high priority may not be anywhere near the top of God's list for your life.
                <Br />
                So learn to live with loose ends and trust God! If you want growth, you've
                got to be at peace with the messiness and mistakes that accompany it. If
                you're one of those people who needs to put everything into a neat little box,
                this Scripture is especially for you: <Bold>Trust in the Lord with all your heart;
                do not depend on your own understanding. (Proverbs 3:5 NLT).</Bold>
            </Text>,
            prayerPoints: [
                "I receive daily help from God, in the name of Jesus.",
                "I shall not be put to shame in my life and ministry, in the name of Jesus.",
                "Lord, intervene in my matter, in the name of Jesus.",
            ]
        }        
    ],
    February: [
        {
            day: 1,
            topic: "When You Feel Anxious (2)",
            wordStudy: "Philippians 4:10-13",
            confession: "The LORD is on my side; I will not fear. What can man do to me? (Psalm 118:6)",
            fullText: <Text style={styles.text}>It's so easy to get a dose of the "worry bug". Maybe, you've got it now; the
                symptoms are easy enough to spot:
                <Br />
                <Bold>1.</Bold>    Your bowels seem to be employing more workers on the dayshift.
                <Br single/>
                <Bold>2.</Bold>    Your head seems to be inviting as many irrational thoughts as possible to
                the big crisis meeting it's holding.
                <Br single/>
                <Bold>3.</Bold>    Your self-esteem seems to have plummeted to an all-time low.
                <Br />
                So what's the cure? Digging a hole in the patio and burying your head in
                the cement? This avoids the issue rather than tackles it. No, it's time to be
                brave and face up to your worries. Come on, you can do it; splash your face
                with some cold water, slap yourself and say, "I can do it!" twenty times in the
                bathroom mirror (probably best wait till everyone is out of the house first).
                <Br />
                The Bible says: <Bold>"The righteous are bold as a lion" (Proverbs 28:1
                NIV).</Bold> Each time you start to confront your worries, you take a step forward.
                Each time you choose not to stress over your worry bug, but look to what
                needs to change in order to overcome it, you take a step forward. Nobody
                enjoys confrontation but without confronting your worries you won't be able
                to move on. So be brave: Kick the worry bug.
            </Text>,
            prayerPoints: [
                "I can do all things through Christ who strengthens me, in the name of Jesus.",
                "Lord, grant me the grace to overcome anxiety, in the name of Jesus.",
                "I receive grace and victory over fear and anxiety, in the name of Jesus.",
            ]
        },
        {
            day: 2,
            topic: "When You Feel Anxious (3)",
            wordStudy: "Proverbs 4:20-22",
            confession: "God's will for me, above all things, is that I prosper and be in health, even as my soul prospers—and so shall it be, in the name of Jesus (3 John 2).",
            fullText: <Text style={styles.text}>Dr. Restoration Daily Devotional is guessing you wimped out on taking
                yesterday's advice in trying to kick the worry bug. As your unofficial,
                unqualified GP, Dr Restoration Daily Devotional would like to warn you
                once again that worry can seriously damage your health. It's medically
                proven that worry suppresses your immune system; worry elevates blood
                pressure, which in turn creates cholesterol that blocks your arteries. So, if you
                weren't worried before, you are now! John writes: <Bold>"I pray that... you may
                prosper and be in good health, just as your soul prospers" (3 John 1:2
                NAS).</Bold> Why would he say that unless there's a connection between your body
                and your spirituality?
                <Br />
                Before you think, "This is getting a little too hug-a-tree-hippy for my
                liking," stop and think about it. When you worry, your mind is far too loud
                and busy to hear the still small voice of God, so you're robbed of peace. When
                you worry your physical body is more prone to illness, so you're not fighting
                fit when it comes to spiritual opposition. Can you see where this is heading?
                Worry is a spiritual matter. Anxiety is a weapon the enemy is firing at you to
                stop you from receiving all that God has for you: <Bold>"Listen closely to My Word."</Bold>
            </Text>,
            prayerPoints: [
                "Let the Word of God dwell richly in my life, in the name of Jesus.",
                "Let the joy of the Lord be my strength, in the name of Jesus.",
                "I reject every spirit of fear in my life and ministry, in the name of Jesus.",
            ]
        },
        {
            day: 3,
            topic: "Make Yourself at Home in the Lord (1)",
            wordStudy: "John 15:1-10",
            confession: "Confession: I who dwell in the secret place of the Most High shall abide under the shadow of the Almighty (Psalm 91:1).",
            fullText: <Text style={styles.text}>If our need for a relationship with God is so deep and constant, why do we fail
                to pursue it? One reason is, we're convinced He remembers all the bad we've
                done and is quick to judge us for how we're doing now. That's not so. Think of
                the qualities of a best friend: they accept you and make time for you; you
                always leave their presence feeling better. What you appreciate in a best
                friend is exactly what God offers you today. He wants to abide with you even
                more than you want to abide with Him. <Bold> "As the Father loved Me, I also
                have loved you; abide in My love" (John 15:9 NKJV).</Bold> If we were really
                abiding in His love we'd come away feeling so cherished that we'd rush back
                to Him each day.
                <Br />
                The word abiding means to "be at home" in God's love; to be convinced
                of your acceptance as a member of His family in good standing; to be at peace
                there, to be fed and nurtured by His presence, to feel protected knowing it's
                the safest place on earth you'll ever be. Abiding is all about the most
                important relationship in your life. In abiding you seek, long for, thirst for,
                wait for, see, know, love, hear and respond to Him. Abiding means more of
                Him in your activities, thoughts and desires. Too often in our rush to perform
                for God we simply fail to enjoy His company. Yet we were created to be
                incomplete with anything less. That's why David wrote, <Bold>"As the deer pants
                for the water brooks, so my soul pants for You, O God" (Psalm 42:1
                NAS).</Bold>
            </Text>,
            prayerPoints: [
                "Let the love of God be planted in my heart, in the name of Jesus.",
                "Lord, separate me from ungodly people, in the name of Jesus.",
                "Father, don't let me depart from Your presence, in the name of Jesus.",
            ]
        },
        {
            day: 4,
            topic: "Make Yourself at Home in the Lord (2)",
            wordStudy: "Psalm 63:1-5",
            confession: "Confession: The Lord shall hear me as I call upon Him, in the name of Jesus (Psalm 66:18).",
            fullText: <View> 
                <Text style={styles.text}>
                    To make yourself at home in God's love you must:
                    <Br />
                </Text>

                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            Give Him the first part of your day. The Psalmist wrote, <Bold>"...Early will I
                            seek You..." (Psalm 63:1 NKJV).</Bold> If you don't learn to give God the
                            first part of your day you're unlikely to break through to a deeper
                            relationship with Him. You must set aside a specific time and place each
                            morning where you can read and write comfortably, think, study, talk to
                            God out loud, and yes, weep if you need to.
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            Relish His every Word to you. Receive what He says like food, like
                            treasure, like a love letter. Remember, you're reading in order to meet
                            someone. Your goal is not information, it's intimacy. Stop and ponder
                            what you've read. Let it go down into the core of your being and expect
                            Him to interact with you. He will!
                        </Text>
                    },
                    {
                        serial: 3,
                        body: <Text>
                            Talk and listen to a person—not an invisible force. We treat God as if He
                            were some mystical force "out there". No, He wants you to talk to Him
                            like you would a friend; to hear your requests, your worries, and your
                            thanks. Go ahead, risk being honest and expect His insight in return.
                            Take time to be still before Him. Stay there until you 'connect' with Him.
                        </Text>
                    },
                    {
                        serial: 4,
                        body: <Text>
                            Write down your thoughts and impressions; not a diary of your day or
                            an attempt at literature, but a record of your personal walk with God.
                            Share with Him your disappointments. Ask for His wisdom. Leave your
                            request on the page until you receive guidance. Keep track of His
                            answers.
                        </Text>
                    },
                ]} />

                <Text style={styles.text}>
                    These simple practices are called "disciplines" because they take
                    effort—but the reward is worth it.
                </Text>
            </View>,
            prayerPoints: [
                "Father, grant me the grace to know You, in the name of Jesus.",
                "Grant me the grace to walk with You, in name of Jesus.",
                "Father, let Your grace be sufficient for me, in the name of Jesus.",
            ]
        },
        {
            day: 5,
            topic: "Make Yourself at Home in the Lord (3)",
            wordStudy: "Isaiah 1:19-20",
            confession: "The Lord shall increase me on all sides this year, in the name of Jesus. Amen.",
            fullText: <View>

                <Text style={styles.text}>
                    If abiding is the key to abounding, why don't we achieve it? Because:
                    <Br />
                </Text>

                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            We think abiding is based on our feelings. No, it's about a relationship,
                            not a sensation. That'll probably come as a relief, especially if you think
                            you must have an emotional rush when you spend time with God. You
                            won't always, and you don't need to. We understand this in our
                            marriages. Abiding is an act of faith. It says you value God's presence in
                            your life more than any immediate sensation. If you think you must
                            always have strong feelings in order to know you've been with God,
                            you'll be disappointed and conclude, "This isn't for me." But it is!
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            We think we can abide in Christ without obeying Him. Jesus said, <Bold>"If you
                            keep My commandments, you will abide in My love..." (John 15:10
                            NKJV).</Bold> Disobeying creates a breakdown in your fellowship with
                            Christ. You can enjoy an emotional worship service on Sunday but, if
                            you pursue a sinful lifestyle during the week, you'll never succeed at
                            abiding.
                        </Text>
                    },
                ]} />
                <Text style={styles.text}>
                    One of the greatest pictures of abundant living is the picture of what the
                    spies found in the Promised Land. In the Bible, grapes, which produce wine,
                    represent abundance. <Bold>"They came to the Valley of Eshcol, and there cut
                    down a branch with one cluster of grapes; they carried it between two of
                    them on a pole..." (Numbers 13:23 NKJV).</Bold> Think: it took two of them to
                    carry just one cluster. Wow! Have you ever heard of such abundance? Keep
                    that picture in mind. Remind yourself constantly that it belongs to those who
                    hear and obey God's Word!
                </Text>
            </View>,

            prayerPoints: [
                "Father, don't let sin have dominion over me, in the name of Jesus.",
                "Grant me the grace to live a righteous life, in the name of Jesus.",
                "I will fulfill God's purpose for my life, in the name of Jesus.",
            ]
        },
        {
            day: 6,
            topic: "The Blood of Jesus",
            wordStudy: "Colossians 1:20-22",
            confession: "By the blood of Jesus, I am an overcomer over all the power of the enemy, in the name of Jesus (Revelation 12:11).",
            fullText: <Text style={styles.text}>Jeffrey Ebert says: "When I was five years old, before factory-installed seat
                belts and automobile airbags, my family was driving home at night on a twolane
                country road. I was sitting on my mother's lap when another car driven
                by a drunk driver swerved into our lane and hit us head on. I don't have any
                memory of the collision. I do recall the fear and confusion I felt as I saw
                myself literally covered with blood from head to toe. Then I learned that the
                blood wasn't all mine, but my mother's. In that split second when the two
                headlights glared into her eyes, she instinctively curled her body around
                mine. It was her body that slammed against the dashboard, her head that
                shattered the windshield. She took the impact so that I wouldn't have to. It
                took extensive surgery for my mother to recover from her injuries."
                <Br />
                At the Cross Jesus took the impact of our sin, and the moment we place
                our trust in Him, His shed blood reconciles us to God. Consider God's
                problem. The Bible says God is <Bold> "of purer eyes than to behold evil, and
                cannot look on wickedness..." (Habakkuk 1:13 NKJV).</Bold> So God solves
                His problem by repositioning us "in Christ" and seeing us "through the
                blood." The songwriter wrote: "When God looks at me He doesn't see the
                things I've done, He only sees the blood of His crucified Son." God's Word
                says, <Bold>"The blood ...makes atonement for the soul" (Leviticus 17:11
                NKJV).</Bold> The word atonement means "at-one-ment." Wonderful, the blood of
                Jesus bridges the gap and makes us one with God!
            </Text>,
            prayerPoints: [
                "Lord Jesus, thank You for Your blood that You shed for us on the Cross of Calvary.",
                "By the blood of Jesus, I am justified, in the name of Jesus.",
                "Father, let the blood of Jesus cleanse me from all my sins, in the name of Jesus.",
            ]
        },
        {
            day: 7,
            topic: "Strive for Excellence",
            wordStudy: "Daniel 6:1-5",
            confession: "I can do all things through Christ who strengthens me, in the name of Jesus (Philippians 4:13).",
            fullText: <View>

                <Text style={styles.text}>
                    Daniel is remembered for three things:
                </Text>

                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            Standing by divine principles even when it lands you in the lions' den.
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            Hearing God's Word and recording it.
                        </Text>
                    },
                    {
                        serial: 3,
                        body: <Text>
                            And having an attitude that strives for excellence in all things.
                        </Text>
                    },
                ]} />
                <Text style={styles.text}>
                    John Gardiner said, "The society which scorns excellence in plumbing
                    because plumbing is a humble activity, and tolerates shoddiness in
                    philosophy because it is an exalted activity, will neither have good plumbing
                    nor good philosophy. Neither its pipes nor its theories will hold water."
                    Horace Mann was one of America's greatest educators. A lawyer, not a
                    teacher, he entered politics in 1837 and became president of the
                    Massachusetts State Senate.
                    <Br />
                    As a visionary he saw great possibilities for developing a public
                    education system for the nation. His efforts resulted in Massachusetts
                    creating the nation's first Board of Education as "an experiment." When the
                    Board's leadership position was offered to him, his friends, who believed his
                    political career might culminate in the US Presidency, urged him to decline.
                    But Mann took the job. He told his disappointed friends, "If the title is not
                    sufficiently honourable, then clearly it is left to me to elevate it!" Not only did
                    the job have little prestige, the $1,500 annual salary was only a fraction of
                    what he'd have earned as a lawyer. Concerning this he said, "Only this is
                    certain: if I live and have good health, I'll do more than $1,500 worth of
                    good." He did. From his position, he gave Massachusetts a public school
                    system that other states adopted, enriching the lives of millions of children for
                    years to come. So, strive for excellence!
                </Text>
            </View>,
            prayerPoints: [
                "Father, let the spirit of excellence be released upon me, in the name of Jesus.",
                "Let every obstacle to my progress be removed, in the name of Jesus.",
                "Holy Spirit, help me to discipline myself, in the name of Jesus.",
            ]
        },
        {
            day: 8,
            topic: "Don't Strike Back",
            wordStudy: "Matthew 5:38-42",
            confession: "The Lord shall grant me grace to follow peace with all people, in the name of Jesus (Hebrews 12:14).",
            fullText: <Text style={styles.text}>Do you feel taken for granted? You thought they cared about you, but they
                were just “using” you. That's what happens when we put our lives into the
                hands of people. Men and women are not creators; they can't make anything
                out of us, they can only use us. You snapped at your kids. You yelled at a coworker.
                You screamed at the driver who cut you off. You fought with your
                mate again; now you're pouring out your frustrations to anybody who'll listen.
                Ever think that perhaps your anger is just a by-product of the resentment
                you've allowed to invade your soul? That's hard to acknowledge, but you
                won't get on top of the situation till you do. The answer's not to take it out on
                those around you; it's to take it to the Lord in prayer and let Him help you. It's
                got to start on the inside before it shows outside. That means spending time
                with God, allowing Him to forgive your resentments, remove your pain, heal
                your memories and enable you to love; as He loves. And that's a job for God.
                Don't try it without Him!
                <Br />
                Why don't you pray this prayer: “Father, I want to act in love, not react in
                anger. Instead of being short-tempered and striking back, help me to be patient
                and turn the other cheek. Today let Your love rule my life, for Christ's sake.
                Amen.” That's a prayer God will answer, for His Word says, <Bold>“…Above all
                things have fervent love for one another; love will cover a multitude of
                sins”</Bold>
            </Text>,
            prayerPoints: [
                "Father, grant me the grace to manifest the fruit of the Spirit, in the name of Jesus.",
                "Help me to serve You in spirit and in truth, in the name of Jesus.",
                "Lord, grant me the grace to endure to the end, in the name of Jesus.",
            ]
        },
        {
            day: 9,
            topic: "Paul's Thoughts on Temptation",
            wordStudy: "1 Corinthians 6:6-12",
            confession: "Blessed is the man that endureth temptation: for when he is tried, he shall receive the crown of life, which the Lord hath promised to them that love him (James 1:12).",
            fullText:<View>

                <Text style={styles.text}>
                    Paul gives us four thoughts to keep in mind when it comes to temptation:
                </Text>

                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            Where we consider ourselves strongest, is where we're least likely to
                            prepare for attack. One man tells of getting involved in an affair. He
                            recalls a conversation he had with a friend before it happened.
                            Friend: “If Satan were to blow you out of the water, how do you think he'd
                            do it?”
                            <Br single/>
                            Man: “In all sorts of ways…but there's one way he wouldn't get me.”
                            Friend: “What's that?”
                            <Br single/>
                            Man: “My personal relationships. That's one area where I'm as strong as I
                            can get.”
                            <Br single/>
                            A few years later in the very area where he predicted he was safe, this
                            man's life and marriage fell apart. Paul warns: <Bold>“Let him who thinks he
                            stands take heed lest he fall…” (1 Corinthians 10:12 NKJV).</Bold>
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            What seems innocent, can be destructive. Addressing the temptations of
                            the flesh, Paul writes: <Bold>“Everything is permissible for me-but not
                            everything is beneficial. Everything is permissible for me-but I will
                            not be mastered by anything” (1 Corinthians 6:12 NIV).</Bold> Stop and ask
                            yourself, “Will this benefit or hurt me? Does it have the power to control
                            me?”
                        </Text>
                    },
                    {
                        serial: 3,
                        body: <Text>
                            Flirting with temptation guarantees that at some point you'll give in to it.
                            Again Paul weighs in: <Bold>“…Don't you know that a little yeast works
                            through the whole batch of dough?” (1 Corinthians 5:6 NIV).</Bold> Paul
                            isn't talking about cooking, but about carnality, and his advice is:
                            “Recognise it, avoid it, eliminate it from your life!”
                        </Text>
                    },
                    {
                        serial: 4,
                        body: <Text>
                            Failure won't cut you off from God's grace, but it can destroy your
                            potential. With that in mind, Paul says: <Bold>“…I discipline my body…lest,
                            when I have preached to others, I myself should become
                            disqualified…”</Bold>
                        </Text>
                    },
                ]} />
            </View>,
            prayerPoints: [
                "I receive the grace to overcome temptation, in the name of Jesus.",
                "Father, let my life reject manipulation, in the name of Jesus.",
                "I shall not fall into the trap of my enemies, in the name of Jesus.",
            ]
        },
        {
            day: 10,
            topic: "Are You Feeling Inadequate?",
            wordStudy: "2 Corinthians 3:5-6",
            confession: "Confession: All things are possible with God for me, in the name of Jesus (Matthew 19:26).",
            fullText: <Text style={styles.text}>God will never give you an assignment that does not require His involvement.
                He calls us: <Bold>“…Not according to our works [ability], but according to His
                own purpose and grace which was given to us in Christ Jesus…” (2
                Timothy 1:9 NKJV).</Bold> When God gives you a job to do, He gives you the grace
                to do it. But don't expect it to be easy. God called Moses to instruct Pharaoh to
                let His work force leave, to go worship a God Pharaoh didn't even believe in.
                He told Jonah to go to Nineveh, the most corrupt city in the world, and say to
                its population, 'Repent or die!' When He called Jeremiah to preach to a hardedged,
                self-centred people who refused to listen, he cried so hard that he
                became known as “the weeping prophet.” So if you're feeling inadequate
                today, without God—you are! The heroes in the Bible didn't jump up and say,
                “No problem, I can handle that!”
                <Br />
                But here's the good news: God doesn't call us to work for Him but with
                Him, and that guarantees your success. You say, “But I don't have the ability.”
                No problem: <Bold>“Not that we are sufficient of ourselves…our sufficiency is
                from God, who also made us sufficient…” (2 Corinthians 3:5-6 NKJV).</Bold>
                You say, “But I don't have the finances.” No problem: <Bold>“…God is able to
                make all grace abound to you, so that in all things at all times, having all
                you need, you will abound in every good work” (2 Corinthians 9:8 NIV).</Bold>
                You say, “But I don't have the strength.” No problem: <Bold>“I can do everything
                through Him who gives me strength” (Philippians 4:13 NIV).</Bold>
            </Text>,
            prayerPoints: [
                "I can do all things through Christ, in the name of Jesus.",
                "I reject every spirit of fear in my life, in the name of Jesus.",
                "Lord Jesus, let Your grace be sufficient for me always",
            ]
        },
        {
            day: 11,
            topic: "Spiritual Distance",
            wordStudy: "1 John 2:3-6",
            confession: "The joy of the Lord is my strength, in the name of Jesus (Nehemiah 8:10). Amen.",
            fullText: <Text style={styles.text}>Moses alone knew what God's presence felt like, what His voice sounded like
                and what His radiant glory looked like. And apparently the people of Israel
                were happy to leave it that way. They said to Moses, <Bold>“You speak with
                us…but let not God speak with us…” (Exodus 20:19 NKJV).</Bold> What were
                they afraid of, that God might tell them something they didn't want to hear?
                Hello? When you haven't carried out God's last set of instructions, it's hard to
                get excited about His next set. John writes: <Bold>“…We can be sure that we know
                Him if we obey His commandments. If someone claims, I know God, but
                doesn't obey God's commandments, that person is a liar and is not living
                in the truth. But those who obey God's word truly…love Him. That is
                how we know we are living in Him. Those who say they live in God should
                live their lives as Jesus did” (1 John 2:3-6 NLT).</Bold>
                <Br />
                So, are you content to be an onlooker, watching God move in the lives of
                others instead of accepting the discipline required to have a personal
                relationship with Him yourself? Do you want His gifts and His favour, but not
                the commitment that goes with them? God doesn't want you to be infatuated
                with the Bible or the church or His blessings. No, He wants you to fall in love
                with Him. He's not looking for a date; He's looking for a bride! He wants
                someone who'll stick with Him when the going gets tough. Are you ready to
                put on the ring of commitment today?
            </Text>,
            prayerPoints: [
                "Holy Spirit, tidy up my spiritual life, in the name of Jesus.",
                "Deliver me from every spirit of prayerlessness, in the name of Jesus.",
                "Grant me the grace to endure to the end, in the name of Jesus.",
            ]
        },
        {
            day: 12,
            topic: "In Harness with Jesus",
            wordStudy: "Matthew 11:28-30",
            confession: "Confession: I come to You, Lord Jesus. You are my Burden Bearer; give me rest from all my cares and issues (Matthew 11:28).",
            fullText: <Text style={styles.text}>Some of us find it easy to trust God for health, yet we stay awake all night
                worrying about finances. Or we trust Him for finances, but not to direct our
                steps. Jesus says, 'Take my yoke upon you and learn from Me.' You say,
                'Learn what?' Learn that if you want to lighten your load, don't take on
                more—take on a partner. Get in harness with the Lord and take your lead
                from Him. Working with God will restore your strength, not deplete it!
                <Br />
                Richard Mylander writes: “On my way to a conference in Colorado I was
                driving uphill along a major interstate, when I overtook a freight train going
                the same direction at a slower speed. The train was being pushed uphill by
                two locomotives that sounded as if they were straining at full power.
                <Br />
                “I'm a flatlander from the mid-west. 'Is this how trains move in
                mountainous terrain?' I wondered.
                <Br />
                “I gradually came alongside the front of the over one kilometre-long
                string of cars. There I found five more locomotives pulling the train. Seven
                engines in all! Where I come from, I rarely see more than two or three. That
                train was a lesson for me. I had been under serious strain for some time. I was
                feeling tired and was wondering whether I could persevere under the
                pressure. How like God, I thought. When I am pushing a load uphill with all
                the strength I have and feel like my energy level is depleted, He wants me to
                know that He is in the lead, pulling with power far greater than mine.”
            </Text>,
            prayerPoints: [
                "Let the power of the Lord fall upon me, in the name of Jesus.",
                "Lord, take over the affairs of my life, in the name of Jesus.",
                "Deliver me from the power of wicked people, in the name of Jesus.",
            ]
        },
        {
            day: 13,
            topic: "Naaman (1)",
            wordStudy: "2 Kings 5:1-3",
            confession: "Heal me, O LORD, and I shall be healed; save me, and I shall be saved: for thou art my praise (Jeremiah 17:14).",
            fullText: <Text style={styles.text}>In examining the story of Naaman the leper, we begin by looking at his
                condition. <Bold>“Naaman, commander of the army of…Syria, was a
                great…man…but a leper” (2 Kings 5:1 NKJV ).</Bold> In spite of his past
                victories and present honours, he had a hidden problem that wouldn't stay
                hidden long. Left undealt with, it would eventually destroy him. Can you
                relate? Today the odds may be in your favour. You've graduated from the best
                university, you've a good family and you've built a church or a business, or
                climbed to the top in your career.
                <Br />
                Yet before you can qualify for greater blessing, God will force you to deal
                with a condition that's hidden beneath your armour. Outstanding people in
                every occupation, particularly spiritual ones, go through it. It's what sets them
                apart. It's what ushers them across the bridge from mediocre to exceptional.
                Without obstacles we'll always be ordinary! We're not talking about petty little
                problems. We're talking about issues so overwhelming you can't sleep; gutwrenching
                things that cause your heart to skip a beat and make you fear, “This
                is the one that's going to take me out!” It's the thing you pray about in secret.
                It's what you don't want people to see. So, like Naaman, you wear your “brass”
                then go home and agonise over your condition.
                <Br />
                Understand this: God teaches some of His greatest lessons in the valleys
                of life. That's where you learn to lie prostrate before Him, weeping and
                broken. It's where you pray: “God, don't let this thing destroy me. Deal with it
                through the power of Your Spirit.” And here's the good news: that kind of
                prayer brings deliverance.
            </Text>,
            prayerPoints: [
                "Father, let every sickness in my life be removed, in the name of Jesus.",
                "Father, fix my life, in the name of Jesus.",
                "Let the fire of God consume the problem of my life, in the name of Jesus.",
            ]
        },
        {
            day: 14,
            topic: "Naaman (2)",
            wordStudy: "Luke 4:23-27",
            confession: "How God anointed Jesus of Nazareth with the Holy Ghost and with power: who went about doing good, and healing all that were oppressed of the devil; for God was with him (Acts 10:38).",
            fullText: <Text style={styles.text}>Naaman had a problem; a unique one. In recalling this story Jesus said,
                <Bold>“…Many lepers were in Israel in the time of Elisha the prophet, and none
                of them was cleansed except Naaman…”</Bold> (Luke 4:27 NKJV). It's easier to
                believe God for something you've seen Him do before. But Naaman not only
                had leprosy; nobody he knew had ever been healed of it. Are you being tested
                today because of a unique situation in your life, your marriage or your career?
                Are you afraid to talk about it because you don't know anybody who's ever
                beaten your particular problem? If so, stop focusing on the problem and start
                focusing on God! He doesn't need anything to begin with in order to solve
                your problem. Remember, in Genesis He hung the earth on nothing <Bold>(Job
                26:7)</Bold>, and it's still turning every day! The Bible says that Naaman was a
                <Bold> “great…man” (2 Kings 5:1)</Bold>, but God was about to make him a greater one.
                <Br />
                Whenever He does that He permits us to get into predicaments without
                human solutions. When He wants us to have great influence He often permits
                us to face great challenges. It's how He moves us from being impressive to
                being truly exceptional. But when He does, be careful. One of the first
                questions people will ask is, “How did you do it?” They'll start admiring your
                status and your armour, when all the time it was your affliction that drove you
                to your knees and allowed God to make you the person you've become. Before
                you can be exceptional you must work to develop a faith that believes God for
                the impossible, and trusts what He says regardless of the odds.
            </Text>,
            prayerPoints: [
                "Every altar of shame and reproach raised against my life, be scattered, in the name of Jesus.",
                "Lord, turn my shame to fame, in the name of Jesus.",
                "Lord, let every seeming impossibility in my life become possible, in the name of Jesus.",
            ]
        },
        {
            day: 15,
            topic: "Naaman (3)",
            wordStudy: "2 Kings 5:10-14",
            confession: "Because I fear the name of the Lord, the Sun of Righteousness shall arise with healing in His wings; and I will go out and grow fat like stall-fed calves, in the name of Jesus (Malachi 4:2).",
            fullText: <Text style={styles.text}>When God gives you an answer, don't argue and don't rationalise. Just do
                what He tells you—and you'll come up clean. Naaman's pride almost cost
                him his life. When he left Elisha's house he was angry because Elisha hadn't
                come out to speak with him personally. Instead he sent “a messenger” out to
                tell him to go dip seven times in the muddy Jordan River. “Aren't there
                cleaner rivers?” Naaman asked, feeling insulted. At this point Naaman's
                servants said, “If the prophet had told you to do something great, would you
                not have done it? How much more then, when he says to you, 'Wash, and be
                clean'?” <Bold>(2 Kings 5:13 NKJV).</Bold>
                <Br />
                Today God is asking you the same question. Wouldn't it be worth
                humbling yourself to come up clean? To confess your sin, share your story
                and ask for help? Wouldn't it be worth risking the disapproval of those who
                don't matter because they're not part of God's plan for your life anyway?
                Don't let your reputation get in the way of your solution. Don't let the devil
                entice you to other rivers which have no power to save. All you need is the
                kind of faith that involves looking foolish for a while: the kind that keeps you
                dependent on God when it looks like everybody else is doing better than you.
                Go ahead, dip in the river of God's grace and keep dipping! Though you've
                been insulted, though your feelings are hurt, though the process doesn't seem
                to make sense, though you've reached the end of your rope and you want to
                quit, keep dipping. Act on the Word God has given you! Miracles are for the
                obedient, not the wishful.
            </Text>,
            prayerPoints: [
                "Father, I receive grace to mortify my flesh, in the name of Jesus.",
                "I receive grace to do the right thing at the right time, in the name of Jesus.",
                "I shall not live a wasted life, in the name of Jesus.",
            ]
        },
        {
            day: 16,
            topic: "Naaman (4)",
            wordStudy: "2 Kings 5:1-3",
            confession: "The help of God shall not be far from me, in the name of Jesus. Amen.",
            fullText: <View> 
                <Text style={styles.text}>
                    Before leaving the story of Naaman, let's observe two more things:
                    <Br />
                </Text>

                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            Everybody needs help. Not just the down-'n'-outers, but the up-'n'-outers
                            too. Being a five-star general doesn't mean a thing when someone's dying
                            of leprosy. “But they wouldn't listen to someone like me,” you say. Maybe
                            not today, but when things get bad enough you'd be surprised how they
                            listen. Just serve with excellence and be ready with an answer when the
                            time comes.
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            God's hand is on you to minister where He's placed you. It was the
                            encouragement of someone who got close enough to see Naaman's real
                            problem, then introduced him to God, that brought about his healing.
                            When God puts you close to someone and makes you privy to certain
                            information, He does it so that you can give them <Bold>“a word in season”
                            (Isaiah 50:4 NKJV).</Bold> God used a cleaning lady who worked in his own
                            house to reach Naaman. What if she'd rebelled against making beds and
                            washing dishes? Or decided to go out and gossip about the problem
                            instead?
                        </Text>
                    },
                ]} />

                <Text style={styles.text}>
                    Today you have a role to play in God's plan. The servant who challenged
                    Naaman to humble himself and dip seven times in the Jordan River is not
                    named in Scripture, but he was indispensible to the process. So are you. Few
                    athletes have more fans than basketball star Michael Jordan. Yet when a
                    reporter asked him why he liked his dad to attend his games, he replied, “When
                    he's there, I know I have at least one fan!” Everybody needs encouragement.
                    You can speak a word that changes someone's life!
                </Text>

            </View>,
            prayerPoints: [
                "Lord, send help to me, in the name of Jesus.",
                "Lord, encourage me after the order of David (1 Samuel 30:6), in the name of Jesus.",
                "Father, I reject every distraction on life's journey, in the name of Jesus.",
            ]
        },
        {
            day: 17,
            topic: "Settle Your Disagreement",
            wordStudy: "Mark 11:24-25",
            confession: "For You, Lord, are good, and ready to forgive, and abundant in mercy to all those who call upon You (Psalm 86:5).",
            fullText: <Text style={styles.text}>Euodia and Syntyche were two women who worked hand in hand with Paul
                in building the church. Yet sadly, the only mention of them in Scripture is
                that they couldn't get along with one another. That's not good because God's
                blessing is predicated upon our willingness to forgive and to love one
                another. Jesus said: <Bold>“Therefore I say to you, whatever things you ask
                when you pray, believe that you receive them, and you will have them.
                And whenever you stand praying, if you have anything against anyone,
                forgive him, that your Father in heaven may also forgive you your
                trespasses” (Mark 11:24-25 NKJV).</Bold>
                <Br />
                You can't pray effectively when you have “anything against anyone.”
                Even if the other person is wrong, let God use you as a paramedic of His
                mercy. Tell them you want to see the relationship healed, apologise and be
                reconciled. It may take more than a U-turn to make up the distance between
                you, but each step you take will make the road shorter, and if you stay on
                course you'll get to where you ought to be on time. Don't argue over “who did
                what to whom.” Be bigger than that! Show grace! You've been asking God to
                restore things; well, this is part of it. Yes, your ego will rebel. And if you let it,
                fear of rejection will stop you dead in your tracks. But do it anyway and see
                what happens. Remember, when you forgive someone, you position
                yourself beyond condemnation; at that point God can bless you. Can you
                afford to live without His blessing?
            </Text>,
            prayerPoints: [
                "Father, grant me the grace to live a peaceful life, in the name of Jesus.",
                "Lord, uproot every spirit of bitterness from my life, in the name of Jesus.",
                "Father, take me to higher grounds, in the name of Jesus.",
            ]
        },
        {
            day: 18,
            topic: "Trust God!",
            wordStudy: "Psalm 146:1-10",
            confession: "I will trust in the LORD with all my heart and shall not lean lean on my own understanding, in the name of Jesus (Proverbs 3:5).",
            fullText: <Text style={styles.text}>Hard times energize some people, yet paralyzes others. Look at David.
                Everything he touched turned to gold: Samuel anointed him to be king; he
                defeated Goliath; Saul chose him as a musician and warrior; the army loved
                him and wrote songs about him. Then his life fell apart. He lost his job and his
                marriage failed; Samuel his old mentor died; his best friend Jonathan couldn't
                help him and Saul's soldiers hounded him until he had to hide in a cave. At
                some point we all do time in the cave! It's where you end up when all your
                earthy supports are gone. It's where you learn important things about yourself
                that you can't learn anywhere else. It's where God does some of His best work
                in moulding you into the likeness of Christ. It's where your worst
                inadequacies confirm that you're out of your depth and where God sends His
                power to flow through your weakness.
                <Br />
                When David prayed, “You are my refuge, my portion in the land of the
                living,” he had no way of knowing there was a crown in his future, or that he
                wasn't going to die in hiding. For all he knew, this cave he was in right now
                might be as good as it gets. When you're in a situation you can't fix, can't
                change and can't escape, trust God! Trust God! Trust God! As long as your
                sense of security is tied solely to your success, it'll always be fragile. But when
                you know that God is with you even at your lowest point, you can handle the
                cave and come out stronger!
            </Text>,
            prayerPoints: [
                "Holy Spirit, help me to trust the Lord with all my heart, in the name of Jesus.",
                "I will not disappoint God, in the name of Jesus.",
                "Lord, grant me the grace to endure to the end, in the name of Jesus.",
            ]
        },
        {
            day: 19,
            topic: "Pour Your Heart to God!",
            wordStudy: "Psalm 142:1-7",
            confession: "I shall be glad in the LORD and rejoice because I am the righteousness of God in Christ. I will shout for joy because I am upright in heart (Psalm 32:11).",
            fullText: <Text style={styles.text}>Learn to pray the Psalms. They run the gamut of human emotions from
                thanksgiving, to anger, to fear, to loneliness, to grief. The Psalmist doesn't
                miss a beat when it comes to life. Not life as we wish it was, but life as it is: <Bold>“I
                pour out my complaint before Him; before Him I tell my trouble.”</Bold> He
                vents his pain to God, he allows himself to “feel it.” That takes courage,
                especially when you just want to put on a brave face.
                <Br />
                John Ortberg wrote: “I regret feeling the pain of failure so keenly that I
                backed away from owning it and learning from it. I could not heal and move
                on. I wanted to bury it so deeply that no one would ever guess it was
                there—not even me.”
                <Br />
                Sound familiar? The Bible doesn't discourage the grieving process; it just
                warns us not to get stuck in it. “Weeping may remain for a night, but
                rejoicing comes in the morning” (Psalm 30:5 NIV). To reach your morning
                of rejoicing you must go through your night of weeping.
                <Br />
                F. B. Meyer wrote: “There are some who chide tears as unmanly,
                unsubmissive, unchristian. They comfort us with a chill, bidding us to put on
                a rigid and tearless countenance… We may well ask if a man who cannot
                weep can really love? Sorrow is just love bereaved; its most natural
                expression is tears… Jesus wept. …The Ephesian converts wept on the neck
                of the Apostle Paul whose face they were never to see again.”
                <Br />
                So go ahead and pour out your heart to God. It's a vital step to becoming
                whole.
            </Text>,
            prayerPoints: [
                "Father, let my cry come unto you in Jesus name.",
                "Hear me, O Lord, when I cry unto you in Jesus name.",
                "I shall testify to the glory of God in Jesus name.",
            ]
        },
        {
            day: 20,
            topic: "Mind Your Mouth (1)",
            wordStudy: "Luke 6:43-45",
            confession: "Let the words of my mouth, and the meditation of my heart, be acceptable in thy sight, O LORD, my strength, and my redeemer (Psalm 19:14).",
            fullText: <View>

                <Text style={styles.text}>
                    “Oh, it was just a bunch of meaningless words. No big deal!” Wrong! Our
                    words are a big deal and they do mean a lot! <Bold>“A good man out of the good
                    treasure of his heart brings forth good; and an evil man out of the evil
                    treasure of his heart brings forth evil. For out of the abundance of the
                    heart his mouth speaks” (Luke 6:45 NKJV).</Bold> Your words have either
                    negative or positive impact:
                    <Br />
                </Text>

                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            Consider some negatives. They can wound people to the core. <Bold>“There is
                            one who speaks rashly like the thrusts of a sword…” (Proverbs12:18
                            NAS).</Bold> They can break down a person's spirit, stripping them of the
                            courage for living. <Bold>“Perverseness in it</Bold> [the tongue] <Bold>breaks the spirit”
                            (Proverbs 15:4 NKJV).</Bold> Carelessly spoken words between people can
                            destroy relationships. <Bold>“The hypocrite with his mouth destroys his
                            neighbour…” (Proverbs 11:9 NKJV).</Bold> Emotional, and possibly even
                            physical death, can result from words. <Bold>“Death and life are in the power
                            of the tongue…” (Proverbs 18:21 NKJV).</Bold>
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            Now consider some positives. Your words can spark life into a
                            relationship. <Bold>“A soothing tongue is a tree of life…” (Proverbs 15:4
                            NAS).</Bold> The right words can help heal wounded relationships. <Bold>“Pleasant
                            words are…sweet to the soul and healing to the bones” (Proverbs
                            16:24 NIV).</Bold> Well-chosen words can help us to understand each other.
                            <Bold>“…Sweetness of the lips increases learning…” (Proverbs 16:21
                            NKJV).</Bold> Words spoken at the right times can bring us closer together.
                            <Bold>“The right word spoken at the right time is as beautiful as gold apples
                            in a silver bowl” (Proverbs 25:11 NCV).</Bold>
                        </Text>
                    },
                ]} />

                <Text style={styles.text}>
                What you say matters: so mind your mouth!
                </Text>

            </View>,
            prayerPoints: [
                "Father, grant me the grace to bridle my tongue, in the name of Jesus.",
                "Father, let my tongue be seasoned with the Word of God, in the name of Jesus.",
                "My tongue will not destroy me, in the name of Jesus.",
            ]
        },
        {
            day: 21,
            topic: "Mind Your Mouth (2)",
            wordStudy: "Proverbs 16:6-8",
            confession: "My mouth shall speak wisdom and the meditation of my heart shall give understanding (Psalm 49:3).",
            fullText: <View>

                <Text style={styles.text}>
                    Let's answer some communication questions:
                </Text>

                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            What is good communication? It's honest, positively intended, two-way
                            sharing. It isn't “dumping” or giving someone a piece of your mind! The
                            first law of verbal ecology is: garbage is not biodegradable! It recycles,
                            festering with time. <Bold>“The words of a talebearer are as wounds…they
                            go down into the innermost parts of the belly” (Proverbs 18:8
                            NKJV).</Bold> Many a word accompanies the hearer to their grave.
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            What should we communicate? The truth. <Bold>“Lying lips are an
                            abomination to the Lord, but those who deal truthfully are His
                            delight” (Proverbs 12:22 NKJV).</Bold> But truth can be given like the blow
                            of a sledgehammer or like a soothing hand of friendship. It should be
                            communicated after advance thought for its impact. <Bold>“The heart of the
                            righteous ponders how to answer, but the mouth of the wicked
                            pours out evil things” (Proverbs 15:28 NAS).</Bold>
                        </Text>
                    },
                    {
                        serial: 3,
                        body: <Text>
                            How much should we communicate? As much as God's Word, love and
                            wisdom dictate. Children and distressed people frequently need only
                            limited information. <Bold>“A fool vents all his feelings, but a wise man
                            holds them back” (Proverbs 29:11).</Bold>
                        </Text>
                    },
                    {
                        serial: 4,
                        body: <Text>
                            How should we communicate? How can be as important as what, so
                            speak with consideration for the hearer's feelings. <Bold>“A gentle answer
                            turns away wrath, but a harsh word stirs up anger” (Proverbs 15:1
                            NAS).</Bold> The hearer's response is conditioned by your words.
                        </Text>
                    },
                    {
                        serial: 5,
                        body: <Text>
                            Check your timing; it's vital! <Bold>“It is wonderful to say the right thing at
                            the right time” (Proverbs 15:23 NLT).</Bold> If you're not certain about your
                            timing, wait and pray for wisdom!
                        </Text>
                    },
                ]} />
            </View>,            
            prayerPoints: [
                "I reject a lying tongue, in the name of Jesus.",
                "Father, grant me the grace to use my tongue wisely, in the name of Jesus.",
                "I receive wisdom to use my mouth wisely, in the name of Jesus.",
            ]
        },
        {
            day: 22,
            topic: "Read Your Bible!",
            wordStudy: "1 Peter 1:23-25",
            confession: "For the word of God is living and powerful, and sharper than any two-edged sword, piercing even to the division of soul and spirit, and of joints and marrow, and is a discerner of the thoughts and intents of the heart (Hebrews 4:12).",
            fullText: <View>
                <Text style={styles.text}>
                    Jana Jones says, “During the day I take a few moments to unwind by reading
                    the Bible. After seeing me do this for several years, my four-year-old
                    daughter became concerned: Aren't you ever going to finish reading that
                    book?” You could read the Bible for a lifetime and not scratch the surface.
                    There's no other book like it. For example:
                    
                </Text>

                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            No human being would have written to such a high standard. Stop and
                            think of the best persons you know. You must admit, even they would
                            have left certain things out, wouldn't they?
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            There's an aura the Bible generates that no other book does. Martin
                            Luther said, “The Bible is alive, it speaks to me; it has feet, it runs after
                            me; it has hands, it lays hold of me.” Try laying a newspaper on your
                            table at a restaurant and no one will give you a second look. But lay your
                            Bible on it and they'll stare at you, watch you chew your food, and
                            maybe even take a note of your licence plate when you get into your car.
                            That's because the Bible creates a sense of God's presence that forces a
                            reaction in the hearts of men and women.
                        </Text>
                    },
                    {
                        serial: 3,
                        body: <Text>
                            We are changed as we read it. Our core values are altered, peace enters
                            our spirit, joy wells up within our heart.
                        </Text>
                    },
                    
                ]} />

                <Text style={styles.text}>
                    <Bold>“The Word of God is full of living power. It is sharper than the
                    sharpest knife, cutting deep into our innermost thoughts and desires. It
                    exposes us to what we really are” (Hebrews 4:12 NLT).</Bold> The answers you
                    need are in your Bible, so make it part of your everyday life.
                </Text>
            </View>,
            prayerPoints: [
                "I receive grace to study and understand the Word of God, in the name of Jesus.",
                "Holy Spirit, come and teach me Your Word, in the name of Jesus.",
                "The words of the Lord shall prosper in my life, in the name of Jesus.",
            ]
        },
        {
            day: 23,
            topic: "What's Good for the Goose? (1)",
            wordStudy: "Matthew 6:25-34",
            confession: "The grace of the Lord shall be sufficient for me, in the name of Jesus. Amen.",
            fullText: <Text style={styles.text}>Speaking to an anxiety-ridden crowd, Jesus said, “I tell you not to worry
                about your life… Look at the birds in the sky!” You say, “What can I learn
                from looking at a bunch of birds?” If you've ever been to Pennsylvania, USA,
                in early winter, you'll have seen the skies literally darkened as multitudes of
                Canadian geese gather for their annual flight to the sunny south. For the next
                few days let's look at their behaviour, and learn from it.
                <Br />
                Geese fly united; they don't fly separately, in random style, because no
                goose alone can go that distance. They're designed to fly in their
                characteristic “V” formation. When a bird flaps its wings the air movement
                created provides an uplift, easing the workload of the bird behind it.
                Together, their flight range increases about 70 per cent. Even the youngest,
                weakest and oldest geese can make the trip. They accomplish together what
                they could never accomplish separately. There's a lesson here: When the
                Bible says, <Bold>“Let us not give up meeting together, as some are in the habit
                of doing, but let us encourage one another…” (Hebrews 10:25 NIV),</Bold> it
                means, “Stay in fellowship with one another, and enjoy the uplift it
                provides.” You're not called to fly solo. <Bold>“The eye cannot say to the hand, 'I
                don't need you!' And the head cannot say to the feet, 'I don't need you!”'
                (1 Corinthians 12:21 NIV).</Bold> Occasionally a goose strays off on its own but
                soon becomes exhausted, loses altitude and ultimately pulls wearily back
                into the formation.
                <Br />
                “Look at the birds,” and learn.
            </Text>,
            prayerPoints: [
                "Let there be unity in the body of Christ, in the name of Jesus.",
                "Father, send a divine helper to me, in the name of Jesus.",
                "I reject every spirit of greed, in the name of Jesus.",
            ]
        },
        {
            day: 24,
            topic: "What's Good for the Goose? (2)",
            wordStudy: "Ephesians 4:1-16",
            confession: "The Lord will keep me to the end, in the name of Jesus. Amen.",
            fullText: <Text style={styles.text}>Every formation has to have a “point goose” out front who leads and sets the
                pace for the others. It's a tough position because the point goose cuts the
                headwinds, meets the changing weather conditions, and is first to feel the rain
                in his face, the snow in his eyes and the ice on his wings. He keeps the
                formation on target whatever the situation. It's hard, exhausting and lonely at
                times because there's nobody ahead of him to be the wind beneath his wings.
                The formation depends on him to persevere, stay on track and get the skein
                safely to its destination.
                <Br />
                Every church has its point goose: the pastor. Each department has its
                point geese: youth, finance, evangelism, care ministries, and so on. They
                lead, set the pace and give direction to those who follow. There are two
                common roles in the church: the underemployed and the overfunctioning.
                Point ministries are the latter, so they tend to burn out frequently. Just before
                the point goose is exhausted, a space opens in the formation and he slips back
                into it while another bird replaces him, seamlessly becoming the next point
                goose. Standing in for each other preserves the life of the formation. This is
                what Paul meant: <Bold>“The whole body [is] held together by what every joint
                supplies, according to the proper working of each individual part…”
                (Ephesians 4:16 NAS).</Bold>
                <Br />
                Don't be content to be a consumer; instead, be a producer! A great church
                isn't about paid staff; rather, it's about volunteers willing to stand in whenever
                needed.
            </Text>,
            prayerPoints: [
                "Lord, preserve me in the journey of life, in the name of Jesus.",
                "I shall not be burnt out, in the name of Jesus.",
                "I shall not live a wasted life, in the name of Jesus.",
            ]
        },
        {
            day: 25,
            topic: "What's Good for the Goose? (3)",
            wordStudy: "2 Thessalonians 3:12-15",
            confession: "The Lord will keep and preserve me until the day of His coming, in the name of Jesus. Amen.",
            fullText: <Text style={styles.text}>In the world of geese, the aged, very young, and infirm are kept protected in
                the rear of the formation. But they aren't isolated, discounted or considered
                useless; in fact, they fulfil a vital role. They become the honking section and
                cheer for the leaders. Inevitably, bad weather threatens the mission. The
                going gets tough and the tough are struggling. From the rear of the formation
                a lone honk sounds, initiating a geese chorus honking encouragement to the
                point goose. Paul understood this: <Bold>“…Encourage one another and build
                each other up…” (1 Thessalonians 5:11 NIV).</Bold> He knew we need a
                “honking section” supporting us with uplifting words and prayer. We need
                those who say, “We are behind you. We've got your back covered!”
                <Bold>“But…you, brethren, do not grow weary in doing good” (2
                Thessalonians 3:13 NKJV).</Bold>
                <Br />
                Many a servant of Christ has crossed their deepest valley on a wing, a
                prayer and a honk from some old, scarred, battle–hardened,
                straggle–feathered, half–bald honker of encouragement who was too
                stubborn to let a brother or sister quit on their watch! Occasionally, a strident,
                out–of–tune goose complains loudly and irritatingly. Within moments the
                honking section kicks in, drowning out the grumbler, restoring order and
                unity. The church's problem isn't too many people speaking negatively; it's
                too few speaking positively! When someone cries, “Defeat,” honk back,
                “Victory!” When they cry, “Fear,” honk back, “Faith!” A few words of
                encouragement can overpower a storm of complaints.
                <Br />
                So join the honking section and be known like Barnabas, whose name
                means “Son of Encouragement” <Bold>(Acts 4:36 NIV).</Bold>
            </Text>,
            prayerPoints: [
                "I receive strength in my Christian journey, in the name of Jesus.",
                "I shall not be weary in doing good, in the name of Jesus.",
                "Father, use me for the glory of Your name, in the name of Jesus.",
            ]
        },
        {
            day: 26,
            topic: "What's Good for the Goose? (4)",
            wordStudy: "1 Corinthians 12:24-26",
            confession: "Thank You, Lord Jesus, for Your prayer for us: “That they [the Body of Christ, the Church] all may be one…”",
            fullText: <Text style={styles.text}>With geese, their relationship is “Till death do us part.” And they take it
                seriously. They're fully committed. When the ravages of time or
                circumstances make it impossible for a bird to continue the trip and it begins
                to lose altitude or fails to keep abreast of the formation, the gaggle provides it
                with comfort, nurture and protection. Two strong geese leave the formation,
                flying with the “patient” safely between them; they find a sheltered location
                with food and water and make a home for the needy bird. They'll stay with
                their ward until it either recovers or dies, before joining another formation.
                For them, everything goes “on hold” to care for another bird! What an
                example of Christlike relationship and self–sacrificing love. <Bold>“…God has
                combined the members of the body…that its parts should have equal
                concern for each other. If one part suffers, every part suffers with it…”
                (1 Corinthians 12:24-26 NIV).</Bold>
                <Br />
                In today's culture, we discount and marginalise the needy, including
                those no longer functioning as they once did. But God demands that we “have
                equal concern for each other,” especially with those suffering. If a gaggle of
                birds can do it, surely God's family ought to do it too. <Bold>“Look at the birds in
                the sky!”</Bold> Jesus directs. If they can do it for each other, <Bold>“…Aren't you worth
                more than birds?” (Matthew 6:26 CEV).</Bold> One of the two greatest
                commandments (not suggestions) Christ gave us is, <Bold>“…Love your
                neighbour as yourself. There is no commandment greater…” (Mark
                12:31 NIV).</Bold> When word circulates that we love each other in this way, they'll
                beat down the doors of our church to get in!
            </Text>,
            prayerPoints: [
                "Father, help me to love the people around me, in the name of Jesus.",
                "Lord, raise helpers for me, in the name of Jesus.",
                "I will not be useless in life, in the name of Jesus.",
            ]
        },
        {
            day: 27,
            topic: "What's Good for the Goose? (5)",
            wordStudy: "Matthew 11:28-30",
            confession: "I come to You, Lord Jesus. You are my Burden Bearer; give me rest from all my cares and issues (Matthew 11:28). Amen.",
            fullText: <Text style={styles.text}>Lost and stray geese are always welcomed into the formation. It's not an
                exclusive club for the elite. Birds separated from other formations, isolated
                by the weather, accident or infirmity, are given full family status. The
                formation will alter its plans, reschedule its arrival time and inconvenience
                itself to accommodate any strangers seeking its acceptance. Many
                newcomers arrive dirty, bedraggled and ill–fed; but they're never turned
                away.
                <Br />
                In the same way, God's church isn't a museum for masterpieces—it's a
                hospital for those who have been hurt by life, even self–inflicted hurt! Jesus
                said, <Bold>“…All…who are weary and carry heavy burdens…”</Bold> are accepted
                <Bold>(Matthew 11:28 NLT). “I have other sheep that are not of this sheep pen. I
                must bring them also… there shall be one flock and one shepherd” (John
                10:16 NIV).</Bold> Christ has no higher agenda than to recruit, save and adopt the
                stranger into His flock.
                <Br />
                You say, “But they're not my kind of people; they don't look, talk or smell
                like me!” So what? We don't get to choose our biological or spiritual siblings;
                God does! If they rub us like sandpaper, they're helping rub off the rough
                edges and polish us up.
                Want to see the material God builds the church with? Prepare yourself for
                a shock! <Bold>“ …The unrighteous …fornicators …idolaters …adullterers …homosexuals
                …thieves …covetous …drunkards …revilers …extortioners …And such were
                some of you. But you were washed, but you were sanctified, but you were
                justified in the name of the Lord Jesus and by the Spirit of our God” (1
                Corinthians 6:9-11 NKJV).</Bold> When we were taken into the formation, we
                were washed, sanctified, justified and given full family status!
            </Text>,
            prayerPoints: [
                "Father, purge me with the blood of Jesus, in the name of Jesus.",
                "Lord Jesus, sanctify me with the Word of God.",
                "Father, take me for I'm Yours and never let go of me, in the name of Jesus.",
            ]
        },
        {
            day: 28,
            topic: "Very Present",
            wordStudy: "Psalm 46:1-7",
            confession: "God is my refuge and strength, my very present help in time of trouble, in the name of Jesus (Psalm 46:1). Amen.",
            fullText: <Text style={styles.text}>Randy Reid, a 34-year-old construction worker, was welding on top of a
                nearly completed water tower outside Chicago. According to writer Melissa
                Ramsdell, Reid unhooked his safety gear to reach for some pipes when a
                metal cage slipped and bumped the scaffolding on which he stood. It tipped,
                and Reid lost his balance. He fell about 35m, landing face down on a pile of
                dirt, just missing rocks and construction debris. A fellow worker called the
                emergency services. When paramedics arrived, they found Reid conscious,
                moving, and complaining of a sore back. Apparently the fall didn't cost Reid
                his sense of humour! As paramedics carried him on a backboard to the
                ambulance, Reid had one request: “Don't drop me.” Doctors later said Reid
                came away from the accident with just a bruised leg.
                <Br />
                Sometimes we resemble Randy Reid. God protects us from harm in a 35-
                metre fall, yet we are not willing to trust Him to get us over the next 35-cm
                hurdle. Being sinful, we fail. Being prone to sickness, we hurt. Being mortal,
                we die. Pressure wears on us. Anxiety gives us ulcers. People intimidate us.
                Criticism offends us. Disease scares us. Death haunts us. What's the answer?
                <Bold>“God is our refuge and strength, a very present help in trouble” (Psalm
                46:1 NKJV).</Bold> Note the words <Bold>“very present.”</Bold> When we hit life's rough spots,
                our tendency is to feel abandoned; yet the opposite is true. At that very
                moment we are more than ever the object of His love and concern. Yes, even
                when our vision is blurred, our thinking foggy, our faith fleeting and we look
                up and can't see Him clearly—He is <Bold>“very present.”</Bold>
            </Text>,
            prayerPoints: [
                "Father, send help to me in, in the name of Jesus.",
                "Father, don't let Your help be far from me, in the name of Jesus.",
                "Let the joy of the Lord be my strength, in the name of Jesus.",
            ]
        },        
    ],
    March: [
        {
            day: 1,
            topic: "Fenelon's Formula for Prayer",
            wordStudy: "Jeremiah 29:11-13",
            confession: "As I seek the Lord with all my heart, the Lord will answer me, in the name of Jesus (Verse 13).",
            fullText: <Text style={styles.text}>Is your prayer life consistent? Is it rich and rewarding? Do you see
                measurable growth in it? If not, François Fénelon, a 17th century Frenchman,
                tells us how to pray and get results. It's a tried and true formula worth
                following:
                <Br />
                “Tell God all that is in your heart, as one unloads one's heart, its pleasures
                and its pains, to a dear friend. Tell Him your troubles that He may comfort
                you; tell Him your longings that He may purify them; tell Him your dislikes
                that He may help you conquer them; tell Him your temptations that He may
                shield you from them; show Him the wounds of your soul that He may heal
                them; lay bare your indifference to good, your depraved taste for evil, your
                instability. Tell Him how self–love makes you unjust to others, how vanity
                tempts you to be insincere, how pride hides you from yourself and from
                others.
                <Br />
                “If you thus pour out all your weaknesses, needs and troubles, there will
                be no lack of what to say. You will never exhaust the subject, for it is
                continually being renewed. People who have no secrets from each other
                never want for subjects of conversation. They do not weigh their words for
                there is nothing to be held back. Neither do they seek for something to say.
                They talk out of the abundance of their heart. Without consideration, they
                simply say just what they think. When they ask, they ask in faith, confident
                that they will be heard. Blessed are those who attain such familiar, unreserved
                communication with God.”
            </Text>,
            prayerPoints: [
                "Lord, hear me when I call on You, in the name of Jesus.",
                "My prayer shall not be turned down, in the name of Jesus.",
                "Lord, remove every hindrance in my life, in the name of Jesus.",
            ]
        },
        {
            day: 2,
            topic: "You Can Rise above Your Beginnings",
            wordStudy: "Genesis 39:2-6",
            confession: "I shall be surrounded with greatness, in the name of Jesus.",
            fullText: <Text style={styles.text}>Joseph didn't come from an ideal family. He was the eleventh son of Jacob, a
                con man and a conniver. By this time, however, Jacob had got his spiritual act
                together. But his ten older sons took after their dad. They once deceived and
                killed an entire town of men because one of them raped their sister <Bold>(Genesis
                34)</Bold>. Now they're about to commit another deed of treachery—selling Joseph
                into slavery. Yet Joseph turned out to be a man of greatness.
                <Br />
                What's the bottom line? It is that you can rise above your beginnings!
                Look at Joseph. Perhaps you've been rejected, or discovered the people you
                thought loved you really didn't. That's Joseph's story. But, <Bold>“The Lord was
                with Joseph.”</Bold> Those words change everything! Regardless of what happened
                yesterday, if you remain faithful to God, He can rearrange tomorrow in your
                favour.
                <Br />
                Many people who've been mistreated are still focusing on the people who
                hurt them, waiting for them to make things right. In a lot of cases that isn't
                going to happen. If you've been wronged, you need to turn to the One who
                won't hurt you, and who is there to help and heal you. Face it; some
                relationships aren't going to get fixed! Your parents may never accept you. You
                may not be able to escape the problem you've been living with or resolve it to
                your liking. You see, God's not limited by your past, He's only limited by your
                lack of faith. So trust Him. <Bold>“…If God is for us, who can be against us?”
                (Romans 8:31 NIV)</Bold>. If God is for you, in the final analysis, it doesn't matter
                who's against you.
            </Text>,
            prayerPoints: [
                "Father, let me go far in life, in the name of Jesus.",
                "I shall fulfill the purpose of God for my life, in the name of Jesus.",
                "I shall rise and shine, in the name of Jesus.",
            ]
        },
        {
            day: 3,
            topic: "You Have What Is Required!",
            wordStudy: "Proverbs 18:15-17",
            confession: "I can do all things through Christ who strengthens me (Philippians 4:13 NKJV).",
            fullText: <Text style={styles.text}>When David first arrived at King Saul's palace, he was a simple, unknown
                shepherd boy. But he had an impressive CV! God, in essence, said, “I saw you
                kill the bear and the lion; I was watching when you didn't realise it.” Solomon
                wrote, <Bold>“A man's gift makes room for him…” (Proverbs 18:16 NKJV)</Bold>.
                David's gift may have been only a slingshot, but it opened the door to his
                future as Israel's king.
                <Br />
                So even though what you've got doesn't seem like much, give it to God
                and watch what He can do with it. You say, “But I'm not formally trained.” All
                God requires is that you show up, say “yes,” and make yourself available.
                When you give God what you've got, He gives you what He's got—and that
                makes your odds unbeatable.
                <Br />
                “But I don't have the right credentials,” you say.
                <Br single/>
                So what? Benjamin Franklin had less than two years of formal schooling.
                Yet at 25, he founded America's first library. At 31, he started its first fire
                department. At 36 he designed a heating stove that's still in use today. At 40 he
                harnessed the power of electricity. At 45 he founded the nation's first
                university. And at 79, he invented bifocals.
                <Br />
                He was an economist, philosopher, diplomat, inventor, educator,
                publisher and linguist who spoke and wrote in five languages. Chances are,
                you already have more education than he had when he was your age. Add to
                that these two promises: <Bold>“The Lord is my helper…” (Hebrews 13:6 NKJV)</Bold>
                and our text Scripture, <Bold>“I can do all things through Christ who strengthens
                me”</Bold> and you have what's required!
            </Text>,
            prayerPoints: [
                "My gifts shall open doors for me, in the name of Jesus.",
                "My glory shall arise and shine, in the name of Jesus.",
                "Remove every hindrance of the work of my hands and achievements in life, in the name of Jesus.",
            ]
        },
        {
            day: 4,
            topic: "Start Where You Are!",
            wordStudy: "Psalm 102:1-3",
            confession: "My children shall be for signs and wonders, in the name of Jesus (Isaiah 8:18).",
            fullText: <Text style={styles.text}>When did we get our first clue that Jesus knew He is the Son of God? In the
                temple when He was 12 years old. His parents were three days into the return
                trip to Nazareth before they noticed He was missing. The temple was the last
                place they searched, but it was the first place Jesus went. By the time Joseph
                and Mary located their son, He had confounded the most learned scholars in
                the temple.
                <Br />
                As a boy, Jesus already sensed the call of God. But what did He do next?
                Recruit disciples and preach sermons? No, He went back home and learnt the
                family business. That's what you, too, should do! Before you attempt to do
                great things for God, go home, love your family and take care of business.
                “But I'm called to be a missionary,” you say. Good, but your first mission field
                is where you live!
                <Br />
                What makes you think you'll be believed by those who barely know you,
                if you're not credible to those who know you best? Charity begins at home!
                The Psalmist says, “…I will lead a life of integrity in my own home” (Psalm
                101:2 NLT). For His first 30 years, Jesus' neighbours remembered Him as a
                worker. “…He's just a carpenter…” <Bold>(Mark 6:3 NLT)</Bold>. Jesus spent years
                developing a work ethic, sharpening His vocational skills and supporting His
                family. <Bold>“…He had to be one of us…” (Hebrews 2:17 CEV).</Bold>
                <Br />
                Why did Jesus do it that way? So He would know how we feel, and so
                we'd be confident in going to Him with our needs, knowing He's qualified and
                able to meet them.
                <Br />
                So, start where you are!
            </Text>,
            prayerPoints: [
                "Father, let me be a good example to my household, in the name of Jesus.",
                "Let my life preach the name of Jesus, in the name of Jesus.",
                "Father, I shall not remain the same, in the name of Jesus",
            ]
        },
        {
            day: 5,
            topic: "Knowing Who Belongs in Your Life",
            wordStudy: "Numbers 35:50-56",
            confession: "I shall fulfill the purpose of God for my life, in the name of Jesus.",
            fullText: <Text style={styles.text}>Earthquakes result from two tectonic plates on a fault line shifting against one
                another, then lurching in opposite directions. And that's what happens when
                you bond with the wrong people. It's why God instructed the Israelites
                concerning the hostile nations in the Promised Land: <Bold>“Those you allow to
                remain will become…thorns in your sides.”</Bold> When a relationship is not
                working, when your efforts to rehabilitate it have failed, acknowledge it.
                Sometimes you just have to swallow your pride and admit that instead of you
                lifting them up, they're dragging you down.
                <Br />
                Releasing somebody doesn't mean they'll never improve, it just means
                God is better suited to the job. Be careful around those who are always trying
                to make you feel guilty for not “being there.” Only God can always be there!
                There's a difference between helping somebody and carrying them. Your help
                may actually be a hindrance. Why should they even try, if you're always there
                to do it for them? Your need to be needed could be getting in the way of their
                need to grow. Step back and let them walk on their own. Not everybody will
                be happy when you do that, but they're not supposed to be. Jesus said, <Bold>“Woe to
                you when all men speak well of you…” (Luke 6:26 NKJV).</Bold>
                <Br />
                Don't let the fear of criticism overwhelm your common sense. People
                don't take confrontations well, but this is a matter of survival. Every
                relationship is for a reason, and a season. Discern those who belonged in your
                past, from those who belong in your life now.
            </Text>,
            prayerPoints: [
                "The Lord shall take me to higher grounds, in the name of Jesus.",
                "Lord, separate me from destiny destroyers, in the name of Jesus.",
                "Father, expose every desire of my enemies, in the name of Jesus.",
            ]
        },
        {
            day: 6,
            topic: "Always Give Your Best",
            wordStudy: "Colossians 3:23-25",
            confession: "The joy of the Lord is my strength, in the name of Jesus (Nehemiah 8:10).",
            fullText: <Text style={styles.text}>One of Michelangelo's greatest masterpieces was his sculpture of David. He
                worked on it with such passion that he often slept in his clothes, resenting the
                time it took to take them off and put them on again. He repeatedly examined
                and measured the marble to see what pose it could accommodate. He made
                hundreds of sketches of possible attitudes and detailed drawings from models.
                He tested his ideas in wax on a small scale and only when he was satisfied did
                he pick up his chisel and mallet.
                <Br />
                He approached the painting of the Sistine Chapel with the same intensity.
                Lying at uncomfortable angles on hard boards, breathing the suffocating air
                just under the vault, he suffered from inflamed eyes and skin irritation from
                the plaster dust. For the next four years he literally sweated in physical
                distress; but look at what he produced! Dr Martin Luther King Jr said, “If a
                man is called to be a street sweeper, he should sweep streets as Michelangelo
                painted, or Beethoven composed music, or Shakespeare wrote poetry. He
                should sweep streets so well that all the hosts of Heaven and Earth will pause
                and say, 'Here lived a great street sweeper who did his job well.'
                <Br />
                If you're not passionate about what you do, find something you can be
                passionate about! Don't just strive to make money, strive to make a difference.
                Significance should be your goal, not survival. Paul gives us the ultimate
                reason for always giving our best: <Bold>“…Do your work willingly as though you
                were serving the Lord Himself…” (Colossians 3:23 CEV).</Bold>
            </Text>,
            prayerPoints: [
                "I reject every spirit of laziness, in the name of Jesus.",
                "Father, teach my hand to fight, in the name of Jesus.",
                "Father, don't let the grace in my life be in vain, in the name of Jesus.",
            ]
        },
        {
            day: 7,
            topic: "Work on Your Attitude",
            wordStudy: "John 5:11-12",
            confession: "The love of God shall manifest in my life, family and ministry, in the name of Jesus.",
            fullText: <Text style={styles.text}>The most important choice you make each day is your choice of an attitude. So
                choose, first, an attitude of thanksgiving. <Bold>“In everything give thanks; for this
                is the will of God…for you” (1 Thessalonians 5:18 NKJV).</Bold> Paul was in
                prison when he wrote those words. His attitude wasn't based on his
                surroundings; it was a choice he made. Second, choose an attitude of trust.
                <Bold>“…The Lord's unfailing love surrounds the man who trusts in Him”
                (Psalm 32:10).</Bold> Where does such trust come from? Feeding daily on God's
                Word! Nehemiah's wall-builders were surrounded by enemies, yet they were
                able to <Bold>“celebrate…because they understood the words which had been
                made known to them” (Nehemiah 8:12 NAS).</Bold> God's Word gives you
                confidence!
                <Br />
                Satan doesn't fear your sin; he knows God can forgive it. He doesn't fear
                your depression; he knows God can drive it away. He doesn't fear your lack; he
                knows God can provide. He fears your discovery of God's Word, because your
                ignorance of it is the most effective weapon he can use against you. Finally,
                choose an attitude of love. <Bold>“These things I have spoken to you…that your
                joy may be full. This is My commandment, that you love one another…”
                (John 15:11-12 NKJV).</Bold> Love says, “I accept you as you are, care when you
                hurt, desire only what's best for you, and erase all offences.” It refuses to look
                for ways to run, and opts for working things through. It's resilient. While the
                world around gives the opposite advice, love stands firm.
            </Text>,
            prayerPoints: [
                "Lord Jesus, let Your Word dwell richly in my heart.",
                "Father, let the fruit of the Spirit manifest in my life that Christ Jesus may be glorified in me, in the name of Jesus.",
                "Father, grant me the grace to serve You to the end, in the name of Jesus.",
            ]
        },
        {
            day: 8,
            topic: "You Have the Power, So Use It!",
            wordStudy: "Acts 1:1-8",
            confession: "I shall receive power when the Holy Spirit has come upon me; and I shall be a witness to the Lord Jesus both in my Jerusalem, and in all Judea and Samaria, and to the end of the earth (Verse 8).",
            fullText: <Text style={styles.text}>When Ray and Dorothy Buker went to Burma in 1926, there were other
                missionaries there. These missionaries had established their own banking
                system and afternoon tea routine, complete with trained butlers, none of
                which fitted Buker's concept of preaching the Gospel. A former Olympian,
                accustomed to hardship, he wanted to go where others wouldn't, and that
                brought him north into China. When his wife suffered a nervous breakdown
                and help wasn't available, Buker prayed, read to her, and nursed her back to
                health. During World War II, he fled for his life from the Japanese army; and
                when he died at 92 he left a legacy of souls won to Christ. Now where did
                Buker get his grit and tenacity?
                <Br />
                Jesus said, <Bold>“When the Holy Spirit has come upon you, you will
                receive power to preach with great effect” (Acts 1:8 TLB).</Bold> During the
                early days of computers, when the power went out, if you hadn't saved your
                work you lost it. Now an Uninterruptible Power Supply (UPS) stops that
                from happening. God never intended us to operate using the world's system,
                because His Spirit gives us the power to do five things.
                <Br />
                First, we are given the power to choose by keeping us
                “established…[strong in…purpose]…having control over [our]…will and
                desire…” <Bold>(1 Corinthians 7:37 AMP)</Bold>. Second, we have the power to endure.
                Handling trials with grace proves that <Bold>“our…power is from God, not from
                ourselves” (2 Corinthians 4:7 NLT).</Bold> Third, we are given the power to
                overcome: <Bold>“We are weak…but…we…live…by the power of God” (2
                Corinthians 13:4 RSV)</Bold> Fourth, we are given the power to minister:
                <Bold>“According to the…working of His power” (Ephesians 3:7 RSV).</Bold>
                Finally, we have the power to witness: “For the Kingdom of God depends not
                on talk but on power” <Bold>(1 Corinthians 4:20 NRS)</Bold>. You have the power, so use
                it!
            </Text>,
            prayerPoints: [
                "Lord, I thank You for the power of the Holy Spirit in and upon Your Church, in the name of Jesus.",
                "Anything in my life resisting the power of the Holy Spirit, die, in the name of Jesus.",
                "Let the power of the Holy Spirit possess me afresh, in the name of Jesus.",
            ]
        },
        {
            day: 9,
            topic: "Making Giving a Lifestyle",
            wordStudy: "Luke 6:36-38",
            confession: "I receive the grace to sow bountifully and to give cheerfully, in the name of Jesus (2 Corinthians 9:6-9).",
            fullText: <Text style={styles.text}>A child who had just learned to tie his shoelaces was crying. So his mum asked,
                “Is it that hard?” “No,” he sobbed, “but now I'll have to do it for the rest of my
                life!” Is that how you feel about giving? You want to learn, but you're not sure
                that you want to make it a lifestyle? Some swallows were teaching their young
                to fly from a branch overhanging a lake. One by one, the mother bird pushed
                each of her chicks to the end, until somewhere between the branch and the
                water they discovered they could fly. Their mother understood what they
                didn't: until you learn to fly you're not really living! Understand this: giving is
                an action built into us by God; it's the air into which we were born. But until
                you realize that, you'll cling to everything you have!
                <Br />
                The Bible says, <Bold>“…Whoever sows generously will also reap
                generously” (2 Corinthians 9:6 NIV).</Bold> Everything in life begins with a seed.
                Your seed is anything that can multiply: your love, your time or your money.
                Your harvest is what comes back to you in benefits such as joy, good
                relationships, and, yes, finances. If what you have is not enough to be a
                harvest, sow it as a seed, believing God will multiply it back to you in the areas
                you need it most. Go ahead, you'll love the results! Giving is like flying. When
                you learn to let go of what you're clinging to and launch out, you will realize,
                “This is how I was born to live!”
            </Text>,
            prayerPoints: [
                "I receive grace to make giving my lifestyle, in the name of Jesus.",
                "As I give, I shall reap bountifully, in the name of Jesus.",
                "The reward of my giving shall not be manipulated, in the name of Jesus.",
            ]
        },
        {
            day: 10,
            topic: "What's Your Attitude towards Your Child?",
            wordStudy: "Luke 15:11-24",
            confession: "My children shall be taught of the Lord and great shall be their peace in the name of Jesus (Isaiah 54:13).",
            fullText: <Text style={styles.text}>Chuck Colson writes: “When I was at Buckingham Palace…Prince Philip
                asked me, 'What can we do about crime here in England?' I replied, 'Send
                more children to Sunday school.' He thought I was joking. But I pointed out a
                study by sociologist Christy Davies which found that in the first half of the
                1800s, British society was marked by high levels of crime and violence which
                dropped dramatically in the late 1800s and early 1900s. What changed an
                entire nation's national character? Throughout that period, attendance at
                Sunday schools rose steadily until, by 1888, a full 75 percent of children in
                England were enrolled.”
                <Br />
                Since then attendance has fallen off…with a corresponding increase in
                crime and disorder. If we fill the Sunday schools, we can change hearts and
                restore society. Does that mean our children will never lose their way? No, it
                just means they'll realise the truth of what they were taught, recall the joys of
                being in “Father's house,” and return because they know they'll be welcomed
                back. You'll notice, no charge of parental neglect is laid at the doorstep of the
                prodigal's father. He was a great role model. He supported his children
                financially and emotionally. He guided them without forcing them to
                conform. He gave them room to fail and a place to return.
                <Br />
                What does reflect on us as parents, however, is our attitude towards our
                children. Your child may be “a great way off,” but they need to know you care,
                that you love and pray for them, and that you'll welcome them home.
            </Text>,
            prayerPoints: [
                "Lord, thank You for the life of my children, in the name of Jesus.",
                "My children shall not be manipulated, in the name of Jesus.",
                "The grace of God shall be sufficient for my children, in the name of Jesus.",
            ]
        },
        {
            day: 11,
            topic: "Your Work Matters to God (1)",
            wordStudy: "John 5:9-16",
            confession: "The work of my hands shall be sufficient for me, in the name of Jesus.",
            fullText: <Text style={styles.text}>A salesman stopped to visit a client and was amazed to find a big dog
                emptying wastebaskets. “All part of the job!” the dog said cheerfully. “Does
                your boss know how fortunate he is to have a talking dog?” the salesman
                asked. “No,” replied the dog, “and don't tell him or he'll have me answering
                phones next!”
                <Br />
                The Bible says, <Bold>“There is nothing better than to…find satisfaction in
                work… these pleasures are from…God” (Ecclesiastes 2:24 NLT).</Bold> Max
                Lucado says, “Before God gave Adam a wife or a child…He…gave him a
                job…in <Bold>“the garden…to cultivate…and keep it'” (Genesis 2:15 NAS).</Bold>
                God deems work worthy of its own engraved commandment: “You shall
                work six days, but on the seventh…you shall rest…” <Bold>(Exodus 34:21 NAS).</Bold>
                But emphasis on the day of rest can make us miss the command to work. Your
                work matters to God…and society… Cities need plumbers. Nations need
                soldiers. Traffic lights break. Bones break. Someone has to raise kids, raise
                cane, and manage the kids who raise Cain! Whether you log on or lace
                up…you imitate God… Jesus said, <Bold>“My Father never stops working…so I
                keep working too” (John 5:17 NCV).</Bold>
                <Br />
                Your career consumes half of your lifetime. Shouldn't it broadcast God?
                Don't those 40-60 hours a week belong to Him too? The Bible never
                promotes workaholism as pain medication. But God calls the physically able
                to till the gardens He gives. So use your uniqueness (what you do), to make a
                big deal out of God (why you do it), every day of your life (where you do it).
            </Text>,
            prayerPoints: [
                "Lord, I thank You for the work of my hands, in the name of Jesus.",
                "The work of my hands shall prosper, in the name of Jesus.",
                "I receive grace and strength for the work of my hands, in the name of Jesus.",
            ]
        },
        {
            day: 12,
            topic: "Your Work Matters to God (2)",
            wordStudy: "Proverbs 16:8-11",
            confession: "I shall be like a tree planted by the rivers of water. I will bring forth my fruit in my season and whatsoever I do with my hands shall prosper, in the name of Jesus (Psalm 1:3).",
            fullText: <Text style={styles.text}>The Human Resources Director was taken aback when he heard what the job
                applicant asked to be paid. “You certainly expect to be compensated well for a
                beginner.” The applicant replied, “Well sure, work is a lot harder when you
                don't know what you're doing.”
                <Br />
                A cute story, but <Bold>“The Lord demands fairness in…business…”
                (Proverbs 16:11 NLT).</Bold> You should give an honest day's work for an honest
                day's wages. Eric Harvey says, “Think about somebody you know who's of
                good character, and reflect on the characteristics that make them a role
                model… Chances are that high on the list is commitment, an unwavering
                dedication to being a good family member and friend, to doing their best on
                and off the job…to doing what's right, noble and decent. Committed
                people…have their heads and hearts in the right place. They keep their
                priorities straight…stay focused on what's important… What they believe
                drives how they behave, and how they behave determines their
                character…their reputation…and the legacy they leave.”
                <Br />
                Abraham Lincoln observed, “Commitment is what transforms a promise
                into reality. It's the words that speak boldly of your intentions, and the actions
                which speak louder than the words. It's making time when there is none…it's
                coming through time after time, year after year. Commitment is the stuff
                character's made of…the power to change the face of things…the daily
                triumph of integrity over scepticism.”
                <Br />
                An admirer approached the world-renowned pianist Van Cliburn after one
                of his concerts and said, “I'd give my life to be able to play like you.”
                <Br single/>
                Smiling, Cliburn replied, “I did!”
            </Text>,
            prayerPoints: [
                "Let the work of my hands reject manipulation, in the name of Jesus.",
                "The work of my hands shall not suffer losses, in the name of Jesus.",
                "Lord, release Your favour upon the work of my hands, in the name of Jesus.",
            ]
        },
        {
            day: 13,
            topic: "Your Work Matters to God (3)",
            wordStudy: "Genesis 31:36-42",
            confession: "I shall be like a tree planted by the rivers of water. I will bring forth my fruit in my season and whatsoever I do with my hands shall prosper, in the name of Jesus (Psalm 1:3).",
            fullText: <Text style={styles.text}>Dr Adrian Rogers said, “God takes ordinary people and gives them power to
                do extraordinary things. Whether you put hub caps on tyres…key in data…dig
                ditches or wash dishes, <Bold>'Work…as…for the Lord…'(Colossians 3:23 NLT).</Bold>
                Jesus' home was the cottage of a working man. Mending plows or mending
                souls, Jesus was doing God's work because people need houses and furniture.
                Knowing you're serving the Lord puts dignity in running a machine, servicing
                cars, carrying mail, painting houses, or cutting grass. Tell God, 'I'm doing this
                for you and I'll do it with all my might' That kind of attitude puts a spring in
                your step… You're a priest of God…in full-time service, and if that doesn't ring
                your bell, your clapper's broken!”
                <Br />
                As Leigh Priebe Kearney says, “No job's perfect…there'll always be
                things you aren't thrilled about. See the big picture: find out where the
                company's heading and how you fit in. Reach out: ask what's important to your
                co-workers and how you can help them.”
                <Br />
                You can have almost anything you want, if you help enough other people
                get what they want. Remember the three C's: (1) Commitment. Workers who
                get ahead share a sense of commitment: they're fully engaged in their work. (2)
                Control. They're proactive, not passive. (3) Challenge. They see stressful
                situations as opportunities for growth. Don't wait for your ship to come in;
                swim out to meet it! Management won't suddenly recognise your potential,
                pluck you from obscurity and rocket you to the top. Draft a plan, then talk to
                your boss. A game plan shows you're open to professional growth. Keep
                learning: talk to people from other departments, take classes, and tackle
                projects outside your comfort zone.
            </Text>,
            prayerPoints: [
                "I will not labour in vain, in the name Jesus.",
                "Lord, prosper the work of my hands, in the name of Jesus.",
                "Anointing for prosperity, fall upon the work of my hands, in the name of Jesus.",
            ]
        },
        {
            day: 14,
            topic: "Is There Someone You Need to Forgive?",
            wordStudy: "Luke 6:36-37",
            confession: "I receive grace to forgive those who offend me, in the name of Jesus.",
            fullText: <Text style={styles.text}>The greatest power you have over anyone who hurts you is the power of
                forgiveness. When you say, “I forgive, and no longer hold it against you,”
                both sides are set free from the negative bond that exists between you. But
                there's more: we also free ourselves from the burden of being the “offended
                one.” As long as we don't forgive those who've wounded us, we take them
                with us, or worse, carry them like an albatross around our neck. One of our
                greatest temptations is to cling in anger to our enemies, then define ourselves
                as being wounded by them. Forgiveness, therefore, not only liberates the
                other party, but it also liberates us! It's the way to true freedom.
                <Br />
                Now, forgiving doesn't always mean forgetting. Though we forgive
                somebody, the memory of what they did might stay with us a long time. We
                can carry it in our emotions as a scar, or even in our bodies as a physical sign.
                But forgiveness changes how we remember. It turns the curse into a blessing.
                When we forgive our parents for their divorce, our children for their lack of
                love, our friends for their unfaithfulness in times of need, our counselors for
                their bad advice, or our boss for treating us unfairly—we no longer have to
                experience ourselves as the victims of events over which we have no control.
                Forgiveness allows us to take back our power and not let the events embitter,
                limit or destroy us.
                <Br single/>
                Is there someone you need to forgive?
            </Text>,
            prayerPoints: [
                "Lord, deliver me from the spirit of unforgiveness, in the name of Jesus.",
                "I receive healing from every wound or hurt I have sustained from others, in the name of Jesus.",
                "Holy Spirit, comfort and strengthen me on every side, in the name of Jesus.",
            ]
        },
        {
            day: 15,
            topic: "Keep Bending and Shaping the Twig",
            wordStudy: "Deuteronomy 6:3-7",
            confession: "I will diligently teach my children the words and the commandments of God, in the name of Jesus.",
            fullText: <Text style={styles.text}>On Father's Day, a pastor was making the point that it's not enough to “talk the
                talk,” you must also “walk the walk.” He said, “It doesn't matter what you tell
                your kids, they'll turn out just like you. That scares me. Does it scare anyone
                else?” Looking over the congregation, he saw that the only hand raised was
                his son's! “As the twig is bent, so grows the tree.” As a father, it's your
                responsibility to <Bold>“train up a child in the way he should go.” (Proverbs 22:6
                NKJV).</Bold> Just as you nourish them with good food, the attitudes you transmit
                become food for their minds. Don't feed them cynicism and half-baked
                theology; feed them spiritually by talking to them about the things of God
                <Bold>“when you sit…walk…lie down…rise up” (Deuteronomy 6:7 NKJV).</Bold>
                <Br />
                Dr. D. James Kennedy observed: “Sending children to public schools and
                [expecting] school to teach them everything is a relatively recent
                development. From the 1600s until the mid 19th century parents maintained
                responsibility for their children's education. And…the curriculum included
                Bible Reading and Prayers. Today…society has allowed the eradication of
                both. We can no longer rely on the school system to support Christian views,
                we must work even harder to ensure our children grow in Christ. How do we
                bend the twig? With a God-centred education that teaches sound moral
                principles…kids need high academic standards…and direction towards
                attaining them through discipline…guidelines, boundaries and
                rewards…Those of you who have children…don't give up. Keep bending
                [and shaping] the twig…one day you'll have a tree that stands tall and bears
                fruit for God's glory.”
            </Text>,
            prayerPoints: [
                "Lord, open the ears of my children to hear the Word of God, in the name of Jesus.",
                "My children will not miss their destiny, in the name of Jesus.",
                "My children shall bring glory to God, in the name of Jesus.",
            ]
        },
        {
            day: 16,
            topic: "Wonder and Worship",
            wordStudy: "2 Chronicles 20:20-25",
            confession: "I will come before the Lord, sing a new song and make a joyful noise to the rock of my salvation (Psalm 95:1).",
            fullText: <Text style={styles.text}>Never lose your sense of wonder, or you'll lose your motivation for worship.
                Unless you believe God is all-powerful, ever-present and approachable,
                you'll live in a state of anxiety believing that everything depends on you.
                You'll shrink from sharing your faith in case you're ridiculed or can't find the
                right words. You'll fail to be generous because the source of your security is in
                yourself. You'll avoid confronting those who need it, because without the
                certainty of God's acceptance, you become a slave to people's opinions.
                <Br />
                We live in a cynical age that discourages wonder. We've diminished the
                marvel and awe, yet deep down, we still ache for it. When you shrink your
                concept of God to fit your own rationale, you pray without faith, work without
                passion, serve without joy, and suffer without hope. And the result is fear,
                retreat and loss of vision.
                <Br />
                But there's one thing that's guaranteed to restore your understanding of
                how big and wonderful God is: worship. God created us so that when we
                experience something awe-inspiring we need to praise it, to wrap words
                around it. We don't worship God because He needs it, but because we do.
                Without worship, our perception of Him is incomplete; we forget how great
                He is; we overlook our calling and become self-involved. We lose our sense
                of wonder and gratitude by plodding through life with blinkers on, and we
                become self-reliant, stubborn and proud. So, each day, let us pause and say
                with the Psalmist, “Exalt the Lord our God, and worship at His footstool…”
                <Bold>(Psalm 99:5 NKJV).</Bold>
            </Text>,
            prayerPoints: [
                "Lord, I thank and worship You for all Your doings in my life and family.",
                "Father, let Your grace possess my worship, in the name of Jesus.",
                "Wonders of worship shall not cease in my heart and mouth, in the name of Jesus.",
            ]
        },
        {
            day: 17,
            topic: "God Rewards Diligence",
            wordStudy: "Ruth 2:2-10",
            confession: "I shall prosper in all ways, in the name of Jesus.",
            fullText: <Text style={styles.text}>Ruth was working as a farmhand when Boaz picked her to be his wife. Elisha
                was ploughing his fields when Elijah hired him to be his assistant. Peter, James
                and John were fishing when Jesus chose them to be disciples. God calls people
                with a proven work ethic. Not once did Jesus call lazy people, or those sitting
                around waiting for their big break. In God's eyes there are no menial
                jobs—only menial attitudes. Whether you're the caretaker or the CEO, <Bold>“it is
                required of stewards that one be found trustworthy” (1 Corinthians 4:2
                NAS).</Bold> God will only give you more when you've proven yourself with what
                you've got.
                <Br />
                The Bible says: <Bold>“He who has a slack hand becomes poor, but the hand
                of the diligent makes rich” (Proverbs 10:4 NKJV). “The soul of a lazy man
                desires, and has nothing; but the soul of the diligent shall be made rich”
                (Proverbs 13:4 NKJV). “Do you see a man who excels in his work? He will
                stand before kings…” (Proverbs 22:29 NKJV).</Bold>
                <Br />
                Blessing follows work just as surely as reaping follows sowing. <Bold>“The
                Lord will command the blessing on…all to which you set your hand…”
                (Deuteronomy 28:8 NKJV).</Bold>
                <Br />
                God blesses dreams, but not daydreamers. You've got to get up and go to
                work! <Bold>“Then the Lord God took the man and put him in the garden of
                Eden to tend and keep it” (Genesis 2:15 NKJV).</Bold> When God made Adam, He
                put him to work, because productivity is essential to self-esteem. So discern
                where you belong, work hard, then when something needs to be done, God
                will know where to find you.
            </Text>,
            prayerPoints: [
                "In the name of Jesus, Father, I bless You for the work of my hands.",
                "I receive grace to be diligent at work, in the name of Jesus.",
                "I will not lose the reward of my work, in the name of Jesus.",
            ]
        },
        {
            day: 18,
            topic: "Abraham (1)",
            wordStudy: "Genesis 12:1-5",
            confession: "For as many as are led by the Spirit of God, they are the sons of God—and I am one of them (Romans 8:14).",
            fullText: <Text style={styles.text}>We can learn so much from the life of Abraham. For starters, God can call us
                at any time of life. Without protesting or postponing, Abraham said, “Yes,
                Lord!” And his unquestioning obedience is a rebuke to those of us who say,
                “I'm too old, leave it to the younger ones.” Or, “I've earned the right to relax
                and take it easy; let others do the heavy lifting.” For another, there's no
                retirement in the service of the King of Kings! He has the right to ask us for
                anything and send us anywhere at any season of life.
                <Br />
                We also learn that God's plan brings fulfillment—and it's always greater
                than our own personal fulfillment. Abraham was called to populate the entire
                Middle East: <Bold>“I will make you…a great nation…I will bless you…make
                your name great, and you shall be a blessing” (Genesis 12:2 NIV).</Bold> These
                words are a rebuke to those among us who seek to make a name for ourselves
                rather than allowing God to do it for us. In God's Kingdom, you don't seek
                status; you earn it by being of service to others.
                <Br />
                Finally, we learn that God will treat others according to how they treat us.
                God told Abraham, <Bold>“I will bless those who bless you, and I will curse him
                who curses you…” (Genesis 12:3 NKJV).</Bold> When we are in God's will, we
                don't have to promote or protect ourselves, God will do it for us. “He
                permitted no man to do them wrong; yes, He rebuked kings for their sakes,
                saying, 'Do not touch My anointed ones…'” <Bold>(1 Chronicles 16:21-22 NKJV).</Bold>
                Knowing that sets us free to go where God wants us to go and do what He
                wants us to do.
            </Text>,
            prayerPoints: [
                "Lord, open my ears to hear You, in the name of Jesus.",
                "I receive an obedient heart, in the name of Jesus.",
                "Lord, lead me according to Your will, in the name of Jesus.",
            ]
        },
        {
            day: 19,
            topic: "Abraham (2)",
            wordStudy: "Genesis 12:1-5",
            confession: "The Lord will instruct me and teach me in the way which I shall go: He will guide me with His eye (Psalm 32:8).",
            fullText: <Text style={styles.text}>Abraham had the ability to hear God's voice. <Bold>“Now the Lord…said to
                Abram: 'Get out of your country, from your family and from your
                father's house, to a land that I will show you'” (Genesis 12:1 NKJV).</Bold>
                Abraham couldn't turn to the Scriptures for guidance, and there were no
                prophets around to say, “Thus saith the Lord…” Getting it right was critical.
                He was being called to leave his job, his home and his security. He needed to
                know that the voice he was hearing was truly God's voice. This leads us to
                conclude that Abraham had developed a relationship with the Lord; and
                building a relationship takes time, effort, sensitivity and commitment. Others
                may counsel you and confirm what God is telling you, but ultimately you must
                learn to hear from God for yourself. Nothing increases your confidence like
                knowing God has given you clear direction for your life.
                <Br />
                Author Gary Thomas says he and his wife prayed extensively about
                buying a house and gave God many opportunities to close the door. Five years
                later, when the house was worth considerably less than they paid for it, they
                asked God, “Why didn't You stop us?” At that point God's Spirit whispered,
                “Have you ever considered the possibility that I wanted you in that
                neighborhood to minister there, rather than to bolster your financial equity?”
                God didn't tell Abraham, “Stay here, and I'll make you even richer.” No, He
                said, “Get out.” Yes, he became rich. But does obedience obligate God to bless
                us in the way we think He should, or can it call us to sacrifice? Think about the
                Cross before you answer that one!
            </Text>,
            prayerPoints: [
                "By the power of God, I separate myself from my evil foundation, in the name of Jesus.",
                "I receive divine direction, in the name of Jesus.",
                "Any manipulation against my progress, be terminated, in the name of Jesus.",
            ]
        },
        {
            day: 20,
            topic: "Do Something!",
            wordStudy: "Acts 6:1-7",
            confession: "I must work the works of Him that sent me, while it is day: the night cometh, when no man can work (John 9:4).",
            fullText: <Text style={styles.text}>Are you easily swayed by what others say and do, or do you have a plan and
                stick with it? Are you doing what you love, or just doing things out of
                mindless routine? Mark Twain said, “The secret of success is making your
                vocation your vacation.” In other words, love what you do and do what you
                love! Many of us never get around to fulfilling God's purpose for our lives
                because we're so busy keeping everybody happy. This world is filled with
                people who think they know what you should be doing with your life. No, it's
                your life, and when you stand before God, He won't ask anybody else about it
                except you! Are you courageous enough to follow your heart rather than the
                crowd? Are you focused, even when many voices try to draw you away from
                your purpose?
                <Br />
                An interesting phenomenon occurs when you have no strong convictions
                or clear purpose of your own; you tend to get irritated with those who do. The
                term whatever seems to be especially popular these days. While we must
                always strive to live in harmony and to consider other people's feelings and
                viewpoint, God's Word condemns complacency, indifference and an
                unwillingness to stand up for what we know is right. We need to be
                intentional. We can't just stand around waiting to see what everyone else is
                going to do, then follow the crowd. There are basically two kinds of people:
                those who wait for something to happen, and those who make things happen.
                Don't say, “I wish they would do something about this problem.” You are
                “they.” Do something!
            </Text>,
            prayerPoints: [
                "Lord, I thank You for the purpose of my creation, in the name of Jesus.",
                "I receive grace to fulfill my purpose, in the name of Jesus.",
                "Any power attacking my destiny, die, in the name of Jesus.",
            ]
        },
        {
            day: 21,
            topic: "Knowing What's Important for You",
            wordStudy: "Acts 6:1-7",
            confession: "I must work the works of him that sent me, while it is day: the night cometh, when no man can work (John 9:4).",
            fullText: <View>

                <Text style={styles.text}>
                    The New Testament church experienced phenomenal growth when its leaders
                    recognized what they were not called to do, delegated it to others and got back
                    to doing what only they could do. As a result, <Bold>“the number of the disciples
                    multiplied greatly in Jerusalem” (Acts 6:7 NKJV).</Bold> Keeping your mind on
                    what's important for you requires simplification, and simplification means
                    trade-offs. It means saying no to certain things you'd like to do. If you don't
                    choose what trade-offs you make, someone else will. The Pareto Principle
                    teaches that 20 percent of your effort produces 80 per cent of your success. So
                    identify your 20 percent, stay there, delegate the rest or let it go.
                    <Br single/>
                    To help you do this, answer these three questions:
                    <Br />
                </Text>

                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            <Italic>What takes up most of your time and energy?</Italic> Determine how your
                            activities can be categorized according to the following list: (a)
                            something I was taught or told I should do; (b) something I see other
                            successful people doing; (c) something I know I should be doing.
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            <Italic>Are you maximizing your strengths?</Italic> If you need help determining what
                            your strengths are, seek advice from others who know you well, then ask
                            yourself, “Am I doing them? Am I growing in my ability to do them? Am
                            I enlisting others to do what I can't or shouldn't do?”
                        </Text>
                    },
                    {
                        serial: 3,
                        body: <Text>
                            <Italic>Are you spread out too thinly?</Italic> Are you determined to know everything
                            that goes on in every area of your life? Is your philosophy, “If you want
                            something done right, do it yourself?” No, nobody has that much time
                            and energy! Learn to let some stuff go, get back into focus and start
                            concentrating on what's important for you.
                        </Text>
                    },
                ]} />
                
            </View>,

            prayerPoints: [
                "I will not lose focus, in the name of Jesus.",
                "I reject distraction in every area of my life, in the name of Jesus.",
                "I receive grace to use my time wisely, in the name of Jesus.",
            ]
        },
        {
            day: 22,
            topic: "The Best of Both Worlds",
            wordStudy: "Matthew 11:28-30",
            confession: "I have eternal life in me because I believe in the Son of God (John 3:36).",
            fullText: <Text style={styles.text}>It's said that the wealthy English Baron Fitzgerald had only one child, a son.
                Early in his teens the boy's mother died. Tragically, in his late teens the boy
                also died. In the meantime, Fitzgerald's financial holdings greatly increased
                due to acquiring the artwork of Europe's greatest masters. Before his death,
                Fitzgerald left explicit instructions that an auction be held at which his entire
                art collection would be sold. A big crowd of prospective buyers gathered.
                Among them were many well-known museum curators and private
                collectors eager to bid. The artwork was displayed for viewing before the
                auction began.
                <Br />
                Among the paintings was one that received little attention. It was of poor
                quality and by an unknown local artist. It happened to be a portrait of
                Fitzgerald's only son. As the auction began, the auctioneer read from
                Fitzgerald's will which clearly instructed that the first painting to be sold was
                of “my beloved son.” Because of its poor quality the painting didn't receive
                any bids—except one. The bidder was the old servant who'd helped raise the
                boy and had dearly loved him. For less than a pound he bought the painting.
                <Br />
                At that point the auctioneer stopped the bidding and asked the lawyer to
                read again from the will. The crowd was hushed as he read, “Whoever
                purchases this painting of my son gets all my art collection. The auction is
                over!” Christ: without Him you have nothing. But with Him you have the
                best of both worlds: this one, and the one to come.
            </Text>,
            prayerPoints: [
                "I receive grace to follow Jesus to the end, in the name of Jesus.",
                "My enemies shall not overcome me, in the name of Jesus.",
                "I will live to enjoy the blessing of God on the earth, in the name of Jesus.",
            ]
        },
        {
            day: 23,
            topic: "Be a Tither",
            wordStudy: "Malachi 3:8-12",
            confession: "And all nations shall call me blessed, for I shall be a delightsome land, in the name of Jesus (Malachi 3:12).",
            fullText: <Text style={styles.text}>It's estimated that less than one-tenth of all those who attend church give a
                tenth of their income to God's work, even though the Bible clearly teaches that
                the first tenth of all we earn belongs to God. <Bold>“A tithe of everything…is holy
                to the Lord” (Leviticus 27:30 NIV).</Bold> Today it is estimated that two-thirds of
                the church's income comes from people aged 69 or older. That's alarming! We
                need to teach our children the responsibility and rewards of tithing. Perhaps
                you think tithing only applied to those who lived under the Old Testament law,
                like the Pharisees. No, Jesus said, <Bold>“…Unless your righteousness surpasses
                that of the Pharisees… you will certainly not enter the Kingdom of
                Heaven” (Matthew 5:20 NIV).</Bold> The tithe is a minimum; your love and faith
                should determine the maximum.
                <Br />
                Abraham paid tithes 600 years before the law <Bold>(Genesis 14:20).</Bold> Then
                Jesus endorsed it, saying we <Bold>“ought”</Bold> to tithe <Bold>(Matthew 23:23)</Bold>. And the
                writer to the Hebrews confirmed that it is still God's plan <Bold>(Hebrews 7:4-5)</Bold>. As
                believers, we are the spiritual seed of Abraham, and as such, his blessings
                have been promised to us <Bold>(Galatians 3:14)</Bold>. But if you want what Abraham
                had, you've got to do what Abraham did—and he was the first man in
                Scripture to tithe! Giving God the first tenth of your income makes Him
                number one in your priorities. His Word says, <Bold>“Bring the whole tithe… Test
                Me in this…and see if I will not throw open the floodgates of Heaven and
                pour out so much blessing that you will not have room enough for it”
                (Malachi 3:10 NIV)</Bold>. Do you want to walk in God's blessings? Become a
                tither!
            </Text>,
            prayerPoints: [
                "I receive grace to be faithful in my tithing, in the name of Jesus.",
                "Let the blessings of the Lord be released upon the work of my hands, in the name of Jesus.",
                "I shall be a delightsome land, in the name of Jesus.",
            ]
        },
        {
            day: 24,
            topic: "Pace Yourself (1)",
            wordStudy: "Isaiah 40:28-31",
            confession: "I shall mount up with wings like eagles, I shall run and not be weary and walk but not faint, in the name of Jesus (Isaiah 40:31).",
            fullText: <Text style={styles.text}>Too many of us are like the man who said, “When I rest, I rust.” Here's a news
                flash: There will always be something else to do! If you're constantly driven
                by “what needs to be done,” you'll burn out and fail to do what God has
                assigned you to do. So here are some practical suggestions for pacing
                yourself.
                <Br />
                First, change your eating habits. The Bible says that it's not smart to stuff
                yourself with too many sweets <Bold>(Proverbs 25:27)</Bold>. You wouldn't put sugar
                into your petrol tank, so be wise: educate yourself about good nutrition.
                Second, be clear about where you want to go. Set specific goals, and have a
                plan and a timetable for reaching them. Don't be controlled by mindless
                routine. Third, instead of ignoring your problems, resolve them. Act before
                they become chronic and start sapping your energy. Next, don't “veg” on TV.
                Too much television dulls you mentally and lulls you into lethargy, <Bold>“but they
                that wait upon the Lord…renew their strength…”(Isaiah 40:31 KJV).</Bold>
                <Br />
                Then, if you're overloaded, don't take on more. Don't say yes out of a
                sense of guilt, false responsibility or the need to be needed. Sixth, take a
                break. When did you last take time to get away? A change of pace and fresh
                scenery are great battery-chargers. Finally, get to bed earlier. When you burn
                the candle at both ends, you are the one who burns out. You say, “But I'm
                doing it for the Lord.” Notice what the Lord says, <Bold>“It is vain for you to rise
                up early, to sit up late…for so He gives His beloved sleep” (Psalm 127:2
                NKJV).</Bold>
            </Text>,
            prayerPoints: [
                "I receive strength from above, in the name of Jesus.",
                "Father, grant me wisdom to manage my health, in the name of Jesus.",
                "I reject every form of sickness, in the name of Jesus.",
            ]
        },
        {
            day: 25,
            topic: "Pace Yourself (2)",
            wordStudy: "Matthew 11:28-30",
            confession: "I can do all things through Christ who strengthens me, in the name of Jesus (Philippians 4:13).",
            fullText: <Text style={styles.text}>Galileo said, “I do not feel obliged to believe that the same God who has
                endowed us with sense, reason and intellect has intended us to forego their
                use.” When Jesus says, <Bold>“…My yoke is easy and My burden is light”
                (Matthew 11:30 NKJV),</Bold> His intention for you is to discover His will for your
                life, unlock and develop the gifts He's given you, draw daily on His power, use
                your common sense, and don't live stressed out. That means you need to
                change your attitude. Have you ever noticed how some people on a roller
                coaster close their eyes, clench their teeth and wait for the ride to end, while up
                front the wide-eyed thrill-seekers relish every peak and plunge? They're all on
                the same trip, but their attitudes are entirely different. You can't always control
                what happens to you, but you can decide how you'll respond to it. For instance,
                choosing to see problems as opportunities for growth develops you instead of
                draining you.
                <Br />
                You also need to get physical. <Bold>“Physical exercise has…value” (1
                Timothy 4:8 GNT).</Bold> You don't have to drive 40 minutes to a health club, then
                wait for 30 minutes to get on a treadmill for 20 minutes. Just start walking! A
                brisk walk works wonders for you physically and mentally. Come on, get
                moving! Finally, you must tap into the power of God's Word. Practice
                scriptural affirmations: <Bold>“Let the word of Christ dwell in you richly…”
                (Colossians 3:16 NIV).</Bold> But before that can happen, you must feed your mind
                with the Scriptures, then meditate on them until they take root and grow within
                you. That way they'll be there when you need them!
            </Text>,
            prayerPoints: [
                "Father, the enemy shall not write the last chapter of my life, in the name of Jesus.",
                "Let the Word of God profit me, in the name of Jesus.",
                "Father, grant me the grace to meditate on the Word of God daily, in the name of Jesus.",
            ]
        },
        {
            day: 26,
            topic: "Performance-based or Grace-based?",
            wordStudy: "Matthew 20:1-14",
            confession: "It is not by works of righteousness, but by His grace alone (Ephesians 2:8-9; Titus 3:5).",
            fullText: <Text style={styles.text}>In Christ's parable, five different groups of workers are hired. The first group
                complained when those who worked only the last hour got paid the same
                wages as those who worked all day. They are typical of performance-based
                Christians. They think that because they avoid certain sins, God's getting a
                good deal. Instead of rejoicing with those who come late to the Kingdom,
                they're resentful. They're easy to spot because they've three characteristics.
                They show a complaining spirit. They overlook the good, home in on the
                negative, and seek out people who agree with them. What's the cure? A good
                memory! Recalling what God has done for you, and all the grace-based
                benefits of His love you enjoy each day.
                <Br />
                Performance-based Christians have also got a resentful spirit. Instead of
                resting in God's grace they act like duty-bound soldiersconscripts, not
                volunteers! They struggle constantly for God's approval, never feel like they
                measure up, and their hostility prevents them from experiencing joy. But the
                moment they begin to reflect on God's goodness to them, their resentment
                begins to lessen. Finally, they show a judgmental spirit. “It's not fair,” they
                think. “I did the work and they get the credit.” They forget that God dispenses
                gifts, not wages. <Bold>“He does not…repay us according to our iniquities”
                (Psalm 103:10 NIV)</Bold>, but lavishes us with grace and mercy.
                <Br />
                So who are these grace-based Christians? Latecomers with no contracts
                or merits; their relationship with God is based solely on grace. They're happy
                with whatever He's offering, humbled by His goodness, and motivated to
                work by overwhelming gratitude.
            </Text>,
            prayerPoints: [
                "Let the grace and mercy of God be sufficient for me, in the name of Jesus.",
                "I reject every spirit of bitterness, in the name of Jesus.",
                "Father, deliver me from the works of the flesh, in the name of Jesus.",
            ]
        },
        {
            day: 27,
            topic: "Strive for Integrity, Not Popularity",
            wordStudy: "Philippians 2:5-11",
            confession: "Let integrity and uprightness preserve me, in the name of Jesus (Psalm 25:21).",
            fullText: <Text style={styles.text}>Joseph refused the advances of Potiphar's wife; not because he thought he
                couldn't get away with it, but because he knew he couldn't live with himself if
                he accepted them. Ted Engstrom writes, “The world needs people who cannot
                be bought; whose word is their bond; who put character above wealth; who
                possess opinions and a will; who are larger than their vocations; who don't
                hesitate to take chances; who won't lose their individuality in a crowd; who
                will be as honest in small things as they are in great things; who will make no
                compromise with wrong; whose ambitions are not confined to their own selfish
                desires; who will not say they do it 'because everybody else does it'; who are
                true to their friends through good report and evil report, in adversity as well as
                in prosperity; who do not believe that shrewdness, cunning and hardheartedness
                are the best qualities for winning success; who are not afraid to
                stand for the truth even when it's unpopular; who say 'no' with emphasis, even
                though the rest of the world says 'yes.'”
                <Br />
                In what he calls “a compromise of integrity,” psychiatrist Leo Rangell
                analyses the relationship between former President Nixon and some of his
                closest confidants in the Watergate scandal. He records a conversation between
                Senator Howard Baker and Nixon aide Herbert Porter.
                <Br />
                Baker: “Did you ever have qualms about what you were doing?”
                <Br single/>
                Porter: “Yes, I did!”
                <Br single/>
                Baker: “Why didn't you [do anything]?”
                <Br single/>
                Porter: “…Group pressure… the fear…of not being considered a team player.”
                <Br single/>
                So strive for integrity, not popularity!
            </Text>,
            prayerPoints: [
                "Grant me the grace to live a life of integrity, in the name of Jesus.",
                "I reject and refuse to compromise the truth, in the name of Jesus.",
                "The Lord will defend me in the midst of my adversaries, in the name of Jesus.",
            ]
        },
        {
            day: 28,
            topic: "Keep the Torch Burning",
            wordStudy: "John 9:1-7",
            confession: "I must work the works of him that sent me, while it is day: the night cometh, when no man can work (John 9:4)..",
            fullText: <Text style={styles.text}>In ancient Greek marathons, a torch was handed to each runner at the starting
                line. To win, they had to cross the finishing line with their torch still burning.
                What a picture! <Bold>“For everyone to whom much is given, from him much will
                be required” (Luke 12:48 NKJV).</Bold> In God's Kingdom you are called to run
                your best race and cross the finishing line with the fire in your heart still
                burning. The torch race was a tough one that led through the mountains and
                valleys. Doubtless there were times when others would pass you by, when
                your strength would fail, when you lost your way and had to get back on track,
                or stumbled and had to get back up. What counted in this race was not style,
                but staying power! The Bible says, <Bold>“Run with endurance the race that is set
                before us, looking unto Jesus…lest you become weary and
                discouraged…” (Hebrews 12:1-3 NKJV).</Bold>
                <Br />
                How did Jesus run His race? As a boy of twelve He told His parents, <Bold>“I
                must be about my Father's business” (Luke 2:49 NKJV).</Bold> Later, when the
                crowd tried to take Him in a different direction and impose their agenda on
                Him, He said, <Bold>“I must work the works of Him that sent Me…” (John 9:4
                NKJV)</Bold> Facing the cross, He announced, <Bold>“For this cause I was born…”
                (John 18:37 NKJV).</Bold> On the cross, He announced, <Bold>“It is finished!” (John
                19:30 NKJV)</Bold>. In Revelation we see one of our last recorded glimpses of
                Christ, and <Bold>“His eyes [are] like a flame of fire” (Revelation 1:14 NKJV)</Bold>. He
                crossed the finishing line with the torch still burning. And He is our example!
            </Text>,
            prayerPoints: [
                "Let the fire of God continue to burn in CLAM, in the name of Jesus.",
                "Let the fire of God consume every obstacle in my life, in the name of Jesus.",
                "My fire of revival will not go down, in the name of Jesus.",
            ]
        },
        {
            day: 29,
            topic: "Marriage Rights (1)",
            wordStudy: "Jeremiah 31:1-4",
            confession: "The God of heaven shall be the Lord over my family, in the name of Jesus.",
            fullText: <Text style={styles.text}>Sonja Ely writes, “I was watching my five-year-old granddaughter play with
                her toys. At one point she staged a wedding, first playing the role of the
                mother who assigned specific duties, then suddenly becoming the bride with
                her 'teddy bear' groom. She picked him up and said to the 'minister' presiding,
                'Now you can read us our rights.' Without missing a beat, she became the
                minister who said, 'You have the right to remain silent, anything you say may
                be held against you, you have the right to have an attorney present, you may
                kiss the bride.'”
                <Br />
                To know what your marriage rights are, you must understand that, first,
                marriage is God's idea. In Eden He said, <Bold>“It is not good that man should be
                alone; I will make him a helper comparable to him” (Genesis 2:18
                NKJV)</Bold>. The word comparable means “compatible with his (or her) needs.”
                When you marry someone, you marry everything they've been through. Each
                of you brings your own baggage. Unless you sort out what to keep and what to
                discard, things can quickly erode. You must also understand that when you
                leave God out, you have problems. Satan has made marriage one of his prime
                targets. He loves to promote strife. When it comes to marriage, we must learn
                to forgive, <Bold>“lest Satan should take advantage of us; for we are not
                ignorant of his devices” (2 Corinthians 2:11 NKJV).</Bold>
                <Br />
                God's plan for your relationship is strength and harmony, not strife and
                confusion. He wants to help you build a strong, loving union that glorifies
                Him. To do that, you must show grace, and resolve to make Jesus Lord of your
                relationship.
            </Text>,
            prayerPoints: [
                "Bless God for your marriage.",
                "I reject confusion in my marriage in Jesus name.",
                "Let the peace of God reign in my marriage in Jesus name.",
            ]
        },
        {
            day: 30,
            topic: "Marriage Rights (2)",
            wordStudy: "Ecclesiastes 9:7-9",
            confession: "The voice of thanksgiving and the voice of them that make merry shall proceed always from my marriage, in the name of Jesus (Jeremiah 30:19).",
            fullText: <Text style={styles.text}>In discussing marriage on his TV sitcom, Jerry Seinfeld tells his friend why
                he's not married: “No healthy person would want the neglect I have to offer.”
                Let's face it, even the best marriages are made up of two imperfect people
                who sometimes neglect each other. And when you've been hurt, it's easy to
                react in the flesh instead of responding in the Spirit.
                <Br />
                Some hurts go deep. At this point we must remember that forgiveness is a
                decision, but trust is a process; when it's been torn down, it takes time to
                rebuild. Men and women often perceive trust differently. When a woman's
                been hurt, her husband may think an apology should immediately enable her
                to trust him again, move on, and not talk about it. That's not so; two things
                need to happen. First, the offending partner needs to acknowledge what
                they've done. Don't just tell your partner to “get over it.” Validate their
                feelings, even though they act like they don't want you to. Acknowledge their
                pain. When others rationalize or trivialize what's hurting us, it only makes us
                angrier. Only when we feel validated do our wounds begin to heal. Second,
                the offended partner needs to make sure that bitterness doesn't creep in.
                <Br />
                “How can I do that?” you ask. By refusing to stay hurt any longer than is
                absolutely necessary, and by allowing God to heal your heart and restore
                your love. The Bible says, <Bold>“If you hear His voice today, don't be
                stubborn!” (Hebrews 4:7 CEV)</Bold>. When God gives you the grace to forgive
                and release the hurt, you need to seize it!
            </Text>,
            prayerPoints: [
                "Thank God for sustaining your marriage",
                "I reject bitterness in my marriage, in the name of Jesus.",
                "Help me to experience fresh oil and new wine in my marriage, in the name of Jesus.",
            ]
        },
        {
            day: 31,
            topic: "Marriage Rights (3)",
            wordStudy: "2 Samuel 12:19-24",
            confession: "I receive the comfort of God upon my home and marriage, in the name of Jesus.",
            fullText: <Text style={styles.text}>After the death of the child David fathered with Bathsheba, we read, <Bold>“Then
                David got up…washed himself…changed his clothes…went to the
                Tabernacle and worshipped the Lord. After that, he returned to the
                palace and…ate. His advisers were amazed. 'We don't understand you,'
                they told him. 'While the child was still living, you wept and refused to
                eat. But now that the child is dead, you have stopped your mourning and
                are eating again'” (2 Samuel 12:20-21 NLT).</Bold>
                <Br />
                Why did David mourn more intensely before the baby died than he did
                after? Because men grieve hard, but not necessarily as long. Once
                something's over, they've a tendency to move on. This can cause problems.
                Often men genuinely don't understand why their wives can't accept that
                “what's done is done” and move on too. Listen to David's logic: <Bold>“I fasted and
                wept while the child was alive, for I said, 'Perhaps the Lord will be
                gracious to me and let the child live.' But why should I fast when he is
                dead? Can I bring him back again? I will go to him one day, but he cannot
                return to me” (2 Samuel 12:22-23 NLT).</Bold>
                <Br />
                The Bible says that God <Bold>“comforts us…so that…when they are
                troubled, we will be able to give them the same comfort” (2 Corinthians
                1:4 NLT).</Bold> David's next move was crucial. He <Bold>“comforted Bathsheba, his
                wife” (2 Samuel 12:24 NLT)</Bold>. Notice, David didn't ask, “What's wrong with
                you? When are you going to get over this?” He recognized that even though
                he was beginning to heal, his wife was still hurting. The healing process
                accelerates once you begin to empathise with and comfort your mate.
            </Text>,
            prayerPoints: [
                "Lord, let Your comfort rule and reign in my home, in the name of Jesus.",
                "I reject trouble in my marriage, in the name of Jesus.",
                "Goodness and mercy shall not depart from my marriage, in the name of Jesus.",
            ]
        },
    ],
    April: [
        {
            day: 1,
            topic: "Qualities of a Good Leader",
            wordStudy: "1Timothy 3:1-10",
            confession: "I will not be lifted up with pride lest I fall into the condemnation of the devil, in the name of Jesus (v. 6).",
            fullText: <View> 
                <Text style={styles.text}>
                    Good leaders practice four qualities:
                </Text>

                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            <Bold>Submission.</Bold> Only a leader who has followed well knows how to lead
                            others well. Connecting with people becomes possible because you've
                            walked in their shoes. Leaders who've never submitted to authority tend
                            to be proud, unrealistic and autocratic.
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            <Bold>Self–discipline.</Bold> To make consistently good decisions requires character
                            and self–discipline. To do otherwise is to lose control of ourselves.
                            British essayist John Foster writes, “A man without decision of character
                            can never be said to belong to himself. He belongs to whatever can make
                            a captive of him.” Peter writes, <Bold>“Knowing God leads to self–control”
                            (2 Peter 1:6 NLT).</Bold>
                        </Text>
                    },
                    {
                        serial: 3,
                        body: <Text>
                            <Bold>Patience.</Bold> Leaders look ahead, think ahead and want to move ahead.
                            That's what makes them leaders. But the true goal of leadership is not to
                            cross the finishing line first, but to take as many others with you as you
                            can. For that reason you have to deliberately slow your pace, stay
                            connected to your people, keep them informed and inspired, enlist the
                            help of others to fulfill your vision, and keep going. And you can't do that
                            if you're running too far ahead of everybody. Solomon writes,
                            <Bold>“Finishing is better than starting. Patience is better than pride”
                            (Ecclesiastes 7:8 NLT).</Bold>
                        </Text>
                    },
                    {
                        serial: 4,
                        body: <Text>
                            <Bold>Accountability.</Bold> Many people feel that accountability is a willingness to
                            explain your actions. In reality, real accountability begins long before
                            you take action. Most wrong actions come about because we are not
                            being accountable early enough.
                        </Text>
                    },
                ]} />
            </View>,
            prayerPoints: [
                "Father, help me to be disciplined in every area of my life, in the name of Jesus.",
                "Father, help me to be submissive and accountable in my assignment for the kingdom, in the name of Jesus..",
                "Lord, remove every seed of pride from my life, in the name of Jesus.",
            ]
        },
        {
            day: 2,
            topic: "Don't Take the Bait",
            wordStudy: "2 Corinthians 2: 9-11",
            confession: "Satan will not get an advantage over me. I will not be ignorant of his devices, in the name of Jesus (v. 11).",
            fullText: <Text style={styles.text}>I give all glory to God for preserving and upholding my life to celebrate
                another birthday. We also give all honour and adoration to then almighty God as we celebrate our 20th anniversary today as a church. Christ Livingspring
                Apostolic Ministry (CLAM) is indeed a child of G-R-A-C-E!
                <Br />
                Satan, whom the Bible refers to as “the tempter,” works like a seasoned
                fisherman in two ways. First, he knows that fish get hungry, and he knows
                when they will be biting. Understand this: you were born with a hole in your
                soul that only God can fill, and unless you turn to God and build a
                relationship with Him, you will keep trying to fill that hole with the wrong
                things. “Where can I find the right things?” you ask. The Psalmist answers,
                <Bold>“In Your presence is fullness of joy; at Your right hand are pleasures
                forevermore” (Psalm 16:11 NKJV).</Bold> There's a marked difference in how
                Joseph responded to temptation with Potiphar's wife and how David
                responded to it with Bathsheba. David looked, and kept looking, but Joseph
                fled! The Bible says, <Bold>“If sinners entice you, do not consent” (Proverbs
                1:10 NKJV).</Bold>
                <Br />
                Be honest with yourself. Acknowledge that there are places you
                shouldn't go, habits you shouldn't indulge, and the company you should
                never be in. Second, he knows what bait will hook the fish. Next time
                somebody fails and you're tempted to think, “I'd never do that,” stop and ask
                yourself, “What would it take to make me bite?” You may not know the
                answer to that question at the moment, but Satan does. And he is patient; he'll
                wait a lifetime if he has to, he's in no hurry. He doesn't mind whether you fall
                through “youthful lusts,” or on the last few laps through more subtle sins—so
                long as you lose the race.
                <Br single/>
                So, don't take the bait.
            </Text>,
            prayerPoints: [
                "Give thanks to God, in the name of Jesus, for the 20 Anniversary of CLAM and all God has been doing and will continue to do in our church and ministry.",
                "Satanic traps will not catch me and my family, in the name of Jesus.",
                "Lord, empower me to resist every satanic bait that might come my way, in the name of Jesus.",
            ]
        },
        {
            day: 3,
            topic: "Working Together",
            wordStudy: "Romans 12:3-10",
            confession: "I shall be kind and affectionate towards my brethren and play my part in the body of Christ with all humility, in the name of Jesus (Romans 12:10).",
            fullText: <Text style={styles.text}>If you're wise you'll pray, “Lord, who do I need in my life at this time?” and,
                “Who do you want me to help?” The first question requires humility, the
                second requires unselfishness. There's no limit to what can be accomplished
                when nobody cares who gets the credit. A great illustration of this is the life of
                an international mountain climber and his team. He tells how he and his team
                members accomplished what no other human beings ever had: conquering
                Mount Everest.
                <Br />
                He said, “For each level we reached, a higher degree of teamwork was
                required. One group would exhaust themselves just getting the equipment up
                the mountain for the next group. Two–man teams would work finding a path,
                cutting steps, securing ropes, spending themselves to make the next leg of the
                climb possible for others. You don't climb a mountain like Everest by trying to
                race ahead on your own or by competing with your comrades. No, you do it
                slowly and carefully, by unselfish teamwork. Certainly I wanted to reach the
                top myself; it was the thing I'd dreamed of all my life. But if the lot fell to
                someone else I would take it like a man, not a crybaby. Where would this
                climber have been without the others? Without the climbers who made the
                route? The people who carried the loads? Those who cleared the path ahead?
                It was only through the work and sacrifice of them all that he had a chance at
                the top.”
                <Br />
                The Bible teaches, <Bold>“We…are…one body, and each member belongs to
                all the others” (v. 5)</Bold>. So let's keep that in mind today and try to work together!
            </Text>,
            prayerPoints: [
                "Father, help me to be a good team player in my Kingdom assignment, in the name of Jesus.",
                "Lord, unite all Christians all over the world so, together, we can win more souls for Your Kingdom, in the name of Jesus.",
                "The gates of hell shall not prevail against the Church, in the name of Jesus.",
            ]
        },
        {
            day: 4,
            topic: "Do I Have Compassion for Others?",
            wordStudy: "Luke 10:30-37",
            confession: "I will have compassion towards others and I will be courteous always by the strength of God, in the name of Jesus (1 Peter 3:8).",
            fullText: <Text style={styles.text}>The priest and the Levite in the story of the Good Samaritan saw the plight of
                the injured man, but the Good Samaritan exercised compassion and did
                something about it. So, ask yourself, “Have I become too calloused to care or
                too busy to be bothered?” There is so much need around us, and God knows
                we can't meet all of it. But what needs are you called to meet?
                <Br />
                Next time you see the Salvation Army asking for donations, remember
                how it all started. There was a couple who were living in London 155 years
                ago. For the first ten years of their time there, William Booth was in a
                quandary: What was God calling him to do? One day his wife Catherine, a
                Bible teacher, was invited to speak at a local event. While they were there
                William took a late–night walk through the slums of the East End. Every fifth
                building was a pub. Most had steps at the counter so little children could climb
                up and order gin. That night he told Catherine, “I seem to hear a voice
                sounding in my ears, 'Where can you go to find such heathen as these, and
                where is there so great a need for your labours?' Darling, I have found my
                destiny!”
                <Br />
                Later that year, 1865, the couple opened the East London Christian
                Mission in London's slums. Their life's vision: To reach the down–and–outers
                who other Christians ignored. That simple vision of two people grew into the
                Salvation Army, which ministers through over three million members in 113
                nations. Now, stop and ask yourself, “Do I have compassion for others?”
            </Text>,
            prayerPoints: [
                "Help me, Lord, to have compassion for others, in the name of Jesus.",
                "Open my eyes to see the needs of others around and empower me to be of help, in the name of Jesus.",
                "I will never lack help and helpers, in the name of Jesus.",
            ]
        },
        {
            day: 5,
            topic: "Get Serious about Living a Godly Life",
            wordStudy: "Galatians 5:16-25",
            confession: "I will walk in the spirit and I will not fulfill the lust of the flesh, in the name of Jesus (Galatians 5:16).",
            fullText: <Text style={styles.text}>The truth is if we are not seeking the Lord, the devil is seeking us! And he gets
                us when we lower our guard and become complacent. If you are serious about
                living a godly life, read on:
                <Br />
                <Bold>“Live according to your new life in the Holy Spirit. Then you won't be
                doing what your sinful nature craves. The old sinful nature loves to do
                evil, which is just the opposite of what the Holy Spirit wants. And the
                Spirit gives us desires that are the opposite of what the sinful nature
                desires. These two forces are constantly fighting each other, and your
                choices are never free from this conflict. When you follow the desires of
                your sinful nature, your lives will produce these evil results: sexual
                immorality, impure thoughts, eagerness for lustful pleasure, idolatry,
                participation in demonic activities, hostility, quarrelling, jealousy,
                outbursts of anger, selfish ambition, divisions, the feeling that everyone
                is wrong except those in your own little group, envy, drunkenness, wild
                parties, and other kinds of sin. But when the Holy Spirit controls our
                lives, He will produce this kind of fruit in us; love, joy, peace, patience,
                kindness, goodness, faithfulness, gentleness, and self-control… Those
                who belong to Christ Jesus have nailed the passions and desires of their
                sinful nature to His cross and crucified them there. If we are living now
                by the Holy Spirit, let us follow the Holy Spirit's leading in every part of
                our lives” (Galatians 5:16-25 NLT).</Bold>
                <Br />
                When you get serious about living a godly life, God will help you.
            </Text>,
            prayerPoints: [
                "Help me, O Lord, to live in the Spirit and to also walk in the Spirit, in the name of Jesus.",
                "I decree and declare that my life shall be fruitful unto the Lord, in the name of Jesus.",
                "I shall not be discouraged in my walk with God, in the name of Jesus.",
            ]
        },
        {
            day: 6,
            topic: "Learn to Value Others",
            wordStudy: "Mark 12:31-33",
            confession: "I am the salt of the earth, I will not lose my value, in the name of Jesus (Matthew 5:13).",
            fullText: <Text style={styles.text}>There are two things we should never do: First, to expect to feel fully at home
                in this world, because <Bold>“we are citizens of Heaven” (Philippians 3:20).</Bold> And
                second, to become so heavenly minded that we are no earthly use. The “salt”
                and “light” principles Jesus taught call for us to influence and illuminate
                others for good and for God. That means taking responsibility to do things
                better at home, on the job, and in all our dealings. If the only people you show
                genuine care for are in your church, your salt isn't flavouring and your light
                isn't dispelling darkness! Christ's command to <Bold>“Love your neighbor”</Bold>
                includes the less-than-lovable. And you only love others when you add value
                to their lives!
                <Br />
                You may ask, “How do I do that?” Here are four suggestions. First, by
                truly valuing them. That calls for believing in them before they believe in you,
                serving them before they serve you, loving them before they love you, and
                giving to them without expecting anything in return. Second, by making
                yourself more valuable. You can't give what you don't have, so you must earn
                and grow in order to give and guide. Third, by knowing what they value. What
                happens when you're interested only in your own agenda? You know very
                little about the people around you. Make others' priorities your priorities. Ask
                to hear their stories. Discover their hopes and dreams. Make their success part
                of your mission. Finally, by doing things that God values. When your life is
                done, what will you have lived for? Eventually, everything on earth will turn
                to dust, including you! So give yourself to things that will last beyond your
                lifetime.
            </Text>,
            prayerPoints: [
                "Help me, Lord, to take responsibility to do things better in all my dealings, in the name of Jesus.",
                "My salt will not lose its flavour, in the name of Jesus.",
                "I shall make Heaven, in the name of Jesus.",
            ]
        },
        {
            day: 7,
            topic: "When God Gives You a Vision (1)",
            wordStudy: "Acts 26:19; Jeremiah 29:11",
            confession: "For the vision is yet for an appointed time, but at the end it shall speak, and not lie: though it tarry, I will wait for it; because it will surely come, it will not tarry (Habakkuk 2:3).",
            fullText: <Text style={styles.text}>Paul had a <Bold>“heavenly vision.”</Bold> But there are those who <Bold>“speak a vision of
                their own heart, not from the mouth of the Lord” (Jeremiah 23:16
                NKJV).</Bold> So you must be sure you are operating according to God's plan,
                neither your own nor somebody else's. The story of the Tower of Babel
                teaches us two things. First, that when people make up their mind to do
                something, they often succeed; and second, that succeeding doesn't mean
                that you're in the will of God. The word Babel (confusion) means others can
                think you're right, yet you're wrong. You ask, “But isn't it all right to make
                plans and set goals?” Yes, but your plans and goals should come out of a
                vision that God has given you. When Paul said, <Bold>“I was not disobedient unto
                the heavenly vision,”</Bold> he meant that there's only one acceptable response to
                God's plan—and that was total obedience! Partial obedience, selective
                obedience or delayed obedience are still all disobedience.
                <Br />
                William Carey, who is considered the father of modern missions, gave up
                comfort and fortune to go to India and introduce the Gospel. One of his more
                memorable quotes is: “Expect great things from God; attempt great things for
                God.” But you can only say that with confidence when you know what God
                has called you to do. What He ordains, He sustains! When you set your own
                goals, you lack a heartfelt confidence that God is doing the work; so you wear
                yourself out thinking it's all up to you. However, when you know God has
                given you a vision for your life, you trust Him—even when you can't see any
                way to bring it to pass.
            </Text>,
            prayerPoints: [
                "Help me, Lord, to operate according to Your vision for my life, in the name of Jesus.",
                "I come against the spirit of confusion in my journey in life, in the name of Jesus.",
                "Father, grant me a heart of total obedience to Your vision for my life, in the name of Jesus.",
            ]
        },
        {
            day: 8,
            topic: "When God Gives You a Vision (2)",
            wordStudy: "Jeremiah 1:4-10",
            confession: "The secret of victory lies in knowing that it is God working in me (Philippians 2:13).",
            fullText: <View> 
                <Text style={styles.text}>
                    When God gives you a vision:
                </Text>

                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            <Bold>It will interrupt you.</Bold> Sometimes God will speak in a voice you can't
                            tune out. Other times, like Jonah, God will let you go to the bottom to get
                            your attention. Either way, things won't go right until you say yes to Him.
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            <Bold>It will illuminate you.</Bold> You may not like what God says. Ananias didn't
                            like going to pray for Saul of Tarsus, a man with the power to put
                            Christians to death. But God said, <Bold>“Go, for he is a chosen vessel… I will
                            show him how many things he must suffer for my name's sake” (Acts
                            9:15-16 NKJV)</Bold>. Notice the word suffer. The devil isn't going to send
                            you a congratulatory telegram because you have decided to do God's
                            will, so be prepared for an attack.
                        </Text>
                    },
                    {
                        serial: 3,
                        body: <Text>
                            <Bold>It will inspire you.</Bold> You will accomplish things you never dreamed
                            possible. Look at Gideon. When God found him, he was hiding in a
                            winepress <Bold>(Judges 6:11 NKJV)</Bold>; not exactly a promising start. When the
                            angel called him <Bold>“a mighty man of valour” (v. 12 NKJV)</Bold>, he replied, “I
                            am the least in my father's house” <Bold>(v. 15 NKJV)</Bold>. When the angel said,
                            <Bold>“The Lord is with you,”</Bold> he replied, <Bold>“If the Lord is with us, why then
                            has all this happened to us?” (vv. 12-13 NKJV)</Bold>. When he finally took
                            the job, he wondered, <Bold>“How can so few of us defeat so many of them?”</Bold>
                            Yet at that very moment his enemies were having nightmares about him
                            <Bold>(Judges 7:13-15).</Bold>
                        </Text>
                    },
                ]} />
            </View>,
            prayerPoints: [
                "Lord, only Your voice will I hear all the days of my life, in the name of Jesus.",
                "Father, order my steps, in the name of Jesus.",
                "I receive the grace to start and finish well in life, in the name of Jesus.",
            ]
        },
        {
            day: 9,
            topic: "Cut Yourself Some Slack!",
            wordStudy: "James 3:1-2",
            confession: "I am still not all I should be, but I am focusing on, forgetting the past, and looking forward, in the name of Jesus (Isaiah 57:14b).",
            fullText: <Text style={styles.text}>A little boy came home one day with his clothes all muddy so his Mum sent
                him to the basement to drop them into the washing machine. Later, thinking
                things were a bit too quiet, she shouted, “Young man, I hope you're not down
                there running around in your underwear.” Imagine her embarrassment when a
                strange voice replied: “No ma'am, I'm down here servicing your heater.”
                James says, <Bold>“We all make…mistakes…” (James 3:2 NLT)</Bold>. The trick is
                learning to forgive yourself.
                <Br />
                Much of the time we're guilty of doing the wrong thing with the right
                motive. Remember the famous Leaning Tower of Pisa in Italy, where the
                architect designed a three meter deep foundation for a 55 meter tall building?
                How would you like that on your CV? The Apostle Paul says, <Bold>“No…I am still
                not all I should be, but I am focusing…on…forgetting the past and
                looking forward…” (Philippians 3:13 NLT).</Bold> Chuck Swindoll writes:
                “Show me the guy who wrote the rules for perfectionism, and I'll guarantee
                he's a nail biter with a face full of tics…whose wife dreads to see him come
                home.”
                <Br />
                God says, <Bold>“I…am He who blots away your sins…and will never think
                of them again” (Isaiah 43:25 TLB)</Bold>. Now, if God is willing to pardon your
                mistakes and even bury them, isn't it time you stopped beating yourself up,
                received His grace and moved on?
            </Text>,
            prayerPoints: [
                "I receive grace to forgive myself of my past and move on, in the name of Jesus.",
                "Anything in my past holding me down, your time is up, release me by fire, in the name of Jesus.",
                "Lord, remove every seed of bitterness from my life, in the name of Jesus.",
            ]
        },
        {
            day: 10,
            topic: "Stumbling Blocks",
            wordStudy: "Isaiah 57:14; Philippians 2:3",
            confession: "I will trust in the LORD and He will take away every stumbling block out of my way, in the name of Jesus (Isaiah 57:14b).",
            fullText: <View> 
                <Text style={styles.text}>
                    To fulfill your God-given purpose in life you must confront your defects of
                    character, see them as stumbling blocks and begin to remove them. Here are
                    six of the most common ones:
                </Text>

                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            <Bold>Pride—spiritual pride, social pride.</Bold> God hates all forms of pride. His
                            Word says, <Bold>“With humility…regard one another as more important
                            than yourselves” (Philippians 2:3 NAS)</Bold>. Pride toppled satan from his
                            high position in Heaven, and it will topple you too if you let it!
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            <Bold>Insecurity.</Bold> Insecure people aren't willing to take risks. They complain
                            about their lot in life, yet they're afraid to embrace change and do
                            something about it. They prefer to remain comfortable. What's the
                            solution? Stepping out in faith, being confident in God!
                        </Text>
                    },
                    {
                        serial: 3,
                        body: <Text>
                            <Bold>Moodiness.</Bold> Moody people are like the wind, you don't know which
                            way they'll blow. You can't depend on them. Confidence is never built
                            on a person who's fickle.
                        </Text>
                    },
                    {
                        serial: 4,
                        body: <Text>
                            <Bold>Perfectionism.</Bold> Perfectionism is the obsessive need to perform
                            flawlessly. It stifles your creativity, and turns others off. Perfectionists
                            can't affirm themselves, therefore it's difficult for them to affirm
                            anybody else.
                        </Text>
                    },
                    {
                        serial: 5,
                        body: <Text>
                            <Bold>Over-sensitivity.</Bold> Over-sensitive people are always licking their
                            wounds and looking inward; as a result, they're unaware of the needs of
                            others. Ironically, they never understand why they're so lonely.
                        </Text>
                    },
                    {
                        serial: 6,
                        body: <Text>
                            <Bold>Negativity.</Bold> Negative people are hard to be around. Their personality
                            says no to life in general, and people tend to avoid them like the plague.
                            Do you recognize any of these stumbling blocks in your life? If so, pray,
                            ask God for help, and start to remove them.
                        </Text>
                    },
                ]} />
            </View>,
            prayerPoints: [
                "Father, help me to confront and conquer the stumbling blocks in my life, in the name of Jesus .",
                "Lord, break me down and remould me to suit Your purpose for my life, in the name of Jesus.",
                "All stumbling blocks delaying my manifestation, collapse, in the name of Jesus.",
            ]
        },
        {
            day: 11,
            topic: "Use Your Talents to Glorify God",
            wordStudy: "1 Peter 4:8-11",
            confession: "I will use my gifts to minister to others as a good steward of the manifold grace of God (1 Peter 4:10).",
            fullText: <Text style={styles.text}>Your work is important to God. Your job might use as much as 40 hours in a
                week, so it should represent Him. <Bold>“God has given gifts to each of you…
                Manage them well…then God will be given the glory.”</Bold> We've each been
                given talents for the purpose of honouring God in this world. Undiscovered,
                undeveloped gifts dishonour Him; so do misused ones. God has gifted you to
                do something in a way nobody else can. By tapping into your unique abilities
                and using them to promote His Kingdom, you're fulfilling His will in the truest
                sense. Paul says, <Bold>“Everything comes from Him…and is intended for His
                glory” (Romans 11:36 NLT)</Bold>. The breath we breathe, the blood that courses
                through our veins, the grey matter between our ears are all God's investment in
                us, and He expects a return.
                <Br />
                Max Lucado writes, “We exist to exhibit God, to display His glory. We
                serve as canvases for His brush strokes, papers for His pen, soil for His seeds,
                and glimpses of His image… He uncommons the common by turning kitchen
                sinks into shrines, cafes into convents, and nine-to-five workdays into
                spiritual adventures… When you magnify your Maker with your
                strengths…your days grow suddenly sweet.”
                <Br />
                So, be like the great New England preacher Jonathan Edwards, who lived
                by two resolutions: “Resolved first: that all men should live for the glory of
                God. Resolved second: that whether others do or not, I will.'
            </Text>,
            prayerPoints: [
                "Father, give me the grace to use my talents to glorify You, in the name of Jesus.",
                "Lord, empower me to promote Your Kingdom in all that I do, in the name of Jesus.",
                "My life shall glorify God, in the name of Jesus.",
            ]
        },
        {
            day: 12,
            topic: "God's Promise to Prodigals (1)",
            wordStudy: "Luke 15:15-20",
            confession: "God will do for me beyond my asking and thinking, in the name of Jesus (Ephesians 3:20).",
            fullText: <Text style={styles.text}>Here's an interesting and largely unknown thing about the story of the
                Prodigal Son. Jewish families living in small villages were tightly knit
                communities, where people knew one another well. So when something like
                this happened, word travelled fast. When the younger son demanded his
                inheritance, it was like saying to his father, “I can't wait until you die. I want
                what's mine, now!” Such a thing was unheard of. Then he went away, forgot
                the values he had been taught, and squandered his inheritance on parties and
                prostitutes. As a result he ended up destitute, working in a pig sty. For a Jew,
                you can imagine the stigma.
                <Br />
                After breaking his father's heart and the rules of the community, he
                decided to come back home. And that's when his father “ran” to meet him.
                Here's why. Had he reached home after failing so badly, the village elders
                would have held a “Ceremony of Shame” known in Hebrew as Kezazah.
                They'd have taken a clay pitcher and smashed it on the ground in front of him,
                meaning his ties with the community were broken and he was no longer
                welcome. That's why his father ran to meet him. He was saying, “I have to get
                to my son with grace before they get to him with the law. I have to give him
                hope before they take it away. I have a different ceremony in mind: a
                homecoming party to celebrate his restoration.” What the father did for his
                prodigal son that day, God will do for you today, if you will only turn to Him.
            </Text>,
            prayerPoints: [
                "Lord, deliver me from idolatory and disobedience, in the name of Jesus.",
                "Where I have been rejected, I shall be celebrated, in the name of Jesus.",
                "The Lord will do for me and my household beyond our asking and thinking, in the name of Jesus.",
            ]
        },
        {
            day: 13,
            topic: "God's Promise to Prodigals (2)",
            wordStudy: "Luke 15:20-24",
            confession: "O LORD, cause me to know the way in which I should walk, for I lift up my soul to You, in the name of Jesus (Psalm 143:8).",
            fullText: <View> 
                <Text style={styles.text}>
                    Observe carefully and prayerfully what the father did for his prodigal son the
                    moment he humbled himself and said, <Bold>“I have sinned…” (v. 18 NKJV).</Bold>
                    Why? Because God will do the same for you.
                </Text>

                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            The father said to his servants, <Bold>“Bring out the best robe and put it on
                            him…”(v. 22)</Bold>. Can you imagine what this boy smelled like and looked
                            like, after wallowing in the muck of a pig sty? Can you identify with
                            him? Good news: God covers our sinfulness in the robe of Christ's
                            righteousness. And from that point on He sees us “in Christ.” Therefore
                            we are always acceptable in His eyes.
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            The father said <Bold>“…Put a ring on his hand…” (v. 22)</Bold> This was the
                            family signet ring used in transacting business. When placed on wax, it
                            was equal to a signature. More good news: God doesn't partially restore
                            you; He recommissions you and gives you back full authority to do
                            business in His name.
                        </Text>
                    },
                    {
                        serial: 3,
                        body: <Text>
                            <Bold>“Put…sandals on his feet.” (v. 22).</Bold> The prodigal son was getting ready
                            to say to his father, <Bold>“I am no longer worthy to be called your son.
                            Make me like one of your hired servants” (v. 19).</Bold> In those days hired
                            servants didn't wear shoes in public; only sons did. How wonderful—his
                            father gave him the full rights of sonship.
                        </Text>
                    },
                    {
                        serial: 4,
                        body: <Text>
                            <Bold>“Bring the fatted calf here and kill it, and let us eat and be merry” (v.
                            23 ).</Bold> You don't fatten a calf overnight! The father had been planning this
                            celebration for a long time. He never gave up on his son and the word for
                            you today is: God hasn't given up on you either! Come back to Him and
                            let Him restore you.
                        </Text>
                    },
                ]} />
            </View>,
            prayerPoints: [
                "Lord, cause me to know your lovingkindness, in the name of Jesus.",
                "I receive grace to be humble, in the name of Jesus.",
                "I shall be celebrated this year, in the name of Jesus.",
            ]
        },
        {
            day: 14,
            topic: "What Keeps Us from Serving Others (1)",
            wordStudy: "1 Corinthians 13:1-7",
            confession: "I receive grace to be swift to hear, slow to speak, slow to wrath, in the name of Jesus (James 1:19).",
            fullText: <Text style={styles.text}>To serve others effectively, you must be attuned to what they need. What keeps
                us from doing that? First: assumption. A lady in an airport lounge bought a
                packet of biscuits and sat down to read a newspaper. She heard a rustling noise
                and looked up to see the man beside her helping himself to the biscuits. Not
                wanting to make a scene, she leaned over and took one herself, hoping he'd get
                the message. Then she heard more rustling. She couldn't believe it. The man
                was helping himself to another biscuit! There was only one left! She watched
                in disbelief as he broke the remaining biscuit in two, pushed half across to her,
                popped the other half in his mouth, and left. She was still furious when her
                flight was announced.
                <Br />
                Imagine how she felt when she opened her handbag to get her ticket
                out—and found her unopened packet of biscuits! Now be honest, didn't you
                assume the stranger was helping himself to her biscuits? Most people do and
                that tells us a lot about ourselves! Too often we're guilty of making
                assumptions about people and once you put someone in a box, it's hard to think
                of them any other way. Do you agree?
                <Br />
                Every time a good tailor sees a client, he takes new measurements. He
                never assumes they're the same size as they were the last time. That's a good
                policy. Never make assumptions about someone's background, profession,
                race, gender, age, nationality, politics, faith, or other factors. Once you do, you
                stop paying attention and miss clues that can help you to know what they
                really need.
            </Text>,
            prayerPoints: [
                "Lord, deliver me from the spirit of assumption and presumption, in the name of Jesus.",
                "I receive the spirit of discernment, in the name of Jesus.",
                "Lord, deliver me from judging others, in the name of Jesus.",
            ]
        },
        {
            day: 15,
            topic: "What Keeps Us from Serving Others (2)",
            wordStudy: "Romans 12:3-4",
            confession: "My help is in the name of the Lord who made heaven and earth (Psalm 124:8).",
            fullText: <Text style={styles.text}>Another thing that will keep you from serving others is arrogance. What
                others think and feel isn't important to you. Arrogant people seldom meet
                people on common ground. They don't believe they should have to because,
                by their own estimation, they live on higher ground and shouldn't have to
                descend to anyone else's level. They expect others to come to them. Justice
                Louis D. Brandeis observed: “Nine–tenths of the serious controversies that
                arise in life result from misunderstanding, from one man not knowing the
                facts which to the other man seem important, or otherwise failing to
                appreciate his point of view.”
                <Br />
                It's ridiculous for anyone to think they have all the answers. Such people
                can become opinionated, narrow–minded and arrogant, listening only to
                their own thoughts and ignoring advice and suggestions from others. When
                your overriding goal is to build a case for your own viewpoint or yours is a
                “my way or no way” attitude, people will get turned off. To win them you
                must be willing to build a relationship. The letters in the word silent also form
                the word listen. Relationships are built by listening to people, loving them,
                learning from them and leaving them better off than you found them. <Bold>“A wise
                man will hear and increase learning, and a man of understanding will
                attain wise counsel” (Proverbs 1:5 NKJV).</Bold>
            </Text>,
            prayerPoints: [
                "Lord, deliver me from folly, in the name of Jesus.",
                "I receive the grace not to despise others, in the name of Jesus.",
                "Lord, grant me the grace to be merciful to others even as You have shown me mercy, in the name of Jesus.",
            ]
        },
        {
            day: 16,
            topic: "What Keeps Us from Serving Others (3)",
            wordStudy: "Proverbs 14:25-28",
            confession: "I shall not make light the things of God and His kingdom, in the name of Jesus (Matthew 22:5).",
            fullText: <Text style={styles.text}>Indifference is the third thing that keeps us from serving others. A
                well–known comedian quipped, “Scientists announced today that they'd
                found a cure for apathy. However, they claim no one has shown the slightest
                bit of interest in it.” When it comes to communicating, that could be said of
                many of us; we're indifferent to how we come across. We may not necessarily
                feel superior to others, but we don't go out of our way to get to know them
                either. Why? Because it's hard work! In reality, indifference is another form
                of selfishness that keeps us from connecting with people. Novelist George
                Eliot put it like this: “Try to care about something in this vast world besides
                the gratification of small selfish desires. Try to care for what is best in thought
                and action—something that is good apart from the accidents of your own lot.
                Look on other lives… See what their troubles are, and how they are borne.”
                <Br />
                People truly appreciate it when you make the effort, no matter how
                small, to try to see things from their point of view. Paul writes, <Bold>“Be kindly
                affectionate to one another with brotherly love, in honour giving
                preference to one another” (Romans 12:10 NKJV)</Bold>. Everybody you meet
                is wrestling with a fear or fighting a battle and, if you're caring and sensitive,
                God will give you a word to lift them. Isaiah said, <Bold>“The Lord God has given
                me the tongue of the learned, that I should know how to speak a word in
                season to him who is weary…” (Isaiah 50:4 NKJV)</Bold>. What a gift—to be
                able to lift someone who is struggling.
            </Text>,
            prayerPoints: [
                "I shall not be indifferent to others in need, in the name of Jesus.",
                "Lord, make me to be kind to others, in the name of Jesus.",
                "Holy Spirit, teach me what to say and what not to say to others in need, in the name of Jesus.",
            ]
        },
        {
            day: 17,
            topic: "What Keeps Us from Serving Others (4)",
            wordStudy: "Galatians 5:13-16",
            confession: "But through knowledge shall the just be delivered. Therefore, I and my family will be delivered because we know You Lord (Proverbs 11:9b).",
            fullText: <Text style={styles.text}>Control is the fourth thing that deters us from serving. While it's important to
                focus on understanding others, we must also be authentic and open so they
                can understand us. Because it can make us feel vulnerable, many of us are
                unwilling to be transparent. Former U.S. Navy Captain Mike Abrashoff
                observes: “Some leaders feel that by keeping people in the dark, they
                maintain a measure of control. But that is a leader's folly and an
                organization's failure. Secrecy spawns isolation, not success. Knowledge is
                power, yes, but what leaders need is collective power, and that requires
                collective knowledge. I found that the more people knew what the goals
                were, the better buy–in I got—and the better the results we achieved
                together.” Any time people sense information is being withheld from them, it
                creates distance. They feel like outsiders, and, as a result, morale drops along
                with their performance.
                <Br />
                In <Italic>Lead, Follow, or Get Out of the Way</Italic>, Jim Lundy writes about what he
                calls the “Subordinates' Lament”: “We, the uninformed, working for the
                inaccessible, are doing the impossible for the ungrateful!” Ever feel like that?
                Then there's the “Mushroom Farm Lament” which goes like this: “We feel
                we're being kept in the dark. Every once in a while someone comes around
                and spreads manure on us. But when our heads pop up, they're chopped off
                and then we're canned.” Good leaders don't isolate themselves, and they don't
                deliberately keep people in the dark. They inform them, and include them in
                the decision–making process whenever possible. If you're serious about
                serving others, open up. Let people know who you are and what you believe.
            </Text>,
            prayerPoints: [
                "Lord, help me to always acknowledge superior opinion, in the the name of Jesus.",
                "Lord, make me to walk in the counsel of godly men, in the name of Jesus.",
                "Lord, cause me to delight in Your Word that I may gain a heart of wisdom to serve others, in the name of Jesus.",
            ]
        },
        {
            day: 18,
            topic: "Understanding Your Temptations (1)",
            wordStudy: "Mark 4:1-13",
            confession: "Father Lord, lead us not into temptation, but deliver us from every evil, in the name of Jesus (Matthew 6:13).",
            fullText: <Text style={styles.text}>Many of us suffer unnecessarily because of our misconceptions about
                temptation. The first one is: expecting your temptations to cease—or at least
                decrease. When the Israelites entered the Promised Land they had the same
                expectation. They were surprised that the Canaanites were still in the land.
                So they got discouraged, thinking they must have gone the wrong way.
                “Surely this couldn't be the land of promise.” God's Word makes it clear that
                the Christian life, from start to finish, is warfare. Paul says we are called to
                <Bold>“wrestle…against principalities, against powers, against the rulers of
                the darkness of this world, against spiritual wickedness in high places”
                (Ephesians 6:12 KJV)</Bold>. And your level of temptation increases as you draw
                closer to God!
                <Br />
                Notice that when Israel first left Egypt, God didn't lead them through
                Philistine territory, <Bold>“though that was shorter. For God said, “If they face
                war, they might change their minds and return to Egypt” (Exodus 13:17
                NIV)</Bold>. God understood their fledgling faith and protected them from their
                enemies. But later, when their faith had grown, He permitted them to face
                seven powerful nations. Just like the Promised Land, the promised life of
                victory requires you to deal with temptation and wrestle your way through to
                victory, believing that <Bold>“greater is He that is in you, than he that is in the
                world” (1 John 4:4 KJV)</Bold>. Indeed, their enemies' attacks proved that Israel
                was in the Promised Land. So experiencing temptations is proof you are truly
                abiding in Christ.
            </Text>,
            prayerPoints: [
                "Sin shall not have dominion over me, in the name of Jesus.",
                "Lord, make me to walk in the Spirit so as not to fulfil the desires of the flesh, in the name of Jesus.",
                "Lord, increase my faith, in the name of Jesus.",
            ]
        },
        {
            day: 19,
            topic: "Understanding Your Temptations (2)",
            wordStudy: "1 Timothy 3:3-6",
            confession: "Because I have chose to submit myself to God in absolute surrender, as I resist the devil, he will flee from me, in the name of Jesus (James 4:7).",
            fullText: <Text style={styles.text}>The second misconception is: “Temptation is sin.” The devil wants you to
                blame yourself for the temptations you experience. Why? Because when you
                believe that you are the source of it, you feel condemned and discouraged. As
                you indulge in feelings of discouragement, you become an easy prey and
                satan convinces you to go ahead and yield to the temptation. Ironically, you
                yield to sin through the fear of having already fallen. No, the temptation itself
                isn't sin; the sin only occurs when you yield to it! Many of us feel like we're
                spiritually deficient, distant from God, even phonies because we think we
                initiated the temptation ourselves.
                <Br />
                Imagine a burglar in your house, and when you detect him, he accuses
                you of being the burglar! Would you believe him? Of course not! Yet that's
                satan's approach with us. After he tempts us, he accuses with thoughts like,
                “A real Christian would never have such thoughts!” Knowing our own
                weakness, his accusations sound plausible and we believe him. Then we feel
                condemned, spiral into discouragement, and yield to the temptation.
                Understand this: Condemnation comes from satan <Bold>(1 Timothy 3:6)</Bold>. The
                Bible says, <Bold>“Therefore, there is now no condemnation for those who are
                in Christ Jesus” (Romans 8:1 NIV).</Bold> When satan sends temptation, the
                Holy Spirit goes to work in us, helping us to reject his suggestions and
                reminding us of Jesus' availability to help us triumph. <Bold>“Because He Himself
                suffered when He was tempted, He is able to help those who are being
                tempted” (Hebrews 2:18 NIV).</Bold> So when you're tempted, reject satan's
                condemnation and reach for Christ's overcoming assistance.
            </Text>,
            prayerPoints: [
                "I refuse to be discouraged, in the name of Jesus.",
                "I receive the grace to endure temptations, in the name of Jesus.",
                "Ii shall not accept satan's suggestions, in the name of Jesus.",
            ]
        },
        {
            day: 20,
            topic: "Understanding Your Temptations (3)",
            wordStudy: "James 1:2-4",
            confession: "I shall not be overtaken by temptation, in the name of Jesus (1 Corinthians 10:13).",
            fullText: <Text style={styles.text}>Satan knows that when you recognize him as the source of your temptation,
                he loses his advantage. He knows that when you understand he initiated the
                temptation, you'll recoil from it faster than if you think you yourself are the
                source of it. If Satan approached you and said, “I've come to make you sin and
                feel condemned,” your vulnerability to his temptations would evaporate! He
                won't tell you that—but if you remember it, you'll have the advantage over
                him. As Paul puts it, <Bold>“We are not ignorant about Satan's scheming” (2
                Corinthians 2:11 GWT).</Bold> Another misconception is that temptation is time
                lost and effort wasted. In long periods of temptation we feel like we've lost
                ground rather than gained it. No! Withstanding temptation is <Bold>“[fighting] the
                good fight of faith” (1 Timothy 6:12 NKJV),</Bold> and that's when God develops
                your character. <Bold>“When all kinds of…temptations crowd into your lives…,
                realise that they come to…produce in you the quality of endurance…”</Bold>
                and when that happens, <Bold>“…you will find you have become men [and
                women] of mature character…” (James 1:2-4 ).</Bold>
                <Br />
                Indeed, your compensation goes far beyond your present development to
                the reaping of eternal rewards. <Bold>“Be exceedingly glad on this account,
                though now for a little while you may be distressed by trials and suffer
                temptations, so that [the genuineness] of your faith may be tested, [your
                faith] which is infinitely more precious than the perishable gold, which is
                tested and purified by fire” (1 Peter 1:6-7 AMP).</Bold> Add to this God's
                glorious guarantee: <Bold>“Blessed is the man who endures temptation… he will
                receive the crown of life…” (James 1:12 NKJV).</Bold>
            </Text>,
            prayerPoints: [
                "Satan will not write the last chapter of my life, in the name of Jesus.",
                "Lord, baptize me with the spirit of faith that can move mountains, in the name of Jesus.",
                "I receive grace to overcome every obstacle on my way, in the name of Jesus.",
            ]
        },
        {
            day: 21,
            topic: "Commit to It",
            wordStudy: "1 Samuel 9:11",
            confession: "I will lift up my eyes to the hills—from whence comes my help? My help comes from the LORD, who made heaven and earth (Psalm 121:1-2).",
            fullText: <Text style={styles.text}>After arriving in the New World, the Spanish explorer, Cortez, took his crew
                off their ships, then set the ships on fire as the sailors watched in confusion
                and horror. By that one act, Cortez sealed their commitment to explore new
                territory and ensured they'd never give in or go back. Guess what? Having no
                alternative clears your perception, does away with procrastination, and helps
                you make up your mind. When you're in a corner, you learn to reach deeper
                inside yourself, and, in so doing, you discover God–given strengths you
                never knew existed. When you're up against a deadline, it releases a creative
                flow within you. For example, nothing increases your commitment like the
                threat of failure or the risk of embarrassment.
                <Br />
                In <Italic>The Tyranny of E-mail</Italic>, John Freeman says, “Attention is one of the
                most valuable modern resources. If we waste it on frivolous communication,
                we will have nothing left when we really need it.” Distractions drain your
                energy, blur your focus, and disrupt your momentum. Projects fail,
                companies collapse, and marriages are damaged because of the way
                distractions deplete our resources and destroy our relationships. Don't get to
                the end of your life only to look back and discover you forfeited things which
                were truly significant, to make way for things that added nothing of value.
                Hannah made a vow to God that resulted in the birth of her son Samuel, who
                became one of Israel's greatest Judges. Be a person who honours your
                commitments.
            </Text>,
            prayerPoints: [
                "Lord, make me a man/woman of integrity for Your name's sake..",
                "I shall not forsake Christ, in the name of Jesus.",
                "I shall make Heaven, in the name of Jesus.",
            ]
        },
        {
            day:22,
            topic: "Get Your Spending Right",
            wordStudy: "Romans 13:8-10",
            confession: "Lord, instruct me, teach me and guide me (Psalm 32:8).",
            fullText: <Text style={styles.text}>If you think winning the lottery would solve all your problems, you couldn't
                be more wrong! Almost half of those who become overnight millionaires end
                up bankrupt within a few years. Why? For a variety of reasons: bad business
                deals, extravagant living, crazy schemes, fast–talking relatives, etc.. And
                here's another one—a big one! Believing that they now have the Midas touch
                and that Lady Luck is on their side, they continue to gamble at an even higher
                level—and end up losing it all.
                <Br />
                Try to understand this: unless you spend less than you earn, no amount of
                income will ever be enough! So if you're wise, when you get a salary increase
                or an unexpected windfall you won't adopt a more extravagant lifestyle.
                You'll reduce your debt before it becomes an albatross around your neck that
                drowns you. No amount of income will be sufficient if your spending is not
                brought under control. The only way to get ahead financially is to deny
                yourself some of the things you want and save for the future. If you don't have
                the discipline to do that, you'll always be in debt. Now, when the government
                gets into debt, they simply tax you. But when you get into debt—you're on
                your own. And that's not how God wants you to live.
                <Br />
                True contentment doesn't come from getting all you want, but
                discovering the blessings you have been given and learning to enjoy them.
                This should be your goal: <Bold>“Owe no one anything—except for your
                obligation to love one another” (Romans 13:8 NLT).</Bold>
            </Text>,
            prayerPoints: [
                "Lord, make me prudent in every way, the name of Jesus.",
                "I will not trust in uncertain riches, in the name of Jesus.",
                "I will trust in the Lord with all my heart, in the name of Jesus name.",
            ]
        },
        {
            day: 23,
            topic: "Today, Show that You Care",
            wordStudy: "John 15:12-14",
            confession: "The Spirit of God has made me, and the breath of the Almighty gives me life (Job 33:4).",
            fullText: <Text style={styles.text}>Take a deep breath, and hold it for a moment. Now exhale it. Where did it go?
                The air that you just exhaled will circle the globe in the next twelve months,
                during which time each of the molecules you exhaled will be breathed in by
                someone else only to be exhaled again. In this way we are all linked to each
                other, we are all connected by our breath to each other and to the One who
                first breathed life into us. Just as we all share our chemistry with other
                members of the human family, so we are all interdependent. We're affected
                positively or negatively by the actions of each other.
                <Br />
                During the self-centered days of what used to be called the “Me
                Generation,” it was common to hear people say, “As long as I'm not hurting
                anyone, it's nobody's business what I do.” Unfortunately, everything we do
                affects other people, and there's no such thing as a completely independent
                act. The poet John Donne wrote, “No man is an island, entire of itself, every
                man is a piece of a continent, a piece of the main.” With that in mind, read
                these two Scriptures:
                <Br />
                <Bold>A new commandment I give you, that you love one another; as I
                have loved you (John 15:12 NKJV).
                Owe nothing to anyone—except for your obligation to love one
                another (Romans 13:8 NLT).</Bold>
                <Br />
                That means you have an obligation today to show your care for others in
                some practical way.
                <Br single/>
                Today is Pastor (Mrs) Bukola Oladiyun's, birthday. Please remember to
                lift her up in your prayers.
            </Text>,
            prayerPoints: [
                "Thank You, Lord, for Your handmaiden, Pastor Mrs Bukola Oladiyun. Let Your grace abound more and more in her life, in the name of Jesus.",
                "Lord, make me to be sensitive to the plight of others, in the name of Jesus.",
                "I refuse to live in an island called “Self,” in the name of Jesus.",
                "I receive grace to love others unconditionally as I am loved of the Lord, in the name of Jesus.",
            ]
        },
        {
            day: 24,
            topic: "To Lead, You Must Keep Learning",
            wordStudy: "2 Peter 1:5-10",
            confession: "Through the knowledge of God, I shall be delivered of all that does not glorify Him, in the name of Jesus (Proverbs 11:9b).",
            fullText: <Text style={styles.text}>As a leader you should stand on this Scripture: <Bold>“[God's] divine power has
                given us everything we need for a godly life …He has given us His very
                great and precious promises, so that through them you may participate
                in the divine nature… For this… reason, make every effort to add to
                your faith goodness; and to goodness, knowledge; and to knowledge,
                self–control; and to self–control, perseverance; and to perseverance,
                godliness; and to godliness, brotherly kindness; and to brotherly
                kindness, love. For if you possess these qualities in increasing measure,
                they will keep you from being ineffective and unproductive” (2 Peter
                1:3-8 NIV).</Bold>
                <Br />
                Note the word add. Whether you're a leader at home, the workplace, or
                the church, you must keep learning and growing. When you continually
                invest in yourself, over time the inevitable result is growth. Although it's true
                that some people are born with greater natural gifts than others, the ability to
                lead is really a collection of skills, nearly all of which can be learned and
                improved.
                <Br />
                But the process doesn't happen overnight. Leadership is complicated. It
                has many different facets such as: respect, experience, emotional strength,
                people skills, discipline, vision, momentum, timing—the list goes on and on.
                That's why leaders require so much seasoning to be effective. The truth is, in
                order to keep leading, you must keep learning and the learning process is
                ongoing, resulting from self–discipline and perseverance. Your goal must be
                to get a little better each day, to build on the previous day's success, and to
                learn from its failure. Solomon put it this way: <Bold>“A wise man will hear and
                increase learning” (Proverbs 1:5 NKJV).</Bold>
            </Text>,
            prayerPoints: [
                "Lord make me hunger and thirst after Your Word, in the name of Jesus.",
                "I receive grace to lead others by example, in the name of Jesus.",
                "Lord, baptize me with the spirit of humility, in the name of Jesus.",
            ]
        },
        {
            day: 25,
            topic: "You Can Break the Cycle (1)",
            wordStudy: "John 9:2-3",
            confession: "I will trust in the Lord with all my heart and lean not on my own understanding. I will acknowledge Him in all my ways and He will direct my paths (Proverbs 3:5-6).",
            fullText: <Text style={styles.text}><Bold>“His disciples asked Him, saying, 'Rabbi, who sinned, this man or his
                parents, that he was born blind?'” “Jesus answered, 'Neither this man
                nor his parents sinned, but that the works of God should be revealed in
                him'” (John 9:2-3 NKJV).</Bold> When someone is trapped in a self–destructive
                habit, we sometimes say it's genetic: “It runs in the family.” Yet psychiatrists
                tell us 90% of those who abuse others were first abused themselves; they are
                simply “acting out” what was done to them. Some say if alcoholism and
                addiction run in your family, you've got an “addictive gene,” meaning you're
                at much greater risk if you pick up a drink or a drug. But you'll notice Jesus
                didn't get into the whole “nurture vs nature” debate. In essence, He said,
                “This is just an opportunity for God to demonstrate His love and grace by
                setting this man free.”
                <Br />
                So your problem may be too big for you, but it's not too big for God. It's
                actually a platform for Him to demonstrate His grace in your life. <Bold>“Therefore
                if any person is [ingrafted] in Christ (the Messiah) he is a new creation (a
                new creature altogether); the old [previous moral and spiritual
                condition] has passed away. Behold, the fresh and new has come!” (2
                Corinthians 5:17 AMP).</Bold> God doesn't consult your past in order to determine
                your future. Before you met Jesus, you were spiritually dead. That's why He
                said, <Bold>“You must be born from above…” (John 3:3 CEV).</Bold> From that point
                forward, the past no longer has any power over you—except the power you
                give it. So you can break the cycle.
            </Text>,
            prayerPoints: [
                "I disconnect myself from every inherited evil family pattern, in the name of Jesus.",
                "My life must and shall glorify God, in the name of Jesus.",
                "I reject every ungodly addiction in my life, in the name of Jesus.",
            ]
        },
        {
            day: 26,
            topic: "You Can Break the Cycle (2)",
            wordStudy: "John 9:6-7",
            confession: "Make haste, O God, to deliver me; make haste to help me, O Lord, in the name of Jesus (Psalm 70:1).",
            fullText: <Text style={styles.text}>Dr. Samuel Rodriguez, president of the National Hispanic Christian
                Leadership Conference, points out that your DNA is in your saliva and that
                when Jesus spat, this blind man received a divine DNA transfer. The fact of
                the matter is that sin is in our DNA and that's why Jesus goes back to the root
                of our problem! Understand this: God doesn't merely treat your symptoms;
                He goes back to the source of your problem in order to set you free from it. To
                change the fruit, He changes the root. This man never had the ability to see;
                he was born blind and needed a creative miracle—and that's exactly what he
                got: physical miracle and spiritual miracle, a total transformation. Our God
                is awesome, isn't He?
                <Br />
                Do you know why Jesus came? <Bold>“To bind up the brokenhearted, to
                proclaim freedom for the captives and release from darkness for the
                prisoners, to proclaim the year of the Lord's favour…to comfort all who
                mourn…to bestow on them a crown of beauty instead of ashes, the oil of
                joy instead of mourning, and a garment of praise instead of a spirit of
                despair…” (Isaiah 61:1-3 NIV).</Bold> It doesn't matter whether your parents
                contributed to your problem or you simply made bad choices in life, Jesus
                can set you free, make you whole, and give you a new life. Jesus didn't blame
                this man or his parents. That's because God has no desire for your
                condemnation; His desire is for your transformation.
            </Text>,
            prayerPoints: [
                "I receive divine encounter for all-round transformation, in the name of Jesus.",
                "I receive the uncommon testimony that will bring others to Christ, in the name of Jesus.",
                "I call forth my miracle to fulfil destiny and purpose on Earth today, in the name of Jesus.",
            ]
        },
        {
            day: 27,
            topic: "You Can Break the Cycle (3)",
            wordStudy: "John 9:24-25",
            confession: "The Lord will bring me out of the horrible pit, out of the miry clay, and He shall set my feet upon a rock and He will establish my goings, in the name of Jesus (Psalm 40:2).",
            fullText: <Text style={styles.text}>When Jesus restored this blind man's sight, the Pharisees tried to engage him
                in a theological debate about whom Jesus was and where His power came
                from, but he refused to argue with them. He simply said, <Bold>“One thing I know:
                that though I was blind, now I see.”</Bold> And the same arguments still continue
                today. Some people say, “The day of miracles is past.” The truth is that there is
                no day of miracles; there's only a God of miracles Who says, <Bold>“I am the Lord,
                I do not change” (Malachi 3:6 NKJV).</Bold> The Bible says, <Bold>“Jesus Christ the
                same yesterday, and today, and forever” (Hebrews 13:8 KJV).</Bold> That
                means what He was, He still is. What He did, He still does. What He said, He
                still says.
                <Br />
                Whatever your problem may be, you have two choices: live with it and
                try to adjust to it, or believe God to set you free from it. Certain things like
                alcoholism, addiction, and abuse may run in your family, but by the grace of
                God you can break the cycle! Please hear this: God's power is greater than
                your habit! Not only can He set you free from the thing that holds you captive,
                He can deliver you from its lingering effects. His Word to you is: <Bold>“Forget the
                former things; do not dwell on the past. See, I am doing a new thing! Now
                it springs up; do you not perceive it? I am making a way…” (Isaiah
                43:18-19 NIV).</Bold>
                <Br />
                So turn to God today. Give Him a chance. You've tried other things, now
                try Him. He will not fail you!
            </Text>,
            prayerPoints: [
                "I refuse to be spiritually and physically blind, in the name of Jesus name",
                "I receive the grace to forget every wrong done to me in the past, in the name of Jesus.",
                "I refuse the company of scorners, in the name of Jesus.",
            ]
        },
        {
            day: 28,
            topic: "You Can Break the Cycle (4)",
            wordStudy: "John 9:7-9",
            confession: "I will not give up on God, in the name of Jesus (Galatians 6:9).",
            fullText: <Text style={styles.text}>Sometimes Jesus healed sick people by speaking to them, other times He laid
                His hands on them. But this blind man's healing was not the work of a
                moment; it was the result of a process. <Bold>“He said to him, 'Go, wash in the pool
                of Siloam' …So he went and washed, and came back seeing” (John 9:7
                NKJV).</Bold> Question: How far away was the pool of Siloam? For a blind man,
                any journey is a long one. Picture this man with mud in his eyes, trying to
                navigate his way to the pool of Siloam. There's an important lesson here. Even
                when God touches us, sometimes we've got to walk with our mess until we get
                to the place of healing, freedom and deliverance.
                <Br />
                Here's another thought. Whoever told this man about Jesus, and brought
                him to Jesus, had to keep walking with him until he got his sight back and
                could walk on his own. You can't say to someone who's struggling with a
                problem, “I told you about Jesus, and I even brought you to church but you're
                still stumbling around in darkness bound by the same problems, so I give up.”
                <Br />
                God never gives up on people, and we must not give up on them either.
                When someone continues to struggle with an old habit, some Christians say
                they mustn't really be saved. Question: How often do you give in to
                resentment, pride, lust, greed, gossip, etc., etc.? The list is endless, yet God
                doesn't give up on you. Sometimes people have to “walk out” their
                deliverance and healing until it fully comes to pass. That's how long you need
                to love them, believe in them, pray for them, and keep working with them.
            </Text>,
            prayerPoints: [
                "Lord connect me with my destiny helpers, in the name of Jesus.",
                "I will not give up on God, in the name of Jesus.",
                "Lord, teach me to wait on You, in the name of Jesus.",
            ]
        },
        {
            day: 29,
            topic: "You Can Break the Cycle (5)",
            wordStudy: "John 9:11-14",
            confession: "I will behold wondrous things out of the Word of God, in the name of Jesus (Psalm 119:18).",
            fullText: <Text style={styles.text}>Jesus told this blind man to go to the pool of Siloam and wash the dirt from
                his eyes. So here's the question: What do you need to remove from your life
                today in order to see what God wants you to see and do what He wants you to
                do? Whether it's a destructive habit, a toxic relationship, or a bad
                attitude—you must get rid of it. James writes: <Bold>“Therefore submit to God.
                Resist the devil and he will flee from you. Draw near to God and He will
                draw near to you. Cleanse your hands, you sinners; and purify your
                hearts, you double–minded” (James 4:7-8 NKJV).</Bold> God will do His part,
                but you must do yours. God will extend grace to you, but you must act on it.
                <Br />
                It's said that the water which flowed into the pool of Siloam came from
                under the temple and was used to wash the utensils which were part of
                worship and sacrifice. In other words, this water didn't come from a
                man–made source, but from the presence of God. Don't miss the point here!
                Jesus said, <Bold>“You are already clean because of the word which I have
                spoken to you” (John 15:3 NKJV).</Bold> Paul speaks of <Bold>“the washing of water
                by the word” (Ephesians 5:26 KJV).</Bold> David writes, <Bold>“How can a young
                man [or an older one] cleanse his way? By taking heed according to Your
                Word” (Psalm 119:9 NKJV).</Bold> The secret of victorious Christian living is
                bathing your mind each day in the Scriptures. Picture yourself taking a bath
                in God's Word—and the dirt and grime coming off you. That's what
                meditating on the Scriptures does for you.
            </Text>,
            prayerPoints: [
                "My life must cooperate with the will of God, in the name of Jesus.",
                "I receive grace to be obedient to God's Word, in the name of Jesus.",
                "I must manifest God's glory, in the name of Jesus.",
            ]
        },
        {
            day: 30,
            topic: "What Are Your Children Watching?",
            wordStudy: "Psalm 101:2-3",
            confession: "By the grace of God in Christ, I will set my mind on things above, not on things on the earth (Colossians 3:2).",
            fullText: <Text style={styles.text}>Today our children have more access to the Internet than ever before. Over
                half of all children between the ages of five and seventeen have Internet
                cable or satellite television access in their bedrooms. The old image of the
                family gathered around a single TV set in the living room is almost
                non–existent. Instead, often children are off by themselves where they can
                choose anything they want to see. They don't even have to be in their
                bedroom because now they can access it all on a hand–held device like a
                tablet or an iPhone! One prominent social researcher put it this way:
                “Almost everything children are seeing is essentially going into their minds
                in some sort of uncensored or unfiltered way.”
                <Br />
                Under the Freedom of Expression rule, the government and courts
                permit Internet sites to peddle explicit sex, violence, nudity and profanity to
                your children and your grandchildren. Are you concerned? You should be!
                Why do sheep need a shepherd? For the same reason children need loving
                and observant parents! And your ignorance of technology is no defense!
                Because you weren't exposed to these evils growing up doesn't guarantee
                that your children won't get hooked on them. Many parents don't even take
                the time to know what their children are watching! Don't be one of them.
                <Br />
                After his disastrous affair with Bathsheba and the heartache and havoc it
                brought to his family, David said, <Bold>“…I will walk within my house with a
                perfect heart. I will set nothing wicked before my eyes…” (Psalm 101:2-
                3 NKJV).</Bold>
                <Br single/>
                Parents, what are your children watching?
            </Text>,
            prayerPoints: [
                "I will not fail in my responsibilities, in the name of Jesus.",
                "I refuse to drift away from God, in the name of Jesus.",
                "For me and my household, I receive the grace to serve the Lord at all times, in the name of Jesus.",
            ]
        },
    ],
    May: [
        {
            day: 1,
            topic: "How to Overcome Doubt",
            wordStudy: "Matthew 21:21-22",
            confession: "I will keep my heart with all diligence, for out of it are the issues of life (Proverbs 4:23).",
            fullText: <View> 
                <Text style={styles.text}>
                    Doubt is a doorway through which satan enters your life. It causes the <Bold>“fight of
                    faith” (I Timothy 6:12)</Bold> to become the “flight of faith.” When fear, confusion,
                    discouragement and despair take up residence within you, they rob you of
                    confidence, joy and peace. But isn't doubting just human? Of course it is and
                    it's also satan's ploy! <Bold>“Whoever does not believe God has made Him out to
                    be a liar, because they have not believed the testimony God has given…”
                    (1 John 5:10 NIV).</Bold> Your doubts reveal a lack of confidence in what God says.
                    <Br />
                    So how can you stop doubting? The same way you deal with any other
                    sin—by acknowledging you have a problem and doing something about it.
                    Doubt cannot be conquered by reason or even resistance; it will only submit to
                    complete relinquishment. Trying to overcome your doubts one by one is like
                    an alcoholic trying to reduce his or her alcohol intake one drink at a time. It
                    won't work.
                    <Br single/>
                    The solution is two-fold.
                </Text>

                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            It begins with a once–for–all decision. It calls for a total relinquishing of
                            your right to doubt anything God promised. It's saying, “From now on, I
                            will not doubt God!” It's believing that when you surrender a thing to
                            Him, He takes it and deals with it.
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            By faith declare, “Lord, I absolutely trust Your every Word!” Will doubt
                            come knocking again at your door? Yes, but instead of letting it in, <Bold>“take
                            up the shield of faith, with which you can extinguish all the flaming
                            arrows of the evil one” (Ephesians 6:16 NIV).</Bold> Don't fret, wrestle, or
                            reason with your doubts. Instead, repeat your Scriptural declaration of
                            faith, disregard your feelings, and trust God to do what He says.
                        </Text>
                    },
                ]} />
            </View>,
            prayerPoints: [
                "Lord, increase my faith, in the name of Jesus.",
                "I bind the spirit of doubt and unbelief and paralyse its activities in my life, in the name of Jesus.",
                "I receive grace to resist the devil, in the name of Jesus.",
            ]
        },
        {
            day: 2,
            topic: "The Huddle",
            wordStudy: "1 Corinthians 1:10-13",
            confession: "I will always come boldly, in the name of Jesus, to the throne of grace that I may obtain mercy and find grace to help in time of need (Hebrews 4:16).",
            fullText: <View> 
                <Text style={styles.text}>
                    To lead your family effectively, you need to learn how to huddle regularly.
                    The “huddle” is where a team:
                </Text>

                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            Sets its goals.
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            Discusses the division of responsibilities; and
                        </Text>
                    },
                    {
                        serial: 3,
                        body: <Text>
                            Tackles the issues that determine whether it wins or loses.
                        </Text>
                    },
                ]} />

                <Text style={styles.text}>
                    Parent, even though you call the plays from overhead, your family must
                    be taught how to accomplish them on the field. That means working through
                    things, talking through the disagreements, motivating and appreciating each
                    member. Try to listen with an open heart. Don't just hear what your children
                    say, try to understand how they feel. Yes, you're the boss, and yes, you can try
                    to enforce your will if you want to. But sooner or later you'll have trouble, for
                    resentment grows when people feel left out.
                    <Br />
                    Every member of your team has got to be part of the decision–making
                    process. Involve them! Ask God to help you look beyond what you want to
                    what's best for all of you. And don't fall under the spell of instant gratification.
                    What looks good to you today could be taking you off the path to a better
                    tomorrow. And don't let “outsiders” into your huddle. Tell them to stay in
                    their own. Too often their opinions are based on hearsay, self–interest or
                    jealousy. Respect the privacy of your team. Build loyalty. Huddle regularly in
                    prayer. When you do that, everybody wins!
                </Text>
            </View>,
            prayerPoints: [
                "Lord, give me the wisdom to lead my family right, in the name of Jesus.",
                "Lord, help me to understand how my family members feel, so I can lead them aright, in the name of Jesus.",
                "Anointing of help from above, fall on me and my family, in the name of Jesus.",
            ]
        },
        {
            day: 3,
            topic: "Handling Your Failures (1)",
            wordStudy: "Joshua 7:5-12",
            confession: "For I will declare my iniquity; I will be sorry for my sin (Psalm 38:18).",
            fullText: <Text style={styles.text}>Even the most seasoned believers experience failure and the big question at
                such times isn't, “How could I have let that happen?” but, “What's the right
                way to deal with it?” Indulging in self-condemning thoughts comes naturally
                to us, but it does no good. It just produces discouragement and adds failure
                upon failure. As a result of Achan's sin, Israel was defeated at Ai and fled
                before their enemies. Like us, in response to their defeat, <Bold>“the hearts of the
                people melted, and became like water.”</Bold> Their faith forsook them, and they
                <Bold>“lost their courage” (Joshua 7:5 GNT).</Bold> Even their leader Joshua <Bold>“fell
                facedown to the ground” (Joshua 7:6 NIV),</Bold> despairing that things could
                only get worse! Have you been there? Do you recognise the pattern? Defeat,
                discouragement, despair, and more defeat. What was God's response? <Bold>“Get
                up! Why do you lie thus…? Israel has sinned… Get up, sanctify the
                people…” (Joshua 7:10-13 NKJV).</Bold> He's a God of repentance, not
                wallowing in remorse; a God of getting up, not lying down in failure!
                <Br />
                Holiness is a path, not a place. When you're off the path, God's plan is
                simple: get back on it immediately. Confess your sin <Br />(1 John 1:9). By faith
                accept God's mercy and forgiveness. And refuse satan's condemnation
                <Bold>(Romans 8:1). “…Forgetting those things which are behind…press
                toward the goal” (Philippians 3:13-14 NKJV).</Bold> Like a child learning to
                walk, when you fall, don't lie down and give up. Minimise your down time,
                get back up and walk again. Remember this: the moment that brings the
                awareness of sin should also bring the confession of sin and the assurance of
                forgiveness!
            </Text>,
            prayerPoints: [
                "I receive courage to move on from failure and discouragement, in the name of Jesus.",
                "I will not hide my sin, in the name of Jesus.",
                "I shall make it to the end, in the name of Jesus.",
            ]
        },
        {
            day: 4,
            topic: "Handling Your Failures (2)",
            wordStudy: "Joshua 7:13-20",
            confession: "Search me, O God, and know my heart… and see if there is any wicked way in me (Psalm 139:23-24).",
            fullText: <Text style={styles.text}>What's the root of our failures? Failure doesn't lie in the strength of the
                enemy, or our own weakness, or God's reluctance to help us overcome. Israel
                was defeated by a small town after having conquered mighty Jericho. Their
                failure, like ours, was the result of unconfessed sin <Bold>(Joshua 7).</Bold> But once their
                sin was brought to light and dealt with, God spoke encouragement to them.
                <Bold>“Do not be afraid…Take all the people of war with you… I have given
                into your hand the king of Ai, his people, his city, and his land” (Joshua
                8:1 NKJV).</Bold> Sin unacknowledged will keep you defeated, but sin confessed
                will bring you forgiveness and restoration that leads to victory.
                <Br />
                Author Hannah Whitall Smith tells of moving into a new home. She
                noticed in the cellar a very clean looking cider cask sealed at both ends. She
                debated whether to open it and see what was inside, but decided to leave it
                undisturbed. Each spring and autumn when the cleaning was being done,
                she'd remember the cask but put off opening it. Unaccountably, moths began
                to fill the house. She used every means she knew to get rid of them—without
                success. At last, remembering the cask, she opened it and thousands of moths
                poured out! The lesson here is simple: anything we cling to that's contrary to
                God's Word will cause us to fall before our enemies.
                <Br />
                Always keep before you David's prayer: <Bold>“Search me, O God, and know
                my heart…see if there be any wicked way in me…” (Psalm 139:23-24
                NKJV).</Bold> Whatever God shows you, confess it immediately and, by faith,
                receive His forgiveness and cleansing. Then you'll be able to stand victorious
                before your enemies.
            </Text>,
            prayerPoints: [
                "Create in me a clean heart, O Lord, in the name of Jesus.",
                "Sin shall not have dominion over me, in the name of Jesus.",
                "My prayer altar, receive the fresh fire of God, in the name of Jesus.",
            ]
        },
        {
            day: 5,
            topic: "On the Home Front",
            wordStudy: "Matthew 7:21",
            confession: "But as for me and my house, we shall serve the Lord (Joshua 24:15).",
            fullText: <View> 
                <Text style={styles.text}>
                    Being the high priest in Israel was a prestigious job, and Eli seemed suited to
                    it. Yet he failed badly on the home front. <Bold>“His sons did not follow his ways.
                    They turned aside after dishonest gain and accepted bribes and
                    perverted justice” (1 Samuel 8:3 NIV).</Bold> As a result, God told Eli He <Bold>“would
                    judge his family forever because of the sin he knew about; his sons
                    blasphemed against God, and he failed to restrain them” (1 Samuel 3:13
                    NIV).</Bold> As a parent and leader you need to answer this question: If God applied
                    the same standard to you as He did to Eli, how well would you do? Eli ended
                    up losing his credibility, his children, his career, and eventually his life. He
                    missed the mark because he made some critical errors.
                    <Br single/>
                    Let's look at them and see what we can learn:
                </Text>

                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            He had the wrong emphasis. He emphasised the Word of God to the
                            people in his congregation—but not to his own children.
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            He had the wrong expectations. He thought his children would “get it”
                            because they lived under his roof and worked in the church. But it doesn't
                            work that way.
                        </Text>
                    },
                    {
                        serial: 3,
                        body: <Text>
                            He set the wrong example. Eli failed to live at home what he taught at
                            work. He had 20/20 vision when it came to his profession, but where his
                            family was concerned he was blind to his own weakness.
                        </Text>
                    },
                ]} />

                <Text style={styles.text}>
                    Leadership on the home front isn't about power and control; it's about
                    “giving yourself” to the people who should matter most <Bold>(Ephesians 5:25).</Bold>
                    So, how are you doing on the home front?
                </Text>
            </View>,
            prayerPoints: [
                "I will not lower the standard of God in my life and family, in the name of Jesus.",
                "I reject failure in my life, home and ministry, in the name of Jesus.",
                "I and my family shall finish well, in the name of Jesus.",
            ]
        },
        {
            day: 6,
            topic: "Staying When You Feel Like Leaving (1)",
            wordStudy: "Mark 10:7-9",
            confession: "My home will endure, in the name of Jesus.",
            fullText: <Text style={styles.text}>Not every relationship can be saved. When physical, mental or emotional
                abuse threatens your child's safety, or your own, you may be forced to leave.
                Failing to do so could lead to tragedy, but where a workable resolution can be
                found, a troubled relationship can become a source of shared joy and
                fulfillment. Here are some keys to make staying worthwhile:
                <Br />
                Adopt God's perspective on sin—yours and your spouse's. One of the
                major problems is the way we classify sin—especially our spouse's. You're
                understandably overwrought and anxious because they're incorrigible and
                selfish. They're the wilful sinner—you're the offended saint. They need a
                major overhaul, and you're responsible to see they get it. Things like being
                critical, nagging and controlling seem like small things compared to a spouse
                who swears, drinks and visits porn sites. From God's perspective, sin is
                sin—yours and theirs! It's all harmful to relationships.
                <Br />
                Stop “classifying” sin and try to discover the relationship–transforming
                power of handling the situation the way Jesus taught. <Bold>“How can you say to
                your brother, 'Let me take the speck out of your eye,' when all the time
                there is a plank in your own eye? You hypocrite, first take the plank out
                of your own eye, and then you will see clearly to remove the speck from
                your brother's eye” (Matthew 7:4-5 NIV).</Bold> You'll be amazed at how God
                will cause your spouse to acknowledge and deal with “their” problem when
                you get honest and deal with “yours”!
            </Text>,
            prayerPoints: [
                "I receive grace to declare my sins, in the name of Jesus.",
                "Lord, help me to examine myself first before I do so to others, in the name of Jesus .",
                "Lord, make me a peacemaker according to your Word (Matthew 5:9).",
            ]
        },
        {
            day: 7,
            topic: "Staying When You Feel Like Leaving (2)",
            wordStudy: "Ephesians 5:21-25",
            confession: "Satan shall not take advantage of me and my family, in the name of Jesus (2 Corinthians 2:11).",
            fullText: <Text style={styles.text}>Recognize who the real enemy of your marriage is. On those days when you
                think, “I can't spend another moment in this relationship,” it's easy to lose
                perspective and focus on the wrong things. Marriage was God's idea. He
                planned it as the foundation of His earthly Kingdom. <Italic>That makes marriage one
                of satan's prime targets.</Italic> It's why he poisoned the perspective and confused the
                thinking of the first couple. He deceived Adam into believing that Eve was his
                problem, blaming the fiasco on her <Bold>(Genesis 3:12).</Bold> But both of them were
                deceived by <Bold>“the father of lies” (John 8:44 NLT).</Bold> Satan knew he could
                undermine God's plan by driving a wedge between the first couple, creating
                antagonism, blame and self–interest, and his methods haven't changed. It's
                why we “keep tabs” on each other's shortcomings, identifying our mate as the
                problem and refusing to show grace.
                <Br />
                Paul helps us understand how to overcome satan's strategy. <Bold>“I have
                forgiven in the sight of Christ…in order that Satan might not outwit
                us…we are not unaware of his schemes” (2 Corinthians 2:10-11 NIV).</Bold>
                Then he counsels us further by saying that <Bold>“love… keeps no record of
                wrongs” (1 Corinthians 13:4-5 NIV).</Bold> That doesn't mean love lives in denial,
                but that it chooses to practice self-denial! So rather than keeping score of your
                spouse's worst qualities, choose to look for their best ones and show your
                appreciation. Nothing melts resentment and hardness like expressing
                appreciation for each other.
            </Text>,
            prayerPoints: [
                "I shall not be manipulated into marriage, in the name of Jesus.",
                "My marriage shall stand the test of time, in the name of Jesus.",
                "The devil shall not write the last chapter of my life, work and marriage, in the name of Jesus.",
            ]
        },
        {
            day: 8,
            topic: "Staying When You Feel Like Leaving (3)",
            wordStudy: "Colossians 3:12-14",
            confession: "Your will shall be done in my life, O Lord, and Your glory shall be seen (Matthew 6:9-10).",
            fullText: <View> 
                <Text style={styles.text}>
                    Here are two helpful keys to resolving marriage conflict:
                </Text>

                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            Let God direct your prayers. Prayer can be closed-ended or open–ended.
                            When we think that our perspective is the only accurate one, we pray
                            closed-ended prayers calling on God to solve the problem our way,
                            believing it's the only correct way. However, closed-ended praying
                            produces two problems. First, it locks us into rigid thinking and blinds
                            us to other perspectives. Secondly, it keeps us from seeing God's
                            perspective, the one that can heal and restore the relationship. Openended
                            praying asks God to solve the problem His way. <Bold>“Pray then like
                            this…Your will be done…” (Matthew 6:9-10).</Bold> Ask God to reveal His
                            will to you both, wait until He does, then pray accordingly. The Bible
                            says: <Bold>“This is the confidence we have in approaching God: that if we
                            ask anything according to His will, He hears us. And if we know that
                            He hears us…we know that we have what we asked of Him” (1 John
                            5:14-15 NIV).</Bold>
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            Remove the conditions from your love. Does this sound difficult? Love
                            is a biblical command, not an arbitrary emotion. God's not asking you to
                            feel warm and fuzzy; He's asking you to act in a loving way. Wouldn't
                            that be hypocritical? No, it's rising above resentment, hurt, and fear, and
                            practicing real faith. It means asking yourself: “If I were loving
                            unconditionally right now, what would I be doing? How would I be
                            responding to my spouse?” Then do it.
                        </Text>
                    },
                ]} />
                <Text style={styles.text}>
                    The Bible says, <Bold>“Love never fails” (1 Corinthians 13:8 NIV).</Bold> You can
                    lovingly act your way into a new way of feeling for both you and your spouse.
                </Text>
            </View>,
            prayerPoints: [

            ]
        },
        {
            day: 9,
            topic: "Staying When You Feel Like Leaving (4)",
            wordStudy: "Proverbs 3:5-7",
            confession: "I choose to cast all my care on the Lord for He cares for me (1 Peter 5:7).",
            fullText: <View> 
                <Text style={styles.text}>
                    Give your marriage to God. The last word on the matter must be God's Word!
                    Seeking professional help is a good thing; but until you've transferred
                    ownership of your marriage into God's hands, you haven't exercised your best
                    option. You say, “What does handing my marriage over to God mean in
                    practical terms?” It means two things:
                </Text>

                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            You stop calling the shots—that's God's job and you must get out of His
                            way so that He can do His work unhindered. Your self–interest and need
                            to control must bow to His will. As long as you insist on “being right” and
                            “straightening out” your spouse, you will remain part of the problem. On
                            the other hand, when you give the problem to God, He—not you—has a
                            problem to work on!
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            You learn how to <Bold>“walk by faith, not by sight” (2 Corinthians 5:7
                            NKJV).</Bold> When things feel out of control, you will want to resume
                            ownership of the problem. Don't do it, or the result will be more of what
                            doesn't work. Renew your decision to allow God to have control and
                            work in both of your hearts. Walk by faith, not by feelings. The psalmist
                            puts it this way: <Bold>“Leave your troubles with the Lord, and He will
                            defend you…” (Psalm 55:22 GNT).</Bold>
                        </Text>
                    },
                ]} />

                <Text style={styles.text}>
                    When you trust God to handle it, three things happen:
                </Text>

                <IndentList items={[

                    {
                        serial: 1,
                        body: <Text>
                            You experience peace.
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            Your spouse's resistance is likely to diminish because you're no longer
                            stirring the pot; and
                        </Text>
                    },
                    {
                        serial: 3,
                        body: <Text>
                            God goes to work: <Bold>“He who began a good work in you [both] will
                            bring it to completion” (Philippians 1:6 ESV).</Bold>
                        </Text>
                    },
                ]} />
            </View>,
            prayerPoints: [
                "Help me, Lord, to walk by faith only and not by feelings, in the name of Jesus.",
                "I bow myself, my interests and needs to the control of Your will, O Lord, in the name of Jesus.",
                "I cast all my cares on You, Lord, help me!",
            ]
        },
        {
            day: 10,
            topic: "Failure and Success",
            wordStudy: "Joshua 1:8",
            confession: "The Lord will give me my every expected end, in the name of Jesus (Jeremiah 29:11b).",
            fullText: <View> 
                <Text style={styles.text}>
                    To turn your failures into successes you need to do two things:
                </Text>

                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            Realize that God wants you to succeed because you are His child. When
                            you think about it, what good and loving parent wouldn't? In the face of
                            overwhelming obstacles and enemy threats, Nehemiah announced,
                            <Bold>“The God of Heaven will give us success” and God did!</Bold>
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            Realize that failure is a teaching tool. The greater the failure, the greater
                            the opportunity to learn from it. But first you must acknowledge the
                            teaching potential in your mistakes and commit to learning, growing
                            and changing as a result.
                        </Text>
                    },
                ]} />

                <Text style={styles.text}>
                    Thomas Edison said, “I'm not discouraged, because every wrong
                    attempt discarded is another step forward.” At 21 years old, he set up his
                    laboratory in Menlo Park, New Jersey, and became a full–time inventor. At
                    any given time, he and his team were working on as many as 40 different
                    projects and they applied for more than 400 patents a year. Edison's feverish
                    work schedule and productivity caused the local citizens to dub him “The
                    Wizard of Menlo Park.” But he wasn't always successful. The fact is, he
                    struggled with many of his inventions but, despite embarrassing failure after
                    embarrassing failure, he refused to give up. Often ridiculed for his
                    perseverance, he engaged in some 10,000 experiments before finally
                    inventing the incandescent light bulb in 1879. Concerning his checkered
                    work history, he said, “I haven't failed. I've found 10,000 ways that don't
                    work.”
                    <Br />
                    Stop labelling your failures as negative. There are very few real failures
                    in life—only options. Some options work, others don't. The truth is, with
                    God on your side you'll win if you persist!
                </Text>
            </View>,
            prayerPoints: [
                "Anointing for success, fall on me, in the name of Jesus.",
                "I reject failure as a lifestyle, in the name of Jesus.",
                "I refuse to give up in the journey of life, in the name of Jesus.",
            ]
        },
        {
            day: 11,
            topic: "The Outcome Is Up to You",
            wordStudy: "Romans 12:1-2",
            confession: "I will guard my heart with all diligence, in the name of Jesus (Proverbs 4:23).",
            fullText: <Text style={styles.text}>You determine the outcomes in your life, because God has given no one but
                you power over your options. When things aren't working out for us, we often
                look elsewhere for the reason. Like Eve, we can say, <Bold>“The devil made me do
                it.”</Bold> No, Jesus said, <Bold>“I give unto you power…over all the power of the
                enemy” (Luke 10:19 KJV).</Bold> Like Adam, we say, <Bold>“Eve made me do it.” But
                the psalmist says, “I trust God, so I am not afraid. What can human beings
                do to me?” (Psalm 56:4).</Bold> Sometimes we believe circumstances beyond our
                control are responsible, but with God, <Bold>“all things [including people and
                circumstances] work together for [our] good” (Romans 8:28 NKJV)—not
                for our defeat!</Bold>
                <Br />
                Your mistaken beliefs victimise you and predispose you to a lifestyle of
                trying to change people and circumstances. That's like attempting to
                unscramble an egg. You become a blame-fixer instead of a problem-solver.
                Accept the truth that no matter what satan, others or circumstances do, God
                gives you the right to the last word in your life. <Bold>“Greater is He that is in you,
                than he that is in the world” (1 John 4:4 KJV).</Bold> The outcome is not
                determined by what goes on around you, but by what goes on within you. <Bold>“As
                he thinks in his heart, so is he” (Proverbs 23:7 NKJV).</Bold> The devil can only
                determine the outcome with your permission and co–operation—and you
                don't have to give them! So believe in God's ability to bring you triumphantly
                through this difficulty.
            </Text>,
            prayerPoints: [
                "I reject every form and activity of bewitchment against me and bring them to naught, in the name of Jesus.",
                "Grace of God to excel in life, rest upon me, in the name of Jesus.",
                "The power of God to overcome every wile, scheme and device of the enemy, fall on me, in the name of Jesus.",
            ]
        },
        {
            day: 12,
            topic: "Your Blind Spot (1)",
            wordStudy: "Psalm 19:12-14",
            confession: "O Lord, teach me to number my days that I may gain a heart of wisdom, in the name of Jesus (Psalm 90:12).",
            fullText: <Text style={styles.text}>There are parts of yourself you'll never see without God's help. In one sense,
                you know yourself better than anyone else does. You alone have access to
                your inner thoughts, feelings and judgments. In another sense, you know
                yourself worse than anyone else does. Why? Because we rationalize, justify,
                minimize, forget, and embellish—and we don't even know we're doing it. We
                all fall for the self-serving bias. We claim too much credit and accept too little
                blame. We pay attention to experts who agree with our opinions, while
                ignoring or discounting all evidence to the contrary. Our memories are not
                simply faulty, they're faulty in favour of our ego.
                <Br />
                The book <Italic>Egonomics</Italic> cites a survey in which 83 per cent of people were
                confident in their ability to make good decisions, but only 27 per cent were
                confident in the ability of the people they worked closely with to make good
                decisions. We're stunned when someone sees past our defences into our
                souls. It's not that they're geniuses, it's just that we're sitting in our own blind
                spot and, without the work of the Holy Spirit within us, much of the time we
                can't even see our sin.
                <Br />
                The psalmist wrote: <Bold>“How can I know all the sins lurking in my heart?
                Cleanse me from these hidden faults. Keep Your servant from deliberate
                sins! Don't let them control me. Then I will be free of guilt and innocent
                of great sin. May the words of my mouth and the meditation of my heart
                be pleasing to You, O Lord, my Rock and my Redeemer” (Psalm 19:12-
                14 NLT).</Bold> That's a prayer you should pray—every day.
            </Text>,
            prayerPoints: [
                "Lord, cleanse me from every hidden sin and fault, in the name of Jesus.",
                "Lord, deliver me from every selfish act, in the name of Jesus.",
                "I reject the spirit of pride, in the name of Jesus.",
            ]
        },
        {
            day: 13,
            topic: "Your Blind Spot (2)",
            wordStudy: "Psalm 19:9-2",
            confession: "O Lord, teach me to hide and treasure Your Word in my heart that I might not sin against You (Psalm 119:11).",
            fullText: <Text style={styles.text}>Sometimes trying to see the truth about yourself is like trying to see inside
                your own eyeballs. <Bold>“Who can discern their own errors?”(Psalm 19:12),</Bold> the
                psalmist asked. Fortunately, you're not left on your own. The Spirit is already
                at work in you. Your job is simply to listen and respond. Guilt isn't your
                enemy; sin is. God's Spirit will often bring a sense of conviction, and, when it
                does, the proper response isn't to try and suppress the guilt but to deal with the
                issue. When you don't, those issues can build up like cholesterol in your
                arteries.
                <Br />
                At a moment of great crisis, Samson arose to exert his strength, <Bold>“but he
                did not know that the Lord had left him” (Judges 16:20 NIV).</Bold> He had
                become callous and lost his sensitivity to God. Our bodies have an amazing
                capacity to warn us about what ails them, if we learn to read the signs. Chest
                pains may indicate heart trouble and there are more subtle clues. Many of us
                will have seen the TV ads or read about detecting when someone is having a
                stroke. God will enable you to find the truth about your soul if you are open
                and willing. Left to yourself, you will usually rationalize or defend yourself.
                You will <Bold>“call evil good and good evil” (Isaiah 5:20 NIV).</Bold>
                <Br />
                At the other extreme, you can become a neurotic over-analyzer. Please be
                warned against “depending on the diligence of your own scrutiny rather than
                on God for the knowledge and discovery of our sin.” What's the answer?
                Allow your thoughts and responses to be guided by the Holy Spirit.
            </Text>,
            prayerPoints: [
                "I won't take God for granted, in the name of Jesus.",
                "Holy Spirit, help me in my Christian walk, in the name of Jesus.",
                "Lord, make me to see myself the way You see me, in the name of Jesus.",
            ]
        },
        {
            day: 14,
            topic: "Your Blind Spot (3)",
            wordStudy: "Colossians 3:9-10",
            confession: "I will be anxious for nothing, but in everything by prayer and supplication, with thanksgiving, I shall make my request known to God (Philippians 4:6).",
            fullText: <View> 
                <Text style={styles.text}>
                    The Bible speaks about <Bold>“putting off”</Bold> certain sins. Then it speaks about
                    <Bold>“putting on”</Bold> certain Christ-like characteristics. Using this metaphor of
                    clothing, let's think about the acronym <Bold>RAGS:</Bold>
                </Text>

                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            <Bold>Resentment.</Bold> How's your irritability level these days? Are you more, or
                            less, easily irritated? How about bitterness and unforgiveness? When
                            someone hurts you, do you attack or withdraw? Is your handling of
                            resentment getting better, worse, or in neutral?
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            <Bold>Anxiety.</Bold> On a scale of one to ten, what's your discouragement level these
                            days? Do you have more or fewer fears about money, your health, your
                            job, or what other people think of you? Do you allow those fears to keep
                            you from doing what God wants? Do you find your concerns motivate
                            you to pray more frequently?
                        </Text>
                    },
                    {
                        serial: 3,
                        body: <Text>
                            <Bold>Greed.</Bold> is just mis-managed desire. So is your self-control going
                            up, down, or is it in neutral? Are you living with more openness and less
                            hiddenness than you used to—living more of your life in the light? Do
                            you find that what you desire and enjoy is increasingly in line with what
                            God wants for you?
                        </Text>
                    },
                    {
                        serial: 4,
                        body: <Text>
                            <Bold>Superiority.</Bold> Are you becoming less preoccupied with self? Do you find
                            yourself thinking more about other people and God, as well as the work
                            He has for you to do? How often in conversation do you remark on the
                            positive characteristics of others instead of their negative qualities? Do
                            you come across as hard, jaded and cynical? Are you spending more or
                            less time serving others?
                        </Text>
                    },
                ]} />
                <Text style={styles.text}>
                    Today, ask God to help you look into your own heart and give you the
                    grace to deal with what you find there.
                </Text>
            </View>,
            prayerPoints: [
                "I receive courage to do the will of God at all times, in the name of Jesus.",
                "I receive grace to be focused in my Christian walk with the Lord, in the name of Jesus.",
                "Lord, grant me the spirit of meekness, in the name of Jesus.",
            ]
        },
        {
            day: 15,
            topic: "Your Blind Spot (4)",
            wordStudy: "Colossians 3:10-14",
            confession: "Let the words of my mouth and the meditation of my heart be acceptable in thy sight, O Lord (Psalm 19:14).",
            fullText: <Text style={styles.text}>Jesus said when the Holy Spirit came He would convict us of sin. But
                conviction is not the same as “getting caught.” When we get caught doing
                wrong, we feel pain, but it's not necessarily a conviction over sin. Often it's
                just embarrassment over how others are thinking about us. If we thought no
                one would ever know, we wouldn't be in pain. And conviction isn't the same
                thing as fear of punishment.
                <Br />
                Conviction is when you get a glimpse of what you're capable of: “How did
                I become the kind of person who can lie, cheat on a test, have an affair, claim
                credit for what I didn't do, act in a cowardly way instead of courageously, or
                use people for my own ends?” These aren't questions you'd normally ask
                yourself. The Bible says, <Bold>“This is the verdict: Light has come into the
                world, but people loved darkness instead of light…” (John 3:19 NIV).</Bold>
                When God is at work in your life, the pain isn't about other people knowing, or
                even about the consequences. That's all external. The pain of conviction is
                internal—it's over who you are.
                <Br />
                Unless your car windscreen is clean, you run the risk of ending up in a
                ditch and you can't clean it yourself while you're driving—that's why you have
                windscreen wipers. Similarly, the Holy Spirit's job is to reveal sin, let you
                repent of it, and cleanse you so that you can go where God wants to take you in
                life. Each day you need to pray, “Lord, send as much light as I can stand. Clean
                off my windscreen so I can see more clearly. Cleanse me.”
            </Text>,
            prayerPoints: [
                "Lord, cleanse me from every hidden sin, in the name of Jesus.",
                "I refuse to walk in darkness, I shall walk in the light of life, in the name of Jesus.",
                "Light of God, illuminate my life, in the name of Jesus.",
            ]
        },
        {
            day: 16,
            topic: "Focus on Service, Not Promotion",
            wordStudy: "Proverbs 25:6-7",
            confession: "I will humble myself under the mighty hand of God that He may exalt me in due time, in the name of Jesus (1 Peter 5:6).",
            fullText: <View> 
                <Text style={styles.text}>
                    Aspiring to leadership is commendable. Paul said, <Bold>“If someone aspires to be
                    an elder, he desires an honourable position” (1 Timothy 3:1 NLT).</Bold> But
                    there's a difference between stepping forward to take on the responsibility of
                    leadership, and stepping forward to project yourself into the limelight. Harry
                    Truman said, “If you can't stand the heat, stay out of the kitchen.” The
                    question isn't whether you enjoy being the focus of attention, but whether
                    you can take the heat that goes along with it. For every person who
                    acknowledges your skills and appreciates your accomplishments, there'll be
                    ten who make demands on your time, talent and treasure.
                    <Br />
                    Consider Barnabas. When the Gospel was first preached to the Gentiles,
                    church leaders in Jerusalem sent Barnabas to check it out. <Bold>“When he came
                    and had seen the grace of God, he was glad, and encouraged them all,
                    that with purpose of heart they should continue with the Lord. For he
                    was a good man, full of the Holy Spirit and faith. And a great many
                    people were added to the Lord” (Acts 11:23-24 NKJV).</Bold> Barnabas had
                    three sterling qualities:
                </Text>

                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            He had nothing to prove. He never sought the limelight. When he
                            mentored Paul, he happily let the emerging apostle rise above him,
                            supporting him every step of the way.
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            He had nothing to lose. He didn't seek to guard his reputation or fear
                            losing his popularity. He wanted to serve, not be served.
                        </Text>
                    },
                    {
                        serial: 3,
                        body: <Text>
                            He had nothing to hide. He didn't try to maintain a façade or image. He
                            remained authentic, vulnerable and transparent and, best of all, he
                            rejoiced in the success of others.
                        </Text>
                    },
                ]} />
            </View>,
                    
            prayerPoints: [
                "Lord, empower me to help others in their legitimate assignment, in the name of Jesus.",
                "I will not hinder others from rising, in the name of Jesus.",
                "Lord, make me to be Kingdom minded, in the name of Jesus.",
            ]
        },
        {
            day: 17,
            topic: "The First Five Minutes",
            wordStudy: "Proverbs 18:20-21",
            confession: "I shall not be snared with the words of my mouth, in the name of Jesus (Proverbs 6:2).",
            fullText: <Text style={styles.text}>What you say in the first five minutes can set the tone for the hours that
                follow. Good speakers understand they must grab the audience's attention in
                the first five minutes, otherwise the opportunity to have an impact on them or
                move them to action can be lost. The same principle applies to your family.
                The first five minutes of a morning can determine how a mother will interact
                with her children that day. A snarl or a complaint as the children gather for
                breakfast can sour their relationship for hours. When a man arrives home
                from work at the end of the day, the way he greets his wife can influence their
                interaction throughout the evening. If he mutters, “Not rice again!” the
                relationship can be put on edge until bedtime.
                <Br />
                But it doesn't have to be that way in your house! When you've been apart
                from those you love, you can do an attitude check and reset your mood before
                you walk through the door. It's OK to share your concerns but, generally
                speaking, you should leave work-related problems at work. Question: Does
                your family look forward to you coming home each night? If not, why not? A
                little sensitivity on your part can go a long way, and bring you wonderful
                benefits. The Bible says: <Bold>“An offended friend is harder to win back than a
                fortified city. Arguments separate friends like a gate locked with bars.
                Wise words satisfy like a good meal; the right words bring satisfaction.
                The tongue can bring death or life…” (Proverbs 18: 19-21 NLT).</Bold>
                Remember: it depends on the first five minutes.
            </Text>,
            prayerPoints: [
                "Teach me, Lord, to bridle my tongue, in the name of Jesus.",
                "Father, help me to be sensitive to the feelings of my loved ones and everyone you bring my way, in the name of Jesus.",
                "I receive the grace to be swift to hear and slow to speak; from my mouth shall proceed words of wisdom, in the name of Jesus.",
            ]
        },
        {
            day: 18,
            topic: "Walk by Faith (1)",
            wordStudy: "2 Corinthians 5:7",
            confession: "I am born of God in Christ and shall live by faith, in the name of Jesus (Romans 1:17).",
            fullText: <Text style={styles.text}>A blind man with a guide dog “walks by faith” in his dog. He believes that
                what the dog sees will be translated into a signal that tells him when it's time
                to go, stop, turn right or left. And he picks up on those signals by holding the
                harness that connects him with the dog. Why does he trust the dog? Because
                it has something he doesn't have: sight. And it's the same with you and God.
                Today it might be unclear where God is taking you. That's why the blind man
                must hold the harness and stay connected to the dog. He may wonder, “Why
                am I stopping at this corner so long?” Because there's traffic coming and he's
                being protected from unseen danger.
                <Br />
                God says, <Bold>“I will bring the blind by a way they did not know… I will
                make darkness light before them, and crooked places straight. These
                things I will do for them, and not forsake them” (Isaiah 42:16 NKJV).</Bold>
                God has your tomorrow already planned out, even though you haven't been
                there yet. He works outside of time, so He's not held back by the limitations
                we labour with. Paul writes, <Bold>“I pray that your hearts will be flooded with
                light so that you can see something of the future He has called you to
                share… I pray that you will begin to understand how incredibly great
                His power is to help those who believe Him. It is the same mighty power
                that raised Christ from the dead and seated Him in the place of honour
                at God's right hand…” (Ephesians 1:18-20 TLB).</Bold>
            </Text>,
            prayerPoints: [
                "Teach me, O Lord, to depend on You alone, in the name of Jesus.",
                "Lord, make every crooked path of my life straight, in the name of Jesus.",
                "Holy Spirit, illuminate my life, in the name of Jesus.",
            ]
        },
        {
            day: 19,
            topic: "Walk by Faith (2)",
            wordStudy: "Hebrews 11:6",
            confession: "God will do something new in my life and family, in the name of Jesus (Isaiah 43:19).",
            fullText: <View> 
                <Text style={styles.text}>
                    When you decide to fly to a certain destination, these things all have to work
                    together:
                </Text>

                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            You must identify the right airline and flight schedule that will get you to
                            your intended destination.
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            The plane must operate on a timetable that tells you when it's leaving and
                            when it's arriving, and that the pilot has the route worked out in advance.
                        </Text>
                    },
                    {
                        serial: 3,
                        body: <Text>
                            The airline has to set a price you can afford to pay. Then they put your
                            name in a computer, and everything is ready to go.
                        </Text>
                    },
                ]} />
                <Text style={styles.text}>
                    However, here's what's not going to happen when you arrive at the airport.
                    You're not going to ask them to explain how the plane works; what buttons the
                    pilot plans to push and what the equipment will do when he or she pushes
                    them. You're not going to check on the route they plan to take, or ask how fast
                    they plan to fly, or how high, or whether the pilot plans to fly by manual
                    control or autopilot. You're not going to argue about the price, after you've
                    already made your reservation and paid your fare. Why? Because you're
                    confident that the equipment is sound and the pilot is experienced; you know
                    you will safely reach your destination.
                    <Br />
                    You say, “What if the plane goes down?” If you're a redeemed child of
                    God, you go up! <Bold>“To depart, and to be with Christ …is far better”
                    (Philippians 1:23 NKJV).</Bold> Either way, you win. Basically, you put your trust
                    in the plane, the pilot and the airline. Today, God is asking you to do the same
                    with Him—no more and no less!
                </Text>
            </View>,
            prayerPoints: [
                "I totally surrender to You, Lord; lead and guide me, in the name of Jesus.",
                "I reject delay, in the name of Jesus.",
                "I shall get to my God-ordained land, in the name of Jesus.",
            ]
        },
        {
            day: 20,
            topic: "Concerned about Your Weight?",
            wordStudy: "1 Corinthians 6:19-20",
            confession: "I present my body as a living sacrifice, holy and acceptable to God and His will, in the name of Jesus (Romans 12:2).",
            fullText: <View> 
                <Text style={styles.text}>
                    Here are seven principles for dealing with a weight problem:
                </Text>

                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            Realise that your body is not your own. “Your body is the temple of the
                            Holy Spirit who is in you… therefore glorify God in your body” (1
                            Corinthians 6:19-20 NKJV). Remind yourself that you're the manager
                            of a body God has entrusted you with, to do His will each day.
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            Keep a food journal to determine how much you're eating, or not eating.
                            Focus on eating foods that help your body, and avoid or minimize foods
                            that hurt it.
                        </Text>
                    },
                    {
                        serial: 3,
                        body: <Text>
                            Listen to the concerns of your loved ones.
                        </Text>
                    },
                    {
                        serial: 4,
                        body: <Text>
                            Check your weight against the standard weight charts to see if you're at a
                            significant variation. It's easier to lose five kilograms than twenty.
                        </Text>
                    },
                    {
                        serial: 5,
                        body: <Text>
                            Instead of going on repeated diets, write down the kinds of food you will
                            and won't eat, and the portions. Yes, it will take time for your new
                            discipline to take root and your will to strengthen. However, each day
                            you do it, your self-control will increase until you start feeling so good
                            about yourself that your old appetite begins to lose its grip on you.
                        </Text>
                    },
                    {
                        serial: 6,
                        body: <Text>
                            Avoid fat-phobic people with a judgmental attitude toward those who
                            are overweight. Often they're just masking their own insecurities and
                            reinforcing your distorted self-image.
                        </Text>
                    },
                    {
                        serial: 7,
                        body: <Text>
                            Stay the course. You may have several ups and downs, but with
                            determination and God's help, you will get on the road to freedom. <Bold>“You
                            need to persevere so that when you have done the will of God, you
                            will receive what He has promised” (Hebrews 10:36 NIV).</Bold>
                        </Text>
                    },
                ]} />
            </View>,
            prayerPoints: [
                "Lord, help me to control my appetite, in the name of Jesus.",
                "I reject the spirit of laziness, in the name of Jesus.",
                "Lord, give me listening and a hearing ear, in the name of Jesus.",
            ]
        },
        {
            day: 21,
            topic: "Learning to Overlook the Flaws",
            wordStudy: "1 Peter 4:8-10",
            confession: "There is no fear in love; but perfect love casts out fear (1 John 4:18a).",
            fullText: <Text style={styles.text}>A person's sense of self-worth is often based on the reactions, positive or
                negative, of those around them. So, your words and attitudes can literally build
                them up or tear them down. Dr Paul Brand was a flight surgeon during World
                War II. He tells in one of his books of a man named Peter Foster, a Royal Air
                Force pilot. Foster flew a Hurricane, a fighter plane, with a design flaw: the
                single-propeller engine was mounted in the front, and the fuel lines ran past the
                cockpit. In a direct hit, the pilot would instantly be engulfed in flames before
                he could eject. The consequences have been often tragic. Some RAF pilots
                caught in that inferno would undergo ten or twenty surgeries to reconstruct
                their faces.
                <Br />
                Peter Foster was one of those downed pilots whose faces were burnt
                beyond recognition. But Foster had the support of his family and the love of
                his fiancée. She assured him that nothing had changed except a few
                millimeters of skin. Two years later they were married. Foster said of his wife,
                “She became my mirror. She gave me a new image of myself. When I look at
                her, she gives me a warm, loving smile that tells me I'm OK.”
                <Br />
                Your marriage, and other valued relationships in your life, ought to work
                that same way too—even when disfigurement has not occurred. It should be
                like a mutual admiration society that builds each other's self-esteem and
                overlooks flaws that could otherwise be destructive. There's a biblical word
                for this kind of commitment: it's called “love.”
            </Text>,
            prayerPoints: [
                "Lord, let my life point others to You, in the name of Jesus.",
                "I refuse to be used to bring others down, in the name of Jesus.",
                "Holy Spirit, help me to show the love of God You have shed abroad in my heart to others, in the name of Jesus.",
            ]
        },
        {
            day: 22,
            topic: "You Have a Sound Mind! (1)",
            wordStudy: "Ephesians 1:17-18",
            confession: "God has not given me a spirit of fear, but of power and of love and of a sound mind (2 Timothy 1:7).",
            fullText: <Text style={styles.text}>No matter what others say about you, God says you have <Bold>“a sound mind”!</Bold>
                Many people live their lives believing that they are intellectually
                inferior—born that way, and will always be that way. The problem with
                believing this is that you begin to live accordingly. Not because it's true—but
                because you believe it's true! God created us so that whatever we really
                believe, we act on. So our self-concept becomes a self-fulfilling prophecy.
                For example, we may act stupidly because we believe we are stupid, or we
                may act wisely because we believe we are wise. So we must choose to
                believe God's opinion of what we are, and, by faith, adapt our actions
                accordingly—until His opinion becomes our opinion! The Bible calls this
                <Bold>“the renewing of your mind” (Romans 12:2 NKJV).</Bold> This calls for an
                attitude transformation, and it can begin with a few small steps.
                <Italic>Step 1: Choose to believe what God says about you.</Italic> Every outcome
                begins with a choice—an act of your will. No matter what your emotions say,
                choose to <Bold>“let God be true, but every man [and every contrary emotion]
                a liar” (Romans 3:4 NKJV).</Bold> Whatever you've said about yourself
                previously, or whatever others have said, God says He has not given you an
                attitude of fear, <Bold>“but of power and of love and of a sound mind.”</Bold> He has
                <Bold>“given”</Bold> it to you, but you must choose to believe and receive His gift of
                <Bold>“power, love and a sound mind.”</Bold> Memorise it and meditate on it until it
                takes root in you.
            </Text>,
            prayerPoints: [
                "Lord, increase my faith, in the name of Jesus.",
                "I reect the spirit of doubt and unbelief, in the name of Jesus.",
                "I receive the spirit of boldness and of love and of a sound mind, in the name of Jesus.",
            ]
        },
        {
            day: 23,
            topic: "You Have a Sound Mind! (2)",
            wordStudy: "Ephesians 1:19-20",
            confession: "I have not received the spirit of bondage again to fear, but I have receieved the Spirit of adoption, by whom I cry out, “Abba, Father” (Romans 8:15).",
            fullText: <Text style={styles.text}><Italic>Step 2: Start saying what God says about you in His Word.</Italic> Words matter,
                especially when they're God's words! Look around you: everything in
                creation was first a word that was thought and spoken by God. <Bold>“Then God
                said, 'Let there be…' And it was so” (Genesis 1:14-15 NIV).</Bold> His words
                contain the power of who He is. So when you believe and speak God's Word,
                you're connecting with His mighty creative <Italic>Logos.</Italic> Why is it necessary to
                speak God's Word and not just think it? First, because hearing yourself
                speaking God's Word is a powerful reinforcer. Second, because you
                remember what you recite. Third, because the enemy hears you. When Jesus
                spoke God's Word to satan in the wilderness temptation, he fled <Bold>(Matthew
                4:11).</Bold>
                <Italic>Step 3: Act on what God says about you.</Italic> God has given you <Bold>“a sound
                mind”</Bold>—that means you're a good thinker. Now, if you really believed that,
                what would you do that you're not doing now? How big a step of faith would
                you be willing to take? God has given you the ability to <Bold>“love.”</Bold> If you really
                believed that, how would you demonstrate your love for God and others?
                Would you begin or resume giving to your church? Would you forgive
                someone who has hurt you? Would you serve others instead of being selfabsorbed?
                Nothing builds your faith like acting on God's Word. Take these
                steps and it will change the way you believe about yourself and God's Word.
            </Text>,
            prayerPoints: [
                "Lord, grant me the grace to live by Your Word, in the name of Jesus.",
                "Lord, teach me by Your Spirit to manifest Your love to others.",
                "Lord, make me a faithful servant, in the name of Jesus.",
            ]
        },
        {
            day: 24,
            topic: "You Have a Sound Mind! (3)",
            wordStudy: "Ephesians 1:21-23",
            confession: "I will wait on the Lord until I am endued with power from on high in the name of Jesus (Luke 24:49).",
            fullText: <View> 
                <Text style={styles.text}>
                    You say, “What is this 'sound mind' that God says He has given us?” The
                    original Greek word means “sane, rational, logical, realistic, and
                    reasonable.” It includes qualities such as:
                </Text>

                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            <Bold>Being self-controlled.</Bold> It's not controlled by anyone or anything outside
                            itself—including people, circumstances, impulses, emotions, intellect,
                            and especially not fear. It stays in control at all times. Even when faced
                            by problems with no easy solutions, it remains anchored to the truth, as
                            defined by God and His Word.
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            <Bold>Being discerning.</Bold> It distinguishes wisely between alternatives. It
                            discerns between reality and fantasy, good and evil, effective and
                            ineffective, worthwhile and worthless. It's the faculty that enables us to
                            make good decisions in difficult situations.
                        </Text>
                    },
                    {
                        serial: 3,
                        body: <Text>
                            <Bold>Being focused.</Bold> Like a laser beam, it has the capacity to lock in on a
                            desired target. Like a finely tuned receiver, it can minimise distortion
                            and resist competing sound waves. It limits interference, uncertainty
                            and confusion. Once it's “set on task,” a sound mind will help you
                            concentrate your energies on fulfilling the important priorities in your
                            life.
                        </Text>
                    },
                    {
                        serial: 4,
                        body: <Text>
                            <Bold>Being orderly.</Bold> It is a process–oriented mind, operating with
                            cause/effect logic. It's concerned with getting you from A to B—goal to
                            realisation—by a series of orderly steps that contribute to the fulfilment
                            of your purpose. You are <Bold>“fearfully and wonderfully made” (Psalm
                            139:14 NIV).</Bold> The God whose mind created you is reflected in the mind
                            He gave you to use.
                        </Text>
                    },
                ]} />
                <Text style={styles.text}>
                    Knowing who you are, now go forward and experience <Bold>“life… more
                    abundantly” (John 10:10 NKJV).</Bold>
                </Text>
            </View>,
            prayerPoints: [
                "Holy Spirit, help me in the way I relate with others, in the name of Jesus.",
                "I receive the gift of discernment, in the name of Jesus.",
                "Lord, help Your Church to be orderly, in the name of Jesus.",
            ]
        },
        {
            day: 25,
            topic: "Who Is Advising You?",
            wordStudy: "Ephesians 3:7-9",
            confession: "Whatsoever I do in righteousness shall prosper, in the name of Jesus.",
            fullText: <Text style={styles.text}>The first Psalm in the Bible talks about the dangers of getting the wrong
                counsel, listening to the wrong voices, and mixing with the wrong people.
                <Bold>“Oh, the joys of those who do not follow the advice of the wicked, or stand
                around with sinners, or join in with mockers. But they delight in the law
                of the Lord, meditating on it day and night. They are like trees planted
                along the riverbank, bearing fruit each season. Their leaves never
                wither, and they prosper in all they do” (Psalm 1:1-3 NLT).</Bold> Implicit in
                the word <Italic>prosper</Italic> is “stability, nourishment, productivity, strength, durability
                and success.”
                <Br />
                So pick your friends carefully and prayerfully, because those closest to you
                can have the ability to make you or break you. Check out any advisor before
                he or she speaks into your life. Make sure they are: (1) people of integrity; (2)
                people of faith; (3) people with influence; (4) people with complementary
                gifts; (5) people who share your vision; (6) people who are loyal; and (7)
                people who are creative. If you look for these seven qualities in those who
                advise you, there's every chance you'll succeed in what you've been called to
                do. Also be sure to get God's input. <Bold>“Whether you turn to the right or to the
                left, your ears will hear a voice behind you, saying, 'This is the way; walk
                in it”' (Isaiah 30:21 NIV).</Bold> Make sure the goals and plans you've set are in
                harmony with God's will for you. Because if they're not, having the best
                counsellors in the world won't help you one bit.
            </Text>,
            prayerPoints: [
                "I refuse to keep company with sinners, in the name of Jesus.",
                "Lord, connect me with destiny helpers, in the name of Jesus.",
                "I reject evil counsel, in the name of Jesus.",
            ]
        },
        {
            day: 26,
            topic: "Speak the Truth in Love",
            wordStudy: "Romans 15:14",
            confession: "I shall speak the truth in love, in the name of Jesus (Ephesians 4:15).",
            fullText: <Text style={styles.text}>Even in a great relationship, you will have disagreements. That's why you
                need to learn to fight fair. Paul addresses it this way: <Bold>“Speak the truth in
                love, growing in every way more and more like Christ” (Ephesians 4:15).</Bold>
                If you grew up in a home filled with tension and angry words, you may find
                yourself trying to avoid arguments at any cost. But silence doesn't solve
                problems; it just allows them to become unspoken wedges between you.
                What should you do? First, try to understand the difference between healthy
                and unhealthy conflict. In an unstable marriage, hostility is aimed at your
                spouse's soft underbelly with comments like, “You never do anything right!”
                and “Why did I marry you in the first place?” and “You're getting more like
                your mother every day!” Such remarks strike at the very heart of your mate's
                self-worth.
                <Br />
                Healthy conflict, by contrast, remains focused on the issues that cause
                disagreement. For example: “It upsets me when you don't tell me you're
                going to be late for dinner,” or “I was embarrassed when you made me look
                foolish at the party last night.” Can you hear the difference? Even though the
                two approaches may be equally contentious, the first assaults the dignity of
                your spouse while the second addresses the source of conflict. When you
                learn to make this important distinction, you can work through your
                disagreements without wounding and insulting each other. Plus, when
                gaining the upper hand leaves your spouse feeling wounded and upset, you
                both lose.
                <Br />
                Remember: when someone feels loved and valued, they're generally
                more open and receptive to what you have to say.
            </Text>,
            prayerPoints: [
                "Lord, make me a peacemaker, in the name of Jesus.",
                "Father, teach me to speak through every circumstance, in the name of Jesus.",
                "I refuse to be controlled by untoward emotions, in the name of Jesus.",
            ]
        },
        {
            day: 27,
            topic: "Whose Company Do You Prefer?",
            wordStudy: "Acts 4:23-24",
            confession: "My ears will seek knowledge, in the name of Jesus (Proverbs 18:15b).",
            fullText: <Text style={styles.text}>We all have our “own company.” It's where we feel most at home, where we
                naturally gravitate to when we have the choice. A great Bible teacher once
                said, “The important thing about a man is not where he goes when he is
                compelled to go, but where he goes when he is free to go where he will.” The
                apostles went to jail under compulsion, but when they were given their
                liberty, they chose to return to <Bold>“their own company”</Bold> of praying believers.
                How revealing! The clear choices of life—not the compulsions—reveal our
                true character.
                <Br />
                Let's consider a real-life illustration. You're absent from church on
                Sunday. Where are you? If you're having surgery, your absence reveals that
                you're compelled to miss church due to hospitalization. However, if you're on
                the golf course, that tells us a different story. Assuming that you are not on a
                well deserved vacation, we know that you chose to skip church in favour of
                your “own company”—your fellow golfers. Has your choice to play instead
                of to pray become a habit? If so, you might be on the threshold of letting slip
                <Bold>“meeting together with other believers, which some people have got into
                the habit of doing” (Hebrews 10:25).</Bold> Habits are not the product of one
                decision, they are the result of repeated choices that eventually become
                automatic responses.
                <Br />
                How can you know your true spiritual condition? By the company you
                keep and the choices you make. When you are free to go, where do you
                choose to go? Whose company do you prefer?
            </Text>,
            prayerPoints: [
                "By the power of the Holy Spirit, I will always gravitate towards right things and the right people, in the name of Jesus.",
                "I receive power to resist satan and sin, in the name of Jesus.",
                "Father, help me to make the right choice in my life at all times, in the name of Jesus.",
            ]
        },
        {
            day: 28,
            topic: "Dealing with Change (1)",
            wordStudy: "Isaiah 54:2-3",
            confession: "The Lord will enlarge my coast on every side, in the name of Jesus (1 Chronicles 4:10).",
            fullText: <Text style={styles.text}>Think how foolish it is to pray for success, and resist the changes needed to
                bring it about. However, we do that, don't we? Let's face it; it's easier to settle
                for the status quo than to face the what-ifs that accompany progress. Some of
                us even think it's selfish and unspiritual to pray for success. Not Jabez! Listen
                to his prayer: <Bold>“Oh, that You would bless me indeed, and enlarge my
                territory, that Your hand would be with me, and that You would keep me
                from evil, that I may not cause pain!” (1 Chronicles 4:10 NKJV).</Bold> So God
                granted him what he requested.
                <Br />
                Consider this list of “changes” you may be resisting: leaving an
                unfulfilling job, starting a business, or letting go of an unprofitable one,
                learning to use a computer, abandoning a toxic relationship, letting go of an
                unaffordable home, car or lifestyle, establishing a responsible spending plan.
                All these changes are normal; they're part of daily life.
                <Br />
                However, panic sets in when the change is unexpected. That's when we
                discover the difference between what we say and what we truly believe. “And
                we know that all things work together for good to those who love God, to
                those who are called according to His purpose” <Bold>(Romans 8:28 NKJV).</Bold> The
                key words in this Scripture are <Bold>“all things,” “good”</Bold> and <Bold>“purpose.”</Bold> Since
                you are in Christ, nothing can get to you without first coming through Him. In
                “all things,” including things you don't like, God has a “purpose,” and He's
                working for your “good.” You will look back and say, “If I hadn't gone
                through that, I wouldn't be experiencing the blessing of God I enjoy today.”
            </Text>,
            prayerPoints: [
                "Father, grant me good success in all my pursuits, in the name of Jesus.",
                "Lord, grant me the strength to cast down and forsake every unprofitable and ungodly attitude, in the name of Jesus.",
                "Lord, enlarge my coast, in the name of Jesus.",
            ]
        },
        {
            day: 29,
            topic: "Dealing with Change (2)",
            wordStudy: "Acts 7:2-3",
            confession: "Death and life are in the power of the tongue: and they that love it shall eat the fruit thereof (Proverbs 18:2).",
            fullText: <Text style={styles.text}>How would you feel if God told you to leave your job, sell your house and go
                to a city you didn't know? That's what happened to 75-year-old Abram and his
                65-year-old wife, Sarai. <Bold>“Leave your native country… and go to the land
                that I will show you.”</Bold> Why would God do that? Because He has a special
                destiny in mind for each of us! He told Abram, <Bold>“I will make you into a great
                nation. I will bless you…and you will be a blessing to others” (Genesis
                12:2 NLT).</Bold> With no hint of resistance, Abram embraced change, trusted God,
                and reaped the incredible rewards. It is no wonder the Bible calls him the
                father of faith. To walk with God and fulfill His will you must have a mindset
                that adapts to change, sometimes costly and uncomfortable change. How do
                you do it? “By faith!” <Bold>(Hebrews 11:8).</Bold> Faith in whom? Yourself? Others?
                No. Faith in the goodness of God!
                <Br />
                Did you know that those who flow with change rather than resist it are
                less stressed, healthier, and generally happier? So start developing a “change
                mindset.” Break out of your rut and develop a new routine and if you are
                serious about it, ask your friends and family to point out areas where you
                constantly demonstrate inflexibility. Where should you start? With this
                Scripture: <Bold>“Casting the whole of your care [all your anxieties, all your
                worries, all your concerns, once and for all] on Him, for He cares for you
                affectionately and cares about you watchfully” (1 Peter 5:7 AMP).</Bold> Live
                by the principle: “Where God guides, He provides.”
            </Text>,
            prayerPoints: [
                "Lord, I receive grace for the obedience of faith, in the name of Jesus.",
                "Anointing of greatness, fall on me, in the name of Jesus.",
                "I reject the spirit of worry and anxiety, in the name of Jesus.",
            ]
        },
        {
            day: 30,
            topic: "Dealing with Change (3)",
            wordStudy: "Joshua 1:9",
            confession: "I will go forward, in the name of Jesus (Exodus 14:15b).",
            fullText: <View> 
                <Text style={styles.text}>
                    With the Red Sea in front of them and Pharaoh's chariots behind them, Moses
                    told the Israelites: <Bold>“Do not be afraid. Stand still, and see the salvation of
                    the Lord, which He will accomplish for you today. For the Egyptians,
                    whom you see today, you shall see again no more, forever. The Lord will
                    fight for you, and you shall hold your peace. And the Lord said to
                    Moses… 'Tell the children of Israel to go forward”' (Exodus 14:13-15
                    NKJV).</Bold> Notice, God told the people of Israel two things. First, that He would
                    fight for them. Second, that they must go forward—in spite of their fear.
                    When you face the unknown, it's normal to feel fear, but that's when you need
                    to put your trust and confidence in God!
                    <Br />
                    In order for Him to fulfil His promise to you, you must obey what He has
                    told you to do. How should you deal with change? In three ways:
                </Text>

                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            Focus on its long-term benefits instead of its short-term inconvenience.
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            Be honest with yourself about what you really feel and fear about the
                            change, and take it to God in prayer.
                        </Text>
                    },
                    {
                        serial: 3,
                        body: <Text>
                            Keep an open attitude toward new ideas, and refuse to be influenced by
                            rigid thinkers. Stand on this Scripture: <Bold>“Enlarge the place of your tent,
                            and let them stretch out the curtains of your dwellings; do not
                            spare; lengthen your cords, and strengthen your stakes. For you
                            shall expand to the right and to the left…” (Isaiah 54:2-3 NKJV).</Bold>
                        </Text>
                    },
                ]} />
                <Text style={styles.text}>
                    God doesn't change, but He moves! In order to succeed, you must move
                    with Him.
                </Text>
            </View>,
            prayerPoints: [
                "Anointing for enlargement, fall on me, in the name of Jesus.",
                "Lord, make my attitude right towards You, in the name of Jesus.",
                "My family and I will move forward, in the name of Jesus.",
            ]
        },
        {
            day: 31,
            topic: "Lead Me with Your Strength",
            wordStudy: "Psalm 46:1-3",
            confession: "The Lord will teach me to profit and lead me in the way I should go, in the name of Jesus (Isaiah 48:17).",
            fullText: <View> 
                <Text style={styles.text}>
                    In case you're wondering whether or not God wants you to succeed, the
                    answer is a resounding 'Yes!' He told the Israelites, “I am the Lord your God,
                    who teaches you to profit, who leads you by the way you should go”
                    (Isaiah 48:17 NKJV). But you'll only succeed if you know and do the right
                    things. Only as the leader grows, will the company or ministry grow. It's
                    amazing how much time, money and energy we spend on things that can't
                    produce growth. Logos, websites, brochures and slogans are all important, but
                    they'll never make up for incompetent leadership. So what should you do?
                    Once you know you are walking in God's will and your private life is in order,
                    the keys to success are <Italic>priorities</Italic> and <Italic>concentration.</Italic>
                    <Br single />
                    John Maxwell offers the following guidelines:
                </Text>

                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            <Bold>Focus 70% of your energy on developing your strengths.</Bold> Effective
                            leaders who reach their potential spend more time on what they do well
                            than on what they do badly.
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            <Bold>Focus 25% on new things.</Bold> If you want to get better you have to keep
                            changing and improving. That means stepping out into new areas. If you
                            dedicate time to new things related to your strong areas, you'll grow as a
                            leader
                        </Text>
                    },
                    {
                        serial: 3,
                        body: <Text>
                            Focus 5% on areas of weakness. Nobody can entirely avoid working in
                            their areas of weakness. (Note: We're not talking here about sin or
                            character weaknesses that must be dealt with.) The key is to delegate to
                            gifted people the things that you're not particularly good at. That way you
                            are free to concentrate in the areas of your God given strengths.
                        </Text>
                    },
                ]} />
            </View>,
            prayerPoints: [
                "Lord, make me an effective leader, in the name of Jesus.",
                "Father, raise effective leaders for the Church and our nation, in the name of Jesus.",
                "I refuse to be held back by my past, in the name of Jesus.",
            ]
        },
    ],
    June: [
        {
            day: 1,
            topic: "Your Child's Love",
            wordStudy: "Ephesians 6:4",
            confession: "My children shall be for signs and wonders in the name of Jesus (Isaiah 8:18).",
            fullText: <Text style={styles.text}>It's easy to complain about the things you don't have and overlook, or take for
                granted, the things you do, simple things that enrich your life! Maybe this
                story by Dr James Dobson will help change your perspective:
                <Br />
                “Some time ago a friend of mine punished his three-year-old daughter for
                wasting a roll of gold wrapping paper. Money was tight and he became upset
                when she tried to decorate a box to put under the Christmas tree. Nevertheless,
                the little girl brought the gift to her father the next morning and said, 'This is
                for you, Daddy.' He was embarrassed by his earlier over-reaction. But his
                anger flared again when he opened the box and found it empty. He shouted at
                her, 'Don't you know that when you give someone a present, there's supposed
                to be something inside?' The little girl looked up at him in tears and said, 'Oh,
                Daddy, it's not empty. I blew kisses into it. I filled it with my love and I
                wrapped it up just for you.' He was crushed. Quickly he put his arms around
                her, hugged her and asked for her forgiveness.
                <Br />
                “My friend told me that he kept that gold box by his bed for years, and
                whenever he got discouraged, he'd take out an imaginary kiss and remember
                the love of the child who had put it in there. In a very real sense, each of us
                parents has been given a gold container filled with the unconditional love of
                our children. There's not a more precious possession anyone could hold.”
            </Text>,
            prayerPoints: [
                "Bless God, in the name of Jesus, for the lives of our children.",
                "My children shall be taught of the Lord and great shall be their peace, in the name of Jesus.",
                "My children shall be for signs and wonders, in the name of Jesus.",
            ]
        },
        {
            day: 2,
            topic: "Shamgar's Secret (1)",
            wordStudy: "Judges 3:27-31",
            confession: "I shall not dismiss the day of little beginnings; the Lord shall make me great, in the name of Jesus.",
            fullText: <Text style={styles.text}>The Bible says, <Bold>“…Shamgar… killed six hundred… Philistines with
                an ox goad; and he also delivered Israel” (Judges 3:31 NKJV).</Bold> Shamgar
                had been just a farmer who owned a field, but he was willing to fight for it.
                His odds weren't good: 600 to 1. But he beat the odds, saved his field, and
                delivered Israel. So you can start out small and end up blessing others
                greatly. The Bible says, <Bold>“Do not despise these small beginnings, for the
                Lord rejoices to see the work begin” (Zechariah 4:10 NLT).</Bold> Every oak
                tree started as an acorn.
                <Br />
                Tom Monaghan started Domino's Pizza in 1960 with one little shop. He
                struggled for eight years. When his shop burned to the ground, the insurance
                company paid him only one cent on the dollar to cover his losses. All he
                knew was pizza, so he started another shop. He worked 100 hours a week. Up
                to this point he had only taken one weeklong holiday for his honeymoon. By
                1971 he was $1.5 million in debt. But he stayed in his field, which was the
                pizza business, and tried something new. He limited his menu to pizza only,
                and decided to deliver it hot and fresh to customers at no extra charge. It
                worked.
                <Br />
                By 2007, Domino's Pizza had grown to 6,100 stores across the United
                States and other countries. Today he's one of the richest men in the nation,
                and he gives most of his profits to charity. What was his secret? He started
                where he was. He fought for what he believed to be his God-given purpose.
                And when he got knocked down, he got back up.
            </Text>,
            prayerPoints: [
                "Lord, in the name of Jesus, thank You for Your purpose for creating me.",
                "Lord, empower me to fulfill Your purpose or my life, in the name of Jesus.",
                "Any power attacking my destiny and purpose, die, in the name of Jesus.",
            ]
        },
        {
            day: 3,
            topic: "Shamgar's Secret (2)",
            wordStudy: "Judges 5:1-7",
            confession: "The Lord shall fight for me and I shall hold my peace, in the name of Jesus.",
            fullText: <Text style={styles.text}>Shamgar lived in dangerous times. Philistine gangs roamed the countryside,
                robbing people: <Bold>“In the days of Shamgar… people avoided the main
                roads; and travellers stayed on… pathroads” (Judges 5:6 NLT).</Bold>
                Shamgar's only weapon was an ox goad, a long wooden staff with a steel tip
                used to prod oxen. But it wasn't until he came under attack that he discovered
                its full potential. Understand this: God has given you an “ox goad,”
                something you can use to win in the situation you're facing. If you seek Him,
                He'll show you what it is. Like the boy with the five loaves and two fishes,
                God will take something small, bless it, then multiply it to bless others.
                <Br />
                The Bible says we are to <Bold>“be instant in season, out of season” (2
                Timothy 4:2 KJV).</Bold> The Greek word for <Italic>be instant</Italic> could be translated “be
                prepared.” The Greek word <Italic>for season</Italic> means “opportunity.” You need to be
                preparing yourself now, when it looks like nothing is happening, because
                your season will change and the opportunity to act will suddenly present
                itself. When it does, you must be ready. In life, opportunities are either
                coming towards you, or passing you by. In college, a professor told Dr.
                Martin Luther King Jr that if he kept using such lofty words, he would never
                be a very effective public speaker. You have to wonder what that professor
                thought as he listened to Dr. King's “I have a dream” speech, and watched
                him go on to champion civil rights. What's your ox goad? Use it, and God will
                bless it!
            </Text>,
            prayerPoints: [
                "Lord, open my eyes to discover my potentials, in the name of Jesus.",
                "Lord, empower my talents and giftings for productivity to Your glory, in the name of Jesus.",
                "My potential shall be a blessing to my generation, in the name of Jesus.",
            ]
        },
        {
            day: 4,
            topic: "Do Others See Jesus in You?",
            wordStudy: "2 Corinthians 3:1-4",
            confession: "I receive the mind of our Lord Jesus Christ, in the name of Jesus.",
            fullText: <Text style={styles.text}>Bill was a drunk, converted in a Skid Row mission. Before his conversion, he
                had gained a reputation as a tramp and an alcoholic for whom there was no
                hope. But when he gave his life to Christ everything changed. He became the
                most caring person at the mission. He spent his days there doing whatever
                needed to be done. There was never anything he was asked to do that he
                considered beneath him. Whether it was cleaning up vomit left by some sick
                alcoholic or scrubbing toilets used by men who had left them filthy, Bill did it
                all with a heart of gratitude. He could be counted on to feed any man who
                wandered in off the streets, undress him and tuck him into bed when he was
                incapable of taking care of himself.
                <Br />
                One evening after the mission director delivered his evangelistic
                message to the usual crowd of solemn men with drooped heads, one of them
                came down to the altar and kneeled to pray, crying out for God to help him
                change. The repentant drunk kept shouting, “O God, make me like Bill! Make
                me like Bill! Make me like Bill!” The director of the mission leaned over and
                said, “Wouldn't it be better if you prayed, 'Make me like Jesus'?” After
                thinking about it for a few seconds, the man looked up and asked, “Is He like
                Bill?”
                <Br />
                Live, so that others see Jesus in you!
            </Text>,
            prayerPoints: [
                "Thank You, Lord, for the salvation of my soul, in the name of Jesus.",
                "Every character failing and defect in me making me to misrepresent Jesus, die, in the name of Jesus.",
                "Holy Spirit, make my life reflect the name of Christ, in the name of Jesus.",
            ]   
        },
        {
            day: 5,
            topic: "Secrets",
            wordStudy: "Psalm 51:1-7",
            confession: "Purge me with hyssop, and I shall be clean: wash me, and I shall be whiter than snow in the name of Jesus (Psalm 51:7).",
            fullText: <Text style={styles.text}>Lisa Whittle says, “Pornography… excessive shopping… [and] eating
                disorders develop in isolation… What we watch… think… how we spend our
                time are the 'secret places' satan uses. It's not that we don't want to be honest…
                but fear of being discovered keeps us [trapped]… If satan can get us to bury
                our secret[s] we can't be effective for God… our private sins will eat away at
                us, making us feel fraudulent and unworthy… The truth is we're the ones who
                don't love and accept ourselves, not God! <Bold>'What is hidden He brings forth to
                light' (Job 28:11 NKJV)</Bold> because He's after truth from the inside out.”
                <Br />
                So, practise honesty. When you catch yourself telling a lie, stop, admit it
                and apologize. It's embarrassing, but you'll think twice next time! Develop a
                strong faith. Make authenticity a top priority. Strengthen your walk with God
                through daily prayer and Bible-reading. Consider your motivation. Make
                sure the improvements you're working on in your life aren't just to impress
                others. Appreciate your God-given attributes. Make a list, stop and look at it
                when you feel “less than.” Value your own judgment. Consider what you've
                already accomplished and the good decisions you've made in the past.
                <Br />
                Dr Edwin Locke says, “Think independently… be mindful of what's
                suitable for you regardless of what the rest of the world says.” Stop trying to
                be perfect or look super spiritual. Openness means living without pretence
                and giving others permission to be transparent. Remember, seemingly
                “together people” have their not-so-together moments, and falling apart is
                sometimes how you get put back together. Live with a genuineness others will
                want to emulate. Honesty facilitates honesty. When you're real, people can
                tell.
            </Text>,
            prayerPoints: [
                "Lord, deliver me from the lusts of the flesh, in the name of Jesus.",
                "Father, let not sin have dominion over me, in the name of Jesus.",
                "I receive grace to walk with God after the order of Enoch, in the name of Jesus.",
            ]
        },
        {
            day: 6,
            topic: "Set Free from the Fear of Dying",
            wordStudy: "2 Corinthians 15:51-57",
            confession: "I shall not die, but live to declare the glory of the Lord (Psalm 118:17) in the name of Jesus (Psalm 118:17).",
            fullText: <Text style={styles.text}>Joe Bayly, who lost three sons, says, “At the hospital [after] we buried our
                five-year-old… I saw a little boy playing on the floor. 'He has the same
                problem your son had,' the secretary said. I sat down next to his mother. 'It's
                hard bringing him here for tests, isn't it?' I said. 'Hard! I die every time,' her
                voice trailed off. 'It's good to know,' I spoke slowly, choosing my words
                carefully, 'that, although the medical outlook is hopeless, after our children
                die, they'll be removed from suffering and be well and happy.' She replied, 'If
                only I could believe that, but I don't. I'll just have to cover him up with dirt
                and forget I ever had him.'”
                <Br />
                Bayly wanted to be alone with his grief, but he felt compelled to speak.
                “I'm glad I don't feel that way. We covered our little boy with dirt yesterday,
                and I'm here today to thank the doctor for his kindness.” She replied, “You
                look like a rational person. How can you possibly believe the death of a man
                or a little boy is any different from the death of an animal?”
                <Br />
                Max Lucado writes, “Heaven enjoys a maternity-ward reaction…
                Angels watch the way grandparents monitor delivery-room doors… they
                can't wait to see the new arrival. While we're driving hearses, they're hanging
                streamers… Jesus came to <Bold>'free all who lived as slaves to the fear of dying'
                (Hebrews 2:15 NLT).</Bold> Heaven knows no untimely death. David said,
                <Bold>'Before I was born, every day of my life was recorded in Your book'
                (Psalm 139:16 NLT).</Bold> Fear and dread end when you know that Heaven is
                your true home.”
            </Text>,
            prayerPoints: [
                "Bless God for the salvation of your soul.",
                "Father, let every agenda of sudden death against me and my family be aborted, in the name of Jesus.",
                "Today, I renounce and reject every covenant of sudden death, in the",
            ]
        },
        {
            day: 7,
            topic: "Get Up and Go Again",
            wordStudy: "I Kings 18:41-46",
            confession: "The joy of the Lord shall be my strength, in the name of Jesus.",
            fullText: <Text style={styles.text}>Elijah the prophet told King Ahab that the drought in the land was over and the
                rains were coming. Then Elijah told his servant, <Bold>“Go up now, look toward
                the sea.” So he went up and looked, and said, “There is nothing.” And
                seven times he said, “Go again.” Then it came to pass the seventh time, he
                said, “There is a cloud, as small as a man's hand, rising out of the sea!” So
                he said, “Go up, say to Ahab, 'Prepare your chariot, and go down before
                the rain stops you'” …In the meantime…the sky became black… and
                there was a heavy rain…” (1 Kings 18:43-45).</Bold>
                <Br />
                Notice, seven times Elijah said, <Bold>“Get up and go again.”</Bold> You must
                persist! If you don't, you'll miss what God wants you to have. Furthermore, if
                you don't recognize and celebrate small miracles “the size of a man's hand,”
                you'll miss the bigger ones so essential to your future. Beth Anne DeCiantis
                had to complete a 42-kilometre marathon in less than two hours and 45
                minutes to qualify for the Olympics. She reached the final straight in 2 hours
                43 minutes, with just two minutes left. Two hundred metres from the finish
                she stumbled and fell. Dazed, she stayed down for 20 seconds. The crowd
                yelled, “Get up!” The clock was ticking—less than a minute to go. Beth Anne
                staggered to her feet and began walking. Five metres short of the finish line,
                with ten seconds to go, she fell again. As the crowd cheered her on, she
                crossed the finish line on her hands and knees. Her time? Two hours, 44
                minutes, 57 seconds. So the word for you today is: “Get up and go again.”
            </Text>,
            prayerPoints: [
                "Let the anointing of God fall upon me, in the name of Jesus.",
                "I receive strength to rise to higher grounds, in the name of Jesus.",
                "I receive grace to finish well in all my projects, in the name of Jesus.",
            ]
        },
        {
            day: 8,
            topic: "Know Your Life's Vision",
            wordStudy: "Romans 8:28-30",
            confession: "For as many as are led by the Spirit of God, they are the sons [and daughters] of God (Romans 8:14).",
            fullText: <View> 
                <Text style={styles.text}>
                    Nobody can give you a vision for your life, but here's how to find yours:
                </Text>

                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>Look within you. What moves you? What are you excited about? Paul
                            said, <Bold>“I am compelled to preach…” (1 Corinthians 9:16 NIV).</Bold>
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>Look behind you. What has life taught you? Experience provides us with
                            the wisdom needed to fulfil our destiny. What does your past tell you
                            about your future?
                        </Text>
                    },
                    {
                        serial: 3,
                        body: <Text>Look around you. God never calls us alone. Moses needed 70 elders.
                            Jesus picked 12 disciples. Paul spoke of those who worked with him.
                            Fulfilling your destiny requires having the right people in your life.
                        </Text>
                    },
                    {
                        serial: 4,
                        body: <Text>Look ahead of you. Helen Keller said, “The only thing that's worse than
                            being blind, is having sight but no vision.” What do you see through the
                            eye of faith? St. Augustine said, “Faith is to believe what we do not see,
                            and the reward of faith is to see what we believe.”
                        </Text>
                    },
                    {
                        serial: 5,
                        body: <Text>Look beside you. What resources are available to you, for example,
                            books, CDs, conferences and mentors? If your vision is not bigger than
                            you, it's not of God. And the greater it is, the more resources it will
                            require.
                        </Text>
                    },
                    {
                        serial: 6,
                        body: <Text>Look above you. Jeremiah writes, <Bold>“…They speak a vision of their own
                            heart, not from the mouth of the LORD (Jeremiah 23:16 NKJV).</Bold> We
                            are “called according to His purpose” (Romans 8:28), not according
                            to the need, or the fact that the door is open, or that our talent will be
                            appreciated and well rewarded. Your vision must fulfil your Godordained
                            destiny, nothing else.
                        </Text>
                    },
                ]} />
                <Text style={styles.text}>
                    So, do you need to recognise your vision, resurrect your vision or refocus
                    on your vision?
                </Text>
            </View>,
            prayerPoints: [
                "Lord, help me to know Your vision and purpose my life, in the name of Jesus.",
                "Let the power of resurrection fall upon me today, in the name of Jesus.",
                "Lord, empower me to fulfill Your purpose for my life, in the name of",
            ]
        },
        {
            day: 9,
            topic: "It's Only Money",
            wordStudy: "I Timothy 6:6-10",
            confession: "The Lord shall satisfy my desires, in the name of Jesus.",
            fullText: <Text style={styles.text}>A lot of us think that the goal of work is to accumulate enough money so that
                we can quit working. No, if your work is not fulfilling, eventually you'll
                resent it. That's so, whether you make much or little. Money alone is not
                worth giving your life to. John Ortberg writes, “I have a friend. He's a
                businessman. He is in his seventies now. Years ago I attended his church
                when he gave a terrific message. We talked afterwards and he said to me, 'You
                know, when I was a young man I always felt that I ought to go into pastoral
                ministry.' I asked him, 'Why didn't you?'
                <Br />
                “Well, when it came right down to it, it was just money. He was doing
                well in the business world so he didn't follow what he sensed was God's
                calling on his life. Now he was financially secure—but he was running out of
                time. It's only money! We get all twisted up in knots over it, stay awake at
                night thinking about it, stew, get anxious, scheme, trade away our integrity to
                get a little more of it… and we worry that somebody else might get hold of it.
                It's only money! It's never a reason not to follow Christ. It's never a reason not
                to do the thing that He has called you to do.”
                <Br />
                Billy Graham hit the bull's eye when he said, “If a person gets his attitude
                toward money straight, it will help straighten out almost every other area of
                his life.”
                <Br />
                It's okay to make money, and enjoy money, but <Bold>“Keep your [life] free
                from the love of money.”</Bold>
            </Text>,
            prayerPoints: [
                "Father, make me heavenly conscious, in the name of Jesus.",
                "The love of money shall not have dominion over me, in the name of Jesus.",
                "I shall not lack good things, in the name of Jesus.",
            ]
        },
        {
            day: 10,
            topic: "Following in the Footsteps of Jesus",
            wordStudy: "1 Peter 2:21-25",
            confession: "Lead me in thy truth, and teach me: for thou art the God of my salvation; on thee do I wait all the day (Psalm 25:5).",
            fullText: <View> 
                <Text style={styles.text}>
                    If you want to follow in the footsteps of Jesus, observe the following things
                    about Him:
                </Text>

                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            <Italic>He never acted in haste.</Italic> He didn't make decisions in response to the
                            pressure tactics of others. Skilled negotiators know that waiting is a
                            weapon; whoever is the most hurried usually ends up with the short end
                            of the deal. Waiting reveals the weakness in any plan, plus the motives of
                            those around you. Your greatest mistakes will often happen because of
                            impatience, so think long term.
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            He knew when to work and when to rest. In the storm on the Sea of
                            Galilee the disciples sweated, but Jesus slept. In the garden of
                            Gethsemane the disciples slept, but Jesus sweated. That's because He
                            knew when to work and when to rest. Knowing when to act and when to
                            trust, what to give your attention to and what to leave in God's hands is a
                            secret you must learn if you're to do God's will—and not burn out.
                            Nobody was busier than Jesus. Everybody wanted something from Him.
                            The more successful you are the more people will reach for you. Jesus
                            separated Himself in order to receive. He understood that you can't give
                            what you don't have. Work means giving; rest means receiving. Jesus
                            understood the balance; that's why He accomplished so much in three
                            short years.
                        </Text>
                    },
                ]} />
                <Text style={styles.text}>
                    When you are rested you think more clearly, you make better decisions,
                    you see life through confident eyes, you accomplish more in less time, and
                    what you build is built to last. So stop your frantic rush. Following in the
                    footsteps of Jesus means being led, not driven!
                </Text>
            </View>,
            prayerPoints: [
                "Father, take over the affairs of my life, in the name of Jesus.",
                "I shall not live a wasted life, in the name of Jesus.",
                "Father, take me to higher grounds, in the name of Jesus.",
            ]
        },
        {
            day: 11,
            topic: "Appreciate Your Wife",
            wordStudy: "Proverbs 31:10-26",
            confession: "Thank You, Lord, for the favour of a good wife.",
            fullText: <Text style={styles.text}>Do you have <Italic>any</Italic> idea how hard your wife works to be a good mother?
                Imagine this:
                <Br />
                Six dads are dropped on a desert island with one car and three kids each
                for six weeks. Each child will play two sports and take music or dance
                lessons. There's no fast food, and every man has to correct homework, help
                with science projects, cook meals, do laundry, budget for groceries, pay the
                bills without enough money, know the birthdays of friends and relatives and
                send cards. In addition, he has to take each kid for haircuts and to doctor and
                dentist appointments, bake cakes for school functions, plant flowers and keep
                his home presentable at all times.
                <Br />
                He can only watch TV after the kids are in bed and his chores are done,
                and then he must have enough energy to be intimate with his spouse at a
                moment's notice. He should be well-groomed, go to church at least once a
                week, read to his kids, pray with them every night, pack their lunches and
                favourite snacks, fix breakfast, make sure they are dressed and on the school
                bus by 8.00 am.
                <Br />
                At the end of six weeks every guy will be tested on his child's height,
                weight, shoe size, favourite colour, song, drink, toy and their biggest fear.
                And here's the best part: the winner gets to play the game over and over again
                for the next eighteen to twenty-one years! So, do you still think you want to
                change places with your wife?
                <Br />
                The Bible says: <Bold> “The husband must give his wife the same sort of love
                that Christ gave to the Church, when He sacrificed himself for her”
                (Ephesians 5:25 PHPS) because “a good wife is worth more than rubies”
                (Proverbs 31:10 NCV).</Bold>
            </Text>,
            prayerPoints: [
                "Bless God for your wife.",
                "Let the joy of the Lord be released upon my home, in the name of Jesus.",
                "I reject the strange woman in my life and home, in the name of Jesus.",
            ]
        },
        {
            day: 12,
            topic: "Don't Lose Your Peace of Mind",
            wordStudy: "Isaiah 26:1-4",
            confession: "The peace of God that surpasses all understanding shall surround me, in the name of Jesus.",
            fullText: <View> 
                <Text style={styles.text}>
                    We lose our peace of mind for four reasons:
                </Text>

                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            <Italic>We try to change the people in our lives.</Italic> As you grow wiser you begin to
                            realize that you can't change other people, only God can! And He does,
                            when you back off, and love them as they are. This doesn't mean
                            agreeing with everything they do. It means committing to love them
                            regardless, claiming God's promises on their behalf and allowing Him
                            to deal with them His way, in His time and for His glory. The reason
                            you're stressed out may be because you keep trying to do something
                            about something you can't do anything about!
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            <Italic>We try to make things happen when it's not the right time.</Italic> <Bold>“There is a
                            time for everything…” (Ecclesiastes 3:1NIV).</Bold> If you've raised
                            children, you know that one of their chief characteristics is impatience;
                            they can't wait for anything. God wants us to outgrow our childishness
                            so He makes us wait, trust, and mature!
                        </Text>
                    },
                    {
                        serial: 3,
                        body: <Text>
                            <Italic>We get upset because we're not progressing fast enough.</Italic> You can slow
                            down your spiritual growth through neglect, but ultimately, <Bold>“…we all…
                            are being changed… [by] the Spirit” (2 Corinthians 3:18 NCV).</Bold> So
                            learn to enjoy your life while God works on your problems, for you'll
                            always have problems!
                        </Text>
                    },
                    {
                        serial: 4,
                        body: <Text>
                            <Italic>We push ourselves harder and harder.</Italic> We do what we think God wants
                            without consulting Him as to what He actually wants, when He wants it,
                            or how He wants it done. As a result we wear ourselves out. What's the
                            solution? <Bold>“You will keep him in perfect peace, whose mind is
                            [focused] on you, because he trusts in you” (Isaiah 26:3 NKJV).</Bold>
                        </Text>
                    },
                ]} />
            </View>,
            prayerPoints: [
                "Let the peace of God reign in my life, in the name of Jesus.",
                "Let the joy of the Lord be my strength, in the name of Jesus.",
                "Holy Spirit, help me to serve the Lord to the end, in the name of Jesus.",
            ]
        },
        {
            day: 13,
            topic: "Live Caleb's Way (1)",
            wordStudy: "Joshua 14:10-14",
            confession: "The grace of the Lord shall abound for me, in the name of Jesus.",
            fullText: <Text style={styles.text}>At the ripe old age of eighty-five, Caleb said to Joshua, <Bold>“…The Lord has
                kept me alive… just as my strength was then, so now is my strength for
                war… Give me this mountain of which the Lord spoke in that day… And
                Joshua blessed him, and gave Hebron to Caleb… because he wholly
                followed the LORD…” (Joshua 14:10-14 NKJV).</Bold> There was nothing halfbaked
                or half-hearted about Caleb. His dream kept him alive; he got out of bed
                every morning, intent on pursuing it.
                <Br />
                When God gives you a dream as big as a mountain, it will keep you going
                while others around you are giving up. But be careful who you listen to. Don't
                let the critics discourage you by saying, “You're too old.” The Bible says that
                like a palm tree, you can produce your greatest harvest of fruit in your final
                years <Bold>(Psalm 92:12-14).</Bold> When the sun goes down the stars come out, so you
                can shine brightest in the closing chapters of your life.
                <Br />
                Barbara Klassen says: “My great-great uncle lived to one hundred and
                six. He was healthy and spry and took joy in chauffeuring his less able-bodied
                senior friends around town. On his hundredth birthday his driver's licence
                came up for renewal. When he went to the licensing bureau, a sceptical clerk
                said, 'You're a hundred years old! What do you need a driver's licence for?' My
                uncle, completely nonplussed, replied, <Italic>'Somebody</Italic> has to drive the old folks
                around!' He continued to have a legal driver's licence for the next five years.”
                <Br single/>
                Do it Caleb's way: live right up to the moment you die!
            </Text>,
            prayerPoints: [
                "I shall not die before my ordained time, in the name of Jesus.",
                "My destiny shall not be cut short, in the name of Jesus.",
                "Lord, strengthen me all the days of my life, in the name of Jesus.",
            ]
        },
        {
            day: 14,
            topic: "Live Caleb's Way (2)",
            wordStudy: "Numbers 13:30-33",
            confession: "With men it may not be possible, but not with God, for with God all things shall be possible for me, in the name of Jesus (Mark 10:27).",
            fullText: <View> 
                <Text style={styles.text}>
                    Caleb said, <Bold>“…Let us go up at once and take possession [of the Promised
                    Land], for we are well able… But the men… with him… gave… a bad
                    report…, saying, …There we saw the giants… and we were like
                    grasshoppers in our own sight…” (Numbers 13:30-33 NKJV).</Bold> Notice
                    two attitudes at work in this story:
                </Text>

                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            <Italic>An attitude of doubt.</Italic> Ten of twelve spies came back saying, “It can't be
                            done!” But the majority isn't necessarily right. If God promises you
                            something, it's yours, even if you're in the minority. The majority report
                            terrified God's people; they got spiritual amnesia; they forgot all about
                            God's supernatural provision. They talked themselves into believing
                            that Egypt was actually the land of milk and honey. “…You have
                            brought us up out of a land flowing with milk and honey, to kill us in the
                            wilderness” <Bold>(Numbers 16:13 NKJV).</Bold> Incredible! Egypt was a land of
                            straw and slavery, not milk and honey. What was their problem? They
                            allowed their surroundings to influence them more than God's
                            promises. When that happens, you dry up spiritually, begin to grumble
                            and say stuff like, “God may have worked miracles in the past, but He
                            doesn't do it anymore.” As a result of their unbelief, not one of them
                            entered the Promised Land—except Joshua and Caleb.
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            <Italic>An attitude of faith.</Italic> God said, <Bold>“But My servant Caleb, because he has
                            a different spirit in him and has followed Me fully, I will bring into
                            the land… and his descendants shall inherit it” (Numbers 14:24
                            NKJV).</Bold>
                        </Text>
                    },
                ]} />
                <Text style={styles.text}>
                    So, which attitude do you have?
                </Text>
            </View>,
            prayerPoints: [
                "Father, grant me the grace to possess my possessions, in the name of Jesus.",
                "I receive grace to follow Christ to the end, in the name of Jesus.",
                "Holy Spirit, possess me afresh, in the name of Jesus.",
            ]
        },
        {
            day: 15,
            topic: "Your Prayer Carries Weight with God",
            wordStudy: "Genesis 20:3-7",
            confession: "The Lord will pay attention to my cry, in the name of Jesus.",
            fullText: <Text style={styles.text}>In the Old Testament Abraham journeyed south. When he reached Gerar we
                read that he “said of his wife Sarah, <Bold>'She is my sister.'</Bold> Then Abimelech king of
                Gerar sent for Sarah and took her” <Bold>(Genesis 20:2 NIV).</Bold> Can you imagine how
                Sarah felt, trapped and alone in the palace, contemplating what lay ahead at
                the hands of her captor? And what's worse, her husband, the man she trusted
                with her life, let it happen! We're not privy to how Sarah prayed that night. But
                she must have touched the heart of God because He <Bold>“came to Abimelech in a
                dream… and said, 'The woman you have taken… is… married… return
                the man's wife… and you will live… if… not… you… will die” (Genesis
                20:3-7 NIV).</Bold> God's directives are always clear; there's never any
                ambivalence about what He says. <Bold>“The king's heart is in the hand of the
                Lord; He directs it… wherever He pleases” (Proverbs 21:1 NIV).</Bold>
                <Br />
                That means authority figures who don't even know you exist, have to stop
                and listen when God speaks, because to Him <Bold>“one man is not different from
                another” (Romans 2:11 AMP).</Bold> So what can you do when you're helpless to
                change a situation you didn't create? Or when someone you loved and trusted
                lets you down? Pray! Instead of giving in to bitterness or fear, cry out to God.
                He will hear you like He heard Sarah. How can you be sure? Because His
                Word says, <Bold>“The earnest prayer of a righteous person has great power and
                produces wonderful results.”</Bold> No matter how bad things may look right now,
                and they looked pretty bad for Sarah that night, your prayers carry weight with
                God.
            </Text>,
            prayerPoints: [
                "Father, revive my prayer altar, in the name of Jesus.",
                "Let my prayer life receive fire, in the name of Jesus.",
                "Remove every hindrance to my prayers, in the name of Jesus.",
            ]
        },
        {
            day: 16,
            topic: "Show Kindness",
            wordStudy: "Romans 12:9-18",
            confession: "Father, help me to live peaceably with all people, in the name of Jesus",
            fullText: <Text style={styles.text}>Sometimes we defend our lack of love, kindness, gentleness, patience, and
                all those other characteristics of the “fruit of the Spirit” in the name of
                productivity and hard work. But we can't square it with Christ's teachings. He
                said, <Bold>“whatever you want [others] to do to you, do also to them”
                (Matthew 7:12 NKJV).</Bold> When was the last time you went out of your way to
                help somebody, expecting nothing in return? Mother Teresa said, “A day
                lived without doing something good for others is a day not worth living.”
                Loving people must be a way of life, a fixed attitude, a commitment we make
                every day. William Barclay said, “More people have been brought into the
                church by the kindness of real Christian love than by all the theological
                arguments in the world. And more people have been driven from the church
                by the hardness and ugliness of so-called Christians than by all the doubts in
                the world.”
                <Br />
                Have you ever stopped to think that all those little irritations which come
                your way each day are just God giving you an opportunity to become more
                Christ-like? Years ago, Chuck Swindoll wrote: “What does the Lord do to
                help broaden my horizons and assist me in seeing how selfish I am? Very
                simple: He gives me four busy kids who step on toes, wrinkle clothes, spill
                milk, lick car windows and drop sticky candy on the carpet… Being unselfish
                in attitude strikes at the very core of our being. It means we are willing to
                forego our own comfort, our own preferences, our own schedule, our own
                desires for another's benefit. And that brings us back to Christ.
            </Text>,
            prayerPoints: [
                "I receive the grace to be a channel of blessing to others, in the name of Jesus.",
                "Let the glory and beauty of God manifest in my life, in the name of Jesus.",
                "Holy Spirit, help me to mortify every work of the flesh in my life, in the name of Jesus.",
            ]
        },
        {
            day: 17,
            topic: "Don't Judge",
            wordStudy: "Romans 8:33-34",
            confession: "The grace of the Lord shall be sufficient for me, in the name of Jesus.",
            fullText: <Text style={styles.text}>When you tear someone down, you're on thin ice with God. The Bible says,
                <Bold>“Who dares accuse us whom God has chosen for His own? Will God? No!
                He is the one who has given us right standing with Himself… Will Christ
                Jesus? No, for He is the one who died for us and was raised to life for us
                and is sitting… next to God, pleading for us” (Romans 8:33-34 NLT).</Bold>
                Your fellow-believers are not perfect, but God says they <Bold>“belong to his dear
                Son” (Ephesians 1:6 NLT).</Bold> There's <Italic>nothing</Italic> you can bring against them that
                God doesn't already know. Stop and think: by discrediting them you're
                questioning the One who redeemed them, implying He made a mistake and
                doesn't know what He's doing. You say, “But shouldn't I speak up when
                something is wrong?” Yes, but be careful about overstepping your bounds
                and condemning the <Italic>person.</Italic>
                <Br />
                Your attitude should be one of helpfulness, forgiveness and
                reconciliation. Anytime you try to judge what you've no authority over,
                you're out of your jurisdiction! Paul writes, <Bold>“Who are you to judge someone
                else's servant? To his own master he stands or falls. And he will stand, for
                the Lord is able to make him stand” (Romans 14:4 NIV).</Bold> It's God's job to
                judge others and He doesn't need your help to do it!
            </Text>,
            prayerPoints: [
                "Lord, help me to bridle my tongue, in the name of Jesus.",
                "Father, silence all my adversaries, in the name of Jesus.",
                "Lord Jesus, baptize me with the spirit of forgiveness.",
            ]
        },
        {
            day: 18,
            topic: "Legalism or Grace (1)",
            wordStudy: "Galatians 2:15-21",
            confession: "Grace be to me and peace from God the Father, and from the Lord Jesus Christ (2 Corinthians 1:2).",
            fullText: <Text style={styles.text}>Imagine going to the emergency room and being asked to leave because you're
                bleeding all over the floor! Jesus encountered that legalistic mindset when He
                healed a crippled woman on the Sabbath. <Bold>“Indignant…, the synagogue ruler
                said…, 'There are six days for work.' So come and be healed on those
                days…” (Luke13:14 NIV).</Bold> Observing the law was more important to him
                than the people he was supposed to care for.
                <Br />
                Max Lucado says: “All…religions can be placed in one of two camps:
                legalism or grace. A legalist believes if you look right, speak right and belong
                to the right group, you will be saved… The outside sparkles… but something's
                missing… Joy. What's there instead? Fear that you won't do enough.
                Arrogance that you've done enough. Failure that you've made a mistake.
                Legalism is …slow suffocation of the spirit, amputation of one's dreams…
                enough religion to keep but not nourish you… Your diet is rules and standards.
                Legalism… doesn't need God… It's the search for innocence, not
                forgiveness… a systematic process of defending… explaining… exalting…
                justifying… [It] turns my opinion into your burden.
                <Br />
                “There's only room for one opinion… and guess who's wrong? [It] turns
                my opinion into your boundary. Your opposing opinion makes me question
                not only your right to fellowship with me, but your salvation. It turns my
                opinion into your obligation. Christians must toe the company line. Your job
                isn't to think, it's to march… Salvation is God's business. Grace is his idea, his
                work, and his expense. He offers it to whomever he desires, when he desires.
                Our job is to inform… people, not screen [them].”
                <Br />
                Paul writes, <Bold>“Do not set aside…grace…; for if righteousness comes
                through the law, … Christ died in vain” (Galatians 2:21 NKJV). </Bold>Thank
                God: <Bold>“He saved us because of His mercy…, not…good deeds we did”
                (Titus 3:5 NCV).</Bold>
            </Text>,
            prayerPoints: [
                "Let the grace of God speak good things in my life, in the name of Jesus.",
                "Father, take me to higher grounds in my walk with you and works of righteousness, in the name of Jesus.",
                "Lord, grant me grace to enjoy the joy of my salvation.",
            ]
        },
        {
            day: 19,
            topic: "Legalism or Grace (2)",
            wordStudy: "Romans 8:38-39",
            confession: "The Lord shall give me total victory over sin, in the name of Jesus.",
            fullText: <Text style={styles.text}>Rigid adherence to a list of do's and don'ts appeals to our pride and 
                self-sufficiency by fostering the myth that if we work hard enough we can earn
                God's favour. That's fear-based thinking, and <Bold>“God has not given us a spirit
                of fear…” (2 Timothy 1:7 NLT). “There is no fear in love. …Fear has to do
                with punishment…” (1 John 4:18 NIV).</Bold> Legalism is fear that God isn't big
                enough to forgive your sins; that unless you do the right thing in the right way
                at the right time and do it perfectly, you're in trouble. Jon Walker writes:
                “When we fear making mistakes we become timid, and limit ourselves from
                living abundantly. We let…analysis permeate our decisions as we lead quiet,
                desperate, anti-faith lives, afraid to move with the bold confidence that grace
                gives us to walk in uncertainty…unafraid of rejection.”
                <Br />
                Speaking against works-based religion, Martin Luther said, “Be a sinner,
                and let your sins be strong, but let your trust in Christ be stronger… rejoice in
                Christ… the victor over sin.” No, Luther wasn't excusing sin! He was
                restoring grace to its rightful place, affirming that nothing can separate us
                from God's love <Bold>(Romans 8:38-39).</Bold> He wasn't downgrading the law, He was
                upgrading grace. Grace means talking to God and listening for His voice
                when it would be easier to just consult the rule book. The truth is, when
                <Bold>“[Jesus] entered…Heaven…to appear…before God on our behalf”
                (Hebrews 9:24 NLT),</Bold> He freed us to have a relationship with Him without
                fear of sin separating us.
            </Text>,
            prayerPoints: [
                "I receive grace to overcome fear, in the name of Jesus.",
                "Father, let sin not have dominion over my life according to Your Word, in the name of Jesus.",
                "Grace of the Lord Jesus Christ, abound and speak for me always, in the name of Jesus.",
            ]
        },
        {
            day: 20,
            topic: "Get into God's Word",
            wordStudy: "Acts 17:10-15",
            confession: "Only let your conversation be as it becometh the gospel of Christ: that whether I come and see you, or else be absent, I may hear of your affairs, that ye stand fast in one spirit, with one mind striving together for the faith of the gospel (Philippians 1:27).",
            fullText: <Text style={styles.text}>The Devil doesn't mind you paying lip-service to God's Word as long as you
                don't take the time to read and study it. How long have you been talking about
                reading your Bible? When are you going to get around to doing it and
                applying its principles in your everyday life?
                <Br />
                Dr. Howard Hendricks writes: “Anybody can come up with a grandiose
                scheme for change. One person says he wants to reach the world for Christ.
                Somebody else wants to study every book in the Bible over the next five
                years. Somebody else plans to memorize a hundred verses. Somebody else is
                going to become a Christ-like spouse. Wonderful! When are you going to
                begin? Until you answer that, all you have is good intentions. Those have
                about as much value as a worthless cheque.
                <Br />
                “After all, what good does it do to dream of reaching the world with the
                Gospel if you can't share Christ with the person in the office next to you?
                How are you going to study the entire Bible when you don't even know what
                verse you're going to study tomorrow? How can you memorize a hundred
                verses when you've never even tried to memorize one? Rather than fantasize
                about a Christ-like marriage, why not start with something simple such as
                doing the dishes if you're a husband, or encouraging your husband if you're a
                wife? Too much application stays at the level of good intentions because we
                talk about the end of the journey without specifying when, where, and how
                we're going to take the first step. As someone has well said, 'We don't plan to
                fail, we fail to plan.'”
            </Text>,
            prayerPoints: [
                "I receive grace to read, study, memorize, meditate on and live by the Word of God, in the name of Jesus.",
                "Holy Spirit, open the eyes of my understanding, in the name of Jesus.",
                "Open my heart to receive the Word of God, in the name of Jesus.",
            ]
        },
        {
            day: 21,
            topic: "Guiding Your Kids through the Tough Years (1)",
            wordStudy: "Isaiah 54:13-15",
            confession: "My children shall be for signs and wonders, in the name of Jesus.",
            fullText: <View> 
                <Text style={styles.text}>
                    Are you raising a teenager? Welcome to the tough years! There's nothing
                    wrong with you, you're just parenting an adolescent. You say, “But they are
                    just ten, this craziness shouldn't be happening yet!” Sorry, but now they
                    develop faster! Puberty hits them between ten and twelve years of age, and
                    learning how this accelerated genetic mix functions is vital to good parenting.
                    So here are some helpful updates:
                </Text>

                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            <Italic>Some of your old ways aren't likely to work anymore.</Italic> What worked with
                            young kids frequently fails with older ones. Do you remember when
                            raising your voice to your seven-year-old brought instant obedience? Try
                            that with your hormone-charged teen and get ready for battle! Teenage
                            chemistry defies the old logic, so learn what makes them tick, pray for
                            grace and respond based on what works, not what doesn't. If you treat
                            teens like pre-teens you will get nowhere!
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            <Italic>What didn't come naturally, can be learned.</Italic> Those “model parents” you
                            heard about are either understating it, enjoying a short-term break, or
                            they earned their stripes the hard way. It's not easy. You learn to do it well
                            by first doing it poorly, then doing it better, then asking God to do what
                            you can't. And He will!
                        </Text>
                    },
                    {
                        serial: 3,
                        body: <Text>
                            <Italic>Your only unforgivable mistake is the one you won't acknowledge.</Italic> Your
                            children know you're not flawless and they can handle it. They also know
                            how big you have to be to admit it, and they're quick to forgive. So forget
                            modelling perfection; just show them, humbly and constructively, how
                            to handle it when they've been imperfect!
                        </Text>
                    },
                ]} />
            </View>,
            prayerPoints: [
                "My children shall not be bewitched, in the name of Jesus.",
                "Father, let my children grow in grace, in the name of Jesus.",
                "Grant me wisdom to train and nurture my children in the knowledge of You, in the name of Jesus.",
            ]
        },
        {
            day: 22,
            topic: "Guiding Your Kids Through the Tough Years (2)",
            wordStudy: "Ecclesiastes 9:11-18",
            confession: "I receive grace for wisdom from above, in the name of Jesus.",
            fullText: <Text style={styles.text}>Parent, you influence your child more than anyone else! You say, “But they
                seem to pay little or no attention to me, and lots to their friends!” Peers are
                very influential, and if you're dismissive or judgmental with them you'll just
                increase your child's negative peer pressure. Your teenager's resistance isn't
                necessarily saying they disagree with your opinions or think you're wrong;
                they're just experiencing two powerful, life-shaping, natural tendencies.
                <Br />
                <Italic>First,</Italic> for healthy social growth they need peer relations. Appearing
                “cool” to friends promotes those relations, so they'll seem to downplay your
                influence. Don't take it personally; it's about them and their growing needs,
                not about you.
                <Br />
                <Italic>Second,</Italic> normal development is pushing them towards becoming more
                independent. When they push back, it's usually more about this than about
                rebellion or even substantial disagreement with you. These God-designed
                inclinations prepare them to “leave father and mother and become one flesh”
                with someone else for life (Genesis 2:24). In fishing, “you let out the line or
                risk breaking the rod and losing the catch.”
                <Br />
                Discover the natural flow of teenage development and work with, not
                against it! Try to redirect your teen by helping them find constructive ways to
                express their new autonomy and you'll help them harness it the right way.
                What you think or believe, isn't the problem; how you handle it with them is.
                Remaining rational, loving, affirming, prayerful and patient invites
                cooperation rather than confrontation. In God's timing they'll embrace your
                values, beliefs and attitudes. <Bold>“…When he [or she] is old, he will not depart
                from it” (Proverbs 22:6 NKJV).</Bold>
            </Text>,
            prayerPoints: [
                "Father Lord, teach me to train my children in Your way, in the name of Jesus.",
                "Father, let Your talents and giftings in the life of my children begin to manifest to the praise of Your name,, in the name of Jesus.",
                "Lord, baptize me with wisdom from above.",
            ]
        },
        {
            day: 23,
            topic: "Guiding Your Kids Through the Tough Years (3)",
            wordStudy: "Proverbs 2:5-7",
            confession: "For the LORD giveth wisdom: out of his mouth cometh knowledge and understanding (Proverbs 2:6).",
            fullText: <Text style={styles.text}>If you're serious about changing your child, work on changing yourself!
                Trying to change others usually doesn't work anyway. Adolescents perceive
                such efforts as saying or meaning, “You're not acceptable to me.” That's a
                perfect way to turn them into freedom-fighters whose cause become resisting
                your efforts to change them. The best way to change someone is to change
                how you handle them. When you change your steps, the entire dance routine
                changes because you've changed what they're responding to. If what you're
                doing isn't working, stop it and do something that will! You don't have to
                come up with the perfect solution at first. Just stopping the tired, frustrating
                dance of conflict will improve your environment, decrease stress and
                opposition, and help make way for a more effective strategy. Nagging only
                makes your kids ask, “Why are you always on my back?”, diverting them
                from the real issues. Decreasing tension, while affirming your child's value to
                you, increases your likelihood of success.
                <Br />
                The adolescent mind is wired differently. They're not crazy, they're just
                dealing with rapidly changing chemistry. There was a time when we thought
                that by five years of age the brain was finished changing. Were we ever
                wrong! We know now its most sophisticated development happens
                throughout adolescence. In the emerging teen, brain neurons fire off
                spontaneously, without warning or conscious reasons, leaving your child
                overwhelmed by feelings they don't understand and haven't yet learned to
                control. So they behave irrationally, inconsistently, unpredictably,
                irritatingly. Your job is to try to understand this and become a calming
                influence. The “craziness” will pass. In the meantime, pray. <Bold>“Wisdom is
                better than strength” (Ecclesiastes 9:16a NKJV).</Bold>
            </Text>,
            prayerPoints: [
                "Father, let my children give me peace, in the name of Jesus.",
                "Lord, make me a good example and role model to my children, in the name of Jesus.",
                "My children shall be a source of blessing to their generation, in the name of Jesus.",
            ]
        },
        {
            day: 24,
            topic: "Never Betray a Confidence",
            wordStudy: "Proverbs 11:11-13",
            confession: "Some trust in chariots, and some in horses: but we will remember the name of the LORD our God.",
            fullText: <View> 
                <Text style={styles.text}>
                    We all need a shoulder to cry on. When we're battling habits and hang-ups we
                    need a safe place to go, confident we'll be loved, understood, supported and
                    prayed for. If those who are hurting can't find these qualities in church, where
                    are they supposed to go? The betrayal of a confidence is a terrible sin. “But
                    what I said was true,” you object. So what? <Bold>“A talebearer reveals secrets,
                    but he who is of a faithful spirit conceals a matter” (Proverbs 11:13
                    NKJV).</Bold> Note the words <Italic>reveals</Italic> and <Italic>conceals.</Italic>
                    <Br />
                    The Hippocratic Oath says: “Whatsoever I shall see or hear in the course
                    of my profession, I will never divulge, holding such things to be holy
                    secrets.” That oath is taken by physicians and other professionals in positions
                    of trust. But it ought to be binding on every one of us! How would you feel if
                    your doctor, counselor, minister or a trusted friend violated your confidence
                    and broadcast your holy secrets? Hurt? Betrayed? The longer we live the
                    more we realize there's a severe shortage of people who can be trusted to keep
                    their mouths shut, and the more we value them. If you were asked to define a
                    person of integrity, wouldn't the ability to keep a confidence be close to the
                    top of your list? So here are some ground rules to live by:
                </Text>

                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>Instead of talking, pray about it;
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>Instead of criticizing, look for something good;
                        </Text>
                    },
                    {
                        serial: 3,
                        body: <Text>Instead of showing anger, show grace.
                        </Text>
                    },
                ]} />
                <Text style={styles.text}>
                    How you handle others determines how God will handle you!
                </Text>
            </View>,
            prayerPoints: [
                "Lord, grant me grace to be faithful in all ways, in the name of Jesus.",
                "Holy Spirit, help me to bridle my tongue, in the name of Jesus.",
                "Father, let my trust and confidence be in You always, in the name of Jesus.",
            ]
        },
        {
            day: 25,
            topic: "P-R-A-Y",
            wordStudy: "1 Chronicles 29:11-13",
            confession: "Seek the LORD, and his strength, seek his face continually (1 Chronicles 16:11).",
            fullText: <Text style={styles.text}>To help you remember the four parts of prayer, think of the acrostic <Bold>P-R-A-Y:</Bold>
                <Br />
                <Bold>P – P</Bold>raise the Lord! Not sure how? Think you'll run out of words? Not if you
                use the Scriptures. David gives us a beautiful example in <Bold>1 Chronicles
                29:11-13.</Bold> It's one you can use: “Yours, O Lord, is the greatness and the power
                and the glory and the majesty and the splendour… Yours, O Lord, is the
                Kingdom… In Your hands are strength and power to exalt and give strength
                to all. Now, our God, we give You thanks, and praise Your glorious name.”
                <Br />
                <Bold>R – R</Bold>epent of your sins! Just as heat forces impurities to the surface so the
                metal refiner can remove them, your prayer time will reveal attitudes that
                must be changed, habits that must be broken, and barriers to blessing that
                must be removed. It's not enough to tell God about your sins: He already
                knows them. You must ask Him to help you turn away from them. This is true
                repentance.
                <Br />
                <Bold>A – A</Bold>sk for yourself and others! Your prayers invite God into the situation,
                and your faith activates His power to change it. There's no distance in prayer,
                no culture or language barrier it can't overcome. It's like turning on a power
                switch; things begin to move when we pray. Jesus said, “I will give you the
                keys of the Kingdom of Heaven… whatever doors you open on earth
                shall be open in Heaven” (Matthew 16:19 TLB).
                <Br />
                <Bold>Y – Y</Bold>ield yourself to God's will! Declaring the Lordship of Jesus Christ in
                your life is like signing your name to your biography, and inviting Him to
                write your life's story.
            </Text>,
            prayerPoints: [
                "Bless the name of the Lord for His covenant-keeping faithfulness.",
                "Father, hear me every time I call upon Your name, in the name of Jesus.",
                "Lord Jesus, take over my life and make it a trophy of your grace, wisdom, power and love.",
            ]
        },
        {
            day: 26,
            topic: "Use Your Gift",
            wordStudy: "1 Corinthians 12:1-11",
            confession: "For as many as are led by the Spirit of God, they are the sons of God (Romans 8:14).",
            fullText: <Text style={styles.text}>One of the great benefits of finding and fulfilling your life's calling is that it
                settles the question of what constitutes true success. Many of us have a faulty
                definition. We've bought into the idea that success is measured by how well
                we do compared to how well others do. That's a recipe for frustration! No
                matter what you do, someone else will always do it better. But when you
                define success in terms of <Italic>God's purpose for your life,</Italic> the standard changes
                completely. True success is not what you've done, compared to what others
                have done, but what you've done, compared to what God assigned you to do:
                <Bold>“A spiritual gift is given to each of us as a means of helping the entire
                church” (1 Corinthians 12:7 NLT).</Bold> Jesus said He was successful because
                He accomplished the work His Father had sent Him to do <Bold>(John 17:4).</Bold> Paul
                could say at the end of his life, <Bold>“I have finished my course…” (2 Timothy
                4:7 KJV).</Bold>
                <Br />
                By this standard, success may mean leaving a lucrative job to follow
                God's call. It may mean using your talents for His glory, instead of chasing
                fame and fortune. Whatever it is, once you know you're in your calling, you
                can stop comparing yourself to others or wishing you were someone else.
                The Bible says we are each given gifts <Bold>“for the common good” (1
                Corinthians 12:7 NAS).</Bold> Only when you're using your gifts to bless others
                will you experience true satisfaction. Pay cheques and promotions are good,
                but they can't take the place of divine purpose. Only in your calling, will you
                experience lasting joy.
            </Text>,
            prayerPoints: [
                "Lord, grant me grace to fulfill my destiny in You, in the name of Jesus.",
                "I shall not live a wasted life; Your will shall be done in my life, in the name of Jesus.",
                "Father, make me a blessing to my generation, in the name of Jesus.",
            ]
        },
        {
            day: 27,
            topic: "Balancing Family and Career",
            wordStudy: "1 Timothy 3:4-5",
            confession: "The Lord shall deliver me from every evil work, in the name of Jesus.",
            fullText: <Text style={styles.text}>Balancing family and career calls for tough, unselfish choices. If you make the
                right ones, you'll look back with joy, not regret. John Ortberg writes: “A friend
                of mine is a professional musician. For many years he made his living on the
                road. He was becoming increasingly successful. Then three years ago, he
                became a father. He was on the road about half the time. He realized that when
                his daughter was about a year old, she hardly knew him. He knew he needed to
                make a change, but it was frightening to him. What if his career slowed to a
                crawl? What if being home more actually made life harder? He took a job as
                the head of a music department at a university. He still performs, but he travels
                now only a fraction of the time. His relationship with his daughter has become
                a source of pride and joy in his life that he otherwise never would have known.
                He did have to let go of some of his old dreams, but he has since recorded a
                bestselling CD and been nominated for a Grammy. Most importantly, he
                realises his daughter will grow up a fundamentally different human being
                now—than she would have if she had grown up with a hole in her heart where
                her father was supposed to be. By the end of his life he will have a title that
                means much more to him than <Italic>Rock Star.</Italic> The title is Dad! The Bible says a
                leader <Bold>“must manage his own family well” (1 Timothy 3:4 NIV).</Bold> And that
                doesn't just apply to leaders, it applies to all of us!”
            </Text>,
            prayerPoints: [
                "Father, grant me the wisdom to manage my home, in the name of Jesus.",
                "Let my home experience the peace of God, in the name of Jesus.",
                "Lord, deliver my family from every evil work, in the name of Jesus.",
            ]
        },
        {
            day: 28,
            topic: "How to Prepare Yourself (1)",
            wordStudy: "Isaiah 43:18-19",
            confession: "Behold, I will do a new thing; now it shall spring forth; shall ye not know it? I will even make a way in the wilderness, and rivers in the desert (Isaiah 43:19).",
            fullText: <View> 
                <Text style={styles.text}>
                    Look at the instructions Naomi gave Ruth for approaching Boaz, her future
                    husband, and you'll see that there's a certain protocol involved in walking with
                    God. Once you understand it, the things you've been waiting for begin to
                    happen. So:
                </Text>

                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text><Italic>Be sure it's God's will for you.</Italic> Ruth wasn't looking for just any man, she
                            had a specific one in mind. And because Naomi had done her homework,
                            she was able to tell Ruth where to find him: <Bold>“He is winnowing barley
                            tonight at the threshing floor” (Ruth 3:2 NKJV).</Bold> Research what you
                            want from God before you start claiming things in prayer. Be sure it's
                            what He wants too! If your name's not on it, don't pursue it. Don't go after
                            something because it looks good in someone else's life. God has a plan
                            for you, one that's unique and specific. Seek Him and He will reveal it to
                            you.
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text><Italic>Deal with your past.</Italic> Naomi said to Ruth, <Bold>“Wash yourself…” (Ruth 3:3
                            NKJV).</Bold> In order to gain acceptance with Boaz, Ruth couldn't approach
                            him looking and smelling like Moab, the famine-stricken place she'd
                            come from. She needed to settle her past so it didn't sabotage her future.
                            God will open the door for you, but until you've resolved your old issues
                            you won't be able to walk through it. You can't receive what He has for
                            you now if you're still contaminated by what you went through then.
                            Whether it takes six months or six years, sort out your emotional
                            baggage. God says: <Bold>“Forget the former things… I am doing a new
                            thing… I am making a way…” (Isaiah 43:18-19 NIV).</Bold>
                        </Text>
                    },
                ]} />
            </View>,
            prayerPoints: [
                "Every power resisting my progress, die, in the name of Jesus.",
                "My life shall move forward and upward, in the name of Jesus.",
                "Let the name of the Lord be glorified in my life, in the name of Jesus.",
            ]
        },
        {
            day: 29,
            topic: "How to Prepare Yourself (2)",
            wordStudy: "Romans 12:1-2",
            confession: "And be not conformed to this world: but be ye transformed by the renewing of your mind, that ye may prove what is that good, and acceptable, and perfect, will of God (Romans 12:2).",
            fullText: <View> 
                <Text style={styles.text}>
                    In order to prepare Ruth for meeting Boaz, her future husband, Naomi taught
                    her two important principles about succeeding in life:
                </Text>

                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text><Italic>You must have the right attitude.</Italic> Naomi said to Ruth, <Bold>“Anoint
                            yourself…” (Ruth 3:3 NKJV).</Bold> In Bible times people anointed
                            themselves with oil in order to be refreshed and renewed. So Ruth was,
                            in essence, adopting the right attitude. If you're praying for a good job or
                            a suitable partner or about an outcome in a particular area but it hasn't
                            happened yet, don't automatically blame satan: check your attitude. <Bold>“Let
                            God re-mould your minds from within so that you [move]…
                            towards true maturity” (Romans 12:2 PHPS).</Bold> To get the right result,
                            you need the right approach.
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text><Italic>You must be willing to stand out in the crowd.</Italic> Naomi instructed Ruth,
                            <Bold>“Put on your best garment…” (Ruth 3:3 NKJV).</Bold> But why get dressed
                            up for something that hasn't happened yet? Because you need to be
                            prepared! When your time comes you must be ready. Ruth's story
                            teaches us that it's those who are willing to stand out in the crowd who
                            get noticed. Any time you dress for where you're going, there's a good
                            chance you'll look out of place where you are. That's okay. Your highest
                            priority should be God's approval, not man's. You must know you have a
                            definite destination, otherwise you'll be tempted to make excuses and try
                            to explain why you're so different from everyone else. When you know
                            where God is taking you, you won't care. The truth is, when others look
                            at your preparation they should be in no doubt as to your destination.
                        </Text>
                    },
                ]} />
            </View>,
            prayerPoints: [
                "Father, sanctify my heart for Your service, in the name of Jesus.",
                "Father, use me for Your glory, in the name of Jesus.",
                "Father, empower me to serve You with all my strength, in the name of Jesus",
            ]
        },
        {
            day: 30,
            topic: "How to Prepare Yourself (3)",
            wordStudy: "Ruth 3:1-6",
            confession: "The Lord shall grant me favour in His sight, in the name of Jesus.",
            fullText: <View>
                <Text style={styles.text}>
                    Observe two more things Naomi taught Ruth, in preparation for meeting
                    Boaz:
                </Text>

                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text><Italic>Make sure you're in the right place.</Italic> Naomi told Ruth, <Bold>“Go down to the
                            threshing floor” (Ruth 3:3 NKJV).</Bold> Why? Because that's where Boaz
                            was! To receive what God has for you, you must be in the right place
                            spiritually. Satan will tell you you're unworthy. He will try to convince
                            you to stay where you are and to listen to those who'd keep you from
                            where God wants you to be. He will make you feel out of place even
                            when you're in the right place. Don't believe his lies; when God calls
                            you He equips you, empowers you, and uses you for His glory.
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text><Italic>Understand the importance of timing.</Italic> <Bold>“Do not make yourself known
                            to the man until…” (Ruth 3:3 NKJV).</Bold> Ruth had waited a long time for
                            this moment; now she had to learn to be quiet because the person God
                            planned to bless her through was sleeping. It's hard to be all keyed up
                            about something nobody else is excited about; you want to get them
                            excited too. But sometimes God says, “Wait.” Stop working to make
                            things happen before their time! Don't try to promote yourself. <Bold>“The
                            vision is yet for an appointed time…Though it tarries, wait for it;
                            because it will surely come” (Habakkuk 2:3 NKJV).</Bold> God may not
                            respond when you think He should, but His timing is perfect. He has
                            blessings with your name on them, and no matter how many others want
                            them, when the time is right He will give them to you.
                        </Text>
                    },
                ]} />
            </View>,
            prayerPoints: [
                "Father, connect me to my destiny helpers, in the name of Jesus.",
                "I shall reach my goal in life and ministry for Your glory, in the name of Jesus.",
                "I will possess my possessions, in the name of Jesus.",
            ]
        },
    ],
    July: [
        {
            day: 1,
            topic: "Fresh Oil",
            wordStudy: "Acts 10:38",
            confession: "O Lord, anoint my head with fresh oil, in the name of Jesus.",
            fullText: <Text style={styles.text}>
                The shepherd anoints his sheep with oil for two purposes. First, to repel
                insects. If allowed to deposit their eggs into the soft membrane of a sheep's
                nose they can drive the sheep insane; the ewes stop giving milk and the lambs
                stop growing. So the shepherd anoints his sheep with an oil-like repellent that
                keeps the enemy at bay and the flock at peace. Second, to heal wounds. Most
                wounds result from living with the flock. So the shepherd regularly inspects
                his sheep because he doesn't want today's wound to become tomorrow's
                infection.
                <Br />
                Whether circumstances are driving you crazy, or you've just been
                wounded as a result of living with the flock, always go to the Shepherd. Before
                you go to anybody else, go to God because <Bold>"He heals the broken-hearted
                and binds up their wounds" (Psalm 147:3 NIV).</Bold> Then, bow before Him. In
                order to be anointed, the sheep must lower their heads and let the shepherd
                apply the oil. Humble yourself, call on the Lord. <Bold>"God will always give what
                is right to His people who cry to Him... He will not be slow to answer
                them" (Luke 18:7 NCV). The Psalmist writes, "I have been anointed with
                fresh oil" (Psalm 92:10b NKJV).</Bold> Yesterday's oil is quickly contaminated by
                the dirt and grime of the journey, so you must go to God daily and ask Him to
                empower you with His Spirit. A sheep doesn't understand how the oil works,
                but it doesn't have to. It's enough to know that something happens in the
                presence of its shepherd that happens nowhere else.
            </Text>,
            prayerPoints: [
                "Lord, baptize me with the Holy Ghost and power.",
                "The power of God to do exploits, my life is available: fall upon me, in the name of Jesus.",
                "The presence of God will not depart from my life, in the name of Jesus.",
            ]
        },
        {
            day: 2,
            topic: "When You Are Disappointed",
            wordStudy: "Psalm 42:1-6",
            confession: "O my God, make haste for my help, in the name of Jesus (Psalm 71:12).",
            fullText: <View>
                <Text style={styles.text}>
                    Max Lucado says, "When God doesn't do what we want it's not easy. Never
                    has been. Never will be. But faith is the conviction that God knows more than
                    we do... and He'll get us through. Disappointment is caused by unmet
                    expectations... [and] is cured by revamped expectations... Don't panic...
                    don't give up... be patient... God's in control. It ain't over till it's over." So,
                    when you're disappointed: 
                    <Br />
                </Text>

                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            Look inward. David asked, "Why am I discouraged? Why is my heart
                            so sad?" (Psalm 42:5 NLT). Admit how you feel. You can't deal with
                            what you don't acknowledge. Ask God to show you the root of the
                            problem. Is it pent-up anger? Envy? Unforgiveness? Pride? Lust?
                            Physical and mental fatigue? Be open to what He reveals.
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            Look upward. David said, "I will… hope in God! I will praise Him..." 
                            (v. 5). Instead of focusing on your discouragement, focus on the One
                            who knows the way out.
                        </Text>
                    },
                    {
                        serial: 3,
                        body: <Text>
                            Remember God's past faithfulness. David said, "I am… discouraged,
                            but I will remember You" Through every trial Joseph clung to the
                            assurance that God still controlled his destiny. Recalling God's
                            faithfulness builds your confidence that He'll continue to provide.
                        </Text>
                    },
                    {
                        serial: 4,
                        body: <Text>
                            Remember, you don't have to understand. Just because you can't figure
                            out what God's doing right now, doesn't mean it won't make sense later.
                            He "causes all things to work together for good to those who love
                            [Him]..." (Romans 8:28 NAS).
                        </Text>
                    },
                    {
                        serial: 5,
                        body: <Text>
                            Don't give in to bitterness. When your hopes are dashed, resentment can
                            set in. "If God is for us, who can be against us?" (Romans 8:31
                            NKJV). Regardless of how deep the pit may seem, God is on your side!
                        </Text>
                    },
                ]} />

            </View>,
            prayerPoints: [
                "Bless God, in the name of Jesus, for the salvation of your soul.",
                "I receive grace to overcome every disappointment, in the name of Jesus.",
                "Father, strengthen me in the journey of life, in the name of Jesus.",
            ]
        },
        {
            day: 3,
            topic: "Make Allowance (1)",
            wordStudy: "Colossians 3:8-15",
            confession: "As the elect of God, I put on bowels of mercies, kindness, meekness and longsuffering, in the name of Jesus.",
            fullText: <Text style={styles.text}>
                When it comes to forgiving, you cant say it better than Tim Stafford did: "I
                would rather be cheated a hundred times, than develop a heart of stone." The
                Bible says, "Make allowance for each others faults, and forgive anyone
                who offends you" (v. 13). You dont get to choose who youll forgive. Love is a
                command, forgiveness is an act of obedience. "He has given us this
                command: Whoever loves God must also love his brother" (1 John 4:21
                NIV). You can't be closer to God than you are to the people you love least. God
                sets the bar high because grudges are like cancer, and forgiveness is the laser
                that removes them. Bitterness chains us to the past, destroys families, divides
                churches and sours relationships. Forgiveness is the key that unlocks the
                handcuffs of hate.
                Take your hurt feelings to God and say, "Point out anything... that
                offends you..." (Psalm 139:24 NLT). It takes courage, but that's the kind of
                prayer He answers. It gets easier as you grow in Christ; in the meantime you
                have to work at it. As a child of God His Spirit lives inside you. You're no
                longer a slave to sin (Romans 6:14). God knows it's hard to forgive deep
                seated hurts, but He will give you grace to do it. "To make allowance" means
                to "take a charitable view and consider extenuating circumstances.” People
                change and grow over time, so don't insist on clinging to a limited, outdated
                view of them. Try to see them as they are today. Most folks are doing their best
                based on the knowledge and understanding they have now, so give them a
                break!
            </Text>,
            prayerPoints: [
                "Appreciate God for your marriage.",
                "Lord, let the peace of God rule and reign in my marriage, in the name of Jesus.",
                "I reject evil attack against my marriage, in the name of Jesus.",
            ]
        },
        {
            day: 4,
            topic: "Make Allowance (2)",
            wordStudy: "Colossians 3:9-14",
            confession: "Forbearing one another, and forgiving one another, if any man have a quarrel against any: even as Christ forgave you, so also do ye (Colossians 3:13 KJV).",
            fullText: <Text style={styles.text}>
                C.S. Lewis said, "Everyone says forgiveness is a lovely idea until they have
                something to forgive." Author Mac Anderson writes, "A few times in my life
                I've been wronged. My first reaction… was anger and resentment. I felt my
                stomach tie up in knots, my appetite wane, and the joy slip out of my life like
                I'd played the first half of a basketball game in steel shoes. In the locker room
                the coach said, 'Try these new Nikes in the second half.' Multiply that by ten
                and you'll understand how it feels to unload your emotional baggage through
                the power of forgiveness."
                <Br />
                Jesus debunked the myth that love is based on feelings. It's an act of your
                will, and if you love God, you'll keep His commandments. It's that simple. If
                you're struggling to forgive someone who's hurt you, here are some scriptural
                guidelines to help you.
                <Br />
                Don't seek retribution. Instead, <Bold>"overcome evil with good" (Romans
                12:21 NKJV).</Bold> God said, <Bold>"Vengeance is mine; I will repay" (Romans 12:19
                NKJV).</Bold> Don't rush to judgment. That's not always easy when you're the
                injured party, but the ultimate Judge says, <Bold>"With what judgment you judge,
                you will be judged" (Matthew 7:2 NKJV).</Bold> Make things right. Jesus says,
                <Bold>"...be reconciled to your brother" (Matthew 5:24 NIV).</Bold> Don't wait for the
                other person to make the first move, you do it. "Make every effort to live in
                peace with all men..." (Hebrews 12:14 NIV). Pray for the offender. As much
                as it goes against your grain, when you <Bold>"pray for those who mistreat you"
                (Luke 6:28 NIV),</Bold> God gives you the grace to forgive and see them through
                His eyes.
            </Text>,
            prayerPoints: [
                "Appreciate God for your marriage.",
                "Lord, let the peace of God rule and reign in my marriage, in the name of Jesus.",
                "I reject evil attack against my marriage, in the name of Jesus.",
            ]
        },
        {
            day: 5,
            topic: "Time Out",
            wordStudy: "Genesis 24:61-65",
            confession: "Confession: As I cast my burden upon the Lord, he will sustain me and he will never allow me to be moved, in the name of Jesus (Psalm 55:22).",
            fullText: <Text style={styles.text}>
                A hiker with two heavy holdall was walking down a road in 35° heat. A truck
                driver stopped and told him to hop on. Later when the driver looked in his
                rear-view mirror, the hiker was standing in the truck-bed still holding his
                bags! We smile, but as Jon Walker points out, many of us stand in the truck of
                faith still carrying our burdens, thinking they are independent of the ride we
                are taking. We think God can carry us but not our burdens... The Psalmist
                says, <Bold>"Cast your burden on the Lord, and He shall sustain you" (Psalm
                55:22a).</Bold> Rest in His power and grace... confident He is looking out for your
                best interests. Rest requires dependence, and one sign that you are not resting
                in God is worry, the need to control. Next time you feel overwhelmed, settle
                into a chair. <Bold>"Be still, and know that [He is] God" (Psalm 46:10).</Bold> In this
                instance, the command to be still here comes from a Hebrew word which
                means "to let go, release, lift up."
                <Br />
                You have struggled long enough; give your burden to the Burden Bearer.
                Set it down! In order to do that you must do two things. First: practise saying
                no. If you are constantly available you will end up stretched so thin you will
                be no good to anybody. Jesus didn't try to please everybody. He prayed, got
                God's agenda and stuck to it. God will give you the grace to do only what He
                has commanded, not what you think or what others would like. Second, value
                solitude. Turn off your pager and mobile phone. The sign of a well-ordered
                mind is the ability to sit quietly in one place and linger in your own company.
            </Text>,
            prayerPoints: [
                "Father, I thank You for Your lovingkindness towards me, in the name of Jesus. ",
                "Take care of my burden, in the name of Jesus.",
                "Manifest as God in my situation, in the name of Jesus.",
            ]
        },
        {
            day: 6,
            topic: "\"But God\" Moment",
            wordStudy: "Genesis 45:5-11",
            confession: "The steps of a good man are ordered by the LORD: and he delighteth in his way (Psalm 37:23).",
            fullText: <Text style={styles.text}>
                Are you in the dark about the direction God seems to be leading you? Jon
                Walker says, 'The choice to do His will before we know the specifics...
                develops trust of His character... This is where many people fall into a battle
                of wills... wanting Him to reveal His plan first... That's not how He works.
                God wants you to decide in advance to trust Him, ...believing that His will is
                best for your life... He knows your hesitancy. He won't be surprised if you
                say, 'I don't know that I'm willing to step out in faith without knowing
                everything that's going to happen, but I'm willing to be made willing.' One
                reason God doesn't give you the full picture... is you may be overwhelmed...
                it may appear impossible, ...but that's the point—there's no way you can fulfil
                your mission without Him." 
                <Br />
                When Joseph was reunited with his brothers in Egypt, he told them, <Bold>"Do
                not be angry with yourselves... it was not you who sent me here, but
                God..."</Bold> Joseph's brothers sold him out, “but God” used it to preserve the
                Jewish race. The Israelites were in slavery, “but God” sent Moses to lead them
                to the Promised Land. Jesus died on the cross, “but God” used His death to
                save a lost world. Oswald Chambers calls God “The Great Engineer, creating
                circumstances to bring about moments of divine importance, leading us to
                divine appointments." 
                <Br />
                Think back: Can't you recall some “but God” moments of your own
                where you'd reached the end of your rope and He came through for you?
            </Text>,
            prayerPoints: [
                "Thank God, in the name of Jesus, for His divine direction",
                "Lord, order my steps, in the name of Jesus.",
                "Lord, open my eyes to know Your agenda for my life, in the name of Jesus.",
            ]
        },
        {
            day: 7,
            topic: "Walking in Purity",
            wordStudy: "Malachi 3:1-4",
            confession: "LORD, purify and purge me as gold and silver that I may offer unto you offerings in righteousness (Malachi 3:3).",
            fullText: <Text style={styles.text}>
                When gold is refined over extreme heat the first thing to come to the surface is
                dross. The next thing to be separated from it is silver, a less precious metal
                that blends with the raw gold ore. There's an important lesson here. Most of
                us are unable to separate the good from the best, so Malachi writes, “He will
                purify… and refine them like gold.” God does it, because we don't know
                how! After committing adultery with Bathsheba, David writes, “Create in
                me a clean heart, O God, and renew a steadfast spirit within me” (Psalm
                51:10 NKJV). David prays for two things: a clean heart and a steadfast desire
                to walk in purity.
                <Br />
                Personal failure is usually the result of a slow, steady build-up, rather
                than a single act of disobedience. It happens when we focus on the wrong
                things and neglect our spiritual life. So God calls us to the place of solitude, a
                place with no distractions, in order to probe our deepest thoughts and open
                our eyes to certain issues that need attention. It is here He makes us aware of
                what we excuse, or try to hide from others. It's here the junk we've collected
                during the busy hours of our day gets filtered out. With the debris out of the
                way, we're able to see things more clearly and respond to God's nudgings.
                There's no way to have a deeper, more intimate relationship with God
                without the discipline James talks about: “Draw near to God and He will
                draw near to you. Cleanse your hands… and purify your hearts…”
                (James 4:8).
            </Text>,
            prayerPoints: [
                "Father, purify me from every work of the flesh, in the name of Jesus.",
                "I receive grace to draw close to God, in the name of Jesus.",
                "I reject every form of failure in my life, in the name of Jesus.",
            ]
        },
        {
            day: 8,
            topic: "Are You Struggling Financially? (1)",
            wordStudy: "Psalm 40:1-6",
            confession: "I have been young, and now am old; yet have I not seen the righteous forsaken, nor his seed begging bread (Psalm37:25).",
            fullText: <Text style={styles.text}>
                For many who've lost their jobs or who don't earn enough to live on, debt has
                become a harsh reality. So much so, that they're afraid to even think about their
                situation. But burying your head in the sand isn't the answer. With God's help
                and some common-sense suggestions you can start moving towards financial
                freedom. A respected financial consultant offers these suggestions. First, be
                truthful. Before you can change anything you need to know where your
                money's going. There's nothing to be ashamed of; like an alcoholic entering
                recovery, the first step is admitting you have a problem. On a card jot down
                every dollar you spend for 30 days. It adds up quickly, “all those little foxes…
                ruin the vineyard” <Bold>(Song of Solomon 2:15 NLT).</Bold> By looking at the big
                picture, you'll see areas you can cut back.
                <Br />
                Second, learn from your past experiences. If you don't learn from the past
                you'll keep repeating it. For example, if you didn't receive many Christmas
                gifts as a child, it can be tempting to overcompensate with your own kids and
                end up maxing out your credit cards. Or, growing up in an atmosphere of
                financial turmoil can make it difficult to talk about money with your spouse.
                Third, value yourself more than your payslip. Jesus said the important things
                in life aren't “things” <Bold>(Matthew 6:25) </Bold>. Beating yourself up won't help you
                earn more; it will just blind you to the real blessings in life. Remember, no
                matter how tough it gets, God is faithful. David said, <Bold>“I have never seen the
                righteous forsaken or their children begging,”</Bold> and you won't either!
            </Text>,
            prayerPoints: [
                "I receive grace to be prudent with my spending, in the name of Jesus.",
                "Doors of financial breakthrough, be opened to me, in the name of Jesus.",
                "I reject every spirit of poverty, in the name of Jesus.",
            ]
        },
        {
            day: 9,
            topic: "Are You Struggling Financially? (2)",
            wordStudy: "Matthew 6:25-34",
            confession: "Be careful for nothing; but in everything by prayer and supplication with thanksgiving let your requests be made known unto God (Philippians 4:6).",
            fullText: <Text style={styles.text}>
                Here are three more steps you can take to deal effectively with debt. First,
                start digging yourself out. It's disheartening to find bills piling up on your
                doormat every day, but you need to know the bottom line. List your debts
                from the highest interest rate to the lowest. Try to pay as much as possible
                towards the highest and the minimum towards the rest. Make a start, no
                matter how long it takes; there's too much at stake not to. Second, model
                sound financial principles for your children. Be open with them and explain
                that electricity, food, toys and clothes cost money, and that the reason you're
                not driving the latest car is because you can't afford it. Your kids are making
                money memories now, so <Bold> “teach them diligently” (Deuteronomy 6:7 NKJV). </Bold>
                <Br />
                Third, <Bold>“give, and it will be given to you” (Luke 6:38 NAS).</Bold> Suze
                Orman says, “One day I was in a terrible [state]. I turned on the TV and
                happened to catch a fundraiser. I picked up the phone and pledged as much as
                I could. Those people needed money more than me. Immediately I felt like a
                burden had been lifted. From then on whenever I felt down, I'd give… and I'd
                feel better. Amazingly in every instance the amount was showered back on
                me tenfold.” The rewards of giving aren't always financial; sometimes it's the
                sense of freedom and purpose that comes from doing it. Plus, money should
                never be so important that you can't let it go. Remember, by helping others
                you're paving the way for God to <Bold>“supply all your needs,”</Bold> and His shovel is
                much bigger than yours!
            </Text>,
            prayerPoints: [
                "Bless God for His blessings upon your life.",
                "Father, make me a channel of blessing, in the name of Jesus.",
                "Father, release the shower of blessings upon me, in the name of Jesus.",
            ]
        },
        {
            day: 10,
            topic: "Rise Above Worry",
            wordStudy: "Matthew 6:25-34",
            confession: "But my God shall supply all your need according to his riches in glory by Christ Jesus (Philippians 4:19).",
            fullText: <View>
                <Text style={styles.text}>
                    We all worry from time to time, but wise people have learned to avoid overanxiety
                    by following these guidelines.
                    <Br />
                </Text>

                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            Define the situation clearly. What exactly is it you're worrying about?
                            Be specific. Often when we take time to clarify the problem, a way to
                            solve it will present itself.
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            Face the worst that can happen. Would you lose your job? Your
                            relationship? Your investment? In most cases even if the worst did
                            happen, chances are it wouldn't ruin you. It may be inconvenient or
                            painful, but does it really warrant all the anxiety you're giving it?
                        </Text>
                    },
                    {
                        serial: 3,
                        body: <Text>
                            Resolve to accept the outcome, whatever it is. Most of the stress of
                            worry comes from denial, from not being willing to face the worst that
                            could happen. Once you make up your mind to accept whatever happens,
                            you'll find worry loses its power over you.
                        </Text>
                    },
                    {
                        serial: 4,
                        body: <Text>
                            Work to improve the situation. Renowned neurologist James H. Austin said, “Chance favours those in motion.” Do all you can to ensure
                            the best possible outcome, and “after you have done everything… stand”
                            (Ephesians 6:13 NIV). Stand on God's promises!
                        </Text>
                    },
                    {
                        serial: 5,
                        body: <Text>
                            Give the problem to God. He's “the controller of all things.” The
                            Psalmist says, “Those who know Your name will trust in You, for You,
                            Lord, have never forsaken those who seek You” (Psalm 9:10 NIV).
                            Remember, nothing can happen to you today, or any other day, that God is
                            not aware of, in control of, and able to bring you through.
                        </Text>
                    },
                ]} />

            </View>,
            prayerPoints: [
                "Grant me the grace to overcome every spirit of worry and anxiety, in the name of Jesus. ",
                "By Your Grace, Lord, let Your promises be manifested in my life, in the name of Jesus.",
                "Father, meet my needs according to Your riches in glory, in the name of Jesus.",
            ]
        },
        {
            day: 11,
            topic: "The Deal Is Sealed",
            wordStudy: "Romans 8:35-39",
            confession: "Who shall lay anything to the charge of God's elect? It is God that justifieth (Romans 8:33 KJV).",
            fullText: <View>
                <Text style={styles.text}>
                If you are having doubts about your salvation, understand this clearly: the
                moment you trusted in Christ as your Saviour, His blood cleansed you from
                all sin. At that point <Bold>“you were marked with a seal, the promised Holy
                Spirit, guaranteeing [your]… redemption” (Ephesisns 1;13-14). The
                Bible says, “You should not be like cowering fearful slaves. You should
                behave instead like God's very own children, adopted into His
                family—calling Him, 'Father, dear Father!' For the Holy Spirit speaks to
                us deep in our hearts and tells us we are God's children. And since we are
                His children, we will share His treasures—for everything God gives to
                His Son, Christ, is ours too” (Romans 8:15-16 NLT).</Bold>
                <Br />
                In the book of Ruth we read that <Bold>“the custom…in Israel concerning
                redeeming…[was] one man took off his sandal…gave it to the other, and
                this was a confirmation…” (Ruth 4:7 NKJV).</Bold> Notice, the “redeemer” put
                on another man's sandal, signifying his willingness to stand in his place.
                That's what Jesus did for us at the cross. He stood in our shoes so we could
                stand before God in Him—redeemed and righteous! Don't let doubt steal
                your joy. Refuse to go by your feelings. Make up your mind to believe what
                God says, for until you do, you'll be plagued by uncertainty. Study God's
                Word until you can say with assurance, <Bold>“I know whom I have believed and
                am persuaded that He is able to keep what I have committed to Him until
                that Day” (2 Timothy 1:12 NKJV).</Bold>
                <Br />
                Are you trusting in Christ as your Saviour? Then your salvation is not in
                doubt. The deal is sealed!
                </Text>
            </View>,
            prayerPoints: [
                "Bless God for the salvation of your salvation",
                "I will not backslide, in the name of Jesus.",
                "Lord, uphold me to the end, in the name of Jesus.",
            ]
        },
        {
            day: 12,
            topic: "What Is Your Achilles Heel?",
            wordStudy: "Hebrew 12:1",
            confession: "Father, let no sin have dominion over me, in the name of Jesus.",
            fullText: <View>
                <Text style={styles.text}>
                    Achilles, the Greek hero of the Trojan War, was invulnerable, with one
                    exception: his heel. Shoot him there and he'd go down. Guess what? He died
                    from an arrow to the heel! We all have our Achilles heel—the “sin that so
                    easily trips us up.” Here, blindness, indifference or denial sets us up for
                    failure. So how do we overcome our Achilles heel? Author John Piper offers
                    us a solution in the form of an acronym: <Bold>A-N-T-H-E-M.</Bold>
                </Text>

                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            <Bold>A: A</Bold>void  sights and situations that arouse unfitting desires. Prevent what
                            fuels your appetite for sin.
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            <Bold>N:</Bold> Say <Bold>“No”</Bold> to every lustful thought within five seconds. In the first two
                            seconds shout, “No, get out of my head!” In the next two, cry out, “O
                            God, in the name of Jesus, help me. Save me now. I am yours.” Say it out
                            loud. The Puritan writer John Owen said, “Be killing sin, or it will be
                            killing you.”
                        </Text>
                    },
                    {
                        serial: 3,
                        body: <Text>
                            <Bold>T</Bold>: <Bold>T</Bold>urn the mind forcefully towards Christ. Attack sin with the promises
                            of Christ.
                        </Text>
                    },
                    {
                        serial: 4,
                        body: <Text>
                            <Bold>H</Bold>: <Bold>H</Bold>old the promise and pleasure of Christ firmly in mind. For how
                            long? Until it pushes other images out. Hold it. Don't let it go. Hold it until
                            you win!
                        </Text>
                    },
                    {
                        serial: 5,
                        body: <Text>
                            <Bold>E</Bold>: <Bold>E</Bold>njoy a superior satisfaction, namely, pleasure in Christ. If you have
                            little taste for Jesus, competing pleasures will triumph.
                        </Text>
                    },
                    {
                        serial: 6,
                        body: <Text>
                            <Bold>M</Bold>: <Bold>M</Bold>ove into useful activity, away from idleness and other vulnerable
                            behaviours. <Bold>“Abound in the work of the Lord” (1 Corinthians 15:58).</Bold>
                            Replace deceitful lusts with a passion for good deeds.
                        </Text>
                    },
                ]}/>

                <Text style={styles.text}>
                    Today, using this simple acronym, you can move from vulnerability to
                    victory by drawing on the power of God's Spirit within you <Bold>(Galatians 5:25).</Bold>
                </Text>
            </View>,
            prayerPoints: [
                "Father, I receive grace to follow You to the end, in the name of Jesus.",
                "I will not run ahead of God, in the name of Jesus.",
                "Father, give me grace to endure to the end, in the name of Jesus.",
            ]
        },
        {
            day: 13,
            topic: "Your Shepherd and Lord",
            wordStudy: "Psalm 23:1-6",
            confession: "The LORD is my shepherd; I shall not want any good thing, fullText: in the name of Jesus (Psalm 23:1).",
            fullText: <View>
                <Text style={styles.text}>
                    Abraham Lincoln wrote, “I have been driven many times to my knees by the
                    overwhelming conviction that I had nowhere else to go. My wisdom, and that
                    of all about me, seemed insufficient for the day.” All of us feel that way at
                    times. That's because we are all sheep in need of a shepherd. Of all God's
                    creatures, sheep seem the least able to care for themselves. They're
                    directionless: they'll walk off a cliff or wander into a river and drown. They're
                    defenceless: they've no claws so they can't fight, run fast or climb a tree to
                    safety. They're dirty: your cat and dog can clean themselves, but sheep get
                    dirty and stay that way.
                    <Br />
                    You say, “I don't like being compared to sheep.” All right, then answer
                    these questions. How well do you control your impulses? Do you ever play
                    the victim? Can you relate to Dr Jekyll and Mr Hyde? Are you always upbeat
                    and upright? Do you add to the conflict and confusion, or do you always
                    make peace? Do you truly love people, or do you just use them for your own
                    ends? Are you generous, or do you mostly give to those who have something
                    to give back to you? On a scale of one to ten, how do you score when it comes
                    to the fear of people, or failure, or sickness, or death, or rejection, or risk? Is
                    your life an open book? How often do you fail and need forgiveness: weekly?
                    daily? hourly? It sounds like you need a shepherd! So instead of saying, <Bold>“The
                    Lord is my shepherd,”</Bold> try saying, “Because I need a shepherd, I make You
                    Lord of my life.”
                </Text>
            </View>,
           prayerPoints: [
                "Lord, I receive grace to follow You as the Shepherd and Guardian of my soul, in the name of Jesus.",
                "I will not run ahead of God, in the name of Jesus.",
                "Lord, You shall be my Lord to the end, in the name of Jesus.",
            ]
        },
        {
            day: 14,
            topic: "What You Really Want Is the Lord!",
            wordStudy: "Ruth 1:11-16",
            confession: "The Lord is my rock, my fortress, my provider and my deliverer, in the name of Jesus.",
            fullText: <View>
                <Text style={styles.text}>
                    Do you remember when all the stuff you now have was stuff you only
                    dreamed of? Now you realise that lasting joy doesn't come from what you
                    drive, deposit or dwell in. The words <Bold>“The LORD is my shepherd; I shall not
                    want”</Bold> teach us that what we have in God is greater than whatever else we
                    don't have. Here are two stories to help you realise that. Story One: While on a
                    short-term mission trip, Pastor Jack Hinton was leading worship at a leper
                    colony when a woman who'd been facing away from the pulpit suddenly
                    turned around. “It was the most hideous face I'd ever seen,” Hinton said. Her
                    nose and ears were entirely gone. She lifted a fingerless hand in the air and
                    asked, “Can we sing <Italic>Count Your Blessings</Italic>?” Overcome, Hinton left the
                    service. He was followed by a team member who said, “I guess you'll never be
                    able to sing that song again.” “Yes, I will,” he replied, “but I'll never sing it the
                    same way.”
                    <Br />
                    Story Two: Doug McKnight was diagnosed with multiple sclerosis at 32.
                    Over the next 16 years it would cost him his career, his mobility, and
                    eventually his life. But he never lost his sense of gratitude. When his church
                    friends asked him to compile a list of prayer requests, he responded by
                    sending them 18 blessings for which to be grateful and six concerns for which
                    to be prayerful. His blessings were three times greater than his needs! Doug
                    McKnight discovered that what he had in God was greater than whatever else
                    he didn't have in life. Have you discovered that yet?
                </Text>
            </View>,
           prayerPoints: [
                "Lord, I receive grace to follow You as the Shepherd and Guardian of my soul, in the name of Jesus.",
                "I will not run ahead of God, in the name of Jesus.",
                "Lord, You shall be my Lord to the end, in the name of Jesus.",
            ]
        },
        {
            day: 15,
            topic: "You Will Accomplish More by Learning to Do Less",
            wordStudy: "Exodus 20:8-11",
            confession: "The Lord will make me lie down in green pastures, in the name of Jesus.",
            fullText: <View>
                <Text style={styles.text}>
                    Are you like the man who said, “The devil never takes a vacation, so why
                    should I?” Since when did the devil become your role model? The words <Bold>“He
                    makes me lie down in green pastures” (Psalm 23:2a)</Bold> teach us that only the
                    Shepherd can choose the path, prepare the pasture, and protect us. Our role is
                    simply to follow, feed and fellowship with Him. Getting the idea? Of the Ten
                    Commandments God gave to Moses, which one do you think required the
                    most words? Adultery? No, that only took five words. Murder? No, that only
                    took four words. The longest of the Ten Commandments is about taking a day
                    off: 98 words! <Bold>(Exodus 20:8-11).</Bold> God knows us so well: He knew the store
                    owner would say, “Somebody's got to work that day; if I can't, my son will.”
                    So God says, <Bold>“Nor your son.”</Bold> “Then my daughter will.” <Bold>“Nor your
                    daughter.”</Bold> “Then an employee.” <Bold>“Nor your…servant.”</Bold>
                    <Br />
                    God says, “One day a week you'll say no to work and yes to worship. You'll
                    slow down, sit down, lie down and rest. After all, I rested on the seventh day
                    and the world didn't crash. So repeat after Me, 'It's not my job to run the
                    world.'” Charles Spurgeon said, “The very sea pauses at ebb and flood; earth
                    keeps the Sabbath of the wintry months, and man… must rest or faint, trim
                    his lamp or let it burn low… In the long term, we shall do more, by sometimes
                    doing less.” The bottom line is that if you honour God's principles you will
                    find rest for your body, mind and emotions.
                </Text>
            </View>,
           prayerPoints: [
                "Lord, I receive grace to follow You as the Shepherd and Guardian of my soul, in the name of Jesus.",
                "I will not run ahead of God, in the name of Jesus.",
                "Lord, You shall be my Lord to the end, in the name of Jesus.",
            ]
        },
        {
            day: 16,
            topic: "God Will “Be There” for You",
            wordStudy: "Psalm 46:1-4",
            confession: "For this God is our God for ever and ever: he will be our guide even unto death (Psalm 48:14 KJV).",
            fullText: <View>
                <Text style={styles.text}>
                    Philip Yancey says, “Faith means believing in advance, what will only make
                    sense in reverse.” If you fear the future, look back and see how God took care
                    of you in the past. <Bold>“This I call to mind and therefore I have hope: Because
                    of the Lord's great love we are not consumed, for His compassions never
                    fail. They are new every morning; great is Your faithfulness… The Lord
                    is good to those whose hope is in Him, to the one who seeks Him”
                    (Lamentations 3:21-25 NIV).</Bold> You say, “I don't know what I'll do if my loved
                    one dies.” You will—when the time comes. You say, “I don't know how I'll
                    pay these bills.” Jehovah Jireh, the Lord who provides, will be there—when
                    the time comes. You say, “I'm not qualified to handle this, there's too much I
                    don't know.” Maybe you want to know everything too soon. God will give
                    you wisdom—when the time comes.
                    <Br />
                    The key is to meet today's problems with today's strength, and leave
                    tomorrow in God's hands. During World War II, Arthur Sulzberger, publisher
                    of The New York Times, found it hard to sleep or rid his mind of fear until he
                    adopted these words from the hymn Lead, Kindly Light: “I do not ask to see
                    the distant scene; one step enough for me.” And God isn't going to let you see
                    the distant scene either. No, He promises a lamp for your feet, not a crystal
                    ball for your future. <Bold>“He leads me”</Bold> and that's enough for today. And
                    tomorrow? <Bold>“We will find grace to help us when we need it” (Hebrews 4:16
                    NLT).</Bold>
                </Text>
            </View>,
           prayerPoints: [
                "Father, deliver me in the day of trouble, in the name of Jesus.",
                "Father, let my every step be ordered by You, in the name of Jesus.",
                "Let Your presence be with me always, Lord, according to Your Word.",
            ]
        },
        {
            day: 17,
            topic: "Have You Lost Your Peace of Mind?",
            wordStudy: "Psalm 42:1-6",
            confession: "The Lord will give me peace of mind, in the name of Jesus.",
            fullText: <View>
                <Text style={styles.text}>
                    When life overwhelms us we usually look for one of two things. We may look
                    for somebody with all the answers. The trouble is, when you look to someone
                    for that which only God can provide, you set that person up to fail and
                    yourself up to be disappointed. Alternatively, we may look for somewhere
                    with fewer problems. Paul Harvey wrote, “After Hurricane Andrew
                    devastated South Florida, Patricia Christy vowed she was going to leave on
                    the first plane. She was determined to get as far away from the horror of the
                    hurricane damage as she possibly could and have a restful vacation.” Harvey
                    said, “I just heard from Patricia Christy. She was standing in line for fresh
                    water on the Hawaiian Island Kauai, having just gone through Hurricane
                    Iniki!”
                    <Br />
                    Sheep get anxious when storms come, predators roar, or they're forced to
                    move to unfamiliar territory. How do they handle such times? By moving
                    closer to the shepherd! Have you lost your peace of mind today? Read the
                    following Scriptures; they'll help you move closer to your Shepherd: <Bold>“But let
                    all those rejoice who put their trust in You; let them ever shout for joy,
                    because You defend them…” (Psalm 5:11 NKJV). “The eternal God is
                    your refuge, and underneath are the everlasting arms…” (Deuteronomy
                    33:27 NIV). “You will keep him in perfect peace, whose mind is stayed on
                    You, because he trusts in You” (Isaiah 26:3 NKJV). “The LORD is good, a
                    stronghold in the day of trouble; and He knows those who trust in Him”
                    (Nahum 1:7 NKJV). For, “The LORD remembers us and will bless us…”
                    (Psalm 115:12 NIV).</Bold>

                </Text>
            </View>,
           prayerPoints: [
                "Every evil targeted against me, disappear, in the name of Jesus.",
                "Father, let me experience peace of mind in You, in the name of Jesus.",
                "Let every storm in my life cease this day, in the name of Jesus.",
            ]
        },
        {
            day: 18,
            topic: "Made Righteous Through Christ?",
            wordStudy: "Ephesians 2:4-9",
            confession: "The Lord shall lead me in the paths of righteousness, in the name of Jesus.",
            fullText: <View>
                <Text style={styles.text}>
                    How could a God who is perfect live side by side in Heaven with people like
                    us who aren't? That's like asking if a top university would admit someone
                    with only a kindergarten education. Then what are we to do? Find a worthy
                    substitute, or be lost forever! That's where Jesus comes in; He's the Good
                    Shepherd who gave His life for the sheep. It's the great transfer! At the cross
                    God placed all our sins on Christ, and at the point of believing faith, He places
                    all Christ's righteousness on us. Awesome!
                    <Br />
                    Actor Kevin Bacon recalls when his six-year-old son saw the movie
                    Footloose for the first time: “Hey, Dad, you know that part in the movie
                    where you swing from the rafters? That's really cool, how did you do that?” I
                    said, “Well, I didn't do that part… it was a stuntman.” “What's a stuntman?”
                    he asked. “That's someone who dresses like me and does things I can't do.”
                    “Oh,” he replied, and walked out of the room looking a little confused. Later,
                    he said, “Hey, Dad, you know that part in the movie where you spin around on
                    that gym bar and land on your feet? How did you do that?” I said, “Well, I
                    didn't do that either. That was a gymnastics double.” “What's a gymnastics
                    double?” he asked. “That's a guy who dresses in my clothes and does things I
                    can't do.” Then my son asked, “Dad, what did you do?” “I got all the glory,” I
                    sheepishly replied.
                    <Br />
                    The songwriter puts it this way:
                    <Br />
                    <Italic>Dressed in His righteousness alone, faultless to stand before Thy throne.
                    On Christ the solid rock I stand, all other ground is sinking sand.</Italic>
                </Text>
            </View>,
           prayerPoints: [
                "Lord, uproot every spirit of unrighteousness in my life, in the name of Jesus.",
                "Father, deliver me from the powers of darkness, in the name of Jesus.",
                "I receive grace to follow the paths of righteousness all the days of my life, in the name of Jesus.",
            ]
        },
        {
            day: 19,
            topic: "It's All about Him",
            wordStudy: "Romans 12:1-4",
            confession: "Humble yourselves therefore under the mighty hand of God, that he may exalt you in due time (1 Peter 5:6).",
            fullText: <View>
                <Text style={styles.text}>
                Have you ever asked, “How could God use someone like me?” The Psalmist
                answers, <Bold>“For His name's sake”! (Psalm 23:3c)</Bold>. Only one name
                counts—and it's not yours! With the same intensity that God hates pride, He
                loves humility. His Word says, <Bold>“God opposes the proud but gives grace to
                the humble” (James 4:6 NIV).</Bold> So here are a few tips on staying humble.
                First, evaluate yourself honestly. Don't be like the beaver who told the rabbit
                as they stared up at the immense wall of Hoover Dam, “No, I didn't actually
                build it myself. But it was based on an idea of mine.” The Bible says, <Bold>“Don't
                cherish exaggerated ideas of yourself…but try to have a sane estimate of
                your capabilities” (Romans 12:3-4 PHPS).</Bold>
                <Br />
                Second, learn to celebrate others. Paul writes, <Bold>“Don't try to impress
                others. Be humble, thinking of others as better than yourselves”
                (Philippians 2:3 NLT).</Bold> Every goal scored in life is usually a team effort. Be
                like the little boy who came home from the auditions for the school play and
                announced, “Mummy, I got the part. I've been chosen to sit in the audience
                and cheer.” When you can do that, your head finally fits your hat size.
                <Br />
                Finally, don't announce your success before it occurs. <Bold>“One who puts on
                his armour should not boast like one who takes it off” (1 Kings 20:11
                NIV).</Bold> When one of Charles Spurgeon's students proudly stepped up to
                preach, but came back down having failed miserably, Spurgeon supposedly
                said something like, “If you'd gone up the way you came down, you'd have
                come down the way you went up.” God blesses our efforts only when they're
                done <Bold>“for His name's sake.”</Bold>
                </Text>
            </View>,
           prayerPoints: [
                "Father, make me a vessel of honour in Your hand, in the name of Jesus.",
                "Lord, use me for Your glory.",
                "Holy Spirit, give me the grace to be humble, in the name of Jesus.",
            ]
        },
        {
            day: 20,
            topic: "Facing Death with Confidence",
            wordStudy: "John 10:28-30",
            confession: "I shall not die, but live, and declare the works of the LORD, in the name of Jesus (Psalm 118:17).",
            fullText: <View>
                <Text style={styles.text}>
                One day Jesus will come and take each of us to His house. “How do we get
                there?” you ask. <Bold>“Through the valley of the shadow of death” (Psalm
                23:4a).</Bold> This verse is quoted at the graves of paupers and carved on the
                headstones of presidents for a couple of reasons. First because we must all
                face death. Some senior citizens were lounging on the patio of their
                retirement community. One looked up as a large flock of birds flew overhead.
                He nudged his buddy who'd dozed off, “Frank, you'd better move around a
                little, those vultures look like they're closing in on us.” The Bible says, <Bold>“It is
                appointed unto men once to die…” (Hebrews 9:27 KJV).</Bold> Sooner or later
                the buzzards will move in; will you be ready? It's also quoted to remind us that
                we don't have to face it alone.
                <Br />
                Donna Spratt writes, 'To begin a discussion on values, our youth pastor
                asked the teens this question: 'What would you do if your doctor told you that
                you had only 24 hours to live?' The teens' responses were typically, 'Be with
                friends and family.' But the discussion came undone when Jason, our 13-yearold,
                said, 'I'd get a second opinion.' When it comes to opinions about death,
                only one counts: Jesus said, <Bold>“I am the Resurrection and the Life. He who
                believes in Me will live, even though he dies; and whoever lives and
                believes in Me will never die. Do you believe this?” (John 11:25-26 NIV).</Bold>
                Rejoice, you won't have to make the journey alone. Furthermore, whatever
                you give up for Christ in this life will be nothing to what you inherit in the
                next.
                </Text>
            </View>,
           prayerPoints: [
                "Father, I receive victory over sickness and death, in the name of Jesus.",
                "I reject sudden death, in the name of Jesus.",
                "Every covenant of sudden death, be nullified by the blood of Jesus, in the name of Jesus.",
            ]
        },
        {
            day: 22,
            topic: "Running the Race",
            wordStudy: "Hebrews 12:1-2",
            confession: "Looking unto Jesus the author and finisher of our faith; who for the joy that was set before him endured the cross, despising the shame, and is set down at the right hand of the throne of God (Hebrews 12:2).",
            fullText: <View>
                <Text style={styles.text}>
                    In Greece there's a place tourists seldom visit. The writer of Hebrews may
                    have had it in mind when he wrote, <Bold>“Lay aside every weight, and the sin
                    which so easily ensnares us, and…run with endurance…” (Hebrews 12:1
                    NJKV).</Bold> It's where the Isthmian Games, a forerunner to the modern Olympics,
                    were held: a place where athletes were hailed as heroes. To develop muscle
                    they trained with weights strapped to their legs, but on the day of the race they
                    stripped off anything that wasn't essential. There's a lesson here. We think
                    what we're clinging to is important. If we didn't, letting go wouldn't be a
                    struggle; we'd simply set it down.
                    <Br />
                    The Christian life is a race that starts the day you accept Christ and ends
                    when you meet Him face-to-face. In order to cross the finish line as a winner
                    you must eliminate:
                    <Br />
                </Text>

                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            <Italic>Anything that slows you down.</Italic> In other words, anything that hinders
                            your spiritual progress. In and of itself, it may not be wrong, but it
                            becomes a “weight” when it stops you from living for God to the fullest.
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            <Italic>Anything that causes you to stumble short of the finish line</Italic> You must
                            constantly monitor the level of your commitment to Christ, the growth of
                            your faith, your home life, your relationships, your integrity, your work
                            ethic, your thought life and your habits. Make up your mind to stay
                            focused on the prize. Greek athletes who won received a garland that
                            eventually withered, but <Bold>“you will receive the crown of glory that will
                            never fade” (1 Peter 5:4 NIV).</Bold> Isn't that worth running the race for?
                        </Text>
                    },
                ]} />
            </View>,
           prayerPoints: [
                "I receive grace to run my Christian race to the end and finish my course, in the name of Jesus.",
                "My life shall move forward and upward, in the name of Jesus.",
                "Father, grant me the grace to make Heaven, in the name of Jesus.",
            ]
        },
        {
            day: 23,
            topic: "Two Thoughts about Elisha",
            wordStudy: "John 15:14-16",
            confession: "Ye have not chosen me, but I have chosen you, and ordained you, that ye should go and bring forth fruit, and that your fruit should remain: that whatsoever ye shall ask of the Father in my name, he may give it you (John 15:16).",
            fullText: <View>
                <Text style={styles.text}>
                    <Italic>Elisha served others until he died.</Italic> We read: <Bold>“Elisha had become sick with
                    the illness of which he would die. Then Joash the king of Israel came
                    down to him [for advice]” (2 Kings 13:14 NKJV).</Bold> On his deathbed, Elisha
                    gave the king a strategy for defeating his enemies. So, you can lift others even
                    when you yourself are down. You can feel like a hypocrite because things
                    aren't so great in your own life, yet still minister to them. The truth is, when
                    you reach out in love to someone else it takes the focus off you, your
                    discouragement lifts, and it works for your good.
                    <Br />
                    Elisha's impact continued after he died. One day <Bold>“Elijah said to Elisha,
                    'What can I do for you?' [He replied] 'Let me inherit a double portion of
                    your spirit'” (2 Kings 2:9 NIV).</Bold> Elijah, his mentor, performed seven major
                    miracles recorded in Scripture. When he died, Elisha had performed only
                    thirteen. Did God fail to grant his request? No. <Bold>“Then Elisha died, and they
                    buried him. And the raiding bands from Moab invaded the land… So it
                    was, as they were burying a man, that suddenly they spied a band of
                    raiders; and they put the man in the tomb of Elisha; and when the man
                    was let down, and touched the bones of Elisha, he revived and stood on
                    his feet” (2 Kings 13:20-21 NKJV).</Bold> Miracle fourteen: Elisha got his double
                    portion! Jesus said, <Bold>“I chose you… that you should go and bear fruit, and
                    that your fruit should remain…” (John 15:16 NKJV).</Bold> So pray, “Lord,
                    give me a legacy of righteousness. Make my impact greater than my lifespan.
                    Give me fruit that remains.”
                </Text>
            </View>,
           prayerPoints: [
                "Father, I receive grace to bear fruit to the end, in the name of Jesus.",
                "Father, let my fruit remain to Your Honour, in the name of Jesus.",
                "Make my life leave behind a legacy of righteousness, in the name of Jesus.",
            ]
        },
        {
            day: 24,
            topic: "Don't Play the Fool",
            wordStudy: "1 Samuel 26:19-25",
            confession: "I receive wisdom from above, in the name of Jesus.",
            fullText: <View>
                <Text style={styles.text}>
                    Your obituary says a lot about you. Towards the end of his life, King Saul said,
                    <Bold>“I have played the fool…” (1 Samuel 26:21 NKJV).</Bold> Israel's first king was
                    destined for greatness till he decided to do things his own way, instead of
                    God's way. When the Philistines attacked Israel, he tried to rally his troops
                    who were immobilised by fear. He knew he should wait for the prophet
                    Samuel to come and offer a burnt sacrifice as required by the law. But he said,
                    “I'll just do it myself. Under the circumstances, God won't mind.” That one act
                    of disobedience ended his career. He died by committing suicide on the
                    battlefield; a life filled with promise, ended in disgrace.
                    <Br />
                    You play the fool by:
                    <Br />
                </Text>

                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            <Italic>Disregarding God in little things.</Italic> Saul's fall didn't happen overnight.
                            Little sins morph into big ones. Thinking, “It's no big deal,” Saul took
                            matters into his own hands. He said,<Bold> “…I felt compelled to offer the
                            burnt offering myself…” (1 Samuel 13:12 NLT).</Bold> Note the words, “I
                            felt.” It can feel so right, yet be so wrong. Only trust your feelings when
                            they line up with God's Word.
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            <Italic>Trying to justify your behaviour.</Italic> Saul rationalised, <Bold>“…I saw my men
                            scattering… you didn't arrive when you said… the Philistines
                            are…ready for battle” (1 Samuel 13:11 NLT).</Bold> Stop rationalising,
                            repent, and obey God!
                        </Text>
                    },
                    {
                        serial: 3,
                        body: <Text>
                            <Italic>Letting resentment control you.</Italic> Though he had the makings of a great
                            leader, when David started to gain popularity, Saul's resentment ended
                            up destroying him. Most people learn from their own mistakes; wise
                            people learn from other people's! Learn from Saul; don't play the fool.
                        </Text>
                    },
                ]}
                />
            </View>,
           prayerPoints: [
                "Holy Spirit, help me to do Your will, in the name of Jesus.",
                "I shall not be manipulated, in the name of Jesus.",
                "Father, separate me from every destiny destroyer, in the name of Jesus.",
            ]
        },
        {
            day: 25,
            topic: "When You Are the Leader (1)",
            wordStudy: "1 Corinthians 3:4-9",
            confession: "Study to shew thyself approved unto God, a workman that needeth not to be ashamed, rightly dividing the word of truth (2 Timothy 2:15).",
            fullText: <View>
                <Text style={[styles.text]}>
                    Church growth consultant Jim Wideman highlights some things you can do
                    to help those looking to you for direction:
                    <Br />
                </Text>

                <View style={styles.indentView}>
                    <IndentList items={[
                        {
                            serial: 1,
                            body: <Text>
                                <Italic>Put their goals and needs first.</Italic> You're a part, not the whole enchilada,
                                the whole thing, everything. Think about how your actions affect others.
                                Be observant of the challenges and setbacks they're experiencing, and
                                find ways to lighten their load even if they don't ask.
                            </Text>
                        },
                        {
                            serial: 2,
                            body: <Text>
                                <Italic>Help others to win.</Italic> Let others take a slam-dunk while you take an assist.
                                Keep the ball moving till someone has a chance to score. Adopt the
                                motto, “It doesn't matter who gets the credit.” Be willing to accept
                                blame and reluctant to assign it. Maintain an authentic desire to share
                                victories. “A kingdom divided by civil war will collapse” <Bold>(Mark 3:24
                                NLT)</Bold>; you can't sink someone else's end of the boat and keep your own
                                a float!
                            </Text>
                        },
                        {
                            serial: 3,
                            body: <Text>
                                <Italic>Over-communicate.</Italic> Keep everyone in the loop, spend time with your
                                peers, invest in your teammates, and admit when you need help. Be open
                                to correction and advice. Leadership coach Rick Tate said, “Feedback is
                                the breakfast of champions,” and a good communicator is a good
                                listener.
                            </Text>
                        },
                        {
                            serial: 4,
                            body: <Text>
                                <Italic>Don't take things personally.</Italic> Leadership, by definition, is about
                                “others.” When you start thinking it's all about “you,” you lose
                                perspective.
                            </Text>
                        },
                        {
                            serial: 5,
                            body: <Text>
                                Give it all you've got. Paul, Timothy's mentor, said to him: <Bold>“Do your
                                best… [be] a worker who has no need to be ashamed…” (2 Timothy
                                2:15 NRS).</Bold> Good leaders empower others. Henry Mintzberg said:
                                “…The best managing of all may well be silent.”
                            </Text>
                        },
                        
                    ]} />
                </View>
            </View>,
           prayerPoints: [
                "Father, help me to see beyond the ordinary, in the name of Jesus.",
                "Make me a channel of blessing to others, in the name of Jesus.",
                "My business shall prosper, in the name of Jesus.",
            ]
        },
        {
            day: 26,
            topic: "When You Are the Leader (2)",
            wordStudy: "Matthew 20:25-27",
            confession: "I shall experience joy in the midst of every trial, in the name of Jesus.",
            fullText: <View>
                <Text style={[styles.text]}>
                Mac Anderson says: “Like every human being, I have doubts, fears and
                disappointments… As leaders, however, we must manage our attitude… We
                can't underestimate the influence of our actions and attitudes. Churchill said,
                The price of leadership is responsibility… to stay positive whether you feel
                like it or not. A good leader launches out before success is certain… doesn't
                run from confrontation… talks about his own mistakes before anybody else's,
                and acknowledges them before others have to discover and reveal them. He
                looks for opportunities to find his teammates doing something right, and
                encourages the smallest improvement. He doesn't tolerate murmuring in
                himself or in others… is specific in his expectations… values
                accountability… does what's right instead of what's popular or convenient.”
                <Br />
                What does the word servant bring to your mind? The guy who works
                behind the scenes? The personal assistant who makes you look good? The
                mate who worked so you could complete your degree? Charles Stanley says:
                “Godly servants are all around us, but, sadly, we frequently take them for
                granted. This is a tragedy we need to correct, not only for their sakes but for
                our own. Their faithful service brings untold blessings… Wherever Joseph
                went, the people he served were blessed. Potiphar wasn't a God-fearing man,
                yet he prospered because of Joseph. Find the people in your life who have the
                gift of godly service and spend time with them… When you turn your nose up
                at someone doing servant's work; you cut yourself off from a relationship that
                could literally change your life.”
                <Br />
                Each of us has the potential to be great, not famous; but great, because
                greatness comes by serving!
                </Text>
            </View>,
           prayerPoints: [
                "Father, let my work attract godly workers, in the name of Jesus.",
                "Let the seed of greatness in my life begin to manifest, in the name of Jesus.",
                "Let my hands be anointed for exploits, in the name of Jesus.",
            ]
        },
        {
            day: 27,
            topic: "When You Are the Leader (3)",
            wordStudy: "Luke 22:24-27",
            confession: "The Lord will make me great, in the name of Jesus.",
            fullText: <View>
                <Text style={[styles.text]}>
                    Michael Bruner writes about how, as a brash young college student, he
                    attended a lecture by a former US Attorney General. He says: “Afterwards I
                    approached him to see if we could meet for coffee. To his associates' shock,
                    he said, 'How about tomorrow?' We met and talked for an hour… I peppered
                    him with questions. What famous people had he met? What was it like to be
                    Attorney General in the 60s? When I asked him who was the greatest person
                    he'd ever met, he said, 'I don't think of people in those terms.' He went on to
                    tell me something I'll never forget: 'Don't ever seek to be the greatest. Seek
                    instead to do great things. If you aspire to greatness, your greatness will die
                    with you. But if you aspire to do great things, your legacy will live on. The
                    only way to do this is by being a servant. Lead by serving and you'll do great
                    thing.'
                    <Br />
                    I was too young in the faith to know he'd taken those words from
                    Scripture… Jesus was the embodiment of servant leadership. He didn't just
                    tell the disciples what they should do, He did it along with them… As I left the
                    hotel that morning and waited to cross the street, a blind man with a seeingeye
                    dog came up alongside me. I stared at the beautiful Lab… his senses alert,
                    his sole purpose in life to serve his…master. Then the light turned green and
                    gently the dog led [him] across the street… God had sent me a living parable.
                    I learned a lesson that morning I would never forget. Pursue great things, not
                    greatness; lead by serving.”
                </Text>
            </View>,
           prayerPoints: [
                "I receive grace to pursue great things, in the name of Jesus.",
                "I shall do exploits for the kingdom of God, in the name of Jesus.",
                "Father, make me a good follower of Christ, in the name of Jesus.",
            ]
        },
        {
            day: 28,
            topic: "Character: The Beauty That Lasts",
            wordStudy: "1 Peter 3:3-4",
            confession: "The Lord will take me to higher grounds, in the name of Jesus.",
            fullText: <View>
                <Text style={[styles.text]}>
                    When it comes to beauty, get your perspective right: <Bold>“Don't be concerned
                    about the outward beauty of fancy hairstyles, expensive jewellery, or
                    beautiful clothes… Clothe yourselves…with the beauty that comes from
                    within…” (1 Peter 3:3-4 NLT).</Bold> It's said the public relations department of a
                    beauty products company asked its customers to send pictures along with brief
                    letters, describing the most beautiful women they knew. Thousands of letters
                    came in. One caught the attention of the employees and was passed on to the
                    president. It was written by a boy from a broken home who lived in a run-down
                    neighbourhood. With lots of spelling errors, an excerpt from his letter read: “A
                    beautiful woman lives down the street from me. I visit her every day. She
                    makes me feel like the most important kid in the world. We play checkers and
                    she listens to my problems. She understands me. When I leave she always yells
                    out the door that she's proud of me.”
                    <Br />
                    The boy ended his letter saying, “This picture shows you that she is the
                    most beautiful woman in the world, and one day I hope to have a wife as pretty
                    as her.” Intrigued, the president asked to see the woman's picture. His secretary
                    handed him a photograph of a smiling, toothless woman, well advanced in
                    years, sitting in a wheelchair. Sparse grey hair was pulled back in a bun. The
                    wrinkles that formed deep furrows on her face were somehow diminished by
                    the twinkle in her eyes. “We can't use this woman,” said the president, smiling.
                    “She would show the world that our products aren't necessary to be beautiful.”
                </Text>
            </View>,
           prayerPoints: [
                "Father, I receive grace this day to raise my family in a godly way, in the name of Jesus.",
                "I receive the wisdom of God to manage my home, in the name of Jesus.",
                "Father, increase and strengthen my family, in the name of Jesus.",
            ]
        },
        {
            day: 29,
            topic: "Handling Family Problems",
            wordStudy: "1 Timothy 3:1-7",
            confession: "The glory of the presence of the Lord shall not depart from my home, in the name of Jesus.",
            fullText: <View>
                <Text style={[styles.text]}>
                    In order to bless all the families of the earth, Abraham had to start with his own
                    family. Before a man could qualify for leadership in the New Testament
                    church, they examined his home life (1 Timothy 3:5). Their thinking was, “If
                    he doesn't succeed there, don't enlarge his territory.” But if you're going to
                    enjoy God's blessing as a family you must learn to cope with difficulties. So:
                    <Br />
                </Text>
                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            <Italic>Try to remember that you're all on the same team.</Italic> Don't take your
                            frustrations out on your loved ones. Too often, home is where we go
                            when we're tired of being “nice.”
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            <Italic>Before you speak, get the facts.</Italic> Nothing's more damaging than jumping
                            to conclusions. <Bold>“Those who control their tongue will have a long life;
                            opening your mouth can ruin everything” (Proverbs 13:3 NLT).</Bold>
                        </Text>
                    },
                    {
                        serial: 3,
                        body: <Text>
                            <Italic>Handle it with wisdom.</Italic> List all your options and you'll be more objective.
                            That's how you'd handle a problem at work; why not do the same with
                            your family?
                        </Text>
                    },
                    {
                        serial: 4,
                        body: <Text>
                            <Italic>Find something good in the situation.</Italic> <Bold>“And we know that all things
                            work together for good to those who love God…” (Romans 8:28
                            NKJV).</Bold> No matter how bad things seem, every situation holds
                            something positive; look for it.
                        </Text>
                    },
                    {
                        serial: 5,
                        body: <Text>
                            <Italic>Make sure they know you love them.</Italic> It's okay to express how you feel so
                            long as you do it graciously. But make sure your family knows you love
                            them. When people feel loved they can weather almost any crisis. Think:
                            When do you need God's love most? When you deserve it least! Try to
                            follow suit.
                        </Text>
                    },
                ]} />
            </View>,
           prayerPoints: [
                "Holy Spirit, I receive grace to cooperate with You for the fruit of the Spirit to manifest in my life, in the name of Jesus.",
                "The glory of the Lord shall radiate in and from my home, in the name of Jesus.",
                "My family shall experience the peace of God, in the name of Jesus.",
            ]
        },
        {
            day: 30,
            topic: "Handling Family Problems",
            wordStudy: "1 Timothy 3:1-7",
            confession: "The glory of the presence of the Lord shall not depart from my home, in the name of Jesus.",
            fullText: <View>
                <Text style={[styles.text]}>
                    In order to bless all the families of the earth, Abraham had to start with his own
                    family. Before a man could qualify for leadership in the New Testament
                    church, they examined his home life (1 Timothy 3:5). Their thinking was, “If
                    he doesn't succeed there, don't enlarge his territory.” But if you're going to
                    enjoy God's blessing as a family you must learn to cope with difficulties. So:
                    <Br />
                </Text>
                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            <Italic>The first person you must know is yourself.</Italic> Human nature seems to
                            endow us with the ability to size up everybody—except ourselves. If
                            you're not comfortable with yourself, you won't be comfortable with
                            others. And if you don't believe in yourself, your lack of self-worth will
                            undermine you in life. One marriage counsellor says, “The most
                            important relationship you will ever have is with yourself. You've got to
                            be your own best friend first.” But how can you be best friends with
                            someone you don't know, like, or respect? That's why it's important to
                            discover what God's called you to be, then work at becoming that
                            person. And you won't get there overnight; it's a process, one that
                            requires a mindset of honesty, frequent repentance and constant selfcorrection.
                            But if you commit yourself to it God will help you.
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            <Italic>The first person you must work on is yourself.</Italic> Dr. Samuel Johnson said,
                            “He who has so little knowledge of human nature as to seek happiness
                            by changing anything but his own disposition, will waste his life in
                            fruitless efforts, and multiply the grief which he purposes to remove.”
                            The Bible says, “Examine yourselves”' Only as we examine ourselves
                            before God do we discover where our true battles lie. Then we've a
                            choice. We can be like the man who visited his doctor and found out he'd
                            serious health issues. When the doctor showed him his X-rays and
                            suggested surgery, the man asked, “OK, but how much would you
                            charge just to touch up the X-rays?” Or, you can decide to change!
                        </Text>
                    },
                ]} />
            </View>,
           prayerPoints: [
                "Holy Spirit, guard and guide me from falling, in the name of Jesus.",
                "Help me to discover God's purpose for my life, in the name of Jesus.",
                "Grace to endure to the end, I receivev today, in the name of Jesus.",
            ]
        },
        {
            day: 31,
            topic: "Handling Family Problems",
            wordStudy: "1 Timothy 3:1-7",
            confession: "The glory of the presence of the Lord shall not depart from my home, in the name of Jesus.",
            fullText: <View>
                <Text style={[styles.text]}>
                    Addressing a worldwide convention of demons, satan told them: “As long as
                    Christians stay close to God. We have no power over them, so:
                </Text>
                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            Keep them busy with non-essentials.
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            Tempt them to overspend and go into debt
                        </Text>
                    },
                    {
                        serial: 3,
                        body: <Text>
                            Make them work long hours to maintain empty lifestyles.
                        </Text>
                    },
                    {
                        serial: 4,
                        body: <Text>
                            Discourage them from spending family time, for when homes
                            disintegrate there's no refuge from work.
                        </Text>
                    },
                    {
                        serial: 5,
                        body: <Text>
                            Overstimulate their minds with television and computers so that they
                            can't hear God speaking to them.
                        </Text>
                    },
                    {
                        serial: 6,
                        body: <Text>
                            Fill their coffee tables and nightstands with newspapers and magazines
                            so they've no time for Bible reading.
                        </Text>
                    },
                    {
                        serial: 7,
                        body: <Text>
                            Flood their letter boxes with sweepstakes, promotions and get-richquick
                            schemes; keep them chasing material things.
                        </Text>
                    },
                    {
                        serial: 8,
                        body: <Text>
                            Put glamorous models on TV and on magazine covers to keep them
                            focused on outward appearances; that way they'll be dissatisfied with
                            themselves and their mates.
                        </Text>
                    },
                    {
                        serial: 9,
                        body: <Text>
                            Make sure couples are too exhausted for physical intimacy; that way
                            they'll be tempted to look elsewhere.
                        </Text>
                    },
                    {
                        serial: 10,
                        body: <Text>
                            Emphasize Santa and the Easter Bunny; that way you'll divert them
                            from the real meaning of Christmas and Easter.
                        </Text>
                    },
                    {
                        serial: 11,
                        body: <Text>
                            Involve them in “good” causes so they won't have any time for
                            “eternal” ones.
                        </Text>
                    },
                    {
                        serial: 12,
                        body: <Text>
                            Make them self-sufficient. Keep them so busy working in their own
                            strength that they'll never know the joy of God's power working through
                            them. Do these twelve things faithfully. I promise it'll work!
                        </Text>
                    },
                    
                ]} />

                <Text style={[styles.text]}>
                    Have you figured out the difference between being busy and being
                    successful in what God's called you to do? Sometimes being <Bold>B-U-S-Y</Bold> just
                    means <Bold>B</Bold>eing <Bold>U</Bold>nder <Bold>S</Bold>atan's <Bold>Y</Bold>oke!
                    <Br />
                </Text>

            </View>,
            prayerPoints: [
                "Holy Spirit, let the Word of God dwell richly in me in all wisdom, in the name of Jesus .",
                "Deliver me from the powers that are stronger than I, in the name of Jesus.",
                "Holy Spirit, envelope my life with the Word of God, in the name of Jesus.",
            ]
        },
    ],
    August: [
        {
            day: 1,
            topic: "Never, Never Quit",
            wordStudy: "Proverbs 16:1-3",
            confession: "The Lord shall make me succeed and I shall stand before kings and not unknown men, in the name of Jesus (Proverbs 22:29).",
            fullText: <Text style={styles.text}>
                The moment you start believing you're successful enough to rest on your
                laurels, you're in trouble. You've put a lid on your growth! Solomon wrote,
                <Bold>“Do you know a hard-working man? He shall be successful and stand
                before kings!” (Proverbs 22:29 TLB).</Bold> Success is always a possibility, but
                never a guarantee. It belongs to the man or woman who's willing to show up
                early, stay late, go the extra mile, and keep asking, “Is there a better way?” An
                agency once created an ad for an automobile company. It said, “At sixty miles
                an hour the loudest noise in this new Rolls-Royce comes from the electric
                clock.” When they ran the ad before the company's CEO, he smiled and said,
                “I guess we've got to do something about that clock!”
                <Br />
                A young man once asked Henry Ford, “How can I make a name for
                myself and be successful?” He replied, “Decide what you want, then stick
                with it. Never deviate from your course no matter how long it takes or how
                hard the road, until you've accomplished your purpose.” Successful people
                have one thing in common: they refuse to quit! No matter how many times
                they fall, they get back up, dust themselves off, learn from it, and start over.
                Paul J. Meyer says, “Ninety-nine per cent of those who fail are not actually
                defeated, they simply quit.” The Bible says, <Bold>“And let us not grow weary of
                doing good, for in due season we will reap, if we do not give up”
                (Galatians 6:9 ESV).</Bold> The only people who never fail are those who never
                try. So keep going, and don't even think about quitting!
            </Text>,
            prayerPoints: [
                "Lord, take control of my life, in the name of Jesus.",
                "Make my life a success by Your grace, in the name of Jesus.",
                "The grace not to give up, fall on me, in the name of Jesus.",
            ]
        },
        {
            day: 2,
            topic: "Strengthen Your Marriage",
            wordStudy: "1 Peter 3:1-7",
            confession: "My marriage will not lack any good thing, in the name of Jesus.",
            fullText: <View>
                <Text style={styles.text}>
                    When an accusing voice confronts you with your past, respond in the
                    manner Jesus did—It is written: <Bold>“I will…remember their sins no more.”</Bold>
                    It's not that God is forgetful; it's that He chooses not to remember your sins.
                    And when you choose otherwise, you question His forgiveness, allow the
                    enemy to guilt-trip you, and forfeit the confidence you need to receive what
                    God has promised you <Bold>(See 1 John 3:21-22.).</Bold> When you keep rehearsing
                    your past you not only keep it alive, you empower it. What you keep on
                    deposit, you're more likely to withdraw and act on in a moment of weakness.
                    Just as nobody knows when a dormant volcano may erupt, you can't predict
                    when an unresolved issue will resurface, turning your words into hot coals
                    and your behaviour into a blaze of destruction. Only by the power of God's
                    forgiveness, and by forgiving others, is the hold your past has over you
                    broken.
                    <Br />
                    Shame isn't a blessing; it's a weight Jesus bore for you on the cross. So set
                    it down and walk away! God's Word says, <Bold>“As far as the east is from the
                    west, so far has He removed our transgressions from us” (Psalm 103:12
                    NKJV).</Bold> Notice, there's a North Pole and a South Pole, but no east or west
                    pole. Why? Because that distance is infinite and beyond measure. Are you
                    getting the idea? Whenever you are reminded of past sin, the enemy hopes
                    you're ignorant of the truth in order to rob you of your future. Don't take the
                    bait! Instead look to the Cross of Christ, refuse to dwell on it further, and keep
                    moving forward.
                    <Br />
                </Text>
            </View>,
            prayerPoints: [
                "Grace to live above sin, fall on me, in the name of Jesus.",
                "Voice of the Lord, swallow every voice condemning me, in the name of Jesus.",
                "Sin shall not have dominion over me, in the name of Jesus.",
            ]
        },
        {
            day: 3,
            topic: "You Have Been Forgiven",
            wordStudy: "1 John 3:21-22",
            confession: "For thou, Lord, art good, and ready to forgive; and plenteous in mercy unto all them that call upon thee (Psalm 86:5).",
            fullText: <View>
                <Text style={styles.text}>
                    A couple celebrating their fiftieth wedding anniversary was asked the secret to
                    their success. The husband replied, “The day we got married we agreed that if
                    an argument arose, I'd go out and stand on the porch until I cooled off. And it
                    worked like a charm; fifty years of being outdoors in all that fresh air was
                    exactly what this relationship needed!” If you've been spending too much
                    time out on the porch, here are three ways to strengthen your marriage:
                    <Br />
                </Text>
                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            <Bold>Commitment</Bold> — <Italic>“You're first.”</Italic> Speaking those words on special
                            occasions is easy, but you need to speak them seven days a week. A film
                            star who'd been through several failed marriages told an interviewer,
                            “I've given up trying to find the right person. Now I'm working at
                            becoming the right person.” That formula always works!
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            <Bold>Concern </Bold> —<Italic>“What do you need?”</Italic> Husbands and wives are as different
                            as chalk and cheese. And to complicate things further, their needs change
                            according to the season of life they're in. So when you ask, “How are you
                            today?” slow down and listen. Your wife may not want you to solve the
                            problem, but to share it. Closeness in marriage isn't an accident: it's a
                            decision you make, and keep making every day.
                        </Text>
                    },
                    {
                        serial: 3,
                        body: <Text>
                            <Bold>Coping </Bold> —<Italic>“We can work it out”</Italic> Marriage may alleviate the problem of
                            loneliness, but it presents the challenge of getting along with another
                            person. It teaches you that you can't always avoid conƒPict, but you can
                            make it work for you. The truth is, unless there are two winners in a
                            marriage, there are none at all.
                        </Text>
                    },
                ]} />
            </View>,
            prayerPoints: [
                "Holy Spirit, help me to be sensitive to the needs of my spouse, in the name of Jesus.",
                "Help me to be the suitable companion for my spouse, in the name of Jesus.",
                "Every spirit of marital failure, loose your hold over my marriage, in the name of Jesus.",
            ]
        },
        {
            day: 4,
            topic: "Don't Be Cynical",
            wordStudy: "1 Samuel 17:28-36",
            confession: "One thing have I desired of the LORD, that will I seek after; that I may dwell in the house of the LORD all the days of my life, to behold the beauty of the LORD, and to enquire in his temple (Psalm 27:4).",
            fullText: <View>
                <Text style={styles.text}>
                    There are very few monuments erected to sceptics. That's because instead of
                    building people up, they tear them down. David's older brother Eliab was like
                    that. Here's his story. When nobody else in Saul's army, including
                    Eliab—who was a general—was willing to take on Goliath, David
                    volunteered to go out and fight this <Bold>“uncircumcised Philistine” (1 Samuel
                    17:36 KJV).</Bold> In Bible times circumcision was a sign of God's covenant of
                    protection and provision for the Israelites. And David knew this bully had no
                    such relationship with God; only the Israelites could claim such a benefit. As
                    a result, David was very secure in the covenant, and embraced God's
                    promise.
                    <Br />
                    Obviously this wasn't the case for Eliab. His <Bold>“anger burned against
                    David and he said: “Why have you come down? And with whom have
                    you left those few sheep in the wilderness? I know your insolence and the
                    wickedness of your heart; for you have come down in order to see the
                    battle” (1 Samuel 17:28 NAS).</Bold> Interestingly, the name Eliab means “God is
                    my Father,” so Eliab not only represents secular cynics, but Christian ones
                    too. Yes, we have them in the Church! All it takes is one skeptical member
                    and soon all those with weaker faith, or no faith at all, start chiming in and
                    perpetuating the negativity. Beware: cynicism can wreak havoc in any
                    relationship and environment. That's why the Bible says, <Bold>“How blessed is
                    the man who does not… sit in the seat of scoffers.” (Psalm 1:1 NAS).</Bold>
                    Think twice before sitting down in that seat. If you stay there too long you
                    may not be able to get up again!
                    <Br />
                </Text>
            </View>,
            prayerPoints: [
                "Lord, help me to love You and others the way You want me to, in the name of Jesus.",
                "Deliver from the seat of the scoffers, in the name of Jesus.",
                "Deliver from every friendly enemy, in the name of Jesus.",
            ]
        },
        {
            day: 5,
            topic: "Your Quiet Time with God (1)",
            wordStudy: "Psalm 27:7-14",
            confession: "But seek ye first the kingdom of God, and his righteousness; and all these things shall be added unto you (Matthew 6:33).",
            fullText: <View>
                <Text style={styles.text}>
                    <Bold>Seek first the Kingdom of God and His righteousness, and all
                    these things shall be added to you.”</Bold> What kind of <Bold>“things”</Bold> was He talking
                    about? Things like money, houses, relationships, health and jobs. What did He
                    mean by <Bold>“the Kingdom of God”</Bold>? Living under the rule of Christ each
                    moment and submitting to His will in all things. When Jesus used the word
                    seek, He called for three things:
                    <Br />
                </Text>
                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            <Bold>Intentionality.</Bold> When something important is lost, you must put aside
                            other things and seek until you find it.
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            <Bold>Importance.</Bold> Position, performance, prosperity, and popularity can be
                            good things when properly used. But without the rule of Christ in your
                            life, you'll always be vulnerable to the devil.
                        </Text>
                    },
                    {
                        serial: 3,
                        body: <Text>
                            <Bold>Importunity.</Bold> No matter how long it takes, how hard you must work, or
                            what you must rearrange, restore your Quiet Time with God to its rightful
                            place. The psalmist wrote, <Bold>“When You said, 'Seek My face,' my heart
                            said to You, 'Your face, LORD, I will seek”' (Psalm 27:8 NKJV).</Bold> Quiet
                            Time has been called many things throughout the history of the Church:
                            Morning Watch, Daily Devotion, Appointment with God or Personal
                            Devotional Time. It really doesn't matter what you call it, as long as you
                            have it regularly.
                        </Text>
                    },
                ]}/>
                <Text style={styles.text}>
                    Your Quiet Time with God is just daily fellowship with Him through His
                    Word and prayer. It's a time you deliberately set aside to meet with Him. The
                    goal is that you might grow in your personal relationship with God so that you
                    can know Him, love Him, serve Him and become more like Him.
                    <Br />
                </Text>
            </View>,
            prayerPoints: [
                "Help me, O Lord, with my daily fellowship with You, in the name of Jesus.",
                "Help me, O Lord, to become more like You, in the name of Jesus.",
                "Lord Jesus, I submit myself unreservedly to Your Lordship.",
            ]
        },
        {
            day: 6,
            topic: "Your Quiet Time with God (2)",
            wordStudy: "Revelation 4:10-11",
            confession: "Thou art worthy, O Lord, to receive glory and honour and power: for thou hast created all things, and for thy pleasure they are and were created (Revelation 4:11).",
            fullText: <View>
                <Text style={styles.text}>
                    Did you know?
                    <Br />
                </Text>
                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            <Italic>God created you for His own pleasure and His greater glory.</Italic> He also
                            saved you <Bold>“both to will and to work for His good pleasure”
                            (Philippians 2:13 ESV).</Bold> Your attitude towards a Quiet Time with God
                            will be transformed when you realise this. Time spent with God brings
                            Him great pleasure, honour and glory. He waits for you and welcomes
                            you into His presence—He knows how much benefit you will receive
                            from Him.
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            <Italic>The Cross is what made this relationship possible.</Italic> When Adam sinned,
                            God drove him from the Garden of Eden and placed angels with swords
                            at its entrance so that mankind could never re-enter. Then God did
                            something truly amazing; He took on human form, lived among us, died
                            in our place, and was cut down by the sword of divine judgment at the
                            Cross, providing us with a way back into His presence. Wonderful, isn't
                            it?
                        </Text>
                    },
                ]}/>
                <Text style={styles.text}>
                    In the Old Testament, only one man, the high priest, could go into the
                    Holy of Holies. And he could only do it one day a year. A thick veil separated
                    God from the people. They stood outside wondering what God's voice
                    sounded like, what His presence felt like and what His glory looked like. Only
                    the high priest knew. But when Jesus cried, <Bold>“It is finished!” (John 19:30
                    NKJV),</Bold> the Bible says, <Bold>“The veil of the temple was torn in two from top to
                    bottom” (Mark 15:38 NKJV).</Bold> Now, as <Bold>“priests unto God”</Bold> we can come
                    into His presence at any time and meet with Him <Bold>(Revelation 1:6 KJV).</Bold> What
                    a privilege!
                    <Br />
                    Grace makes it possible. And gratitude should be the magnet that draws
                    you there each day.
                    <Br />
                </Text>
            </View>,
            prayerPoints: [
                "Father, glorify Your name in my life, in the name of Jesus.",
                "Don't let sin hinder my prayer, in the name of Jesus.",
                "I ask this day for a heart of gratitude always, in the name of Jesus.",
            ]
        },
        {
            day: 7,
            topic: "Your Quiet Time with God (3)",
            wordStudy: "Mark 1:35-39",
            confession: "My voice shalt thou hear in the morning, O LORD; in the morning will I direct my prayer unto thee, and will look up (Psalm 5:3).",
            fullText: <View>
                <Text style={styles.text}>
                    Why do you need a Quiet Time with God each day? Because Jesus did, and
                    He's your example: <Bold>“He was up long before daybreak and went…to pray”
                    (Mark 1:35 TLB).</Bold> The truth is, we make time for what we value most, for the
                    people we love most, for our highest priorities, and what we find most
                    rewarding. Notice, Jesus seldom prayed for anybody in public. Why?
                    Because He'd already done His praying before He got there! He made
                    deposits each morning so that He could make withdrawals all day long. And
                    the busier He got, the more He prayed. Did He know something we don't?
                    Jesus had no difficulty choosing between the crowd's agenda and His Father's
                    will. <Bold>“I can do nothing on My own. I judge as God tells Me. Therefore,
                    My judgment is just, because I carry out the will of the One who sent Me”
                    (John 5:30 NLT).</Bold>
                    <Br />
                    Why do spiritual leaders sometimes fall? Because they get caught up in
                    the work of the Lord and neglect their relationship with Him. Throughout
                    history, anyone who has been greatly used by God was a person of prayer.
                    Martin Luther said, “I have so much to do that I must spend the first three
                    hours each day in prayer.” Ceaseless activity will drain you and leave you
                    vulnerable to Satan's attack. The sign on a church bulletin board says it all:
                    “Seven prayerless days make one weak Christian.” So the busier you
                    become, the more time you need to spend with God. Simply stated: If you're
                    too busy to have a Quiet Time with God, you're too busy!
                    <Br />
                </Text>
            </View>,
            prayerPoints: [
                "Help me, O Lord, with my daily fellowship with You, in the name of Jesus.",
                "Help me, O Lord, to become more like You, in the name of Jesus.",
                "I submit myself to Your Lordship totally, in the name of Jesus.",
            ]
        },
        {
            day: 8,
            topic: "Your Quiet Time with God (4)",
            wordStudy: "Psalm 19:7-14",
            confession: "For the word of God is quick, and powerful, and sharper than any twoedged sword, piercing even to the dividing asunder of soul and spirit, and of the joints and marrow, and is a discerner of the thoughts and intents of the heart (Hebrews 4:12).",
            fullText: <View>
                <Text style={styles.text}>
                    Your Quiet Time with God is more than just a good idea, it's vital to your
                    spiritual survival. It's also essential to your spiritual growth and maturity. You
                    say, “But I go to church every week.” Can you imagine what would happen if
                    you ate only once a week? The patriarch Job said, <Bold>“I… have treasured His
                    words more than daily food” (Job 23:12 NLT).</Bold> Peter described the
                    Scriptures as nourishing milk (1 Peter 2:2), and the writer to the Hebrews
                    called the Word of God solid food <Bold>(Hebrews 5:14).</Bold> Your Quiet Time is also
                    like a spiritual bath. Jesus said, “You are already clean because of the Word
                    which I have spoken to you” (John 15:3 NKJV). You shower every day to
                    stay clean and avoid body odours. It's not easy to be around someone who
                    smells badly, and you run the risk of offending them by telling them so. But if
                    you love them you'll do it. Paul describes the Christian as <Bold>“the aroma of
                    Christ to God” (2 Corinthians 2:15 ESV).</Bold>
                    <Br />
                    Here's the bottom line. Unless you protect your Quiet Time with God: 
                    <Br single />
                    <Bold>(a)</Bold> You'll be cut off from your source of strength, guidance, and wisdom; 
                    <Br single />
                    <Bold>(b)</Bold> Your usefulness to God will be limited; and 
                    <Br single />
                    <Bold>(c)</Bold> You'll be inconsistent in your
                    <Br single />
                    Christian life. You say, “But I don't have time!” You have the same 168 hours
                    each week that everybody else has! And how you spend them is determined by
                    what you think is most important. So if you think being in fellowship with
                    God is important, begin to make time for it.
                </Text>
            </View>,
            prayerPoints: [
                "I receive grace this day for the Word of God to dwell richly in my heart, in the name of Jesus.",
                "Lord, give me the grace to read, study, memorize and meditate Your Word, in the name of Jesus.",
                "Help me to spend my time wisely, in the name of Jesus.",
            ]
        },
        {
            day: 9,
            topic: "Be Gentle",
            wordStudy: "Matthew 11:25-29",
            confession: "Come unto me, all ye that labour and are heavy laden, and I will give you rest (Matthew 11:28).",
            fullText: <View>
                <Text style={styles.text}>
                    Jesus said, “I am humble and gentle at heart, and you will find rest for your
                    souls.” We'd all be a lot better off if people like Jesus ran the world, because
                    those who aren't gentle are making a real mess of things! Francis de Sales said,
                    “Nothing is so strong as gentleness, and nothing so gentle as real strength.”
                    Just as you catch more flies with honey than with vinegar, people respond
                    more readily to gentleness than aggressiveness.
                    <Br />
                    The famous football coach, John Wooden, told the following story: “My
                    dad, Joshua Wooden, was a strong man in one sense, but a gentle man. He
                    could lift heavy things men half his age couldn't, but he would also read poetry
                    to us each night after a day working in the fields raising corn, hay, wheat,
                    tomatoes, and watermelons. We had a team of mules named Jack and Kate on
                    our farm. Kate would often get stubborn and lie down on me when I was
                    ploughing. I couldn't get her up no matter how roughly I treated her. Dad
                    would see my predicament and walk across the field until he got close enough
                    to say, “Kate.” Then she would get up and start working again. He never
                    touched her in anger. It took me a long time to understand that even a stubborn
                    mule responds to gentleness.”
                    <Br />
                    When the Bible speaks of humility and meekness, it's not speaking of
                    weakness. Meekness means “power under control.” An unbroken horse is
                    useless; an overdose of medicine kills rather than cures; wind out of control
                    destroys everything in its path. Jesus was powerful but He was gentle. And
                    you are called to follow in His footsteps <Bold>(1 Peter 2:21).</Bold>
                </Text>
            </View>,
            prayerPoints: [
                "Lord, baptize me with Your Spirit of gentleness.",
                "Lord, baptize me with Your Spirit of meekness.",
                "By Your grace, I choose to follow in Your footsteps.",
            ]
        },
        {
            day: 10,
            topic: "Through Christ, You Can Conquer Your Habit",
            wordStudy: "Romans 12:1-2",
            confession: "And be not conformed to this world: but be ye transformed by the renewing of your mind, that ye may prove what is that good, and acceptable, and perfect, will of God (Romans 12:2).",
            fullText: <View>
                <Text style={styles.text}>
                    Let's say you're trying to lose weight, but you love doughnuts. How can you
                    overcome temptation? By repeating, “I must not eat doughnuts, I must not eat
                    doughnuts, I must not eat doughnuts”? No, the more you think about the
                    doughnuts, the more you're going to want them. Instead, you must focus on
                    something else (or someone else—Jesus!) who can help you overcome the
                    temptation. Your problem is not in the doughnut shop, it's in your mind. That's
                    where victory is won or lost.
                    <Br />
                    Paul writes: <Bold>“Dear brothers and sisters, I plead with you to give your
                    bodies to God because of all He has done for you. Let them be a living and
                    holy sacrifice—the kind He will find acceptable. This is truly the way to
                    worship Him. Don't copy the behaviour and customs of this world, but let
                    God transform you into a new person by changing the way you think.
                    Then you will learn to know God's will for you, which is good and
                    pleasing and perfect” (Romans 12:1-2 NLT).</Bold> You can lock yourself up in a
                    room and still think about doughnuts. On the other hand, you can get your
                    mind on Jesus, draw strength from Him, and drive victoriously past every
                    doughnut shop in town. The same principle applies to any habit you want to
                    break and any sin you want to conquer. Does victory come easily or
                    overnight? No, satan tempted Jesus repeatedly in the wilderness, and he'll
                    keep tempting you until he realises his strategies no longer work. <Bold>“Then the
                    devil left Him, and behold, angels came and ministered to Him”
                    (Matthew 4:11 NKJV).</Bold> Through Christ, you can conquer your habit.
                </Text>
            </View>,
            prayerPoints: [
                "I receive power to live above every temptation, in the name of Jesus.",
                "I receive power to live victoriously above every sin and improper habit, in the name of Jesus.",
                "I draw strength from You, O Lord, because You are my Life.",
            ]
        },
        {
            day: 11,
            topic: "You Won't Talk about It",
            wordStudy: "Genesis 50:15-21",
            confession: "Look upon mine affliction and my pain; and forgive all my sins (Psalm 25:18).",
            fullText: <View>
                <Text style={styles.text}>
                    Twenty-two years after selling Joseph into slavery, his brothers now stand
                    before him as prime minister of Egypt. They don't recognise him, and he holds
                    their fate in his hands. If you'd been in his shoes, what would you have done?
                    Got even? Reminded them of their past offences? For the next few days, let's
                    look at what Joseph did: he kept it to himself. <Bold>“There was no one with Joseph
                    when he made himself known to his brother” (Genesis 45:1 NIV).</Bold> Joseph
                    made sure no one in Egypt would ever know what they'd done to him. And
                    isn't that how God treats us? The fact is He has enough on each of us to bury us,
                    yet He refuses to resurrect our past sins.
                    <Br />
                    So why do we? To punish! <Bold>“Perfect love drives out fear, because fear
                    has to do with punishment” (1 John 4:18 NIV).</Bold> What are we afraid of? That
                    they'll get away with it. We want them punished, so we tell everybody what
                    happened. And when we do:
                </Text>

                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            <Italic>We play God!</Italic> God says, <Bold>“Vengeance is mine; I will repay” (Romans
                            12:19 KJV).</Bold> He alone knows the weakness in your offenders that caused
                            them to hurt you, and whether they've repented and changed.
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            <Italic>We set the standard by which we ourselves will be judged.</Italic> <Bold>“You will be
                            judged in the same way that you judge others” (Matthew 7:2 NCV).</Bold>
                            If that's a truth you're not comfortable with: <Bold>“Get rid of all bitterness…
                            Instead, be kind to each other, tenderhearted, forgiving one another,
                            just as God… has forgiven you” (Ephesians 4:31-32 NLT).</Bold>
                        </Text>
                    },
                ]}/>

                <Text style={styles.text}>
                    When you've been wronged, “forgive and forbear” is the right response!
                </Text>

            </View>,
            prayerPoints: [
                "Father, turn around every bad situation around me to good, in the name of Jesus.",
                "Help me to genuinely forgive my offenders, in the name of Jesus.",
                "I reject the spirit of fear, in the name of Jesus.",
            ]
        },
        {
            day: 12,
            topic: "You Won't Try to Intimidate Them",
            wordStudy: "Genesis 45:3-5",
            confession: "For God hath not given us the spirit of fear; but of power, and of love, and of a sound mind (2 Timothy 1:7).",
            fullText: <View>
                <Text style={styles.text}>
                    Joseph's brothers <Bold>“were… stunned with surprise. 'Come over here,' he said.
                    So they came closer. And he said again, 'I am Joseph, your brother whom
                    you sold into Egypt! But don't be angry with yourselves that you did this
                    to me, for God did it!'” (Genesis 45:3-5 TLB).</Bold> You'll notice that Joseph didn't
                    react the way we so often do when someone hurts us. He didn't: (1) distance his
                    brothers; (2) enjoy watching them squirm; (3) practise one-upmanship; (4)
                    gloat and say “Gotcha!”; (5) remind them of how they'd put him down and
                    despised his dreams; (6) demand they acknowledge that he was right and they
                    were wrong; (7) say, “I told you so!”
                    <Br />
                    No, Joseph knew at the heart of the matter was God's sovereign purposes
                    being accomplished—even through the evil deeds of his brothers. Having the
                    right view of God's providence freed Joseph and allowed him to seek love
                    from his brothers, not fear—restoration in place of revenge. He knew the longterm
                    benefits of healing a relationship far outweighed any short-term
                    satisfaction you get from retaliation. The Bible also says, <Bold>“You did not
                    receive a spirit…to fear, but… the Spirit of sonship. And by Him we cry,
                    'Abba, Father.”'</Bold> The word <Italic>Abba</Italic> is a term of endearment which means
                    “Daddy.” God doesn't bring up your past, or keep you at arm's length because
                    of your failures. He wants you to know you can come to Him at any time, know
                    that you're accepted, feel secure in His presence, and call Him “Daddy.” And
                    that's the kind of love He wants you to show to others—a love that doesn't want
                    them to feel afraid in your presence.
                </Text>
            </View>,
            prayerPoints: [
                "I reject every spirit of failure, in the name of Jesus.",
                "Father, restore every good thing I have lost, in the name of Jesus.",
                "Holy Spirit, help me to serve God in holiness and righteousness to the end, in the name of Jesus.",
            ]
        },
        {
            day: 13,
            topic: "You Won't Lay a Guilt Trip on Them",
            wordStudy: "Genesis 45:5-7",
            confession: "And we know that all things work together for good to them that love God, to them who are the called according to his purpose (Romans 8:28).",
            fullText: <View>
                <Text style={styles.text}>
                    Joseph told his brothers, <Bold>“Don't be angry with yourselves that you did this
                    to me…God has sent me here to keep you and your families alive, so that
                    you will become a great nation” (Genesis 45:5-7 TLB).</Bold> When are we most
                    likely to lay a guilt trip on others? When we've forgotten the grace we
                    ourselves received from God. A right perspective on God's providence helps
                    us realise that <Bold>“for those who love God, all things work together for good,
                    for those… called according to His purpose” (Romans 8:28 ESV).</Bold> Often
                    we forget the grace and forgiveness God extends to us, expecting us to
                    forgive others in turn!
                    Paul wrote, <Bold>“I…wasted [the Church]” (Galatians 1:13 KJV).</Bold> He uses
                    a mafia term for killing people. Now he goes back to those same towns and
                    preaches, and who's in the audience? The widows and orphans! If Paul hadn't
                    learned to receive God's grace, he could never have fulfilled God's will.
                    Some ancient societies punished murderers by strapping the victim to their
                    back. Paul may have had this in mind when he wrote, <Bold>“Who will deliver me
                    from this body of death?” (Romans 7:24 NKJV).</Bold> Nothing is heavier than
                    unresolved guilt. It will:
                </Text>

                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            <Italic>Drag you down.</Italic>
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            <Italic>Cause you to leave a bad taste wherever you go.</Italic> Even your friends will
                            become exhausted and say, “Get over it.”
                        </Text>
                    },
                    {
                        serial: 3,
                        body: <Text>
                            <Italic>Destroy your relationships.</Italic> Who wants to be around someone who's
                            obsessed with a corpse? You'd only be using the new relationship to
                            numb the pain of the old one.
                        </Text>
                    },
                    {
                        serial: 4,
                        body: <Text>
                            <Bold>Destroy your health, because you aren't built to carry resentment.</Bold>
                        </Text>
                    },
                ]} />

                <Text style={styles.text}>
                    Cut it loose! Somebody said, “Everyone should have a special cemetery
                    lot in which to bury the faults of friends and loved ones.” Grieve if you need
                    to—then bury it and move on!
                </Text>
            </View>,
            prayerPoints: [
                "Father, let my season of joy begin to manifest, in the name of Jesus.",
                "Father, create in me a clean heart according to Your Word, in the name of Jesus.",
                "I reject the spirit of anger in my life, in the name of Jesus.",
            ]
        },
        {
            day: 14,
            topic: "You Will Preserve Their Dignity and Self-worth",
            wordStudy: "Ephesians 5:1-2",
            confession: "Grant me the grace to be a true follower of Christ, in the name of Jesus (Ephesian 5:1).",
            fullText: <View>
                <Text style={styles.text}>
                    Imagine how Joseph's brothers felt when he said, <Bold>“It was not you who sent
                    me here, but God” (Genesis 45:8 NKVJ).</Bold> Is he serious? God did it? This is a
                    new level of forgiveness! When we recognize the hand of God in our lives, we
                    see He wants us to preserve the dignity and self-worth of others because He's
                    already done it for us! With full knowledge of our sinful past, He covers us
                    with the garment of grace. And He expects us to do the same for others. As you
                    read the genealogy of Jesus in Matthew chapter one, you might think the sin
                    of adultery between David and Bathsheba was part of the divine strategy all
                    along. No, sin never is, and David paid a high price. Yet the Bible records
                    these events as though they were supposed to have happened in just that way.
                    <Br />
                    The Bible says, <Bold>“Be full of love for others, following the example of
                    Christ, who loved you and gave Himself to God as a sacrifice to take away
                    your sins. And God was pleased” (Ephesians 5:2 TLB).</Bold> When you truly
                    forgive, there's no place for self-righteousness. You're able to forgive
                    because: (1) you remember what you yourself have been forgiven of; (2) you
                    acknowledge what you're capable of; (3) you see God's hand at work in the
                    bigger picture. Joseph wasn't being condescending, or patronising, nor was he
                    thinking, “I'll be admired for being so gracious.” No, during his years in
                    prison God had moved on his heart and changed his attitude. So when Joseph
                    said, <Bold>“You meant evil against me; but God meant it for good” (Genesis
                    50:20 NKJV),</Bold> he really meant it! That kind of response takes forgiveness to a
                    whole new level!
                </Text>
            </View>,
            prayerPoints: [
                "Father, multiply Your grace upon my life, in the name of Jesus.",
                "Cover me with garment of grace, in the name of Jesus,",
                "Holy Spirit, fill my life with love to the glory of the Father, in the name of Jesus.",
            ]
        },
        {
            day: 15,
            topic: "You Will Protect Them",
            wordStudy: "Genesis 45:9-11",
            confession: "Create in me a clean heart, O God; and renew a right spirit within me (Psalm 51:10).",
            fullText: <View>
                <Text style={styles.text}>
                    Not only did Joseph forgive his brothers, he protected them from their worst
                    nightmare—having to go back and tell their ageing father what they'd done
                    twenty-two years earlier. Joseph is a step ahead of them; he tells them what to
                    say and what not to say: <Bold>“Go up to my father, and say to him, 'Thus says
                    your son Joseph: God has made me lord of all Egypt; come… you shall
                    dwell in the land of Goshen, and you shall be near to me, you and your
                    children… I will provide for you'” (Genesis 45:9-11 NKJV).</Bold> You say, “I
                    think they should have been forced to confess what they'd done.” No, that
                    would have given their father, Jacob, an even greater burden to
                    bear—struggling with regret over his lost years with Joseph, not to mention
                    having to fight bitterness towards his other sons. Joseph was wise. And it
                    made his brothers respect him all the more.
                    <Br />
                    There's a big difference between confessing, and “dumping.” Irreparable
                    damage can be done when you try to get relief by dumping the details of your
                    guilt on somebody who can't handle them. Sometimes confessing is the
                    proper route, but only after talking with an experienced counsellor. After
                    David sinned with Bathsheba, he wrote, <Bold>“Against You [God], You only,
                    have I sinned” (Psalm 51:4 NKJV).</Bold> When you consider that God knows all
                    about your sin— yet promises to keep it a closely guarded secret, it should:
                    (1) increase your sense of humility and gratitude; (2) cause you to keep your
                    mouth shut; (3) make you refuse to hold anybody else's sins and
                    shortcomings over their head.
                </Text>
            </View>,
            prayerPoints: [
                "Lord Jesus, today, wash away by Your blood every bitterness in my life.",
                "Father, let not sin have dominion over me, in the name of Jesus.",
                "Grant me the grace to bridle my tongue, in the name of Jesus.",
            ]
        },
        {
            day: 16,
            topic: "You Will Walk in Forgiveness",
            wordStudy: "Genesis 50:15-21",
            confession: "I receive grace to forgive my offenders from the heart, in the name of Jesus.",
            fullText: <View>
                <Text style={styles.text}>
                    Seventeen years after being reunited with Joseph, his long lost son, Jacob
                    died and Joseph's brothers panicked. They started to think, <Bold>“Now Joseph
                    will pay us back for all the evil we did to him” (Genesis 50:15 TLB).</Bold> So
                    they got together, made up a story, and sent word to Joseph, saying, <Bold>“Before
                    your father died, he instructed us to say to you: 'Please forgive your
                    brothers for the great wrong they did to you'” (Genesis 50:16-17 NLT).</Bold>
                    Now think about it. If their father really had said this, he wouldn't have told
                    Joseph's brothers, he'd have told Joseph himself, right? He wouldn't have
                    gone to his grave with the fear that Joseph might exact revenge. When Joseph
                    heard that his brothers doubted his forgiveness he called them together and
                    wept, saying, <Bold>“Don't be afraid… I myself will take care of you and your
                    families.” '…And he spoke very kindly to them, reassuring them'
                    (Genesis 50:21 TLB).</Bold>
                    <Br />
                    True forgiveness, the kind that's taught in Scripture, is a commitment
                    you must practise every day of your life. People need loving the most—when
                    they deserve it the least. No one ever said it would be easy. If Jesus had
                    waited until His enemies repented, He'd never have prayed on the cross:
                    <Bold>“Father, forgive them, for they do not know what they do” (Luke 23:34
                    NKJV).</Bold> Sure, it's easier to forgive when others acknowledge their offence.
                    But if that's a prerequisite, you may never experience victory! And what you
                    don't forgive—you have to relive! So for your own sake forgive, take back
                    your life, and begin walking in the blessing of the Lord.
                </Text>
            </View>,
            prayerPoints: [
                "Lord, turn around for me every bad situation for good my good, in the name of Jesus.",
                "Help me to genuinely forgive my offenders, in the name of Jesus.",
                "I reject the spirit of fear, in the name of Jesus.",
            ]
        },
        {
            day: 17,
            topic: "You Will Ask God to Bless Them",
            wordStudy: "Matthew 5:44-45",
            confession: "The Lord shall bless me as I forgive my offenders, in the name of Jesus (Job 42:10).",
            fullText: <View>
                <Text style={styles.text}>
                    Walking in love and forgiveness is difficult on several fronts: (1) It goes
                    against your carnal nature. (2) There's a chance others may never know you
                    forgave the offence. (3) Your heart could break as you watch God bless the
                    offender in answer to your prayers—as if they'd never sinned against you in
                    the first place. John Calvin pointed out that praying like this “is exceedingly
                    difficult,” and early Church theologian John Chrysostom called it “the
                    highest summit of self-control.” The Bible says Job's suffering ended and his
                    prosperity was restored once he was able to pray for those who'd become a
                    thorn in his side <Bold>(Job 42:10).</Bold> When you pray this way, you put into practice
                    the words of Jesus in His famous Sermon on the Mount: <Bold>“But I say to you,
                    love your enemies, bless those who curse you, do good to those who hate
                    you, and pray for those who spitefully use you and persecute you, that
                    you may be sons of your Father in Heaven” (Matthew 5:44-45 NKJV).</Bold>
                    That's Christ's standard of forgiveness, and it's a high one.
                    <Br />
                    Maybe you're wondering how anybody could possibly live that way.
                    Look at the life and death of Stephen, the first Christian martyr. Even while
                    his enemies were stoning him, he prayed, <Bold>“Lord, do not hold this sin
                    against them” (Acts 7:60 NIV).</Bold> Therein lay one of the secrets of Stephen's
                    great effectiveness. True forgiveness is the medicine that heals the deepest
                    emotional wounds. It closes the door on the past, and gives you grace and
                    motivation to move forward and enjoy the life God wants you to live.
                </Text>
            </View>,
            prayerPoints: [
                "Grace to love others with the love of God, fall on me, in the name of Jesus.",
                "Holy Spirit, help me to genuinely forgive my offenders, in the name of Jesus.",
                "I receive grace to be good to my offenders, in the name of Jesus.",
            ]
        },
        {
            day: 18,
            topic: "Billy",
            wordStudy: "1 Corinthians 1:27-31",
            confession: "The joy of the Lord is my strength, in the name of Jesus.",
            fullText: <View>
                <Text style={styles.text}>
                    Writing about his time at a teen Bible camp, a counsellor tells this story:
                    “Teenage boys have a tendency to pick on some unfortunate child. That
                    summer it was 13-year-old Billy, a child who couldn't walk or talk right.
                    When the children from his cabin were assigned to lead devotions, they voted
                    Billy in as the speaker. It didn't seem to bother him. He dragged himself up to
                    the pulpit amid sneers and snickers, and it took him a long time to stammer,
                    'Je–sus loves… me… and… I… love Je–sus.'
                    <Br />
                    “There was stunned silence, and when I looked around there were boys
                    with tears streaming down their cheeks. We'd done many things to try to
                    reach these boys, but nothing had worked. We'd even brought in famous
                    baseball players whose batting averages had gone up since they started
                    praying, but it had no effect. It wasn't until a special-needs child declared his
                    love for Christ that everything changed. I travel a lot and it's surprising how
                    often I meet people who say, 'You probably don't remember me. I became a
                    Christian at a camp where you were a counsellor, and do you know what the
                    turning point was for me?' I never have to ask. I always know I'm going to
                    hear—Billy!”
                    <Br />
                    The Bible says, <Bold>“God chose things the world considers foolish in
                    order to shame those who think they are wise” (1 Corinthians 1:27 NLT).</Bold>
                    So when you find yourself focusing on what you can't do, remember His
                    <Bold>“power works best in [your] weakness” (2 Corinthians 12:9 NLT).</Bold> Just
                    do what you can, and God will do the rest! He'll crown your efforts with
                    success.
                </Text>
            </View>,
            prayerPoints: [
                "Lord, turn every bad situation around me for my good and Your glory, in the name of Jesus.",
                "Lord, crown my efforts with success, in the name of Jesus.",
                "No matter the situation, Lord, help me to always know that You love me, in the name of Jesus.",
            ]
        },
        {
            day: 19,
            topic: "Different Types of Prayer (1)",
            wordStudy: "Ephesians 6:16-19",
            confession: "Confess your faults one to another, and pray one for another, that ye may be healed. The effectual fervent prayer of a righteous man availeth much (James 5:16).",
            fullText: <View>
                <Text style={styles.text}>
                    In Scripture there are many different kinds of prayer. Let's look at some of
                    them and see what we can learn:
                </Text>

                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            <Italic>The prayer of surrender.</Italic> When Paul met Christ on the Damascus Road he
                            prayed, <Bold>“Lord, what do You want me to do?” (Acts 9:6 NKJV).</Bold> That's
                            like signing your name to a blank cheque and saying, “Here I am, Lord,
                            do with me as You please. I hope I like what You choose, but even if I
                            don't, I'll do it anyway; Your will be done, not mine.” You're deciding to
                            voluntarily follow God rather than trying to get Him to follow you. As a
                            result He will do the work that needs to be done in you, so that He can do
                            the work He desires to do through you.
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            <Italic>The prayer of commitment.</Italic> The Bible says, <Bold>“Casting the whole of your
                            care [all your anxieties, all your worries, all your concerns, once and
                            for all] on Him” (1 Peter 5:7 AMP).</Bold> As long as you keep trying to
                            control everything, your stress levels will keep mounting. But once you
                            learn to hand things over to God, you'll wonder why you spent even a
                            single day worrying.
                        </Text>
                    },
                    {
                        serial: 3,
                        body: <Text>
                            <Italic>The prayer of intercession.</Italic> The prophet Ezekiel writes, <Bold>“I looked for
                            someone… who would… stand before me in the gap on behalf of the
                            land” (Ezekiel 22:30 NIV).</Bold> 'The gap' is the distance between what
                            is—and what can be. And when there's a “gap” in someone's relationship
                            with God due to a particular sin, as a believer you have the privilege (and
                            responsibility) of placing yourself in that gap and praying for them.
                        </Text>
                    },
                ]} />
            </View>,
            prayerPoints: [
                "Spirit of intercession, fall on me, in the name of Jesus.",
                "Holy Spirit, help me to tarry at the altar of prayer, in the name of Jesus.",
                "I will not pray amiss, in the name Jesus.",
            ]
        },
        {
            day: 20,
            topic: "Different Types of Prayer (2)",
            wordStudy: "Mark 11:22-26",
            confession: "God shall grant me the grace to pray without ceasing, in the name of Jesus (1 Thessalonians 5:17).",
            fullText: <View>
                <Text style={styles.text}>
                    Here are three more types of prayer:
                </Text>

                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            <Italic>The prayer of petition.</Italic> You must learn to be confident in asking God to
                            meet your needs. Jesus promised, <Bold>“Therefore I say to you, whatever
                            things you ask when you pray, believe that you receive them, and you
                            will have them” (Mark 11:24 NKJV).</Bold> If we'd stop trying to impress
                            God, we'd be a lot better off. Length, loudness or eloquence isn't the
                            issue; it's the sincerity of our heart, the faith that's in our heart, and the
                            assurance that we're praying according to God's will that gets results.
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            <Italic>The prayer of agreement.</Italic> <Bold>“All these with one accord were devoting
                            themselves to prayer…” (Acts 1:14 ESV).</Bold> When you're up against
                            something too big to handle alone, find a prayer partner and get into
                            agreement with them. This isn't for people who constantly live in strife,
                            then decide to agree because they're desperate. God honours the prayers
                            of those who pay the price to live together in harmony <Bold>(See Psalm
                            133:1.).</Bold>
                        </Text>
                    },
                    {
                        serial: 3,
                        body: <Text>
                            <Italic>The prayer of thanksgiving.</Italic> When your prayers outnumber your praises,
                            it says something about your character. Self-centred people ask, but
                            rarely appreciate. God won't release us into the fullness of all He has
                            planned for us until we become thankful for what we've already
                            received. Petition avails much; praise avails much more! <Bold>“In every
                            situation, by prayer and petition, with thanksgiving, present your
                            requests to God” (Philippians 4:6 NIV).</Bold> Powerful living comes
                            through thanksgiving. We can literally <Bold>“pray without ceasing” (1
                            Thessalonians 5:17 KJV)</Bold> by being thankful all day long, praising God
                            for His favour, mercy, lovingkindness, grace, longsuffering and
                            goodness.
                        </Text>
                    },
                ]} />
            </View>,
            prayerPoints: [
                "Father, remove every obstacle to my prayers, in the name of Jesus.",
                "Father, hear me everytime I call, in the name of Jesus.",
                "Goodness and mercy shall follow me all the days of my life, in the name of Jesus.",
            ]
        },
        {
            day: 21,
            topic: "Remember to Say “Thank You”",
            wordStudy: "Psalm 103:1-5",
            confession: "Bless the LORD, O my soul, and forget not all his benefits (Psalm 103:2).",
            fullText: <View>
                <Text style={styles.text}>
                    Gratitude comes with a host of benefits. It improves your heart rhythm,
                    reduces stress, and helps you heal physically and think more clearly under
                    pressure. It floods your body and brain with endorphins that strengthen and
                    rejuvenate you. And like any muscle, the more you exercise it the stronger it
                    grows. It doesn't have to be complicated; just take a walk and think about your
                    blessings and it will set the tone for your day. The psalmist said, <Bold>“Praise the
                    Lord and do not forget all His kindnesses” (Psalm 103:2 NCV).</Bold> God's
                    blessings operate 24 hours a day, 365 days a year. Try this: When you sit down
                    to eat, have everyone at the table name something they're thankful for. There's
                    always something. An elderly lady at a nursing home said, “I thank you, Lord,
                    for two good teeth, one upper and one lower. And I thank you that they meet!”
                    <Br />
                    Psychologist Martin Seligman suggests sending a letter or email of
                    gratitude to somebody, then visiting that person and reading it to them. People
                    who say “thank you” are measurably happier and less depressed. The CEO of
                    Campbell Soup wrote over sixteen thousand thank–you notes to his
                    employees, and energised the entire company in the process. Go ahead,
                    encourage your friends and co-workers by letting them know you appreciate
                    what they do. The Bible says, <Bold>“…be peaceable and considerate”</Bold> One author
                    observes: “You have it in your power to increase the sum total of the world's
                    happiness by giving a few words of sincere appreciation to someone who's
                    lonely or discouraged. Perhaps you'll forget the kind words you say today, but
                    the recipient may cherish them for a lifetime.”
                </Text>
            </View>,
            prayerPoints: [
                "Father, I receive grace today to appreciate all the days of my life, in the name of Jesus.",
                "May the name of the Lord be glorified in my life for ever, in the name of Jesus.",
                "Let the joy of the Lord be my strength, in the name of Jesus.",
            ]
        },
        {
            day: 22,
            topic: "Today, Speak Words that Encourage",
            wordStudy: "Romans 14:16-20",
            confession: "Follow peace with all men, and holiness, without which no man shall see God (Hebrews 12:14).",
            fullText: <View>
                <Text style={styles.text}>
                    The Bible says, <Bold>“Let us pursue what makes for… mutual upbuilding”
                    (Romans 14:19 ESV).</Bold> When Job was in trouble, his friend Eliphaz reminded
                    him how in the past Job's words had <Bold>“encouraged those who were weak or
                    falling.”</Bold> Words can hurt or heal, bless or blister, destroy or deliver, tear down
                    or build up. <Bold>“The tongue has the power of life and death” (Proverbs 18:21
                    NIV).</Bold>
                    <Br />
                    Jon Walker writes: “You …the one with Jesus in your heart—are capable
                    of murder. And so am I. We have the power to speak death with our words,
                    and… the power to speak life. Perhaps you've been on the receiving end of a
                    message meant to murder. 'You're not smart enough… thin enough… fast
                    enough… good enough… a real Christian wouldn't think such things.' In a
                    world where people are beaten up and put down, God gives you superhero
                    power to punch through the negativity. You speak life when you say, 'You
                    matter to me. I like you just the way you are… Your life counts. You were
                    created for a purpose. God loves you, and you're incredibly valuable to Him.'
                    You can become the voice of God's grace in the lives of others, supporting,
                    loving, helping and encouraging them with the words that flow from your
                    mouth.”
                    <Br />
                    God wants us to encourage each other, but that doesn't mean flattering or
                    buttering people up. It means speaking words that help them to stay on their
                    feet and keep going. What you say can give fresh hope to a friend, a relative, a
                    neighbour, or a co-worker who's about to collapse. What a gift!
                </Text>
            </View>,
            prayerPoints: [
                "Father, I receive grace to speak the right word in season, in the name of Jesus.",
                "By Your grace, O Lord, let me fulfill Your purpose for my life gloriously, in the name of Jesus.",
                "The words of my mouth shall not lead me into trouble, in the name of Jesus.",
            ]
        },
        {
            day: 23,
            topic: "Depend on God",
            wordStudy: "2 Corinthians 12:7-10",
            confession: "The grace of the Lord is sufficient for me, in the name of Jesus",
            fullText: <View>
                <Text style={styles.text}>
                    Hindrances, hang-ups and hurdles are God's gift to the self-sufficient. While
                    He won't let you use your weakness as a crutch or a cop-out, He'll allow it to
                    keep you dependent on Him. Paul wrote, <Bold>“I was given a thorn…to…keep
                    me from becoming proud” (2 Corinthians 12:7 NLT).</Bold> Why would God
                    keep you in touch with your limitations? To embarrass you? No, to empower
                    you so that you can do His will. God's intention is to increase, not decrease,
                    your need for Him.
                    <Br />
                    Perhaps this illustration will help you. Imagine four steel rings. The first
                    can support 80 kg, the second 60 kg, the third 40 kg, and the fourth 20 kg.
                    Linked together, what's the greatest weight the chain can support? 200 kg?
                    No, a chain is only as strong as its weakest link, so the answer is 20 kg! And
                    it's the same with us; we're only as strong as our weakest area. That's why we
                    sometimes try to excuse or ignore them. But that's dangerous because relying
                    on your own strength may win you a few victories and accolades and cause
                    you to think you can handle everything on your own.
                    <Br />
                    It was because Paul was so brilliant that God permitted difficult
                    circumstances that kept him on his knees, living in a state of forced
                    dependence. After praying repeatedly for God to take his weakness away,
                    Paul finally came to the place where he could say, <Bold>“I will boast all the more
                    gladly about my weaknesses, so that Christ's power may rest on me” (2
                    Corinthians 12:9 NIV).</Bold> So today, depend on God!
                </Text>
            </View>,
            prayerPoints: [
                "Father, grant me the grace to serve You to the end, in the name of Jesus.",
                "Deliver me from the powers that are stronger than I, in the name of Jesus.",
                "Father, let all my trust and confidence be in You and You alone, in the name of Jesus.",
            ]
        },
        {
            day: 24,
            topic: "A Faith Perspective",
            wordStudy: "Philippians 1:19-21",
            confession: "For surely there is an end; and thine expectation shall not be cut off, in the name of Jesus (Proverbs 23:18).",
            fullText: <View>
                <Text style={styles.text}>
                    Roman prisons were terrible places. Offenders were stripped, flogged
                    and placed in leg irons. Their blood-soaked clothing wasn't changed
                    even in the dead of winter. And the <Bold>“inner cell” (Acts 16:24 NIV)</Bold>
                    where Paul and Silas were imprisoned was the worst. Lack of water,
                    cramped conditions and the stench of toilets (if that's what you could
                    call them) made sleep impossible. Prisoners routinely begged for
                    death, and some even committed suicide. It was your worst nightmare!
                    Yet <Bold>“Paul and Silas were…singing…and the other prisoners were
                    listening” (Acts 16:25 NIV).</Bold>
                    <Br />
                    Paul's attitude impressed his fellow inmates before his religious beliefs
                    ever reached them. Let's face it, anybody can sing in church, including
                    hypocrites. But when you can praise God in the midst of pain, pressures, and
                    problems—that's something else. How did they do it? They had a faith
                    perspective! It's not what you have lost, but what you have left that counts!
                    Paul didn't just sing in prison, he wrote some of his best stuff there. Here's his
                    take on it: <Bold>“Through my being in prison, the Lord has given most of our
                    brothers and sisters confidence to speak God's word more boldly and
                    fearlessly than ever… I will speak very boldly and honour Christ… now
                    as always, whether I live or die. Christ means everything to me in this life,
                    and when I die I'll have even more” (Philippians 1:19-21 GWT).</Bold>
                    What are you going to do with a man or woman like this? They're beyond
                    your threats. Their strength comes from a source that's not diminished by
                    outside circumstances. That's because they have a faith perspective. And
                    that's what you need today too!
                </Text>
            </View>,
            prayerPoints: [
                "Father, break every chain holding down my progress, in the name of Jesus.",
                "I receive all my strength from You, in the name of Jesus.",
                "Increase and strengthen my faith this day for always, in the name of Jesus.",
            ]
        },
        {
            day: 25,
            topic: "Peace of Mind",
            wordStudy: "Mark 4:35-41",
            confession: "In his days shall the righteous flourish; and abundance of peace so long as the moon endureth (Psalm 72:7).",
            fullText: <View>
                <Text style={styles.text}>
                    You can control what goes on in your mind by filling it with God's Word. Not
                    the Word you read casually, but the Word you process mentally, apply to each
                    situation that arises, and stand on in times of crisis because you know it's your
                    right to have the peace Jesus promised. Jesus corrected His disciples because
                    they lost their peace of mind during a storm. He didn't lose His. He was asleep
                    in the back of the boat. So where are you today? Resting with Jesus in the
                    back of the boat, or panicking with the others up front? Worry overwhelms
                    you when you forget two things:
                </Text>

                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            <Italic>What the Lord has told you.</Italic> Jesus said, <Bold>“Let us go over to the other
                            side” (Mark 4:35 NIV).</Bold> And once He spoke those words there wasn't a
                            wave big enough to sink them. Anytime you're doing what God's told
                            you to do, you may go through storms but you won't sink.
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            <Italic>Who's with you in the boat.</Italic> The disciples thought they knew Jesus pretty
                            well, but before the night was over they were asking, <Bold>“Who is this?
                            Even the wind and the waves obey Him!” (Mark 4:41 NIV).</Bold> Has it
                            ever occurred to you that the storm you're in right now has been
                            permitted by God to show you that you don't have a problem He can't
                            solve; that you're not alone, and that through this experience you'll come
                            to know Him better? In the Amplified Bible the words of Jesus are
                            translated like this: <Bold>“Do not let your hearts be… distressed,
                            agitated”(John 14:1 AMP).</Bold> The only power worry has over you—is
                            the power you give it.
                        </Text>
                    },
                ]} />
            </View>,
            prayerPoints: [
                "Father, let every storm in my life cease forthwith, in the name of Jesus.",
                "Let the Word of God find home in and saturate my life, in the name of Jesus.",
                "I shall experience peace round about, in the name of Jesus.",
            ]
        },
        {
            day: 25,
            topic: "Peace of Mind",
            wordStudy: "Mark 4:35-41",
            confession: "In his days shall the righteous flourish; and abundance of peace so long as the moon endureth (Psalm 72:7).",
            fullText: <View>
                <Text style={styles.text}>
                    You can control what goes on in your mind by filling it with God's Word. Not
                    the Word you read casually, but the Word you process mentally, apply to each
                    situation that arises, and stand on in times of crisis because you know it's your
                    right to have the peace Jesus promised. Jesus corrected His disciples because
                    they lost their peace of mind during a storm. He didn't lose His. He was asleep
                    in the back of the boat. So where are you today? Resting with Jesus in the
                    back of the boat, or panicking with the others up front? Worry overwhelms
                    you when you forget two things:
                </Text>

                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            <Italic>What the Lord has told you.</Italic> Jesus said, <Bold>“Let us go over to the other
                            side” (Mark 4:35 NIV).</Bold> And once He spoke those words there wasn't a
                            wave big enough to sink them. Anytime you're doing what God's told
                            you to do, you may go through storms but you won't sink.
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            <Italic>Who's with you in the boat.</Italic> The disciples thought they knew Jesus pretty
                            well, but before the night was over they were asking, <Bold>“Who is this?
                            Even the wind and the waves obey Him!” (Mark 4:41 NIV).</Bold> Has it
                            ever occurred to you that the storm you're in right now has been
                            permitted by God to show you that you don't have a problem He can't
                            solve; that you're not alone, and that through this experience you'll come
                            to know Him better? In the Amplified Bible the words of Jesus are
                            translated like this: <Bold>“Do not let your hearts be… distressed,
                            agitated”(John 14:1 AMP).</Bold> The only power worry has over you—is
                            the power you give it.
                        </Text>
                    },
                ]} />
            </View>,
            prayerPoints: [
                "Father, let every storm in my life cease forthwith, in the name of Jesus.",
                "Let the Word of God find home in and saturate my life, in the name of Jesus.",
                "I shall experience peace round about, in the name of Jesus.",
            ]
        },
        {
            day: 26,
            topic: "What Your Child Deserves (1)",
            wordStudy: "Psalm 127:1-5",
            confession: "My children shall not depart from the way of the Lord, in the name of Jesus (Proverbs 22:6).",
            fullText: <View>
                <Text style={styles.text}>
                    Your children deserve certain things, like:
                </Text>

                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            <Italic>Time.</Italic> Not leftover time at the end of the day, but prioritised time. If your
                            life is ruled by a schedule and your children aren't on it, do
                            something—quickly. Otherwise, there'll come a day when you're not
                            included in their schedule. Simply watching television together for three
                            hours won't cut it; you must be “emotionally present.” Sometimes that
                            means letting them see your fears and insecurities, even as they witness
                            your delight and appreciation of them.
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            <Italic>Openness.</Italic> There's so much our children can teach us about themselves,
                            about ourselves, and about who God is. Once we realise we don't have
                            all the answers, we become open to allowing God to speak to us through
                            our children. That kind of receptivity strengthens their faith, helps them
                            remain teachable, and also keeps us young at heart.
                        </Text>
                    },
                    {
                        serial: 3,
                        body: <Text>
                            <Italic>Structure.</Italic> It's vital, during the formative years, to establish rules and
                            maintain boundaries. Children need guidelines and a framework to feel
                            secure. In the early years this includes things like having an established
                            bedtime, then moving it back as they get older. This helps them
                            understand that age brings freedom, but not all at once, because freedom
                            brings responsibility and they're not as ready to handle it as they think.
                        </Text>
                    },
                ]} />
            </View>,
            prayerPoints: [
                "Father, I receive grace to train my children in Your way, in the name of Jesus.",
                "My children shall be for signs and wonders to the praise of Your glory, in the name of Jesus.",
                "Grant me the wisdom to train them in the knowledge of You, in the name of Jesus.",
            ]
        },
        {
            day: 27,
            topic: "What Your Child Deserves (2)",
            wordStudy: "Genesis 48:5-9",
            confession: "The children God has given me are for signs and wonders, in the name of Jesus (Isaiah 8:18)",
            fullText: <View>
                <Text style={styles.text}>
                    Here are three more things your children deserve from you:
                </Text>

                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            <Italic>Forgive them, and be willing to ask for their forgiveness.</Italic> By doing this
                            you're teaching them that: 
                            <Br single />
                            <Bold>(a)</Bold> We must all deal with the consequences of
                            our actions. And that when we do, we grow. 
                            <Br single />
                            <Bold>(b)</Bold> Failing doesn't make
                            you a failure; it's just part of learning and maturing. It comes with the
                            turf. 
                            <Br single />
                            <Bold>(c)</Bold> We should be quick to extend to others the same grace that has
                            so often been extended to us.
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            <Italic>Separate the baggage.</Italic> One man became anxious and depressed as his
                            son approached his twelfth birthday. Shortly after the boy's birthday
                            party, the father was thumbing through a photo album from his own
                            childhood. That's when it dawned on him that he was twelve when his
                            father abandoned the family and then killed himself. Watching his son
                            approach the same age made him afraid because it reopened old
                            wounds—unhealed ones. A caring counsellor helped him regain his
                            perspective and peace by helping him realise he was a very different
                            man from his father, and he wasn't about to abandon his family
                        </Text>
                    },
                    {
                        serial: 3,
                        body: <Text>
                            <Italic>Bless them.</Italic> <Bold>'“They are the sons God has given me here,' Joseph said
                            to his father. Then Israel said, 'Bring them to me so I may bless
                            them”' (Genesis 48:9 NIV).</Bold> The principles you live by and the
                            blessings you enjoy are meant to be passed on to your children and
                            grandchildren. Whether it's expressing what's in your heart, or sending a
                            note or email to say you're proud of them, bless your children at every
                            possible opportunity.
                        </Text>
                    },
                ]} />
            </View>,
            prayerPoints: [
                "Lord, baptize me with the spirit of forgiveness, in the name of Jesus.",
                "I reject every evil family pattern, in the name of Jesus.",
                "Lord, make me a blessing to my children in every way, in the name of Jesus.",
            ]
        },
        {
            day: 28,
            topic: "What Your Child Deserves (3)",
            wordStudy: "Deuteronomy 4:40",
            confession: "The Lord shall watch over my children, in the name of Jesus.",
            fullText: <View>
                <Text style={styles.text}>
                    Sometimes we get scared because our kids remind us so much of ourselves.
                    We see in them the same fears and proclivities that we have struggled with.
                    We watch them veering off life's highway in some of the same spots where
                    we crashed and burned and it's difficult not to want to save them. But
                    sometimes we can't. They've grown up with their own mindset, strengths and
                    dreams. And if the parable of the prodigal son teaches us anything, it's that
                    good parents can raise children who are only capable of learning the hard
                    way. So what can you do? Pray for them—and “be there” when they return.
                    Many a successful adult was once a prodigal saved by the prayers of a parent
                    who refused to give up on them.
                    <Br />
                    If what you're doing is taking you away from prayer time for your
                    children, your priorities are wrong. There's nothing more valuable than the
                    time you spend before God interceding on their behalf. You say, “But I don't
                    know how to pray.” Try this: “Father, I'm concerned about the direction my
                    children are taking. Right now they seem beyond the reach of my voice and
                    influence. But You can reach them. You can remind them of what they've
                    been taught—and initiate the circumstances that will bring them back to You.
                    Your Word says if I obey You, things will go well for me and my children
                    <Italic>(Deuteronomy 4:40).</Italic> So I stand on Your promise, believing they'll choose to
                    serve You and walk in Your blessing for the rest of their lives. In Christ's
                    name I pray. Amen.”
                </Text>
            </View>,
            prayerPoints: [
                "Bless God for the vision of our Parents Interceding programme at CLAM.",
                "My children shall be a source of joy to me, in the name of Jesus.",
                "Father, make me an intercessor, not only for my children but also for others' children, in the name of Jesus.",
            ]
        },
        {
            day: 29,
            topic: "When You Belittle, You Become Little",
            wordStudy: "Ephesians 4:29-32",
            confession: "Out of my mouth shall come forth always words that build others up, not words that bring them down, in the name of Jesus.",
            fullText: <View>
                <Text style={styles.text}>
                    Here are four questions you need to be able to answer:
                </Text>
                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            Do people feel better about themselves after spending time with you?
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            Are your expectations so high that you focus on people's shortcomings
                            instead of their strengths?
                        </Text>
                    },
                    {
                        serial: 3,
                        body: <Text>
                            When somebody speaks well of a person you don't particularly like, do
                            you feel the need to inject a disparaging remark?
                        </Text>
                    },
                    {
                        serial: 4,
                        body: <Text>
                            Are you so insecure and lacking in self-worth that you only feel good
                            about yourself by putting others down?
                        </Text>
                    },
                ]} />
                <Text style={styles.text}>
                    Well, how did you do on the test? If you're not four-for-four, it's time to
                    spend time with God in prayer and ask Him to help you change your attitude
                    and what comes out of your mouth. Paul says, <Bold>“Do not let any unwholesome
                    talk come out of your mouths, but only what is helpful for building others
                    up according to their needs, that it may benefit those who listen”
                    (Ephesians 4:29 NIV).</Bold> Words are like hammers: they can be used to break
                    down or build up. It all depends on the person swinging the hammer! It's just
                    as easy to be part of the construction crew as it is to be a member of the
                    wrecking crew.
                    <Br />
                    Make a habit of encouraging your family and your friends. Let your wife
                    know she's the only woman in the world for you. Express appreciation for
                    your husband's care and sense of responsibility. Applaud your teenager for
                    avoiding drugs and alcohol. Thank your friends for keeping your secrets!
                    Accept people as they are, and resist the temptation to constantly “fix”
                    something about them—that's God's job, not yours. Remember, you only have
                    them for a short time!
                </Text>
            </View>,
            prayerPoints: [
                "I receive grace for humility, in the name of Jesus.",
                "Let the joy of the Lord be my strength, in the name of Jesus.",
                "Father, help me to live in peace with people around me, in the name of Jesus.",
            ]
        },
        {
            day: 30,
            topic: "Take Time to Rest and Sharpen",
            wordStudy: "Mark 6: 30-31",
            confession: "Come ye yourselves apart…, and rest a while (Mark 6:31 KJV).",
            fullText: <View>
                <Text style={styles.text}>
                    A competition was held between two men to see who could chop down the
                    most trees in a single day. One man was older and more experienced, while
                    the other was younger and less experienced. And that's where the difference
                    showed up. The younger man spent eight hours chopping down trees, and at
                    the end of the day he had a total of twenty-five. Believing the older man
                    lacked stamina and youth, he sat down, fully confident he would win.
                    Meanwhile the older man, who had taken a ten-minute break each hour,
                    ended his day by chopping down forty trees.
                    <Br />
                    In shock the younger man asked, “How is this possible, old man? I didn't
                    stop. You stopped every hour for ten minutes and yet you chopped down
                    almost twice as many trees as I did.” The older man replied, “Every hour I sat
                    down for ten minutes and did two things. First, I took time to rest and
                    recharge my batteries. Second, I took time to sharpen my axe. Yes, you were
                    working hard but you were working with a dull axe.” There's an important
                    lesson here for you. In order to succeed at what God has called you to do in
                    life, you must always do these two things:
                </Text>
                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            <Italic>Make time for rest and renewal.</Italic> You cannot always be giving out; you
                            must also stop and take in. That's where prayer and Bible reading come
                            in: they restore what life depletes.
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            <Italic>Stay sharp.</Italic> When your axe is dull, it requires more energy and produces
                            fewer results. So take time to rest and sharpen.
                        </Text>
                    },
                ]} /> 
            </View>,
            prayerPoints: [
                "Father, cause me to experience peace round about, in the name of Jesus.",
                "I receice grace to fulfill Your purpose for my life, in the name of Jesus.",
                "Father, let Your grace be sufficient for me always, in the name of Jesus.",
            ]
        },
        {
            day: 31,
            topic: "Stop Being So Critical",
            wordStudy: "Numbers 12:11-16",
            confession: "Set guard, O LORD, over my mouth; keep watch over the door of my lips, in the name of Jesus (Psalm 141:3).",
            fullText: <View>
                <Text style={styles.text}>
                    When Miriam criticised her brother Moses because of the wife he chose, the
                    Lord heard and she was smitten with leprosy. Maybe you grew up in a family
                    that was forever finding fault, and now you hear the same tone in how you
                    talk to your children. You can't enjoy God's blessings because you've been
                    programmed to inspect, nitpick and form opinions—usually negative ones.
                    The Bible says, <Bold>“If you want to enjoy life and see…happy days, keep your
                    tongue from speaking evil” (1 Peter 3:10 NLT).</Bold> The definition of the word
                    <Italic>criticism</Italic> (dwelling upon the perceived faults of another with no view to their
                    good) should cause you to pause and think.
                    <Br />
                    First, there's the word perceived. Often your perceptions aren't accurate;
                    there are always circumstances you don't understand. Next we come to the
                    words <Italic>dwelling upon.</Italic> We're inclined to walk through life saying, “That's not
                    right,” or “I wouldn't do it that way.” You say, “I'm an analytical person, it's
                    how God made me.” That's fine, but the problem comes when you choose to
                    dwell on your observations—when you can't set them aside. You say, “But
                    how can I help somebody if I don't dwell on what they're doing?” That's why
                    the last part of the definition—with no view to their good—is so important.
                    It's not wrong to dwell upon somebody's faults, provided you do it in a nonjudgmental
                    way with a view to helping them find a solution. Does that mean
                    it's OK to discuss it with a third party? Only if you can end the conversation
                    by saying, “Let's pray about it, keep it in confidence, and try to help.”
                </Text>
            </View>,
            prayerPoints: [
                "Lord, let the fruit of the Spirit manifest in my life, in the name of Jesus.",
                "Father, deliver me from the spirit of self-righteousness, in the name of Jesus.",
                "I frustrate and nullify the works of the spirit of pride in my life, in the name of Jesus.",
            ]
        },
    ],
    September: [
        {
            day: 1,
            topic: "The Power to Solve the Problem",
            wordStudy: "Romans 8:35-39",
            confession: "Despite all my challenges, overwhelming victory is mine, in the precious name of Jesus (Romans 8:37).",
            fullText: <View>
                <Text style={styles.text}>
                    Everyone has problems. The only people who don't—are in cemeteries. The
                    real problem is—how we handle our problems. Often we try to solve them
                    with our own power. How can you tell when you're doing that? Because
                    you're tired all the time! You're like the guy who said, “I'm sick and tired of
                    being sick and tired.” Or we're like the lady who said, “Just about the time I
                    manage to make ends meet, somebody moves the ends.” We say things like,
                    “I'm doing OK under the circumstances.” Well, what are you doing <Italic>under</Italic>
                    them? Someone has said that circumstances are like a mattress; if you're on
                    top you rest easy, but if you're underneath you suffocate!
                    <Br />
                    What's the answer? Take your focus off your problems and put it on
                    God's promises. Here's one of them: <Bold>“Can anything ever separate us from
                    Christ's love? Does it mean He no longer loves us if we have trouble? …”
                    (Romans 8:35 NLT).</Bold> Paul answers, <Bold>“No, despite all these things,
                    overwhelming victory is ours through Christ, who loved us” (Romans
                    8:37 NLT).</Bold> The word <Italic>conqueror</Italic> means “one who overcomes by gaining
                    control.” And Paul says that we are <Bold>“more than conquerors” (Romans 8:37
                    NKJV).</Bold> To be a conqueror means to fight a battle and win. To be more than a
                    conqueror means to win a battle without having to fight. Jesus fights on your
                    behalf; He does it in you, and through you, and that makes you “more than” a
                    conqueror.
                </Text>
            </View>,
            prayerPoints: [
                "I repent of faithlessness and prayerlessness, in the name of Jesus.",
                "Father, I receive grace to focus on You above all else, above every problem or challenge, in the name of Jesus.",
                "Power to overcome till the end, fall on me today, in the name of Jesus.",
            ]
        },
        {
            day: 2,
            topic: "The Power to Change",
            wordStudy: "Isaiah 41:9-13",
            confession: "By the resurrection power of the Lord Jesus Christ, everything dead in my life shall be raised back to life, in the name of Jesus (Isaiah 41:10).",
            fullText: <View>
                <Text style={styles.text}>
                    We go to seminars and conferences looking for a painless cure by which our
                    lives can be zapped and changed. We go on diets. We join health clubs and
                    our enthusiasm runs strong for about two weeks. Then we fall back into the
                    same old rut. We don't change. We read self-help books, but the problem with
                    self-help books is that they tell us what to do but can't give us the power to do
                    it. We are told things like: “Get rid of all your bad habits. Be positive; don't be
                    negative.” But how? Where do we get the power to change? The Greek word
                    for “power,” <Italic>dunamis,</Italic> is used in the New Testament to describe the most
                    powerful event that ever happened—the resurrection of Jesus Christ from the
                    dead. And that resurrection power is available to change your life today.
                    <Br />
                    The most important thing in life is knowing Christ and experiencing the
                    power of His resurrection. Paul writes, <Bold>“I want to know Christ… to know
                    the power of His resurrection…” (Philippians 3:10 NIV).</Bold> Again he writes,
                    <Bold>“I pray that you will begin to understand how incredibly great His
                    power is to help those who believe in Him. It is that same mighty power
                    that raised Christ from the dead…” (Ephesians 1:19-20 TLB).</Bold> This
                    Greek word for “power,” <Italic>dunamis</Italic>, can be understood in two ways: (1)
                    dynamite, which is an explosive force, or (2) dynamo, which is a constant
                    flow of power. And in Christ, you have both. Through Him you can break the
                    chains that bind you and the limits that constrain you, and walk victoriously
                    in His power today.
                </Text>
            </View>,
            prayerPoints: [
                "Lord Jesus, I receive grace today that I may know You and the power of Your resurrection.",
                "Power of God to overcome negative habits, fall on me and my family, in the name of Jesus.",
                "Dynamite power of the Holy Spirit for change, .ow through me now, in the name of Jesus.",
            ]
        },
        {
            day: 3,
            topic: "Rules for Serving",
            wordStudy: "Romans 12:9-13",
            confession: "Because the love of God has been shed abroad in my heart by the Holy Spirit, I shall continually walk in love, in the name of Jesus (Leviticus 19:18).",
            fullText: <View>
                <Text style={styles.text}>
                    When it comes to serving others, try to live by these three rules:
                </Text>
                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            Serve sincerely. “Don't just pretend to love others. Really love
                            them…”(Romans 12:9). God knows your heart, so be honest with
                            yourself about your true motives in serving and sacrificing for others. If
                            you need help in this area, turn to the Scriptures: “For the Word of God is
                            living and powerful, and sharper than any two-edged sword… and is
                            a discerner of the thoughts and intents of the heart” (Hebrews 4:12
                            NKJV).
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            Serve silently. “…When you do a charitable deed, do not sound a
                            trumpet…that [you] may have glory from men…” (Matthew 6:2
                            NKJV). Note the words, “do not sound a trumpet.” When people take
                            your kindness for granted or fail to appreciate the things you do for them,
                            don't toot your own horn. Look to God for your reward, not people;
                            otherwise you'll be disappointed. “…As the eyes of servants look to the
                            hand of their masters… so our eyes look to the Lord our God…”
                            (Psalm 123:2 NKJV).
                        </Text>
                    },
                    {
                        serial: 3,
                        body: <Text>
                            Serve selectively. You're not called to go to every place, meet every need,
                            and help every person. On two different occasions God stopped Paul
                            from going into Asia to preach the Gospel. But look what happened next:
                            “And a vision appeared to Paul in the night. A man of Macedonia
                            stood and pleaded with him, saying, 'Come over to Macedonia and
                            help us.' …Immediately we sought to go to Macedonia, concluding
                            that the Lord had called us to preach the Gospel to them” (Acts 16:9-
                            10 NKJV).
                        </Text>
                    },
                ]} />
                <Text style={styles.text}>
                    Why is this important to know? Because when God guides you, He provides for you!
                </Text>
            </View>,
            prayerPoints: [
                "Father Lord, help me to serve You with a perfect heart, in the name of Jesus.",
                "Create in me a clean heart, O Lord, and renew a steadfast spirit within me, in the name of Jesus.",
                "Holy Spirit, cleanse me from all filthiness hindering my service to the Lord, in the name of Jesus.",
            ]
        },
        {
            day: 4,
            topic: "Shake It Off",
            wordStudy: "Mark 6:7-13",
            confession: "I shake off the dust of rejection from my life and destiny, rejection shall not hinder my life and work, in the name of Jesus (Mark 6:11).",
            fullText: <View>
                <Text style={styles.text}>
                    Here's some good advice Jesus gave His disciples. When someone rejects
                    you or won't listen to you, <Bold>“…Shake the dust off your feet when you leave,
                    as a testimony against them.”</Bold> When you experience rejection, you have
                    two choices: allow it to affect your confidence and self-worth, or “shake it
                    off” and move on. Now, Jesus was not talking about sincere minds that don't
                    understand, but proud minds that reject the truth. He was saying, “Don't let it
                    break your stride and keep you from accomplishing what needs to be done.”
                    <Br />
                    When Paul was shipwrecked on the island of Malta, he was bitten by a
                    snake while gathering firewood. Immediately the islanders said to one
                    another, <Bold>“…This man is a murderer, whom, though he has escaped the
                    sea, yet justice does not allow to live” (Acts 28:4 NKJV).</Bold> How did Paul
                    respond? <Bold>“…He shook off the creature into the fire and suffered no
                    harm” (Acts 28:5 NKJV).</Bold> What did the islanders say about him then?
                    <Bold>“…They changed their minds and said that he was a god” (Acts 28:6
                    NKJV).</Bold> Wow! People's minds change like the wind! So don't let their
                    compliments puff you up, or their criticisms tear you down. God's will is for
                    you to help others. If they accept your help, give it. If not, go where you will
                    be accepted and appreciated. Jesus said, <Bold>“…I say to you, he who receives
                    whomever I send, receives Me…” (John 13:20 NKJV).</Bold> In the final
                    analysis it's not you they are rejecting, but the One who sent you. Knowing
                    that, gives you confidence and peace of mind.
                </Text>
            </View>,
            prayerPoints: [
                "I put on the whole armour of God against every fiery dart of the enemy, in the name of Jesus.",
                "Grace to move on and conquer in the face of the attacks and opposition of the enemy, rest on me, in the name of Jesus.",
                "I shall not be wounded in the fight of faith, in the name of Jesus.",
            ]
        },
        {
            day: 5,
            topic: "Are You Afraid? Trust God!",
            wordStudy: "Isaiah 26:2-9",
            confession: "The Lord shall keep me and my family in perfect peace, for I put my trust in Him (Isaiah 26:3).",
            fullText: <View>
                <Text style={styles.text}>
                    Fear is a normal part of living. And the most successful and confident people
                    you know experience it. The difference is they refuse to be ruled by fear
                    because they know that, when it gets out of control, it can paralyze you. The
                    Bible says, <Bold>“Fear involves torment…” (1 John 4:18 NKJV).</Bold>
                    Neuroscientists at Yale University discovered that patients who expected to
                    experience an electric shock had anxiety levels similar to those who
                    responded to the real thing. Researcher Elizabeth Phelps writes, “A lot of our
                    fears and anxieties are learned through communication. If someone tells you
                    to be afraid of a dog, then the brain responds as if you actually were.” In other
                    words, our brains don't know the difference between real and imagined
                    threats. This goes a long way toward explaining why the National Institute of
                    Mental Health reported that almost twenty million Americans suffer from
                    anxiety disorders. Someone who's perfectly safe, but has a fear of being
                    robbed, suffers just as much as someone living in a situation with a high risk
                    of robbery, or someone actually in the act of being robbed.
                    <Br />
                    Because our brains don't discriminate between emotions that are real or
                    imagined, fear can dominate our lives, and it's just as devastating as physical
                    injury. Psychologist Marilyn Barrick said, “For the most part, fear is nothing
                    but an illusion. When you share it with someone, it tends to disappear”' So
                    share your fears with God and watch them begin to disappear. The Bible
                    says, <Bold>“You will keep in perfect peace all who trust in You, all whose
                    thoughts are fixed on You!”</Bold>
                </Text>
            </View>,
            prayerPoints: [
                "I command every spirit of fear to depart from my life and family, in the name of Jesus.",
                "Fear and terror shall not come near my dwelling, in the name of Jesus.",
                "I refuse to be paralyzed by fear, in the name of Jesus.",
            ]
        },
        {
            day: 6,
            topic: "A Blank Only God Can Fill",
            wordStudy: "Acts 17:24-34",
            confession: "Because I am planted in the house of the Lord, I shall flourish in the courts of our God, in the name of Jesus (Psalm 92:13).",
            fullText: <View>
                <Text style={styles.text}>
                    Here's a time-tested maxim: “The heart is a God-shaped blank, and only God
                    can fill it.” In <Italic>Through the Valley of the Kwai: From Death-Camp Despair to
                    Spiritual Triumph,</Italic> Ernest Gordon writes about soldiers who were captured,
                    tortured and starved by the Japanese in the Malay Peninsula in World War II.
                    As a result, they started acting like animals, clawing, fighting and stealing
                    food. In an effort to turn things around, the group decided to start reading the
                    New Testament. And as Gordon read it to them, they were converted to
                    Christ, and this community of animals was transformed into a community of
                    love. That's because Jesus is God and is willing to live in the hearts of those
                    who trust Him. He offers joy, peace, a transformed life and assurance of
                    eternal life to all those who place their trust in His atoning death.
                    <Br />
                    So what are you using to try to fill the void in your life? Material
                    possessions? Mood-altering substances? Sex? Career success? The pursuit
                    of intellectual growth? Excitement? You may as well give it up now—it
                    won't work! Paul explained it like this to the philosophers in Athens:
                    <Bold>“…God…made the world and everything in it… He…gives life…to
                    everything… He satisfies every need… His purpose…was [that we
                    should] seek after God…feel [our] way towards Him and find
                    Him—though He is not far from…us. For in Him we live and move and
                    exist…” (Acts 17:24-28 NLT).</Bold> In the words of the time-honoured hymn:
                    <Italic>“Now none but Christ can satisfy; none other name for me. There's love, and
                    life, and lasting joy, Lord Jesus, found in Thee.”</Italic>
                </Text>
            </View>,
            prayerPoints: [
                "Father, in the name of Jesus, I repent of the sin of trusting in my own abilities and worldly substance.",
                "I receive grace to trust You more and more, in the name of Jesus.",
                "Lord, fill my heart with a thirst and a hunger for You and Your Word, in the name of Jesus.",
            ]
        },
        {
            day: 7,
            topic: "Signs of the Times (1)",
            wordStudy: "Matthew 24:3-51",
            confession: "The spirit of the end time shall not overtake me and my household, in the name of Jesus (Matthew 24:4-5).",
            fullText: <View>
                <Text style={styles.text}>
                    When the disciples came to Jesus and asked Him, <Bold>“What will be the sign of
                    Your coming, and of the end of the age?”,</Bold> He told them things were going
                    to get worse before they got better. Then He added these words: <Bold>“See to it
                    that you are not alarmed…” (Matthew 24:6 NIV).</Bold> The word alarmed is
                    one Jesus used only on this occasion; it means “to wail, to cry aloud.” In
                    essence, what He was saying is this: “Don't fall apart when bad things
                    happen.” For the next few days, let's look at some of the signs Jesus said
                    would herald His return.
                    <Br />
                    <Italic>Spiritual deception.</Italic> <Bold>“Watch out that no one deceives you. For many will
                    come in My name, claiming, 'I am the Messiah,' and will deceive many”
                    (Matthew 24:4-5 NIV).</Bold> Notice, there'll be many deceived, as well as many
                    deceivers. Jesus also said they'll come <Bold>“in My name,”</Bold> masquerading as
                    ministers of God and claiming special status and superior spirituality. They'll
                    boast of insider information and adorn their teachings with phrases like “God
                    told me,” implying they have access to information the common people like
                    you don't have. Don't be misled. Jesus also warned, <Bold>“False Messiahs and
                    false prophets will appear and perform great signs and wonders to
                    deceive, if possible, even the elect” (Matthew 24:24 NIV).</Bold> Multitudes and
                    miracles—when you see them, be careful. Satan can counterfeit both. Be
                    doctrinally diligent. Focus on one question: “Is this person pointing listeners
                    to the real, Biblical Jesus?” There is only room for one name on the
                    marquee—and that's His!
                </Text>
            </View>,
            prayerPoints: [
                "Spirit of deception will not possess me and my household, in the name of Jesus.",
                "I shall not be misled by false prophets and teachers, in the name of Jesus.",
                "I receive the grace to overcome the the flesh, world and the devil till the end, in the name of Jesus.",
            ]
        },
        {
            day: 8,
            topic: "Signs of the Times (2)",
            wordStudy: "Luke 21:8-28",
            confession: "When darkness shall cover the earth and gross darkness the people, I will arise and shine for Christ is my Light, in the name of Jesus (Isaiah 60:1-2).",
            fullText: <View>
                <Text style={styles.text}>
                    Those who suffered through two world wars have no difficulty
                    comprehending the word wars. But for the next six decades or so, their
                    children and grandchildren had a more limited concept of what war meant.
                    To them, wars were mostly regional and overseas, but not on the homeland.
                    Then came 9/11, and Americans began to understand the words, <Bold>“Men's
                    hearts failing them for fear…” (Luke 21:26 NKJV).</Bold> Some dates will
                    forever stand out in our memory. Like the day President Kennedy was
                    assassinated, and the day three thousand people perished in the World Trade
                    Centre.
                    <Br />
                    Today the political rationale is if we fight terrorism “over there,” we
                    won't have to fight it here at home. But don't be deceived. Borders will always
                    need checkpoints. War correspondents will always have jobs. The world will
                    never see lasting peace this side of Heaven. Jesus said, <Bold>“…All these are the
                    beginning of birth pains” (Matthew 24:8 NKJV).</Bold> Rejoice! God is still on
                    the throne. His plan is still being worked out. The death throes of the old order
                    we live in are the “birth pains” of a new and glorious order when Christ will
                    return to set up His Kingdom. An old Prudential Insurance Company slogan
                    says, “Get a piece of the rock.” But when you're trusting in Jesus for your
                    security, you have the whole Rock!
                </Text>
            </View>,
            prayerPoints: [
                "In the face of “wars” and “rumours of war,” my heart shall not fail for fear, in the name of Jesus.",
                "Every war raging against my destiny, cease forthwith, in the name of Jesus.",
                "My family and I shall not be joined with them for burial, in the name of Jesus.",
            ]
        },
        {
            day: 9,
            topic: "Signs of the Times (3)",
            wordStudy: "Matthew 24:7-14",
            confession: "In the face of persecution and death, my faith and strength in God will not fail, in the name of Jesus (Matthew 24:9).",
            fullText: <View>
                <Text style={styles.text}>
                    Notice the words <Bold>“put to death.”</Bold> Heaven is filled with those who have
                    already fulfilled this prophecy—and hatred of Christians still abounds. <Italic>Voice
                    of the Martyrs,</Italic> a Christian agency that defends religious liberties, contends
                    that more of Christ's followers have been killed for their faith in the last
                    century than in most of the previous centuries combined. The global
                    evangelism movement reports an average of 165,000 martyrs a year, more
                    than four times the number in the past century. Today countries that facilitate
                    religious freedom are experiencing increasing hostility toward Christians.
                    Professors publicly mock Bible-believing students. TV talk show hosts
                    denigrate people of faith. We can expect the persecution to increase, and
                    when it does, fragile convictions will collapse and <Bold>“the love of many will
                    grow cold” (Matthew 24:12 NLT).</Bold> The half-hearted will become the coldhearted.
                    Spiritual stowaways will jump ship. Many church attendees will be
                    exposed as faith pretenders. They'll not only leave the faith, they'll make the
                    lives of the faithful miserable. When these things begin to happen, Jesus
                    counselled, <Bold>“See to it that you are not alarmed…” (Matthew 24:6 NIV).</Bold>
                    When the outlook is bleak, the <Bold>“blessed hope” (Titus 2:13 AMP)</Bold> of Christ's
                    return calms our fears.
                </Text>
            </View>,
            prayerPoints: [
                "Lord, I receive the grace to be faithful in the face of persecution, in the name of Jesus.",
                "In the name of Jesus, my love for God and the things of God shall not wax cold.",
                "I reject the spirit of lukewarmness, in the name of Jesus.",
            ]
        },
        {
            day: 10,
            topic: "Signs of the Times (4)",
            wordStudy: "Matthew 24:14-51",
            confession: "I shall be an instrument for the propagation of the gospel in these last days, in the name of Jesus (Matthew 28:19-20).",
            fullText: <View>
                <Text style={styles.text}>
                    Jesus said when we see calamity and apostasy, don't give in to fear because
                    they're signs of His imminent return. <Bold>“But he who endures to the end shall
                    be saved. And this Gospel of the Kingdom will be preached in all the
                    world as a witness to all the nations, and then the end will come”
                    (Matthew 24:13-14 NKJV).</Bold> Every prophecy concerning Christ's first
                    coming was fulfilled in detail, and so will every prophecy concerning His
                    second coming. Our mandate is to work, pray, give, and take the Gospel to the
                    whole world—then Christ will return. Though the church is winnowed down
                    like Gideon's army, and the world is in a state of upheaval, don't overreact.
                    <Bold>“Be still in the presence of the Lord, and wait patiently for Him to act.
                    Don't worry about evil people who prosper or fret about their wicked
                    schemes” (Psalm 37:7 NLT).</Bold> God's hand is still on the wheel. <Bold>“The Most
                    High rules in the kingdom of men…” (Daniel 4:17 NKJV).</Bold>
                    <Br />
                    Avoid Pollyanna optimism. Though you may be personally blessed, the
                    world order as you know it will continue to disintegrate. But that doesn't
                    mean you should join the “Chicken Licken chorus” and declare, “The sky's
                    falling down!” Don't give in to the extremes of blind denial or blatant panic.
                    When the bombs of World War II levelled Warsaw, only one skeletal structure
                    remained standing on the city's main street. It was the British and Foreign
                    Bible Society, and on its walls was clearly written: <Bold>“Heaven and earth shall
                    pass away, but My words shall not pass away” (Matthew 24:35 KJV).</Bold>
                </Text>
            </View>,
            prayerPoints: [
                "The grace to endure to the end and be saved, rest upon me and my family, in the name of Jesus.",
                "In these last days, my faith shall not be overthrown, in the name of Jesus.",
                "The anointing to walk steadfastly in faith, fall on me, in the name of Jesus.",
            ]
        },
        {
            day: 11,
            topic: "Are You Selling Your Dream?",
            wordStudy: "Genesis 50:19-22",
            confession: "Anything meant or designed to harm me shall work for my good, now and in the future, in the name of Jesus (Genesis 50:20).",
            fullText: <View>
                <Text style={styles.text}>
                    In the movie <Italic>Up in the Air,</Italic> actor George Clooney is hired by a major
                    corporation to handle big layoffs. His job is to fire people. In one scene when
                    he's about to fire an ageing middle manager, he notices on the man's CV that
                    he'd been trained as a French chef. As the man expresses despair over losing
                    his job, Clooney reminds him of his original dream and asks him this soulsearching
                    question: “Back when you started, how much did it take to buy you
                    away from your dream?” At that pivotal moment the middle manager thought
                    back to the time he decided to settle for a steady pay packet in exchange for
                    what he really wanted to do with his life. Are you doing that? Understand this:
                    The day you were born God had a track for you to run on and an assignment
                    for you to fulfil. So the question you must ask yourself is this: “Is the difficult
                    situation I'm in right now a God-given opportunity for me to go back and fulfil
                    the dream God gave me in the first place?”
                    <Br />
                    Looking back on the most painful chapter in his life, when he was
                    betrayed by his family, Joseph said, <Bold>“You intended to harm me, but God
                    intended it for good”</Bold> His greatest loss led Joseph to the fulfillment of the
                    dream God gave him in the beginning. Is that your story too? Do you realize
                    that you've settled for second best, and now God's giving you a second
                    chance; a chance to do the thing He put you in the world to do?
                </Text>
            </View>,
            prayerPoints: [
                "My destiny shall not be exchanged through dreams, in the name of Jesus.",
                "My God-given dreams shall come to pass, in the name of Jesus.",
                "Lord, preserve the original blueprint of my destiny, in the name of Jesus.",
            ]
        },
        {
            day: 12,
            topic: "Sometimes You Must Confront",
            wordStudy: "Matthew 18:12-17",
            confession: "All of my days, I shall walk in love with my brethren, in the name of Jesus (Matthew 18:15).",
            fullText: <View>
                <Text style={styles.text}>
                    Jesus said, <Bold>“…If your brother sins against you, go and tell him his fault
                    between you and him alone. If he hears you, you have gained your
                    brother.”</Bold> Confronting someone requires these three things: character,
                    courage and caution. Let's look at each:
                </Text>
                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            <Italic>Character.</Italic> Since Jesus was secure in His identity and character as God's
                            Son, He was able to let go of the need to please and be accepted by those
                            around Him. Being grounded in who you are in Christ and in what your
                            heavenly Father created you to be allows you the freedom to confront
                            people, when necessary, without worrying about the fall-out or the
                            negative consequences.
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            <Italic>Courage.</Italic> The Pharisees had the power to undermine Christ's reputation,
                            which eventually led to His death on the cross. But He told them the
                            truth nonetheless. And you must do the same. You must be willing to
                            take up your cross and follow Him, even when it means risking an
                            argument and handling hurt feelings. To <Italic>confront</Italic> means to get “in front”
                            of someone, look into their face, and deal with the issue honestly and
                            lovingly because you value the relationship.
                        </Text>
                    },
                    {
                        serial: 3,
                        body: <Text>
                            <Italic>Caution.</Italic> Make sure you're following God's timing and not your own. It's
                            easy to use false courage or bravado to challenge others in order to get
                            your own way. It's easy to claim a confrontation is necessary, when what
                            you actually want is control. No, you must ask God to show you when,
                            how and where to confront others. Your goal should always be to
                            reconcile and restore the relationship.
                        </Text>
                    },
                ]} />
                <Text style={styles.text}>
                    So <Bold>“go and tell him [or her].”</Bold>
                </Text>
            </View>,
            prayerPoints: [
                "I receive grace not to take offense at my brethren, in the name of Jesus.",
                "I receive grace to walk in love always, in the name of Jesus.",
                "I receive courage to confront my brethren when necessary, in the name of Jesus.",
            ]
        },
        {
            day: 13,
            topic: "Can You Give and Receive Correction?",
            wordStudy: "Proverbs 27:4-10",
            confession: "I shall gladly receive the correction of the Lord all the days of my life, in the name of Jesus (Revelation 3:19).",
            fullText: <View>
                <Text style={styles.text}>
                    The word <Italic>correction</Italic> is not one we are always comfortable with. It sometimes
                    recalls memories of frowning parents and painful consequences. Or we think
                    of the risk of destroying a relationship rather than the possibilities of
                    strengthening and enriching it. <Bold>“Faithful are the wounds of a friend, but the
                    kisses of an enemy are deceitful.”</Bold> We all need someone to tell us the truth
                    because others have 20/20 vision where we have blind spots. Yes, someone
                    may react defensively, but they may also be thrilled to realize you care more
                    about their ultimate well-being than their immediate response to you.
                    Corrections made from a loving spirit, not a self-righteous,
                    I–know–what's–best attitude is usually well received. If others sense you are
                    genuinely concerned for them and long for a better relationship, they are
                    likely to consider what you have to say and be open to making changes.
                    <Br />
                    Think of the courage Nathan the prophet showed in confronting David
                    over his sin with Bathsheba. As king, David held the power of life and death
                    over him. But Nathan loved David too much to allow sin to rob him of his
                    destiny. God often uses us in each other's lives to restore us to the narrow yet
                    rich paths of destiny that He has preordained. Looking back, David could say,
                    <Bold>“Before I was afflicted I went astray: but now I keep Your Word” (Psalm
                    119:67 NKJV).</Bold> There are times when wisdom says, “Mind your own
                    business and stay out of it.” And there are other times when wisdom says,
                    “Because you love them, get involved.”
                </Text>
                
            </View>,
            prayerPoints: [
                "The lines shall fall to me in pleasant places, in the name of Jesus.",
                "The grace to be humble and take correction, rest on me, in the name of Jesus.",
                "My destiny shall not be wounded or truncated by pride, in the name of Jesus.",
            ]
        },
        {
            day: 14,
            topic: "Information Overload",
            wordStudy: "Daniel 12:4-12",
            confession: "I shall walk in wisdom and understanding all the days of my life, in the name of Jesus (Proverbs 4:7).",
            fullText: <View>
                <Text style={styles.text}>
                    Technology lets us do things we've never been able to do, but it can be
                    addictive. You can become tied to it in ways that are exhausting. One expert
                    notes: “There's a strong tendency for humans to do everything they're able to.
                    Combine that with constant connectivity and the workday need never end. It's
                    easy to contact anyone anytime, and with information always available on
                    line, you can keep clicking forever.” Paul writes about <Bold>“making the most of
                    your time” (Ephesians 5:16 NAS).</Bold> We say, “Time is money,” but in actuality
                    time is much more valuable, because it's a non-renewable resource. Once
                    spent, it's gone forever. Managing information overload means reestablishing
                    boundaries that technology has demolished. So:
                </Text>
                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            <Italic>Recognize the signs:</Italic> If you communicate with people all day yet you're
                            still lonely, chances are, technology is dominating your life.
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            <Italic>Take baby steps:</Italic> Make yourself unavailable for short periods and see
                            what happens. The wheels of industry won't grind to a halt! Remember,
                            you have a choice: people who think they should be available 24/7
                            exaggerate their own importance or the control others have over them.
                        </Text>
                    },
                    {
                        serial: 3,
                        body: <Text>
                            <Italic>Establish boundaries:</Italic> Rein in the emails, instant messages and online
                            feeds. Do you really need all those FYI's about the same thing?
                        </Text>
                    },
                    {
                        serial: 4,
                        body: <Text>
                            <Italic>Give clear instructions:</Italic> Tell people you answer emails at designated
                            times throughout the day, and let them know who to contact for an
                            immediate response.
                        </Text>
                    },
                    {
                        serial: 5,
                        body: <Text>
                            <Italic>Make a task list:</Italic> That way if you're interrupted you'll get back on track
                            faster.
                        </Text>
                    },
                    {
                        serial: 6,
                        body: <Text>
                            <Italic>Stick to a schedule:</Italic> Constantly dipping in and out of the computer is a
                            classic self-interruption.
                        </Text>
                    },
                    {
                        serial: 7,
                        body: <Text>
                            <Italic>Do a reality check:</Italic> After a few minutes of surfing the web, ask yourself,
                            “Should I be doing this now?”
                        </Text>
                    },
                ]} />
            </View>,
            prayerPoints: [
                "Lord, grant me true wisdom and understanding, in the name of Jesus",
                "Lord, help me to take hold of Your instructions and precepts so I don't stumble, in the name of Jesus.",
                "Let your Word be a lamp to my feet and a light to my path, in the name of Jesus.",
            ]
        },
        {
            day: 15,
            topic: "Information Overload (2)",
            wordStudy: "John 13:13-17",
            confession: "Wisdom and knowledge shall be the stability of my times, in the name of Jesus (Isaiah 33:6).",
            fullText: <View>
                <Text style={styles.text}>
                    When you're constantly multi-tasking, important things can fall between the
                    cracks. A lawyer who negotiated an unbelievable deal in favour of his client
                    was asked how he did it. He replied, “I was the only person at the meeting that
                    day who didn't spend the whole time text messaging!” A respected researcher
                    says we've trained our brains “to constantly flit around the universe of
                    messages and information, seeking brief hits of excitement. Grazing
                    ceaselessly, we never dig too deeply before moving on to the next
                    distraction…and it ratchets up our stress levels in ways we're only starting to
                    understand.” Another expert says: “We get more information in 72 hours than
                    our parents received in a month, and most people don't have the skills to deal
                    with it. They let new things in, but don't get rid of old information they wanted
                    to act on.”
                    <Br />
                    When was the last time you enjoyed some relaxed, uninterrupted down
                    time? Can you even remember? We're so results-oriented that unless we're
                    doing something that can be quantified, we think it's a waste of time. The fact is
                    you need time away from the unrelenting onslaught of information to regroup,
                    reflect and recharge. Jesus had a schedule to keep, people to minister to,
                    disciples depending on Him, and a short time to do it all. Nevertheless, <Bold>“He
                    withdrew…into the wilderness, and prayed” (Luke 5:16 KJV).</Bold> Now, since
                    <Bold>“the servant is not greater than his lord,”</Bold> do you think you're above all that?
                    Are you wiser than He? Or more spiritual? Jesus said, <Bold>“Come… apart…and
                    rest a while” (Mark 6:31 KJV),</Bold> because when you don't come apart, you fall
                    apart!
                </Text>
            </View>,
            prayerPoints: [
                "Lord, help me touse wisdom to handle my daily tasks, in the name of Jesus.",
                "I receive the grace to retreat at the right time always, in the name of Jesus.",
                "I shall not be distracted in my God-given assignment, in the name of Jesus.",
            ]
        },
        {
            day: 16,
            topic: "How's Your Self-Talk These Days?",
            wordStudy: "Luke 6:43-49",
            confession: "My mouth shall speak wisdom, and the meditation of my heart shall be of understanding (Psalm 49:3).",
            fullText: <View>
                <Text style={styles.text}>
                    When John Roebling devised a plan to build a bridge between Manhattan and
                    Brooklyn, experts thought it was impossible. They said a bridge spanning
                    that distance couldn't withstand the winds and tides. But Roebling refused to
                    concede defeat; instead, he and his son, Washington, worked to solve the
                    problems. Then just as construction was about to start, Roebling was killed in
                    an underwater accident that left his son brain-damaged and unable to walk or
                    talk. The prevailing wisdom was to abandon the project, but Washington
                    Roebling was determined to fulfil his father's dream. He developed a system
                    of communication by touching a finger to his wife's arm, and she in turn
                    conveyed his ideas to the project engineers. For thirteen years, that's how he
                    supervised construction—and in 1883 the first car drove across the Brooklyn
                    Bridge. The 'impossible' had become reality!
                    <Br />
                    Are you facing a seemingly impossible situation at home, on the job,
                    with your finances, or in a relationship? If so, what you tell yourself about it,
                    is important. Your self-talk sets you up for joy or misery. You can tell where
                    your faith is by what comes out of your mouth: <Bold>“Good people bring good
                    things out of the good they stored in their hearts…” (Luke 6:45 NCV).</Bold>
                    One pastor says: “When the pressure's on, what comes out of your mouth lets
                    you know if you need to make some adjustments… When you want
                    something to show up in your outward man, deposit God's Word in the
                    inward man. Feed on it continually… once you believe it, you'll find yourself
                    saying it, and once you start saying it, your entire being will reflect the
                    treasure of His Word inside.”
                </Text>
            </View>,
            prayerPoints: [
                "Let the words of my mouth and the meditation of my heart be acceptable in your sight, O Lord my redeemer.",
                "My mouth will not truncate my destiny, in the name of Jesus.",
                "Holy Spirit, set a guard over my mouth and keep watch over the door of my lips, in the name of Jesus.",
            ]
        },
        {
            day: 17,
            topic: "Going Out on a Limb",
            wordStudy: "Luke 19:12-28",
            confession: "By faith, I shall obtain a good report, in the name of Jesus (Hebrews 11:1).",
            fullText: <View>
                <Text style={styles.text}>
                    Some people are naturally cautious and some revel in risk. Jon Walker says:
                    “If God tells you to take a step of faith but you hesitate to take it until He shows
                    you what the second step will be, you're not waiting on God. He's waiting on
                    you.” Patience is no excuse for hesitation. “God uses risks, large and small, to
                    push us into a deeper faith. And so He wants us to step forward in faith, even if
                    we don't know where the second step will take us. The not knowing is what
                    requires faith, and the not knowing compels us to rely on God to g uide us
                    forward.”
                    <Br />
                    The Bible says, <Bold>“If you wait for perfect conditions, you'll never get
                    anything done” (Ecclesiastes 11:4 TLB).</Bold> Helen Keller said, “Security is
                    mostly a superstition. It doesn't exist in nature nor do the children of men as a
                    whole experience it… Life is either a daring adventure, or nothing.” If you
                    want “more than you ever dreamed,” you've got to go out on a limb. That's
                    where the best fruit is. Acting in faith, with no “earthly” guarantee what's on
                    the other side means attempting something you couldn't possibly do unless
                    God gave you the ability. Jon Walker goes on: “Faith grows when we take
                    risks—not just any kind of risks, but ones specifically directed by God. These
                    God-nudges push us beyond the borders of our 'independent states' into 'the
                    promised land' of life by faith.”
                </Text>
            </View>,
            prayerPoints: [
                "Lord, help me to walk by faith that I may please you, in the name of Jesus.",
                "Anointing to do exploits for God, fall on me, in the name of Jesus",
                "Anointing for the supernatural, fall on me and my family, in the name of Jesus.",
            ]
        },
        {
            day: 18,
            topic: "Stop Complaining (1)",
            wordStudy: "Philippians 2:12-16",
            confession: "My mouth shall speak of the mercies and goodness of the Lord at all times, in the name of Jesus (Psalm 89:1).",
            fullText: <View>
                <Text style={styles.text}>
                    A lady who worked at the post office was approached by a customer who
                    said, “I can't write. Would you mind addressing this postcard for me?” After
                    addressing it for him and writing a short message, the postal clerk asked, “Is
                    there anything else I can do for you, sir?” The man thought for a moment and
                    said, “Yes, could you add a P.S. at the end saying, 'Please excuse the sloppy
                    handwriting.”' Now, there's gratitude for you! The Bible says, <Bold>“Do all things
                    without complaining”</Bold> because when you don't, you end up hurting:
                </Text>
                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            <Italic>Yourself.</Italic> Complaining leads to anger and depression. God loves you
                            and He doesn't want you hurting yourself.
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            <Italic>God.</Italic> Complaining calls into question God's care, His character and His
                            competence. In reality, what you're saying is, “Lord, You blew it! You
                            had a chance to fulfil my demands and You chose not to.”
                        </Text>
                    },
                    {
                        serial: 3,
                        body: <Text>
                            <Italic>Others.</Italic> Your words affect the people around you and nobody enjoys
                            spending time with a member of “the cold-water bucket brigade.”
                        </Text>
                    },
                ]} />
                <Text style={styles.text}>
                    Complaining temporarily satisfies our selfish nature, but it changes
                    nothing. When you complain, you explain your pain for no gain. But here's
                    the good news: The Bible says, <Bold>“…The people became like those who
                    complain of adversity…” (Numbers 11:1 NAS).</Bold> You didn't start out as a
                    complainer; you “became” that way, and by God's grace you can become a
                    thanksgiver! Once you acknowledge your habit of moaning and fault-
                    finding, it becomes possible to choose a better one. A bad habit is like a nice
                    soft bed; it's easy to get into and hard to get out of. So if you've fallen into the
                    habit of “complaining”—stop it!
                </Text>
            </View>,
            prayerPoints: [
                "Lord, I repent of the sin of complaining, forgive me, in the name of Jesus",
                "Lord, I repent of the sin of murmuring, forgive me, in the name of Jesus.",
                "Every good door complaining has closed in my life, by the mercy of God be opened, in the name of Jesus.",
            ]
        },
        {
            day: 19,
            topic: "Stop Complaining (2)",
            wordStudy: "Numbers 11:1-13",
            confession: "Heaven will not regret over my life, in the name of Jesus (Genesis 6:6).",
            fullText: <View>
                <Text style={styles.text}>
                    If you think complaining is no big deal, read this: <Bold>“…When the people
                    complained…the LORD heard…and His anger was aroused. So the fire of
                    the LORD burned among them, and consumed some in the outskirts of the
                    camp” (Numbers 11:1).</Bold> A wise man once said, “I complained that I had no
                    shoes, until I met a man who had no feet.” God has blessed you in 101
                    different ways, and He doesn't want to hear you whining. What were the
                    Israelites grumbling about anyway? <Italic>“Adversity.”</Italic> For some of us adversity
                    comes through illness. For others, it's a faltering career, not enough money to
                    pay the bills, or a family situation that happened years ago and now you're left
                    shouldering the responsibility. Some of us made poor decisions earlier in life,
                    and as a result our plans fell apart. Now we're struggling with marital
                    problems, blended families, and the consequences of our choices.
                    <Br />
                    We all have to deal with some level of adversity. We each have something
                    in our life that God doesn't want to hear us griping about! Understand this: it's
                    hard to live with adversity but, when you complain, you forego the grace
                    that'll get you through it. By choosing to complain and cling to the image of a
                    perfect life, you forfeit the grace that's available to you and will bring you
                    victory. So change your way of thinking. Get down on your knees and pray:
                    “Lord, I want the landscape of my life to be different; to experience the joy
                    You give to those who leave the wilderness of ingratitude and move into the
                    Promised Land of thanksgiving.” That's a prayer that will change you!
                </Text>
            </View>,
            prayerPoints: [
                "Spirit of complaining and murmuring, depart from me, in the name of Jesus.",
                "My life shall not bring displeasure to the Lord, in the name of Jesus.",
                "Foundational battles causing me to complain always, expire, in the name of Jesus.",
            ]
        },
        {
            day: 20,
            topic: "Don't Be Inflexible",
            wordStudy: "2 Kings 5:7-15",
            confession: "Anger will not destroy me and my destiny, in the name of Jesus (James 1:20).",
            fullText: <View>
                <Text style={styles.text}>
                    As a general in the Syrian army, Naaman was accustomed to having things
                    his own way. So when Elisha told him to dip seven times in the muddy Jordan
                    River to be healed of leprosy, he <Bold>“went away in a rage.”</Bold> He said, <Bold>“I
                    expected him to wave his hand over the leprosy and call on the name of
                    the Lord his God and heal me!” (2 Kings 5:11 NLT).</Bold> Fortunately, he
                    listened to his servants, swallowed his pride and received a miracle. There
                    are important lessons here. Since all progress calls for adapting to change
                    and overcoming obstacles, ask yourself:
                </Text>

                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            <Italic>What's at the core of my fear and anger over this situation?</Italic> Am I afraid
                            of the unknown and the changes it may bring?
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            <Italic>Am I being inflexible and trying to impose my will in this situation?</Italic> Am I
                            willing to forfeit God's perfect will by resisting a change He's
                            orchestrating? Many of us miss God's best. Why? Like Naaman, we are
                            accustomed to being waited on and having our ego stroked. E-G-O
                            means Edging God Out! Are you doing that?
                        </Text>
                    },
                    {
                        serial: 3,
                        body: <Text>
                            <Italic>Am I being lazy or incompetent, not wanting to invest the necessary time
                            and effort into the change?</Italic> Unless you're willing to change, you won't
                            grow. And if you don't grow, you won't position yourself to receive the
                            blessing God has in mind for you. Charles Franklin Kettering said, “The
                            world hates change, yet it is the only thing that has brought progress.”
                        </Text>
                    },
                ]} />
                <Text style={styles.text}>
                    Today ask God for the emotional and spiritual strength to embrace the
                    changes He's bringing into your life, and to help you to see them as being for
                    your good (Romans 8:28).
                </Text>
            </View>,
            prayerPoints: [
                "Lord, take away anger from my life, in the name of Jesus.",
                "The spirit of anger will not truncate my life and destiny, in the name of Jesus.",
                "I recover all the opportunities I have lost to anger, in the name of Jesus.",
            ]
        },
        {
            day: 21,
            topic: "You Need Friends Who Encourage You",
            wordStudy: "Proverbs 17:14-18",
            confession: "The Lord will add to me destiny-promoting friends, in the name of Jesus (Proverbs 18:24).",
            fullText: <View>
                <Text style={styles.text}>
                    If you're wise, you'll surround yourself with people who support you
                    emotionally and spiritually, and you can share your dreams and ideas with
                    them. C.S. Lewis and J.R.R. Tolkien were members of The Inklings, an
                    informal literary discussion group associated with Oxford University. It was
                    comprised of teachers, writers and friends who met regularly at a wellknown
                    Oxford pub to discuss passages from their favourite books as well as
                    their own writings.
                    <Br />
                    In 1936, they decided the world needed more novels that had faith and
                    morality as their central theme. Lewis and Tolkien decided to write science
                    fiction after realising the inferior quality of similar stories being published at
                    the time. They literally tossed a coin to decide who would write a book on
                    space travel versus time travel. Tolkien got the time travel nod, but his early
                    efforts with such a story never really worked out. Later, however, he
                    achieved great success with <Italic>The Lord of the Rings.</Italic> Lewis wrote his famous
                    series of novels called <Italic>The Space Trilogy,</Italic> and from that momentum he
                    eventually penned The <Italic>Chronicles of Narnia.</Italic>
                    <Br />
                    Now, you may not reach that level of success, but you'll go further with
                    the encouragement of true friends than you will without it. So ask yourself
                    what's involved in cultivating the kinds of friendships that'll help you fulfil
                    your God-given potential. It may mean coming out of your shell and reaching
                    out to others. Could you encounter hurt and rejection? Sure! But you'll
                    succeed only if you're willing to take that chance.
                </Text>
            </View>,
            prayerPoints: [
                "Lord, give me godly friends after the order of Jonathan and David, in the name of Jesus.",
                "I cut off from every unfriendly friend, in the name of Jesus. ",
                "Lord, use me an encourager for others for Your glory, in the name of Jesus.",
            ]
        },
        {
            day: 22,
            topic: "Give God His Tithe (1)",
            wordStudy: "Leviticus 27:28-34",
            confession: "By the spirit of obedience, my heavens shall continually be opened in the in, in the name of Jesus (Malachi 3:10).",
            fullText: <View>
                <Text style={styles.text}>
                    If you put your mortgage before your gifts to God, you show more gratitude
                    to your bank than you do to the One who made you. You say, “I earned it, so I
                    own it.” No. The Bible says, <Bold>“The earth is the Lord's, and everything in it,
                    the world, and all who live in it” (Psalm 24:1 NIV).</Bold> God owns every
                    square metre of land on earth and every person who walks on it—including
                    you. You're not an owner, you're a steward! And while a good steward
                    deserves to be well rewarded by his master, his first priority is to please him
                    and carry out his will. The Bible says, <Bold>“A tithe of everything… belongs to
                    the Lord…”</Bold> Get your thinking straight on this issue! In God's eyes 10
                    percent of your income is “dedicated funds” to be used for one thing
                    only—His purposes on the earth.
                    <Br />
                    Tithing isn't how the church raises its budget; it's how God raises His
                    children. We distort God's Word when we teach tithing as a means of getting
                    money to pay off church debt, or as a substitute for other worn-out methods
                    of fundraising, or as a cure-all for the church's financial shortfalls. The Bible
                    says, <Bold>“…Son [daughter]… give Me your heart” (Proverbs 23:26 NIV).</Bold>
                    When God has your heart's deepest affection you'll give gladly, not
                    grudgingly. The first man in Scripture to tithe was Abraham. Why did he do
                    it? Gratitude, because God had delivered him from the hand of an enemy
                    who was out to destroy him. Has God delivered you? Has He blessed you?
                    Then show your gratitude by giving Him His portion!
                </Text>
            </View>,
            prayerPoints: [
                "I repent of the sin of disobedience to tithing, in the name of Jesus..",
                "Doors that have been shot against me as a result of my disobedience, by the mercy of God, be opened in the name of Jesus.",
                "Never let me forget for a minute, Lord, that You are my Source that my life may glorify You.",
            ]
        },
        {
            day: 23,
            topic: "Give God His Tithe (2)",
            wordStudy: "Genesis 28:16-22",
            confession: "In blessing, the Lord will bless me, and I shall be a blessing to my generation, in the name of Jesus (Genesis 22:17).",
            fullText: <View>
                <Text style={styles.text}>
                    Jacob wasn't a giver by nature. Quite the opposite, in fact! He cheated his
                    brother Esau out of his birthright, which entitled him to twice as much of their
                    father's inheritance. And when he worked for his father-in-law, Laban took
                    advantage of him too. But one night Jacob met God in a life-changing dream,
                    and when he woke the next morning he told God, <Bold>“…Of all that You give
                    me, I will give You a tenth”</Bold> Note, this was hundreds of years before the Law
                    of Moses was instituted, which said, <Bold>“A tithe of everything…belongs to the
                    Lord…” (Leviticus 27:30 NIV).</Bold> Jacob wasn't motivated by law, he was
                    motivated by love for God. You can give without loving; some folks give
                    under pressure, or to impress others or to get an income tax deduction. But
                    you can't love without giving! The Bible says, <Bold>“For God so loved the world,
                    that He gave His only begotten Son…” (John 3:16 KJV).</Bold>
                    <Br />
                    You're never more like God than when you're in the act of giving. Jesus
                    said, <Bold>“…Seek first the Kingdom of God, and His righteousness; and all
                    these things shall be added to you” (Matthew 6:33 NKJV).</Bold> Tithing is
                    about putting God and His Kingdom first. When self, home, business and
                    pleasure come first in your life, your priorities are out of whack. Tithing
                    reverses that order and puts God where He rightfully belongs—in first place.
                    When God instructs you to tithe, He's telling you to establish the habit of
                    putting Him in the No. 1 slot as a life principle. When you do, Jesus said <Bold>“all
                    these things”</Bold> you're so concerned about will be <Bold>“added to you.”</Bold>
                </Text>
            </View>,
            prayerPoints: [
                "Spirit of poverty, be frustrated and bound in my life, in the name of Jesus.",
                "The grace to give joyfully, rest on me, in the name of Jesus.",
                "The windows of heaven shall be continually open onto me and my family, in the name of Jesus.",
            ]
        },
        {
            day: 24,
            topic: "Give God His Tithe (3)",
            wordStudy: "Malachi 3:8-12",
            confession: "The Lord shall multiply my seed as the stars of heaven, in the name of Jesus (Genesis 22:17).",
            fullText: <View>
                <Text style={styles.text}>
                    When you honour the Lord in your business, He will bless your business.
                    Imagine the advantage you'd have in the marketplace with God as your
                    partner! Many famous Christian business people throughout history were
                    faithful tithers, including Henry John Heinz of Heinz 57 Varieties, and
                    William Colgate, the toothpaste magnate. Some of these people were so
                    blessed that, before their lives were over, they gave God 90 percent of their
                    income and lived on the other 10 per cent. God says: <Bold>“Bring the whole tithe
                    into the storehouse, that there may be food in My house. Test Me in
                    this…and see if I will not throw open the floodgates of Heaven and pour
                    out so much blessing that you will not have room enough to store it…all
                    the nations will call you blessed…” (Malachi 3:10-12 NIV).</Bold>
                    <Br />
                    When you demonstrate that God's business means more to you than your
                    own, He will prosper you. And He invites you to “test” Him in this. How long
                    have you known that you should tithe, wanted to tithe, said you were going
                    to, and yet you've never got round to it? Start now—and start with what you
                    have! What you can do now is the only influence you have over your future.
                    John D Rockefeller is reputed to have made the following statement
                    concerning the habit of tithing: “I never would've been able to tithe the first
                    million dollars I made if I hadn't tithed my first salary, which was a dollar fifty
                    per week.” Tithing demonstrates that you've conquered self-interest and the
                    fear of lack. It's a demonstration of faith—and God always rewards faith!
                </Text>
            </View>,
            prayerPoints: [
                "Lord, as I tithe, let the rain of blessing fall on me and my work, in the name of Jesus.",
                "Grace to move beyond 10% in my tithing, rest on me, in the name of Jesus.",
                "I receive fresh financial strength, in the name of Jesus.",
            ]
        },
        {
            day: 25,
            topic: "Give God His Tithe (4)",
            wordStudy: "Psalm 34:3-11",
            confession: "Because I fear the Lord, I shall lack nothing in life (Psalm 34:9).",
            fullText: <View>
                <Text style={styles.text}>
                    There are three kinds of givers: the flint, the sponge and the honeycomb. To
                    get even a spark from a flint you have to hammer it. To get anything out of a
                    sponge you have to squeeze it. But a honeycomb just overflows with
                    sweetness. Which kind of giver would you like to be? The Psalmist writes,
                    <Bold>“Taste and see that the Lord is good; blessed is the man who takes refuge
                    in Him…for those who fear Him lack nothing” (Psalm 34:8-9 NIV).</Bold>
                    Tithing is an act of worship. Of the 100 to 110 hours you're awake each week,
                    one–third to one–half generally involves earning money. So when you give
                    God your money, you're giving Him your brain, your brawn and yourself.
                    When you go to the Lord's house on the Lord's Day, partake of the Lord's
                    Supper and put the Lord's tithe into the Lord's treasury, it's an act of profound
                    worship.
                    <Br />
                    Now, let's be clear: a God who paves Heaven's streets with gold isn't
                    going to go broke because you don't give Him a tithe of your income. The act
                    of tithing isn't about the tithe; it's about the attitude of the tither to the Creator.
                    It's not about the gift; it's about the attitude of the giver to the Giver. It's not
                    about the money; it's about the attitude of the man or woman to the Maker.
                    It's not about possessions; it's about the attitude of the loved ones to the
                    Lover. As the songwriter Isaac Watts said, <Italic>“Were the whole realm of nature
                    mine; that were an offering far too small. Love so amazing, so divine,
                    demands my soul, my life, my all.”</Italic>
                </Text>
            </View>,
            prayerPoints: [
                "The grace to obey God in all things, rest on me, in the name of Jesus.",
                "God my Father, help me to seek first Your kingdom, in the name of Jesus.",
                "The Lord will make all grace abound towards me that I, always having sufficiency in all things, will have an abundance for every good work, in the name of Jesus",
            ]
        },
        {
            day: 26,
            topic: "Find People Who Believe in You",
            wordStudy: "Galatians 6:1-10",
            confession: "No day shall I lack for encouragement and encouragers all the days of my life, in the name of Jesus (1 Samuel 30:6).",
            fullText: <View>
                <Text style={styles.text}>
                    Mark Twain said, “Keep away from people who try to belittle your ambitions.
                    Small people always do that, but the really great make you feel that you, too,
                    can become great.” Why does the Bible say, <Bold>“Carry each other's burdens”?</Bold>
                    Because one person can only carry a burden so far on their own.
                    <Br />
                    American novelist John Kennedy Toole quickly discovered that. As a
                    young writer, he worked alone writing a novel in New Orleans. When it was
                    finished he sent it to publisher after publisher, but they all turned him down.
                    Overcome by rejection, he took his own life. Sometime after the funeral, his
                    mother found a coffee-stained manuscript in the attic and took it to a professor
                    at Louisiana State University who agreed to read it. Immediately he
                    recognized its genius and recommended it to a major publisher. After its
                    release, John Kennedy Toole's novel, <Italic>A Confederacy of Dunces,</Italic> won a
                    Pulitzer Prize and was heralded as one of the major novels of the twentieth
                    century. If only he'd surrounded himself with friends who knew how to share
                    his burden, encourage him when he faced rejection and motivate him to keep
                    going, his life would have turned out very differently.
                    <Br />
                    So the word for you today is—“Find people who believe in you.”
                    Encourage and support them, and welcome their support in return. Spend
                    more time with those who sharpen you and make you better, and less time
                    with those who drain your energy, time and talent. The truth is, friends who
                    speak encouragement into your life are priceless. Their words are <Bold>“like
                    apples of gold in settings of silver…” (Proverbs 25:11 NIV).</Bold>     
                </Text>
            </View>,
            prayerPoints: [
                "Vessels of encouragement, locate me now, in the name of Jesus.",
                "My life and destiny will not lack encouragement, in the name of Jesus.",
                "My talents and gifting shall find expression for the glory of God, in the name of Jesus.",
            ]
        },
        {
            day: 27,
            topic: "A Good Attitude",
            wordStudy: "Philippians 4:4-9",
            confession: "My soul will magnify the Lord all the days of my life, in the name of Jesus (Luke 1:46).",
            fullText: <View>
                <Text style={styles.text}>
                    When you're going through bad times, your goal should be to keep a good
                    attitude. And with God's help, you can. Dr Viktor Frankl, a Nazi Holocaust
                    camp survivor, said: “If a prisoner felt that he could no longer endure the
                    realities of camp life, he found a way out in his mental life—an invaluable
                    opportunity to dwell in the spiritual domain, the one that the SS was unable to
                    destroy. Spiritual life strengthened the prisoner, helped him to adapt, and
                    thereby improved his chances of survival.” Here is some practical advice on
                    keeping a good attitude in bad times:
                </Text>

                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            <Italic>Always believe the best about others, but don't get bent out of shape
                            when they disappoint you.</Italic> Nobody is perfect, including you. Just be
                            grateful for the people that bring joy and endeavour to be counted
                            among them.
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            <Italic>When you are tempted to retaliate, judge or become impatient, say to
                            yourself, “This is an opportunity for me to model a great attitude for the
                            glory of God.”</Italic> You say, “But this person is driving me crazy.” Then
                            refuse to be a “passenger” and go along with them. Take back the wheel,
                            get into the driver's seat of your life, and determine which direction
                            you'll go and what attitude you'll have.
                        </Text>
                    },
                ]} />

                <Text style={styles.text}>
                    The Bible says: <Bold>“…Whatever things are true, whatever things are
                    noble, whatever things are just, whatever things are pure, whatever
                    things are lovely, whatever things are of good report, if there is any
                    virtue and if there is anything praiseworthy—meditate on these
                    things…and the God of peace will be with you.”</Bold>
                </Text>
            </View>,
            prayerPoints: [
                "Lord, I receive grace to look away from the imperfections of others. but rather to pray for them, in the name of Jesus.",
                "Holy Spirit, remove all judgmental attitudes from me, in the name of Jesus.",
                "Renew a right spirit within me, O Lord, in the name of Jesus.",
            ]
        },
        {
            day: 28,
            topic: "God's Forgiveness, Mercy and Grace (1)",
            wordStudy: "1 John 4:15-21",
            confession: "I shall continually walk under the banner of God's love and mercy all the days of my life, in the name of Jesus (Song of Solomon 2:4).",
            fullText: <View>
                <Text style={styles.text}>
                    God cares about you too much to leave you in any doubt about His love. The
                    Bible says His “perfect love expels all fear.” If God loved us with an
                    imperfect love, we'd have cause to worry. Human love is flawed; it keeps a
                    checklist of our sins and shortcomings—and consults it often. God keeps no
                    such list. His love casts out our fear because it casts out our guilt. John writes,
                    “…if our heart condemns us, God is greater than our heart…” (1 John 3:20
                    NKJV). When you feel unforgiven, question your feelings but don't question
                    God. Go back to His Word; it outranks self-criticism and self-doubt.
                    <Br />
                    Nothing fosters confidence like a clear grasp of God's grace, and nothing
                    fosters fear like ignorance of it. The fact is if you haven't accepted God's
                    grace, you're doomed to live in fear. No pill, pep talk, psychiatrist or earthly
                    possession can put your mind at ease. Those things may help numb your fear,
                    but they can't eradicate it. Only God's grace can do that. Have you accepted
                    Christ's forgiveness? If not, get down on your knees and do it now. The Bible
                    says, “If we confess our sins, He is faithful and just to forgive us…and to
                    cleanse us from all unrighteousness” (1 John 1:9 NKJV). The place of
                    confession is also the place of cleansing and restored confidence towards
                    God. Your prayer can be as simple as this: “Lord, I admit I've turned away
                    from You. Please forgive me. I place my soul in Your hands and my trust in
                    Your grace. In Jesus' name I pray. Amen.” Now, having received God's
                    forgiveness, mercy and grace—live like it!
                </Text>
            </View>,
            prayerPoints: [
                "God's canopy of mercy over my life shall not be destroyed, in the name of Jesus.",
                "Thank You, Lord, in the name of Jesus, that I am today what I am by Your grace.",
                "I shall enjoy the grace and mercy of the Lord all the days of my life, in the name of Jesus.",
            ]
        },
        {
            day: 29,
            topic: "God's Forgiveness, Mercy and Grace (2)",
            wordStudy: "Psalm 3:1-5",
            confession: "By the grace of God, my horn shall be lifted up like that of the unicorn, in the name of Jesus (Psalm 92:10).",
            fullText: <View>
                <Text style={styles.text}>
                    Regardless of how badly you have failed or how often you have failed—God
                    won't give up on you. So don't give up on yourself! Nothing you've done is
                    beyond the scope of His grace. Others may give up on you, but not God. King
                    David fell as low as a person can get. He was guilty of adultery, deception and
                    murder—all major offences. But God forgave and restored him. He writes
                    about it in two psalms. In the first psalm he writes: <Bold>“Many are saying of me,
                    God will not deliver him. But You, Lord, are a shield around me, O Lord;
                    You bestow glory on me and lift up my head. To the Lord I cry aloud, and
                    He answers me from His holy hill. I lie down and sleep; I wake again,
                    because the Lord sustains me. I will not fear the tens of thousands drawn
                    up against me on every side… From the Lord comes deliverance”
                    (Psalm 3:2–7 NIV).</Bold>
                    <Br />
                    Later on in the psalms he writes: <Bold>“…He turned to me and heard my
                    cry. He lifted me out of the slimy pit, out of the mud and mire; He set my
                    feet on a rock and gave me a firm place to stand. He put a new song in my
                    mouth, a hymn of praise to our God. Many will see and fear the Lord and
                    put their trust in Him” (Psalm 40:1-3 NIV).</Bold> And the God who turned
                    David's greatest mess into a message, and his greatest test into a testimony,
                    will do the same for you when you turn to Him and receive His forgiveness,
                    mercy and grace.
                </Text>
            </View>,
            prayerPoints: [
                "Lord, by Your mercy, lift me out of the slimy pit and miry clay, in the name of Jesus.",
                "Lord, by Your mercy, lift up my head and that of my family, in the name of Jesus.",
                "The shield of God around me shall not be destroyed, in the name of Jesus.",
            ]
        },
        {
            day: 30,
            topic: "Acknowledge Your Mistakes",
            wordStudy: "Proverbs 15:31-33",
            confession: "Pride will not destroy my life and destiny, in the name of Jesus (James 4:6).",
            fullText: <View>
                <Text style={styles.text}>
                    In spite of his faults, flaws and failures, God called David <Bold>“a man after My
                    own heart” (Acts 13:22 NKJV).</Bold> And one of the qualities that made David
                    great was his willingness to acknowledge his mistakes. Here were two
                    instances of it:
                </Text>
                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            When fleeing the wrath of King Saul, he sought the help of a certain
                            priest—a decision that caused Saul to order the death of eighty-five priests
                            and their families. Devastated but not defensive, David told the surviving son
                            of the slain priest who had assisted him, <Bold>“…I have caused the death of all
                            your father's family” (1 Samuel 22:22 NLT).</Bold> Can you imagine taking
                            responsibility for such a tragic consequence?
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            When the prophet Nathan confronted him about his affair with Bathsheba
                            and his attempt to cover up her resulting pregnancy by having her husband
                            killed, David acknowledged, <Bold>“…I have sinned against the Lord…” (2
                            Samuel 12:13 NLT).</Bold>
                        </Text>
                    },
                ]} />
                <Text style={styles.text}>
                    Would you have the emotional and spiritual maturity to make such an
                    admission? Or do you have a tendency to defend your actions for fear of being
                    judged, criticised or rejected? Do you sometimes feel attacked when someone
                    offers feedback, whether it is positive or negative? Do you retreat in silence?
                    Do you counter-accuse or blame your attacker? Do you make hostile
                    comments? Do you become sarcastic? Making mistakes doesn't make you a
                    lesser person, but defending them does. Don't let pride rob you of the wisdom
                    that comes from acknowledging your mistakes and ultimately growing
                    through them. <Bold>“He who disdains instruction despises his own soul, but he
                    who heeds rebuke gets understanding” (Proverbs 15:32).</Bold>
                </Text>
            </View>,
            prayerPoints: [
                "Help me not to sin against You, O Lord my God, in the name of Jesus.",
                "Holy Spirit, forgive my sins of disobedience, in the name of Jesus.",
                "I receive grace to retrace my steps whenever I sin, in the name of Jesus.",
            ]
        },
    ],
    October: [
        {
            day: 1,
            topic: "Affair-proof Your Relationship",
            wordStudy: "1 Corinthians 10:11-12",
            confession: "The Lord will instruct me and teach me in the way I shall go (Psalm 32:8).",
            fullText: <View>
                <Text style={styles.text}>
                    Most people don't plan on getting into an affair. They just happened to be with
                    the wrong person, at the wrong time, in the wrong frame of mind. The Bible
                    plainly warns, <Bold>“Even if you think you can stand up to temptation, be
                    careful not to fall.”</Bold> When you think, <Bold>“It couldn't happen to me,”</Bold> you're a
                    target for satan! So how can you affair-proof your marriage? By consistent
                    communication.
                </Text>
                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            <Italic>Communicate regularly with God.</Italic> Jesus said, <Bold>“When you pray, [say]…
                            'Give us this day our daily bread'”</Bold> Matthew <Bold>(Matthew 6:7-11 NKJV).</Bold>
                            Daily communication with God arms you against marital mischief. He
                            also knew you'd need to pray together. <Bold>“Our Father…give us…forgive
                            us our sins” (Matthew 6:9-12)</Bold> assumes we are needy people praying
                            together. Couples praying together are harder to pry apart.
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            <Italic>Communicate faithfully with God's Word.</Italic> Books about marriage can
                            inform you, but only the Bible, God's marriage manual, has the power to
                            transform your life together. Shared Bible reading illuminates your
                            understanding; it exposes, sensitizes, and purifies your hearts' intentions;
                            it safeguards your relationship.
                        </Text>
                    },
                    {
                        serial: 3,
                        body: <Text>
                            <Italic>Communicate openly with each other.</Italic> Build “relationship hedges” so
                            that temptation doesn't get a foothold. Establish sensible guidelines for
                            interacting with the opposite sex. Be open and honest with each other
                            about your social, workplace and church relationships. Anything that
                            makes your spouse uncomfortable should be noted, and, where possible
                            changed. Next to God, you are each other's best protection against failure.
                            So, listen, learn, and love!
                        </Text>
                    },
                ]} />
                <Text style={styles.text}>
                    <Bold>Happy Independence Day Nigeria!</Bold>
                </Text>
            </View>,
            prayerPoints: [
                "Holy Spirit, help me not to fall into temptation, in the name of Jesus.",
                "Help me, Lord, to walk in faith in my relationship with my spouse, in the name of Jesus.",
                "Father, give me the wisdom I need to make it easy for my spouse and others to co-habit amiably and peacefully with me, in the name of Jesus.",
                "Father, perfect all that concerns our country Nigeria and fight for us, in the name of Jesus.",
            ]
        },
        {
            day: 2,
            topic: "The Power of God's Word (1)",
            wordStudy: "Psalm 19:7-11",
            confession: "He sent His word and healed them, and delivered them from their destructions (Psalm 107:20).",
            fullText: <View>
                <Text style={styles.text}>
                    Of all the words ever spoken, only <Bold>“the Word of God is alive and powerful”
                    (Hebrews 4:12).</Bold> So when you read your Bible, you tap into a life-changing
                    force. Let's look at some of the wonderful characteristics and effects
                    attributed to God's Word in <Bold>Psalm 19.</Bold>
                </Text>
                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            <Bold>“The law of the Lord is perfect…”(v. 7a).</Bold> To understand the word law,
                            think of governing forces by which things like gravity, aerodynamics
                            and physics work. When you deny or defy these laws you suffer; when
                            you operate according to them you succeed, but either way they are
                            always in force! So is God's Word. It cannot be improved upon, added to,
                            or taken from. It is adequate and able to accomplish His purposes in your
                            life. Additionally, it <Bold>“refreshes the soul”!</Bold> Why wouldn't you embrace
                            it?
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            <Bold>“The statutes of the Lord are trustworthy…” (v. 7b NIV).</Bold> You can
                            count on the reliability and accuracy of God's Word in every
                            circumstance of life. In a world of changing values, conditions and
                            relationships, you can place your full weight on the Scriptures, confident
                            they will hold up—and hold you up, too! Furthermore, it will <Bold>“make
                            wise the simple”</Bold>—the ignorant and immature. It will produce in you
                            wisdom far beyond what's available through study and education.
                        </Text>
                    },
                    {
                        serial: 3,
                        body: <Text>
                            <Bold>“The precepts of the Lord are right…” (v. 8a NIV).</Bold> God's precepts are
                            accurate, correct and virtuous, and we are called to <Bold>“add to [our] faith
                            virtue”</Bold> (2 Peter 1:5 NKJV ) or moral excellence. This cannot be
                            accomplished through will-power, but through God's supernatural
                            Word-power.
                        </Text>
                    },
                ]} />
              
            </View>,
            prayerPoints: [
                "Lord, help me to depend only on Your Word always, in the name of Jesus.",
                "I receive grace to hunger and thirst after Your Word all the days of my life, in the name of Jesus.",
                "Lord, may Your Word forever be alive always in my heart, in the name of Jesus.",
            ]
        },
        {
            day: 3,
            topic: "The Power of God's Word (2)",
            wordStudy: "Psalm 19:7-11",
            confession: "For I am not ashamed of the gospel of Christ: for it is the power of God unto salvation to everyone that believeth…(Romans 1:16).",
            fullText: <View>
                <Text style={styles.text}>
                    Observe:
                </Text>
                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            <Bold>“The commandments of the Lord are right…”</Bold> When people like Plato
                            and Shakespeare speak, the effect of their words on us depends on what
                            we bring to the process. Some of us may be informed and entertained,
                            while others may be indifferent or bored. Their words could be described
                            as intrinsically neutral but this is not so with God's words: they radiate!
                            Like heat and light, they are sources of spiritual energy. They possess a
                            dynamic power that produces change wherever it goes. Likewise, God's
                            Word is dynamic. It radiates transforming power that changes anyone
                            who lives in its “environment”!
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            <Bold>“The judgments given by the Lord are trustworthy…” (v. 9b).</Bold> The
                            Hebrew word for trustworthy comes from “certainty, honesty and
                            faithfulness.” With absolute confidence, you can stake your all on the
                            reliability of God's Word. It guarantees you that God says what He
                            means, and means what He says. You can stand secure on every syllable
                            of it. In other words, you can <Bold>“live…by every word that proceeds from
                            the mouth of God” (Matthew 4:4 NKJV).</Bold>
                        </Text>
                    },
                    {
                        serial: 3,
                        body: <Text>
                            <Bold>“By your teachings, Lord, I am warned; by obeying them, I am
                            greatly rewarded”</Bold> (v. 11 CEV). God's Word is more than just a list of
                            “dos and don'ts.” It's like radar that signals to you when you're in danger
                            of straying off course and getting into trouble. Your conscience alone is
                            inadequate. <Bold>“How can I know all the sins lurking in my heart?
                            Cleanse me from these hidden faults… Don't let them control me” (v.
                            12-13).</Bold>
                        </Text>
                    },
                ]} />
                <Text style={styles.text}>
                    By obeying God's Word, you will be <Bold>“greatly rewarded”!</Bold>
                </Text>
              
            </View>,
            prayerPoints: [
                "Lord, may I live by every Word that proceeds from Your mouth, in the name of Jesus.",
                "Father, cleanse me from every hidden sin, in the name of Jesus.",
                "Lord, grant me the grace to trust in You with all my heart, in the name of Jesus.",
            ]
        },
        {
            day: 4,
            topic: "Are You Afraid of Losing Someone?",
            wordStudy: "1 Samuel 20:18",
            confession: "The Lord is good, a stronghold in the day of trouble; and He knows those who trust in Him (Nahum 1:7).",
            fullText: <View>
                <Text style={styles.text}>
                    Who is the one person whose death you dread most? Is it causing you
                    anxiety? If so, begin to put these things into practice:
                </Text>
                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            Ask God for the grace to accept death as an inevitable part of life. Both
                            you and your loved one will eventually die, and no one but God knows
                            who's going to do so first. Therefore, worrying about something outside
                            your control only robs you of your peace and joy.
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            Maximise each moment. If you knew the exact date of your loved one's
                            departure, what would you wish you had done? Or not done? Would you
                            take that trip you've always talked about? Would you be less critical and
                            more complimentary? Would you spend more time with them, and tell
                            them more often that you love them?
                        </Text>
                    },
                    {
                        serial: 3,
                        body: <Text>
                            Don't give another moment of your time to silly arguments and little
                            irritations.
                        </Text>
                    },
                    {
                        serial: 4,
                        body: <Text>
                            Make every effort to be sure your loved one is in right standing with
                            God. Then you can be assured they are at peace, and that you will spend
                            eternity together.
                        </Text>
                    },
                    {
                        serial: 5,
                        body: <Text>
                            Remember, your loved one is God's loved one, too. And He loves them
                            even more than you do. When the time comes, God will be right there
                            with you. <Bold>“Yea, though I walk through the valley of the shadow of
                            death, I will fear no evil: for You are with me…”</Bold> (Psalm 23:4 NKJV).
                        </Text>
                    },
                    {
                        serial: 6,
                        body: <Text>
                            Try to picture your loved one in Heaven. Jesus said, <Bold>“I go to prepare a
                            place for you…that where I am, there you may be also”</Bold> (John 14:2-3
                            NKJV). The parting will only be momentary, but the reunion will be
                            forever.
                        </Text>
                    },
                ]} />
              
            </View>,
            prayerPoints: [
                "Father, grant me the grace to accept what I cannot control, in the name of Jesus.",
                "Help me, Lord, to live in peace with Your will for my life, in the name of Jesus.",
                "I will make Heaven together with my loved ones; I receive the boldness to witness to my unsaved loved ones, in the name of Jesus.",
            ]
        },
        {
            day: 5,
            topic: "Beyond the Myths of Parenting (1)",
            wordStudy: "Psalm 127:3",
            confession: "He is in the way of life that keepeth instruction; but he that refuseth reproof erreth (Proverbs 10:17).",
            fullText: <View>
                <Text style={styles.text}>
                    Here are two myths of parenting:
                </Text>
                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            Good parents always keep tidy homes. We think our house should look
                            picture-perfect, so we get upset when our children turn it upside down.
                            However, an obsession with neatness can result in missing precious
                            moments that never come again. Unintentionally, we teach our children
                            that things—not people—are important. We instill the idea that keeping
                            up appearances matters more than enjoying life together. The truth is, the
                            house will be orderly sooner than you think, and quiet, and empty! So
                            enjoy the disarray, the laughter, the spills and scrapes. Let the nicks and
                            scratches on the furniture become memories of precious moments with
                            little people who'll grow up feeling loved and important to you.
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            Good parents must always be “right.” Writer and mother Ann Peterson
                            shares an insight that struck while she was engaged in a run-in with her
                            son. “Finally, through clenched teeth I managed to ask him, 'Why must
                            you always be right?' He responded through clenched teeth (must have
                            learned that from his father), 'Because you always have to be right.' I
                            sensed God watching that moment. Words were unnecessary. I got the
                            message loud and clear. From that moment on, being 'right' lacked the
                            lustre it once held for me.” The relationship with her son became less
                            resistant, “Something I would have missed, had I not conceded the need
                            to be always right.” So choose your battles carefully! Good parenting
                            isn't about racking up “victories” and dishing out “defeats”; it's about
                            enjoying your family.
                        </Text>
                    },
                ]} />
              
            </View>,
            prayerPoints: [
                "I shall not fail in my responsibility as a parent, in the name of Jesus.",
                "I hand over my life and family to You, O Lord, in the name of Jesus.",
                "I will not indulge my children in any ungodly attitude, in the name of Jesus.",
            ]
        },
        {
            day: 6,
            topic: "Beyond the Myths of Parenting (2)",
            wordStudy: "Psalms 127:4-5",
            confession: "I will chasten my children while there is hope (Proverbs 19:18a).",
            fullText: <View>
                <Text style={styles.text}>
                    Here are another two parenting myths.
                </Text>
                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            Good parents always produce good children. If only that were so, but it's
                            not! Even when you do all the right things, your children get to make
                            their own choices in life. Cain and Abel were raised in the same home by
                            the same parents, but Abel's choice pleased God while Cain's led him to
                            commit the first recorded murder. Even model parents have no control
                            over the choices their children ultimately make. This doesn't mean your
                            attempts to be a godly influence on them are wasted; not at all! It just
                            means that when you've done your best you should: (a) recognise and
                            accept your limits; (b) teach your children wisdom, allowing them to be
                            responsible for the consequences of their decisions; (c) trust God to do
                            what you cannot do. Some kids get the message quickly, others like the
                            Prodigal Son take detours—but God never gives up on them—and
                            neither should you.
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            Good parents treat all their children the same way. The Bible says,
                            <Bold>“Teach your children to choose the right path…”</Bold> (Proverbs 22:6
                            NLT). Every child is wired with a unique set of needs and abilities, and
                            wise parents recognise and work with these characteristics. Your
                            responsibility isn't to try to make them the “perfect kid.” It's to try to
                            discover the distinctive pattern God built into each child, and work to
                            develop that pattern in them. The reward God promises such parents is
                            <Bold>“when they are older [detours notwithstanding!] they will remain
                            upon it”</Bold> (Proverbs 22:6 NLT).
                        </Text>
                    },
                ]} />
              
            </View>,
            prayerPoints: [
                "I shall bring forth godly children, in the name of Jesus.",
                "I shall be a good example to my children, in the name of Jesus.",
                "I receive wisdom to succeed in my parental responsibilities, in the name of Jesus.",
            ]
        },
        {
            day: 7,
            topic: "From Guesswork to Guidance",
            wordStudy: "Psalm 32:8",
            confession: "I will trust in the Lord, with all of my heart, in Jesus name (Proverbs 3:5).",
            fullText: <View>
                <Text style={styles.text}>
                    You can know God's will for your life. He promises, <Bold>“I will instruct you and
                    teach you in the way you should go”</Bold> (Psalm 32:8 NIV). His Word says, <Bold>“In
                    all your ways acknowledge Him, and He shall direct your paths”</Bold> (Proverbs
                    3:6 NKJV). God wants you to move from guesswork to guidance, but getting
                    there requires four things:
                </Text>
                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            Knowing that God's will begins with surrendering your will. Jesus said,
                            <Bold>“I do not seek My own will, but the will of Him who sent Me”</Bold> (John
                            5:30). You only recognise God's will as you learn to lay aside your own
                            will, and that gets easier with practice.
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            Keeping a spiritual mindset. It's not possible to sense God's will while
                            you're controlled by self-interest. <Bold>“The mind governed by the flesh is
                            hostile to God. It does not submit to God's law, nor can it do so”</Bold>
                            (Romans 8:7 NIV).
                        </Text>
                    },
                    {
                        serial: 3,
                        body: <Text>
                            Praying for God's guidance. David prayed, <Bold>“Teach me to do Your will,
                            for You are my God; may Your good Spirit lead me…”</Bold> (Romans
                            143:10 NIV). James also encourages us: <Bold>“If you need wisdom, ask our
                            generous God, and He will give it to you. He will not rebuke you for
                            asking”</Bold> (James 1:5 NLT).
                        </Text>
                    },
                    {
                        serial: 4,
                        body: <Text>
                            Reading God's Word with a heart that's open to hear from Him. <Bold>“Your
                            word is a lamp that gives light wherever I walk”</Bold> (Psalm 119:105
                            CEV). Daily exposure to the Word of God will help you recognise His
                            voice when He speaks to you. <Bold>“The sheep follow Him: for they know
                            His voice”</Bold> (John 10:4 NKJV).
                        </Text>
                    },
                ]} />
              
            </View>,
            prayerPoints: [
                "Lord, teach me to seek Your will at all times, in the name of Jesus.",
                "Cause me to hunger and thirst after Your Word, O Lord, in the name of Jesus.",
                "Lord, revive my prayer altar, in the name of Jesus.",
            ]
        },
        {
            day: 8,
            topic: "How to Test Your Thoughts",
            wordStudy: "2 Corinthians 10:3-5",
            confession: "By the grace of the Lord, I will keep my heart with all diligence, in the name of Jesus (Proverbs 4:23).",
            fullText: <View>
                <Text style={styles.text}>
                    You will never be any better, wiser or happier than the thoughts that influence
                    you. You say, “I can't help it; these thoughts just come into my mind.” Yes,
                    you can! <Bold>“The weapons we fight with…have divine power to demolish
                    strongholds…we take captive every thought to make it obedient to
                    Christ”</Bold> (2 Corinthians 10:4-5 NIV). A stronghold is a thought pattern that
                    controls you. It could be fear, greed, inferiority, lust, etc. It's a stronghold
                    because it holds you strongly in its grip. How do you break that grip? With
                    God's Word.
                    <Br />
                    For example, when fear and inadequacy tell you you're not good enough
                    to succeed, become proactive and say, <Bold>“I can do all things through Christ
                    who strengthens me”</Bold> (Philipians 4:13 NKJV). If your immediate goal is to
                    “feel good,” you will never develop self-control. <Bold>“For the time being no
                    discipline brings joy, but seems grievous and painful; but afterwards it
                    yields a peaceable fruit of righteousness to those who have been trained
                    by it…”</Bold> (Hebrews 12:11 AMP). Discipline doesn't bring immediate joy, but
                    it brings lasting joy later on. Note the words “trained by it.” You must train
                    your mind to go the right way; otherwise it'll take you the wrong way. God
                    says, <Bold>“I have set before you life and death, blessing and cursing:
                    therefore choose life…”</Bold> (Deuteronomy 30:19 NKJV).
                    <Br />
                    God gives you choices. But He won't make your choices for you; you
                    must do that. In order to walk in God's blessing each day, you must choose the
                    right thoughts each day, and the longer you keep doing it the easier it will
                    become. You've just got to get through the “training” period.
                </Text>              
            </View>,
            prayerPoints: [
                "Every stronghold of spiritual blindness in my life, be pulled down today, in the name of Jesus.",
                "I reject the spirit of fear, in the name of Jesus.",
                "I receive soundness of mind, in the name of Jesus.",
            ]
        },
        {
            day: 9,
            topic: "We All Matter (1)",
            wordStudy: "Romans 12:4-5",
            confession: "The glory of God shall be revealed through us even as we remain as one, in the name of Jesus (John 17:21-22).",
            fullText: <View>
                <Text style={styles.text}>
                Ever tried to complete a jigsaw puzzle with a piece missing? It changes
                the end result, doesn't it? In the church God is building today, everyone has a
                place. Paul writes, <Bold>“…The whole building…fitted together, is growing…”</Bold>
                (Ephesians 2:21 <Bold>NAS</Bold>). A bundle of sticks is stronger than a single twig. So,
                <Bold>“…in Christ we…form one body, and each member belongs to all the
                others” (Romans 12:5 NIV).</Bold> A non-serving Christian is a contradiction in
                terms. In Nehemiah's day, it was the farmers of Tekoa who helped repair the
                wall, <Bold>“but their nobles would not put their shoulders to the work”
                9Nehemeiah 3:5 NRS).</Bold> Bricklaying wasn't part of their job description, so
                they let others do the heavy lifting. We all know people who subscribe to that
                philosophy!
                <Br />
                Never underestimate the significance of your assignment.
                <Bold>“…Parts…that seem…least important are…the most necessary” (1
                Corinthians 12:22 NLT).</Bold> Think about who is more important: a world leader
                or a sanitation worker. When Ronald Reagan was shot in 1981, although he
                was the leader of the free world, it was “business as usual” for the nation.
                Conversely, when Philadelphia refuse collectors went on strike the city
                descended into chaos as piles of rotting refuse built up everywhere. The point
                is, you don't have to be a seminary graduate to pray and read the Bible, or a
                prophet to hear from God, or a specialist to minister to hurting people. To do
                great things for God, be faithful in little ones <Bold>(Luke 16:10)</Bold>. Until you are
                willing to serve time on the assembly line, God can't promote you to
                management!
                </Text>              
            </View>,
            prayerPoints: [
                "Father, open our eyes to begin to see how important each member is as a member of the body of Christ, in the name of Jesus.",
                "Father, give me the grace to effectively do my own part, in the name of Jesus.",
                "Father, pull down the wall of division hindering the growth and spread of the Gospel, in the name of Jesus.",
            ]
        },
        {
            day: 10,
            topic: "We All Matter (2)",
            wordStudy: "Acts 2:44-46",
            confession: "For by one Spirit we we were all baptized into one body—whether Jews or Greeks, whether slaves or free—and have all been made to drink into one Spirit (1 Corinthians 12:13 NKJV).",
            fullText: <View>
                <Text style={styles.text}>
                    Synergy is when the whole is greater than the sum of its parts. And creating
                    positive synergy means having the right people, in the right place, at the right
                    time, focusing on the right goal. After World War II, Chester Nimitz, Chief of
                    US Naval Operations, wanted to keep alive public interest in naval aviation.
                    As a result the <Italic>Blue Angels Navy Flight Demonstration Squadron</Italic> was
                    created and performed its first exhibition within the year. To this day the team
                    has clear objectives. They select only qualified candidates who consistently
                    operate at peak performance. After being carefully screened, a pilot must
                    receive sixteen votes from the existing members. If one votes no, the
                    candidate is out of the running. No reasons or explanations required—that's
                    the level of trust and respect the members have for one another's judgment!
                    As a Blue Angel, your teammates become like your family, and when a new
                    member is enlisted you're entrusting him with your life. Making the squad
                    isn't a one-shot deal either. You're responsible for playing your part,
                    demonstrating your value and pursuing excellence on a daily basis. You have
                    to earn the right to wear the crest. Nothing short of total commitment is
                    accepted.
                    <Br />
                    So, would you qualify to be a Blue Angel? Are you a sole-trader or a team
                    player? Are you loyal and reliable? When the chips are down, can others rely
                    on you to put their interests ahead of your own?
                </Text>              
            </View>,
            prayerPoints: [
                "Lord Jesus, restore to Your church singleness of heart and purpose.",
                "Empower and uphold me to play my role accordingly without fail as a blood-bought member of Your body.",
                "Lord, unite Your church more than ever before this end time.",
            ]
        },
        {
            day: 11,
            topic: "Can They Tell You Have Been withJesus?",
            wordStudy: "Acts 11:26; 4:13",
            confession: "I shall bear much fruit and Christ will be magnified in me, in the name of Jesus (John 15:8; Philippians 1:20).",
            fullText: <View>
                <Text style={styles.text}>
                    Luann Prater says: “I got a company-logo licence plate…now everybody can
                    see who I work for. I was excited and proud till somebody cut me off in traffic
                    and I realised I can't react now without considering the impression it will
                    leave! If you had to wear and quote God's seal on your forehead <Bold>(Revelations
                    9:4),</Bold> would you be nicer to the store clerk, the grumpy boss, the bratty kid? If
                    the world could see at a glance who you are, would it alter your behaviour…?
                    When you're filled with the Holy Spirit it's as evident as having a nameplate
                    saying you belong to Christ… I've gotten used to the nameplate now…I think
                    before I react… consider the consequences… It no longer makes me
                    anxious…afraid I'll discredit my company. Now people see me coming and
                    wave…”
                    Who's reading <Italic>your</Italic> nameplate? Can they tell you have been with Jesus?
                    Jesus laid it out clearly: <Bold>“This is to My Father's glory, that you bear much
                    fruit, showing yourselves to be My disciples” (John 15:8 NIV).</Bold> If
                    somebody were to videotape your life, would it show you are a loving husband
                    or wife, a good parent, a conscientious worker and a joy to be around? Author
                    Lysa TerKeurst says: “Let's just be honest, it is tough being a sold-out soul for
                    Christ stuck in a flesh-filled body… I must allow…His teaching to seep into
                    my heart and mind…ask Him to interrupt my natural flesh response with the
                    truth He taught me… So if my husband forgets to do something, or my kids
                    punch my buttons, or somebody cuts me off in traffic…causing my flesh to rear
                    up…I can say, <Bold>'Not my will God, but yours be done'”</Bold>
                </Text>              
            </View>,
            prayerPoints: [
                "Father, empower me to live a life that points people to You, in the name of Jesus.",
                "Lord, let not Your name be reproached through my life. in the name of Jesus.",
                "Father, I receive grace this day to be Your good ambassador on Earth, in the name of Jesus.",
            ]
        },
        {
            day: 12,
            topic: "What Is Your Approach to People?",
            wordStudy: "Ephesians 4:32; Phillipians 2:3-4",
            confession: "I take the grace to live daily in love and fulfill Jesus' commandment to love my neighbor as myself, in the name of Jesus (Romans 13:8).",
            fullText: <View>
                <Text style={styles.text}>
                    It's said that William Gladstone and Benjamin Disraeli were great leaders but
                    intense rivals. Gladstone, leader of the Liberal Party, is considered by many
                    to personify the best qualities of Victorian England. A career public servant,
                    he was a great orator, master of finance, and a staunchly moral man. He was
                    made Prime Minister of Great Britain four different times, the only person in
                    history to achieve that honour. Under his leadership Great Britain established
                    a national education system, instituted parliamentary reform and saw the
                    vote given to a significant number of people in the working classes. Disraeli,
                    who served twice as Prime Minister, had a different kind of background. In
                    his thirties he entered politics and built a reputation as a diplomat and social
                    reformer. His greatest accomplishment was masterminding Britain's
                    purchase of the Suez Canal.
                    <Br />
                    Both men accomplished much. <Italic>But what really separated them was their
                    approach to people!</Italic> The difference can be best illustrated by a story told by a
                    young woman who dined with each of the two rival statesmen on consecutive
                    nights. When asked for her impression of them, she said, “When I left the
                    dining room after sitting next to Mr. Gladstone, I thought he was the cleverest
                    man in England. But after sitting next to Mr. Disraeli, I thought I was the
                    cleverest woman in England.” There's an important lesson here. Good
                    leaders win the confidence, trust and friendship of people they lead by taking
                    the spotlight off themselves and putting it on others. In fact, this principle will
                    work for <Italic>anybody.</Italic> It's why the Bible says, <Bold>“…In honour giving preference
                    to one another” (Romans 12:10 NKJV).</Bold>
                </Text>              
            </View>,
            prayerPoints: [
                "Father, empower me to walk in love always, in the name of Jesus.",
                "Holy Spirit, help me to never look down on any one as from today, in the name of Jesus.",
                "Empower me to live an edifying life, in the name of Jesus.",
            ]
        },
        {
            day: 13,
            topic: "Put Away Your Old Measuring Rod!",
            wordStudy: "Philippians 4:19; Ezekiel 47:3-12",
            confession: "For the Lord shall enlarge me and I shall breakforth right and left…in the name of Jesus (Isaiah 54:2-3).",
            fullText: <View>
                <Text style={styles.text}>
                    Paul writes, <Bold>“Now to Him who is able to do exceedingly abundantly
                    above all that we ask or think, according to the power that works in us”
                    (Ephesians 3:20 NKJV).</Bold> You say, “With my lack of education, limited
                    income, poor health and past mistakes, I don't see how I can succeed.” When
                    you're dealing with God, put away your old measuring rod!
                    <Br />
                    Ezekiel had a vision. He saw a river flowing from the temple: <Bold>“When the
                    man went out…with the line in his hand, he measured one thousand
                    cubits, and he brought me through the waters; the water came up to my
                    ankles. Again he measured one thousand and brought me through the
                    waters; the water came up to my knees. Again he measured one thousand
                    and brought me through; the water came up to my waist. Again he
                    measured one thousand, and it was a river that I could not cross; for the
                    water was too deep, water in which one must swim…Then He said to me
                    '…It shall be that every living thing that moves, wherever the rivers go,
                    will live… Along the bank of the river, …will grow all kinds of trees used
                    for food; their leaves will not wither, and their fruit will not fail. They will
                    bear fruit every month, because their water flows from the sanctuary…'”</Bold>
                    (Ezekiel 47:3-12 NKJV). Notice, this river flows from God. He is the One who
                    determines your future and decides your abundance. No matter who sows or
                    who reaps, <Bold>“God…gives the increase”</Bold> (1 Corinthians 3:7 NKJV).
                    <Br />
                    Who determines the extent of your blessing? God! So put away your old
                    measuring rod!
                </Text>              
            </View>,
            prayerPoints: [
                "Father, in any area I have been limiting you, please have mercy, in the name of Jesus.",
                "Any situation limiting God in my life, expire today, in the name of Jesus.",
                "Father, as from today, make me swim in Your overwhelming blessings, in the name of Jesus.",
            ]
        },
        {
            day: 14,
            topic: "You Can Conquer Worry",
            wordStudy: "Matthew 6:34; Phillipians 4:6",
            confession: "For God has not give me a spirit of fear, but of power and of love and of a sound mind, in the name of Jesus (2 Timothy 1:7).",
            fullText: <View>
                <Text style={styles.text}>
                    Most of the stuff we worry about never happens—or even turns out better
                    than we anticipate! Worry, like faith, is a spiritual force. Like a magnet, it
                    attracts the very things we fear, clouds our judgment and distorts our
                    perspective. God never intended us to carry tomorrow's burdens along with
                    today's. So, here are three steps to help you conquer worry:
                </Text>              
                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            <Italic>Replace it.</Italic> Telling somebody not to worry doesn't work. Worry is like an
                            emotional spasm; the only way to break it is to replace it. <Bold>“Whatever is
                            lovely, whatever is admirable, if anything is excellent or
                            praiseworthy, think about such things” (Philippians 4:8 NIV).</Bold> In
                            other words, switch the channel!
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            <Italic>Dissect it.</Italic> <Bold>“…Fear involves torment” (1John 4:18 NKJV).</Bold> Worry
                            torments you; your imagination runs amok, conjuring up all kinds of
                            scary scenarios. But it's also illogical; when you take it apart rationally
                            and systemically, it loses its power to control you.
                        </Text>
                    },
                    {
                        serial: 3,
                        body: <Text>
                            <Italic>Rise above it.</Italic> A well-known pastor was flying over the Mississippi River
                            one day when the sky grew dark. “We can't see where we're going!” he
                            exclaimed. Calmly the pilot replied, “We just need to rise above the
                            ground heat, dust and smoke.” After climbing another 300 metres they
                            emerged into a clear, beautiful world.
                        </Text>
                    },
                ]} />
                <Text style={styles.text}>
                    Corrie Ten Boom called faith “the radar that pierces through the fog.”
                    When worry tries to fog you in, you can rise above it by placing your trust in
                    the Lord. <Bold>“Those who trust in the Lord…will soar…like eagles…”
                    (Isaiah40:31 NLT).</Bold>
                </Text>  
            </View>,
            prayerPoints: [
                "I bind the spirit of worry operating in my life, in the name of Jesus.",
                "I take authority over every unpleasant situation in my life and command them to bow this day to the name of Jesus",
                "Father, enable me to trust wholeheartedly in You always, in the name of Jesus.",
            ]
        },
        {
            day: 15,
            topic: "A Blueprint for Prayer",
            wordStudy: "Luke 11:2-4",
            confession: "From today I surrender my will to You Lord, I will not pray amiss, in the name of Jesus (James 4:3).",
            fullText: <View>
                <Text style={styles.text}>
                    Jesus taught us to pray: <Bold>“Our Father in Heaven, Hallowed be Your name.
                    Your Kingdom come. Your will be done in earth, as it is in Heaven. Give
                    us this day our daily bread. And forgive us our debts, as we forgive our
                    debtors. And lead us not into temptation, but deliver us from the evil one:
                    For Yours is the kingdom and the power, and the glory forever. Amen”
                    (Matthew 6:9-13 NKJV).</Bold> When it comes to praying, Jesus gave us a
                    blueprint:
                    <Br />
                    <Bold>“Our Father”</Bold> establishes the basis of your relationship with God. It
                    reminds you that you're not just part of His creation, you're His child! Think
                    about it; you can create something without being related to it, but once you
                    father it, it's always part of you. <Bold>“In Heaven”</Bold> affirms your connection to the
                    God of the universe. <Bold>“The Lord…made the heavens His throne; from
                    there He rules over everything” (Psalm 103:19 NLT).</Bold> You are
                    acknowledging God's power and His unique qualifications to answer your
                    prayers. <Bold>“Hallowed be Your name”</Bold> reminds you that God is holy; that being
                    His child doesn't give you the right to disobey or take Him for granted. When
                    you <Bold>“enter into His gates with thanksgiving, and into His courts with
                    praise” (Psalm 100:4 NIV),</Bold> it gets His attention, because by exalting His
                    name you're recognising His character. <Bold>“Your will be done in earth”</Bold> is
                    reminiscent of Jesus' prayer in Gethsemane: <Bold>“…Your will…be done, not
                    mine” (Matthew 26:39 NLT).</Bold>
                    <Br />
                    Christ taught us to seek God's agenda, not our own; to have the same mind
                    as our heavenly Father; to walk with Him by praying in agreement with Him.
                </Text>             
            </View>,
            prayerPoints: [
                "Lord, help me always to pray according to Your will, in the name of Jesus.",
                "Father, as from today, I yield myself to You in absolute surrender and submit to Your will, in the name of Jesus.",
                "As I pray in agreement with You Lord, answer me speedily, in the name of Jesus.",
            ]
        },
        {
            day: 16,
            topic: "Work on Your Marriage",
            wordStudy: "Ephesians 5:21-25",
            confession: "Love and respect shall dominate my home from now on, in the name of Jesus (Ephesians 5:33).",
            fullText: <View>
                <Text style={styles.text}>
                    Former US Education Secretary William Bennett attended a contemporary
                    wedding where the bride and groom pledged in their vows to remain together,
                    “as long as love shall last.” Bennett said, “I sent them paper plates as a
                    wedding gift.” If you want to build a great marriage, one that will go the
                    distance, you must concentrate on doing these four things:
                </Text>             
                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            <Italic>Take responsibility for your own happiness.</Italic> By blaming your spouse
                            you never have to face yourself honestly or change your own behaviour.
                            That's a cop-out! Relationships are not dumping grounds. Happiness is
                            an inside job; it comes from having a healthy self-esteem and a growing
                            relationship with God.
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            <Italic>Be a friend.</Italic> Ever noticed how easily we accept our friends as they are,
                            yet have trouble doing that with the one we are married to? Look out,
                            familiarity breeds contempt! Doesn't your partner deserve at least the
                            same respect, loyalty, patience, gratitude and appreciation you give
                            others?
                        </Text>
                    },
                    {
                        serial: 3,
                        body: <Text>
                            <Italic>Have a shared goal.</Italic> You always need something to plan for and work
                            towards together. Doing this will enrich your relationship and take it to
                            higher levels. So, what's your next goal?
                        </Text>
                    },
                    {
                        serial: 4,
                        body: <Text>
                            <Italic>Have courage.</Italic> There are no perfect situations in life. You need courage
                            to face whatever comes, to realise that what you can't solve, you can outlast
                            and out-love!
                        </Text>
                    },
                ]} />
            </View>,
            prayerPoints: [
                "Father, help me to put in whatever it takes to make my marriage work, in the name of Jesus.",
                "Holy Spirit, empower me to fulfill my marital vow unconditionally, in the name of Jesus.",
                "Lord, restore my marital joy, in the name of Jesus.",
            ]
        },
        {
            day: 17,
            topic: "Do It God's Way and Find Fulfilment",
            wordStudy: "Isaiah 55:8-9; Matthew 11:28-29",
            confession: "The voice of the Lord will guide me in the way of peace and fulfillment, in the name of Jesus (Isaiah 30:21).",
            fullText: <View>
                <Text style={styles.text}>
                    Using his own home as a demo, a TV anchorman showed his viewers how to
                    protect their homes from robbers. He installed double locks on all the doors,
                    windows that can't be opened from outside, sophisticated alarms; the works.
                    But what he failed to take into account was that thieves were also watching and
                    learning the details of his security system and where the TV, VCR and
                    furniture were located. The following week, while he was on the air, they
                    cleaned him out because that's what thieves do! Satan is a thief! Jesus said he
                    <Bold>“comes only to steal and kill and destroy; I have come that [you] may have
                    life…to the full”(John 10:10 NIV).</Bold>
                    <Br />
                    Since Adam and Eve, people have had to choose whom to believe. Satan
                    convinced them that [disobeying] God would gain them everything. Instead
                    their disobedience robbed them of all they had. For the rest of their lives they
                    experienced only a fraction of the blessing God had intended. The world seeks
                    to convince you that you will find fulfilment by adopting its standards of
                    morality for your marriage, raising your children, and advancing your
                    career… If you believe this you will never experience the blessings God
                    intended. Sin brings death <Bold>(Romans 6:23).</Bold> Jesus wants you to live your life
                    with security, knowing that you are a beloved child of God. If you are not
                    experiencing His love, joy and peace, you have settled for less than God
                    intends. If you have been making excuses for why you're not living an
                    abundant and joyful life, determine today to settle for nothing less than God's
                    best. Instead of following the world's way, listen to the Saviour's voice and find
                    fulfilment.
                </Text>             
               
            </View>,
            prayerPoints: [
                "Father, in any area I have “tried to help You,” have mercy on me, in the name of Jesus.",
                "Father, destroy every alternative I have put up consciously or otherwise in Your place, in the name of Jesus.",
                "As I walk in Your way, Lord, let every benefit You have in store for me begin to manifest, in the name of Jesus.",
            ]
        },
        {
            day: 18,
            topic: "Show Appreciation",
            wordStudy: "Ephesians 5:20; Colossians 3:15",
            confession: "Thank You, Lord, for all Your wonderful deeds in my life (Psalm 100:4).",
            fullText: <View>
                <Text style={styles.text}>
                    Some of us “self-starters” need little or no appreciation; the rest of us need it
                    regularly. God told Moses to encourage Joshua, his replacement, because he
                    had big shoes to fill. Even Paul needed appreciation. <Bold>“…We were troubled
                    on every side… Nevertheless God, who comforts the downcast,
                    comforted us by the coming of Titus” (2 Corinthians 7:5-6 NKJV).</Bold> When
                    you show appreciation, you are doing God's work! We think wonderful things
                    about people but we don't tell them. Appreciation only works when you
                    express it!
                    <Br />
                    A psychologist interviewed the son of a successful business tycoon. The
                    boy had refused to take over the family business after his dad's death, even
                    though it would have made him very rich. The boy explained: “You don't
                    understand the relationship I had with my father. He was a driven man who
                    came up the hard way. His objective was to teach me self-reliance, and he
                    thought the best way to do it was never to encourage or praise me. Every day
                    we played ball in the yard. The idea was for me to catch the ball ten straight
                    times. I'd catch it eight or nine times, but always on the tenth throw he would
                    do everything possible to make me miss it. He would throw it on the ground
                    or over my head so I had no chance of catching it.” Then he paused tearfully
                    and said, “That's why I have to get away; I want to catch that tenth ball!” This
                    young man grew up feeling he could never be good enough to please his
                    father. Sound familiar? The Bible says, <Bold>“Anxiety in the heart of man causes
                    depression, but a good word makes it glad” (Proverbs 12:25 NKJV).</Bold> So,
                    show appreciation.
                </Text>             
            </View>,
            prayerPoints: [
                "Lord, baptize me with the spirit of thanksgiving. in the name of Jesus.",
                "I receive grace to be able to appreciate others' acts of kindness, in the name of Jesus.",
                "Father, please forgive me for every lack of appreciation, in the name of Jesus.",
            ]
        },
        {
            day: 19,
            topic: "Don't Stop Praying for Your Children",
            wordStudy: "Isaiah 54:18; Psalm 144:12",
            confession: "My children are godly children; they are not given to lying for God is their Saviour (Isaiah 63:8).",
            fullText: <View>
                <Text style={styles.text}>
                    Every saint was once a child; so was every criminal. What makes the
                    difference? At the top of the list is the influence of a parent! The prophet,
                    Samuel, led the nation of Israel for forty years, anointed David to be king and
                    guided him in some of his most crucial decisions. But who was the dominant
                    influence in Samuel's life? His mother Hannah. Listen to her prayer at his
                    birth: <Bold>“I prayed for this child, and the Lord has granted me what I asked of
                    him. So now I give him to the Lord. For his whole life he will be given over to
                    the Lord…” (1 Samuel 1:27-28 NIV).</Bold>
                    <Br />
                    Never underestimate the power that's released when a parent prays on
                    behalf of a child. Only God knows how many prayers are being answered right
                    now because of a faithful parent who prayed many years ago. If what you are
                    doing in this fast-paced society is taking you away from prayer time for your
                    children and your grandchildren, you are doing too much! There's nothing
                    more important than the time you spend with God on their behalf. It's not too
                    late for the child who has brought you tears and grief. Jesus' mother had to
                    watch her son being crucified, but she also had the joy of seeing Him raised
                    again from the dead. Indeed, you may go to your grave wondering if your
                    prayers are effective and when they will be answered, but never, never stop
                    praying, because: (a) God works according to His schedule, not ours (b) His
                    ways and means are far superior to ours (c) When a parent prays, God moves!
                </Text>             
            </View>,
            prayerPoints: [
                "Father, let my children fulfill Your purpose for their lives by conforming them to the image of Christ, in the name of Jesus.",
                "My children will not fulfill evil end-time prophecies, in the name of Jesus.",
                "Father, keep my children in the path of holiness and righteousness that they may fulfill their destiny in You, in the name of Jesus.",
            ]
        },
        {
            day: 20,
            topic: "Keep Practising",
            wordStudy: "Ecclesiastes 9:10; 1 Timothy 4:13, 15-16",
            confession: "Lord, help me give myself wholly to following You and Your instructions daily.",
            fullText: <View>
                <Text style={styles.text}>
                    The average person in any line of work could double their productive
                    capacity if they began right now to practice all the things they know they
                    should do, and stopped practicing all the things they know they shouldn't do.
                    Now, success doesn't come just by practice; you must practice right and do
                    the right things. Dr. John Maxwell gives us some pillars of practice we should
                    incorporate into our daily lives in order to succeed:
                </Text>             
                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            <Italic>A good coach.</Italic> Did you hear about the guy who entered his mule into the
                            Melbourne Cup? “He will never win,” a friend said. “I know,” he
                            replied, “but we only learn and grow by association.” You need to get
                            around the right people. <Bold>(2 Thessalonians 3:6-9).</Bold>
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            <Italic>Your best effort.</Italic> Are you practicing in a way, a time and a place that
                            allow you to give your best? Are you giving yourself incentives? Paul's
                            motivation was: <Bold>“a crown is being held for me” (2 Timothy 4:8 NCV).</Bold>
                        </Text>
                    },
                    {
                        serial: 3,
                        body: <Text>
                            <Italic>A clear purpose.</Italic> Be certain about what you're trying to accomplish.
                            Effort without knowledge is like speed without direction. Paul's
                            objective was to <Bold>“complete the task the Lord Jesus has given me”
                            (Acts 20:24 NIV).</Bold>
                        </Text>
                    },
                    {
                        serial: 4,
                        body: <Text>
                            <Italic>The right resources.</Italic> Paul challenges Timothy to become a reader <Bold>(1
                            Timothy 4:13).</Bold> What resources should you invest in to help you excel?
                        </Text>
                    },
                ]} />
                <Text style={styles.text}>
                    You only play well if you practice well. So, where are you on the practice
                    continuum? Just starting out? At the phase of rapid progress? Have you
                    already made your greatest gains, now you're fine tuning? Knowing where
                    you are, helps you know what you need to do. So keep practicing.
                </Text>    
            </View>,
            prayerPoints: [
                "Lord, empower me to daily give myself to doing what I need to do, in the name of Jesus.",
                "Father, please surround me with the right people always, in the name of Jesus.",
                "Lord, help me do the right things and do them right also, in the name of Jesus.",
            ]
        },
        {
            day: 21,
            topic: "Integrity (1)",
            wordStudy: "1 Samuel 12:1-5",
            confession: "Let integrity and uprightness preserve me; for I wait on thee (Psalm 25:21 KJV).",
            fullText: <View>
                <Text style={styles.text}>
                    To protect themselves from the barbaric hordes from the north, the people of
                    China built the Great Wall. It was so high nobody could climb over it and so
                    thick nobody could break through it, so they settled back to enjoy life. During
                    the first hundred years of the wall's existence, China was invaded three
                    different times. Not once did the enemy try to break down the wall or climb
                    over it; they simply bribed the gatekeeper and marched in. While those who
                    built it were relying on their wall of stone, they neglected to teach integrity to
                    their children. As a result they grew up without moral and spiritual principles
                    to guide them.
                    <Br />
                    Have you ever watched a big tree fall while others around it stood tall?
                    How come the same storm that builds strength in one, topples another? You'll
                    find the answer in the tree's core and roots. Getting the idea? When it comes to
                    building integrity, here are some questions you should ask yourself regularly:
                </Text>             
                <IndentList items={[
                    {
                        serial: <Text><Bold>a</Bold></Text>,
                        body: <Text>
                            <Italic>Am I the same, no matter who I'm with?</Italic>
                        </Text>
                    },
                    {
                        serial: <Text><Bold>b</Bold></Text>,
                        body: <Text>
                            <Italic>Am I willing to make decisions that are best for others, even though
                            another choice would benefit me more?</Italic>
                        </Text>
                    },
                    {
                        serial: <Text><Bold>c</Bold></Text>,
                        body: <Text>
                            <Italic>Can I be counted on to keep the commitments I've made to God,
                            myself and others?</Italic> Can you say like the Psalmist, <Bold>“Judge me, O
                            Lord; for I have walked in…integrity…”? (Psalm 26:1 KJV).</Bold>
                        </Text>
                    },
                ]} />
                <Text style={styles.text}>
                    Life is like a vice: at times it will squeeze you. In those moments whatever
                    is inside will come out. Image-building and self-interest promise much but
                    produce little, but integrity never disappoints. So, work on your integrity.
                </Text>    
            </View>,
            prayerPoints: [
                "Lord, help me to walk in integrity always, no matter what happens, in the name of Jesus.",
                "Holy Spirit, show me how to work on my integrity, in the name of Jesus.",
                "I shall not compromise in any way, in the name of Jesus.",
            ]
        },
        {
            day: 22,
            topic: "Integrity (2)",
            wordStudy: "Job 2:3; 1 Kings 9:4",
            confession: "Let integrity and uprightness preserve me; for I wait on thee (Psalm 25:21 KJV).",
            fullText: <View>
                <Text style={styles.text}>
                    Though repeatedly tempted by Potiphar's wife, Joseph said no. Faced with
                    similar circumstances, David said yes. But the story doesn't end there. David
                    married Bathsheba; when their son Solomon grew up, <Bold>“he had seven
                    hundred wives…and three hundred concubines, and his wives led him
                    astray” (1 Kings 11:3 NIV).</Bold> Integrity may not seem like a big deal now, but
                    your lack of it will have far-reaching consequences. Integrity means keeping
                    commitments, even though the circumstances in which you made them have
                    changed. One leader points out that when integrity is the referee, your lips
                    and your life will be in agreement. Your beliefs will be mirrored by your
                    behaviour. There will be no discrepancy between what you appear to be and
                    what you are, whether in good times or bad. Integrity is not only the referee
                    between opposing value systems, it's the decision maker between being at
                    peace and being fragmented within. It frees you to become a whole person no
                    matter what comes your way.
                    <Br />
                    There is a story about a job applicant who was asked why he was
                    discharged from his last position. He replied, “Because I wanted to take work
                    home with me.” When asked who he worked for and what he does, he replied,
                    “I work with a bank as a cashier.” We smile, but in a recent survey of
                    employees, 55% said they didn't trust their top management. Are you
                    trustworthy? People's minds are changed through observation, not argument.
                    People do what people see. What they hear, they understand; what they see,
                    they believe and follow. That's why a corrected and contrite Psalmist prayed,
                    <Bold>“Guard my life… let me not be put to shame… May integrity…protect
                    me…” (Psalm 25:20-21 NIV).</Bold>
                </Text>              
            </View>,
            prayerPoints: [
                "I take grace not to trade my integrity away, in the name of Jesus.",
                "Lord, make me a man/woman of integrity, in the name of Jesus.",
                "By Your grace, Lord, I will not fail when my integrity is being tested, in the name of Jesus.",
            ]
        },
        {
            day: 23,
            topic: "Your Self-esteem Must Come from God",
            wordStudy: "Jeremiah 1:5-8",
            confession: "Judges 6:12 I am a man of valour because the spirit of God dwells in me (Judges 6:12).",
            fullText: <View>
                <Text style={styles.text}>
                    Dr Martyn Lloyd-Jones said, “Though you are one of the teeming millions in
                    this world, and though the world would have you believe that you do not count
                    and that you are but a speck in the mass, God says, 'I know you.'” How
                    wonderful is that? Take a moment and consider the things in life that rob us of
                    self-esteem. Words such as, “I don't want you; I don't love you anymore.” Or
                    “You're unsuitable for the job; sorry, we have to let you go.” Or, “You keep
                    making the same stupid mistakes. You'll never get it right.” Or, “Why can't you
                    be like your brother, or your sister, or so-and-so?” Understand this:
                </Text>            
                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            <Italic>Your self-esteem cannot be based on what you do for a living.</Italic> Because
                            when you can no longer do it you'll feel worthless. Think about some of
                            the famous people you know: athletes, artists, speakers, etc. When they
                            can no longer do what they do they often get depressed, even suicidal.
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            <Italic>Your self-esteem cannot be based on what you own.</Italic> Recently the US
                            housing market lost up to 40% of its value in one year. Millions of people
                            saw their social status go down and their financial security go up in
                            smoke.
                        </Text>
                    },
                    {
                        serial: 3,
                        body: <Text>
                            <Italic>Your self-esteem cannot be based on other people's opinion</Italic> because it
                            will always make you vulnerable to rejection.
                        </Text>
                    },
                ]} />  
                <Text style={styles.text}>
                    What's the solution? Discover what God thinks about you and build your
                    life on it. <Bold>“…Thus says the Lord, who created you…who…redeemed you;
                    I have called you by your name; You are Mine” '(Isaiah 43:1 NKJV).</Bold> It
                    doesn't get any better than that!
                </Text>  
            </View>,
            prayerPoints: [
                "Father help me come into the full knowledge of who I am in Christ and to live it out, in the name of Jesus.",
                "I withdraw from circulation every negative statement affecting my self-esteem, in the name of Jesus.",
                "Father, help me not to base my life and self-esteem on temporal things but on eternal values, in the name of Jesus.",
            ]
        },
        {
            day: 24,
            topic: "Life Will Test You (1)",
            wordStudy: "Genesis 22:1-4",
            confession: "Examine me, O LORD, and make me of a proven character in you (Psalm 26:2).",
            fullText: <View>
                <Text style={styles.text}>
                    Do you remember the tests you took in school? You passed or failed, but you
                    couldn't avoid them. Life works that way too. When it comes to life's tests
                    you must prepare yourself in advance! Jesus said: <Bold>“…A wise man…built his
                    house on the rock: and the rain descended, the floods came, and the
                    winds blew and beat on that house; and it did not fall… a foolish
                    man…built his house on the sand: and the rain descended, the floods
                    came, and the winds blew and beat on that house; and it fell. And great
                    was its fall” (Matthew 7:24-27 NKJV).</Bold>
                    <Br />
                    The first man built his house on rock because he knew it wasn't a question
                    of if, but when, a storm would come. The second man built his house on sand
                    because it was cheap and easy. When the storm came the first man's house
                    stood and the second man's house fell. What's the point Jesus was making?
                    Your talents, your resume and your reputation may get you to the top, but if
                    you haven't built strong character you won't stay there long. Furthermore,
                    your beliefs may be sincere and line up with what other people around you
                    think, but unless they're founded on God's Word, they will fail you when you
                    need them most. Four times in the Bible we read, <Bold>“…The just shall live by
                    <Italic>faith</Italic>” (Habakkuk 2:4; Romans 1:17; Galatians 3:11, Hebrews 10:38).</Bold>
                    When the tests of life come you have got to be able to rise up and say, “I may
                    not have all the answers, but I have proven God's character and track record
                    and I'm trusting Him to do what He has promised in His Word!”
                </Text>            
            
            </View>,
            prayerPoints: [
                "Holy Spirit, help me develop a time-tested and proven character, in the name of Jesus.",
                "I receive grace to stand when the storms of life come, in the name of Jesus.",
                "Lord, increase my faith in You, in the name of Jesus.",
            ]
        },
        {
            day: 25,
            topic: "Life Will Test You (2)",
            wordStudy: "Psalm 17:3; Deuteronomy 13:3",
            confession: "Examine me Lord, and make me of a proven character in You",
            fullText: <View>
                <Text style={styles.text}>
                    God told His people: <Bold>“…The Lord your God led you…these forty years in
                    the wilderness, to…test you, to know what was in your heart, whether you
                    would keep His commandments or not” (Deuteronomy 8:2 NKJV).</Bold> The
                    tests of life reveal how well you take instruction, what you've learned, and
                    what you will do in any given set of circumstances. What you have been taught
                    is only theory, until it's been tested. And life will test you!
                    <Br />
                    Chuck Swindoll tells the story of a bird named Chippy: “Chippy the
                    parakeet never saw it coming. One second he was peacefully perched in his
                    cage singing, the next he was sucked in, washed up, and blown over. His
                    problem began when his owner decided to clean his cage with a vacuum. She
                    stuck the nozzle in to suck up the seeds and feathers at the bottom of the cage.
                    Then the phone rang. Instinctively she turned to pick it up. She'd barely said
                    'Hello' when Chippy got sucked in. She gasped, let the phone drop, and
                    snapped off the vacuum. With her heart in her mouth, she unzipped the bag.
                    There was Chippy alive, but stunned, covered with heavy black dust. She
                    grabbed him and rushed to the bathtub, turned on the faucet full blast and held
                    Chippy under a torrent of ice-cold water, power-washing him clean. Then she
                    did what any compassionate pet owner would do: she snatched up the
                    hairdryer and blasted the wet, shivering little bird with hot air.” Swindoll
                    closes his story with these words: “Chippy doesn't sing much anymore.”
                    Life will test you, but don't let it steal your song!
                </Text>            
            
            </View>,
            prayerPoints: [
                "When life tests me, Father, help me to be strong in You, in the name of Jesus.",
                "Lord, enable me to develop godly character, in the name of Jesus.",
                "Father, make me victorious anytime the tests of life come, in the name of Jesus.",
            ]
        },
        {
            day: 26,
            topic: "Life Will Test You (3)",
            wordStudy: "Exodus 20:20; Mark 6:45-51",
            confession: "The Lord will deliver me from all afflictions in Jesus name (Psalm 34:19).",
            fullText: <View>
                <Text style={styles.text}>
                    Do you remember your school days? When you were being tested the teacher
                    was silent. The Bible says: <Bold>“…Jesus made His disciples get into the boat
                    and go on ahead of Him… When evening came, the boat was in the
                    middle of the lake, and…He saw the disciples straining at the oars,
                    because the wind was against them. About the fourth watch of the night
                    [just before the dawn] He went out to them, walking on the lake…They
                    cried out, because they…were terrified. Immediately He spoke to them
                    and said, 'Take courage! It is I. Don't be afraid'; Then He climbed into
                    the boat with them, and the wind died down. They were completely
                    amazed” (Mark 6:45-51 NIV).</Bold> This story teaches that:
                </Text>     

                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            <Italic>Being in the will of God requires you to go through storms.</Italic> <Bold>“Many are
                            the afflictions of the righteous: but the Lord delivers [them] out of
                            them all” (Psalm 34:19 NKJV).</Bold>
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            <Italic>When you can't see Him, His eye is still on you.</Italic> You are never out of His
                            sight, His care or His reach.
                        </Text>
                    },
                    {
                        serial: 3,
                        body: <Text>
                            <Italic>He will never give you an assignment you can complete without His
                            help,</Italic> so don't try it alone.
                        </Text>
                    },
                    {
                        serial: 4,
                        body: <Text>
                            <Italic>If you let it, fear will cloud your thinking and keep you from recognising
                            Him when He comes to you.</Italic>
                        </Text>
                    },
                    {
                        serial: 5,
                        body: <Text>
                            <Italic>First He will speak to you, then to the circumstances that threaten you.</Italic>
                            When He does, you'll experience supernatural peace.
                        </Text>
                    },
                    {
                        serial: 6,
                        body: <Text>
                            When your boat is <Bold>“in the middle”</Bold> of the storm, the best is yet to come!
                        </Text>
                    },
                    {
                        serial: 7,
                        body: <Text>
                            <Italic>The darkest hour is just before the dawn.</Italic> Rejoice! The sun will shine
                            again; God won't fail you.
                        </Text>
                    },
                ]} />
            
            </View>,
            prayerPoints: [
                "My Father, when I am going through my test, let me feel You close by, in the name of Jesus.",
                "Father, let the fear of Your guide me and keep me in Your will always, in the name of Jesus.",
                "Lord, deliver me from temptation, in the name of Jesus.",
            ]
        },
        {
            day: 27,
            topic: "No Peace? It's Out!",
            wordStudy: "John 14:27; Phillipians 4:7",
            confession: "The peace of God shall keep my heart and mind from anxiety and guide me to make the right decisions always, in the name of Jesus (Colossians 3:15).",
            fullText: <View>
                <Text style={styles.text}>
                    When God speaks to us He gives us a deep sense of inner peace to confirm that
                    the message is truly from Him. Beware of false peace! When we have a strong
                    desire to do something it can produce a false sense of peace that actually
                    comes only from our excitement. As time passes this false peace disappears
                    and God's true will for our lives emerges. So we should never move too
                    quickly on important decisions.
                    <Br />
                    When the devil speaks to us he cannot give us peace. And when we try to
                    solve things with our own reasoning, we cannot get peace. <Bold>“But if the Holy
                    Spirit controls your mind, there is…peace” (Romans 8:6 NLT).</Bold> Lay your
                    decision on the scales of peace. Don't proceed if your inner peace cannot hold
                    its weight against what you think or hear. You don't have to explain to others
                    why you don't have peace about it; sometimes you won't know why. Just say,
                    “It's not wise for me to do this because I don't have peace about it.” There's
                    power in having peace! And once you know you've clearly heard from God,
                    you must do all you can to “keep your peace” and not become anxious.
                    <Br />
                    Peace is an inner “knowing” that your actions are approved by God. <Bold>“Let
                    the peace (soul harmony which comes) from Christ rule (act as umpire
                    continually) in your hearts [deciding and settling with finality all
                    questions that arise in your minds, in that peaceful state] to which…you
                    were also called [to live]” (Colossians 3:15 AMP).</Bold> God leads us by peace.
                    His peace is like an umpire who decides what's “safe” or what's “out.” No
                    peace? It's “out.”
                </Text>            
           
            </View>,
            prayerPoints: [
                "Lord, let Your peace guide me in all my choices in life, in the name of Jesus.",
                "Concerning that pressing matter (mention it)………………, Lord, give me peace, in the name of Jesus.",
                "I speak peace to every storm in my life and family, in the name of Jesus.",
            ]
        },
        {
            day: 28,
            topic: "Do the Numbers (1)",
            wordStudy: "Psalm 90:12; 39:4",
            confession: "The Lord knoweth my days and I shall not be afraid in the evil time (Psalm 37:18-19).",
            fullText: <View>
                <Text style={styles.text}>
                    Our souls aren't hungry for fame, comfort, wealth or power. Those things
                    create as many problems as they solve. Our souls are hungry for meaning. We
                    want our lives to matter. The Psalmist writes, <Bold>“Our lifetime is seventy years
                    or, if we are strong, eighty…” (Psalm 90:10 NCV).</Bold> Eighty years is just
                    under 30, 000 days. Think about that in financial terms: $30 000 will buy you
                    a car or make a deposit on a house. It's not that much money and it's not that
                    much time. None of us knows how many years we have left, but we know
                    how many we have been given till now. If you were to draw a line and add
                    them up, eternally speaking, what would you have to show for your life? Job
                    said, <Bold>“My days fly faster than a weaver's shuttle…” (Job 7:6 NLT);
                    “…they flee away…” (Job 9:25 NAS).</Bold>
                    <Br />
                    Bryan Wilkerson says: “Teens count the number of friends they have, the
                    number of colleges they get into. College students count grade
                    points…credit hours…how many countries they have visited… Adults
                    measure success by the number of bedrooms in their house, cars in the
                    garage…or the yield on their investments. Most make two mistakes…they
                    think they've so much [time] they can afford to waste it, or so little time they
                    can't possibly do something significant, so they don't try. Our days are like
                    suitcases all the same size but some can pack more into them than others. 'A
                    person who chases fantasies has no sense' <Bold>(Proverbs 12:11 NLT).</Bold>
                    Numbering your days means offering them to God and seeking His direction
                    for your life.”
                </Text>            
           
            </View>,
            prayerPoints: [
                "Lord, by Your mercy, restore to me my wasted years, in the name of Jesus.",
                "Father, teach me to apply my heart unto wisdom for the remaining years of my life, in the name of Jesus .",
                "Lord, empower me to live in Your will, in the name of Jesus.",
            ]
        },
        {
            day: 29,
            topic: "Do the Numbers (2)",
            wordStudy: "Job 7:1; Matthew 5:16",
            confession: "The Lord knoweth my days and I shall not be afraid in the evil time (Psalm 37:18-19).",
            fullText: <View>
                <Text style={styles.text}>
                    Talking about a friend's funeral, a Pastor says: “The man…was a
                    Christian—bright, hardworking, and dynamic. He helped start two hospitals
                    …the local football association…travelled…sang in choirs around the
                    world…a man of remarkable energy and ability… But nobody spoke of his
                    spiritual influence. He introduced many to soccer but few…to Christ.” The
                    pastor could say nothing of his contribution…except he critiqued the sermon
                    and complains about the bad grammar after service. His wife and children had
                    few words; they got the short end of his attention and energy. Imagine what he
                    <Italic>could</Italic> have accomplished by offering his time and talent to God.
                    <Br />
                    Compare that to another funeral I attended. This man was also “…highly
                    regarded in the secular community. But he was careful to number his days and
                    resources and offer them to God. People didn't speak about his success and
                    influence in the marketplace, but of his spiritual impact…how he pointed
                    them to Christ…his leadership and support of Christian ministries…his years
                    of church service…his mission trips around the country. His family spoke of
                    his love of Scripture…his love for them…his faithfulness as a husband and
                    father.” I remember wishing the whole church could be there to see what God
                    can do with somebody who offers every day of their life to Him. So, how
                    many people have you pointed to Christ? Where are you investing your time,
                    treasure and talent? God's gifts are never loans, they are deposits and He
                    expects a return. The only even thing in this world is the number of hours in a
                    day. The difference between winning and losing is how you use them.
                </Text>            
           
            </View>,
            prayerPoints: [
                "Father, empower me to live a meaningful life that points to You, in the name of Jesus.",
                "Father, teach me to apply my heart unto wisdom for the rest of my days, in the name of Jesus.",
                "Empower me to live my life doing Your will, in the name of Jesus.",
            ]
        },
        {
            day: 30,
            topic: "What's Your Motive for Giving?",
            wordStudy: "Matthew 6:1-4",
            confession: "And whatsoever ye do, do it heartily as unto the Lord (Colossians 3:23).",
            fullText: <View>
                <Text style={styles.text}>
                    God measures your giving in two ways: (a) <Italic>Your means:</Italic> how much do you
                    have? (b) <Italic>Your motives:</Italic> why are you giving? <Bold>“Take heed that you do not do
                    your charitable deeds before men, to be seen by them. Otherwise you
                    have no reward from your Father in Heaven…do not let your left hand
                    know what your right hand is doing, that your charitable deed may be in
                    secret; and your Father who sees in secret will Himself reward you
                    openly” (Matthew 6:1-4 NKJV).</Bold> Here are two stories that illustrate the
                    point Jesus was making.
                    <Br />
                    Story One: In 1977 a couple agreed to donate the three million needed to
                    build a new children's zoo. But problems arose because the 5 cm tall plaque
                    acknowledging their gift wasn't big enough. Plus, a couple who'd donated
                    half a million thirty years earlier to build the original zoo had a bigger plaque.
                    It was proposed that the names of the original donors be replaced by the
                    names of the new donors. When the park commission refused, the couple
                    withdrew their gift.
                    <Br />
                    Story Two: It's said that a man and his wife would sell, but refused to give
                    away the eggs their chickens laid. Even close relatives were told, “You may
                    have them if you pay for them.” As a result the man and his wife were
                    misrepresented as being greedy. Only after the wife died was the full story
                    revealed. All the profits from the sale of the eggs went to support two elderly
                    widows. Apparently the man and his wife were more concerned with how
                    God felt about their giving, than how people felt.
                </Text>            
           
            </View>,
            prayerPoints: [
                "Father, forgive me for every wrong motive of my giving over time, in the name of Jesus.",
                "Lord, help me with the right mindset and attitude towards giving, in the name of Jesus.",
                "Lord my Father, empower me from now on to give bountifully, in the name of Jesus.",
            ]
        },
        {
            day: 31,
            topic: "When God Shows Up",
            wordStudy: "Acts 16:25-26",
            confession: "Suddenly, God shall appear in my matter and I shall testify, in the name of Jesus.",
            fullText: <View>
                <Text style={styles.text}>
                    Sometimes God shows up dramatically: like an unexpected alert from the
                    bank, a door suddenly opening, or the Lord protecting you from a situation
                    that would otherwise destroy you (See Psalm 91:11.). Other times He shows
                    up quietly, replacing your anxiety with assurance, whispering, <Bold>“Fear not…I
                    am with you…” (Isaiah 41:10 NKJV).</Bold> And when those around you say,
                    “You're different today, what's changed?” you reply, “I don't know, I just have
                    peace about it.” That's because, <Bold>“…the Lord, whom you seek, will suddenly
                    come…” (Malachi 3:1 NKJV).</Bold>
                    <Br />
                    Here are two Bible stories which illustrate this: The first story
                    demonstrates how God intervenes in situations where you are misunderstood
                    and mistreated. <Bold>“After they had been severely flogged… About midnight
                    Paul and Silas were praying and singing hymns…Suddenly…all the
                    prison doors flew open…” (Acts 16:23-26 NIV).</Bold> Midnight prayers and
                    songs of praise help you see beyond the problem to God, the great problemsolver.
                    By taking the focus off yourself and putting it on Him, your whole
                    outlook begins to change.
                    <Br />
                    The second story demonstrates how God intervenes when we are
                    overwhelmed by the sheer size of the challenge. Faced with leading two
                    million Israelites into the Promised Land, Moses prays, “…Show me now
                    Your way…” <Bold>(Exodus 33:13 NKJV).</Bold> And God replies, <Bold>“Here is a place by
                    Me, I will put you in the cleft of the rock, and will cover you with My
                    hand…”(Exodus 33:21-23 NKJV).</Bold> When God tells you, “Here is a place by
                    Me,” get into it and stay there! Even when you can't see Him clearly, you'll feel
                    His presence, experience His goodness, and know that everything is going to
                    be OK.
                </Text>            
           
            </View>,
            prayerPoints: [
                "God of suddenly, on this matter (mention it)......, appear, in the name of Jesus.",
                "My testimony will manifest suddenly to the shame of the devil, in the name of Jesus.",
                "The God of suddenly will appear to heal me of all ill-health, in the name of Jesus.",
            ]
        },
    ],
    November: [
        {
            day: 1,
            topic: "Be Willing to Let God Mould You",
            wordStudy: "Jeremiah 18:1-11",
            confession: "The will of the Lord shall be done in my life and that of my family, now and in the future, in the name of Jesus (Jeremiah 18:4)..",
            fullText: <View>
                <Text style={styles.text}>
                    Henry Poppen, one of China's first missionaries, spent forty years telling its
                    people about the love of Jesus and how He died to take away their sin. One day,
                    after he had finished speaking, a man approached him and said, “We know this
                    Jesus! He's been here.” Dr Poppen explained how that wasn't possible because
                    Jesus had lived and died long ago in a country far from China. “Oh no,” the
                    man insisted, “He died here. I can even show you his grave.” He led Dr.
                    Poppen outside the city to a cemetery where an American was buried. There,
                    inscribed on a crumbling gravestone was the name of a medical doctor who
                    felt called by God to live and die among the people of this remote Chinese
                    village. And when its people heard Dr Poppen describe the attributes of
                    Jesus—His mercy, His love, His kindness, His willingness to forgive—they
                    remembered the missionary doctor.
                    <Br />
                    God will use you when you're willing to become <Bold>“clay in the potter's
                    hand.”</Bold> Clay has no aspirations; it's mouldable, pliable and completely subject
                    to the potter's will. Henry Blackaby says: “When God's assignment demands
                    humility, He finds a servant willing to be humbled. When it requires zeal, He
                    looks for someone He can fill with His Spirit. God uses holy vessels, so He
                    finds those who'll allow Him to remove their impurities. It's not a noble task
                    being clay. There's no glamour to it, nothing boast-worthy, except it's exactly
                    what God's looking for.”
                </Text>
            </View>,
            prayerPoints: [
                "Lord, mould my life and destiny after Your will, in the name of Jesus.",
                "As clay in the hands of the potter, make me pliable in Your hands, O Lord my Father, in the name of Jesus.",
                "I shall not become unmouldable for God to use, in the name of Jesus.",
            ]
        },
        {
            day: 2,
            topic: "Study Your Bible (1)",
            wordStudy: "Joshua 1:8",
            confession: "Open my eyes, that I may see wondrous things from Your law (Psalm 119:18).",
            fullText: <View>
                <Text style={styles.text}>
                    Let's consider some practical suggestions for getting more out of your Bible
                    study: <Italic>Schedule it</Italic>. If you don't, it won't happen. Learn to say no to
                    unimportant things. For many of us the biggest obstacle to studying the Bible
                    is television; the average person watches over four hours every day. Do the
                    maths: that's sixty-one days a year—two months! By the age of eighteen, the
                    average person has seen 200,000 acts of violence, including 16,000 murders.
                    And by the age of sixty-five they've spent about nine-and-a-half years
                    watching TV. By contrast, if you went to Sunday school regularly from birth
                    until the age of sixty-five, you'd only have had a total of four months of Bible
                    teaching. No wonder we struggle spiritually.
                    <Br />
                    Discipline yourself by setting aside a specific time each day to study your
                    Bible—and let nothing get in the way. And do it when you're at your best
                    physically, emotionally, and intellectually, not when you're distracted and
                    hurried. You know if you're “a day person” or a “night person,” so pick the
                    time when you're most alert. The spirit may be willing but the flesh is weak, so
                    if you don't want your study time to turn into “snore time,” don't try to study
                    when you're tired, or right after a big meal. Keep a notebook to jot down your
                    observations and keep track of what God is saying to you. Writing does three
                    things: it clarifies, reinforces and personalises. Also, ask yourself, “What can
                    I take away from this passage?” It will help to fix God's Word firmly in your
                    mind and jog your memory regarding what you're supposed to do about it.
                </Text>            
           
            </View>,
            prayerPoints: [
                "Father, help me to prioritize my time, in the name of Jesus.",
                "Father, grant me the grace to be faithful in my Quiet Time, in the name of Jesus.",
                "Deliver me, O Lord, from the spirit of prayerlessness, in the name of Jesus.",
            ]
        },
        {
            day: 3,
            topic: "Study Your Bible (2)",
            wordStudy: "1 Corinthians 2:14-16",
            confession: "My tongue shall speak of thy word: for thy commandments are righteousness (Psalm 119:172).",
            fullText: <View>
                <Text style={styles.text}>
                    Spend time in prayer before you study the Bible. Ask the Lord to cleanse you
                    from all known sin and fill you with the Holy Spirit so that you'll be in
                    fellowship with Him during your study time. Again, the purpose of Bible
                    study isn't to get new revelations and new rules; it's to build a relationship with
                    the Author of the Bible. The Psalmist said, <Bold>“How can a young man cleanse
                    his way? By taking heed [of] Your Word”(Psalm 119:9 NKJV)</Bold> Sin doesn't
                    cancel your relationship with Christ, but it hinders your fellowship with Him.
                    You have to be in fellowship with Him to understand and apply His Word. So
                    before you search the Scriptures, ask God to search your heart.
                    <Br />
                    Paul says, <Bold>“A person who isn't spiritual doesn't accept the teachings of
                    God's Spirit… He can't understand them because a person must be
                    spiritual to evaluate them. 'Who has known the mind of the Lord so that
                    he can teach Him?' However, we have the mind of Christ”(1 Corinthians
                    2:14-16 GWT).</Bold> It's possible to read the same portion of Scripture many times,
                    yet fail to see what God wants you to see until He “opens” your eyes. Once that
                    happens, your attitude toward Bible study will be transformed. Like a hungry
                    person at a table overflowing with good food, you'll relish God's Word and
                    your faith will grow. As Paul said: <Bold>“'No one has ever imagined what God
                    has prepared for those who love him.' But God has shown us these things
                    through the Spirit” (1 Corinthians 2:9-10 NCV).</Bold>
                </Text>            
           
            </View>,
            prayerPoints: [
                "Father, cleanse me from every form of unrighteousness, in the name of Jesus.",
                "Father, give me the spirit of wisdom and revelation in the knowledge of Your Word, in the name of Jesus.",
                "Lord Jesus, cause me to grow in the grace and knowledge of You.",
            ]
        },
        {
            day: 4,
            topic: "Facing Death without Fear",
            wordStudy: "1 Corinthians 15:19-21",
            confession: "I shall not die, but live, and declare the works of the LORD, in the name of Jesus (Psalm 118:17).",
            fullText: <View>
                <Text style={styles.text}>
                    Aristotle called death the thing to be feared most because “it appears to be the
                    end of everything.” Jean-Paul Sartre said that death “removes all meaning
                    from life.” Robert Green Ingersoll, one of America's most outspoken
                    agnostics, unable to offer any words of hope at his brother's funeral, said,
                    “Life is a narrow vale between the cold and barren peaks of two eternities. We
                    strive in vain to look beyond the heights.” The last words of French humanist
                    François Rabelais were: “I go to seek 'a Great Perhaps.'” In <Italic>Hamlet</Italic>,
                    Shakespeare describes the afterlife as: “The dread of something after death,
                    the undiscovered country from whose bourn no traveller returns.”
                    Clearly, unbelief isn't just a miserable way to live; it's a tragic way to die.
                    A comedian once quipped, “I intend to live forever…so far, so good.” But
                    what if death is different from how the philosophers thought of it? Not just a
                    curse, but a passageway? Instead of a crisis to be avoided, a corner to be
                    turned? What if the cemetery isn't the domain of the Grim Reaper, but the
                    dominion of the Soul-Keeper who'll someday soon announce, “O dwellers in
                    the dust, awake and sing for joy”? Paul writes: <Bold>“If in this life only we have
                    hope in Christ, we are of all men most pitiable. But now Christ is risen
                    from the dead, and has become the firstfruits of those who have fallen
                    asleep. For since by man came death, by Man also came the resurrection
                    of the dead” (1 Corinthians 15:19-21 NKJV).</Bold>
                    <Br />
                    Death isn't the “great perhaps.” No, your last day on earth will herald the
                    best of all your days!
                </Text>            
           
            </View>,
            prayerPoints: [
                "I reject every form of sickness, in the name of Jesus.",
                "I reject sudden death, in the name of Jesus.",
                "Enemies shall not prevail over me, in the name of Jesus.",
            ]
        },
        {
            day: 5,
            topic: "You Will Be Raised from the Dead",
            wordStudy: "Matthew 27:45-53",
            confession: "The gates of hell shall not prevail against me, in the name of Jesus.",
            fullText: <View>
                <Text style={styles.text}>
                    Jesus promised what no other religious leader could: that He'd return from the
                    dead, and also resurrect His followers from the grave. And He has already
                    made a good-faith deposit on that promise. When He died, <Bold>“Graves were
                    opened; and many bodies of the saints who had fallen asleep were
                    raised… they went into the holy city and appeared to many” (Matthew
                    27:52-53 NKJV).</Bold> And the day is fast approaching when Jesus will fulfil His
                    promise completely. <Bold>“In Christ all shall be made alive…each one in his
                    own order: Christ the firstfruits, afterwards those who are Christ's at His
                    coming” (1 Corinthians 15:22-23 NKJV).</Bold> Note the word “afterwards.”
                    The date of Jesus' return is classified information, but His resurrection
                    guarantees ours.
                    <Br />
                    Traditional Judaism was divided on this topic. The Sadducees said,
                    <Bold>“There is no resurrection” (Acts 23:8 NKJV).</Bold> They saw the grave as a oneway
                    trip to Sheol (hell) with no escape and no hope, while the Pharisees
                    envisioned a resurrection that was spiritual, not physical. The Greeks believed
                    the soul of the deceased was ferried across the River Styx and released into a
                    sunless afterlife of spirits, shades and shadows. Then Jesus, <Bold>“the firstfruits of
                    them that slept” (1 Corinthians 15:20 KJV),</Bold> entered this world of
                    superstition and darkness and turned on the light. The word “firstfruits” can
                    literally be translated “prototype.” Jesus was the first of the resurrected ones,
                    and the rest of us who follow will look just like Him. It's a message that dries
                    our tears, calms our fears and assures us that our best days are yet to come.
                </Text>            
           
            </View>,
            prayerPoints: [
                "Father, I receive grace to serve You to the end, in the name of Jesus.",
                "Let every good thing in my life that is dead begin to resurrect, in the name of Jesus.",
                "I shall fulfill my destiny gloriously, in the name of Jesus.",
            ]
        },
        {
            day: 6,
            topic: "Rejoice, He Is Coming for You",
            wordStudy: "John 14:2-3",
            confession: "Thank You, Lord, for the grace to reign with You, in the name of Jesus.",
            fullText: <View>
                <Text style={styles.text}>
                    Jesus said, <Bold>“…I am going to prepare a place for you…When everything is
                    ready, I will come and get you, so that you will always be with Me where I
                    am” (John 14:2-3 NLT).</Bold> This was a groom-to-bride promise. In Christ's day,
                    when a couple received permission from both their families to marry, the
                    groom would return to his father's home and build a house for his bride. And
                    by promising to do the same for us, Jesus elevated funerals to the same level
                    of hope as weddings. From His perspective, the trip to the cemetery and the
                    walk down the aisle warrant identical excitement. Both celebrate a new era, a
                    new name and a new home.
                    <Br />
                    Rejoice, your heavenly Bridegroom is coming to take you away on His
                    arm! Does that sound too good to be true? Check the record. If you can find
                    one time in all the 66 books in the Bible where God made a promise He didn't
                    keep, you'll have reason to doubt this one. Your final glimpse of life will
                    trigger your first glimpse of Jesus. Because Christ's tomb is empty—His
                    promise isn't. He made it 2000 years ago, and He's been working on your new
                    home ever since. Can you imagine what it'll be like? Living on a tiny, barren
                    island called Patmos, John the Revelator wrote, <Bold>“…There was no more sea”
                    (Revelation 21:1 NKJV).</Bold> Every morning when John arose the sea was all
                    around him; it was the barrier that separated him from his loved ones on the
                    mainland. Think about it: once in your heavenly home the separation will be
                    over.
                </Text>            
           
            </View>,
            prayerPoints: [
                "Lord, thank You for the grace to reign with You.",
                "The joy of the Lord is my strength, in the name of Jesus.",
                "Thank You for all the great promises of Your Second Coming and of eternal life forever with You.",
            ]
        },
        {
            day: 7,
            topic: "The Blessed Hope",
            wordStudy: "1 Corinthians 15:4-8",
            confession: "Uphold me according unto thy word, that I may live: and let me not be ashamed of my hope (Psalm 119:116).",
            fullText: <View>
                <Text style={styles.text}>
                    The blessed hope of the resurrection isn't like a lottery where only one person
                    out of millions hits the jackpot. And it's not a wishful sentiment offered at
                    gravesides to comfort the grieving. It's a hope built on certainty. Jesus said,
                    <Bold>“…Because I live, you will live also” (John 14:19 NKJV). Paul explained:
                    “…There is an order to this resurrection: Christ was raised as the first of
                    the harvest; then all who belong to Christ will be raised when He comes
                    back” (1 Corinthians 15:23 NLT).</Bold> Paul was writing to Corinthian Christians
                    who'd been schooled in the Greek philosophy of a shadowy afterlife.
                    Someone was trying to convince them that corpses couldn't be
                    raised—neither theirs, nor Christ's. And the apostle couldn't bear such a
                    thought.
                    <Br />
                    So with the brilliance of a great attorney in his closing argument, he
                    reviews the facts: <Bold>“…[Jesus] was raised from the dead on the third day…
                    He was seen by Peter… the Twelve… more than 500 of His followers…
                    Last of all… I also saw Him” (1 Corinthians 15:4-8 NLT).</Bold> How many
                    witnesses were there? A handful? No, hundreds! And they didn't see a
                    phantom or experience a sentiment. Graveside eulogies often include phrases
                    like: “She'll live on forever in our hearts.” This isn't what Jesus' followers
                    meant. They saw Him “in the flesh.” And if you've trusted Christ as your
                    Saviour, one day you, too, will see Him in the flesh. At death, your spirit will
                    go to be with Him, and at His return, your resurrected body will rise to meet
                    Him in the air. <Bold>“So shall we ever be with the Lord” (1 Thessalonians 4:17
                    KJV).</Bold> Awesome, absolutely awesome!
                </Text>            
           
            </View>,
            prayerPoints: [
                "Father, grant me the grace to be heavenly conscious, in the name of Jesus.",
                "Father, let the mind of Christ manifest and glorify You in my life, in the name of Jesus.",
                "I shall manifest the glory of God to the praise of His name, in the name",
            ]
        },
        {
            day: 8,
            topic: "Someone Is Always Watching You",
            wordStudy: "Matthew 5:14-16",
            confession: "Darkness will not cover the light of God in my life, in the name of Jesus.",
            fullText: <View>
                <Text style={styles.text}>
                    A reporter once said to Joe DiMaggio, the famous baseball player, “You
                    always seem to play with the same intensity. You run out every grounder and
                    race after every fly ball, even when the Yankees have a big lead in the pennant
                    race and there's nothing on the line. How come?” DiMaggio replied, “I
                    always remind myself that there might be someone in the stands who never
                    saw me play before.” That's the kind of unselfish mindset you must maintain
                    in order to influence others for good and for God. It takes energy and
                    intentionality, whether one-on-one or in a group, but it pays dividends.
                    <Br />
                    Nicodemus, a Jewish leader, was drawn to Christ because he'd listened
                    to His teachings and observed His works from afar. He may have come to
                    Jesus at night to avoid ridicule, but the fact is, he came because he couldn't
                    stay away! <Bold>(John 3:1-21).</Bold> The Bible says you're a <Bold>“…letter…known and
                    read by everybody” (2 Corinthians 3:2 NIV).</Bold> Today someone is watching
                    how you handle problems; how you treat your family and your employees;
                    how you act when the boss isn't around; how you respond to criticism or
                    temptation. Don't disappoint them. Jesus said: <Bold>“You are the light of the
                    world. A city that is set on a hill cannot be hidden. Nor do they light a
                    lamp and put it under a basket, but on a lampstand, and it gives light to
                    all who are in the house. Let your light so shine before men, that they
                    may see your good works and glorify your Father in Heaven” (Matthew
                    5:14-16 NKJV).</Bold>
                </Text>            
           
            </View>,
            prayerPoints: [
                "I shall arise and shine by reason of the Lord's glory upon me, in the name of Jesus.",
                "I shall possess my possessions, in the name of Jesus.",
                "The enemies shall not triumph over me, in the name of Jesus.",
            ]
        },
        {
            day: 9,
            topic: "Are You Flourishing? (1)",
            wordStudy: "Psalm 42:1-11",
            confession: "Confession: I shall be like a tree planted by the rivers of water, that bringeth forth his fruit in his season, in the name of Jesus (Psalm 1:3).",
            fullText: <View>
                <Text style={styles.text}>
                    Do you need motivation today? Take a look at the palm tree.
                </Text>

                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            <Italic>You can cut it, but you can't kill it!</Italic> The minerals and nutrients most trees
                            need to survive are found on the surface, just below the bark, so, when
                            you cut them, they die. But not the palm tree. Its life comes from within,
                            so it flourishes, even under attack. Listen: <Bold>“We have this treasure in
                            earthen vessels…” (2 Corinthians 4:7 NAS).</Bold> How wonderful; even
                            though the earthen vessel without may be cut and wounded, the treasure
                            within is secure, beyond the enemy's reach!
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            <Italic>It will bend, but it won't break.</Italic> Tropical winds can blow most trees away,
                            but not the palm tree. The stronger the wind, the further it
                            bends—sometimes all the way to the ground. Yet when the storm ceases,
                            it straightens up again and is actually stronger in the place where it bent.
                            What a picture! We were made to bend, but not break, because God
                            strengthens us <Bold>“with His glorious power so [we] will have all the
                            patience and endurance [we] need” (Colossians 1:11 NLT).</Bold>
                        </Text>
                    },
                    {
                        serial: 3,
                        body: <Text>
                            <Italic>Its depth always exceeds its height!</Italic> While the roots of the average plant
                            go only a few feet under the ground, the palm tree's roots can go down
                            hundreds of metres in search of water. David said, <Bold>“As the deer pants for
                            streams of water, so my soul pants for you...” (Psalm 42:1 NIV).</Bold> Go
                            deep, work on your own connection to God and you'll never be uprooted
                            or barren or blown away!
                        </Text>
                    },
                ]} />     
           
            </View>,
            prayerPoints: [
                "I shall not cease to bear fruit for God's glory, in the name of Jesus.",
                "The Lord shall prosper me in all my endeavoursand I shall not lack any good thing, in the name of Jesus.",
                "I shall be a blessing to my generation, in the name of Jesus.",
            ]
        },
        {
            day: 10,
            topic: "Are You Flourishing? (2)",
            wordStudy: "Psalm1:1-3",
            confession: "I shall flourish in the courts of our God, in the name of Jesus (Psalm 92:13).",
            fullText: <View>
                <Text style={styles.text}>
                    Inside you there's a battle between your flourishing self—the person you
                    were created to be—and your languishing self. “What's that?” you ask. Your
                    languishing self feels uneasy and discontent. You're drawn to bad habits like
                    mindlessly watching TV, drinking too much, misusing sex, excessive
                    spending—things designed to temporarily anaesthetise pain. Your thoughts
                    automatically drift in the direction of fear and anger. Learning doesn't feel
                    worthwhile. You think about yourself most of the time. Whereas flourishing
                    [thriving, blossoming, and prospering] takes place:
                </Text>       

                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            <Italic>In your spirit.</Italic> You sense you're beginning to receive ideas and energy
                            from an outside source. And you are. You're being empowered by God's
                            Spirit. We talk about being inspired, which literally means “Godbreathed.”
                            God breathes into you; you come alive and feel like you've a
                            purpose for living.
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            <Italic>In your mind.</Italic> Your thoughts are marked by joy and peace. You have a
                            desire to love and to learn. You're literally being transformed by <Bold>“the
                            renewing of your mind…” (Romans 12:2 NIV).</Bold>
                        </Text>
                    },
                    {
                        serial: 3,
                        body: <Text>
                            <Italic>In your time.</Italic> You wake each day with a sense of excitement, and you
                            realise you're never too young to flourish. Mozart was composing
                            brilliant music when he was five. Paul told Timothy, <Bold>“Don't let anyone
                            look down on you because you are young…” (1 Timothy 4:12 NIV).</Bold>
                            You also realise you're never too old to flourish. Grandma Moses was 69
                            when she took up painting, and artist Marc Chagall did some of his best
                            work in his nineties. It's humbling to acknowledge you can't be anything
                            you want. But once you accept that and seek to maximise what God
                            created you to be, you start flourishing.
                        </Text>
                    },
                ]} />     
           
            </View>,
            prayerPoints: [
                "Father, transform me by the renewing of my mind, in the name of Jesus.",
                "Renew my youth like the eagle and sanctify me wholly in my spirit, soul and body, in the name of Jesus.",
                "Holy Spirit, help me fulfill God's purpose for my life, in the name of Jesus.",
            ]
        },
        {
            day: 11,
            topic: "Are You Flourishing? (3)",
            wordStudy: "John 12:24-25",
            confession: "The righteous shall flourish like the palm tree: he shall grow like a cedar in Lebanon (Psalm 92:12).",
            fullText: <View>
                <Text style={styles.text}>
                    Here are two more areas in which God created you to ƒPourish:
                </Text>       

                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            <Italic>Your relationships.</Italic> Relationally, your languishing self is often troubled.
                            You're undisciplined in what you say, sometimes reverting to sarcasm,
                            gossip and flattery. You isolate. You dominate. You attack. You withdraw.
                            Whereas your flourishing self seeks to bless others. They energise you.
                            You're able to disclose your thoughts and feelings in a way that invites
                            openness in them. You're quick to admit your own mistakes and to freely
                            forgive others.
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            <Italic>Your experiences.</Italic> God grows you because He wants to use you in His
                            plans to redeem His world: that's why you find Him changing your
                            experiences. Your flourishing self has a desire to contribute. You live with
                            a sense of calling. Indeed, your inner longing to become all you were
                            meant to be is a tiny echo of God's longing to begin the new creation. The
                            rabbis spoke of this as <Italic>tikkun olam</Italic>—“to repair the world.” Focused on
                            yourself, your life is as small as a grain of wheat. Given to God, however,
                            it's as if that grain is planted in rich soil, growing into part of a much
                            bigger project. Jesus said: <Bold>“Unless a kernel of wheat falls to the ground
                            and dies, it remains only a single seed. But if it dies, it produces many
                            seeds. The man who loves his life will lose it, while the man who hates
                            his life in this world will keep it for eternal life” (John 12:24-25 NIV).</Bold>
                        </Text>
                    },
                ]} />     
           
            </View>,
            prayerPoints: [
                "Father Lord, fulfil Your plan and purpose for my life, in the name of Jesus.",
                "My life and destiny in God shall not be wasted, in the name of Jesus.",
                "Father, remove every hindrance to my progress, in the name of Jesus.",
            ]
        },
        {
            day: 12,
            topic: "Your Answer Is in the Bible",
            wordStudy: "Psalm 19:7-13",
            confession: "Hear, O LORD, when I cry with my voice: have mercy also upon me, and answer me (Psalm 27:7).",
            fullText: <View>
                <Text style={styles.text}>
                    Worldly success can leave you empty, but here's something that won't: <Bold>“The
                    instructions of the LORD are perfect, reviving the soul” (v. 7a NLT).</Bold> Have
                    your plans fallen apart? Have you made a mess of your life? Do you need
                    direction? Read this: <Bold>“The decrees of the LORD are trustworthy, making
                    wise the simple” (v. 7b).</Bold> Have the things you thought would bring you
                    happiness, ended up bringing you misery? Read this: <Bold>“The commandments
                    of the LORD are right, bringing joy to the heart” (v. 8a).</Bold> Are you
                    directionless, wondering where to go and what to do with your life? Read
                    this: <Bold>“The commands of the LORD are clear, giving insight for living” (v.
                    8b).</Bold>
                    <Br />
                    The answers you're seeking are in your Bible. David was a brilliant
                    soldier, a popular king, and a man with access to all the riches anyone could
                    desire. But he discovered these things don't bring lasting joy. What he found
                    instead is that: <Bold>'…The laws of the LORD are true… They are more
                    desirable than gold, even the finest gold. They are sweeter than honey,
                    even honey dripping from the comb. They are a warning to your servant,
                    a great reward to those who obey them” (vs. 9-11).</Bold> What was David's
                    problem? <Bold>“How can I know all the sins lurking in my heart? Cleanse me
                    from these hidden faults. Keep your servant from deliberate sins! Don't
                    let them control me. Then I will be free of guilt…” (vv. 12-13).</Bold>
                    What was David's solution? Reading God's Word daily, feeding his soul
                    on it, and walking according to its precepts.
                </Text>       
            </View>,
            prayerPoints: [
                "This day, Father Lord, let there be a solution to my problems, in the name of Jesus.",
                "Remove every hindrance to my prayers, in the name of Jesus.",
                "Answer me every time I call Your name Lord, in the name of Jesus.",
            ]
        },
        {
            day: 13,
            topic: "The Person You Were Made to Be",
            wordStudy: "Ephesians 2:8-10",
            confession: "I am wonderfully and fearfully made, God's workmanship created in Christ Jesus for good works.",
            fullText: <View>
                <Text style={styles.text}>
                    God created you, and He knows what you were intended to be. He has <Bold>“good
                    works”</Bold> for you to do, but they're not necessarily the kind of “to-do” things we
                    put on a list for our spouses or employees. They're signposts to your true self.
                    Your spiritual life isn't limited to certain devotional activities. It is about being
                    empowered by God to become the person He envisioned when He created
                    you. But just as nobody becomes happy because their goal is to be happy,
                    becoming the person God intended you to be won't happen if your focus is
                    always on yourself. Flourishing is tied to a nobler vision; it doesn't happen by
                    “looking out for number one.” People who flourish bring blessing to others,
                    and they do it in unexpected and humble circumstances.
                    <Br />
                    Every once in a while you catch a glimpse of the person you were made to
                    be. You say something inspirational. You express compassion. You forgive an
                    old hurt. You give sacrificially. You refrain from saying something you'd
                    normally blurt out. And as you do, you glimpse for a moment the reason God
                    made you. Only He knows your full potential, and He's always guiding you
                    toward the best version of yourself. He uses many tools, He's never in a
                    hurry—and that can be frustrating. But even in our frustration He's at work
                    producing patience. He never gets discouraged by how long it takes, and He
                    delights every time you grow. Only God knows what his intended
                    transformation of you is, and He's more committed to it than you are.
                </Text>       
            </View>,
            prayerPoints: [
                "Father, make me a blessing to my generation, in the name of Jesus.",
                "I shall not lose focus in life and ministry, in the name of Jesus .",
                "Holy Spirit, help me to grow in the grace and knowledge of the Lord Jesus Christ, in the name of Jesus.",
            ]
        },
        {
            day: 14,
            topic: "Hurt by Christians (1)",
            wordStudy: "Psalm 32:3-4",
            confession: "He [The LORD] heals the brokenhearted and binds up their wounds (Psalm 147:3).",
            fullText: <View>
                <Text style={styles.text}>
                    Have you been hurt by a Christian who chose to condemn you instead of
                    showing compassion and helping you? Or by someone who neglected you
                    when they should have sought you out, restored you spiritually and returned
                    you to your rightful place in the family of God? Most people who've been hurt
                    by other Christians could easily convince a jury that it should never have
                    happened. And the truth is, it shouldn't. But it did—and reliving it won't
                    change things. But it will change you—and not for the better. Stop and think:
                    if you were mugged and taken to the hospital, you wouldn't spend all your
                    time obsessing about the person who beat you up. No, your main objective
                    would be to recover as quickly as possible and move on. Ironically, with
                    physical wounds we seek help immediately, but with emotional ones we're
                    inclined to focus on the problem instead of the solution. So what are you
                    going to do?
                    <Br />
                    Here are your options:
                </Text>      
                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            <Italic>Rehearse it.</Italic> By constantly talking to others about what happened, you
                            empower your pain and keep it alive.
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            <Italic>Repress it.</Italic> David did that: <Bold>“When I kept silent…my strength was
                            sapped as in the heat of summer” (Psalm 32:3-4 NIV).</Bold>
                        </Text>
                    },
                    {
                        serial: 3,
                        body: <Text>
                            <Italic>Process it.</Italic> That means being willing to uncover the areas where you
                            were wounded and opening yourself to receive God's grace. That's when
                            you discover: <Bold>“He heals the brokenhearted and binds up their
                            wounds” (Psalm 147:3 NKJV).</Bold>
                        </Text>
                    },
                    {
                        serial: 4,
                        body: <Text>
                            <Italic>Share it.</Italic> The Bible says, <Bold>“Confess your sins to each other and pray for
                            each other so that you may be healed…” (James 5:16 NIV).</Bold> If you're
                            wise you'll choose options three and four.
                        </Text>
                    },
                ]} /> 
            </View>,
            prayerPoints: [
                "I receive grace to forgive every hurt I have received, in the name of Jesus.",
                "Every stronghold of bitterness and unforgiveness in my life, be dismantled today, in the name of Jesus.",
                "Heal my broken heart and bind up my wounds, O Lord, in the name of Jesus.",
            ]
        },
        {
            day: 15,
            topic: "Hurt by Christians (2)",
            wordStudy: "John 5:3-9",
            confession: "He [The LORD] heals the brokenhearted and binds up their wounds (Psalm 147:3).",
            fullText: <View>
                <Text style={styles.text}>
                    Getting hurt in life is inevitable; staying hurt is a choice. You can decide to
                    remain a victim by dwelling on how things should have been, or use the
                    experience to grow stronger and wiser. At the Pool of Bethesda Jesus met a
                    man who'd been lying paralysed on a mat for 38 years. When Jesus discovered
                    how long he'd been there, He asked, 'Do you want to get well?' After 38 years,
                    the chances are this man saw his handicap as part of his identity. He'd been
                    incapacitated for so long he thought like a victim: <Bold>“I have no one to help me
                    into the pool…” (John 5:7 NIV).</Bold> Translation: 'Nobody cares.' But Jesus did,
                    and He commanded him to get up and walk. Now this man simply obeyed
                    Jesus' instructions, he didn't argue the point or fall back on traditions or
                    superstitions, he just obeyed and was healed on the spot.
                    The church is made up of flawed human beings who sometimes speak
                    without thinking and hurt others. But that's no reason for leaving the church
                    because we're all at different points along the journey of faith, so we need to be
                    gracious toward each other. <Bold>“…You're part of the body of Christ…”
                    (Colossians 3:15 CEV).</Bold> Apart from it you've no function, no food supply, and
                    no fulfilment, so you begin to wither spiritually. Walk through the process of
                    asking God for healing, trust in His Word and be patient with those walking the
                    journey with you.
                </Text>
            </View>,
            prayerPoints: [
                "Father, heal me of all my hurts and bad experiences, in the name of Jesus.",
                "Comfort and strengthen my heart from all hurts, in the name of Jesus.",
                "Let my life receive healing, in the name of Jesus.",
            ]
        },
        {
            day: 16,
            topic: "Generosity Pays Off",
            wordStudy: "Luke 6:38",
            confession: "He who is generous will be blessed… (Proverbs 22:9 NAS).",
            fullText: <View>
                <Text style={styles.text}>
                    Dr William DeVries, the surgeon who pioneered the artificial heart, is the
                    kind of doctor who shows up at the hospital on Sunday, just to cheer up
                    discouraged patients. He even changes dressings and, if a patient wants him
                    to stick around and talk, he always does. His friends say he's “an old shoe”
                    who fits in wherever he goes. He wears cowboy boots with his surgical garb,
                    and repairs hearts to the music of Vivaldi. “He's always got a smile lurking,”
                    says friend Dr Robert Goodin, “And he's always looking for a way to let it
                    out.” DeVries believes that “arriving” is not a place where others serve you,
                    but where you get to serve them.
                    <Br />
                    Chuck Swindoll writes, “We occupy common space, but we no longer
                    have common interests. It's as if we're on an elevator with rules like 'No
                    talking or smiling or eye-contact allowed without written consent of the
                    management.' We're losing touch with one another! The motivation to help,
                    to encourage, yes, to serve our fellow man is waning. Yet it's these things that
                    form the essentials of a happy, fulfilled life. That's what Jesus meant when He
                    said, <Bold>“The greatest among you will be your servant” (Matthew 23:11
                    NIV).</Bold> Everything God gives you is first a gift to enjoy, then a seed to sow. Got
                    a good education? Leadership ability? More money than you need? You've
                    got seeds—sow them!”
                    <Br />
                    Read God's promises regarding generosity, then start giving to others
                    what He's given to you. That's the way to find happiness!
                </Text>
            </View>,
            prayerPoints: [
                "Father, help me to affect my generation with a generous heart, in the name of Jesus.",
                "Make me a generous steward of Your manifold grace, in the name of Jesus.",
                "I will fulfill my destiny, in the name of Jesus.",
            ]
        },
        {
            day: 17,
            topic: "Practise Being Unselfish (1)",
            wordStudy: "Philippians 2:4-5",
            confession: "I have the mind of Christ and hold the thoughts (feelings and purposes) of His heart (1 Corinthians 2:16b AMP).",
            fullText: <View>
                <Text style={styles.text}>
                    <Bold>“Don't look out only for your own interests, but take an interest in others,
                    too” (Philippians 2:4 NLT).</Bold> Spiritual maturity is the ability to see and act in
                    the best interests of others. Immature people have difficulty seeing things
                    from someone else's point of view. They rarely concern themselves with
                    what's best for the other person. In many ways they're like children. In
                    <Italic>Property Law as Viewed by a Toddler,</Italic> Michael V. Hernandez describes the
                    world from a typical child's viewpoint: (1) If I like it, it's mine. (2) If it's in my
                    hand, it's mine. (3) If I can take it from you, it's mine. (4) If I had it a little while
                    ago, it's mine. (5) If it's mine, it must never appear to be yours in any way. (6) If
                    I'm doing or building something, all the pieces are mine. (7) If it looks like
                    mine, it's mine. (8) If I saw it first, it's mine. (9) If I can see it, it's mine. (10) If I
                    think it's mine, it's mine.
                    <Br />
                    Unfortunately, maturity doesn't always come with age; sometimes age
                    comes alone. You must fight your inherent selfish attitude, and that can be a
                    lifelong battle. But it's an important one, because if you don't win you'll end up
                    focused on your own agenda and overlook other people. Unless somebody's
                    important to your cause or your interests, they won't get your time or attention.
                    The Bible says, <Bold>“You must have the same attitude that Christ Jesus had
                    (Philippians 2:5),”</Bold> and everything He did, He did for others. It comes down to
                    this: if you're serious about following in His footsteps, practise being
                    unselfish.
                </Text>
            </View>,
            prayerPoints: [
                "I reject the spirit of selfishness in my life, in the name of Jesus.",
                "I receive grace to walk in the mind of Christ and to hold the thoughts (feelings and purposes) of His heart, in the name of Jesus.",
                "Let the name of the Lord be glorified in my life, in the name of Jesus.",
            ]
        },
        {
            day: 18,
            topic: "Practise Being Unselfish (2)",
            wordStudy: "1 Corinthians 10:23-24",
            confession: "Let each of you look out not only for his own interests, but also for the interests of others (Philippians 2:4 NKJV).",
            fullText: <View>
                <Text style={styles.text}>
                    Great leaders often have great egos, and therein lies great danger. In <Italic>The
                    Empowered Communicator,</Italic> Calvin Miller uses the form of a letter to
                    describe this problem and the negative impact it has: “Dear speaker, your
                    ego has become a wall between yourself and me. You're not really concerned
                    about me, are you? You're mostly concerned about whether or not this speech
                    is really working…about whether or not you're doing a good job. You're
                    really afraid that I will not applaud, aren't you? You're afraid that I won't
                    laugh at your jokes or cry over your emotional anecdotes. You're so caught
                    up in the issue of how I'm going to receive your speech, you haven't thought
                    much about me at all. I might have loved you, but you're so caught up in selflove
                    that mine is really unnecessary. If I don't give you my attention it's
                    because I feel so unnecessary here.
                    <Br />
                    “When I see you at the microphone, I see Narcissus at his mirror… Is
                    your tie straight? Is your hair straight? Is your deportment impeccable? Is
                    your phraseology perfect? You seem in control of everything, but your
                    audience. You see everything so well, but us. But this blindness to us, I'm
                    afraid, has made us deaf to you. We must go now. Sorry. Call us sometime
                    later. We'll come back to you when you're real enough to see us…after your
                    dreams have been shattered…after your heart has been broken…after your
                    arrogance has been wrecked with despair. Then there will be room for all of
                    us in your world. Then you won't care if we applaud your brilliance. You'll be
                    one of us.”
                </Text>
            </View>,
            prayerPoints: [
                "I receive the spirit of humility, in the name of Jesus.",
                "I reject every spirit of pride in my life, in the name of Jesus.",
                "Let the help of God locate me in my desire to be unselfish, in the name of Jesus.",
            ]
        },
        {
            day: 19,
            topic: "Practise Being Unselfish (3)",
            wordStudy: "Philippians 2:1-11",
            confession: "…Love does not seek its own… (1 Corinthians 13:4 NKJV).",
            fullText: <View>
                <Text style={styles.text}>
                    John Craig says, “No matter how much work a man can do, no matter how
                    engaging his personality may be, he will not advance far if he cannot work
                    through others.” That requires you to see value in other people. This truth is
                    understood worldwide by successful people from every walk of life. At an
                    international meeting of company executives, an American business person
                    asked an executive from Japan what he regarded as the most important
                    language for world trade. The American thought the answer would be English.
                    But the Japanese executive, who had a more holistic understanding of
                    business, replied, “My customers' language.” Having a good product or
                    service isn't enough. Becoming an expert isn't enough. Knowing your product
                    but not your customers just means you'll have something to sell and no one to
                    buy.
                    <Br />
                    Furthermore, the value you place on people must be genuine. Leadership
                    coach Bridget Haymond writes, “You can talk until you're blue in the face, but
                    people know in their gut if you really care about them.” If you want to connect
                    with others you have to get over yourself, change your focus from inward to
                    outward, away from yourself and onto them. And the great thing is, you can do
                    it. Anyone can. All it takes is the will to change, the determination to follow
                    through and a handful of skills anybody can learn. The motivation to learn can
                    be found in these words from the apostle, Paul: <Bold>“Don't look out only for your
                    own interests, but take an interest in others, too” (Philippians 2:4 NLT).</Bold>
                    When you look for opportunities to invest in others, you'll find them.
                </Text>
            </View>,
            prayerPoints: [
                "Father, help me to honour, having respect for others, in the name of Jesus.",
                "I shall not fail in life and ministry by focussing only on myself and not others, in the name of Jesus.",
                "Every hindrance to my greatness, be removed today, in the name of Jesus.",
            ]
        },
        {
            day: 20,
            topic: "Practise Being Unselfish (4)",
            wordStudy: "Psalm 142:1-7",
            confession: "I will cry unto the Lord and he will hear my voice in Jesus name (Psalm 142).",
            fullText: <View>
                <Text style={styles.text}>
                    People you seek to influence in life always ask themselves: Do you care
                    about me? Think about the best experiences you've had with people in your
                    own life. What do they all have in common? They genuinely cared about
                    you, right? And what's wonderful is, you can broaden your ability to care
                    about others outside your social circle. Regardless of your profession, when
                    you help people, you make your life and theirs better. Let's listen to some
                    observations by successful people from various backgrounds.
                    <Br />
                    Business: “You can't make the other fellow feel important in your
                    presence if you secretly feel…he's a nobody.” (Les Giblin, former national
                    salesman of the year and popular speaker) Politics: “If you would win a man
                    to your cause, you must first convince him…you are his sincere friend.”
                    (President Abraham Lincoln) Entertainment: “Some singers want the
                    audience to love them. I love the audience.” (Luciano Pavarotti, legendary
                    Italian tenor) Ministry: “The 'show business,'…incorporated
                    into…Christian work today, has caused us…to think that we have to do
                    exceptional things for God; we have not. We have to be exceptional in
                    ordinary things.” (Oswald Chambers, evangelist)
                    <Br />
                    The Psalmist wrote, <Bold>“No one cares for my soul” (Psalm 142:4 NAS).</Bold>
                    And deep down some of the people you deal with every day feel that way,
                    too. Whether you're trying to share your faith, do business with them, make
                    friends, or help them in a particular area, you must prove to them that you
                    truly care. It takes time, effort, and even sacrifice, but if you're serious about
                    connecting with others, you'll do it. The chances are somebody did it for you,
                    and it helped determine the person you are today. So do it for others!
                </Text>
            </View>,
            prayerPoints: [
                "Father, take me to a higher ground of selflessness and unselfishness, in the name of Jesus.",
                "Increase and strengthen my faith, in the name of Jesus.",
                "Father, make me a positive influence to others for Your glory, in the name of Jesus.",
            ]
        },
        {
            day: 21,
            topic: "Practise Being Unselfish (5)",
            wordStudy: "1 Corinthians 10:23-33",
            confession: "Whatever you do, do all to the glory of God (1 Corinthians 10:31).",
            fullText: <View>
                <Text style={styles.text}>
                    Here are three questions people often ask themselves when you're talking to
                    them:
                </Text>
                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            <Italic>Do you really care about me?</Italic> Dr. Calvin Miller put it like this: “When
                            people listen to others speak, sometimes they're silently thinking, 'I am
                            loneliness waiting for a friend. I am weeping in want of laughter. I am a
                            sigh in search of consolation. I am a wound in search of healing. If you
                            want to unlock my attention, you have but to convince me you want to be
                            my friend.”'
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            <Italic>Can you really help me?</Italic> Successful people bear in mind that others are
                            continually asking themselves that question. And one way you can
                            answer it is by focusing on the benefits you have to offer. Let's face it,
                            people are bombarded every day with the information on the features of
                            this product and that gadget. So eventually they tune out.
                        </Text>
                    },
                    {
                        serial: 3,
                        body: <Text>
                            <Italic>Can I really trust you?</Italic> William Arthur Ward wrote, “Blessed is he who
                            has learned to admire but not envy, to follow but not imitate, to praise but
                            not flatter, and to lead but not manipulate.” Your charisma and ability
                            may get you to the top, but only your character and commitment will
                            keep you there. Trust is built on telling the truth and following through on
                            your commitments. People take action for their own reasons, not yours.
                            And what we learn about them always results in a greater reward than
                            what we tell them about ourselves. Whether they're buying a car,
                            choosing a mate, or listening to a sermon, deep down they want to know,
                            “Can I trust this person?” Well, …can they?
                        </Text>
                    },
                ]} />
            </View>,
            prayerPoints: [
                "Father Lord, I receive grace by Your Spirit to be sensitive to others' needs, in the name of Jesus.",
                "I shall not be self-centered, in the name of Jesus.",
                "Father, make me a trusted companion, colleague and leader, in the name of Jesus.",
            ]
        },
        {
            day: 22,
            topic: "You Can Do It!",
            wordStudy: "Joshua 14:6-15",
            confession: "I can do all things through Christ who strengthens me (Philippians 4:13).",
            fullText: <View>
                <Text style={styles.text}>
                    After seeing the giants in the Promised Land, ten of Israel's twelve spies came
                    back and said, “It can't be done.” But the other two, Joshua and Caleb, said,
                    “It can.” But because of Israel's unbelief, Caleb had been forced to spend
                    forty more years wandering through the wilderness. And by the time the
                    Israelites crossed the Jordan River he was almost eighty years old. Then
                    another seven years passed before the various tribes of Israel were assigned
                    land to occupy. Here's how Caleb described it all, years later: <Bold>“I was forty
                    years old when Moses…sent me…to explore the land. And I brought him
                    back a report according to my convictions, but my fellow Israelites who
                    went up with me made the hearts of the people melt in fear. I, however,
                    followed the Lord my God wholeheartedly” (Joshua 14:7-8 NIV).</Bold>
                    <Br />
                    If you've a negative attitude when you are 40, there's a good chance you'll
                    have one when you are 85. Actually, there's a good chance you won't even
                    make it to 85! Psychologist Martin Seligman studied several hundred people
                    in a religious community; he divided them into quartiles, ranging from the
                    most to the least optimistic. 90% of the optimists were alive at 85, while just
                    34% of the naysayers made it to the same age. Twelve spies went out, but only
                    Joshua and Caleb had the faith to say, “We can do it.” And over 45 years later
                    Caleb was as feisty as ever! Want to guess what happened to the other ten
                    spies? They died. None of them made it to Caleb's age. It's as simple as this:
                    faith and optimism can add years to your life.
                </Text>
            </View>,
            prayerPoints: [
                "I shall make it to the end, in the name of Jesus.",
                "I shall not be discouraged, in the name of Jesus.",
                "I reject every spirit of fear and unbelief, in the name of Jesus.",
            ]
        },
        {
            day: 23,
            topic: "The God of All Comfort (1)",
            wordStudy: "2 Corinthians 1:3-4",
            confession: "Yea, though I walk through the valley of the shadow of death, I will fear no evil: for thou art with me; thy rod and thy staff they comfort me (Psalm 23:4).",
            fullText: <View>
                <Text style={styles.text}>
                    Of all the names given to God in Scripture, none is more consoling than <Bold>“the
                    God of all comfort, who comforts us in all our troubles” (2 Corinthians
                    1:3-4 NIV).</Bold> Notice the words <Bold>“all comfort.”</Bold> That means there are no
                    exceptions and no limitations to God's comfort, no matter how dreadful your
                    circumstances. Comfort is God's name and nature; it's who He is and what He
                    naturally wants to do! You can't get close to Him and not be comforted. Lack of
                    comfort results from disconnecting from God—usually the result of a lack of
                    faith. Comfort comes by faith. You can know intellectually that He's the “God
                    of all comfort,” but not feel comforted because of your doubts. Jesus instructed
                    the blind man who came for healing, <Bold>“According to your faith will it be done
                    to you” (Matthew 9:29 NIV).</Bold> Knowing needs the addition of believing before
                    you'll experience comfort. Feeling follows faith, not vice versa!
                    <Br />
                    Suppose you're taking your first ocean cruise and have some concern
                    about safety. The captain assures you he's a qualified veteran and the ship's the
                    latest version available, equipped with modern communications and safety
                    equipment. If, like too many Christians, you refuse to believe Him until you
                    first feel the comfort, you'll probably disembark or spend the entire voyage
                    unnecessarily sick with worry. But if you make up your mind to believe the
                    captain's word, you'll soon feel the lessening of anxiety and the presence of
                    comfort! Today, decide to trust the Captain of our Salvation (Hebrews 2:10)
                    who has promised to <Bold>“comfort us in all our troubles,”</Bold> and you'll experience
                    the comfort He provides.
                </Text>
            </View>,
            prayerPoints: [
                "Father, deliver me from all evils, in the name of Jesus.",
                "My trust and confidence in You shall not be shaken as You uphold me with Your mighty hand, in the name of Jesus.",
                "I shall not lack for the comfort of the Lord, in the name of Jesus.",
            ]
        },
        {
            day: 24,
            topic: "The God of All Comfort (2)",
            wordStudy: "Isaiah 49:15-16",
            confession: "As one whom his mother comforts, so I will comfort you… (Isaiah 66:13a).",
            fullText: <View>
                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            <Italic>God's comfort is real.</Italic> Do you recall being sad or afraid as a child? And
                            do you remember feeling the comfort of your mother's presence? God's
                            comfort is even more real than your mother's was then. <Bold>“As one whom
                            his mother comforts, so I will comfort you.”</Bold> God's more concerned
                            about you now than your mother was in your childhood. <Bold>“Can a mother
                            forget the baby at her breast…? Though she may forget, I will not
                            forget you! See, I have engraved you on the palms of My hands…”
                            (Isaiah 49:15-16 NIV).</Bold> You can always count on God when you need
                            comfort!
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            <Italic>The Comforter lives in you.</Italic> When we're suffering, God seems distant
                            and inaccessible. But He's not. Jesus resolved that concern for His
                            anxious disciples—then and now. <Bold>“…I will pray [to] the Father, and
                            He shall give you another Comforter, that He may abide with you
                            forever” (John 14:16 KJV).</Bold> The Holy Spirit, who forever abides in
                            you, is closer to you than the air you breathe. He's equipped, willing, and
                            able to comfort you. He will sometimes do it by reminding you of the
                            verse from a hymn, a line from a poem, a sermon you heard, etc. When
                            He does, believe it. Say to yourself repeatedly: “The God of all comfort
                            lives permanently in me!”
                        </Text>
                    },
                    {
                        serial: 3,
                        body: <Text>
                            Scripture brings comfort. The apostle Paul reminds us: <Bold>“…That we
                            through patience and comfort of the Scriptures might have hope”
                            (Romans 15:4 KJV).</Bold> When you're down, you may not feel like reading
                            the Bible. Do it anyway. It'll bring you the comfort you long for.
                        </Text>
                    },
                ]} />
            </View>,
            prayerPoints: [
                "Lord Holy Spirit, You are my Comforter, honour Your name in my life, in the name of Jesus.",
                "The presence of the Lord shall not depart from me, in the name of Jesus.",
                "Thank You, Lord, for Your covenant of everlasting faithfulness with me: You will never leave nor forsake me. Amen.",
            ]
        },
        {
            day: 25,
            topic: "Enjoy Your Old Age",
            wordStudy: "Psalm 92:12-14",
            confession: "The righteous shall flourish like a palm tree, he shall grow like a cedar in Lebanon… They shall still bear fruit in old age; they shall be fresh and flourishing (Psalm 92:12, 14).",
            fullText: <View>
                <Text style={styles.text}>
                    Age is just a date on a calendar; attitude is what counts. You can be old at 29
                    and young at 92. Larry King once interviewed Ty Cobb, one of the all-time
                    great baseball players. He asked Cobb, then 70 years old, “What do you think
                    you'd hit if you were playing these days?” Cobb, a lifetime .366 hitter (still the
                    record) replied, “About .290, maybe .300.” King asked, “Is that because of
                    travel, the night games, the artificial turf and all the new pitches like the
                    slider?” Cobb responded, “No. It's because I'm 70!”
                    <Br single />
                    Here are three great benefits to having lived longer:
                </Text>
                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            <Italic>You should be more tolerant.</Italic> Having fallen into many of life's potholes
                            yourself, you should be quicker to extend a helping hand when others fall
                            into them. Having survived defeats and lived to fight another day, you're
                            qualified to offer strength and hope to those who struggle.
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            <Italic>You should be more humble.</Italic> A man who'd just celebrated his 50th
                            wedding anniversary said, “A man is always as young as he feels, but
                            seldom as important.” Realising that the world doesn't stop at your
                            command or cater to your whims, you become more realistic. And in the
                            process you find peace.
                        </Text>
                    },
                    {
                        serial: 3,
                        body: <Text>
                            <Italic>You should value time more.</Italic> Margaret Deland said, “As soon as you feel
                            too old to do a thing, do it.” Start by asking yourself, “If not me, who? If
                            not now, when?” Here's a promise from Scripture you can stand on:
                            <Bold>“They shall still bear fruit in old age; they shall be fresh and
                            flourishing” (Psalm 92:14 NKJV).</Bold> Now get up off the couch and get
                            going.
                        </Text>
                    },
                ]} />
            </View>,
            prayerPoints: [
                "Father, renew my youth as the eagles, in the name of Jesus.",
                "I reject sudden and untimely death and terminal disease, in the name of Jesus.",
                "I shall not die an infant of days but the number of my days I shall fulfill as the Lord will satisfy me with long life, in the name of Jesus.",
            ]
        },
        {
            day: 26,
            topic: "Are You in a Storm? (1)",
            wordStudy: "Matthew 8:23-27",
            confession: "When you pass through the waters, I will be with you; and through the rivers, they shall not overflow you. When you walk through the fire, you shall not be burned, nor shall the flame scorch you. For I am the LORD your God, the Holy One of Israel, your Saviour… (Isaiah 43:2- 3 NKJV).",
            fullText: <View>
                <Text style={styles.text}>
                    Matthew records, <Bold>“Jesus got into a boat, and His [disciples] went with
                    Him. A great storm arose…” (Matthew 8:23-24 NCV).</Bold> In the Greek
                    language the words <Bold>“a great storm”</Bold> are one word—seismos. A seismologist
                    studies earthquakes and a seismograph measures them. So the storm the
                    disciples were in must have shaken them to the core. There's an important
                    lesson here. Getting on board with Jesus doesn't mean you will never go
                    through a storm. Jesus said, <Bold>“In the world you will have tribulation…”
                    (John 16:33 NKJV).</Bold> In spite of God's promise to protect and prosper us,
                    you'll still have to deal with things like disease, lack, and fear. The difference
                    is: the unbeliever faces the storm without Christ, but as a believer you go
                    through the storm confident that all will be well because Jesus is on board.
                    Does that mean you will never experience panic? If only it were so! But it
                    isn't.
                    <Br />
                    Looking at the swelling waves and the sleeping Saviour, the disciples
                    asked, <Bold>“Do You not care that we are perishing?” (Mark 4:38 NKJV).</Bold> This
                    is why fear is so deadly. It corrodes your confidence in God's goodness. It
                    unleashes a swarm of doubts. It deadens your recall. By this time the
                    disciples had witnessed Jesus <Bold>“healing all kinds of sickness and all kinds of
                    disease among the people” (Matthew 4:23 NKJV).</Bold> Fear creates a form of
                    spiritual amnesia. It dulls your miracle memory. It makes you forget what
                    Jesus has done and how good God is. That's why you must starve your doubts
                    and feed your faith on God's Word, for faith is what will take you through the
                    storm.
                </Text>

            </View>,
            prayerPoints: [
                "Father, let every storm in my life cease forthwith, in the name of Jesus.",
                "The Lord shall deliver me form every storm of life, in the name of Jesus.",
                "I come against the spirit of doubt and unbelief, in the name of Jesus and by the power of His blood, be bound!",
            ]
        },
        {
            day: 27,
            topic: "Are You in a Storm? (2)",
            wordStudy: "Acts 16:16-24",
            confession: "When you pass through the waters, I will be with you; and through the rivers, they shall not overflow you. When you walk through the fire, you shall not be burned, nor shall the flame scorch you. For I am the LORD your God, the Holy One of Israel, your Saviour… (Isaiah 43:2-3 NKJV).",
            fullText: <View>
                <Text style={styles.text}>
                    Matthew records: <Bold>“Then His disciples came to Him and awoke Him,
                    saying, 'Lord, save us! We are perishing!' But He said to them, 'Why are
                    you fearful, O you of little faith?' Then He arose and rebuked the winds
                    and the sea, and there was a great calm. So the men marvelled, saying,
                    'Who can this be, that even the winds and the sea obey Him?'” (Matthew
                    8:25-27 NKJV).</Bold> You'll never really know Jesus, or what He can do for you,
                    until you go through a storm with Him. That's why He plans your storms as
                    part of the journey. Jesus asked, <Bold>“Why are you fearful…?”</Bold> He was teaching
                    us that fear will suck the life out of you and drain you dry of joy. When fear
                    rules your life, safety becomes your God. And when safety becomes your
                    God, you seek a risk-free life.
                    <Br />
                    But those who are fear-filled cannot love deeply, because love is risky.
                    They cannot give to the poor, because humanly speaking, benevolence is no
                    guarantee of return. And the fear-filled cannot dream. What if their dreams
                    shatter and fall from the sky? No wonder Jesus wages such a war against fear.
                    The Gospels list some 125 Christ-issued imperatives. Of these, 21 are to <Bold>“not
                    be afraid.”</Bold> The second most common command, to love God and your
                    neighbour, appears only eight times. If quantity is any indicator, Jesus sees
                    fear as one of our biggest issues. That's why the one statement He makes more
                    than any other, and the one He is making to you today, is: <Bold>“Don't be afraid.”</Bold>
                </Text>

            </View>,
            prayerPoints: [
                "Father, make all grace abound towards me that I may have abundance for every good work, in the name of Jesus.",
                "Father, prosper me and the work of my hands, in the name of Jesus.",
                "We come against the demon of poverty in the Church of the living God: You shall not prosper anymore in the lives of the children of God, in the name of Jesus and by the power of His blood.",
            ]
        },
        {
            day: 28,
            topic: "Withdrawing in Order to Draw",
            wordStudy: "John 6:14-15",
            confession: "To everything there is a season, and a time to every purpose under the heaven (Ecclesiastes 3:1).",
            fullText: <View>
                <Text style={styles.text}>
                    The secret to success lies in knowing what God has assigned and gifted you
                    to do. Once you discover those two things, delegate the rest or let it go. This
                    isn't easy, because others will place demands on your time and energy that
                    aren't in line with your calling. Work out what activities drain you, and unless
                    they're essential—avoid them. Then, work out the things that energise you
                    and fill your tank, and make them an essential part of your life. “But there are
                    so many demands on my time,” you say. Nobody was busier than Jesus. John
                    speaks about His workload: <Bold>“…There are…things that Jesus did, which if
                    they were written…the world itself could not contain the books that
                    would be written…” (John 21:25 NKJV).</Bold>
                    <Br />
                    Question: How did Jesus stay on track and keep from burning out?
                    Answer: He knew the secret to spiritual rest and renewal. The Bible says,
                    <Bold>“When Jesus perceived…they were about to come and take Him by
                    force to make Him king, He departed again to the mountain by Himself
                    alone” (John 6:15 NKJV).</Bold> The word “again” tells us that Jesus made a
                    regular daily practice of withdrawing from the crowd in order to pray,
                    consider His plans and priorities, and recharge His batteries. Understand
                    this: to be effective with others, you must learn to be comfortable alone with
                    yourself. Novelist Louis Auchincloss said, “The only thing that keeps a man
                    going is energy, and what is energy but liking life?” If you can carve out
                    moments to do what energises you, you'll have reserves you can draw on
                    when it's time to give to others.
                </Text>

            </View>,
            prayerPoints: [
                "Father, help me discover Your purpose for my life, in the name of Jesus.",
                "And when I have discovered it, grant me the grace to fulfill it to Your glory, in the name of Jesus.",
                "Father, remove every hindrance to my lifting and manifestation, in the name of Jesus.",
            ]
        },
        {
            day: 29,
            topic: "Round and Round It Goes!",
            wordStudy: "2 Corinthians 9:6-8",
            confession: "And God is able to make all grace abound towards you; that ye, always having all sufficiency in all things, may abound to every good work (2 Corinthians 9:8).",
            fullText: <View>
                <Text style={styles.text}>
                    The Bible says: <Bold>“…Whoever sows … generously will also reap generously.
                    Each of you should give what you have decided in your heart to give, not
                    reluctantly or under compulsion, for God loves a cheerful giver. And God
                    is able to bless you abundantly, so that in all things, at all times, having all
                    that you need, you will abound in every good work” (2 Corinthians 9:6-8
                    NIV).</Bold> When you give what God tells you to, He'll give you back more. When
                    you know that, it'll revolutionise your approach to giving. Paul isn't talking
                    about people who're trying to get rich; he's talking about “cheerful givers”
                    who want to finance God's purposes.
                    He's saying not only is it safe to give, it's the way to greater blessing.
                    That's especially good news for reluctant givers, because when you give
                    something away it can feel like a loss. But contributing to God's work isn't
                    giving something away; it's investing—with a guaranteed return. A farmer
                    who sows doesn't lose his seed, he gains a harvest. Can you imagine a farmer
                    praying this prayer? “God, give me a crop. I'm trusting You to get involved,
                    but I'm holding on to my seed just in case.” Is that what you're doing? If so,
                    here's some good news. When you sow generously, God gets involved in your
                    finances. Can you imagine a more secure position to be in? The Bible says,
                    <Bold>“The generous will prosper…” (Proverbs 11:25 NLT).</Bold> When you give, it
                    enables God to return to you even more, which in turn enables you to give even
                    more—and round and round it goes!
                </Text>

            </View>,
            prayerPoints: [
                "Father, make all grace abound towards me that I may have abundance for every good work, in the name of Jesus.",
                "Father, prosper me and the work of my hands, in the name of Jesus.",
                "We come against the demon of poverty in the Church of the living God: You shall not prosper anymore in the lives of the children of God, in the name of Jesus and by the power of His blood.",
            ]
        },
        {
            day: 30,
            topic: "A Hunger for God's Word",
            wordStudy: "1 Peter 2:1-3",
            confession: "For the word of God is quick, and powerful, and sharper than any two-edged sword, piercing even to the dividing asunder of soul and spirit, and of the joints and marrow, and is a discerner of the thoughts and intents of the heart (Hebrew 4:12).",
            fullText: <View>
                <Text style={styles.text}>
                    How does a newborn baby desire milk? With intensity you can't imagine
                    unless you've heard the midnight cry! The fact that you've worked hard all
                    day and are tired, or that you fed the baby just an hour earlier, doesn't matter.
                    An infant's agenda is about as focused and uncluttered as you can get. He or
                    she wants to eat—right now! When was the last time your spiritual stomach
                    growled so much at midnight that you just had to get up and feed your soul on
                    God's Word? Here's how Peter addressed the issue: <Bold>“Therefore, laying aside
                    all malice, all deceit, hypocrisy, envy, and all evil speaking, as newborn
                    babes, desire the pure milk of the word, that you may grow thereby, if
                    indeed you have tasted that the Lord is gracious” (1 Peter 2:1-3 NKJV).</Bold>
                    <Br />
                    Note the words “if…you have tasted.” When you've tasted the real thing
                    you can't get by on junk food. Your spiritual taste buds won't let you. Note,
                    not only can you not ignore a baby's hunger, you can't fool their sense of taste.
                    When you start mixing stuff into their formula that wasn't meant to be there,
                    their taste buds and stomach will reject it. Have you ever heard the phrase
                    “projectile vomiting”? Junk food is designed to fill a need with a quick fix
                    that may satisfy you for a while, but it doesn't provide any real nourishment.
                    You can tell when you're spiritually malnourished and underdeveloped. Peter
                    says the symptoms are malice, deceit, hypocrisy, envy, and evil speaking. So
                    open your Bible today and pray, “Lord, give me a hunger for Your Word.”
                </Text>

            </View>,
            prayerPoints: [
                "ord, give me a hunger for Your Word.",
                "et the Word of God become the joy and rejoicing of my heart, in the name of Jesus.",
                "he grace of the Lord shall abound for me and sin shall not overcome me, in the name of Jesus.",
            ]
        },
    ],
    December: [
        {
            day: 1,
            topic: "How God Works",
            wordStudy: "Acts 3:1-7",
            confession: "Blessed be God, which hath not turned away my prayer, nor his mercy from me (Psalm 66:20).",
            fullText: <View>
                <Text style={styles.text}>
                    A few important observations:
                </Text>           
                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            <Italic>Peter allowed God to change his plans.</Italic> <Bold>“…Peter and John were going
                            up to the temple at the time of prayer—at three in the afternoon”
                            (Acts 3:1 NIV).</Bold> Is prayer important? Yes, it's all–important! But you can
                            get so caught up in church activities that you walk past hurting people
                            sitting on the church's doorstep. You must be sensitive to people's needs,
                            available to God, and willing to change your plans at a moment's notice.
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            <Italic>Peter used the name that's bigger than any problem.</Italic> <Bold>“…In the name of
                            Jesus Christ of Nazareth, walk” (Acts 3:6 NIV).</Bold> And he did! Paul
                            writes, <Bold>“…At the name of Jesus every knee should bow…every
                            tongue should confess that Jesus…is Lord…” (Philippians 2:10-11
                            NKJV).</Bold> You say, “What's in a name?” In this case—everything! At the
                            name of Jesus hell trembles, sin is forgiven, sickness is healed and
                            stubborn habits broken.
                        </Text>
                    },
                    {
                        serial: 3,
                        body: <Text>
                            <Italic>Peter did more than pray; he extended a helping hand.</Italic> <Bold>“Taking him by
                            the right hand, he helped him up, and instantly the man's feet and
                            ankles became strong. He jumped to his feet and began to walk”
                            (Acts 3:7-8 NIV).</Bold> Today you're surrounded by people who need help to
                            get back on their feet spiritually, emotionally, physically, and financially.
                            Reach out in love and lift them! They don't need you to spend the rest of
                            your life supporting them. They just need a hand up—and an introduction
                            to the One who's able and waiting to meet their need. God used Peter to
                            bring deliverance to this man, and He will use you too. But you have to be
                            ready.
                        </Text>
                    },
                ]} /> 
           
            </View>,
            prayerPoints: [
                "Baptize me, O Lord, with compassion and sensitivity to others' needs spiritually, emotionally, physically and financially.",
                "Father, empower me for signs and wonders, in the name of Jesus.",
                "Lord Jesus, release fresh fire upon me.",
            ]
        },
        {
            day: 2,
            topic: "Stop Being So Critical (1)",
            wordStudy: "1 Peter 3:8-12",
            confession: "My tongue shall not speak evil all the days of my life, in the name of Jesus (1 Peter 3:10).",
            fullText: <View>
                <Text style={styles.text}>
                    God dealt with Moses because of some of the mistakes he made. In fact, one
                    of them kept him from entering the Promised Land. Nevertheless, God
                    wouldn't permit anybody else to criticise Moses—not even his sister Miriam.
                    So what can you learn from this?
                </Text>           
                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            <Italic>We're all capable of harbouring a critical attitude.</Italic> Miriam had great
                            qualities. She saved Moses' life as a child, and she wrote a song of praise
                            Israel used to celebrate the crossing of the Red Sea. But she paid a high
                            price for her critical attitude—leprosy.
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            <Italic>When you're resentful you become critical.</Italic> <Bold>“Miriam…began to talk
                            against Moses because of his Cushite wife” (Numbers 12:1 NIV).</Bold> But
                            was that the real issue? No. <Bold>“They said, 'Has the Lord indeed spoken
                            only through Moses? Has He not spoken through us also?'”
                            (Numbers 12:2 NKJV).</Bold> Moses' wife was just a diversion; the real issue
                            was Moses' success. Their beef was: “How come he gets all the
                            attention?”
                        </Text>
                    },
                    {
                        serial: 3,
                        body: <Text>
                            <Italic>Anytime you succeed you'll be criticised.</Italic> The Bible says, <Bold>“Moses was
                            very humble” (Numbers 12:3 NKJV),</Bold> yet even he couldn't escape the
                            pain inflicted by self-appointed critics. And you're no different; as long
                            as you're alive somebody will find fault with what you're doing. Brush it
                            off and keep going.
                        </Text>
                    },
                    {
                        serial: 4,
                        body: <Text>
                            <Italic>If you've been critical, you need to repent.</Italic> When Aaron acknowledged,
                            <Bold>“We have acted foolishly…we have sinned” (Numbers 12:11 NAS),</Bold>
                            God showed mercy and healed Miriam. Most of us would rather classify
                            criticism as a weakness, but from God's perspective it's a genuine, bona
                            fide, registered sin. And there's only one way to deal with sin—repent
                            and stop committing it.
                        </Text>
                    },
                ]} /> 
           
            </View>,
            prayerPoints: [
                "No, critical, judgemental spirit will not have dominion over me, in the name of Jesus.",
                "I receive the grace to stay focused in the face of criticism, in the name of Jesus.",
                "In every way I have acted foolishly, O Lord, forgive me, in the name of Jesus.",
            ]
        },
        {
            day: 3,
            topic: "Stop Being So Critical (2)",
            wordStudy: "Romans 12:9-13",
            confession: "A critical spirit shall not hinder my relationship with God and my fellow Christians all the days of my life (Ephesians 5:21).",
            fullText: <View>
                <Text style={styles.text}>
                    Having a critical attitude may not destroy your relationship with God, but it'll
                    definitely hurt your capacity to experience His love, His presence and His
                    blessing. Notice, it was God who smote Miriam with leprosy. She started out
                    by criticising her brother Moses, and ended up feeling the consequences in her
                    relationship with the Lord. Why? Because God pays attention to the way we
                    treat each other! Maybe you're wondering, “Why would God make such a big
                    deal out of this?” Because when you choose to sin, you choose to suffer.
                    Everything God classifies as sin is hurtful to you—everything. When God
                    says, “Don't,” what He really means is, “Don't hurt yourself.” And when He
                    says, “Don't criticise,” He's not trying to deprive you of satisfaction. He's
                    saying that having a critical attitude goes against who He made you to be, and
                    what you're called to do. Just as fish were made to swim and birds were made
                    to fly, you were made to live in fellowship with God—and a critical spirit
                    hinders that fellowship. Even people who don't claim to be particularly
                    religious are cognisant of the negative effects of criticism.
                    <Br />
                    Dr David Fink, author of <Italic>Release from Nervous Tension,</Italic> studied
                    thousands of mentally and emotionally disturbed people. He worked with two
                    groups: a stressed-out group and a stress-free one. Eventually one fact
                    emerged: the stressed-out group was composed of habitual fault-finders and
                    constant critics of people and things around them. On the other hand, the
                    stress-free group was loving and accepting of others. There's no doubt about
                    it, the habit of criticising is a self-destructive way to live. Don't go there.
                </Text>           
           
            </View>,
            prayerPoints: [
                "Holy Spirit, help me to look out for the good in others rather than criticizing them, in the name of Jesus.",
                "I receive grace to walk worthy of the Lord, fully pleasing Him every day, in the name of Jesus.",
                "I will not hurt myself and suffer by being critical of others, in the name of Jesus.",
            ]
        },
        {
            day: 4,
            topic: "Stop Being So Critical (3)",
            wordStudy: "Galatians 1:9-11",
            confession: "The Lord resists the proud, but gives more grace to the humble (James 4:6).",
            fullText: <View>
                <Text style={styles.text}>
                    Some important observations:
                </Text>     
                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            <Italic>Criticism is often ego-based.</Italic> Oswald Chambers wrote, “Beware of
                            anything that puts you in the place of the superior person.” And that's
                            exactly what criticism does: it highlights you as the one who “knows.”
                            Not only that, it gives you the satisfaction of shining the spotlight on
                            others. People find it much harder to see your life when you're shining
                            the glaring light of criticism on theirs. When you live this way your
                            attitude says, “If I can't make it in this world by what I do, I'll make it by
                            knowing what you should do better.” James writes, <Bold>“These things ought
                            not so to be” (James 3:10 KJV).</Bold> And Paul writes, <Bold>“Love each
                            other…and take delight in honouring each other” (Romans 12:10
                            NLT).</Bold>
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            <Italic>Criticism can break hearts.</Italic> Imagine how Moses felt when his brother
                            and sister suddenly turned on him. Because the people closest to us know
                            the details of our lives, we're always vulnerable to their criticism.
                            Sometimes as parents we leave our children's lives in shambles by
                            creating a home that's rife with criticism. Maybe as you read these words
                            you hear the voice of your harshest critic—a parent who constantly put
                            you down. A parent whose words still ring in your memory: “You were
                            never any good”; “You'll never amount to anything.”
                        </Text>
                    },
                    {
                        serial: 3,
                        body: <Text>
                            <Italic>How should you respond to criticism?</Italic> Paul answers: <Bold>“If I were…trying
                            to please men, I would not be a…servant of Christ” (Galatians 1:10
                            NAS).</Bold> Instead of listening to your critics, centre your attention on what
                            God thinks of you and your life will take an upswing. In the final
                            analysis, His is the only opinion that counts.
                        </Text>
                    },
                ]} />      
           
            </View>,
            prayerPoints: [
                "Lord, help me not to be a men's pleaser but to walk in the knowledge that You created me to live to please You, in the name of Jesus.",
                "I receive grace to love and honour my bretheren, in the name of Jesus.",
            ]
        },
        {
            day: 5,
            topic: "Stop Being So Critical (4)",
            wordStudy: "James 4:10-17",
            confession: "...And a new spirit will I put within you, and I will take away the stony heart out of your flesh, and I will give you a heart of flesh (Ezekiel 36:26).",
            fullText: <View>
                <Text style={styles.text}>
                    Nobody wants to spend time with someone who monopolises the
                    conversation by updating them on their top-ten-people-to-criticise list.
                    Staying home and watching old movie reruns is more appealing than going to
                    that kind of party! It's a hard truth to hear, but the people you need most are the
                    ones who'll avoid you when you become known as a fault-finder. Sometimes
                    criticism is inadvertent; on a better day, led by God's Spirit and focused on
                    what's positive, you'd never say such things. Notice what Aaron said: <Bold>“We
                    have acted foolishly” (Numbers 12:11 NAS).</Bold> He didn't try to defend his
                    position by saying, “Yes, Moses did marry the wrong person,” or “We
                    deserve more of the limelight.” No, he realised his mistake, repented, and
                    retreated from it. And you must do that too. Why? Because criticism blocks
                    the flow of God's blessing in your life!
                    <Br />
                    Oswald Chambers wrote, “Whenever you're in a critical temper, it's
                    impossible to enter into communion with God.” Stop and ask yourself: “Is the
                    momentary relief I get from criticising others worth losing my sense of God's
                    presence?” To regain that sense of His presence you need to confess and
                    forsake your critical attitude, then replace it with a more gracious and loving
                    one. Today, get down on your knees and pray: “Lord, forgive me for thinking
                    my perspective is always right. I acknowledge that as arrogance. Give me
                    grace in dealing with others—the same grace I've received from You. Help
                    me to accept our differences and not demand that everyone see things exactly
                    as I do. Give me victory over my critical attitude. In Jesus' name. Amen.”
                </Text>                
            </View>,
            prayerPoints: [
                "I repent and forsake my critical attitude, in the name of Jesus.",
                "Lord, forgive me for thinking my perspective is always right",
                "Lord, I acknowledge criticism as arrogance, forgive me, in the name of Jesus.",
            ]
        },
        {
            day: 6,
            topic: "Bring Down Your Goliath",
            wordStudy: "1 Samuel 17:43-47",
            confession: "Confession: …For who is this uncircumsised philistine, that he should defy the armies of the living God? (1 Samuel 17:26).",
            fullText: <View>
                <Text style={styles.text}>
                    To bring down the Goliath in your life, here are three things you must do:
                </Text>                
                <IndentList hasView={true} items={[
                    {
                        serial: 1,
                        body: <Text style={styles.bodyText}>
                            You must stand up to him! Any problem you try to excuse or escape, you
                            empower. After listening to Goliath's threats every day, fear gripped the
                            hearts of God's people and they couldn't stand up to him.
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text style={styles.bodyText}>
                            You must remember what God has already done for you. David recalled
                            his victories over the lion and the bear. And you must do the same.
                            Jeremiah said, “This I call to mind…therefore I have hope: because
                            of the Lord's great love we are not consumed, for His compassions
                            never fail. They are new every morning” (Lamentations 3:21-23
                            NIV). The strength to deal with today's struggles comes from
                            remembering how God helped you solve yesterday's struggles.
                        </Text>
                    },
                    {
                        serial: 3,
                        body: <View>
                            <Text style={styles.bodyText}>
                                You must cut off the giant's head. “David…took his sword…and cut
                                off his head…And when the Philistines saw that their champion was
                                dead, they fled” (1 Samuel 17:51 NKJV). You need to know your
                                spiritual enemy, study his tactics, and be willing to fight with the same
                                level of intensity as he does. You must take what you learn and apply it to
                                his weak areas. And never assume he's dead when he's just dazed. If you
                                do, he'll sneak up on you another day. Go for a permanent solution, not a
                                short-term fix:
                            </Text>

                            <IndentList style={{paddingHorizontal: 0}} items={[
                                {
                                    serial: "a",
                                    body: <Text>
                                        Fortify yourself with prayer.
                                    </Text>
                                },
                                {
                                    serial: "b",
                                    body: <Text>
                                        Fortify yourself with prayer.
                                    </Text>
                                },
                                {
                                    serial: "c",
                                    body: <Text>
                                        Fortify yourself with prayer.
                                    </Text>
                                },
                            ]} />
                        </View>
                    },
                ]} />
                <Text style={styles.text}>
                    Above all, remember your strength doesn't lie in yourself, but in God.
                    With Him on your side you'll win every time.
                </Text>
            </View>,
            prayerPoints: [
                "Every Goliath from my foundation, your time is up, die, in the name of Jesus",
                "You strong man of my father's house defying the power of God in my life, die, in the name of Jesus.",
                "By the finished work of Calvary, I put a stop to the activities of the strong man in my life and family, in the name of Jesus.",
            ]
        },
        {
            day: 7,
            topic: "Be a Mentor to a Fatherless Boy",
            wordStudy: "Isaiah 1:16-20",
            confession: "…Neither will we say any more to the work of our hands, Ye are our gods; for in thee the fatherless find mercy (Hosea 14:3).",
            fullText: <View>
                <Text style={styles.text}>
                    In her book <Italic>Mothers and Sons,</Italic> Jean Lush talks about the challenge single
                    mothers face in raising sons. Ages four to six are especially important and
                    difficult. A boy at that age still loves his mother, but feels the need to gravitate
                    towards a masculine image. If he has a father in the home, he'll want to spend
                    more time with his dad apart from his mother and sisters. So what advice can
                    be given to a mother who's raising a son alone? First, she must understand he
                    has needs that she's not best equipped to meet. Her best option is to recruit a
                    man who can act as a role model to her son. Of course, good mentors can be
                    difficult to find. Single mothers should consider friends, relatives or
                    neighbours who can offer as little as an hour or two a month. Single mothers
                    who belong to a church should be able to find support for their boys among the
                    male members.
                    <Br />
                    Scripture commands people of faith to care for children without fathers:
                    <Bold>“…Defend the cause of the fatherless, plead the case of the widow” (Isaiah
                    1:17b).</Bold> Jesus took boys and girls on His lap and said, <Bold>“Whoever welcomes a
                    little child like this in My name welcomes Me” (Matthew 18:5 NIV).</Bold> If you
                    are a man and you have been asking God to use you in His service, this could
                    be a real ministry opportunity for you. Think of the incredible potential of one
                    small boy, and the privilege of helping to mould him into a man of God who
                    fulfils the purposes of God during his lifetime. What a privilege!
                    <Br />
                </Text>
            </View>,
            prayerPoints: [
                "I receive grace to defend the course of the fatherless, in the name of Jesus.",
                "In every way I have fallen short in this area, Lord, have mercy on me, in the name of Jesus.",
                "Father, make me a blessing to the less privileged in the society, in the name of Jesus.",
            ]
        },
        {
            day: 8,
            topic: "Control Your Anger",
            wordStudy: "Ephesians 4:22-27",
            confession: "The Lord is merciful and gracious, slow to anger, and abundant in loving kindness (Psalm 103:8).",
            fullText: <View>
                <Text style={styles.text}>
                    Uncontrolled anger is like jumping into your car, revving the engine, and
                    discovering too late that the brakes don't work. The Bible says, <Bold>“Be angry
                    without sinning. Don't go to bed angry. Don't give the devil any
                    opportunity [to work]” (Ephesians 4:26-27 GWT).</Bold> Did you get that?
                    Uncontrolled anger opens the door to Satan—and it's all downhill from there!
                    So before you say something you'll regret and can't take back, ask yourself:
                </Text>
                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            <Italic>Is the relief I'll get from venting worth the aftermath?</Italic> The Bible says, <Bold>“A
                            gentle answer turns away wrath, but a harsh word stirs up anger”
                            (Proverbs 15:1 NIV).</Bold> By sounding off, you run the risk of making the
                            finest speech you'll ever regret. By its very nature anger encourages
                            exaggeration, and makes you say things you can't retract. Long after
                            you've moved on, harsh words maintain their power to wound and
                            divide.
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            <Italic>Is it really worth dragging other people into it?</Italic> Anger inevitably affects
                            those around you because it's human to want to take sides, even if you've
                            “no dog in the fight.” Involving other people is usually a way to feed
                            your ego and justify bad behaviour. Don't do it.
                        </Text>
                    },
                    {
                        serial: 3,
                        body: <Text>
                            <Italic>Is my anger appropriate?</Italic> Anger over ignorance and injustice has
                            always led to progress. But it's easy to let small stuff like thoughtless
                            comments and cranky kids make you overreact. For anger to have a
                            healthy result it needs to be measured and constructive. Paul says, <Bold>“The
                            mind governed by the Spirit is life and peace” (Romans 8:6 NIV).</Bold> It
                            comes down to a control issue, and a controlled response is a Christlike
                            response. It always wins.
                        </Text>
                    },
                ]} />
            </View>,
            prayerPoints: [
                "Every door I have opened to the devil as a result of anger, O Lord, shut them, in the name of Jesus.",
                "The spirit of anger will not destroy my life and destiny, in the name of Jesus.",
                "I shall get to my Promised Land, in the name of Jesus.",
            ]
        },
        {
            day: 9,
            topic: "The Cure for Spiritual Boredom",
            wordStudy: "James 1:21-25",
            confession: "Thy word is a lamp unto my feet, and a light unto my path (Psalm 119:105).",
            fullText: <View>
                <Text style={styles.text}>
                    Why does the Bible say, <Bold>“Be doers of the word, and not hearers only”?</Bold>
                    Because hearing without doing becomes boring—every time. You can get to
                    the place where you've heard so much preaching and teaching that you say to
                    yourself, “Oh no, not another sermon!” The problem isn't the Word; it's that
                    you've become oversaturated and spiritually numb. You're bored because you
                    aren't putting it into practice and reaping the rewards of what you've heard.
                    Jesus said, <Bold>“If you know these things, blessed are you if you do them”
                    (John 13:17 NKJV).</Bold> Once you start doing what you've been told to do, you
                    won't have time to be bored.
                    <Br />
                    The phrase <Bold>“hearers only”</Bold> is from the Greek noun akroates. Today we'd
                    use it to describe students who sit in on a class just to hear the teaching, instead
                    of taking any assessment or being graded. These folks weren't interested in
                    learning, passing exams, earning a degree and going out to make a difference
                    in the world. They were there because they found it intellectually stimulating
                    and they loved the excitement of being with the crowd. Often they followed
                    their favourite teacher from one town to another; they loved new speakers,
                    and when the class ended they'd get together to eat, drink, laugh and discuss
                    what they'd heard. Mostly, they just wanted to look knowledgeable in each
                    other's eyes.
                    <Br />
                    Don't let that happen to you. <Bold>“Remember… knowing what is right to do
                    and… not doing it is sin” (James 4:17 TLB).</Bold> Open your heart to the truths
                    you've heard preached and begin to put them to work in your life. When you
                    do, rest assured you'll never again suffer from spiritual boredom.
                </Text>
            </View>,
            prayerPoints: [
                "My path will shine brighter and brighter unto the perfect day, in the name of Jesus.",
                "The Word of God in my heart will not be stolen by the cares of this world, in the name of Jesus.",
                "I receive grace to be a doer of God's word, and not just a hearer, in the name of Jesus.",
            ]
        },
        {
            day: 10,
            topic: "Share Your Life",
            wordStudy: "Acts 4:31-34",
            confession: "Now all who believed were together, and had all things in common, and sold their possessionsand goods, and divided them among all, as anyone had need (Act 2:44-45).",
            fullText: <View>
                <Text style={styles.text}>
                    When you read the story of the New Testament Church, you tend to get
                    caught up in its explosive growth and amazing miracles. But here's a
                    component you shouldn't miss: <Bold>“No one claimed that any of his
                    possessions was his own, but they shared everything they had… There
                    were no needy persons among them…those who owned lands or houses
                    sold them, brought the money…and it was distributed to anyone as he
                    had need” (Acts 4:32-35 NIV).</Bold> You say, “If I just had more money I'd be
                    happy.” You might feel more secure and have fewer worries, but you
                    wouldn't necessarily be happier. In the <Italic>Journal of Happiness Studies,</Italic>
                    researchers looked at what distinguished quite happy people from less happy
                    people. One factor consistently separated those two groups. It's not about
                    how much money you have; it's not about your health, security,
                    attractiveness, IQ or career success. What distinguishes consistently happy
                    people from less happy people is the presence of rich, deep, joy-producing,
                    life-changing, meaningful relationships.
                    <Br />
                    Social researcher Robert Putnam writes: “The single most common
                    finding from a half-century's research on life satisfaction, not only in the U.S.
                    but around the world, is that happiness is best predicted by the breadth and
                    depth of one's social connections.” But you can know a lot of people without
                    really being known by any of them, and end up lonely. Those folks in the New
                    Testament Church got it right: it's in sharing with one another spiritually,
                    emotionally, financially and relationally that you achieve your highest level
                    of joy.
                </Text>
            </View>,
            prayerPoints: [
                "Let the spirit of giving envelope my heart, in the name of Jesus.",
                "By the love of God that has been shed abroad in my heart by the Holy",
                "Spirit, I receive grace to share my life with others spiritually, emotionally, financially and relationally, in the name of Jesus. ▪ Grace to lay it all at the master's feet, rest on me, in the name of Jesus.",
            ]
        },
        {
            day: 11,
            topic: "Learn to Say “Enough!”",
            wordStudy: "Matthew 14:22-27",
            confession: "Only in returning to Me and resting in Me will you be saved. In quietness and confidence is your strength… (Isaiah 30:15).",
            fullText: <View>
                <Text style={styles.text}>
                    Jesus dismissed a crowd of listeners and fans in order to spend time with His
                    Father in prayer. As a result, when He returned to the crowd He was
                    empowered to work miracles. Think about it; before an airliner takes off, the
                    attendant tells you that if the plane gets into trouble you should secure your
                    own oxygen mask before attempting to help others with theirs. After all,
                    unless you're getting enough oxygen, how can you help them? So, are your
                    own needs being met? If not, it's time to start taking care of yourself before
                    you burn out. You can't travel quietly through life hoping people will
                    recognize when your plate is full. Speak up, or they'll just keep pouring on
                    more problems and responsibilities. Personal empowerment begins by taking
                    control of your life. Overloaded people fail at marriage, ministry and
                    management. They fail at parenting, partnership and personal endeavours.
                    <Br />
                    Like an airplane, if you carry too much baggage you won't get off the
                    ground. When you're motivated by the need to please others or impress them,
                    you'll take on too much and fail to reach the heights God planned for you. Or
                    you'll crash because you ignored your limitations. Every situation that arises
                    doesn't warrant your attention! Was Jesus misunderstood when He dismissed
                    the crowd? Probably. And you'll have to make that same decision too. People
                    who don't recognise your needs and respect your goals will drain you, divert
                    you, and keep you grounded. So what's the answer? Give what you can, and
                    learn when to say 'enough!'
                </Text>
            </View>,
            prayerPoints: [
                "The grace to retreat regularly, rest on me, in the name of Jesus.",
                "Power of God for the miraculous as I retreat, rest on me, in the name of Jesus.",
                "I receive fresh fire, fresh grace on a daily basis, in the name of Jesus.",
            ]
        },
        {
            day: 12,
            topic: "The Benefits of Going to Church",
            wordStudy: "Psalm 122:1-9",
            confession: "Jehovah, who shall sojourn in thy tabernacle? Who shall dwell in thy holy hill? He that walketh uprightly, and worketh righteousness (Psalm 15:1-2).",
            fullText: <View>
                <Text style={styles.text}>
                    People stay away from church for many reasons. Sometimes they feel bad
                    because their faith isn't working as well as they think it should. Or they're still
                    struggling with certain problems. Or they're depressed because it looks like
                    everyone else is doing well except them. Don't let discouragement keep you
                    away from your spiritual family. You need their love and support. You need
                    to hear them say, “We made it, and by God's grace you can too.” The Bible
                    says, <Bold>“Not forsaking the assembling of ourselves together, as is the
                    manner of some” (Hebrews 10:25 NKJV).</Bold>
                    <Br />
                    The word <Italic>forsaking</Italic> is taken from three Greek words which could be
                    translated “out,” “down,” and “behind.” It pretty much describes someone
                    who feels left out, spiritually and emotionally down, and far behind everyone
                    else. The moment you feel that way the enemy whispers, “Just stay home
                    from church today; you don't need to go there with all those good people.” If
                    the enemy can separate you from other believers at the very time you need
                    them most, he can rob you of what God has in store for you. Sure, you can
                    stay at home, read your Bible and turn on Christian radio and television. But
                    surrounded by your spiritual family you'll get answers, experience joy and
                    receive encouragement you can't find anywhere else. Church is the last place
                    the devil wants you to go when you're feeling low. He knows if you go, you'll
                    be touched by the presence of the Lord and be able to crawl out of the hole
                    you're in. So, go to church!
                </Text>
            </View>,
            prayerPoints: [
                "Lord, help me to be obedient to Your every commandment, in the name of Jesus.",
                "Every plan to take me out of fellowship, be frustrated, in the name of Jesus.",
                "Every plan to isolate and destroy my destiny in Christ is terminated today, in the name of Jesus.",
            ]
        },
        {
            day: 13,
            topic: "Critics, Coaches & Cheerleaders",
            wordStudy: "Proverbs 25:9-13",
            confession: "I shall not be discouraged, in the name of Jesus.",
            fullText: <View>
                <Text style={styles.text}>
                    Critics point out what's wrong, and leave you feeling bad about yourself.
                    Coaches show you what you did wrong, teach you how to do it right and leave
                    you feeling better about yourself. Cheerleaders lift your spirits, offer their
                    help, and assure you that you can succeed if you keep trying. So which of the
                    three are you? If you tend to be one of those hard-driving, goal-oriented
                    people, coaching someone may be a challenge for you. Perhaps you're from
                    the school of thought that believes a pay cheque should be encouragement
                    enough—especially if an employee is being generously compensated. If
                    so—beware! You're stuck in the Stone Age and in dire need of a mindset
                    change! If you want people to be productive, learn how to build them up. You
                    say, “But it's not my natural inclination to want to coach poor performers. I just
                    want them out.” Hold on there; you're dealing with a human being, a person
                    loved and valued by God! And with a little encouragement, some handholding
                    and lots of communication, you may end up with a level of
                    productivity and loyalty the cocky superstar isn't capable of. Yes, if you've “a
                    bad apple” in the bunch you need to get rid of them before they infect the
                    whole barrel. But before you resort to firing someone, try firing them up!
                    Some of the people you work with just need to be presented with a clear vision,
                    shown how they can be part of it and enlightened about the incentives and
                    rewards that come from being part of the team that's fulfilling a vision.
                </Text>
            </View>,
            prayerPoints: [
                "Lord, help me to be obedient to Your every commandment, in the name of Jesus.",
                "Every plan to take me out of fellowship, be frustrated, in the name of Jesus.",
                "Every plan to isolate and destroy my destiny in Christ is terminated today, in the name of Jesus.",
            ]
        },
        {
            day: 14,
            topic: "What It Means to Repent",
            wordStudy: "2 Corinthians 7:8-11",
            confession: "Create in me a clean heart, O God; and renew a right spirit within me (Psalm 51:10).",
            fullText: <View>
                <Text style={styles.text}>
                    The word <Italic>repent</Italic> means to acknowledge your sin before God, turn from it,
                    seek God's forgiveness, and start living differently. It means doing an aboutface
                    turn and heading in the opposite direction. If you go 20 km down the road
                    in the wrong direction, it requires doing a U-turn and coming 20 km back. At
                    first this can seem discouraging. But it's profitable, because next time you'll
                    think twice about where you're headed. Repentance sometimes means
                    making restitution to others. Zacchaeus was a tax collector who got rich by
                    overcharging people. But after he met Jesus he said, <Bold>“If I have cheated
                    anyone, I will pay back four times as much” (Luke 19:8 GNT).</Bold>
                    <Br />
                    God is more than willing to forgive you, but He may allow you to
                    experience the painful consequences of your sin in order to motivate you
                    towards obedience. <Bold>“No discipline is enjoyable while it is happening—it's
                    painful! But afterward there will be a peaceful harvest of right living for
                    those who are trained in this way” (Hebrews 12:11 NLT).</Bold> Satan will try to
                    tell you that you are beyond the reach of God's grace, but you're not. The
                    Prodigal Son wasted his inheritance and ended up in a pigsty. But the day he
                    decided to come back home, his father ran to meet him and restored him to
                    full sonship in the family. And God will do that for you too. <Bold>“Let the wicked
                    change their ways and banish the very thought of doing wrong. Let them
                    turn to the Lord that He may have mercy on them. Yes, turn to our God,
                    for He will forgive generously” (Isaiah 55:7 NLT).</Bold>
                </Text>
            </View>,
            prayerPoints: [
                "Let the grace for genuine repentance, rest on me, in the name of Jesus.",
                "I receive the boldness to make restitution where necessary, in the name of Jesus.",
                "The mercy of God shall not depart from me and my household, in the name of Jesus.",
            ]
        },
        {
            day: 15,
            topic: "Seeing God's Hand in It",
            wordStudy: "Matthew 21:42-46",
            confession: "The lines are fallen unto me in pleasant places (Psalm 16:6).",
            fullText: <View>
                <Text style={styles.text}>
                    The rejection of his brothers put Joseph on a path that led to the throne of
                    Egypt and the saving of his family and his nation. How often has something
                    happened in your life that you later realised was necessary? If you hadn't
                    experienced this or walked through that, you wouldn't have been ready for the
                    blessings you enjoy now. When you begin to see the hand of God in it you
                    understand that what the enemy intended for your destruction, God used for
                    your development. To be <Bold>“more than a conqueror” (Romans 8:37)</Bold> means
                    you can stand up and say: “Here's how I see it. It took everything I've been
                    through to make me who I am and to teach me what I know. So I choose to be
                    better, not bitter. I trust the faithfulness of God more than ever. I've learned
                    that if faith doesn't move the mountain, it'll give me strength to endure until
                    tomorrow. And if it is not gone by tomorrow, I'll still believe that God is able
                    and trust Him until He does.” Relax, rejoice; your steps are being arranged by
                    the Lord <Bold>(Psalm 37:23).</Bold> He hasn't taken His eye or His hand off you, not even
                    for a single moment. When you get through this trial you'll realise that “the
                    worst thing that could have happened” was, in reality, <Bold>“the Lord's doing,”
                    and it will become “marvellous in your eyes” (Psalm 118:23).</Bold>
                </Text>
            </View>,
            prayerPoints: [
                "Because my steps are ordered by the Lord, He will cause all things to work together for my good and His glory, in the name of Jesus.",
                "Lord, help me to see Your hand in my situation always, in the name of Jesus.",
                "Let the ears of my heart be connected always to Your voice, in the name of Jesus.",
            ]
        },
        {
            day: 16,
            topic: "Protect Your Life Source",
            wordStudy: "Revelation 22:16-18",
            confession: "…Out of my belly shall flow rivers of living waters (John 7:38).",
            fullText: <View>
                <Text style={styles.text}>
                    The Babylonians encircled Jerusalem and cut off its food supply. The question
                    was, how long could they hold out? That's what the Babylonians kept
                    wondering. But a month passed, then two, then an entire year, and still they
                    held out. The secret of Jerusalem's survival lay in a water supply from a spring
                    outside the city walls where Hezekiah had cut a 542 m long tunnel through
                    solid rock. From there water passed under the city walls to a reservoir inside
                    called the Pool of Siloam. Without it God's people would have gone down in
                    defeat. But it's not just another Bible story; there's an important lesson here for
                    you. To live victoriously you must:
                    <Br />
                </Text>
                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            Know your life's true source.
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            Protect it; and
                        </Text>
                    },
                    {
                        serial: 3,
                        body: <Text>
                            Draw from it daily.
                        </Text>
                    },
                ]} />   

                <Text style={styles.text}>
                    If your security, your strength, your self-worth or your strategy for living
                    comes from any other source but God, the enemy can defeat you. Everything
                    you need comes from God, so protect and nurture your relationship with Him,
                    for it will always be the focal point of Satan's attack. A day without reading
                    God's Word isn't merely a slip; it's a set-up for failure. Prayerlessness isn't
                    carelessness; it's foolishness in the extreme. You say, “Well, I'm doing OK and
                    I don't pray or read the Bible very much.” Maybe you haven't reached your
                    hour of testing yet. When that comes, without an established source to draw
                    from you'll struggle more and succeed less. Is that really how you want to live?
                    If not, the Word for you today is: <Bold>“Come and drink the Water of Life”</Bold>
                </Text>
            </View>,
            prayerPoints: [
                "My Life Source shall not dry up, in the name of Jesus.",
                "By the mercy of God, I will not be disconnected from the fountain of life, in the name of Jesus.",
                "Grace to draw daily from my Life Source, rest on me, in the name of Jesus.",
            ]
        },
        {
            day: 17,
            topic: "The Benefits of Reading God's Word",
            wordStudy: "Jeremiah 15:15-21",
            confession: "I have set the Lord always before me. Because He is at my right hand, I will not be shaken. Therefore my heart is glad and my tongue rejoices; my body also will rest secure (Psalm 16:8-9).",
            fullText: <View>
                <Text style={styles.text}>
                    God promises certain things to those who take time each day to get to know
                    Him through His Word. Let's look at some of them:
                    <Br />
                </Text>
                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            Joy. <Bold>“When your words came, I ate them; they were my joy and my
                            heart's delight, for I bear Your name, Lord God Almighty”
                            (Jeremiah 15:16 NIV).</Bold>
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            Strength. <Bold>“Now I commit you to God and to the word of His grace,
                            which can build you up and give you an inheritance among all those
                            who are sanctified” (Acts 20:32 NIV).</Bold>
                        </Text>
                    },
                    {
                        serial: 3,
                        body: <Text>
                            Peace. <Bold>“Great peace have those who love Your law, and nothing can
                            make them stumble” (Psalm 119:165 NIV). “If only you had paid
                            attention to My commands, your peace would have been like a river”
                            (Isaiah 48:18 NIV).</Bold>
                        </Text>
                    },
                    {
                        serial: 4,
                        body: <Text>
                            Stability. <Bold>“I have set the Lord always before me. Because He is at my
                            right hand, I will not be shaken. Therefore my heart is glad and my
                            tongue rejoices; my body also will rest secure” (Psalm 16:8-9 NIV).</Bold>
                        </Text>
                    },
                    {
                        serial: 5,
                        body: <Text>
                            Success. <Bold>“Do not let this Book of the Law depart from your mouth;
                            meditate on it day and night, so that you may be careful to do
                            everything written in it. Then you will be prosperous and successful”
                            (Joshua 1:8 NIV).</Bold>
                        </Text>
                    },
                    {
                        serial: 6,
                        body: <Text>
                            Answered prayer. <Bold>“If you remain in Me and My words remain in you,
                            ask whatever you wish, and it will be done for you” (John 15:7 NIV).</Bold>
                        </Text>
                    },
                ]} />   

                <Text style={styles.text}>
                    Joy, strength, peace, stability, success, and answered prayer—these are
                    the things everybody wants. Don't you? Well, look no further. God has
                    promised them to you when you spend time with Him in His Word each day.
                </Text>
            </View>,
            prayerPoints: [
                "The Word of God shall grow and multiply in my life and family, in the name of Jesus.",
                "There shall be a performance of God's Word concerning me, in the name of Jesus.",
                "His promises in my life shall always be “yea” and “amen,” in the name of Jesus.",
            ]
        },
        {
            day: 18,
            topic: "Pastor, How Are You Doing? (1)",
            wordStudy: "Matthew 18:11-14",
            confession: "I am the good shepherd: the good shepherd lays down his life for the sheep (John 10:46).",
            fullText: <View>
                <Text style={styles.text}>
                    When a traveler was robbed and left to die on the Jericho Road, Jesus said,
                    <Bold>“A… priest… Likewise a Levite…passed by on the other side.” (Luke
                    10:31-32 NKJV).</Bold> That sounds cold, doesn't it? But what if they overlooked
                    the man not because they lacked compassion, but because they were late for a
                    Bible study or a board meeting? Would they have been justified? James
                    MacDonald writes: “This parable didn't register until I'd been in the ministry
                    for years. I'd walked past so many people who could've used my help, but
                    because I was too distracted by church programmes, broken lives by the
                    roadside rarely fitted into my agenda. Love isn't efficient; it can't be
                    scheduled. This hurting man couldn't have waited three weeks for an
                    appointment, or for the Samaritan to launch a ministry of care to similar
                    victims.”
                    <Br />
                    Life's most rewarding experiences rarely come in neat little packages.
                    They're found in unexpected encounters and at critical times in the lives of
                    people. In fact, some people who can't remember your last sermon can tell
                    you in detail what you said to them in the hospital or over coffee last year,
                    because it touched them so personally. Nothing in ministry is more seductive
                    than thinking your work on behalf of the multitudes justifies ignoring those
                    who can only be reached individually. Jesus said, <Bold>“If a [shepherd] has a
                    hundred sheep, and one…is lost…Won't he…go…search for the lost
                    one?”</Bold> When Peter said, <Bold>“Lord…I love You,” Jesus said, “Take care of My
                    sheep” (John 21:16 NIV).</Bold> And today He's saying that to you, Pastor!
                    <Br />
                </Text>
            </View>,
            prayerPoints: [
                "Lord, forgive me for every sheep I have lost as a result of negligence.",
                "The grace to function well as Your undershepherd and care for Your sheep, rest on me, in the name of Jesus.",
                "Lord, help me not to pass by, when I see Your sheep in need, in the name of Jesus.",
            ]
        },
        {
            day: 19,
            topic: "Pastor, How Are You Doing? (2)",
            wordStudy: "John 10:7-18",
            confession: "I myself will be the shepherd of my sheep, and I will cause them to lie down, says the Lord (Ezekiel 34:15).",
            fullText: <View>
                <Text style={styles.text}>
                    Sometimes pastors hide behind their busy schedules to avoid having to say
                    “no” to people. Jesus knew when to get involved, and when not to. Learn from
                    Him. You'll never be able to respond effectively if you feel obligated to meet
                    every need all the time. And God's not asking you to. He only expects you to
                    respond to what He sends your way.
                    <Br />
                    James MacDonald says: “I'll never forget my guidance counsellor's face
                    when she heard I was going into the ministry. 'But you don't even like people!'
                    she exclaimed. I remember thinking, 'So what? Ministry's about preaching
                    and leading a congregation to greater heights. I don't have to worry about
                    individuals.' But that's a myth. We dare not become so programme-focused
                    that we flee from the next person God sends our way. I'm amazed how far
                    bureaucracies will go to make systemic change, rather than making an
                    exception that would easily fix the problem. Personal needs are too significant
                    to commit to the rigidity of any programme. Some of us think the problem
                    with handling needs personally is that they can multiply like rabbits. Plus,
                    programmes exist to prevent you from becoming overwhelmed, right?
                    Wrong! The 'ministry myth' that says, 'What you do for one you must do for
                    all,' is a recipe for burnout.”
                    <Br />
                    Jesus spent much of His time ministering to individual needs. And He
                    said, <Bold>“The good shepherd lays down his life for the sheep.”</Bold> But in order to
                    do that you must make room in your life for the unexpected.
                    <Br />
                </Text>
            </View>,
            prayerPoints: [
                "Lord, anoint me afresh for this assignment, in the name of Jesus.",
                "Grace to lay down my life for Your sheep, rest on me, in the name of Jesus.",
                "Every plan from hell to smite me for the sheep to scatter, fail woefully, in the name of Jesus.",
            ]
        },
        {
            day: 20,
            topic: "Pastor, How Are You Doing? (3)",
            wordStudy: "Isaiah 30:15-18",
            confession: "The Lord shall give me rest in Jesus name-Amen",
            fullText: <View>
                <Text style={styles.text}>
                    God said, <Bold>“Only in returning to Me and resting in Me will you be saved.
                    In quietness and confidence is your strength. But you would have none of
                    it” (Isaiah 30:15 NLT).</Bold> When you're emotionally and spiritually drained by
                    people's expectations, it's easy to grow numb to the full range of human
                    emotions. And while it may seem like a relief to be free from negative
                    emotions, the positive ones also become elusive because you don't feel much
                    of anything—good or bad. When that happens, you're likely to end up being
                    directed by things like: (1) Over-scheduling (2) Poor time management (3)
                    Performance anxiety (4) Having few boundaries (5) Tolerating toxic
                    relationships and bad habits (6) Unresolved grief or pain; and (7) Wrong
                    goals.
                    <Br />
                    So what's the answer? <Bold>“Only in returning to Me and resting in Me will
                    you be saved.”</Bold> Instead of pushing on and struggling to keep going, stop and
                    talk to God about what's happening in your life. Rather than feeling isolated
                    and weighed down by the impossibility of your situation, include Him in the
                    equation by praying: “Lord, help me to keep my mind stayed on You. During
                    this time of busyness and stress help me to reorder my priorities according to
                    Your will, to think Your thoughts, and to let the mind of Christ have its
                    rightful authority in my life. Your Word says You've ordained peace for me.
                    Because You are my fortress and my deliverer, I will not allow myself to be
                    troubled or afraid. Thank You for keeping my heart and mind at rest, through
                    Christ Jesus. Amen.”
                </Text>
            </View>,
            prayerPoints: [
                "Lord, strengthen my hands for this Kingdom assignment, in the name of Jesus.",
                "Help me to get my priorities right, and put first things first, in the name of Jesus.",
                "Lord, help me to feed the sheep and not myself, in the name of Jesus.",
            ]
        },
        {
            day: 21,
            topic: "Pastor, How Are You Doing? (4)",
            wordStudy: "Mark 6:30-32",
            confession: "The hearing ear, and the seeing eye, the Lord hath made even both of them (Proverbs 20:12).",
            fullText: <View>
                <Text style={styles.text}>
                    There are two kinds of “tired.” And the dissimilarity is like the difference
                    between puffy spring rain clouds and the clouds that precede a tornado. One is
                    temporary and normal. It comes from a job well done, and after a period of rest
                    you bounce back. The other is a chronic inner fatigue that accumulates over
                    months, and doesn't always manifest itself in physical exhaustion. In fact, it's
                    often masked by frenetic activity and impulsive behaviours such as: (1) You
                    can't relax over a meal or coffee. (2) You keep checking and rechecking your
                    voice messages and emails. (3) Your bedside table is piled high with
                    publications designed to keep you “ahead of the game.” (4) Taking a day off
                    seems impossible. (5) You don't take breaks and you work every holiday. (6)
                    You can't sleep. (7) Any free time you have is spent in “escapist” behaviour
                    like eating, drinking, spending and mindlessly watching TV. When we don't
                    rest, we lose direction. We're lured away from God by the mistaken belief that
                    determination and effort will allow us to achieve our dreams.
                    <Br />
                    So, the truth is: while you're busy working hard and looking important,
                    you can lose your ability to hear the voice of the One who called you to your
                    position initially. Yes, God expects you to work hard, but not by endangering
                    your health, your family or your time with Him. If that's the shape you're in
                    right now, Jesus is saying, <Bold>“Come aside…and rest a while.”</Bold> If you're wise
                    and you want to go the distance—you'll pay attention and do what He says.
                </Text>
            </View>,
            prayerPoints: [
                "Father, a hearing ear and a seeing ear, give unto me, in the name of Jesus.",
                "Lord, help me to know my limits, in the name of Jesus.",
                "Holy Spirit, grant me the wisdom to trust wholeheartedly in You and not on my strength, in the name of Jesus.",
            ]
        },
        {
            day: 22,
            topic: "Godly Parents, Godly Children",
            wordStudy: "Genesis 18:16-21",
            confession: "I and the children the Lord has given me, are for signs and for wonders in Isreal from the Lord of hosts who dwells in mount Zion (Isaiah 8:18).",
            fullText: <View>
                <Text style={styles.text}>
                    Speaking about Abraham, God said, <Bold>“He will direct his children… to keep
                    the way of the Lord”</Bold> Can God say that about you? When you get serious
                    about raising godly children, God puts the focus where it belongs—on you! If
                    you want to see important truths sail right over your children's heads, try
                    teaching them something you're not personally committed to. Do you think
                    your children are just being difficult? No, they're watching you like a hawk! If
                    your actions demonstrate that what you're teaching them isn't important to
                    you, it'll never be important to them either. And can you blame them? If you're
                    devoted to chasing material things, they will be too. But if you devote
                    yourself to serving the Lord, they'll be drawn to Him as well.
                    <Br />
                    Do you want to enjoy a long, happy life? Pass God's teaching on to your
                    children, and to their children after them <Bold>(See Deuteronomy 6:2.).</Bold> Notice,
                    your love for the Lord comes first: <Bold>“You shall love the Lord your God with
                    all your heart” (Deuteronomy 6:5 NKJV).</Bold> Surprised? You shouldn't be.
                    When your lifestyle demonstrates your love for Christ, He'll attract your
                    children like nothing else on earth. Do your children see you reading the
                    Bible? Do you discuss its truths with them in an engaging way? When
                    behaviours and values come into question, do you lead them to the Scripture
                    for answers? Why is this so important? Because the Bible says, <Bold>“Another
                    generation arose after them who did not know the Lord” (Judges 2:10
                    NKJV).</Bold> In order to raise godly children you must teach them God's
                    principles, and practice them yourself.
                </Text>
            </View>,
            prayerPoints: [
                "I receive the grace to be a doer of God's Word and set godly examples for my children, in the name of Jesus.",
                "Let the awe and fear of the Lord possess me and my children, in the name of Jesus.",
                "My children shall grow to love the Lord, in the name of Jesus.",
            ]
        },
        {
            day: 23,
            topic: "Don't Boast",
            wordStudy: "1 Corinthians 15:9-14",
            confession: "Pride goes before destruction, and a haughty spirit before a fall (Proverbs 16:18).",
            fullText: <View>
                <Text style={styles.text}>
                    If you've been blessed with success, read these words from Paul and take them
                    to heart: <Bold>“God was kind! He made me what I am, and His wonderful
                    kindness wasn't wasted”</Bold> Every good thing you have right now, plus every
                    good thing you'll enjoy in the future, comes from God. Try never to forget
                    that! You say, 'Wait a minute, I worked hard for this. Don't I deserve a little
                    credit?' Yes, you do; the Bible says, <Bold>“Give honour and respect to all those to
                    whom it is due” (Romans 13:7 TLB).</Bold> But praise by its very nature can be
                    intoxicating. The human body is a remarkable piece of chemistry; pat a man or
                    woman on the back—and their head starts to swell! Someone said 'Praise is
                    like perfume; if you consume it, it'll kill you!' That's why Paul gives us this
                    timely reminder: <Bold>“What are you so puffed up about? …if all you have is
                    from God, why act as though you are so great?” (1 Corinthians 4:7 TLB).</Bold>
                    <Br />
                    One day King Nebuchadnezzar's head got too big for his hat, and he
                    boasted, <Bold>“Look at this great city of Babylon! By my own mighty power, I
                    have built…” (Daniel 4:30 NLT).</Bold> But God interrupted his proud moment
                    and stripped him of his kingdom, and he ended up losing his mind and living
                    like a wild beast. Only when he repented and acknowledged that God was the
                    ruler over everything, did God restore his sanity and his kingdom. Corrected,
                    humbled and enlightened, he knelt down and prayed, <Bold>“I, Nebuchadnezzar,
                    raised my eyes towards Heaven, and my sanity was restored” (Daniel
                    4:34 NIV).</Bold> Boasting is a form of insanity—don't do it!
                </Text>
            </View>,
            prayerPoints: [
                "Lord, let not the foot of pride come against me, in the name of Jesus",
                "Let not the hand of the wicked drive me away, in the name of Jesus.",
                "The grace to be humble at all times, rest on me, in the name of Jesus.",
            ]
        },
        {
            day: 24,
            topic: "This Christmas, Look for Jesus",
            wordStudy: "Matthew 2:9-12",
            confession: "I will sanctify the Lord of hosts himself; and he will be my fear, and my dread (Isaiah 8:13).",
            fullText: <View>
                <Text style={styles.text}>
                    The Wise Men couldn't have imagined humbler circumstances than those
                    surrounding the birth of Jesus. Max Lucado paints the picture: “The ground is
                    hard, the hay scarce. Cobwebs cling to the ceiling… Mary looks into the face
                    of her Son. Her Lord. His Majesty. At this point the human being who best
                    understands who God is and what He's doing is a teenage girl… She
                    remembers the angel's words, 'His Kingdom will never end.' Majesty in the
                    midst of the mundane. Honour in the filth of manure and sweat. Divinity
                    entering the world on the floor of a stable. This baby had once overlooked the
                    universe. His robes of eternity were exchanged for the rags keeping Him
                    warm. His golden throne room abandoned in favour of a dirty sheep pen.
                    Worshipping angels replaced with shepherds. Meanwhile the city hums,
                    unaware that God has visited their planet. The innkeeper would never believe
                    he'd just sent God out into the cold. And people would scoff at anyone who
                    told them the Messiah lay in the arms of a teenager on the outskirts of their
                    village. They were all too busy to consider the possibility. But those who
                    missed His Majesty's arrival that night missed it not because of acts of evil or
                    malice. No, they missed it because they weren't looking for Him!”
                    <Br />
                    This Christmas, in the midst of the toys, the tinsel, and the tumult, stop
                    and look for Jesus. If you know Him as your Lord and Saviour, take time to
                    worship Him. And if you don't, make Him your Lord and Saviour. If you do,
                    it'll be the best Christmas you've ever had.
                </Text>
            </View>,
            prayerPoints: [
                "Let the spirit of the fear of God envelope my heart this season and always, in the name of Jesus.",
                "Lord, cause me and my family to have an encounter with You this season, in the name of Jesus",
                "And, Lord, may our lives not remain the same again, in the name of Jesus.",
                
            ]
        },
        {
            day: 25,
            topic: "Quite a Story, Isn't It?",
            wordStudy: "Matthew 2:11",
            confession: "…His name shall be called Wonderful, Counselor, The Mighty God, The Everlasting Father, The Prince of Peace (Isaiah 9:6).", 
            fullText: <View>
                <Text style={styles.text}>
                    In the Christmas story we learn that:
                    <Br />
                </Text>
                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            <Italic>God will use people you don't know to bless you.</Italic> The wise men weren't
                            devout Jews or committed Christians, but Persian stargazers. Don't be
                            selective; you'll tie God's hands. Open your heart and you'll start seeing
                            Him reaching out to you through the most interesting people.
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            <Italic>God will take care of your tomorrows.</Italic> Joseph and Mary would have to
                            leave home and flee to Egypt. What would they live on? Gold,
                            frankincense and myrrh; gifts only the rich can give. When you're in the
                            will of God, He will provide for you. He may do it through your job or the
                            generosity of others, but He will do it. You don't have to stay up all night
                            worrying, trying to figure out how to make ends meet. The Psalmist said,
                            'I have been young, and now am old; yet I have not seen the righteous
                            forsaken, nor his descendants begging bread.' (Psalm 37:25 NKJV)
                        </Text>
                    },
                    {
                        serial: 3,
                        body: <Text>
                            <Italic>God will override the system and get you the help you need.</Italic> 'The star
                            guided them to where the young child should be born.' In the history of
                            the solar system there's never been anything like it. God moved a star like
                            we move a pawn in a chess game, that's because He <Bold>“works all things
                            according to the counsel of His will” (Ephesians 1:11 NKJV).</Bold> The
                            story reads: <Bold>“And when they had come into the house, they saw the
                            young Child with Mary His mother, and fell down and worshipped
                            Him. And when they had opened their treasures, they presented gifts
                            to Him: gold, frankincense, and myrrh” (Matthew 2:11 NKJV).</Bold>
                            Quite a story, isn't it?
                        </Text>
                    },
                ]} />
            </View>,
            prayerPoints: [
                "Lord, lead me, the way You led the wise men, in the name of Jesus.",
                "Spirit of wisdom and understanding, possess me, in the name of Jesus.",
                "God that provided for the needs of Joseph, Mary, and the Jesus Child in Egypt, shall provide for me and my family, in the name of Jesus.",
            ]
        },
        {
            day: 26,
            topic: "Stop Putting Yourself Down",
            wordStudy: "Luke 8:26-34",
            confession: "…And will give him a white stone, and upon the stone a new name written, which no one knows but he that receives it (Revelations 2:17).", 
            fullText: <View>
                <Text style={styles.text}>
                    Remember the man in our Word Study for today who was possessed by
                    demons? <Bold>“Jesus asked him, 'What is your name?' And he said, “Legion,”
                    because many devils had entered him” (v. 30).</Bold> (A legion in the Roman
                    army comprised some six thousand troops.). “Legion” wasn't this man's real
                    name; it represented his demonic oppression. He'd suffered with this
                    oppression so long that perhaps he'd come to accept it as the new norm.
                    Perhaps you're struggling along a similar vein, naming yourself “Fatso”
                    because you've battled weight for so long with no apparent victory in sight.
                    Or you define yourself as a “victim” because you were abused or taken
                    advantage of by others. Maybe you see yourself as a “failure” because you're
                    divorced, or your kids have gone off the tracks. If so, it's time to lose the
                    negative labels and start seeing yourself the way God sees you.
                    <Br />
                    Jesus set this tormented man free, gave him his right-mind back, and
                    restored him to his family. And He wants to do the same for you! Satan will
                    take you from one extreme to the other. He'll make you either boastful or
                    bashful; make you think you're “hot stuff,” or convince you that you're
                    worthless. Don't buy it! Self-deprecation is often disguised as humility, when
                    in reality it's a rejection of God's Word, which assures you that you <Bold>“can do
                    all things through Christ who strengthens [you]” (Philippians 4:13
                    NKJV).</Bold> What others call you doesn't matter; what you call yourself does!
                    The bottom line is: <Bold>“God is able to make all grace abound to you, so that in
                    all things, at all times, having all that you need, you will abound in every
                    good work” (2 Corinthians 9:8 NIV).</Bold>
                    <Br />
                </Text>
            </View>,
            prayerPoints: [
                "Spirit of rejection, depart from me, in the name of Jesus.",
                "Spirit of pride, depart from me, in the name of Jesus.",
                "My past will not destroy my future, in the name of Jesus.",
            ]
        },
        {
            day: 27,
            topic: "Seeing Your Work as God's Will (1)",
            wordStudy: "John 9:1-7",
            confession: "And he charged them, saying, Thus shall ye do in the fear of the Lord, faithfully, and with a perfect heart (2 Chronicles 19:9 KJV).", 
            fullText: <View>
                <Text style={styles.text}>
                    You'll experience a new level of fulfilment when you begin to see what you do
                    for a living as an important part of God's will for your life. Jesus preached and
                    healed, but He saw it all as “work” given to Him by His Father. You must too.
                    Instead of seeing church as a place where you meet with God on Sunday
                    morning, see it as a place where you're fed and strengthened so that you can
                    carry the presence of God with you into the workplace. <Bold>“Whatever you do,
                    whether in word or deed, do it all in the name of the Lord Jesus, giving
                    thanks to God the Father through Him” (Colossians 3:17 NIV).</Bold>
                    <Br />
                    Notice two words here: (1) <Bold>“Word.”</Bold> That covers skills of communication
                    and information. (2) <Bold>“Deed.”</Bold> That covers skills such as creativity and
                    building. Whatever you do, you're supposed to do it with a thankful heart, as
                    though the Lord were your boss—because He is. When you work with that
                    attitude, you come alive. One person comes alive when they pick up a musical
                    instrument, another when they lead a team, another when they counsel
                    someone who's hurting, and another when they're looking at a financial
                    spreadsheet. When each of us is doing what God designed and called us to do,
                    the world around us is enriched. All skill is God-given, and we're invited to
                    live in conscious interaction with the Holy Spirit as we work, so that we can
                    develop the skills He gives us. Work is a form of love. We cannot be fully
                    human without creating value.
                    <Br />
                </Text>
            </View>,
            prayerPoints: [
                "I receive the grace to carry out my Kingdom assignment with the right heart, in the name of Jesus.",
                "Let my work be a tool for ministering the Lord and the Gospel of His grace, in the name of Jesus",
                "Holy Spirit, divinely re-adjust my focus, in the name of Jesus.",
            ]
        },
        {
            day: 28,
            topic: "Seeing Your Work as God's Will (2)",
            wordStudy: "Colossians 3:16-25",
            confession: "And he did that which was right in the eyes of the Lord, but not with a perfect heart (2 Chronicles 25:2).", 
            fullText: <View>
                <Text style={styles.text}>
                    Research shows that the best moments of our lives don't come from leisure or
                    pleasure. They come when you're immersed in a significant task that's
                    challenging, yet matches up well to your highest abilities. In those moments,
                    you're so caught up in an activity that time somehow seems to be altered; your
                    attention is fully focused without your having to work at it. You're deeply
                    aware, without being self-conscious; you're being stretched and challenged,
                    but without a sense of stress or worry. You have a sense of engagement or
                    oneness with what you are doing. This condition is called “flow,” because
                    people experiencing it often use the metaphor of feeling swept up by
                    something outside themselves.
                    <Br />
                    Studies have been done over the past thirty years with hundreds of
                    thousands of subjects to explore this phenomenon of flow. Ironically, you
                    experience it more in your work than you do in your leisure time. In fact, your
                    flow is at its lowest ebb when you've nothing to do. Sitting around doesn't
                    produce flow. This picture of flow is actually a description of what the
                    exercise of dominion was intended to look like. God says in Genesis that
                    we're to <Bold>“rule”</Bold> over the earth, or exercise <Bold>“dominion” (See Genesis 1:26,
                    28.).</Bold> We often think of these words in terms of “dominating” or “bossing
                    around.” But the true idea behind them is that you're to invest your abilities to
                    create value on the earth, to plant and build and write and organise and heal
                    and invent ways that bless people and cause God's Kingdom on earth to
                    flourish.
                    <Br />
                </Text>
            </View>,
            prayerPoints: [
                "Because I am planted in house of the Lord, I shall flourish, in the name of Jesus.",
                "Holy Spirit, help me to flow with your leading, in the name of Jesus.",
                "Lord, help me to focus on my area of my grace, in the name of Jesus.",
            ]
        },
        {
            day: 29,
            topic: "Seeing Your Work as God's Will (3)",
            wordStudy: "Psalm 127:1-5",
            confession: "Whatever you do, whether in word or deed, do it all in the name of the Lord Jesus, giving thanks to God the Father through Him (Colossians 3:17).", 
            fullText: <View>
                <Text style={styles.text}>
                    When your skill level is high but the challenge of the task is too low, you
                    experience boredom. When your skill level is low and the challenge of the task
                    is too high, you experience frustration and anxiety. But when the level of the
                    challenge matches the level of your skills—then you're “in the flow.” We don't
                    work mainly for money, recognition, promotion, applause or fame. We work
                    for the flow that comes from a partnership with God. We hunger for flow, and
                    when it's present, something happens in our spirit as we connect with a reality
                    beyond ourselves and become a co-worker with God. This is why the psalmist
                    says, <Bold>“Unless the Lord builds the house, those who build it labour in vain.”</Bold>
                    Flow is part of what we experience in that partnership and, in that, God in turn
                    uses flow to shape us.
                    <Br />
                    Bezalel experienced flow when he carved wood, David when he played
                    the harp, Samson when he used his strength, Paul when he wrote a brilliant
                    letter, Daniel when he ran a government, and Adam when he gardened. If other
                    people report to you, one of the great spiritual acts of service you can perform
                    is to ask whether they're experiencing flow in their work, and help them
                    experience it even more. When you're working in the flow of service to God,
                    when you're experiencing flow in activities that enhance and bless the lives of
                    others—you're working <Bold>“in the Spirit.”</Bold> Paul was in the flow when he
                    described himself as <Bold>“poor, yet making many rich; having nothing, and yet
                    possessing everything.” (2 Corinthians 6:10 NIV).</Bold>
                    <Br />
                </Text>
            </View>,
            prayerPoints: [
                "The grace to co-labour with the Holy Spirit, rest on me, in the name of Jesus.",
                "I receive divine assistance “in the flow”to build a lasting legacy, in the name of Jesus.",
                "I shall not labour in vain, or for another to eat, in the name of Jesus.",
            ]
        },
        {
            day: 30,
            topic: "Be Grateful for What You Have",
            wordStudy: "1 Thessalonians 5:17-23",
            confession: "O give thanks unto the Lord; for he is good; for his mercy endures for ever (Psalm 136:1).", 
            fullText: <View>
                <Text style={styles.text}>
                    Theoretical physicist Stephen Hawking has a brilliant mind. He has been
                    compared by some to Albert Einstein. But he has a rare degenerative disease
                    called amyotrophic lateral sclerosis (ALS syndrome or motor neurone
                    disease). And it has left him virtually paralysed. He learned to use a computer
                    with the tips of his fingers and was able to communicate his calculations and
                    thoughts. Before he became ill, he described his life as a “pointless
                    existence.” He drank too much and did very little work. But after discovering
                    that he perhaps had only a few years to live, life suddenly took on an urgency
                    and a new meaning. And he was actually happier than he was before. He
                    explained the paradox this way: “When one's expectations are reduced to
                    zero, one really appreciates everything that one does have.”
                    <Br />
                    When you're told that you only have a limited time left to live, it
                    transforms your whole perspective on living. Things you overlook suddenly
                    become meaningful: the laughter of children, a sunrise or sunset, the love of
                    friends and family, or just a walk in the park. The most miserable people in the
                    world are those who believe that life “owes them.” They're never happy,
                    because they never believe they get what they deserve. The apostle Paul was
                    in prison with no hope of getting out when he wrote, <Bold>“In everything give
                    thanks; for this is the will of God in Christ Jesus for you.”</Bold> What was he
                    saying? Simply this: Instead of competing, comparing and complaining,
                    focus on the good things God has given you, enjoy them and develop an
                    attitude of gratitude <Bold>(Philippians 4:6).</Bold>
                    <Br />
                </Text>
            </View>,
            prayerPoints: [
                "Lord, forgive my every attitude and act of ungratefulness, in the name of Jesus.",
                "Spirit of gratitude, possess me and my family, in the name of Jesus.",
                "Lord, I receive grace to set my mind on the things of Heaven, and not the things of the Earth, in the name of Jesus.",
            ]
        },
        {
            day: 31,
            topic: "Don't Look Back",
            wordStudy: "Luke 17:31-37",
            confession: "But Jesus said unto him, no man, having put his hand to the plow,and looking back, is fit for the kingdom of God (Luke 9:62).", 
            fullText: <View>
                <Text style={styles.text}>
                    Before God destroyed the city of Sodom, He sent two angels to rescue Lot and
                    his family from it. Their instructions were clear: <Bold>“Do not look behind you nor
                    stay anywhere in the plain. Escape to the mountains, lest you be
                    destroyed…” (Genesis 19:17 NKJV).</Bold> But, <Bold>“his wife looked back behind
                    him, and she became a pillar of salt.” (Genesis 19:26 NKJV).</Bold> Jesus recalled
                    this story in a simple statement: “Remember Lot's wife.” Here are two
                    lessons you should learn and remember:
                    <Br />
                </Text>
                <IndentList items={[
                    {
                        serial: 1,
                        body: <Text>
                            Don't look back with longing to your old sinful pleasures and pursuits.
                            The promise sin made was false then, and it's still false now. <Bold>“Each one is
                            tempted when he is drawn away by his own desires and enticed.
                            Then, when desire has conceived, it gives birth to sin; and sin, when it
                            is full-grown, brings forth death. Do not be deceived, my beloved
                            brethren” (James 1:14-16 NKJV).</Bold>
                        </Text>
                    },
                    {
                        serial: 2,
                        body: <Text>
                            Don't look back with regret on the mistakes you've made. Lot's two sonsin-
                            law wouldn't heed the message of the angels or the pleading of their
                            father-in-law, so they stayed behind. Perhaps that's why his wife looked
                            back. But it was fatal then, and it's fatal now.
                        </Text>
                    },
                ]} />
                <Text style={styles.text}>
                    <Br />
                    Don't wallow in the regrets of your past. Stop dwelling on the injuries
                    inflicted on you by others. Get your eyes off the rear-view mirror and onto the
                    road ahead. God has great things in store for you; that's what the battle in your
                    life is about. Jesus referred to Satan as a <Bold>“thief” (See John 10:10.).</Bold> He has
                    already stolen too much from you; don't let him steal any more. Today commit
                    your life to Christ, and watch His blessing begin.
                </Text>
            </View>,
            prayerPoints: [
                "Lord, as I enter this New Year, help me not to look back but look ahead to the great future you have in store for me, in the name of Jesus.",
                "The grace for me not to go back to my sinful ways, rest on me, in the name of Jesus.",
                "In the name of Jesus, I declare that my future is bright and my destiny is great.",
            ]
        },
    ],
}