import React from 'react';
import { Text, StyleSheet } from 'react-native';

export default Italic = ({
    children
}) => {
    return (
        <Text style={styles.italic}>{children}</Text>
    )
};

const styles = StyleSheet.create({
    italic: {
        fontStyle: 'italic',
        fontFamily: 'lato-regular',
    }
});
