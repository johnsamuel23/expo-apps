import React from 'react';
import { Text, StyleSheet, View, Dimensions } from 'react-native';

export default IndentList = ({
    items,
    padding,
    hasView,
    style
}) => {

    padding = padding ? padding : 20;
    
    let listItems = items.map((item, index) => {
        let body = hasView ? (<View>{item.body}</View>) : (<Text style={styles.bodyText}>{item.body}</Text>);
        return (
            <View key={index} style={styles.itemGroup}>
                <View style={styles.serialNumber}>
                    <Text style={styles.serialText}>{item.serial}</Text>
                </View>
                <View style={styles.body}>
                    {body}
                </View>
            </View>
        );
    });

    return (
        <View style={[styles.indentView, {paddingHorizontal: padding}, style]}>
            {listItems}
        </View>
    )
};

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row'
    },
    indentView: {
        width: Dimensions.get("window").width,
        // paddingHorizontal: 20
    },
    bold: {
        fontFamily: 'lato-heavy'
    },
    serialText: {
        fontFamily: 'lato-heavy',
        marginRight: 10,
    },
    serialNumber: {
        width: 30,
        marginTop: 1
    },
    bodyText: {
        color: 'black',
        fontFamily: 'lato-regular',
        fontSize: 16
    },
    body: {
      flex: 1  
    },
    listItem: {
        flex: 1,
        fontFamily: 'lato-regular',
    },
    itemGroup: {
        flexDirection: 'row',
        marginBottom: 10
    }

});
