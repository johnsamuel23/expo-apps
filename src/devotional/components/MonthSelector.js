import React, { Component } from 'react';
import { StyleSheet, View, Text, Platform} from 'react-native';
import Touch from './Touch';

export default Button = ({
    onPress,
    month,
    theme
}) => {
    return (
        <View style={styles.container}>
            <Touch action={onPress}>
                <View style={[styles.button]}>
                    <Text style={[styles.text]}>{month}</Text>
                </View>
            </Touch>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        // marginHorizontal: '5%'
    },
    text: {
        color: '#555',
        fontSize: 15,
        textAlign: 'center'
    },
    button: {
        backgroundColor: '#fff',
        padding: 15,
        marginBottom: 20,
        borderRadius: 30,
        ...Platform.select({
            android: {
              elevation: 2
            }
        }),
    }
});