import React from 'react';
import { Text, StyleSheet } from 'react-native';

export default Bold = ({
    children
}) => {
    return (
        <Text style={styles.bold}>{children}</Text>
    )
};

const styles = StyleSheet.create({
    bold: {
        fontFamily: 'lato-heavy'
    }
});
