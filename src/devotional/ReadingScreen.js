import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, Dimensions, Image, TouchableOpacity, Platform } from 'react-native';
import MonthSelector from './components/MonthSelector';
import LocalImage from './components/LocalImage';
import { devotionContent } from './data/MonthsCapsules';
import theme, { capsuleTheme } from './data/theme';
import BackgroundImage from './components/BackgroundImage';
import { getAudio } from './data/capsuleData';
import { Audio, Constants, KeepAwake  } from 'expo';
import BackIcon from './utils/icons/Back';
import SearchIcon from './utils/icons/Menu';

export default class ReadingScreen extends Component {

  componentDidMount() {
    this.props.activateCapsule();
    // this.props.playSoundTrack();
    KeepAwake.activate();
  }

  componentWillUnmount() {
    // this.props.stopSoundTrack();
    this.props.deactivateCapsule();
    KeepAwake.deactivate();
  }

  render() {

    let { selectedMonth, selectedDay } = this.props.store;
    let dayIndex = selectedDay - 1;
    data = devotionContent[selectedMonth];
    let post = data.find(t => t.day === selectedDay);

    let theme = {...capsuleTheme[selectedMonth]};
    if(selectedMonth === "December" || selectedMonth === "August") {
      theme.headerTextColor = "#333";
    }

    let prayerPoints = post.prayerPoints.map((item, index) => {
        return (
            <Text key={index} style={styles.prayerText}>*  {item}</Text>
        )
    });

    return (
      <View style={[styles.container]}>
        <View style={styles.statusBar}></View>
        <View style={{flex: 1, zIndex: 3}}>
          <View style={styles.heading}>
            <View style={[styles.headBox]}>
              <View style={styles.headerLeft}>
                <TouchableOpacity style={styles.backIcon} onPress={() => this.props.navigator.pop({name: 'capsule_screen'})}>
                  <BackIcon color={"white"} size={35} />
                </TouchableOpacity>
              </View>
              <View style={styles.headerCenter}>
                <Text style={[styles.headText]}> Today's Study: </Text>
                <Text style={[styles.headText, {fontFamily: 'lato-regular'}]}>{selectedMonth} { selectedDay }</Text>
              </View>
              <View style={styles.headerRight}>
                <TouchableOpacity activeOpacity={0.5} style={styles.backIcon} onPress={() => this.props.navigator.push({name: 'months_menu'})}>
                  <SearchIcon color={"white"} size={35} />
                </TouchableOpacity>
              </View>
            </View>
          </View>
          
          <View style={[styles.capsuleContainer]}>
            <ScrollView style={{zIndex: 10}} contentContainerStyle={styles.contentText} showsVerticalScrollIndicator={false}>
              <View style={styles.topicHead}>
                <View style={styles.topicHeading}>
                  <Text style={styles.topicText}><Text style={{fontFamily: 'lato-heavy'}}>Topic: </Text><Text style={{fontFamily: 'lato-regular'}}>{post.topic}</Text></Text>
                </View>
                <View style={styles.topicHeading}>
                  <Text style={styles.topicText}><Text style={{fontFamily: 'lato-heavy'}}>Word Study: </Text><Text style={{fontFamily: 'lato-regular'}}>{post.wordStudy}</Text></Text>              
                </View>
                <View style={[styles.topicHeading, styles.lastTopicHead]}>
                  <Text style={styles.topicText}><Text style={{fontFamily: 'lato-heavy'}}>Confession: </Text><Text style={{fontFamily: 'lato-regular'}}>{post.confession}</Text></Text>              
                </View>
              </View>
              <View style={styles.scriptureBox}>

                  { post.fullText }
                  
              </View>
              <View style={[styles.scriptureBox, styles.prayerBox]}>
                <Text style={{color: "black", borderBottomColor: 'white', display: 'flex', fontFamily: 'lato-heavy'}}>PRAYER</Text>
                <View style={{borderBottomWidth: 1, height: 2, width: 70, marginTop: 5, backgroundColor: 'black'}}></View>

                  { prayerPoints }

              </View>
            </ScrollView>
            <View style={styles.absoluteBanner}>
                {/* <Image 
                  source={require('./img/Prayer_Capsules_bread.png')} 
                  style={{
                      width: 120,
                      height: 120
                  }}
              /> */}
            </View>
          </View>
        </View>
        <View style={[styles.backgroundView, {zIndex: 2}]}>
          <View style={styles.backgroundImage}>
            {/* <LocalImage
                originalHeight={457}
                originalWidth={467}
                source={require('./img/pastor_and_mummy_circle.png')}
                guideWidth={90}
            /> */}
          </View>
        </View>
      </View>
    );
  }

}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#000',
    // padding: 20,
    // justifyContent: 'center',
    // alignItems: 'center',
    flex: 1
  },
  topicHeading: {
    backgroundColor: theme.topSectionBackgroundColor,
    padding: 10,
    paddingVertical: 8
  },
  topicHead: {
    ...Platform.select({
      android: {
        elevation: 2,
      }
    }),
  },
  topicText: {
    fontSize: 16,
    color: 'black',
    // fontFamily: 'lato-heavy',
    paddingHorizontal: 10
  },
  wordStudyText: {
    fontSize: 16,
    color: 'black',
    fontFamily: 'lato-heavy',
    paddingHorizontal: 10
  },
  confessionText: {
    fontSize: 16,
    color: 'black',
    fontFamily: 'lato-heavy',
    paddingHorizontal: 10
  },
  prayerPoint: {
    textAlign: 'left'
  },
  menuIcon: {
    position: 'absolute',
    left: 25,
    top: 15,
    width: 50,
    justifyContent: 'center'
  },
  backgroundView: {
    position: 'absolute',
    backgroundColor: theme.contentOffsetBackgroundColor,
    height: Dimensions.get('window').height,
    width: Dimensions.get('window').width,
    justifyContent: 'flex-start',
    alignItems: 'flex-end',
    zIndex: 0,
  }, 
  backgroundImage: {
    opacity: 0.7,
    marginRight: 10,
    marginTop: 110,
  }, 
  statusBar: {
    backgroundColor: theme.statusBarColor,
    height: Constants.statusBarHeight,
    zIndex: 100
  },
  BackIcon: {
    width: 50, 
  },
  heading: {
    // paddingLeft: 15,
    // paddingRight: 15,
    // paddingTop: 15,
    // paddingBottom: 15,
    backgroundColor: theme.headerColor,
    // borderTopLeftRadius: 30,
    // borderTopRightRadius: 30,
    // marginTop: 10
  }, 
  headBox: {
    // borderRadius: 25,
    height: 45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    // borderBottomWidth: 1,
    // borderBottomColor: '#eee'
    ...Platform.select({
        android: {
          elevation: 3
        }
    }),
  },
  headText: {
    textAlign: 'center',
    fontSize: 15,
    fontFamily: 'lato-heavy',
    color: 'white'
  },
  headerLeft: {
    width: 50,
    paddingLeft: 20
  },
  headerCenter: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  }, 
  headerRight: {
    width: 50,
    justifyContent: 'flex-end',
    marginRight: -5
  },
  scriptureText: {
    fontSize: 17,
    textAlign: 'left',
    paddingTop: 20,
    paddingLeft: 10,
    paddingRight: 10,
    fontFamily: 'lato-regular'
  },
  prayerText: {
    // textAlign: 'center',
    fontSize: 16,
    fontFamily: 'lato-heavy',
    paddingTop: 20,
    paddingLeft: 10,
    paddingRight: 10,
  },
  scriptureBox: {
    justifyContent: 'center',
    paddingLeft: 20,
    paddingTop: 20,
    paddingRight: 20,
    alignItems: 'center',
    zIndex: 10,
    marginBottom: 10,
    flex: 1,
  },
  prayerBox: {
    backgroundColor: theme.topSectionBackgroundColor,
    marginBottom: -10,
    justifyContent: 'flex-start',
    paddingBottom: 20,
    marginTop: 20,
    ...Platform.select({
      android: {
        elevation: 5,
        borderTopWidth: 1,
        borderTopColor: 'rgba(100,100,100,0.1)'
      }
  }),
  },
  contentText: {
    zIndex: 10,
    // paddingTop: 30,
    // marginBottom: 10,
    paddingBottom: 10,
    // marginLeft: 20,
    // marginRight: 20,
  },
  capsuleContainer: {
    flex: 1,
    backgroundColor: 'white',
    width: theme.contentAreaWidth,
    marginLeft: 'auto',
    marginRight: 'auto',
    marginTop: theme.readingContentMarginTop
  },
  capsuleHeader: {
    paddingTop: 20,
    // paddingBottom: 20,
    justifyContent: 'center',
    alignItems: 'center'
  },
  absoluteBanner: {
    position: 'absolute',
    bottom: 0,
    zIndex: 0,
  }
});
