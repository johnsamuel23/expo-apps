import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, TouchableOpacity, Platform } from 'react-native';
import MonthSelector from './components/MonthSelector';
import theme, { capsuleTheme } from './data/theme';
import BackIcon from './utils/icons/Back';
import { Constants } from 'expo';

export default MonthsMenu = ({
  selectMonth,
  store,
  navigator
}) => {

  let { months } = store;
  let monthSelectOptions = months.map((item, index) => {
      let theme = capsuleTheme[item.name];
      
      return <MonthSelector theme={theme} key={index} month={item.name} onPress={() => {
          selectMonth(item.name);
          setTimeout(() => {
            navigator.push({name: 'days_menu'});
          }, 50);
        }} />
  });
  
  return (
    <View style={styles.container}>
      <View style={styles.statusBar}></View>
      <View style={styles.header}>
        <View style={styles.headerLeft}>
          <TouchableOpacity style={{width: 50}} onPress={() => navigator.pop({name: 'months_menu'})}>
            <BackIcon size={35} color='#fff' />
          </TouchableOpacity>
        </View>
        <View style={styles.headerCenter}>
          <Text style={styles.headerText}>Select a Month</Text>
        </View>
        <View style={styles.headerRight}></View>
      </View>
      <View style={[styles.menuContainer]}>
        <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={styles.monthsWrap}>
          { monthSelectOptions }
        </ScrollView>
      </View>
    </View>  
  )
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: theme.menuBackgroundColor,
    // paddingLeft: 20,
    // paddingRight: 20,
    paddingTop: 0,
    paddingBottom: 0,
    flex: 1
  },
  headerText: {
    color: '#fff',
    fontSize: 20,
    textAlign: 'center'
  },
  monthsWrap: {
    flexWrap: 'wrap',
    alignItems: 'flex-start',
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  header: {
    minHeight: 50,
    flexDirection: 'row',
    paddingHorizontal: 20,
    ...Platform.select({
        android: {
          elevation: 2
        }
    }),
  },
  headerLeft: {
    // backgroundColor: 'green',
    justifyContent: 'center',
    width: 70
  },
  headerCenter: {
    // backgroundColor: 'blue',
    justifyContent: 'center',
    flex: 1
  },
  headerRight: {
    // backgroundColor: 'red',
    justifyContent: 'center',
    width: 70
  },
  menuContainer: {
    // backgroundColor: '#333',
    padding: 20,
    width: theme.contentAreaWidth,
    marginLeft: 'auto',
    marginRight: 'auto',
    // borderTopLeftRadius: 30,
    // borderTopRightRadius: 30,
    // borderBottomLeftRadius: 5,
    // borderBottomRightRadius: 5,
    flex: 1
  },
  statusBar: {
    backgroundColor: theme.statusBarColor,
    height: Constants.statusBarHeight,
    width: '100%'
  },
});
