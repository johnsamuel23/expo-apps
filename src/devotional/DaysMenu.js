import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, TouchableOpacity } from 'react-native';
import DaySelector from './components/DaySelector';
import theme, { capsuleTheme } from './data/theme';
import BackIcon from './utils/icons/Back';
import { Constants } from 'expo';

export default DaysMenu = ({
  store,
  navigator,
  selectDay
}) => {

    let { months, selectedMonth } = store;

    let selectedMonthObject = months.find(month => month.name === selectedMonth) || {};

    let daySelectOptions = selectedMonthObject.days.map((item, index) => {
        return <DaySelector 
                  key={index} 
                  day={item.day}
                  theme={theme}
                  onPress={() => {
                      navigator.push({name: 'reading_screen'}); 
                      selectDay(item.day) 
                    }}
                  />
    });
    
    return (
      <View style={[styles.container]}>
        <View style={[styles.statusBar]}></View>
        <View style={[styles.monthHead]}>
          <View style={styles.headerLeft}>
            <TouchableOpacity style={{width: 50}} onPress={() => navigator.pop({name: 'days_menu'})}>
              <BackIcon color={"white"} size={35} />
            </TouchableOpacity>
          </View>
          <View style={styles.headerCenter}>
            <Text style={[styles.headText]}>{ selectedMonth }</Text>
          </View>
          <View style={styles.headerRight}></View>
        </View>
        <ScrollView>
          <View style={styles.daysList}>
            <Text style={styles.cue}>Day</Text> 
            <ScrollView style={styles.daysBox} contentContainerStyle={[styles.daysBoxWrapper]} >

                { daySelectOptions }

            </ScrollView>
          </View>
        </ScrollView>
      </View>
    );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: theme.menuBackgroundColor,
    flex: 1
  },
  statusBar: {
    backgroundColor: theme.statusBarColor,
    height: Constants.statusBarHeight,
  },
  headerLeft: {
    width: 70,
    justifyContent: 'center',
  },
  headerCenter: {
    justifyContent: 'center',
    alignSelf: 'center',
    flex: 1
  },
  headerRight: {
    width: 70
  },
  monthHead: {
    backgroundColor: theme.headerColor,
    paddingLeft: 20,
    paddingRight: 20,
    height: 50,
    alignItems: 'center',
    flexDirection: 'row'
    // marginTop: 10
  },
  headText: {
    fontSize: 20,
    textAlign: 'center',
    color: 'white'
  },
  daysList: {
    justifyContent: 'center',
    alignItems: 'center',
    padding: 20,
    width: theme.contentAreaWidth,
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  cue: {
    fontSize: 15,
    marginBottom: 20,
    color: "#fff"
    // alignSelf: 'flex-start'
  },
  daysBox: {
    flexDirection: 'column',
  },
  daysBoxWrapper: {
    flexWrap: 'wrap',
    alignItems: 'flex-start',
    flexDirection: 'row',
    justifyContent: 'space-between'
  }
});
