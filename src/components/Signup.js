import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, TextInput, KeyboardAvoidingView } from 'react-native';

export default class Signup extends Component {
  componentWillMount() {
    this.state = {
        pageBackgroundColor: '#00bcd4',
    }
  }

  changeBackgroundColor(newColor) {
    this.setState({
        pageBackgroundColor: newColor
    })
  }

  render() {

    styles.container.backgroundColor = this.state.pageBackgroundColor

    return (
        <KeyboardAvoidingView style={styles.container}>
            <View style={styles.header}>
                <Text style={styles.headerText}> Register and Enjoy </Text>
            </View>
            <View style={styles.mainContent}>
                <KeyboardAvoidingView>
                    <TextInput style={[styles.textInput, styles.textInputV1]} placeholder="Firstname" underlineColorAndroid="transparent" />
                    <TextInput style={[styles.textInput, styles.textInputV1]} placeholder='Lastname' underlineColorAndroid="transparent" />
                    <TextInput style={[styles.textInput, styles.textInputV1]} placeholder="Disability" underlineColorAndroid="transparent" />
                </KeyboardAvoidingView>
            </View>
        </KeyboardAvoidingView>
    );
  }
}


let styles = StyleSheet.create({
    container: {
        backgroundColor: '#00bcd4',
        flex: 1
    },
    header: {
        padding: 20,
        marginBottom: 50,
        backgroundColor: '#555',
        flexDirection: 'column',
        minHeight: 80
    },
    headerText: {
        fontSize: 25,
        color: 'white',
        marginTop: 50,
        alignItems: 'center',
    },
    proceedBtn: {
        backgroundColor: 'orange',
        padding: 10,
        borderRadius: 20,
        alignItems: 'center',
        width: 100
    },
    readyText: {
        marginTop: 50,
        fontSize: 20,
        marginBottom: 20,
    },
    mainContent: {
        padding: 20,
        flex: 5,
        justifyContent: 'space-around'
    },
    textInput: {
        height: 40,
        borderColor: 'gray',
        borderWidth: 1,
        color: 'black',
        backgroundColor: 'white',
        marginBottom: 10,
        alignItems: 'flex-end'
    }
})