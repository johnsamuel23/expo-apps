import React, { PropTypes, Component } from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import Tts from 'react-native-tts';
import SpeechRecognition from 'react-speech-recognition';

const propTypes = {
  // Props injected by SpeechRecognition
  transcript: PropTypes.string,
  resetTranscript: PropTypes.func,
  browserSupportsSpeechRecognition: PropTypes.bool
}

export default class SplashScreen extends Component {
  componentWillMount() {
    this.state = {
        pageBackgroundColor: '#00bcd4',
    }
  }

  changeBackgroundColor(newColor) {
    this.setState({
        pageBackgroundColor: newColor
    })
  }

  Signup() {
      this.props.changeComponent('Signup')
  }

  componentDidMount() {
    //   Add utterance to TTS queue and start speaking. Returns promise with utteranceId.
        // Tts.setDefaultLanguage('en-IE');
        // Tts.setDucking(true);
        // console.log('TTS.SPEECH', Tts.speak('YEAH'));
        // Tts.speak('Hello, world!');

    //   Stop speaking and flush the TTS queue.
        // Tts.stop();
        console.log(SpeechRecognition);

  } 

  render() {

    styles.container.backgroundColor = this.state.pageBackgroundColor

    return (
      <View style={styles.container}>
          <View>
            <Text style={styles.headerText}>Welcome To Chapair</Text>
            <View style={styles.mainContent}>
                <Text style={styles.readyText}> Ready ? </Text>

                <TouchableOpacity style={styles.proceedBtn} onPress={() => this.Signup() }>
                    <Text style={styles.proceedBtnText}>Sign Up</Text>
                </TouchableOpacity>

            </View>
          </View>
      </View>
    );
  }
}


let styles = StyleSheet.create({
    container: {
        backgroundColor: '#00bcd4',
        flex: 1,
        alignItems: 'center',
        paddingTop: 50,
        paddingLeft: 20,
        paddingRight: 20
    },
    header: {
        padding: 20
    },
    headerText: {
        fontSize: 25,
        color: 'white',
        marginBottom: 50,
        marginTop: 50,
        alignItems: 'center'
    },
    proceedBtn: {
        backgroundColor: 'orange',
        padding: 10,
        borderRadius: 20,
        alignItems: 'center',
        width: 100
    },
    readyText: {
        marginTop: 50,
        fontSize: 20,
        marginBottom: 20,
    },
    mainContent: {
        alignItems: 'center'
    }
})