import React, { Component } from 'react'
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import SplashScreen from './SplashScreen'
import Signup from './Signup'

export default class Root extends Component {
    componentWillMount() {
        this.state = {
            currentComponent: 'SplashScreen'
            // currentComponent: 'Signup'
        }
    }

    changeComponent(newComponent) {
        this.setState({
            currentComponent: newComponent
        })
    }

    getCurrentComponent() {
        switch(this.state.currentComponent) {
            case 'SplashScreen':
                return <SplashScreen changeComponent={this.changeComponent.bind(this)}/>
            case 'Signup':
                return <Signup changeComponent={this.changeComponent.bind(this)}/>
            default: 
                return <SplashScreen changeComponent={this.changeComponent.bind(this)}/>
        }
    }

    render() {

        let componentToRender = this.getCurrentComponent();

        return (
            <View style={styles.container}>

                { componentToRender }

            </View>
        )
    }
}


let styles = StyleSheet.create({
    container: {
        backgroundColor: '#00bcd4',
        flex: 1,
    }
})