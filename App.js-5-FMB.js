import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, KeyboardAvoidingView } from 'react-native';
import Root from './src/fmb';
import { Font, AppLoading } from 'expo';
// import AppLoadingScreen from './src/navigator/AppLoading';
const AppLoadingScreen = AppLoading;
// import Sentry from 'sentry-expo';

// Remove this once Sentry is correctly setup.
// Sentry.enableInExpoDevelopment = true;

// DSN - private
// https://c29167ee76214d3899c3adba4f78f348:883cae96a7bb4525a31909b9176f8b3b@sentry.io/242746

// DSN (Public)
// https://c29167ee76214d3899c3adba4f78f348@sentry.io/242746

// CSP Endpoint
// https://sentry.io/api/242746/csp-report/?sentry_key=c29167ee76214d3899c3adba4f78f348
// Use your CSP endpoint in the report-uri directive in your Content-Security-Policy header.

// Sentry.config('https://c29167ee76214d3899c3adba4f78f348@sentry.io/242746').install();

export default class App extends Component {

  state = {
    isReady: false
  }

  async componentWillMount() {
    
    // Expo.Util.reload() // To reload app and fetch published updates.

    await Font.loadAsync({
      'saira-bold': require('./src/fmb/assets/fonts/Saira-Bold.ttf'),
      'saira-light': require('./src/fmb/assets/fonts/Saira-Light.ttf'),
      'saira-medium': require('./src/fmb/assets/fonts/Saira-Medium.ttf'),
      'saira-regular': require('./src/fmb/assets/fonts/Saira-Regular.ttf'),
      'saira-semibold': require('./src/fmb/assets/fonts/Saira-SemiBold.ttf'),
      'saira-thin': require('./src/fmb/assets/fonts/Saira-Thin.ttf'),
      'segoe-ui': require('./assets/fonts/segoeui.ttf'),
    });

    this.setState({ isReady: true });
  }

  render() {
    if(!this.state.isReady) {
      return <AppLoadingScreen />
    }

    return (
      <KeyboardAvoidingView style={[styles.container]} behavior="padding">
        <View style={styles.container}>
          <Root />
        </View>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});
