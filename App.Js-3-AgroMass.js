import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, KeyboardAvoidingView } from 'react-native';
import Root from './src/agrostore';
import { Font, AppLoading } from 'expo';

export default class App extends Component {
  
  state = {
    isReady: false
  }

  async componentWillMount() {
    
    // Expo.Util.reload() // To reload app and fetch published updates.

    await Font.loadAsync({
        'lato-light': require('./assets/fonts/Lato-Light.ttf'),
        'lato-heavy': require('./assets/fonts/Lato-Heavy.ttf'),
        'lato-regular': require('./assets/fonts/Lato-Regular.ttf'),
    });

    this.setState({ isReady: true });
  }

  render() {
    if(!this.state.isReady) {
      return <AppLoading />
    }

    return (
      <KeyboardAvoidingView style={[styles.container]} behavior="padding">
        <View style={styles.container}>
          <Root />
        </View>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
